﻿using Kalisto.Core;
using System.Windows.Media.Imaging;

namespace Kalisto.Devices.Camera.Drivers
{
  public interface ICameraDriver
  {
    BitmapImage GetSnapshot(string url, string username, string password);
  }

  public class CameraDriver : ICameraDriver
  {
    private readonly IWebImageService _imageService;

    public CameraDriver(IWebImageService imageService)
    {
      _imageService = imageService;
    }

    public BitmapImage GetSnapshot(string url, string username, string password)
    {
      return _imageService.GetImageSource(url, username, password);
    }
  }
}
