﻿namespace Kalisto.Devices.Camera.Models
{
  public class CameraInfo
  {
    public int CameraId { get; set; }

    public int DeviceId { get; set; }

    public string CameraAddress { get; set; }

    public string Username { get; set; }

    public string Password { get; set; }
  }
}
