﻿using System.Windows.Media.Imaging;

namespace Kalisto.Devices.Camera.Models
{
  public class CameraResponse
  {
    public CameraInfo CameraInfo { get; set; }

    public BitmapImage Image { get; set; }
  }
}
