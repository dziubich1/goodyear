﻿using Kalisto.Devices.Camera.Drivers;
using Kalisto.Devices.Camera.Models;
using System;
using System.Collections.Generic;
using System.Threading;

namespace Kalisto.Devices.Camera.Services
{
  public interface ICameraService
  {
    void Configure(CameraInfo[] cameraInfos);

    void StartFetching();

    void StopFetching();

    CameraResponse TakeSnapshot(CameraInfo info);

    event Action<CameraResponse> SnapshotReceived;
  }

  public class CameraService : ICameraService
  {
    private readonly ICameraDriver _cameraDriver;

    private readonly List<CameraInfo> _cameraInfos;

    private bool _fetching;

    public event Action<CameraResponse> SnapshotReceived;

    public CameraService(ICameraDriver cameraDriver)
    {
      _cameraDriver = cameraDriver;
      _cameraInfos = new List<CameraInfo>();
    }

    public void Configure(CameraInfo[] cameraInfos)
    {
      _cameraInfos.Clear();
      _cameraInfos.AddRange(cameraInfos);
    }

    public CameraResponse TakeSnapshot(CameraInfo info)
    {
      return FetchCameraData(info);
    }

    public void StartFetching()
    {
      _fetching = true;
      foreach (var camInfo in _cameraInfos)
      {
        var thread = new Thread(() =>
          {
            while (_fetching)
            {
              var response = FetchCameraData(camInfo);
              SnapshotReceived?.Invoke(response);
              Thread.Sleep(300);
            }
          })
        { IsBackground = true };
        thread.Start();
      }
    }

    public void StopFetching()
    {
      _fetching = false;
    }

    private CameraResponse FetchCameraData(CameraInfo cameraInfo)
    {
      var result = _cameraDriver.GetSnapshot(cameraInfo.CameraAddress, cameraInfo.Username, cameraInfo.Password);
      return new CameraResponse
      {
        CameraInfo = cameraInfo,
        Image = result
      };
    }
  }
}
