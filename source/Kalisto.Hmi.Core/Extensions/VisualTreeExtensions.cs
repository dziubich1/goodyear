﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;

namespace Kalisto.Hmi.Core.Extensions
{
  public static class VisualTreeExtensions
  {
    public static IEnumerable<DependencyObject> GetAllChildren(this DependencyObject parent)
    {
      for (int i = 0; i < VisualTreeHelper.GetChildrenCount(parent); i++)
      {
        // retrieve child at specified index
        var directChild = (Visual)VisualTreeHelper.GetChild(parent, i);

        // return found child
        yield return directChild;

        // return all children of the found child
        foreach (var nestedChild in directChild.GetAllChildren())
          yield return nestedChild;
      }
    }

    public static List<T> FindVisualChildren<T>(DependencyObject obj) where T : DependencyObject
    {
      List<T> children = new List<T>();
      for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
      {
        var o = VisualTreeHelper.GetChild(obj, i);
        if (o is T)
          children.Add((T)o);

        children.AddRange(FindVisualChildren<T>(o)); // recursive
      }
      return children;
    }

    public static T FindUpVisualTree<T>(DependencyObject initial) where T : DependencyObject
    {
      DependencyObject current = initial;

      while (current != null && current.GetType() != typeof(T))
      {
        current = VisualTreeHelper.GetParent(current);
      }
      return current as T;
    }
  }
}
