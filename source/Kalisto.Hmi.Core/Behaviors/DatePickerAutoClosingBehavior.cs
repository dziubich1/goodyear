﻿using MahApps.Metro.Controls;
using System;
using System.Windows.Interactivity;

namespace Kalisto.Hmi.Core.Behaviors
{
  public class DatePickerAutoClosingBehavior : Behavior<DateTimePicker>
  {
    protected override void OnAttached()
    {
      base.OnAttached();

      AssociatedObject.SelectedDateChanged += OnSelectedDateChanged;
    }

    protected override void OnDetaching()
    {
      base.OnDetaching();

      AssociatedObject.SelectedDateChanged -= OnSelectedDateChanged;
    }

    private void OnSelectedDateChanged(object sender, TimePickerBaseSelectionChangedEventArgs<DateTime?> e)
    {
      if (e.OldValue?.Date == e.NewValue?.Date)
        return;

      if (AssociatedObject.IsDropDownOpen)
        AssociatedObject.IsDropDownOpen = false;
    }
  }
}
