﻿using System.Windows;
using System.Windows.Interactivity;
using Kalisto.Configuration.Permissions;
using Kalisto.Hmi.Core.AttachedProperties;
using Kalisto.Hmi.Core.Helpers;
using Kalisto.Hmi.Core.Services;

namespace Kalisto.Hmi.Core.Behaviors
{
  public class ApplyUserPermissionsBehavior : Behavior<UIElement>
  {
    private IAuthorizationService _authorizationService;

    protected override void OnAttached()
    {
      base.OnAttached();

      _authorizationService = FrameworkHelper.Resolve<IAuthorizationService>();
      if (_authorizationService == null)
        return;

      _authorizationService.Authorized += OnAuthorized;
      ApplyPermissions();
    }

    protected override void OnDetaching()
    {
      _authorizationService.Authorized += OnAuthorized;

      base.OnDetaching();
    }

    private void OnAuthorized()
    {
      ApplyPermissions();
    }

    public void ApplyPermissions()
    {
      var ruleName = AuthorizationProperties.GetAuthorizeActionName(AssociatedObject);
      if (string.IsNullOrEmpty(ruleName))
        return;

      var permissions = _authorizationService.GetPermissionForRule(ruleName);
      if (permissions == PermissionType.Full)
      {
        AssociatedObject.IsEnabled = true;
        AssociatedObject.Visibility = Visibility.Visible;
      }

      if (permissions == PermissionType.ReadOnly)
      {
        AssociatedObject.IsEnabled = false;
        AssociatedObject.Visibility = Visibility.Visible;
      }

      if (permissions == PermissionType.None)
      {
        AssociatedObject.IsEnabled = false;
        AssociatedObject.Visibility = Visibility.Collapsed;
      }
    }
  }
}
