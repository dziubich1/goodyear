﻿using System;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Windows;
using System.Windows.Automation.Peers;
using System.Windows.Automation.Provider;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Interactivity;
using System.Windows.Threading;

namespace Kalisto.Hmi.Core.Behaviors
{
  public class ComboBoxAutoWidthBehavior : Behavior<ComboBox>
  {
    private readonly DependencyPropertyDescriptor _itemsSourceDescriptor =
      DependencyPropertyDescriptor.FromProperty(ItemsControl.ItemsSourceProperty, typeof(ComboBox));

    protected override void OnAttached()
    {
      base.OnAttached();

      _itemsSourceDescriptor.AddValueChanged(AssociatedObject, OnItemsSourceChanged);
    }

    protected override void OnDetaching()
    {
      base.OnDetaching();

      _itemsSourceDescriptor.RemoveValueChanged(AssociatedObject, OnItemsSourceChanged);
    }

    private void OnItemsSourceChanged(object sender, EventArgs e)
    {
      var collection = AssociatedObject.ItemsSource as IObservableCollection;
      if (collection == null)
        return;

      collection.CollectionChanged += OnCollectionChanged;
    }

    private void OnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
    {
      void SetAutoWidth()
      {
        AssociatedObject.SetWidthFromItems();
        AssociatedObject.SelectedIndex = 0;
      }

      AssociatedObject.Dispatcher.BeginInvoke((Action)SetAutoWidth, DispatcherPriority.ContextIdle);
    }
  }

  public static class ComboBoxExtensionMethods
  {
    public static void SetWidthFromItems(this ComboBox comboBox)
    {
      double comboBoxWidth = 39;
      var peer = new ComboBoxAutomationPeer(comboBox);
      var provider = (IExpandCollapseProvider)peer.GetPattern(PatternInterface.ExpandCollapse);
      if (provider == null)
        return;

      EventHandler eventHandler = null;
      eventHandler = delegate
      {
        if (!comboBox.IsDropDownOpen ||
            comboBox.ItemContainerGenerator.Status != GeneratorStatus.ContainersGenerated)
          return;

        double width = 0;
        foreach (var item in comboBox.Items)
        {
          var comboBoxItem = comboBox.ItemContainerGenerator.ContainerFromItem(item) as ComboBoxItem;
          if(comboBoxItem == null)
            continue;

          comboBoxItem.Measure(new Size(double.PositiveInfinity, double.PositiveInfinity));
          if (comboBoxItem.DesiredSize.Width > width)
            width = comboBoxItem.DesiredSize.Width;
        }
        comboBox.Width = comboBoxWidth + width;
        comboBox.ItemContainerGenerator.StatusChanged -= eventHandler;
        comboBox.DropDownOpened -= eventHandler;
        provider.Collapse();
      };

      comboBox.ItemContainerGenerator.StatusChanged += eventHandler;
      comboBox.DropDownOpened += eventHandler;
      provider.Expand();
    }
  }
}
