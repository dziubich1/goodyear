﻿using System;
using System.Windows.Data;
using System.Windows.Input;
using Kalisto.Configuration.Permissions;
using Kalisto.Hmi.Core.Commands;
using Kalisto.Hmi.Core.Services;
using PropertyChanged;

namespace Kalisto.Hmi.Core
{
  [AddINotifyPropertyChangedInterface]
  public abstract class DataGridViewModel<TModel> : ViewModelBase
  {
    protected IAuthorizationService AuthorizationService { get; }

    public ListCollectionView Items { get; set; }

    public ICommand GridItemDoubleClickCommand { get; set; }

    protected DataGridViewModel(IDialogService dialogService, IAuthorizationService authorizationService) 
      : base(dialogService)
    {
      AuthorizationService = authorizationService;
      GridItemDoubleClickCommand = new DelegateCommand(InitializeEditItem);
    }

    protected abstract ObservableCollectionEx<TModel> GetItemsSource();

    protected override void OnViewReady(object view)
    {
      base.OnViewReady(view);

      var itemSource = GetItemsSource();
      if(itemSource == null)
        throw new InvalidOperationException();

      Items = new ListCollectionView(itemSource);
    }

    private void InitializeEditItem(object item)
    {
      var ruleName = GetEditRuleName();
      if (string.IsNullOrEmpty(ruleName))
      {
        EditItem(item);
        return;
      }

      var permission = AuthorizationService.GetPermissionForRule(ruleName);
      if (permission == PermissionType.Full)
        EditItem(item);
    }

    protected virtual void EditItem(object item)
    {
    }

    protected virtual string GetEditRuleName()
    {
      return string.Empty;
    }
  }
}
