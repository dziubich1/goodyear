﻿using Caliburn.Micro;
using Kalisto.Communication.Core.Exceptions;
using Kalisto.Hmi.Core.Services;
using System;
using System.Linq;
using System.Threading.Tasks;
using Kalisto.Communication.Core.Messages;
using Kalisto.Localization.Messages;

namespace Kalisto.Hmi.Core
{
  public abstract class ViewModelBase : Screen, IExceptionHandler
  {
    protected IDialogService DialogService { get; }

    protected bool ViewLoaded { get; private set; }

    public abstract string DialogTitleKey { get; }

    protected virtual Type[] AssociatedCommandTypes { get; }

    protected ViewModelBase(IDialogService dialogService)
    {
      DialogService = dialogService;
      DialogService.RegisterContext(this);
    }

    protected sealed override async void OnViewLoaded(object view)
    {
      base.OnViewLoaded(view);
      OnViewLoaded();
      ViewLoaded = true;

      await RefreshDataAsync();
    }

    protected virtual void OnViewLoaded()
    {
      // designed to be overwritten
    }

    protected virtual void OnExceptionOccurred(Exception exception)
    {

    }

    public virtual async Task RefreshDataAsync()
    {
      await Task.FromResult(0);
    }

    public async Task HandleExceptionAsync(Exception exception)
    {
      if (exception is ExceptionBase exceptionBase)
        await DialogService.ShowMessageAsync(Messages.Error, exceptionBase.Message, MessageDialogType.Ok);
      else await DialogService.ShowMessageAsync(Messages.Error, Messages.ErrorOccurred, MessageDialogType.Ok);

      OnExceptionOccurred(exception);
    }

    public virtual bool IsAssociatedWithCommand(IAssociatedCommand command)
    {
      if (AssociatedCommandTypes == null || !AssociatedCommandTypes.Any())
        return false;

      return AssociatedCommandTypes.Any(c => c.IsInstanceOfType(command));
    }
  }
}
