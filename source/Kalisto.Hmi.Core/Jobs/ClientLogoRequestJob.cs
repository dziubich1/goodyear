﻿using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Hmi.Core.Communication;
using Kalisto.Hmi.Core.Services;
using Kalisto.Localization;
using System;
using System.Threading;
using System.Threading.Tasks;
using Kalisto.Communication.Core.Jobs.Base;
using Kalisto.Localization.Messages;

namespace Kalisto.Hmi.Core.Jobs
{
    public class ClientLogoRequestJob : IInitializationJob
    {
        private readonly ILocalizer<Messages> _jobLocalizer;

        private readonly IFrontendMessageBridge _messageBridge;

        private readonly IMessageBuilder _messageBuilder;

        private readonly ClientInfo _clientInfo;

        private readonly IClientLogoService _clientLogoService;

        private readonly CancellationToken _cancellationToken = new CancellationToken(false);

        public string LocalizedMessage =>
          _jobLocalizer.Localize(() => Messages.ClientLogoRequestMessage);

        public string LocalizedExceptionMessage =>
          _jobLocalizer.Localize(() => Messages.ClientLogoRequestExceptionMessage);

        public string FailureCode { get; private set; }

        public ClientLogoRequestJob(ILocalizer<Messages> jobLocalizer, IFrontendMessageBridge messageBridge, IMessageBuilder messageBuilder,
          ClientInfo clientInfo, IClientLogoService clientLogoService)
        {
            _jobLocalizer = jobLocalizer;
            _messageBridge = messageBridge;
            _messageBuilder = messageBuilder;
            _clientInfo = clientInfo;
            _clientLogoService = clientLogoService;
        }

        public async Task ExecuteAsync()
        {
            await Task.Delay(1000, _cancellationToken);
            if (!_clientLogoService.LogoExists())
            {

                var message = _messageBuilder.CreateNew<ClientLogoRequestMessage>()
                  .BasedOn(_clientInfo)
                  .Build();

                var result = await _messageBridge.SendMessageWithResultAsync<string>(message);
                if (!result.Succeeded)
                    throw new InvalidOperationException();

                if (string.IsNullOrEmpty(result.Response))
                    throw new InvalidOperationException();

                var imageToSave = _clientLogoService.ExtractLogo(result.Response);
                _clientLogoService.SaveLogo(imageToSave);
            }


            if (!_clientLogoService.LogoExists(LogoType.receipt))
            {
                var message = _messageBuilder.CreateNew<ClientLogoRequestMessage>()
                  .BasedOn(_clientInfo)
                .Build();
                message.LogoType = "receipt";

              var result = await _messageBridge.SendMessageWithResultAsync<string>(message);
                if (!result.Succeeded)
                    throw new InvalidOperationException();
                if (string.IsNullOrEmpty(result.Response))
                    throw new InvalidOperationException();

                var imageToSave = _clientLogoService.ExtractLogo(result.Response);
                _clientLogoService.SaveLogo(imageToSave, LogoType.receipt);

                // todo: log service message here later
            }
        }
    }
}
