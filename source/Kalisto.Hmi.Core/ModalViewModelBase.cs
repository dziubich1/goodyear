﻿using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Hmi.Core.Commands;
using Kalisto.Hmi.Core.Communication;
using Kalisto.Hmi.Core.Services;
using Kalisto.Hmi.Core.Validation.Caliburn.Micro.Validation;
using PropertyChanged;
using System;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Kalisto.Hmi.Core
{
  [AddINotifyPropertyChangedInterface]
  public abstract class ModalViewModelBase<TViewModel> : ValidatingScreen
    where TViewModel : class
  {
    protected readonly IDialogService DialogService;

    protected readonly IMessageBuilder MessageBuilder;

    protected readonly IFrontendMessageBridge MessageBridge;

    protected readonly ClientInfo ClientInfo;

    private bool _firstValidation = true;

    protected virtual bool IgnoreValidation => false;

    public bool ValidateOnLoad { get; set; }

    public DelegateCommand SubmitCommand { get; set; }

    public DelegateCommand CancelCommand { get; set; }

    public bool IsAsyncOperationPending { get; set; }

    public Func<TViewModel, Task<bool>> SubmitCallback { get; set; }

    protected ModalViewModelBase(IDialogService dialogService, IAsyncProgressService asyncProgressService,
      IMessageBuilder messageBuilder, IFrontendMessageBridge messageBridge, ClientInfo clientInfo)
    {
      DialogService = dialogService;
      DialogService.RegisterModalContext(this);

      MessageBuilder = messageBuilder;
      MessageBridge = messageBridge;
      ClientInfo = clientInfo;

      asyncProgressService.AsyncOperationStarted += OnAsyncOperationStarted;
      asyncProgressService.AsyncOperationFinished += OnAsyncOperationFinished;

      SubmitCommand = new DelegateCommand(OnSubmit, CanSubmit);
      CancelCommand = new DelegateCommand(OnCancel);
    }

    protected abstract void DefineValidationRules();

    protected override void OnViewReady(object view)
    {
      base.OnViewReady(view);

      DefineValidationRules();
    }

    protected virtual bool CanSubmit(object parameter)
    {
      if (_firstValidation && !IgnoreValidation)
      {
        _firstValidation = false;
        if (!ValidateOnLoad)
          return false;
      }

      return !HasError || IgnoreValidation;
    }

    protected override void OnModelValidated()
    {
      base.OnModelValidated();

      SubmitCommand.RaiseCanExecuteChanged();
    }

    protected bool IsNumber(string value)
    {
      if (int.TryParse(value, out _))
        return true;

      return false;
    }

    protected bool IsDecimal(string value)
    {
      if (decimal.TryParse(value, out _))
        return true;

      return false;
    }

    protected bool IsNumberOrNull(string value)
    {
      if (string.IsNullOrEmpty(value))
        return true;

      return IsNumber(value);
    }

    protected bool IsDecimalOrNull(string value)
    {
      if (string.IsNullOrEmpty(value))
        return true;

      return IsDecimal(value);
    }

    protected bool IsGreaterThan(string value, decimal comparedValue)
    {
      if (!IsDecimal(value))
        return false;

      if (decimal.TryParse(value, out decimal convertedValue))
        return convertedValue > comparedValue;

      return false;
    }

    protected bool IsGreaterOrEqualThan(string value, decimal comparedValue)
    {
      if (!IsDecimal(value))
        return false;

      if (decimal.TryParse(value, out decimal convertedValue))
        return convertedValue >= comparedValue;

      return false;
    }

    protected bool IsEmailValid(string email)
    {
      var regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
      if (string.IsNullOrWhiteSpace(email))
        return false;

      return regex.IsMatch(email);
    }

    private async void OnSubmit(object parameter)
    {
      if (SubmitCallback != null)
      {
        if (!await SubmitCallback.Invoke(this as TViewModel))
          return;
      }

      DialogService.PopModalContext();
      TryClose(true);
    }

    private void OnCancel(object parameter)
    {
      DialogService.PopModalContext();
      TryClose(false);
    }

    private void OnAsyncOperationFinished()
    {
      IsAsyncOperationPending = false;
    }

    private void OnAsyncOperationStarted()
    {
      IsAsyncOperationPending = true;
    }
  }
}
