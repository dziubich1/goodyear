﻿using System;
using System.ComponentModel;
using System.Resources;

namespace Kalisto.Hmi.Core.Attributes
{
  public class LocalizableCategoryAttribute : CategoryAttribute
  {
    private readonly ResourceManager _resourceManager;
    private readonly string _resourceKey;

    public LocalizableCategoryAttribute(string resourceKey, Type resourceType)
    {
      _resourceManager = new ResourceManager(resourceType);
      _resourceKey = resourceKey;
    }

    protected override string GetLocalizedString(string value)
    {
      var category = _resourceManager.GetString(_resourceKey);
      return string.IsNullOrWhiteSpace(category) ? string.Format("[[{0}]]", _resourceKey) : category;
    }
  }
}
