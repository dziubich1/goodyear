﻿using System;
using System.ComponentModel;
using System.Resources;

namespace Kalisto.Hmi.Core.Attributes
{
  public class LocalizableDisplayNameAttribute : DisplayNameAttribute
  {
    private readonly ResourceManager _resourceManager;
    private readonly string _resourceKey;

    public LocalizableDisplayNameAttribute(string resourceKey, Type resourceType)
    {
      _resourceManager = new ResourceManager(resourceType);
      _resourceKey = resourceKey;
    }

    public override string DisplayName
    {
      get
      {
        var displayName = _resourceManager.GetString(_resourceKey);
        return string.IsNullOrWhiteSpace(displayName) ? string.Format("[[{0}]]", _resourceKey) : displayName;
      }
    }
  }
}
