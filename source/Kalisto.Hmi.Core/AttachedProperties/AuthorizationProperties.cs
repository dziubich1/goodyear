﻿using System.Windows;

namespace Kalisto.Hmi.Core.AttachedProperties
{
  public static class AuthorizationProperties
  {
    public static readonly DependencyProperty AuthorizeActionNameProperty =
      DependencyProperty.RegisterAttached("AuthorizeActionName",
        typeof(string), typeof(AuthorizationProperties),
        new FrameworkPropertyMetadata(string.Empty));

    public static string GetAuthorizeActionName(DependencyObject dp)
    {
      return (string)dp.GetValue(AuthorizeActionNameProperty);
    }

    public static void SetAuthorizeActionName(DependencyObject dp, string value)
    {
      dp.SetValue(AuthorizeActionNameProperty, value);
    }
  }
}
