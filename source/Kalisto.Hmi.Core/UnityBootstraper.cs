﻿using Caliburn.Micro;
using Kalisto.Communication.Local;
using Kalisto.Core;
using Kalisto.Hmi.Core.Communication;
using Kalisto.Hmi.Core.Helpers;
using Kalisto.Hmi.Core.Services;
using System;
using System.Collections.Generic;
using System.Windows;
using Unity;
using Unity.Lifetime;

namespace Kalisto.Hmi.Core
{
  public class UnityBootstraper<TRootViewModel> : BootstrapperBase
    where TRootViewModel : IConductor
  {
    protected readonly IUnityContainer Container = new UnityContainer();

    public UnityBootstraper()
    {
      Initialize();
    }

    protected override void Configure()
    {
      base.Configure();

      Container.RegisterInstance<IWindowManager>(new WindowManager());
      Container.RegisterInstance<IEventAggregator>(new EventAggregator());
      Container.RegisterType<IFrontendMessageBridge, BasicMessageBridge>();
      Container.RegisterType<IDialogService, DialogService>(new ContainerControlledLifetimeManager());
      Container.RegisterType<IAsyncProgressService, AsyncProgressService>(new ContainerControlledLifetimeManager());
      Container.RegisterType<IClientLogoService, ClientLogoService>(new ContainerControlledLifetimeManager());
      Container.RegisterType<IAuthorizationService, AuthorizationService>(new ContainerControlledLifetimeManager());
      Container.RegisterSingleton<IWebImageService, WebImageService>();
      Container.RegisterSingleton<LocalWire>();

      FrameworkHelper.RegisterContainer(Container);
    }

    protected override void BuildUp(object instance)
    {
      instance = Container.BuildUp(instance);

      base.BuildUp(instance);
    }

    protected override IEnumerable<object> GetAllInstances(Type type)
    {
      return Container.ResolveAll(type);
    }

    protected override object GetInstance(Type type, string name)
    {
      if (!string.IsNullOrEmpty(name))
        return Container.Resolve(type, name);

      return Container.Resolve(type);
    }

    protected override void OnStartup(object sender, StartupEventArgs e)
    {
      if (DisplaySplashScreen())
      {
        DisplayRootViewFor<TRootViewModel>();
        SetupLocalDevices();
      }
      else Application.Current.Shutdown();
    }

    protected virtual bool DisplaySplashScreen()
    {
      return true;
    }

    protected virtual void SetupLocalDevices()
    {

    }
  }
}
