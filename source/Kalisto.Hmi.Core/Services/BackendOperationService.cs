﻿using System;
using System.Collections.Generic;
using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Frontend.Notifications.Base;
using Kalisto.Hmi.Core.Communication;

namespace Kalisto.Hmi.Core.Services
{
  public interface IBackendOperationService
  {
    void RegisterNotificationHandler<TNotification>(Action<object> handler) 
      where TNotification : INotification;
  }

  public abstract class BackendOperationService : IBackendOperationService
  {
    private readonly IDialogService _dialogService;

    private readonly Dictionary<string, List<Action<object>>> _notificationHandlers 
      = new Dictionary<string, List<Action<object>>>();

    protected ClientInfo ClientInfo;

    protected IMessageBuilder MessageBuilder;

    protected IFrontendMessageBridge MessageBridge;

    protected BackendOperationService(ClientInfo clientInfo, IMessageBuilder messageBuilder, IFrontendMessageBridge messageBridge, IDialogService dialogService)
    {
      _dialogService = dialogService;
      _dialogService.ContextChanged += OnDialogContextChanged;

      ClientInfo = clientInfo;
      MessageBuilder = messageBuilder;
      MessageBridge = messageBridge;
      MessageBridge.NotificationReceived += OnNotificationReceived;
      MessageBridge.RegisterExceptionHandler(_dialogService.ExceptionHandler);
    }

    private void OnDialogContextChanged()
    {
      MessageBridge.RegisterExceptionHandler(_dialogService.ExceptionHandler);
    }

    public void RegisterNotificationHandler<TNotification>(Action<object> handler)
      where TNotification : INotification
    {
      var name = typeof(TNotification).Name;
      if (_notificationHandlers.ContainsKey(name))
      {
        _notificationHandlers[name].Add(handler);
        return;
      }

      _notificationHandlers.Add(name, new List<Action<object>>
      {
        handler
      });
    }

    private void OnNotificationReceived(INotification notification)
    {
      if (!_notificationHandlers.ContainsKey(notification.NotificationId))
        return;

      foreach(var handler in _notificationHandlers[notification.NotificationId])
        handler?.Invoke(notification);
    }
  }
}
