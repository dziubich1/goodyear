﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kalisto.Hmi.Core.Services
{
  public class FilesService : IFilesService
  {
    public string ReadTextFromFile(string filePath)
    {
      if (!File.Exists(filePath))
        return string.Empty;

      string text;
      using (var fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read))
      {
        using (var streamReader = new StreamReader(fileStream, Encoding.UTF8))
        {
          text = streamReader.ReadToEnd();
        }
      }

      return text;
    }
  }
}
