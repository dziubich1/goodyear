﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Threading.Tasks;
using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Core;
using Kalisto.Hmi.Core.Communication;
using Kalisto.Localization;
using Kalisto.Localization.Common;

namespace Kalisto.Hmi.Core.Services
{
  public interface IServerImageService
  {
    Task<Image> GetImageAsync(string relativePath);
    Task<Image> GetImageThumbnailAsync(string relativePath);
  }

  public class ServerImageService : IServerImageService
  {
    private readonly IFrontendMessageBridge _messageBridge;

    private readonly IMessageBuilder _messageBuilder;

    private readonly ClientInfo _clientInfo;

    private readonly LocalizationManager<Common> _localizer;

    public ServerImageService(IFrontendMessageBridge messageBridge, IMessageBuilder messageBuilder, ClientInfo clientInfo)
    {
      _messageBridge = messageBridge;
      _messageBuilder = messageBuilder;
      _clientInfo = clientInfo;
      _localizer = new LocalizationManager<Common>();
    }

    public async Task<Image> GetImageAsync(string relativePath)
    {
      var message = _messageBuilder.CreateNew<ServerImageRequest>()
        .BasedOn(_clientInfo)
        .WithProperty(c => c.RelativePath, relativePath)
        .Build();

      var result = await _messageBridge.SendMessageWithResultAsync<string>(message);
      if (result == null || !result.Succeeded || string.IsNullOrEmpty(result.Response))
      {
        return GetDefaultImage();
      }

      return result.Response.ToImage();
    }

    public async Task<Image> GetImageThumbnailAsync(string relativePath)
    {
      var message = _messageBuilder.CreateNew<ServerImageThumbnailRequest>()
        .BasedOn(_clientInfo)
        .WithProperty(c => c.RelativePath, relativePath)
        .Build();

      var result = await _messageBridge.SendMessageWithResultAsync<string>(message);
      if (result == null || !result.Succeeded || string.IsNullOrEmpty(result.Response))
      {
        return GetDefaultImage();
      }

      return result.Response.ToImage();
    }

    private Image GetDefaultImage()
    {
      var resultImage = new Bitmap(800, 600, PixelFormat.Format24bppRgb);
      using (Graphics grp = Graphics.FromImage(resultImage))
      {
        var rect = new RectangleF(0,0,resultImage.Width, resultImage.Height);
        var font = new Font(FontFamily.GenericSerif, 50);
        var format = new StringFormat
        {
          LineAlignment = StringAlignment.Center,
          Alignment = StringAlignment.Center
        };
        grp.FillRectangle(Brushes.Gray, rect);
        grp.DrawString(_localizer.Localize("NoPhotoImageLabel"), font, Brushes.White, rect, format);   
      }
      return resultImage;
    }
  }
}
