﻿using System;
using System.Threading.Tasks;
using Caliburn.Micro;
using MahApps.Metro.Controls.Dialogs;
using Action = System.Action;

namespace Kalisto.Hmi.Core.Services
{
  public interface IDialogService
  {
    void RegisterContext(IScreen context);
    void RegisterModalContext(IScreen context);
    void PopModalContext();

    Task<MessageDialogResult> ShowMessageAsync(string title, string message, MessageDialogType type);

    Task<LoginDialogData> ShowLoginAsync();

    Task<DialogResult<TViewModel>> ShowModalDialogAsync<TViewModel>(Action<TViewModel> setupAction,
      Func<TViewModel, Task<bool>> submitCallback = null)
      where TViewModel : ModalViewModelBase<TViewModel>;

    void ShowDialog<TViewModel>(Action<TViewModel> configureAction) where TViewModel : Screen;

    IExceptionHandler ExceptionHandler { get; }

    event Action ContextChanged;

    bool IsModalOpened { get; }
  }

  public class DialogResult<TViewModel>
  {
    public bool? Result { get; set; }
    public TViewModel ViewModel { get; set; }
  }
}
