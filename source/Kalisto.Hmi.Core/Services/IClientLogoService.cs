﻿using System.Drawing;

namespace Kalisto.Hmi.Core.Services
{
    public enum LogoType
    {
        application, receipt
    }
    public interface IClientLogoService
    {
        void SaveLogo(Image image, LogoType logoType = LogoType.application);

        Image LoadLogo(LogoType logoType = LogoType.application);

        Image ExtractLogo(string base64String);

        bool LogoExists(LogoType logoType = LogoType.application);
    }
}
