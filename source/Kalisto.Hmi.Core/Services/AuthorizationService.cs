﻿using System;
using System.Linq;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Configuration.Permissions;

namespace Kalisto.Hmi.Core.Services
{
  public interface IAuthorizationService
  {
    event Action Authorized;

    void CreateUserSession(UserInfo userInfo);

    void ClearUserSession();

    PermissionType GetPermissionForRule(string ruleName);

    void ForceReauthorizing();
  }

  public class AuthorizationService : IAuthorizationService
  {
    private readonly UserInfo _userInfo;

    public event Action Authorized;

    public AuthorizationService(UserInfo userInfo)
    {
      _userInfo = userInfo;
    }

    public void CreateUserSession(UserInfo userInfo)
    {
      if(userInfo == null)
        throw new ArgumentNullException(nameof(userInfo));

      _userInfo.Update(userInfo.UserRole,
        userInfo.Username, userInfo.Permissions);

      Authorized?.Invoke();
    }

    public void ClearUserSession()
    {
      _userInfo.Clear();
      Authorized?.Invoke();
    }

    public PermissionType GetPermissionForRule(string ruleName)
    {
      if (_userInfo.Permissions == null)
        return PermissionType.None;

      return _userInfo.Permissions.FirstOrDefault(c => c.Definition.Name == ruleName)?
               .Type ?? PermissionType.None;
    }

    public void ForceReauthorizing()
    {
      Authorized?.Invoke();
    }
  }
}
