﻿using Kalisto.Configuration;
using Kalisto.Logging;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Reflection;
using Kalisto.Core;

namespace Kalisto.Hmi.Core.Services
{
    public class ClientLogoService : IClientLogoService
    {
        private const string LogoFileName = "logo";
        private const string ReceiptFileName = "receipt";

        private readonly ILogger _logger;

        private readonly ConfigurationManager _configurationManager;

        public ClientLogoService(ILogger logger, ConfigurationManager configurationManager)
        {
            _logger = logger;
            _configurationManager = configurationManager;
        }

        public Image LoadLogo(LogoType logoType)
        {
            if (!Directory.Exists(_configurationManager.ConfigDirectory))
                throw new Exception("Configuration directory does not exist.");

            try
            {
                var name = logoType == LogoType.application ? LogoFileName : ReceiptFileName;
                var logoFilePath = Directory.GetFiles(_configurationManager.ConfigDirectory)
                  .FirstOrDefault(c => c.Contains(name));
                if (string.IsNullOrEmpty(logoFilePath))
                    return null;

                return Image.FromFile(logoFilePath);
            }
            catch (Exception e)
            {
                _logger.Error($"Could not save logo image. Reason: {e.Message}");
                return null;
            }
        }

        public void SaveLogo(Image image, LogoType logoType)
        {
            if (!Directory.Exists(_configurationManager.ConfigDirectory))
                throw new Exception("Configuration directory does not exist.");

            try
            {
                var name = logoType == LogoType.application ? LogoFileName : ReceiptFileName;
                var ext = GetImageExtension(image.RawFormat).ToLower();
                var logoFilePath = Path.Combine(_configurationManager.ConfigDirectory, $"{name}.png");
                if (ext == "jpeg")
                {
                    image = ConvertJpegToPng(image);
                }
                image.Save(logoFilePath, ImageFormat.Png);
            }
            catch (Exception e)
            {
                _logger.Error($"Could not save logo image. Reason: {e.Message}");
            }
        }
        private Image ConvertJpegToPng(Image bmpImageToConvert)
        {
            Image bmpNewImage = new Bitmap(bmpImageToConvert.Width, bmpImageToConvert.Height);
            Graphics gfxNewImage = Graphics.FromImage(bmpNewImage);
            gfxNewImage.DrawImage(bmpImageToConvert, new Rectangle(0, 0, bmpNewImage.Width, bmpNewImage.Height), 0, 0,
                bmpImageToConvert.Width, bmpImageToConvert.Height, GraphicsUnit.Pixel);
            gfxNewImage.Dispose();
            bmpImageToConvert.Dispose();
            return bmpNewImage;
        }

        public Image ExtractLogo(string base64String)
        {
            return base64String.ToImage();
        }

        public bool LogoExists(LogoType logoType)
        {
            var name = logoType == LogoType.application ? LogoFileName : ReceiptFileName;
            var logoFilePath = Path.Combine(_configurationManager.ConfigDirectory, name);
            return File.Exists(logoFilePath);
        }

        private string GetImageExtension(ImageFormat format)
        {
            return GetImageFormats().TryGetValue(format.Guid, out var name) ? name.ToLower() : null;
        }

        private Dictionary<Guid, string> GetImageFormats()
        {
            return (from p in typeof(ImageFormat).GetProperties(BindingFlags.Static | BindingFlags.Public)
                    where p.PropertyType == typeof(ImageFormat)
                    let value = (ImageFormat)p.GetValue(null, null)
                    select new { Guid = value.Guid, Name = value.ToString() })
              .ToDictionary(p => p.Guid, p => p.Name);
        }
    }
}
