﻿using Caliburn.Micro;
using MahApps.Metro.Controls.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using Kalisto.Localization;
using Kalisto.Localization.Common;
using Unity;
using Action = System.Action;

namespace Kalisto.Hmi.Core.Services
{
  public class DialogService : IDialogService
  {
    private readonly ILocalizer<Common> _localizer;
    private readonly IUnityContainer _container;
    private readonly IWindowManager _windowManager;

    private IScreen _context;

    private readonly Stack<IScreen> _modalContextStack = new Stack<IScreen>();

    public IExceptionHandler ExceptionHandler => _context as IExceptionHandler;

    public event Action ContextChanged;

    public bool IsModalOpened { get; private set; }

    public DialogService(IUnityContainer container, IWindowManager windowManager)
    {
      _localizer = new LocalizationManager<Common>();
      _container = container;
      _windowManager = windowManager;
    }

    public void RegisterContext(IScreen context)
    {
      _context = context;
      _modalContextStack.Clear();
      ContextChanged?.Invoke();
    }

    public void RegisterModalContext(IScreen context)
    {
      _modalContextStack.Push(context);
    }

    public void PopModalContext()
    {
      _modalContextStack.Pop();
    }

    public Task<MessageDialogResult> ShowMessageAsync(string title, string message, MessageDialogType type)
    {
      MessageDialogStyle dialogStyle = MessageDialogStyle.Affirmative;
      MetroDialogSettings dialogSettings = new MetroDialogSettings
      {
        AnimateShow = true,
        AnimateHide = false,
      };

      if (type == MessageDialogType.Ok)
      {
        dialogStyle = MessageDialogStyle.Affirmative;
        dialogSettings.AffirmativeButtonText = _localizer.Localize(() => Common.Ok);
      }
      else if (type == MessageDialogType.YesNo)
      {
        dialogStyle = MessageDialogStyle.AffirmativeAndNegative;
        dialogSettings.AffirmativeButtonText = _localizer.Localize(() => Common.Yes);
        dialogSettings.NegativeButtonText = _localizer.Localize(() => Common.No);
      }

      return DialogCoordinator.Instance.ShowMessageAsync(GetContext(), title, message, dialogStyle, dialogSettings);
    }

    public Task<LoginDialogData> ShowLoginAsync()
    {
      return DialogCoordinator.Instance.ShowLoginAsync(GetContext(), _localizer.Localize(() => Common.LoginHeader), string.Empty,
        new LoginDialogSettings
        {
          AnimateShow = false,
          AnimateHide = false,
          AffirmativeButtonText = _localizer.Localize(() => Common.Login),
          UsernameWatermark = _localizer.Localize(() => Common.Username),
          PasswordWatermark = _localizer.Localize(() => Common.Password),
          NegativeButtonText = _localizer.Localize(() => Common.Cancel),
          RememberCheckBoxText = _localizer.Localize(() => Common.RememberMe),
          RememberCheckBoxVisibility = Visibility.Hidden, // todo: should be visible later
          NegativeButtonVisibility = Visibility.Visible
        });
    }

    public Task<DialogResult<TViewModel>> ShowModalDialogAsync<TViewModel>(Action<TViewModel> configureAction,
      Func<TViewModel, Task<bool>> submitCallback = null) where TViewModel : ModalViewModelBase<TViewModel>
    {
      var viewModel = _container.Resolve<TViewModel>();
      if (configureAction != null)
        configureAction(viewModel);

      viewModel.SubmitCallback = submitCallback;
      IsModalOpened = true;
      var result = _windowManager.ShowDialog(viewModel);
      IsModalOpened = false;

      return Task.FromResult(new DialogResult<TViewModel>
      {
        ViewModel = viewModel,
        Result = result
      });
    }

    public void ShowDialog<TViewModel>(Action<TViewModel> configureAction) where TViewModel : Screen
    {
      var viewModel = _container.Resolve<TViewModel>();
      if (configureAction != null)
        configureAction(viewModel);

      _windowManager.ShowWindow(viewModel);
    }

    private IScreen GetContext()
    {
      if (_modalContextStack.Any())
        return _modalContextStack.Peek();

      return _context;
    }
  }
}
