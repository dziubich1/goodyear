﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kalisto.Hmi.Core.Services
{
  public interface IFilesService
  {
    string ReadTextFromFile(string filePath);
  }
}
