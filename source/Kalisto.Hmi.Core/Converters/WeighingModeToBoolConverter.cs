﻿using Kalisto.Hmi.Core.Controls;
using System;
using System.Globalization;
using System.Windows.Data;

namespace Kalisto.Hmi.Core.Converters
{
  public class WeighingModeToBoolConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if (value == null)
        return true;

      var mode = (WeighingMode)value;
      if (mode == WeighingMode.Manual)
        return true;

      return false;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      return null;
    }
  }
}
