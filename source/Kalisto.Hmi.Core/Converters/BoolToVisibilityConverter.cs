﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Kalisto.Hmi.Core.Converters
{
  public class BoolToVisibilityConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if (value == null)
        return Visibility.Hidden;

      var isVisible = (bool) value;
      if (isVisible)
        return Visibility.Visible;

      return Visibility.Hidden;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      return null;
    }
  }
}
