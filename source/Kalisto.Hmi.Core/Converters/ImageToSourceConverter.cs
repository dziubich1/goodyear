﻿using System;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Windows.Data;
using System.Windows.Media.Imaging;

namespace Kalisto.Hmi.Core.Converters
{
  public class ImageToSourceConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if (!(value is Image image))
        return null;

      var ms = new MemoryStream();
      image.Save(ms, image.RawFormat);
      ms.Seek(0, SeekOrigin.Begin);
      var bitmapImage = new BitmapImage();
      bitmapImage.BeginInit();
      bitmapImage.StreamSource = ms;
      bitmapImage.EndInit();

      return bitmapImage;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      return null;
    }
  }
}
