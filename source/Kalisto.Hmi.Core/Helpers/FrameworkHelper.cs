﻿using Unity;

namespace Kalisto.Hmi.Core.Helpers
{
  /// <summary>
  /// Helper class created for possibility of getting registered instances.
  /// There is no way to force Framework components to use your IoC container (e.g. Behaviors, AttachedProperties etc.)
  /// so we need to provide container in static way - not the most beauty solution but works
  /// </summary>
  public static class FrameworkHelper
  {
    private static IUnityContainer _container;

    public static void RegisterContainer(IUnityContainer container)
    {
      _container = container;
    }

    public static T Resolve<T>()
    {
      if (_container == null)
        return default(T);

      return _container.Resolve<T>();
    }
  }
}
