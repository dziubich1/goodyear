﻿using Caliburn.Micro;
using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Frontend.Notifications;
using Kalisto.Hmi.Core.Communication;
using Kalisto.Hmi.Core.Services;
using Kalisto.Localization;
using Kalisto.Localization.DialogTitles;
using PropertyChanged;
using System;
using Unity;

namespace Kalisto.Hmi.Core
{
  [AddINotifyPropertyChangedInterface]
  public abstract class ShellViewModelBase : Conductor<ViewModelBase>
  {
    private const string AppName = "Waga Pro 2.0";

    private readonly ILocalizer<DialogTitles> _dialogTitleLocalizer;

    private readonly IUnityContainer _container;

    protected IFrontendMessageBridge MessageBridge { get; }

    protected IMessageBuilder MessageBuilder { get; }

    protected IDialogService DialogService { get; set; }

    protected ClientInfo ClientInfo { get; }

    public bool IsAsyncOperationPending { get; set; }

    protected ShellViewModelBase(IUnityContainer container, IDialogService dialogService, IFrontendMessageBridge messageBridge,
      IMessageBuilder messageBuilder, IAsyncProgressService asyncProgressService, ClientInfo clientInfo)
    {
      _container = container;
      ClientInfo = clientInfo;
      DialogService = dialogService;
      DialogService.RegisterContext(this);
      MessageBuilder = messageBuilder;
      MessageBridge = messageBridge;
      MessageBridge.LoginRequiredReceived += OnLoginRequiredReceived;

      asyncProgressService.AsyncOperationStarted += OnAsyncOperationStarted;
      asyncProgressService.AsyncOperationFinished += OnAsyncOperationFinished;
      _dialogTitleLocalizer = new LocalizationManager<DialogTitles>();
    }

    protected override void OnInitialize()
    {
      base.OnInitialize();

      UpdateTitleBar();
    }

    protected override void ChangeActiveItem(ViewModelBase newItem, bool closePrevious)
    {
      base.ChangeActiveItem(newItem, closePrevious);

      UpdateTitleBar(newItem?.DialogTitleKey);
    }

    protected virtual void OnLoginRequiredReceived()
    {

    }

    protected void ActivateItem<TViewModel>() where TViewModel : ViewModelBase
    {
      if (ActiveItem is TViewModel)
        return;

      if (IsAsyncOperationPending)
      {
        DialogService.ShowMessageAsync("", "Nie można przełączyć okna w trakcie pobierania danych.",
          MessageDialogType.Ok);
        return;
      }

      ActivateItem(_container.Resolve<TViewModel>());
    }

    protected void ActivateItem(Type viewModelType)
    {
      if (ActiveItem?.GetType() == viewModelType)
        return;

      if (IsAsyncOperationPending)
      {
        DialogService.ShowMessageAsync("", "Nie można przełączyć okna w trakcie pobierania danych.",
          MessageDialogType.Ok);
        return;
      }

      var viewModel = _container.Resolve(viewModelType) as ViewModelBase;
      if (viewModel == null)
        return;

      ActivateItem(viewModel);
    }

    protected async void HandleDataUpdateNotification(DbUpdatedNotification notification)
    {
      if (!(ActiveItem is ViewModelBase viewModel))
        return;

      var shouldRefreshActiveDialog = viewModel.IsAssociatedWithCommand(notification.AssociatedCommandMetadata);
      if (shouldRefreshActiveDialog)
      {
        await Execute.OnUIThreadAsync(async () =>
        {
          await viewModel.RefreshDataAsync();
        });
      }
    }

    private void OnAsyncOperationFinished()
    {
      IsAsyncOperationPending = false;
    }

    private void OnAsyncOperationStarted()
    {
      IsAsyncOperationPending = true;
    }

    private void UpdateTitleBar(string dialogNameKey = "")
    {
      DisplayName = !string.IsNullOrEmpty(dialogNameKey)
        ? $"{AppName} - {_dialogTitleLocalizer.Localize(dialogNameKey)}" : AppName;
    }
  }
}
