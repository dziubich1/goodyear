﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Resources;
using Kalisto.Localization;
using Kalisto.Localization.Validation;

namespace Kalisto.Hmi.Core.Validation
{
  public class ScreenValidator
  {
    private readonly Dictionary<string, List<ValidationRule>> _validationRules =
      new Dictionary<string, List<ValidationRule>>();
    readonly Dictionary<string, string> _errors = new Dictionary<string, string>();


    public ValidationRule AddValidationRule<TProperty>(Expression<Func<TProperty>> expression)
    {
      var propertyName = expression.GetPropertyFullName();
      if (!_validationRules.ContainsKey(propertyName))
        _validationRules.Add(propertyName, new List<ValidationRule>());

      var rule = new ValidationRule();
      _validationRules[propertyName].Add(rule);

      return rule;
    }

    public void RemoveValidationRules<TProperty>(Expression<Func<TProperty>> expression)
    {
      var propertyName = expression.GetPropertyFullName();
      if (_validationRules.ContainsKey(propertyName))
        _validationRules.Remove(propertyName);
    }

    public virtual string Error
    {
      get
      {
        Validate();
        return Strings.Agregate(Environment.NewLine, _errors.Select(x => x.Value).Distinct().ToArray());
      }
    }

    public bool HasError => !string.IsNullOrWhiteSpace(Error);

    public virtual string this[string columnName] => Validate(columnName);

    public virtual string Validate()
    {
      _errors.Clear();
      return Validate(GetType().GetProperties().Select(x => x.Name).Union(_validationRules.Keys));
    }

    string Validate(string propertyName)
    {
      return Validate(new List<string> { propertyName });
    }

    string Validate(IEnumerable<string> propertyNames)
    {
      var results = new List<string>();
      foreach (var propertyName in propertyNames)
      {
        if (!_validationRules.ContainsKey(propertyName))
          continue;

        if (_errors.ContainsKey(propertyName))
          _errors.Remove(propertyName);

        foreach (var validationRule in _validationRules[propertyName])
        {
          var result = validationRule.Validate();
          if (string.IsNullOrEmpty(result))
            continue;

          results.Add(result);
          if (_errors.ContainsKey(propertyName))
            _errors[propertyName] = result;
          else
            _errors.Add(propertyName, result);
        }
      }

      return Strings.Agregate(Environment.NewLine, results.Distinct().ToArray());
    }
  }

  public class ValidationRule
  {
    private readonly ILocalizer<ValidationMessages> _localizer;

    private bool _firstValidation = true;
    private bool _ignoreOnLoad;
    private Expression<Func<bool>> _condition;
    private string _messageKey;
    private object[] _messageParams = new string[0];

    public ValidationRule()
    {
      _localizer = new LocalizationManager<ValidationMessages>();
    }

    public ValidationRule When(Expression<Func<bool>> condition)
    {
      _condition = condition;
      return this;
    }

    public ValidationRule WithMessageKey(string messageKey)
    {
      _messageKey = messageKey;
      return this;
    }

    public ValidationRule WithMessageKey(string messageKey, params object[] messageParams)
    {
      _messageKey = messageKey;
      _messageParams = messageParams;
      return this;
    }

    public ValidationRule ButIgnoreOnLoad()
    {
      _ignoreOnLoad = true;
      return this;
    }

    public string Validate()
    {
      if (_firstValidation && _ignoreOnLoad)
      {
        _firstValidation = false;
        return string.Empty;
      }

      if (_condition == null)
        return string.Empty;

      try
      {
        return _condition.Compile()() ? string.Format(_localizer.Localize(_messageKey), _messageParams) : string.Empty;
      }
      catch
      {
        return string.Empty;
      }
    }
  }

  public interface ISupportValidation : IDataErrorInfo
  {
    ValidationRule InvalidateProperty<TProperty>(Expression<Func<TProperty>> expression);

    void RemovePropertyValidations<TProperty>(Expression<Func<TProperty>> expression);

    string Validate();
  }
}
