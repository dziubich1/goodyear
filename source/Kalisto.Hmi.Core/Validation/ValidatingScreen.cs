﻿using Caliburn.Micro;
using Kalisto.Localization;
using Kalisto.Localization.ModalDialogTitles;
using System;
using System.Linq.Expressions;

namespace Kalisto.Hmi.Core.Validation
{
  namespace Caliburn.Micro.Validation
  {
    public abstract class ValidatingScreen : Screen, ISupportValidation
    {
      private ScreenValidator _validator;

      private ILocalizer<ModalDialogTitles> _dialogTitleLocalizer;

      public abstract string DialogNameKey { get; set; }

      protected override void OnViewReady(object view)
      {
        base.OnViewReady(view);

        _dialogTitleLocalizer = new LocalizationManager<ModalDialogTitles>();
        _validator = new ScreenValidator();
        DisplayName = _dialogTitleLocalizer.Localize(DialogNameKey);
      }

      public void NotifyErrorChanged()
      {
        OnModelValidated();
        Deferred.Execute(() =>
        {
          NotifyOfPropertyChange(() => Error);
          NotifyOfPropertyChange(() => HasError);
        }, 100);
      }

      public string Validate()
      {
        NotifyErrorChanged();

        return _validator.Validate();
      }

      public string Error => _validator.Error;

      public bool HasError => _validator.HasError;

      public string this[string columnName]
      {
        get
        {
          NotifyErrorChanged();
          return _validator[columnName];
        }
      }

      public ValidationRule InvalidateProperty<TProperty>(Expression<Func<TProperty>> expression)
      {
        return _validator.AddValidationRule(expression);
      }

      public void RemovePropertyValidations<TProperty>(Expression<Func<TProperty>> expression)
      {
        _validator.RemoveValidationRules(expression);
      }

      protected virtual void OnModelValidated()
      {

      }
    }
  }
}
