﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Kalisto.Hmi.Core.Validation
{
  public class DataErrorInfoImpl : IDataErrorInfo
  {
    string IDataErrorInfo.Error => string.Empty;

    [Browsable(false)]
    public bool HasErrors { get; private set; }

    public string this[string columnName]
    {
      get
      {
        var pi = GetType().GetProperty(columnName);
        if (pi == null)
          return null;

        var value = pi.GetValue(this, null);
        var context = new ValidationContext(this, null, null) { MemberName = columnName };
        var validationResults = new List<ValidationResult>();
        if (Validator.TryValidateProperty(value, context, validationResults))
        {
          HasErrors = false;
          return null;
        }

        var sb = new StringBuilder();
        foreach (var vr in validationResults)
          sb.AppendLine(vr.ErrorMessage);

        HasErrors = true;
        return sb.ToString().Trim();
      }
    }
  }
}
