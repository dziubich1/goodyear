﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;

namespace Kalisto.Hmi.Core.Validation
{
  internal class Strings
  {
    internal static string Agregate(string delimiter, params object[] objects)
    {
      if (objects == null)
        return string.Empty;

      objects = objects.Where(x => x != null).ToArray();
      objects = objects.Where(x => !(x is string) || !string.IsNullOrWhiteSpace(x.ToString())).ToArray();
      if (objects.Length == 0)
        return string.Empty;

      return objects.Select(x => x.ToString()).Aggregate((c, v) => string.Format("{0}{1}{2}", c, delimiter, v));
    }
  }

  public static class ExpressionExtensions
  {
    internal static string GetPropertyFullName(this Expression propertyExpression)
    {
      if (propertyExpression is MemberExpression expression)
        return GetPropertyName(expression);

      if (propertyExpression is UnaryExpression unaryExpression)
        return GetPropertyName(unaryExpression.Operand as MemberExpression);

      if (propertyExpression is LambdaExpression lambdaExpression)
        return GetPropertyFullName(lambdaExpression.Body);

      throw new ApplicationException(string.Format("Expression: {0} is not MemberExpression", propertyExpression));
    }

    static string GetPropertyName(MemberExpression me)
    {
      string propertyName = me.Member.Name;
      if (me.Expression.NodeType != ExpressionType.Parameter
          && me.Expression.NodeType != ExpressionType.TypeAs
          && me.Expression.NodeType != ExpressionType.Constant)
      {
        propertyName = GetPropertyName(me.Expression as MemberExpression) + "." + propertyName;
      }

      return propertyName;
    }
  }

  public class DeferredExecutor
  {
    private readonly object _locker = new object();
    private readonly Action _executionAction;

    private bool _started;
    private bool _needRestart;
    DateTime?  _timeToExecute;

    public string Name { get; set; }
    public int ExecutionTimeoutMilliseconds { get; set; }
    public int ExecuteCallsCount { get; private set; }

    public DeferredExecutor(Action executionAction, int executionTimeoutMilliseconds = 500)
    {
      Name = "(unnamed)";

      _executionAction = executionAction ?? throw new ArgumentNullException(nameof(executionAction));
      ExecutionTimeoutMilliseconds = executionTimeoutMilliseconds;
    }

    public void Execute(int? executionTimeoutMilliseconds = null)
    {
      lock (_locker)
      {
        ExecuteCallsCount++;

        if (_timeToExecute == null)
          _timeToExecute = DateTime.Now;

        if (executionTimeoutMilliseconds == null)
          executionTimeoutMilliseconds = ExecutionTimeoutMilliseconds;

        _timeToExecute = DateTime.Now.AddMilliseconds(executionTimeoutMilliseconds.Value);
        _needRestart = true;

        if (_started)
          return;

        _started = true;
        ThreadPool.QueueUserWorkItem(o =>
        {
          try
          {
            while (DateTime.Now <= _timeToExecute.Value)
            {
              Thread.Sleep(1);
            }

            if (_executionAction == null)
              throw new ApplicationException(string.Format("_executionAction == null in deferred executor {0}", Name));

            _needRestart = false;

            try
            {
              _executionAction();
            }
            catch
            {
              // ignore
            }
            finally
            {
              _started = false;
              _timeToExecute = null;
              ExecuteCallsCount = 0;
            }

            if (_needRestart)
              Execute();
          }
          catch
          {
            // ignore
          }
        });
      }
    }
  }

  public class Deferred
  {
    private Deferred()
    {
    }

    private static readonly Dictionary<Action, DeferredExecutor> ActionsToExecute 
      = new Dictionary<Action, DeferredExecutor>();

    public static void Execute(Action action, int executionTimeoutMilliseconds = 500)
    {
      if (action == null)
        throw new ArgumentNullException(nameof(action));

      DeferredExecutor executor ;
      lock (ActionsToExecute)
      {
        if (!ActionsToExecute.ContainsKey(action))
          ActionsToExecute.Add(action, new DeferredExecutor(action, executionTimeoutMilliseconds));

        executor = ActionsToExecute[action];
      }

      executor.ExecutionTimeoutMilliseconds = executionTimeoutMilliseconds;
      executor.Execute();
    }
  }
}
