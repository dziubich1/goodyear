﻿using System.Collections.Specialized;
using System.Windows;
using System.Windows.Controls;

namespace Kalisto.Hmi.Core.Controls
{
  /// <summary>
  /// Interaction logic for ExtendedListView.xaml
  /// </summary>
  public partial class ExtendedListView : ListView
  {
    private bool _internalActions;

    private bool _ignoreChangeEvent;

    public ExtendedListView()
    {
      InitializeComponent();
    }

    public static readonly DependencyProperty PickedItemsProperty =
      DependencyProperty.Register("PickedItems", typeof(IObservableCollection),
        typeof(ExtendedListView), new FrameworkPropertyMetadata(OnPickedItemsChangedCallback)
        {
          DefaultValue = null,
          BindsTwoWayByDefault = true
        });

    public IObservableCollection PickedItems
    {
      get => (IObservableCollection)GetValue(PickedItemsProperty);
      set => SetValue(PickedItemsProperty, value);
    }

    private static void OnPickedItemsChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      var ctrl = d as ExtendedListView;
      if (ctrl == null)
        return;

      ctrl.ListView.SelectedItems.Clear();
      if (e.OldValue is IObservableCollection oldCollection)
        oldCollection.CollectionChanged -= ctrl.OnPickedItemsCollectionChanged;

      if (e.NewValue is IObservableCollection newCollection)
      {
        ctrl._internalActions = true;
        newCollection.CollectionChanged += ctrl.OnPickedItemsCollectionChanged;
        
        foreach (var item in newCollection)
          ctrl.ListView.SelectedItems.Add(item);

        ctrl._internalActions = false;
      }
    }

    public void OnPickedItemsCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
    {
      if (_ignoreChangeEvent)
        return;

      _internalActions = true;
      if (e.OldItems != null)
        foreach (var item in e.OldItems)
          ListView.SelectedItems.Remove(item);

      if (e.NewItems != null)
        foreach (var item in e.NewItems)
          ListView.SelectedItems.Add(item);

      _internalActions = false;
    }

    private void OnSelectionChanged(object sender, SelectionChangedEventArgs e)
    {
      if (_internalActions)
        return;

      _ignoreChangeEvent = true;
      if (e.AddedItems != null)
        foreach (var item in e.AddedItems)
          PickedItems?.Add(item);

      if (e.RemovedItems != null)
        foreach (var item in e.RemovedItems)
          PickedItems?.Remove(item);

      _ignoreChangeEvent = false;
    }
  }
}
