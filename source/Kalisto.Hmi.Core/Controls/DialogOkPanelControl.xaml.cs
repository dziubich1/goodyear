﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Kalisto.Hmi.Core.Controls
{
  /// <summary>
  /// Interaction logic for DialogOkPanelControl.xaml
  /// </summary>
  public partial class DialogOkPanelControl : UserControl
  {
    public DialogOkPanelControl()
    {
      InitializeComponent();
    }

    public static readonly DependencyProperty OkProperty =
      DependencyProperty.Register("Ok", typeof(ICommand),
        typeof(DialogOkPanelControl), new PropertyMetadata(null));

    public ICommand Ok
    {
      get => (ICommand)GetValue(OkProperty);
      set => SetValue(OkProperty, value);
    }
  }
}
