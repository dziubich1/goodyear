﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace Kalisto.Hmi.Core.Controls
{
  /// <summary>
  /// Interaction logic for SoundButton.xaml
  /// </summary>
  public partial class SoundButton : Button
  {
    private bool _soundOn;

    public SoundButton()
    {
      InitializeComponent();
    }

    public static readonly DependencyProperty SoundEnabledProperty =
      DependencyProperty.Register("SoundEnabled", typeof(bool),
        typeof(SoundButton), new FrameworkPropertyMetadata(true, PropertyChangedCallback)
        {
          BindsTwoWayByDefault = true,
          DefaultUpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged
        });

    private static void PropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      var button = d as SoundButton;
      if (button == null)
        return;

      button.UpdateSoundState((bool)e.NewValue);
    }

    public bool SoundEnabled
    {
      get => (bool)GetValue(SoundEnabledProperty);
      set => SetValue(SoundEnabledProperty, value);
    }

    public void UpdateSoundState(bool enabled)
    {
      _soundOn = enabled;
      UpdateButtonVisibility();
    }

    private void OnButtonClick(object sender, RoutedEventArgs e)
    {
      _soundOn = !_soundOn;
      SoundEnabled = _soundOn;
    }

    private void UpdateButtonVisibility()
    {
      if (_soundOn)
      {
        SoundOnImage.Visibility = Visibility.Visible;
        SoundOffImage.Visibility = Visibility.Hidden;
      }
      else
      {
        SoundOnImage.Visibility = Visibility.Hidden;
        SoundOffImage.Visibility = Visibility.Visible;
      }
    }
  }
}
