﻿using System;
using System.Collections.Specialized;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Kalisto.Hmi.Core.Controls
{
  /// <summary>
  /// Interaction logic for FilteringComboBox.xaml
  /// </summary>
  public partial class FilteringComboBox : ComboBox
  {
    private bool _autoSelectionChanged;

    public FilteringComboBox()
    {
      InitializeComponent();
      InitializeItemsViewFilter();
    }

    public static readonly DependencyProperty AllowNonExistingItemsProperty =
      DependencyProperty.Register("AllowNonExistingItems", typeof(bool),
        typeof(FilteringComboBox), new PropertyMetadata(false));

    public bool AllowNonExistingItems
    {
      get => (bool)GetValue(AllowNonExistingItemsProperty);
      set => SetValue(AllowNonExistingItemsProperty, value);
    }

    public static readonly DependencyProperty ItemNotFoundCommandProperty =
      DependencyProperty.Register("ItemNotFoundCommand", typeof(ICommand),
        typeof(FilteringComboBox), new PropertyMetadata(null));

    public ICommand ItemNotFoundCommand
    {
      get => (ICommand)GetValue(ItemNotFoundCommandProperty);
      set => SetValue(ItemNotFoundCommandProperty, value);
    }

    public static readonly DependencyProperty PreFilteredCollectionProperty =
      DependencyProperty.Register("PreFilteredCollection", typeof(IObservableCollection),
        typeof(FilteringComboBox), new PropertyMetadata(null, OnPropertyChangedCallback));

    public IObservableCollection PreFilteredCollection
    {
      get => (IObservableCollection)GetValue(PreFilteredCollectionProperty);
      set => SetValue(PreFilteredCollectionProperty, value);
    }

    private static void OnPropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      var combobox = d as FilteringComboBox;
      if (combobox == null)
        return;

      combobox.PreFilteredCollection.CollectionChanged += combobox.OnPreFilteredCollectionChanged;
      combobox.InitializeItemsViewFilter();
    }

    public void OnPreFilteredCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
    {
      if (PreFilteredCollection.Count == 1)
      {
        _autoSelectionChanged = true;
        FilteredComboBox.SelectedItem = PreFilteredCollection[0];
      }
    }

    private void OnTextChanged(object sender, TextChangedEventArgs e)
    {
      InitializeItemsViewFilter();
      if (string.IsNullOrEmpty(FilteredComboBox.Text) || _autoSelectionChanged)
      {
        _autoSelectionChanged = false;
        return;
      }

      var tb = (TextBox)e.OriginalSource;
      var start = tb.SelectionStart;
      var length = tb.SelectionLength;

      var change = e.Changes.FirstOrDefault();
      if (change != null && change.AddedLength <= 1)
      {
        ShowDropdown();
      }

      FixSelection(start, length);
    }

    private void ShowDropdown()
    {
      if (FilteredComboBox.Items.IsEmpty)
        return;

      if (!FilteredComboBox.IsDropDownOpen)
        FilteredComboBox.IsDropDownOpen = true;
    }

    private void FixSelection(int start, int length)
    {
      var tb = FilteredComboBox.Template.FindName("PART_EditableTextBox", FilteredComboBox) as TextBox;
      if (tb == null)
        return;

      tb.Select(start, length);
    }

    private void OnDropDownClosed(object sender, EventArgs e)
    {
      ValidateSelection();
    }

    public void InitializeItemsViewFilter()
    {
      FilteredComboBox.Items.Filter = FilterDataSource;
    }

    private bool FilterDataSource(object obj)
    {
      var property = obj.GetType().GetProperty(FilteredComboBox.DisplayMemberPath);
      if (property == null)
        return true;

      if (PreFilteredCollection != null && PreFilteredCollection.Count != 0)
      {
        var found = false;
        foreach (var item in PreFilteredCollection)
        {
          var itemProp = item.GetType().GetProperty(FilteredComboBox.DisplayMemberPath);
          if (itemProp == null || itemProp.GetValue(item).ToString() != property.GetValue(obj).ToString())
            found = false;
          else
          {
            found = true;
            break;
          }
        }

        if (!found)
          return false;
      }

      if (string.IsNullOrEmpty(FilteredComboBox.Text))
        return true;

      var objVal = property.GetValue(obj);
      if (objVal == null || string.IsNullOrEmpty(objVal.ToString()))
        return true;

      if (objVal.ToString().ToLower().StartsWith(FilteredComboBox.Text.ToLower()))
        return true;

      return false;
    }

    private void ValidateSelection()
    {
      if (string.IsNullOrEmpty(FilteredComboBox.Text))
        return;

      bool found = false;
      foreach (var item in FilteredComboBox.ItemsSource)
      {
        var property = item.GetType().GetProperty(FilteredComboBox.DisplayMemberPath);
        if (property != null && property.GetValue(item).ToString() == FilteredComboBox.Text)
        {
          found = true;
          break;
        }
      }

      if (!found && !AllowNonExistingItems)
      {
        ItemNotFoundCommand?.Execute(FilteredComboBox
          .ItemsSource.GetType().GetGenericArguments()[0]);
        FilteredComboBox.Text = string.Empty;
      }
    }

    private void OnLostFocus(object sender, RoutedEventArgs e)
    {
      ValidateSelection();
    }
  }
}
