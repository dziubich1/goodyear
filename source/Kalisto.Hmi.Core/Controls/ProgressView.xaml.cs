﻿using System.Windows;
using System.Windows.Controls;

namespace Kalisto.Hmi.Core.Controls
{
  /// <summary>
  /// Interaction logic for ProgressView.xaml
  /// </summary>
  public partial class ProgressView : UserControl
  {
    public ProgressView()
    {
      InitializeComponent();
    }

    public bool IsActive
    {
      get => (bool)GetValue(IsActiveProperty);
      set => SetValue(IsActiveProperty, value);
    }

    public static readonly DependencyProperty IsActiveProperty =
      DependencyProperty.Register("IsActive", typeof(bool),
        typeof(ProgressView), new PropertyMetadata(false, PropertyChangedCallback));

    private static void PropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      var ctrl = d as ProgressView;
      if (ctrl == null)
        return;

      var isActive = (bool) e.NewValue;
      ctrl.ProgressContainer.Visibility = isActive ? Visibility.Visible : Visibility.Hidden;
    }
  }
}
