﻿using System.Collections.Specialized;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Kalisto.Hmi.Core.Controls
{
  /// <summary>
  /// Interaction logic for GalleryControl.xaml
  /// </summary>
  public partial class GalleryControl : UserControl
  {
    public GalleryControl()
    {
      InitializeComponent();
    }

    public IObservableCollection Items
    {
      get => (IObservableCollection)GetValue(ItemsProperty);
      set => SetValue(ItemsProperty, value);
    }

    public static readonly DependencyProperty ItemsProperty =
      DependencyProperty.Register(nameof(Items), typeof(IObservableCollection),
        typeof(GalleryControl), new PropertyMetadata(null, PropertyChangedCallback));

    public int CurrentItemIndex
    {
      get { return (int)GetValue(CurrentItemIndexProperty); }
      set { SetValue(CurrentItemIndexProperty, value); }
    }

    public static readonly DependencyProperty CurrentItemIndexProperty =
     DependencyProperty.Register(nameof(CurrentItemIndex), typeof(int), 
       typeof(GalleryControl), new PropertyMetadata(0));

    private static void PropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      var ctrl = d as GalleryControl;
      if (ctrl == null)
        return;

      var oldList = e.OldValue as IObservableCollection;
      if (oldList != null)
        oldList.CollectionChanged -= ctrl.OnCollectionChanged;

      var newList = e.NewValue as IObservableCollection;
      if (newList == null)
      {
        ctrl.ImagePreview.Source = null;
        ctrl.CurrentItemIndex = -1;
      }
      else
      {
        if (newList.Count > 0)
        {
          ctrl.ImagePreview.Source = newList[0] as ImageSource;
          ctrl.CurrentItemIndex = 0;
        }
        else
        {
          ctrl.ImagePreview.Source = null;
          ctrl.CurrentItemIndex = -1;
        }

        newList.CollectionChanged += ctrl.OnCollectionChanged;
      }

      ctrl.ApplyButtonsVisibility();
    }

    private void OnCollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
    {
      if (e.Action != NotifyCollectionChangedAction.Reset)
        return;

      if (Items.Count > 0)
      {
        ImagePreview.Source = Items[0] as ImageSource;
        CurrentItemIndex = 0;
      }
      else
      {
        ImagePreview.Source = null;
        CurrentItemIndex = -1;
      }

      ApplyButtonsVisibility();
    }

    private void OnRightArrowClick(object sender, RoutedEventArgs e)
    {
      var imageSource = ImagePreview.Source;
      if (imageSource == null)
        return;

      var currentIndex = Items.IndexOf(imageSource);
      if (currentIndex + 1 < Items.Count)
      {
        ImagePreview.Source = Items[currentIndex + 1] as ImageSource;
        CurrentItemIndex = currentIndex + 1;
      }
      else
      {
        ImagePreview.Source = Items.Count > 0 ? Items[0] as ImageSource : null;
        CurrentItemIndex = Items.Count > 0 ? 0 : -1;
      }
    }

    private void OnLeftArrowClick(object sender, RoutedEventArgs e)
    {
      var imageSource = ImagePreview.Source;
      if (imageSource == null)
        return;

      var currentIndex = Items.IndexOf(imageSource);
      if (currentIndex - 1 >= 0)
      {
        ImagePreview.Source = Items[currentIndex - 1] as ImageSource;
        CurrentItemIndex = currentIndex - 1;
      }
      else
      {
        ImagePreview.Source = Items.Count > 0 ? Items[Items.Count - 1] as ImageSource : null;
        CurrentItemIndex = Items.Count > 0 ? Items.Count - 1 : -1;
      }
    }

    private void ApplyButtonsVisibility()
    {
      if (Items != null && Items.Count > 0)
      {
        Button1.Visibility = Visibility.Visible;
        Button2.Visibility = Visibility.Visible;
      }
      else
      {
        Button1.Visibility = Visibility.Collapsed;
        Button2.Visibility = Visibility.Collapsed;
      }
    }
  }
}
