﻿using Kalisto.Hmi.Core.Helpers;
using Kalisto.Hmi.Core.Services;
using Kalisto.Printing;
using Kalisto.Printing.GenericTable;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Permissions;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using System.Windows.Media;
using System.Xml;
using System.Xml.Serialization;
using Kalisto.Localization;
using Kalisto.Localization.DialogTitles;

namespace Kalisto.Hmi.Core.Controls
{
  /// <summary>
  /// Interaction logic for AddUpdateDeleteControl.xaml
  /// </summary>
  public partial class AddUpdateDeleteControl : UserControl
  {
    private readonly IPrintoutManager _printoutManager;

    public AddUpdateDeleteControl()
    {
      InitializeComponent();
      _printoutManager = new PrintoutManager();
    }

    public static readonly DependencyProperty AddItemProperty =
      DependencyProperty.Register("AddItem", typeof(ICommand),
        typeof(AddUpdateDeleteControl), new PropertyMetadata(null));

    public ICommand AddItem
    {
      get => (ICommand)GetValue(AddItemProperty);
      set => SetValue(AddItemProperty, value);
    }

    public static readonly DependencyProperty EditItemProperty =
      DependencyProperty.Register("EditItem", typeof(ICommand),
        typeof(AddUpdateDeleteControl), new PropertyMetadata(null));

    public ICommand EditItem
    {
      get => (ICommand)GetValue(EditItemProperty);
      set => SetValue(EditItemProperty, value);
    }

    public static readonly DependencyProperty DeleteItemProperty =
      DependencyProperty.Register("DeleteItem", typeof(ICommand),
        typeof(AddUpdateDeleteControl), new PropertyMetadata(null));

    public ICommand DeleteItem
    {
      get => (ICommand)GetValue(DeleteItemProperty);
      set => SetValue(DeleteItemProperty, value);
    }

    public static readonly DependencyProperty SyncItemsProperty =
      DependencyProperty.Register("SyncItems", typeof(ICommand),
        typeof(AddUpdateDeleteControl), new PropertyMetadata(null));

    public ICommand SyncItems
    {
      get => (ICommand)GetValue(SyncItemsProperty);
      set => SetValue(SyncItemsProperty, value);
    }

    public static readonly DependencyProperty AddItemEnabledProperty =
      DependencyProperty.Register("AddItemEnabled", typeof(bool),
        typeof(AddUpdateDeleteControl), new PropertyMetadata(true));

    public bool AddItemEnabled
    {
      get => (bool)GetValue(AddItemEnabledProperty);
      set => SetValue(AddItemEnabledProperty, value);
    }

    public static readonly DependencyProperty EditItemEnabledProperty =
      DependencyProperty.Register("EditItemEnabled", typeof(bool),
        typeof(AddUpdateDeleteControl), new PropertyMetadata(true));

    public bool EditItemEnabled
    {
      get => (bool)GetValue(EditItemEnabledProperty);
      set => SetValue(EditItemEnabledProperty, value);
    }

    public static readonly DependencyProperty DeleteItemEnabledProperty =
      DependencyProperty.Register("DeleteItemEnabled", typeof(bool),
        typeof(AddUpdateDeleteControl), new PropertyMetadata(true));

    public bool DeleteItemEnabled
    {
      get => (bool)GetValue(DeleteItemEnabledProperty);
      set => SetValue(DeleteItemEnabledProperty, value);
    }

    public static readonly DependencyProperty SyncItemsEnabledProperty =
      DependencyProperty.Register("SyncItemsEnabled", typeof(bool),
        typeof(AddUpdateDeleteControl), new PropertyMetadata(true));

    public bool SyncItemsEnabled
    {
      get => (bool)GetValue(SyncItemsEnabledProperty);
      set => SetValue(SyncItemsEnabledProperty, value);
    }

    public static readonly DependencyProperty AddItemTextProperty =
      DependencyProperty.Register("AddItemText", typeof(string),
        typeof(AddUpdateDeleteControl), new PropertyMetadata("Dodaj nowy"));

    public string AddItemText
    {
      get => (string)GetValue(AddItemTextProperty);
      set => SetValue(AddItemTextProperty, value);
    }

    public static readonly DependencyProperty EditItemTextProperty =
      DependencyProperty.Register("EditItemText", typeof(string),
        typeof(AddUpdateDeleteControl), new PropertyMetadata("Edytuj"));

    public string EditItemText
    {
      get => (string)GetValue(EditItemTextProperty);
      set => SetValue(EditItemTextProperty, value);
    }

    public static readonly DependencyProperty DeleteItemTextProperty =
      DependencyProperty.Register("DeleteItemText", typeof(string),
        typeof(AddUpdateDeleteControl), new PropertyMetadata("Usuń"));

    public string DeleteItemText
    {
      get => (string)GetValue(DeleteItemTextProperty);
      set => SetValue(DeleteItemTextProperty, value);
    }

    public static readonly DependencyProperty SyncItemsTextProperty =
      DependencyProperty.Register("SyncItemsText", typeof(string),
        typeof(AddUpdateDeleteControl), new PropertyMetadata("Odśwież"));

    public string SyncItemsText
    {
      get => (string)GetValue(SyncItemsTextProperty);
      set => SetValue(SyncItemsTextProperty, value);
    }

    public static readonly DependencyProperty PrintItemProperty =
      DependencyProperty.Register("PrintItem", typeof(ICommand),
        typeof(AddUpdateDeleteControl), new PropertyMetadata(null));


    public ICommand PrintItem
    {
      get => (ICommand)GetValue(PrintItemProperty);
      set => SetValue(PrintItemProperty, value);
    }

    public static readonly DependencyProperty PrintItemsTextProperty =
      DependencyProperty.Register("PrintItemsText", typeof(string),
        typeof(AddUpdateDeleteControl), new PropertyMetadata("Drukuj"));

    public string PrintItemsText
    {
      get => (string)GetValue(PrintItemsTextProperty);
      set => SetValue(PrintItemsTextProperty, value);
    }

    public static readonly DependencyProperty ExportItemsToXmlTextProperty =
      DependencyProperty.Register("ExportItemsToXmlText", typeof(string),
        typeof(AddUpdateDeleteControl), new PropertyMetadata("Eksportuj do pliku XML"));

    public string ExportItemsToXmlText
    {
      get => (string)GetValue(ExportItemsToXmlTextProperty);
      set => SetValue(ExportItemsToXmlTextProperty, value);
    }

    public static readonly DependencyProperty AddItemRuleNameProperty =
      DependencyProperty.Register("AddItemRuleName", typeof(string),
        typeof(AddUpdateDeleteControl), new PropertyMetadata(null));

    public string AddItemRuleName
    {
      get => (string)GetValue(AddItemRuleNameProperty);
      set => SetValue(AddItemRuleNameProperty, value);
    }

    public static readonly DependencyProperty EditItemRuleNameProperty =
      DependencyProperty.Register("EditItemRuleName", typeof(string),
        typeof(AddUpdateDeleteControl), new PropertyMetadata(null));

    public string EditItemRuleName
    {
      get => (string)GetValue(EditItemRuleNameProperty);
      set => SetValue(EditItemRuleNameProperty, value);
    }

    public static readonly DependencyProperty DeleteItemRuleNameProperty =
      DependencyProperty.Register("DeleteItemRuleName", typeof(string),
        typeof(AddUpdateDeleteControl), new PropertyMetadata(null));

    public string DeleteItemRuleName
    {
      get => (string)GetValue(DeleteItemRuleNameProperty);
      set => SetValue(DeleteItemRuleNameProperty, value);
    }

    protected override void OnRender(DrawingContext drawingContext)
    {
      base.OnRender(drawingContext);

      var authorizationService = FrameworkHelper.Resolve<IAuthorizationService>();
      if (authorizationService == null)
        return;

      authorizationService.ForceReauthorizing();
    }

    public static readonly DependencyProperty DataGridProperty =
      DependencyProperty.Register("DataGrid", typeof(DataGrid),
        typeof(AddUpdateDeleteControl), new PropertyMetadata(null));

    public DataGrid DataGrid
    {
      get => (DataGrid)GetValue(DataGridProperty);
      set => SetValue(DataGridProperty, value);
    }

    private void OnPrintItemsButtonClicked(object sender, RoutedEventArgs e)
    {
      if (PrintItem != null)
      {
        PrintItem.Execute(null);
        return;
      }

      if (DataGrid == null)
        return;

      var genericTable = GetGenericTable();
      if (genericTable == null)
        return;

      var printout = new GenericTablePrintout(genericTable);
      _printoutManager.Print(printout);
    }

    private void OnExportItemsToXmlButtonClicked(object sender, RoutedEventArgs e)
    {
      var dlg = new SaveFileDialog
      {
        DefaultExt = ".xml",
        Filter = "XML Files (*.xml)|*.xml"
      };

      var result = dlg.ShowDialog();
      if (!result.HasValue || !result.Value)
        return;

      var fileName = dlg.FileName;
      if (File.Exists(fileName))
      {
        File.SetAttributes(fileName, FileAttributes.Normal);
        var filePermission =
          new FileIOPermission(FileIOPermissionAccess.AllAccess, fileName);
      }

      using (var fs = new FileStream(fileName, FileMode.Create))
      {
        using (var w = new XmlTextWriter(fs, Encoding.UTF8)
        {
          Formatting = Formatting.Indented
        })
        {
          var table = GetGenericTable();
          var xmlDocument = new XmlDocument();
          var serializer = new XmlSerializer(typeof(GenericTable));
          using (var stream = new MemoryStream())
          {
            serializer.Serialize(stream, table);
            stream.Position = 0;
            xmlDocument.Load(stream);
            xmlDocument.Save(w);
          }
        }
      }
    }

    private GenericTable GetGenericTable()
    {
      var viewModel = DataGrid.DataContext as ViewModelBase;
      if (viewModel == null)
        return null;

      return new GenericTable
      {
        Name = new LocalizationManager<DialogTitles>().Localize(viewModel.DialogTitleKey),
        Columns = DataGrid.Columns.Select(c => new GenericTableColumn
        {
          ActualWidth = (float) c.ActualWidth,
          Header = c.Header.ToString()
        }).ToArray(),
        Rows = (from object row in DataGrid.ItemsSource
          select new GenericTableRow
          {
            Items = DataGrid.Columns.Select(c =>
              GetTableItem(row, c, row.GetType().GetProperties().Where(PropertyExistsInColumnSequence))).ToArray()
          }).ToArray()
      };
    }

    private GenericTableItem GetTableItem(object row, DataGridColumn column, IEnumerable<PropertyInfo> properties)
    {
      var boundColumn = column as DataGridBoundColumn;
      if (boundColumn == null)
        throw new InvalidOperationException();

      var binding = boundColumn.Binding as Binding;
      if (binding == null)
        throw new InvalidOperationException();

      var boundedPropertyName = binding.Path.Path;
      foreach (var prop in properties)
      {
        if(boundedPropertyName.StartsWith(prop.Name))
          return new GenericTableItem
          {
            Type = prop.PropertyType,
            Value = GetPropertyValue(prop, row)?.ToString()
          };
      }

      return null;
    }

    private bool PropertyExistsInColumnSequence(PropertyInfo propInfo)
    {
      foreach (var column in DataGrid.Columns)
      {
        var boundColumn = column as DataGridBoundColumn;
        if (boundColumn == null)
          throw new InvalidOperationException();

        var binding = boundColumn.Binding as Binding;
        if (binding == null)
          throw new InvalidOperationException();

        if (binding.Path.Path.StartsWith(propInfo.Name))
          return true;
      }

      return false;
    }

    private object GetPropertyValue(PropertyInfo propInfo, object obj)
    {
      foreach (var column in DataGrid.Columns)
      {
        var boundColumn = column as DataGridBoundColumn;
        if (boundColumn == null)
          throw new InvalidOperationException();

        var binding = boundColumn.Binding as Binding;
        if (binding == null)
          throw new InvalidOperationException();

        if (binding.Path.Path.StartsWith(propInfo.Name))
          return GetPropertyValue(obj, binding.Path.Path);
      }

      return null;
    }

    private object GetPropertyValue(object obj, string propertyName)
    {
      if (!propertyName.Contains("."))
      {
        var property = obj?.GetType().GetProperty(propertyName);
        return property?.GetValue(obj);
      }

      var splitProperties = propertyName.Split('.');
      var firstPropertyName = splitProperties[0];
      propertyName = propertyName.Replace($"{firstPropertyName}.", string.Empty);

      var firstProperty = obj.GetType().GetProperty(firstPropertyName);
      var nestedObj = firstProperty?.GetValue(obj);
      return GetPropertyValue(nestedObj, propertyName);
    }
  }
}
