﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Kalisto.Hmi.Core.Controls
{
  /// <summary>
  /// Interaction logic for ShowChartControl.xaml
  /// </summary>
  public partial class ChartReportControl : UserControl
  {
    public ChartReportControl()
    {
      InitializeComponent();
    }

    public static readonly DependencyProperty ShowChartReportModalProperty =
      DependencyProperty.Register("OpenCommand", typeof(ICommand), 
        typeof(ChartReportControl), new PropertyMetadata(null));

    public ICommand OpenCommand
    {
      get => (ICommand)GetValue(ShowChartReportModalProperty);
      set => SetValue(ShowChartReportModalProperty, value);
    }

    public static readonly DependencyProperty ShowChartReportModalTextProperty =
      DependencyProperty.Register("OpenTooltip", typeof(string),
        typeof(ChartReportControl), new PropertyMetadata("Show dialog"));

    public string OpenTooltip
    {
      get => (string)GetValue(ShowChartReportModalTextProperty);
      set => SetValue(ShowChartReportModalTextProperty, value);
    }
  }
}
