﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Kalisto.Hmi.Core.Controls
{
  /// <summary>
  /// Interaction logic for DialogSubmitPanelControl.xaml
  /// </summary>
  public partial class DialogSubmitPanelControl : UserControl
  {
    public DialogSubmitPanelControl()
    {
      InitializeComponent();
    }

    public static readonly DependencyProperty SubmitProperty =
      DependencyProperty.Register("Submit", typeof(ICommand),
        typeof(DialogSubmitPanelControl), new PropertyMetadata(null));

    public ICommand Submit
    {
      get => (ICommand)GetValue(SubmitProperty);
      set => SetValue(SubmitProperty, value);
    }

    public static readonly DependencyProperty CancelProperty =
      DependencyProperty.Register("Cancel", typeof(ICommand),
        typeof(DialogSubmitPanelControl), new PropertyMetadata(null));

    public ICommand Cancel
    {
      get => (ICommand)GetValue(CancelProperty);
      set => SetValue(CancelProperty, value);
    }
  }
}
