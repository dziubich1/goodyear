﻿using System.Windows;
using System.Windows.Controls;
using Microsoft.Win32;

namespace Kalisto.Hmi.Core.Controls
{
  /// <summary>
  /// Interaction logic for FilePickerControl.xaml
  /// </summary>
  public partial class FilePickerControl : UserControl
  {
    public FilePickerControl()
    {
      InitializeComponent();
    }

    public static readonly DependencyProperty FilePathProperty =
      DependencyProperty.Register("FilePath", typeof(string),
        typeof(FilePickerControl), new PropertyMetadata(string.Empty, PropertyChangedCallback));

    private static void PropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      var filePicker = d as FilePickerControl;
      if (filePicker == null)
        return;

      filePicker.filePathTextBox.Text = e.NewValue.ToString();
    }

    public string FilePath
    {
      get => (string)GetValue(FilePathProperty);
      set => SetValue(FilePathProperty, value);
    }

    private void OnFilePickerButtonClicked(object sender, RoutedEventArgs e)
    {
      var dlg = new OpenFileDialog
      {
        DefaultExt = ".png",
        Filter = "JPEG Files (*.jpeg)|*.jpeg|PNG Files (*.png)|*.png|JPG Files (*.jpg)|*.jpg|GIF Files (*.gif)|*.gif"
      };

      var result = dlg.ShowDialog();
      if (result.HasValue && result.Value)
      {
        FilePath = dlg.FileName;
      }
    }
  }
}
