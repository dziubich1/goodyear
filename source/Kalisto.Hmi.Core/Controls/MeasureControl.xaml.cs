﻿using System.Windows;
using System.Windows.Controls;

namespace Kalisto.Hmi.Core.Controls
{
  public enum WeighingMode
  {
    Auto, Manual
  }
  /// <summary>
  /// Interaction logic for MeasureControl.xaml
  /// </summary>
  public partial class MeasureControl : UserControl
  {
    public MeasureControl()
    {
      InitializeComponent();
      EnableAutoMode();
    }

    public static readonly DependencyProperty WeighingModeProperty =
      DependencyProperty.Register("WeighingMode", typeof(WeighingMode),
        typeof(MeasureControl), new FrameworkPropertyMetadata(WeighingMode.Auto, PropertyChangedCallback)
        {
          BindsTwoWayByDefault = true
        });

    public WeighingMode WeighingMode
    {
      get => (WeighingMode)GetValue(WeighingModeProperty);
      set => SetValue(WeighingModeProperty, value);
    }

    private static void PropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      var ctrl = d as MeasureControl;
      if (ctrl == null)
        return;

      var mode = (WeighingMode)e.NewValue;
      if (mode == WeighingMode.Auto)
        ctrl.EnableAutoMode();
      else ctrl.EnableManualMode();
    }

    private void OnButtonChecked(object sender, RoutedEventArgs e)
    {
      if (Equals(sender, ManualButton))
        EnableManualMode();
      else EnableAutoMode();
    }

    public void EnableManualMode()
    {
      WeighingMode = WeighingMode.Manual;
      if (!ManualButton.IsChecked.HasValue || !ManualButton.IsChecked.Value)
        ManualButton.IsChecked = true;

      AutoButton.IsChecked = false;
    }

    public void EnableAutoMode()
    {
      WeighingMode = WeighingMode.Auto;
      if (!AutoButton.IsChecked.HasValue || !AutoButton.IsChecked.Value)
        AutoButton.IsChecked = true;

      ManualButton.IsChecked = false;
    }
  }
}
