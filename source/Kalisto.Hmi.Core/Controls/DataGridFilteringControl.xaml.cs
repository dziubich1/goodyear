﻿using Kalisto.Localization;
using Kalisto.Localization.Common;
using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

namespace Kalisto.Hmi.Core.Controls
{
  public enum FilteringType
  {
    StartsWith,
    Contains
  }

  /// <summary>
  /// Interaction logic for DataGridFilteringControl.xaml
  /// </summary>
  public partial class DataGridFilteringControl : UserControl
  {
    private class ColumnDescriptor
    {
      public string DisplayName { get; set; }

      public string PropertyName { get; set; }
    }

    private class FilteringTypeDescriptor
    {
      public string DisplayName { get; set; }

      public FilteringType Type { get; set; }
    }

    private readonly ILocalizer<Common> _localizer = new LocalizationManager<Common>();

    private ListCollectionView ListCollectionView => FilteredDataGrid?.ItemsSource as ListCollectionView;

    public readonly DependencyPropertyDescriptor ItemsSourceDescriptor =
      DependencyPropertyDescriptor.FromProperty(ItemsControl.ItemsSourceProperty, typeof(DataGrid));

    public DataGridFilteringControl()
    {
      InitializeComponent();
      FilteringPhraseTextBox.TextChanged += OnFilteringPhraseTextBoxTextChanged;
      ColumnNamesComboBox.ItemsSource = new ObservableCollectionEx<ColumnDescriptor>();
      FilteringTypesComboBox.ItemsSource = new ObservableCollectionEx<FilteringTypeDescriptor>();
    }

    public static readonly DependencyProperty FilteredDataGridProperty =
      DependencyProperty.Register("FilteredDataGrid", typeof(DataGrid),
        typeof(DataGridFilteringControl), new PropertyMetadata(null, OnFilteredDataGridChanged));

    public DataGrid FilteredDataGrid
    {
      get => (DataGrid)GetValue(FilteredDataGridProperty);
      set => SetValue(FilteredDataGridProperty, value);
    }

    private static void OnFilteredDataGridChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      var filteringControl = d as DataGridFilteringControl;
      if (filteringControl == null)
        return;

      if (e.OldValue != null)
        filteringControl.ItemsSourceDescriptor.RemoveValueChanged(e.OldValue, filteringControl.OnItemsSourceChanged);

      if (e.NewValue != null)
        filteringControl.ItemsSourceDescriptor.AddValueChanged(e.NewValue, filteringControl.OnItemsSourceChanged);

      filteringControl.DetermineColumnNames();
      filteringControl.FillFilteringTypes();
    }

    public void DetermineColumnNames()
    {
      if (FilteredDataGrid == null)
        return;

      var collection = ColumnNamesComboBox.ItemsSource as ObservableCollectionEx<ColumnDescriptor>;
      if (collection == null)
        return;

      collection.Clear();
      foreach (var column in FilteredDataGrid.Columns)
      {
        var boundColumn = column as DataGridBoundColumn;
        if (boundColumn == null)
          throw new InvalidOperationException();

        var binding = boundColumn.Binding as Binding;
        if (binding == null)
          throw new InvalidOperationException();

        collection.Add(new ColumnDescriptor
        {
          DisplayName = column.Header.ToString(),
          PropertyName = binding.Path.Path
        });
      }
    }

    public void FillFilteringTypes()
    {
      var filterDescriptors = FilteringTypesComboBox.ItemsSource
        as ObservableCollectionEx<FilteringTypeDescriptor>;
      if (filterDescriptors == null)
        return;

      filterDescriptors.Clear();
      filterDescriptors.Add(new FilteringTypeDescriptor
      {
        Type = FilteringType.StartsWith,
        DisplayName = _localizer.Localize(() => Common.StartsWithLabel)
      });
      filterDescriptors.Add(new FilteringTypeDescriptor
      {
        Type = FilteringType.Contains,
        DisplayName = _localizer.Localize(() => Common.ContainsLabel)
      });
    }

    public void OnItemsSourceChanged(object sender, EventArgs e)
    {
      if (ListCollectionView == null)
        return;

      ListCollectionView.Filter = FilterDataSource;
    }

    private bool FilterDataSource(object obj)
    {
      if (obj == null)
        return false;

      var searchedPhrase = FilteringPhraseTextBox.Text;
      if (string.IsNullOrEmpty(searchedPhrase))
        return true;

      var columnDescriptor = ColumnNamesComboBox.SelectedItem as ColumnDescriptor;
      if (columnDescriptor == null)
        return true;

      var value = GetPropertyValue(obj, columnDescriptor.PropertyName)?.ToString();
      if (string.IsNullOrEmpty(value))
        return false;

      var filteringTypeDescriptor = FilteringTypesComboBox.SelectedItem as FilteringTypeDescriptor;
      if (filteringTypeDescriptor == null)
        return true;

      if (filteringTypeDescriptor.Type == FilteringType.StartsWith)
      {
        if (value.ToLower().StartsWith(searchedPhrase.ToLower()))
          return true;
        return false;
      }

      if (filteringTypeDescriptor.Type == FilteringType.Contains)
      {
        if (value.ToLower().Contains(searchedPhrase.ToLower()))
          return true;
        return false;
      }

      return true;
    }

    private object GetPropertyValue(object obj, string propertyName)
    {
      if (!propertyName.Contains("."))
      {
        var property = obj?.GetType().GetProperty(propertyName);
        return property?.GetValue(obj);
      }

      var splitProperties = propertyName.Split('.');
      var firstPropertyName = splitProperties[0];
      propertyName = propertyName.Replace($"{firstPropertyName}.", string.Empty);

      var firstProperty = obj.GetType().GetProperty(firstPropertyName);
      var nestedObj = firstProperty?.GetValue(obj);
      return GetPropertyValue(nestedObj, propertyName);
    }

    private void OnFilteringPhraseTextBoxTextChanged(object sender, TextChangedEventArgs e)
    {
      ListCollectionView?.Refresh();
    }
  }
}
