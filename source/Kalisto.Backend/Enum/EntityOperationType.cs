﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kalisto.Backend.Enum
{
  public enum EntityOperationType
  {
    Create,
    Update,
    Delete
  }
}
