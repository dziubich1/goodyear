﻿using System;
using System.Data.Entity;
using System.Linq.Expressions;
using System.Reflection;
using Kalisto.Backend.Configuration;
using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.DataAccess.Model.Enum;
using Kalisto.Backend.DataAccess.Model.Logging.Base;
using Kalisto.Communication.Core.Registries;
using Newtonsoft.Json;

namespace Kalisto.Backend.Logging.Base
{
  public abstract class DbEventLoggerBase : IDbEventLogger
  {
    private readonly BackendConfiguration _backendConfiguration;

    private readonly IDbContextFactory _dbContextFactory;

    private readonly IPartnerRegistryReader _partnerRegistryReader;

    protected abstract DbEventType EventType { get; }

    protected DbEventLoggerBase(BackendConfiguration backendConfiguration, IDbContextFactory dbContextFactory,
      IPartnerRegistryReader partnerRegistryReader)
    {
      _backendConfiguration = backendConfiguration;
      _dbContextFactory = dbContextFactory;
      _partnerRegistryReader = partnerRegistryReader;
    }

    public void LogEvent<TTable, TEntity>(string partnerId, Expression<Func<IDbContext, TTable>> propertyLambda, long entityId, TEntity newValue)
      where TTable : IDbSet<TEntity>
      where TEntity : class
    {
      LogEvent<TTable, TEntity>(partnerId, propertyLambda, entityId, newValue, null);
    }

    public void LogEvent<TTable, TEntity>(string partnerId, Expression<Func<IDbContext, TTable>> propertyLambda, long entityId, TEntity newValue, TEntity oldValue)
      where TTable : IDbSet<TEntity>
      where TEntity : class
    {
      var source = "UNKNOWN";
      var user = _partnerRegistryReader.GetCurrentUser(partnerId);
      if (user != null)
        source = user.Name;

      var logEntry = new DbEventLog
      {
        EntityId = entityId,
        TableName = GetTableName<TTable, TEntity>(propertyLambda),
        OldValue = JsonConvert.SerializeObject(oldValue, new JsonSerializerSettings
        {
          TypeNameHandling = TypeNameHandling.All,
          ReferenceLoopHandling = ReferenceLoopHandling.Ignore
        }),
        NewValue = JsonConvert.SerializeObject(newValue, new JsonSerializerSettings
        {
          TypeNameHandling = TypeNameHandling.All,
          ReferenceLoopHandling = ReferenceLoopHandling.Ignore
        }),
        EventType = EventType,
        Source = source,
        EventDateUtc = DateTime.UtcNow
      };

      using (var dbContext = 
        _dbContextFactory.CreateDbContext(_backendConfiguration.DbConnection.ConnectionString))
      {
        dbContext.DbEventLogs.Add(logEntry);
        dbContext.SaveChanges();
      }
    }

    private string GetTableName<TTable, TEntity>(Expression<Func<IDbContext, TTable>> propertyLambda)
      where TTable : IDbSet<TEntity>
      where TEntity : class
    {
      if (!(propertyLambda.Body is MemberExpression memberSelectorExpression))
        throw new InvalidOperationException();

      var property = memberSelectorExpression.Member as PropertyInfo;
      if (property == null)
        throw new InvalidOperationException();

      return property.Name;
    }
  }
}
