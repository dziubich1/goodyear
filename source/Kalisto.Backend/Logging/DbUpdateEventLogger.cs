﻿using Kalisto.Backend.Configuration;
using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.DataAccess.Model.Enum;
using Kalisto.Backend.DataAccess.Model.Logging;
using Kalisto.Backend.Logging.Base;
using Kalisto.Communication.Core.Registries;

namespace Kalisto.Backend.Logging
{
  public class DbUpdateEventLogger : DbEventLoggerBase, IDbUpdateEventLogger
  {
    protected override DbEventType EventType => DbEventType.Update;

    public DbUpdateEventLogger(BackendConfiguration backendConfiguration, IDbContextFactory dbContextFactory,
      IPartnerRegistryReader partnerRegistryReader)
      : base(backendConfiguration, dbContextFactory, partnerRegistryReader)
    {
    }
  }
}
