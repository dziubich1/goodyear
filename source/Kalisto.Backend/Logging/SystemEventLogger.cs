﻿using Kalisto.Backend.Configuration;
using Kalisto.Backend.DataAccess.Model;
using Kalisto.Communication.Core.Logging;
using Kalisto.Communication.Core.Registries;
using System;

namespace Kalisto.Backend.Logging
{
  public class SystemEventLogger : ISystemEventLogger
  {
    private readonly BackendConfiguration _configuration;
    private readonly IDbContextFactory _dbContextFactory;
    private readonly IPartnerRegistryReader _partnerRegistryReader;

    public SystemEventLogger(BackendConfiguration configuration,
      IDbContextFactory dbContextFactory,
      IPartnerRegistryReader partnerRegistryReader)
    {
      _configuration = configuration;
      _dbContextFactory = dbContextFactory;
      _partnerRegistryReader = partnerRegistryReader;
    }

    public void LogEvent(string resourceKey, string[] parameters = null)
    {
      SaveEvent("SYSTEM", resourceKey, parameters);
    }

    public void LogEvent(string partnerId, string resourceKey, string[] parameters = null)
    {
      var source = "UNKNOWN";
      var user = _partnerRegistryReader.GetCurrentUser(partnerId);
      if (user != null)
        source = user.Name;
      SaveEvent(source, resourceKey, parameters);
    }

    private void SaveEvent(string source, string resourceKey, string[] parameters)
    {
      var concatenatedParameters = "";
      if (parameters != null)
        concatenatedParameters = string.Join(",", parameters);
      var systemLog = new SystemLog()
      {
        Source = source,
        LogResourceKey = resourceKey,
        EventDateUtc = DateTime.UtcNow,
        Parameters = concatenatedParameters
      };

      var connString = _configuration.DbConnection.ConnectionString;
      var ctx = _dbContextFactory.CreateDbContext(connString);
      ctx.SystemLogs.Add(systemLog);
      ctx.SaveChanges();
    }
  }
}
