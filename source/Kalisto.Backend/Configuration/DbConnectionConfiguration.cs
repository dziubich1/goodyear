﻿using System;
using Kalisto.Backend.Enum;

namespace Kalisto.Backend.Configuration
{
  [Serializable]
  public class DbConnectionConfiguration
  {
    public DbTypeEnum DatabaseType { get; set; }

    public string ConnectionString { get; set; }
  }
}
