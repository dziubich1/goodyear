﻿using System;
using Kalisto.Backend.Enum;
using Kalisto.Backend.Services;
using Kalisto.Communication.RabbitMq;
using Kalisto.Configuration;
using Kalisto.Devices.IOController.Interfaces;

namespace Kalisto.Backend.Configuration
{
  [Serializable]
  public class BackendConfiguration : ConfigurationBase
  {
    public DbConnectionConfiguration DbConnection { get; set; }

    public RabbitMqConnectionConfiguration RabbitMqConnection { get; set; }

    public string ClientApplicationLogoFilePath { get; set; }
    public string ClientReceiptLogoFilePath { get; set; }

    public int LowWeightTrigger { get; set; }

    public int HighWeightTrigger { get; set; }

    public TrafficState InitialTrafficState { get; set; }

    public BarrierState InitialBarrierState { get; set; }

    public int TruckEntryBarrierDelay { get; set; }

    public int TruckExitBarrierDelay { get; set; }

    public string PhotoDirectoryName { get; set; }

    public override object GetDefaultConfiguration()
    {
      return new BackendConfiguration
      {
        DbConnection = new DbConnectionConfiguration
        {
          DatabaseType = DbTypeEnum.Firebird,
          ConnectionString = $@"User=SYSDBA;Password=masterkey;Database=C:\DB.DAT;DataSource=localhost;Port=3050;"
        },
        RabbitMqConnection = new RabbitMqConnectionConfiguration
        {
          Hostname = "127.0.0.1",
          Username = "admin",
          Password = "admin",
          Port = 5672
        },
        ClientApplicationLogoFilePath = $@"C:\ProgramData\Kalisto\Kalisto.Backend\logo.png",
        ClientReceiptLogoFilePath = $@"C:\ProgramData\Kalisto\Kalisto.Backend\logo2.png",
        PhotoDirectoryName = "photos",
        LowWeightTrigger = 400,
        HighWeightTrigger = 3000
      };
    }
  }
}
