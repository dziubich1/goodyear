﻿using Kalisto.Hmi.Dialogs.Common.Configuration;
using Kalisto.Hmi.Dialogs.Common.Enum;

namespace Kalisto.Backend.Services
{
  public class UnitPreferencesService : IUserPreferencesService
  {
    public UserPreferencesConfiguration Configuration => new UserPreferencesConfiguration
    {
      UnitType = UnitType.T
    };

    public void StorePreferences(UserPreferencesConfiguration preferences)
    {
      // nothing to implement - backend does not support storing user preferences so far
    }
  }
}
