﻿using System.Collections.Generic;
using Kalisto.Configuration.Devices;
using Kalisto.Devices.IOController.Interfaces;
using System.Linq;
using System.Threading.Tasks;

namespace Kalisto.Backend.Services
{
  public class BarrierService : IBarrierService
  {
    private readonly IIoControllerService _ioControllerService;

    private readonly ServerDeviceConfiguration _deviceConfiguration;

    private const int _barrierImpulseDuration = 3000;

    public BarrierService(IIoControllerService ioControllerService, ServerDeviceConfigurationManager deviceConfigurationManager)
    {
      _ioControllerService = ioControllerService;
      _deviceConfiguration = deviceConfigurationManager.Load();
    }

    public async Task RaiseBarrierUpAsync(int weightMeterId)
    {
      var ioController = _deviceConfiguration.IoControllers.SingleOrDefault(c => c.IsActive);
      var barriers = ioController?.Barriers?.Where(c => c.WeightMeterId == weightMeterId).ToList();
      if (barriers == null)
        return;

      if (barriers.Any(c => c.CardReaderId != null))
      {
        var cardReadersIds =
          _deviceConfiguration.CardReaders?.Where(c => c.IsActive && c.WeightMeterId == weightMeterId)
            .Select(c => c.Id) ?? new List<int>();
        barriers = barriers.Where(c => c.CardReaderId != null && cardReadersIds.Contains(c.CardReaderId.Value)).ToList();
      }

      if (!barriers.Any())
        return;

      foreach (var barrier in barriers)
        _ioControllerService.SendSetOutputCommand(ioController.Id, barrier.UpOutputNumber);

      await Task.Delay(_barrierImpulseDuration);
      await RequestBarrierStateAsync();

      foreach (var barrier in barriers)
        _ioControllerService.SendResetOutputCommand(ioController.Id, barrier.UpOutputNumber);
    }

    public async Task LeaveBarrierDownAsync(int weightMeterId)
    {
      var ioController = _deviceConfiguration.IoControllers.SingleOrDefault(c => c.IsActive);
      var barriers = ioController?.Barriers?.Where(c => c.WeightMeterId == weightMeterId).ToList();
      if (barriers == null)
        return;

      if (barriers.Any(c => c.CardReaderId != null))
      {
        var cardReadersIds =
          _deviceConfiguration.CardReaders?.Where(c => c.IsActive && c.WeightMeterId == weightMeterId)
            .Select(c => c.Id) ?? new List<int>();
        barriers = barriers.Where(c => c.CardReaderId != null && cardReadersIds.Contains(c.CardReaderId.Value)).ToList();
      }

      if (!barriers.Any())
        return;

      foreach (var barrier in barriers)
        _ioControllerService.SendSetOutputCommand(ioController.Id, barrier.DownOutputNumber);

      await Task.Delay(_barrierImpulseDuration);
      await RequestBarrierStateAsync();

      foreach (var barrier in barriers)
        _ioControllerService.SendResetOutputCommand(ioController.Id, barrier.DownOutputNumber);
    }

    public async Task RequestBarrierStateAsync()
    {
      var ioControllerConfiguration = _deviceConfiguration.IoControllers.FirstOrDefault(c => c.IsActive);
      if (ioControllerConfiguration == null)
        return;

      _ioControllerService.SendGetInputStatesCommand(ioControllerConfiguration.Id);

      await Task.FromResult(0);
    }
  }
}
