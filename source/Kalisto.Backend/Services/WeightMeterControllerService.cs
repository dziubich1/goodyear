﻿using Kalisto.Backend.Configuration;
using Kalisto.Backend.StateMachine;
using Kalisto.Backend.StateMachine.Payloads;
using System.Collections.Generic;
using System.Linq;

namespace Kalisto.Backend.Services
{
  public interface IWeightMeterControllerService
  {
    void UpdateWeightValue(int deviceId, decimal value, bool isStable);

    decimal GetCurrentWeightValue(int deviceId);

    void Register(int deviceId);
  }

  public class WeightMeterControllerService : IWeightMeterControllerService
  {
    private readonly int _lowWeightTrigger;

    private readonly int _highWeightTrigger;

    private readonly IMeasurementStateMachineRegistry _stateMachineRegistry;

    private readonly List<WeightMeterDevice> _devices = new List<WeightMeterDevice>();

    public WeightMeterControllerService(IMeasurementStateMachineRegistry stateMachineRegistry, BackendConfiguration backendConfiguration)
    {
      _stateMachineRegistry = stateMachineRegistry;
      _lowWeightTrigger = backendConfiguration.LowWeightTrigger;
      _highWeightTrigger = backendConfiguration.HighWeightTrigger;
    }

    public void Register(int deviceId)
    {
      if (_devices.Any(c => c.Id == deviceId))
        return;

      _devices.Add(new WeightMeterDevice
      {
        Id = deviceId
      });
    }

    public decimal GetCurrentWeightValue(int deviceId)
    {
      var device = _devices.FirstOrDefault(c => c.Id == deviceId);
      if (device == null)
        return 0;

      return device.LastWeightValue;
    }

    public void UpdateWeightValue(int deviceId, decimal value, bool isStable)
    {
      var device = _devices.FirstOrDefault(c => c.Id == deviceId);
      if (device == null)
        return;

      if (device.LastWeightValue < value && device.LastWeightValue <= _highWeightTrigger && value >= _highWeightTrigger)
      {
        HandleIncreasingWeightLimitExceeded(deviceId);
      }

      if (device.LastWeightValue > value && device.LastWeightValue >= _lowWeightTrigger && value <= _lowWeightTrigger)
      {
        HandleDecreasingWeightLimitExceeded(deviceId);
      }

      device.LastWeightValue = value;
      device.LastWeightStability = isStable;
    }

    private void HandleDecreasingWeightLimitExceeded(int deviceId)
    {
      _stateMachineRegistry.Fire(MeasurementEvent.DecreasingWeightLimitExceeded, new StateMachinePayloadBase
      {
        AssignedWeightMeterDeviceId = deviceId
      });
    }

    private void HandleIncreasingWeightLimitExceeded(int deviceId)
    {
      _stateMachineRegistry.Fire(MeasurementEvent.IncreasingWeightLimitExceeded, new StateMachinePayloadBase
      {
        AssignedWeightMeterDeviceId = deviceId
      });
    }

    private class WeightMeterDevice
    {
      public int Id { get; set; }

      public decimal LastWeightValue { get; set; }

      public bool LastWeightStability { get; set; }
    }
  }
}
