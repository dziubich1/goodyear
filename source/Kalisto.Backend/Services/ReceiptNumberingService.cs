﻿using Kalisto.Backend.Configuration;
using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.Operations.Measurements.Services;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kalisto.Backend.Services
{
    public class ReceiptNumberingService : IReceiptNumberingService
    {

        private readonly IDbContextFactory _dbContextFactory;
        private readonly BackendConfiguration _configuration;

        public ReceiptNumberingService(IDbContextFactory dbContextFactory, BackendConfiguration configuration)
        {
            _dbContextFactory = dbContextFactory;
            _configuration = configuration;
        }

        public string GetNextReceiptNumberAsync(int year, MeasurementClass measClass)
        {
            using (var context = _dbContextFactory.CreateDbContext(_configuration.DbConnection.ConnectionString))
            {
                int count = context.Measurements.AsNoTracking().Where(c =>
                    c.Class == measClass &&
                    (c.Type == MeasurementType.Single && c.Date1Utc.Year == year ||
                     c.Type == MeasurementType.Double &&
                     (c.Date2Utc != null && c.Date2Utc.Value.Year == year ||
                      c.Date2Utc == null && c.Date1Utc.Year == year)))
                  .OrderBy(c => c.Date1Utc)
                  .ThenBy(c => c.Date2Utc)
                  .Count();

                int seqNumber = count + 1;
                string classStatement = GetClassStatement(measClass);

                return $"{classStatement} {seqNumber}/{year}";
            }
        }

        private string GetClassStatement(MeasurementClass measClass)
        {
            switch (measClass)
            {
                case MeasurementClass.Income:
                    return "PZ";
                case MeasurementClass.Outcome:
                    return "WZ";
                default:
                    return string.Empty;
            }
        }
    }
}