﻿using Kalisto.Backend.Configuration;
using Kalisto.Backend.DataAccess.Model;
using Kalisto.Communication.Core.Logging;
using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Devices.BeltConveyorCounter.Messages.Incoming;
using Kalisto.Devices.BeltConveyorCounter.Messages.Outgoing;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Kalisto.Backend.Services
{
  public class BeltConveyorService : IBeltConveyorService
  {
    private readonly object _syncObject = new object();

    private readonly IMessageBuilder _messageBuilder;

    private readonly IServerMessageProvider _messageProvider;

    private readonly IDbContextFactory _dbContextFactory;

    private readonly ISystemEventLogger _eventLogger;

    private readonly BackendConfiguration _configuration;

    public BeltConveyorService(IMessageBuilder messageBuilder, IServerMessageProvider messageProvider,
      IDbContextFactory dbContextFactory, ISystemEventLogger eventLogger, BackendConfiguration configuration)
    {
      _messageBuilder = messageBuilder;
      _messageProvider = messageProvider;
      _dbContextFactory = dbContextFactory;
      _eventLogger = eventLogger;
      _configuration = configuration;
    }

    public void SendDataImportRequest(int deviceId)
    {
      var message = _messageBuilder.CreateNew<BeltConveyorDataImportRequestMessage>()
        .Broadcasting()
        .WithProperty(c => c.DeviceId, deviceId)
        .Build();

      _messageProvider.BroadcastDeviceMessage(message);
      _eventLogger.LogEvent("BC_DATA_IMPORT_REQUESTED", new[] { deviceId.ToString() });
    }

    public void HandleCounterMemoryError(int deviceId)
    {
      _eventLogger.LogEvent("BC_SD_CARD_ERROR", new[] { deviceId.ToString() });
    }

    public void AcceptDataImport(int deviceId)
    {
      var message = _messageBuilder.CreateNew<BeltConveyorDataImportAcceptMessage>()
        .Broadcasting()
        .WithProperty(c => c.DeviceId, deviceId)
        .Build();

      _messageProvider.BroadcastDeviceMessage(message);
      _eventLogger.LogEvent("BC_DATA_IMPORT_ACCEPTED", new[] { deviceId.ToString() });
    }

    public void CancelDataImport(int deviceId)
    {
      var message = _messageBuilder.CreateNew<BeltConveyorDataImportCancelMessage>()
        .Broadcasting()
        .WithProperty(c => c.DeviceId, deviceId)
        .Build();

      _messageProvider.BroadcastDeviceMessage(message);
      _eventLogger.LogEvent("BC_DATA_IMPORT_CANCELED", new[] { deviceId.ToString() });
    }

    public List<BeltConveyorDataMessage> FilterMeasurementsByDate(List<BeltConveyorDataMessage> measurements)
    {
      var grouped = measurements.GroupBy(c => c.Date.Date).OrderBy(c => c.Key);
      return (from @group in grouped
              let maxDate = @group.Select(c => c.Date).Max()
              select @group.First(c => c.Date == maxDate)).ToList();
    }

    public bool SaveDataImport(int deviceId, IList<BeltConveyorMeasurement> measurements)
    {
      lock (_syncObject)
      {
        using (var context = _dbContextFactory.CreateDbContext(_configuration.DbConnection.ConnectionString))
        {
          var hasItems = context.BeltConveyorMeasurements.Any();
          var lastNumber = hasItems ? context.BeltConveyorMeasurements.Max(c => c.Number) : 0;
          foreach (var meas in measurements)
            meas.Number = ++lastNumber;

          var insertedCounter = 0;
          var updatedCounter = 0;
          foreach (var meas in measurements)
          {
            var existingMeas = context.BeltConveyorMeasurements.FirstOrDefault(c =>
              c.ConfigId == meas.ConfigId && DbFunctions.TruncateTime(c.DateUtc) == DbFunctions.TruncateTime(meas.DateUtc));
            if (existingMeas != null)
            {
              existingMeas.B1 = meas.B1;
              existingMeas.B2 = meas.B2;
              existingMeas.B3 = meas.B3;
              existingMeas.B4 = meas.B4;
              existingMeas.TotalSum = meas.B1 + meas.B2 + meas.B3 + meas.B4;
              existingMeas.IncrementSum = meas.IncrementSum;
              existingMeas.DateUtc = meas.DateUtc;
              updatedCounter++;
            }
            else
            {
              context.BeltConveyorMeasurements.Add(meas);
              insertedCounter++;
            }
          }

          context.SaveChanges();
          _eventLogger.LogEvent("BC_IMPORT_DATA_STORED", new[] { deviceId.ToString() });

          return insertedCounter + updatedCounter == measurements.Count;
        }
      }
    }

    public void ConfirmDataImport(int deviceId)
    {
      var message = _messageBuilder.CreateNew<BeltConveyorDataImportConfirmMessage>()
        .Broadcasting()
        .WithProperty(c => c.DeviceId, deviceId)
        .Build();

      _messageProvider.BroadcastDeviceMessage(message);
      _eventLogger.LogEvent("BC_IMPORT_CONFIRMED", new[] { deviceId.ToString() });
    }

    public void RejectDataImport(int deviceId)
    {
      var message = _messageBuilder.CreateNew<BeltConveyorDataImportRejectMessage>()
        .Broadcasting()
        .WithProperty(c => c.DeviceId, deviceId)
        .Build();

      _messageProvider.BroadcastDeviceMessage(message);
      _eventLogger.LogEvent("BC_IMPORT_REJECTED", new[] { deviceId.ToString() });
    }
  }
}
