﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Devices.BeltConveyorCounter.Messages.Incoming;
using System.Collections.Generic;

namespace Kalisto.Backend.Services
{
  public interface IBeltConveyorService
  {
    void SendDataImportRequest(int deviceId);

    void HandleCounterMemoryError(int deviceId);

    void AcceptDataImport(int deviceId);

    void CancelDataImport(int deviceId);

    bool SaveDataImport(int deviceId, IList<BeltConveyorMeasurement> measurements);

    void ConfirmDataImport(int deviceId);

    void RejectDataImport(int deviceId);

    List<BeltConveyorDataMessage> FilterMeasurementsByDate(List<BeltConveyorDataMessage> measurements);
  }
}
