﻿using Kalisto.Communication.Core.Logging;
using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Devices.Core.Messages;
using Kalisto.Devices.IOController.Messages.Outgoing;
using Kalisto.Logging;
using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading.Tasks;

namespace Kalisto.Backend.Services
{
  public interface IIoControllerService
  {
    int CurrentMessageLoad { get; }

    void SendGetWeightCommand(int deviceId);

    void SendGetInputStatesCommand(int deviceId);

    void SendSetOutputCommand(int deviceId, int output);

    void SendResetOutputCommand(int deviceId, int output);

    void ConfirmCommandResponse<TCommand>() where TCommand : DeviceCommandMessageBase;
  }

  public class IoControllerService : IIoControllerService
  {
    public const int CommandResponseTimeout = 5000;

    private readonly object _syncObj = new object();

    private readonly ILogger _logger;

    private readonly ISystemEventLogger _eventLogger;

    private readonly IMessageBuilder _messageBuilder;

    private readonly IServerMessageProvider _messageProvider;

    private readonly ConcurrentQueue<CommandWrapper<DeviceCommandMessageBase>> _commandQueue = new ConcurrentQueue<CommandWrapper<DeviceCommandMessageBase>>();

    public int CurrentMessageLoad
    {
      get
      {
        lock (_syncObj)
        {
          return _commandQueue.Count;
        }
      }
    }

    public IoControllerService(ILogger logger, ISystemEventLogger eventLogger, IServerMessageProvider messageProvider, IMessageBuilder messageBuilder)
    {
      _logger = logger;
      _eventLogger = eventLogger;
      _messageProvider = messageProvider;
      _messageBuilder = messageBuilder;
    }

    public void SendGetWeightCommand(int deviceId)
    {
      lock (_syncObj)
      {
        var command = _messageBuilder.CreateNew<GetWeightMeterValueMessage>()
          .Broadcasting()
          .WithProperty(c => c.DeviceId, deviceId)
          .Build();

        if (!_commandQueue.Any())
          EnqueueCommand(command);

        var lastElement = _commandQueue.LastOrDefault();
        if (lastElement != null && lastElement.Command.GetType() == command.GetType())
          return;

        EnqueueCommand(command);
      }
    }

    public void SendGetInputStatesCommand(int deviceId)
    {
      lock (_syncObj)
      {
        var command = _messageBuilder.CreateNew<GetInputStatesMessage>()
          .Broadcasting()
          .WithProperty(c => c.DeviceId, deviceId)
          .Build();

        if (!_commandQueue.Any())
          EnqueueCommand(command);

        var lastElement = _commandQueue.LastOrDefault();
        if (lastElement != null && lastElement.Command.GetType() == command.GetType())
          return;

        EnqueueCommand(command);
      }
    }

    public void SendSetOutputCommand(int deviceId, int output)
    {
      lock (_syncObj)
      {
        var command = _messageBuilder.CreateNew<SetOutputMessage>()
          .Broadcasting()
          .WithProperty(c => c.DeviceId, deviceId)
          .WithProperty(c => c.OutputNumber, output)
          .Build();

        EnqueueCommand(command);
      }
    }

    public void SendResetOutputCommand(int deviceId, int output)
    {
      lock (_syncObj)
      {
        var command = _messageBuilder.CreateNew<ResetOutputMessage>()
          .Broadcasting()
          .WithProperty(c => c.DeviceId, deviceId)
          .WithProperty(c => c.OutputNumber, output)
          .Build();

        EnqueueCommand(command);
      }
    }

    public void ConfirmCommandResponse<TCommand>() where TCommand : DeviceCommandMessageBase
    {
      lock (_syncObj)
      {
        if (_commandQueue.TryPeek(out CommandWrapper<DeviceCommandMessageBase> result) && result.Command.GetType() == typeof(TCommand))
          if (_commandQueue.TryDequeue(out _))
            SendNextCommand();
      }
    }

    private void EnqueueCommand<TCommand>(TCommand command) where TCommand : DeviceCommandMessageBase
    {
      lock (_syncObj)
      {
        var commandId = Guid.NewGuid().ToString();
        var wasEmpty = !_commandQueue.Any();
        _commandQueue.Enqueue(new CommandWrapper<DeviceCommandMessageBase>
        {
          Command = command,
          CommandId = commandId
        });

        if (wasEmpty)
          SendNextCommand();
      }
    }

    private void SendNextCommand()
    {
      lock (_syncObj)
      {
        if (_commandQueue.TryPeek(out CommandWrapper<DeviceCommandMessageBase> result) && result != null)
        {
          _messageProvider.BroadcastDeviceMessage(result.Command);
          RunCommandResponseTimeoutCounter(result.CommandId);
        }
      }
    }

    private void RunCommandResponseTimeoutCounter(string commandId)
    {
      Task.Run(async () =>
      {
        await Task.Delay(CommandResponseTimeout);
        if (_commandQueue.TryPeek(out CommandWrapper<DeviceCommandMessageBase> result) &&
            result?.CommandId == commandId)
        {
          if (_commandQueue.TryDequeue(out CommandWrapper<DeviceCommandMessageBase> dequeuedResult))
          {
            _logger.Debug($"Did not receive command response ({dequeuedResult.Command.GetType()}) from IO Controller with ID:{dequeuedResult.Command.DeviceId} withing {CommandResponseTimeout} milliseconds. Command has been disposed.");

            SendNextCommand();
          }
        }
      });
    }

    private class CommandWrapper<TCommand>
    {
      public string CommandId { get; set; }

      public TCommand Command { get; set; }
    }
  }
}
