﻿using Kalisto.Backend.Configuration;
using Kalisto.Backend.Handlers;
using Kalisto.Backend.Operations.Measurements.Commands;
using Kalisto.Backend.StateMachine;
using Kalisto.Backend.StateMachine.Payloads;
using Kalisto.Logging;
using System.Threading.Tasks;
using Kalisto.Devices.IOController.Interfaces;

namespace Kalisto.Backend.Services
{
  public class MeasurementStateMachineEventHandler : IMeasurementStateMachineEventHandler
  {
    private readonly ILogger _logger;

    private readonly MessageHandlerFactory _messageHandlerFactory;

    private readonly IMeasurementStateMachineRegistry _stateMachineRegistry;

    private readonly IWeightMeterControllerService _weightMeterControllerService;

    private readonly ITrafficLightsService _trafficLightsService;

    private readonly IBarrierService _barrierService;

    private readonly BackendConfiguration _backendConfiguration;

    public MeasurementStateMachineEventHandler(ILogger logger, MessageHandlerFactory messageHandlerFactory,
      IMeasurementStateMachineRegistry stateMachineRegistry, IWeightMeterControllerService weightMeterControllerService,
      ITrafficLightsService trafficLightsService, IBarrierService barrierService,
      BackendConfiguration backendConfiguration)
    {
      _logger = logger;
      _messageHandlerFactory = messageHandlerFactory;
      _stateMachineRegistry = stateMachineRegistry;
      _weightMeterControllerService = weightMeterControllerService;
      _trafficLightsService = trafficLightsService;
      _barrierService = barrierService;
      _backendConfiguration = backendConfiguration;
    }

    public async void HandleInitialState(StateMachinePayloadBase parameter)
    {
      _logger.Info($"State machine (ID: {parameter.AssignedWeightMeterDeviceId}) has entered INITIAL state.");

      if (_backendConfiguration.InitialTrafficState == TrafficState.Red)
        _trafficLightsService.SetRedLight(parameter.AssignedWeightMeterDeviceId);
      else _trafficLightsService.SetGreenLight(parameter.AssignedWeightMeterDeviceId);

      if (_backendConfiguration.InitialBarrierState == BarrierState.Up)
        await _barrierService.RaiseBarrierUpAsync(parameter.AssignedWeightMeterDeviceId);
      else await _barrierService.LeaveBarrierDownAsync(parameter.AssignedWeightMeterDeviceId);
    }

    public void HandleMeasurementStarted(StateMachinePayloadBase parameter)
    {
      _logger.Info($"State machine (ID: {parameter.AssignedWeightMeterDeviceId}) has entered MEASUREMENT_STARTED state.");
      var payload = parameter as CardScannedPayload;
      if (payload == null)
      {
        _logger.Error($"Received wrong type of payload. Expected type: {nameof(CardScannedPayload)}, received type: {parameter.GetType().Name}.");
        return;
      }

      // perform measurement here
      var commandHandler = _messageHandlerFactory.GetBackendOperationMessageHandler();
      if (commandHandler == null)
      {
        _logger.Error("Could not retrieve backend operation message handler instance.");
        return;
      }

      commandHandler.HandleMessage(new CreateOrCompleteMeasurementWithCardCommand
      {
        CardCode = payload.CardCode,
        DeviceId = payload.AssignedWeightMeterDeviceId,
        WeightValue = _weightMeterControllerService.GetCurrentWeightValue(payload.AssignedWeightMeterDeviceId)
      });

      _stateMachineRegistry.Fire(MeasurementEvent.MeasurementStored, new StateMachinePayloadBase
      {
        AssignedWeightMeterDeviceId = parameter.AssignedWeightMeterDeviceId
      });
    }

    public async void HandleMeasurementCompleted(StateMachinePayloadBase parameter)
    {
      _logger.Info($"State machine (ID: {parameter.AssignedWeightMeterDeviceId}) has entered MEASUREMENT_COMPLETED state.");

      await _barrierService.RaiseBarrierUpAsync(parameter.AssignedWeightMeterDeviceId);
      await Task.Delay(5000); // introduce a delay between opening a barrier and setting green light on

      _trafficLightsService.SetGreenLight(parameter.AssignedWeightMeterDeviceId);
    }

    public void HandleTruckArrived(StateMachinePayloadBase parameter)
    {
      _logger.Info($"State machine (ID: {parameter.AssignedWeightMeterDeviceId}) has entered TRUCK_ARRIVED state.");

      // TODO: light barriers will be implemented at later stage
    }

    public void HandleTruckArriving(StateMachinePayloadBase parameter)
    {
      _logger.Info($"State machine (ID: {parameter.AssignedWeightMeterDeviceId}) has entered TRUCK_ARRIVING state.");

      _trafficLightsService.SetRedLight(parameter.AssignedWeightMeterDeviceId);
      Task.Run(async () =>
      {
        await Task.Delay(_backendConfiguration.TruckEntryBarrierDelay);
        await _barrierService.LeaveBarrierDownAsync(parameter.AssignedWeightMeterDeviceId);
      });
    }

    public void HandleTruckLeft(StateMachinePayloadBase parameter)
    {
      _logger.Info($"State machine (ID: {parameter.AssignedWeightMeterDeviceId}) has entered TRUCK_LEFT state.");

      if (_backendConfiguration.InitialTrafficState == TrafficState.Red)
        _trafficLightsService.SetRedLight(parameter.AssignedWeightMeterDeviceId);

      Task.Run(async () =>
      {
        await Task.Delay(_backendConfiguration.TruckExitBarrierDelay);
        _stateMachineRegistry.Fire(MeasurementEvent.Reset, new StateMachinePayloadBase
        {
          AssignedWeightMeterDeviceId = parameter.AssignedWeightMeterDeviceId
        });
      });
    }
  }
}
