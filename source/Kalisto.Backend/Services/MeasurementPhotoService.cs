﻿using Kalisto.Backend.Configuration;
using Kalisto.Configuration;
using Kalisto.Core;
using Kalisto.Logging;
using nQuant;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Windows.Media.Imaging;

namespace Kalisto.Backend.Services
{
  public class MeasurementPhotoService : IMeasurementPhotoService
  {
    private static readonly int ThumbnailWidth = 400;

    private readonly ConfigurationManager _backendConfigurationManager;
    private readonly WuQuantizer _quantizer;
    private readonly ILogger _logger;

    public MeasurementPhotoService(ConfigurationManager configuration, WuQuantizer quantizer, ILogger logger)
    {
      _backendConfigurationManager = configuration;
      _quantizer = quantizer;
      _logger = logger;
    }

    public string Store(BitmapImage bitmapImage, string photoRelPath, long measurementNumber, DateTime measurementDateUtc)
    {
      var mainDirectory = _backendConfigurationManager.ConfigDirectory;
      var photosDirectory = _backendConfigurationManager.Load<BackendConfiguration>().PhotoDirectoryName;
      var photoDirectory = Path.Combine(photosDirectory, measurementDateUtc.ToString("MM.yyyy"), measurementNumber.ToString());
      var path = Path.Combine(mainDirectory, photoDirectory, photoRelPath);

      if (!Directory.Exists(Path.GetDirectoryName(path)))
        Directory.CreateDirectory(Path.GetDirectoryName(path) ?? throw new InvalidOperationException());

      try
      {
        using (var bitmap = bitmapImage.ToBitmap())
        {
          using (var quantized = _quantizer.QuantizeImage(bitmap))
          {
            quantized.Save(path, ImageFormat.Png);
          }
        }
      }
      catch
      {
        try
        {
          // we did not quantized image properly - trying to store image as it is
          var encoder = new PngBitmapEncoder();
          encoder.Frames.Add(BitmapFrame.Create(bitmapImage));

          using (var fileStream = new FileStream(path, FileMode.Create))
          {
            encoder.Save(fileStream);
          }
        }
        catch(Exception ex)
        {
          _logger.Error($"Cannot properly store image under {path}. Exception was thrown: {ex.Message}");
          // we did not store the image properly, return empty file path
          return string.Empty;
        }
      }

      return Path.Combine(photoDirectory, photoRelPath);
    }

    public Image Get(string photoRelPath)
    {
      var mainDirectory = _backendConfigurationManager.ConfigDirectory;
      var path = Path.Combine(mainDirectory, photoRelPath);
      if (!Directory.Exists(Path.GetDirectoryName(path)))
      {
        _logger.Error($"Cannot get image from {path} because it does not exist.");
        return null;
      }

      Image image;
      using (var bmpTemp = new Bitmap(path))
      {
        image = new Bitmap(bmpTemp);
      }

      return image;
    }

    public Image GetThumbnail(string photoRelPath)
    {
      var originalImage = Get(photoRelPath);
      var newHeight = (ThumbnailWidth * originalImage.Height) / originalImage.Width;
      var thumbnail = originalImage.ResizeImage(ThumbnailWidth, newHeight);
      return thumbnail;
    }
  }
}
