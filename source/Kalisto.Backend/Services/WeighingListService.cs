﻿using Kalisto.Backend.Configuration;
using Kalisto.Backend.DataAccess.Model;
using Kalisto.Mailing.Notifications.WeighingList;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Kalisto.Backend.Services
{
  public class WeighingListService : IWeighingListService
  {
    private readonly IDbContextFactory _dbContextFactory;

    private readonly BackendConfiguration _configuration;

    public WeighingListService(IDbContextFactory dbContextFactory, BackendConfiguration configuration)
    {
      _dbContextFactory = dbContextFactory;
      _configuration = configuration;
    }

    public Task<List<Measurement>> GetWeighingListAsync(DateTime fromDateUtc, DateTime toDateUtc)
    {
      using (var context = _dbContextFactory.CreateDbContext(_configuration.DbConnection.ConnectionString))
      {
        var measurements = context.Measurements
          .Where(c => c.Date1Utc >= fromDateUtc && c.Date1Utc <= toDateUtc)
          .Where(c => c.Date2Utc == null || c.Date2Utc >= fromDateUtc && c.Date2Utc <= toDateUtc)
          .OrderBy(c => c.Date1Utc)
          .ThenBy(c => c.Date2Utc)
          .Include(c => c.Contractor)
          .Include(c => c.Cargo)
          .AsNoTracking()
          .ToList();

        return Task.FromResult(measurements.Select((c, index) =>
        {
          c.Number = index + 1;
          return c;
        }).ToList());
      }
    }
  }
}
