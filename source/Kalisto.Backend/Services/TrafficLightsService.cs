﻿using System.Collections.Generic;
using Kalisto.Configuration.Devices;
using System.Linq;
using System.Threading.Tasks;
using Kalisto.Devices.IOController.Interfaces;

namespace Kalisto.Backend.Services
{
  public class TrafficLightsService : ITrafficLightsService
  {
    private readonly IIoControllerService _ioControllerService;

    private readonly ServerDeviceConfiguration _deviceConfiguration;

    public TrafficLightsService(IIoControllerService ioControllerService, ServerDeviceConfigurationManager deviceConfigurationManager)
    {
      _ioControllerService = ioControllerService;
      _deviceConfiguration = deviceConfigurationManager.Load();
    }

    public async void SetGreenLight(int weightMeterId)
    {
      var ioController = _deviceConfiguration.IoControllers.SingleOrDefault(c => c.IsActive);
      var trafficLights = ioController?.TrafficLights?.Where(c => c.WeightMeterId == weightMeterId).ToList();
      if (trafficLights == null)
        return;

      if (trafficLights.Any(c => c.CardReaderId != null))
      {
        var cardReadersIds =
          _deviceConfiguration.CardReaders?.Where(c => c.IsActive && c.WeightMeterId == weightMeterId)
            .Select(c => c.Id) ?? new List<int>();
        trafficLights = trafficLights.Where(c => c.CardReaderId != null && cardReadersIds.Contains(c.CardReaderId.Value)).ToList();
      }

      if (!trafficLights.Any())
        return;

      // set green lights & reset red lights
      foreach (var traffic in trafficLights)
      {
        _ioControllerService.SendResetOutputCommand(ioController.Id, traffic.RedOutputNumber);
        _ioControllerService.SendSetOutputCommand(ioController.Id, traffic.GreenOutputNumber);
      }

      await Task.Delay(100);
      await RequestTrafficStateAsync();
    }

    public async void SetRedLight(int weightMeterId)
    {
      var ioController = _deviceConfiguration.IoControllers.SingleOrDefault(c => c.IsActive);
      var trafficLights = ioController?.TrafficLights?.Where(c => c.WeightMeterId == weightMeterId).ToList();
      if (trafficLights == null)
        return;

      if (trafficLights.Any(c => c.CardReaderId != null))
      {
        var cardReadersIds =
          _deviceConfiguration.CardReaders?.Where(c => c.IsActive && c.WeightMeterId == weightMeterId)
            .Select(c => c.Id) ?? new List<int>();
        trafficLights = trafficLights.Where(c => c.CardReaderId != null && cardReadersIds.Contains(c.CardReaderId.Value)).ToList();
      }

      if (!trafficLights.Any())
        return;

      // set red lights & reset green lights
      foreach (var traffic in trafficLights)
      {
        _ioControllerService.SendResetOutputCommand(ioController.Id, traffic.GreenOutputNumber);
        _ioControllerService.SendSetOutputCommand(ioController.Id, traffic.RedOutputNumber);
      }

      await Task.Delay(100);
      await RequestTrafficStateAsync();
    }

    public async Task RequestTrafficStateAsync()
    {
      var ioControllerConfiguration = _deviceConfiguration.IoControllers.FirstOrDefault(c => c.IsActive);
      if (ioControllerConfiguration == null)
        return;

      _ioControllerService.SendGetInputStatesCommand(ioControllerConfiguration.Id);

      await Task.FromResult(0);
    }
  }
}
