﻿using Kalisto.Backend.Operations.Cards.Services;
using Kalisto.Backend.StateMachine;
using Kalisto.Backend.StateMachine.Payloads;
using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Frontend.Notifications;
using System;
using System.Threading.Tasks;

namespace Kalisto.Backend.Services
{
  public interface ICardReaderService : ICardService
  {
    void ForwardProgrammingData(string cardCode);

    void RaiseCardScannedEvent(int deviceId, int assignedWeightMeterDeviceId, string cardCode);
  }

  public class CardReaderService : ICardReaderService
  {
    private readonly IServerMessageProvider _serverMessageProvider;

    private readonly IMessageBuilder _messageBuilder;

    private readonly IMeasurementStateMachineRegistry _stateMachineRegistry;

    private DateTime _lastProgrammingModeActivityDate;

    public int ProgrammingModeIdleTimeout = 60000;

    public bool ProgrammingModeEnabled { get; set; }

    public CardReaderService(IServerMessageProvider serverMessageProvider, IMessageBuilder messageBuilder,
      IMeasurementStateMachineRegistry stateMachineRegistry)
    {
      _serverMessageProvider = serverMessageProvider;
      _messageBuilder = messageBuilder;
      _stateMachineRegistry = stateMachineRegistry;
    }

    public void EnableProgrammingMode()
    {
      ProgrammingModeEnabled = true;
      Task.Run(async () =>
      {
        ResetProgrammingModeIdleTimeoutCounter();
        while ((DateTime.Now < _lastProgrammingModeActivityDate.AddMilliseconds(ProgrammingModeIdleTimeout)) && ProgrammingModeEnabled)
        {
          await Task.Delay(1000);
        }

        if (ProgrammingModeEnabled)
          DisableProgrammingMode();
      });
    }

    public void DisableProgrammingMode()
    {
      ProgrammingModeEnabled = false;
      NotifyProgrammingModeDisabled();
    }

    public void ForwardProgrammingData(string cardCode)
    {
      ResetProgrammingModeIdleTimeoutCounter();
      var notification = _messageBuilder.CreateNew<CardScannedNotification>()
        .Broadcasting()
        .WithProperty(c => c.CardCode, cardCode)
        .Build();

      _serverMessageProvider.BroadcastNotification(notification);
    }

    public void RaiseCardScannedEvent(int deviceId, int assignedWeightMeterDeviceId, string cardCode)
    {
      if (ProgrammingModeEnabled)
        return;

      _stateMachineRegistry.Fire(MeasurementEvent.CardScanned, new CardScannedPayload
      {
        DeviceId = deviceId,
        AssignedWeightMeterDeviceId = assignedWeightMeterDeviceId,
        CardCode = cardCode
      });
    }

    private void NotifyProgrammingModeDisabled()
    {
      var notification = _messageBuilder.CreateNew<CardReaderProgrammingModeStateChangedNotification>()
        .Broadcasting()
        .WithProperty(c => c.ProgrammingModeEnabled, false)
        .Build();

      _serverMessageProvider.BroadcastNotification(notification);
    }

    private void ResetProgrammingModeIdleTimeoutCounter()
    {
      _lastProgrammingModeActivityDate = DateTime.Now;
    }
  }
}
