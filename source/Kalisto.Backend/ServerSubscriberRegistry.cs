﻿using Kalisto.Backend.Handlers;
using Kalisto.Backend.Operations.BeltConveyorMeasurements.Commands;
using Kalisto.Backend.Operations.BeltConveyorMeasurements.Queries;
using Kalisto.Backend.Operations.Cards.Commands;
using Kalisto.Backend.Operations.Cards.Queries;
using Kalisto.Backend.Operations.Cargoes.Commands;
using Kalisto.Backend.Operations.Cargoes.Queries;
using Kalisto.Backend.Operations.Contractors.Commands;
using Kalisto.Backend.Operations.Contractors.Queries;
using Kalisto.Backend.Operations.Dashboard.Commands;
using Kalisto.Backend.Operations.Dashboard.Queries;
using Kalisto.Backend.Operations.DbEventLogs.Queries;
using Kalisto.Backend.Operations.Drivers.Commands;
using Kalisto.Backend.Operations.Drivers.Queries;
using Kalisto.Backend.Operations.Measurements.Commands;
using Kalisto.Backend.Operations.Measurements.Queries;
using Kalisto.Backend.Operations.Shell.Commands;
using Kalisto.Backend.Operations.Shell.Queries;
using Kalisto.Backend.Operations.SystemEventLogs.Queries;
using Kalisto.Backend.Operations.Users.Commands;
using Kalisto.Backend.Operations.Users.Queries;
using Kalisto.Backend.Operations.Vehicles.Commands;
using Kalisto.Backend.Operations.Vehicles.Queries;
using Kalisto.Communication.Core.Communication;
using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Communication.Core.Registries;
using Kalisto.Devices.Core.Communication;
using Kalisto.Devices.Core.Messages;
using Kalisto.Devices.Core.Services;
using Kalisto.Logging;

namespace Kalisto.Backend
{
  public class ServerSubscriberRegistry : SubscriberRegistry
  {
    private readonly MessageHandlerFactory _messageHandlerFactory;

    private readonly DbOperationHandlerFactory _dbOperationHandlerFactory;

    private readonly ICommunicationComponentFactory _communicationComponentFactory;

    private readonly IPartnerRegistryReader _partnerRegistryReader;

    private readonly IOperationMessageHandlerRegistry _operationMessageRegistry;

    public ServerSubscriberRegistry(ICommunicationComponentFactory communicationComponentFactory,
      MessageHandlerFactory messageHandlerFactory, DbOperationHandlerFactory dbOperationHandlerFactory,
      IPartnerRegistryReader partnerRegistryReader, IOperationMessageHandlerRegistry operationMessageRegistry)
    {
      _communicationComponentFactory = communicationComponentFactory;
      _messageHandlerFactory = messageHandlerFactory;
      _dbOperationHandlerFactory = dbOperationHandlerFactory;
      _partnerRegistryReader = partnerRegistryReader;
      _partnerRegistryReader.PartnerRegistryEntryCreated += OnPartnerRegistryEntryCreated;
      _partnerRegistryReader.PartnerRegistryEntryRemoved += OnPartnerRegistryEntryRemoved;
      _operationMessageRegistry = operationMessageRegistry;
    }

    public override void Initialize()
    {
      // register registration queue message receiver
      var registrationMessageReceiver = _communicationComponentFactory.GetRegistrationMessageReceiver();
      RegisterSubscriber(registrationMessageReceiver, PartnerRegistrationMessage.Identifier,
        _messageHandlerFactory.GetRegistrationMessageHandler());

      // register heartbeat queue message receiver
      var heartbeatMessageReceiver = _communicationComponentFactory.GetHeartbeatMessageReceiver();
      RegisterSubscriber(heartbeatMessageReceiver, HeartbeatMessage.Identifier,
        _messageHandlerFactory.GetHeartbeatMessageHandler());

      // register query handlers
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<GetAllUsersQueryHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<GetSmtpSettingsQueryHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<GetNotificationSettingsQueryHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<GetSystemEventLogsQueryHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<GetDbEventLogsQueryHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<GetAllContractorsQueryHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<GetAllCargoesQueryHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<GetAllDriversQueryHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<GetMeasurementsQueryHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<GetAllVehiclesQueryHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<GetAllCardsQueryHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<GetMaxCardNumberQueryHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<GetNotCompletedMeasurementsQueryHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<GetFilteredMeasurementsQueryHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<GetNextMeasurementNumberQueryHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<GetAllSemiTrailersQueryHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<GetMeasurementByIdQueryHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<GetSequenceNumberByDateQueryHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<GetDeviceConfigurationQueryHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<GetMeasurementPhotosByIdQueryHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<GetBeltConveyorMeasurementsQueryHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<GetOrAddCargoQueryHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<GetOrAddContractorQueryHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<GetMeasurementByQrCodeQueryHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<GetCompanyInfoConfigurationQueryHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<GetAllCargoesInventoryQueryHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<GetMeasurementExportSettingsQueryHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<GetStateMachineStatusQueryHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<GetCargoInventoryChartReportDataQueryHandler>());

      // register command handlers
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<CreateUserCommandHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<UpdateUserCommandHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<DeleteUserCommandHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<LoginCommandHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<StoreSmtpSettingsCommandHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<StoreNotificationSettingsCommandHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<GenerateLicensePreKeyCommandHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<AssignLicenseKeyCommandHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<CreateContractorCommandHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<UpdateContractorCommandHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<DeleteContractorCommandHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<CreateCargoCommandHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<UpdateCargoCommandHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<DeleteCargoCommandHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<CreateDriverCommandHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<DeleteDriverCommandHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<UpdateDriverCommandHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<CreateVehicleCommandHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<DeleteVehicleCommandHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<UpdateVehicleCommandHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<CreateCardCommandHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<DeleteCardCommandHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<UpdateCardCommandHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<EnableDisableCardReaderProgrammingModeCommandHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<UpdateVehicleTareCommandHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<CancelMeasurementCommandHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<CreateSingleMeasurementCommandHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<CreateDoubleMeasurementCommandHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<CompleteDoubleMeasurementCommandHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<DeleteMeasurementByIdCommandHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<LogoutCommandHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<UpdateMeasurementCommandHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<UpdateBeltConveyorMeasurementCommandHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<CreateDoubleMeasurementWithQrCodeCommandHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<StoreCompanyInfoConfigurationCommandHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<StoreMeasurementExportSettingsCommandHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<CreateOrCompleteMeasurementWithCardCommandHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<ChangeAutoModeCommandHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<RaiseOrLeaveBarrierCommandHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<TurnGreenOrRedLightCommandHandler>());
      _operationMessageRegistry.RegisterOperationHandler(
        _dbOperationHandlerFactory.GetHandler<RequestOutputStatesCommandHandler>());
    }

    private void OnPartnerRegistryEntryCreated(string partnerId, ClientType type)
    {
      // additional security check
      if (!_partnerRegistryReader.IsPartnerActive(partnerId))
        return;

      var receiver = _communicationComponentFactory.GetServerMessageReceiver(partnerId);
      switch (type)
      {
        case ClientType.Frontend:
          RegisterSubscriber(partnerId, receiver, BackendDbOperationMessage.Identifier,
            _messageHandlerFactory.GetBackendOperationMessageHandler());
          RegisterSubscriber(partnerId, receiver, ClientLogoRequestMessage.Identifier,
            _messageHandlerFactory.GetClientLogoRequestMessageHandler());
          RegisterSubscriber(partnerId, receiver, ServerImageRequest.Identifier,
            _messageHandlerFactory.GetServerImageRequestMessageHandler());
          RegisterSubscriber(partnerId, receiver, ServerImageThumbnailRequest.Identifier,
            _messageHandlerFactory.GetServerImageThumbnailRequestMessageHandler());
          break;
        case ClientType.Device:
          RegisterSubscriber(partnerId, receiver, DeviceMessage.Identifier,
            _messageHandlerFactory.GetDeviceMessageHandler());
          RegisterSubscriber(partnerId, receiver, DeviceConfigurationsRequestMessage.Identifier,
            _messageHandlerFactory.GetDeviceConfigurationRequestHandler());
          break;
      }

      RegisterSubscriber(partnerId, receiver, SystemVersionCompatibilityCheckRequestMessage.Identifier,
        _messageHandlerFactory.GetSystemVersionCheckRequestMessageHandler());
    }

    private void OnPartnerRegistryEntryRemoved(string partnerId)
    {
      // additional security check
      if (_partnerRegistryReader.IsPartnerActive(partnerId))
      {
        return;
      }

      UnregisterSubscribers(partnerId);
    }
  }
}
