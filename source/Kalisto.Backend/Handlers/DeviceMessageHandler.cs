﻿using Kalisto.Backend.Handlers.Devices;
using Kalisto.Backend.Services;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Devices.Core.Messages;

namespace Kalisto.Backend.Handlers
{
  public class DeviceMessageHandler : MessageHandlerBase<DeviceMessage>
  {
    private readonly IDeviceMessageHandlerFactory _handlerFactory;

    public DeviceMessageHandler(IDeviceMessageHandlerFactory handlerFactory)
    {
      _handlerFactory = handlerFactory;
    }

    protected override void HandleMessage(DeviceMessage message)
    {
      var handler = _handlerFactory.GetHandler(message);
      if (handler == null)
        return;

      message.Handle(handler);
    }
  }
}
