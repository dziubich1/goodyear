﻿using Kalisto.Backend.StateMachine;
using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Communication.Core.Registries;
using Kalisto.Licensing;
using Kalisto.Logging;
using System.Threading.Tasks;

namespace Kalisto.Backend.Handlers
{
  public class RegistrationMessageHandler : MessageHandlerBase<PartnerRegistrationMessage>
  {
    private readonly ILogger _logger;

    private readonly IPartnerRegistry _partnerRegistry;

    private readonly IPartnerRegistryReader _partnerRegistryReader;

    private readonly IServerMessageProvider _messageProvider;

    private readonly IMeasurementStateMachineRegistry _measurementStateMachineRegistry;

    private readonly ILicenseInfo _licenseInfo;

    public RegistrationMessageHandler(IMessageBuilder messageBuilder, ILogger logger, IPartnerRegistry partnerRegistry,
      IPartnerRegistryReader partnerRegistryReader, IMeasurementStateMachineRegistry measurementStateMachineRegistry,
    IServerMessageProvider messageProvider, ILicenseInfo licenseInfo)
      : base(messageBuilder)
    {
      _logger = logger;
      _partnerRegistry = partnerRegistry;
      _partnerRegistryReader = partnerRegistryReader;
      _messageProvider = messageProvider;
      _licenseInfo = licenseInfo;
      _measurementStateMachineRegistry = measurementStateMachineRegistry;
    }

    protected override void HandleMessage(PartnerRegistrationMessage message)
    {
      var partnerId = message.PartnerId;
      if (string.IsNullOrEmpty(partnerId))
        return;

      var succeeded = true;
      string failureReasonCode = null;

      if (message.Type == ClientType.Frontend)
      {
        var licenseInfo = _licenseInfo.GetLicenseInfo();
        if (licenseInfo.Type != LicenseType.None &&
            _partnerRegistryReader.RegisteredLicensingPartnersCount >= licenseInfo.StationCount)
        {
          succeeded = false;
          failureReasonCode = "LICENSING_1003";
          _logger.Info("Maximum working stations has been reached");
        }
      }

      if (message.Type == ClientType.Device)
      {
        if (_partnerRegistryReader.IsPartnerTypeAlreadyRegistered(ClientType.Device))
          return;

        Task.Run(async () =>
        {
          await Task.Delay(60000);

          if (!_measurementStateMachineRegistry.IsRunning())
            _measurementStateMachineRegistry.Start();
        });
      }

      _partnerRegistry.RegisterPartner(partnerId, message.Type);
      var responseMessage = MessageBuilder.CreateNew<PartnerRegistrationResponseMessage>()
        .BasedOn(message)
        .WithProperty(c => c.Succeeded, succeeded)
        .WithProperty(c => c.FailureReasonCode, failureReasonCode)
        .Build();

      _messageProvider.SendMessage(responseMessage);
      _logger.Info($"Successfully handled registration message from partner with ID:{partnerId}");
    }
  }
}
