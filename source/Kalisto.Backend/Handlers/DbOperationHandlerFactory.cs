﻿using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Unity;

namespace Kalisto.Backend.Handlers
{
  public class DbOperationHandlerFactory
  {
    private readonly IUnityContainer _container;

    public DbOperationHandlerFactory(IUnityContainer container)
    {
      _container = container;
    }

    public THandler GetHandler<THandler>() where THandler : IOperationMessageHandler
    {
      return _container.Resolve<THandler>();
    }
  }
}
