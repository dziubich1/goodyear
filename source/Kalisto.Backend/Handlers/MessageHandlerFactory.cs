﻿using Kalisto.Communication.Core.Messaging;
using Unity;

namespace Kalisto.Backend.Handlers
{
  public class MessageHandlerFactory
  {
    private readonly IUnityContainer _dependencyContainer;

    public MessageHandlerFactory(IUnityContainer container)
    {
      _dependencyContainer = container;
    }

    public IMessageHandler GetRegistrationMessageHandler()
    {
      return _dependencyContainer.Resolve<RegistrationMessageHandler>();
    }

    public IMessageHandler GetHeartbeatMessageHandler()
    {
      return _dependencyContainer.Resolve<HeartbeatMessageHandler>();
    }

    public IMessageHandler GetBackendOperationMessageHandler()
    {
      return _dependencyContainer.Resolve<BackendOperationMessageHandler>();
    }

    public IMessageHandler GetDeviceMessageHandler()
    {
      return _dependencyContainer.Resolve<DeviceMessageHandler>();
    }

    public IMessageHandler GetClientLogoRequestMessageHandler()
    {
      return _dependencyContainer.Resolve<ClientLogoRequestHandler>();
    }

    public IMessageHandler GetSystemVersionCheckRequestMessageHandler()
    {
      return _dependencyContainer.Resolve<SystemVersionCheckRequestHandler>();
    }

    public IMessageHandler GetServerImageRequestMessageHandler()
    {
      return _dependencyContainer.Resolve<ServerImageRequestHandler>();
    }

    public IMessageHandler GetServerImageThumbnailRequestMessageHandler()
    {
      return _dependencyContainer.Resolve<ServerImageThumbnailRequestHandler>();
    }

    public IMessageHandler GetDeviceConfigurationRequestHandler()
    {
      return _dependencyContainer.Resolve<DeviceConfigurationRequestHandler>();
    }
  }
}
