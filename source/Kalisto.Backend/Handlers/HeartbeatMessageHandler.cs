﻿using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Communication.Core.Registries;
using Kalisto.Logging;

namespace Kalisto.Backend.Handlers
{
  public class HeartbeatMessageHandler : MessageHandlerBase<HeartbeatMessage>
  {
    private readonly ILogger _logger;

    private readonly IPartnerRegistry _partnerRegistry;

    private readonly IPartnerRegistryReader _partnerRegistryReader;

    private readonly IServerMessageProvider _messageProvider;

    private readonly IMessageBuilder _messageBuilder;

    public HeartbeatMessageHandler(ILogger logger, IPartnerRegistry partnerRegistry,
      IPartnerRegistryReader partnerRegistryReader, IServerMessageProvider messageProvider,
      IMessageBuilder messageBuilder)
    {
      _logger = logger;
      _partnerRegistry = partnerRegistry;
      _partnerRegistryReader = partnerRegistryReader;
      _messageProvider = messageProvider;
      _messageBuilder = messageBuilder;
    }

    protected override void HandleMessage(HeartbeatMessage message)
    {
      _logger.Debug($"Received heartbeat for partner with ID:{message.PartnerId}");
      if (_partnerRegistry.UpdatePartnerHeartbeat(message.PartnerId))
        return;

      _partnerRegistry.RegisterPartner(message.PartnerId, message.Type);
      var loginRequiredMessage = _messageBuilder.CreateNew<LoginRequiredMessage>()
        .BasedOn(message)
        .WithProperty(c => c.Broadcasting, true) // intended for all message bridges
        .Build();

      _messageProvider.SendMessage(loginRequiredMessage);
    }
  }
}
