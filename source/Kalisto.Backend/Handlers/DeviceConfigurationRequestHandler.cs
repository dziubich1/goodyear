﻿using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Configuration.Devices;
using Kalisto.Devices.Core.Services;
using System.Collections.Generic;
using System.Linq;
using Kalisto.Configuration.Enums;

namespace Kalisto.Backend.Handlers
{
  public class DeviceConfigurationRequestHandler : MessageHandlerBase<DeviceConfigurationsRequestMessage>
  {
    private readonly IMessageProvider _messageProvider;

    private readonly ServerDeviceConfigurationManager _serverDeviceConfigurationManager;

    public DeviceConfigurationRequestHandler(IServerMessageProvider messageProvider, IMessageBuilder messageBuilder, ServerDeviceConfigurationManager deviceConfigurationManager)
      : base(messageBuilder)
    {
      _messageProvider = messageProvider;
      _serverDeviceConfigurationManager = deviceConfigurationManager;
    }

    protected override void HandleMessage(DeviceConfigurationsRequestMessage message)
    {
      var devices = new List<DeviceConfiguration>();
      var config = _serverDeviceConfigurationManager.Load();
      if (config != null)
      {
        // TODO: add all required device configurations like below
        devices.AddRange(
          config.WeightMeters
          .Where(c => c.IsActive && c.Type == DeviceType.External)
            .Cast<DeviceConfiguration>().ToList());

        devices.AddRange(
          config.BeltConveyorCounters
            .Where(c => c.IsActive && c.Type == DeviceType.External)
            .Cast<DeviceConfiguration>().ToList());

        devices.AddRange(
          config.CardReaders
            .Where(c => c.IsActive && c.Type == DeviceType.External)
            .Cast<DeviceConfiguration>().ToList());

        devices.AddRange(config.IoControllers
          .Where(c => c.IsActive && c.Type == DeviceType.External)
          .Cast<DeviceConfiguration>().ToList());
      }

      var responseMessage = MessageBuilder.CreateNew<DeviceConfigurationsResponseMessage>()
        .BasedOn(message)
        .WithProperty(c => c.Result, devices)
        .Build();

      _messageProvider.SendMessage(responseMessage);
    }
  }
}
