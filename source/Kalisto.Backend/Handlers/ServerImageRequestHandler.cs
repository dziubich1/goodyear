﻿using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Core;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;

namespace Kalisto.Backend.Handlers
{
  public class ServerImageRequestHandler : MessageHandlerBase<ServerImageRequest>
  {
    private readonly IServerMessageProvider _messageProvider;

    // todo: this should be more generic image service (not only for measurement photos)
    private readonly IMeasurementPhotoService _imageService;

    public ServerImageRequestHandler(IMessageBuilder messageBuilder, IServerMessageProvider messageProvider,
      IMeasurementPhotoService imageService)
      : base(messageBuilder)
    {
      _imageService = imageService;
      _messageProvider = messageProvider;
    }

    protected override void HandleMessage(ServerImageRequest message)
    {
      Image image = null;

      try
      {
        image = _imageService.Get(message.RelativePath);
      }
      catch
      {
        // ignore
      }
      var responseMessage = MessageBuilder.CreateNew<ServerImageResponse>()
        .BasedOn(message)
        .WithProperty(c => c.Result, image?.ToBase64String(ImageFormat.Png))
        .Build();

      _messageProvider.SendMessage(responseMessage);
    }
  }
}
