﻿using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Communication.Core.Services;
using Kalisto.Logging;

namespace Kalisto.Backend.Handlers
{
  public class SystemVersionCheckRequestHandler
    : MessageHandlerBase<SystemVersionCompatibilityCheckRequestMessage>
  {
    private readonly ILogger _logger;

    private readonly IMessageProvider _messageProvider;

    private readonly IMessageBuilder _messageBuilder;

    private readonly IAssemblyVerifier _assemblyVerifier;

    public SystemVersionCheckRequestHandler(ILogger logger, IServerMessageProvider messageProvider,
      IMessageBuilder messageBuilder, IAssemblyVerifier assemblyVerifier)
    {
      _logger = logger;
      _messageProvider = messageProvider;
      _messageBuilder = messageBuilder;
      _assemblyVerifier = assemblyVerifier;
    }

    protected override void HandleMessage(SystemVersionCompatibilityCheckRequestMessage message)
    {
      _logger.Info($"Checking assemblies compability for partner: {message.PartnerId}");
      var backendAssembliesVersion = _assemblyVerifier.GetAssembliesVersion();
      var isClientCompatible = message.ClientVersion == backendAssembliesVersion;
      if (!isClientCompatible)
        _logger.Error(
          $"Partner: {message.PartnerId} uses incompatible assemblies (ver.: {message.ClientVersion}).");

      var response = _messageBuilder.CreateNew<SystemVersionCompatibilityCheckResponseMessage>()
        .BasedOn(message)
        .WithProperty(c => c.Result, new SystemVersionCompatibilityDto
        {
          IsCompatible = isClientCompatible,
          ServerVersion = backendAssembliesVersion
        })
        .Build();

      _messageProvider.SendMessage(response);
    }
  }
}
