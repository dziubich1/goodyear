﻿using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Core;
using System.Drawing;
using System.Drawing.Imaging;

namespace Kalisto.Backend.Handlers
{
  public class ServerImageThumbnailRequestHandler : MessageHandlerBase<ServerImageThumbnailRequest>
  {
    private readonly IServerMessageProvider _messageProvider;

    // todo: this should be more generic image service (not only for measurement photos)
    private readonly IMeasurementPhotoService _imageService;

    public ServerImageThumbnailRequestHandler(IMessageBuilder messageBuilder, IServerMessageProvider messageProvider,
      IMeasurementPhotoService imageService)
      : base(messageBuilder)
    {
      _imageService = imageService;
      _messageProvider = messageProvider;
    }

    protected override void HandleMessage(ServerImageThumbnailRequest message)
    {
      Image image = null;

      try
      {
        image = _imageService.GetThumbnail(message.RelativePath);
      }
      catch
      {
        // ignore
      }
      var responseMessage = MessageBuilder.CreateNew<ServerImageResponse>()
        .BasedOn(message)
        .WithProperty(c => c.Result, image?.ToBase64String(ImageFormat.Png))
        .Build();

      _messageProvider.SendMessage(responseMessage);
    }
  }
}
