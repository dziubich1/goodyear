﻿using Kalisto.Backend.Configuration;
using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Core;
using Kalisto.Logging;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;

namespace Kalisto.Backend.Handlers
{
    public class ClientLogoRequestHandler : MessageHandlerBase<ClientLogoRequestMessage>
    {
        private readonly ILogger _logger;

        private readonly IMessageProvider _messageProvider;

        private readonly IMessageBuilder _messageBuilder;

        private readonly BackendConfiguration _configuration;

        public ClientLogoRequestHandler(ILogger logger, IServerMessageProvider messageProvider, IMessageBuilder messageBuilder, BackendConfiguration configuration)
        {
            _logger = logger;
            _messageProvider = messageProvider;
            _messageBuilder = messageBuilder;
            _configuration = configuration;
        }

        protected override void HandleMessage(ClientLogoRequestMessage message)
        {
            _logger.Debug($"Client with ID: {message.PartnerId} has requested the logo image.");
            var imageContent = string.Empty;

            //if message.logoType is "receipt" get receipt logo, if empty client application logo
            var FilePath = string.IsNullOrEmpty(message.LogoType) ? _configuration.ClientApplicationLogoFilePath : _configuration.ClientReceiptLogoFilePath;
            
            if (File.Exists(FilePath))
            {
                try
                {
                    var image = Image.FromFile(FilePath);
                    imageContent = image.ToBase64String();
                }
                catch (Exception ex)
                {
                    _logger.Error($"Cannot load logo image. Exception was thrown: {ex.Message}");
                }
            }

            if (string.IsNullOrEmpty(imageContent))
            {
                _logger.Debug("Client logo image content is null or empty. Application will use default logo");
                var image = new Bitmap(Resources.logo1);
                imageContent = image.ToBase64String(ImageFormat.Png);
            }

            var responseMessage = _messageBuilder.CreateNew<ClientLogoResponseMessage>()
              .BasedOn(message)
              .WithProperty(c => c.Result, imageContent)
              .Build();

            _messageProvider.SendMessage(responseMessage);
        }
    }
}
