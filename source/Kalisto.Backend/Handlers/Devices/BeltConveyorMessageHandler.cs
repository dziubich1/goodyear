﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.Services;
using Kalisto.Devices.BeltConveyorCounter.Handlers;
using Kalisto.Devices.BeltConveyorCounter.Messages.Incoming;
using Kalisto.Devices.Core;
using Kalisto.Logging;
using System.Collections.Generic;
using System.Linq;

namespace Kalisto.Backend.Handlers.Devices
{
  public class BeltConveyorMessageHandler : IBeltConveyorMessageHandler
  {
    private readonly IDeviceSessionManager _deviceSessionManager;

    private readonly IBeltConveyorService _beltConveyorService;

    private readonly ILogger _logger;

    public BeltConveyorMessageHandler(IDeviceSessionManager deviceSessionManager, IBeltConveyorService beltConveyorService, ILogger logger)
    {
      _deviceSessionManager = deviceSessionManager;
      _beltConveyorService = beltConveyorService;
      _logger = logger;
    }

    public void HandleBeltConveyorDataImportResponse(BeltConveyorDataImportResponseMessage message)
    {
      if (message == null)
      {
        _logger.Error($"Cannot handle belt conveyor data import response because message is not valid.");
        return;
      }

      if (message.HasMemoryError)
      {
        _beltConveyorService.HandleCounterMemoryError(message.DeviceId);
        return;
      }

      if (message.ItemsCount > 0)
        _beltConveyorService.AcceptDataImport(message.DeviceId);
      else _beltConveyorService.CancelDataImport(message.DeviceId);
    }

    public void HandleBeltConveyorDataImportSummary(BeltConveyorDataImportSummaryMessage message)
    {
      if (message == null)
      {
        _logger.Error($"Cannot handle properly belt conveyor data import summary because message is not valid.");
        return;
      }

      var measurements = _deviceSessionManager.GetDeviceSession(message.DeviceId)
        .GetObject<List<BeltConveyorDataMessage>>();
      var sessionStoredMeasurementsCount = measurements.Count;
      measurements = _beltConveyorService.FilterMeasurementsByDate(measurements);
      var measurementEntities = measurements.Select(c => new BeltConveyorMeasurement
      {
        B1 = c.B1,
        B2 = c.B2,
        B3 = c.B3,
        B4 = c.B4,
        DateUtc = c.DateUtc,
        IncrementSum = c.IncrementalWeight,
        TotalSum = c.B1 + c.B2 + c.B3 + c.B4,
        ConfigId = c.DeviceId
      }).ToList();

      if (sessionStoredMeasurementsCount == message.TransferredItemsCount &&
          _beltConveyorService.SaveDataImport(message.DeviceId, measurementEntities))
      {
        _beltConveyorService.ConfirmDataImport(message.DeviceId);
      }
      else _beltConveyorService.RejectDataImport(message.DeviceId);

      _deviceSessionManager.GetDeviceSession(message.DeviceId)
        .GetObject<List<BeltConveyorDataMessage>>().Clear();
    }

    public void HandleBeltConveyorDataMessage(BeltConveyorDataMessage message)
    {
      if (message == null)
      {
        _logger.Error($"Cannot handle properly belt conveyor data import summary because message is not valid.");
        return;
      }

      var measurements = _deviceSessionManager.GetDeviceSession(message.DeviceId)
        .GetObject<List<BeltConveyorDataMessage>>();
      measurements.Add(message);
    }
  }
}
