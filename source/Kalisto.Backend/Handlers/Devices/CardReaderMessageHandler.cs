﻿using Kalisto.Backend.Services;
using Kalisto.Communication.Core.Logging;
using Kalisto.Configuration.Devices;
using Kalisto.Devices.CardReader.Handlers;
using Kalisto.Devices.CardReader.Messages;
using Kalisto.Logging;
using System.Linq;

namespace Kalisto.Backend.Handlers.Devices
{
  public class CardReaderMessageHandler : ICardReaderMessageHandler
  {
    private readonly ILogger _logger;

    private readonly ISystemEventLogger _eventLogger;

    private readonly ICardReaderService _cardReaderService;

    private readonly ServerDeviceConfiguration _deviceConfiguration;

    public CardReaderMessageHandler(ILogger logger, ISystemEventLogger eventLogger,
      ICardReaderService cardReaderService, ServerDeviceConfiguration deviceConfiguration)
    {
      _logger = logger;
      _eventLogger = eventLogger;
      _cardReaderService = cardReaderService;
      _deviceConfiguration = deviceConfiguration;
    }

    public void HandleCardReaderMessage(CardReaderMessage message)
    {
      var cardReaderConfiguration =
        _deviceConfiguration.CardReaders?.FirstOrDefault(c => c.Id == message.DeviceId && c.IsActive);
      if (cardReaderConfiguration == null)
      {
        _logger.Error($"Card reader with ID:{message.DeviceId} was not recognized as valid device.");
        return;
      }

      if (_cardReaderService.ProgrammingModeEnabled)
      {
        _cardReaderService.ForwardProgrammingData(message.Code);
        _logger.Info($"Card with code: {message.Code} has been scanned by reader with ID: {message.DeviceId} in order to create new card entry.");
        _eventLogger.LogEvent("CARD_SCANNED_NEW", new[] { message.Code, message.DeviceId.ToString() });
      }
      else
      {

        _cardReaderService.RaiseCardScannedEvent(message.DeviceId, cardReaderConfiguration.WeightMeterId, message.Code);
        _logger.Info($"Card with code: {message.Code} has been scanned by reader with ID: {message.DeviceId} in order to perform measurement.");
        _eventLogger.LogEvent("CARD_SCANNED_MEAS", new[] { message.Code, message.DeviceId.ToString() });
      }
    }
  }
}
