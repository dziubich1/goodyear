﻿using Kalisto.Backend.Services;
using Kalisto.Backend.StateMachine;
using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Configuration.Devices;
using Kalisto.Core;
using Kalisto.Devices.IOController.Handlers;
using Kalisto.Devices.IOController.Interfaces;
using Kalisto.Devices.IOController.Messages.Incoming;
using Kalisto.Devices.IOController.Messages.Outgoing;
using Kalisto.Frontend.Notifications;
using Kalisto.Frontend.Notifications.Enums;
using Kalisto.Logging;
using System.Collections.Generic;
using System.Linq;

namespace Kalisto.Backend.Handlers.Devices
{
  public class IoControllerMessageHandler : IExternalControllerMessageHandler
  {
    private readonly IWeightMeterControllerService _weightMeterControllerService;

    private readonly IIoControllerService _controllerService;

    private readonly IServerMessageProvider _messageProvider;

    private readonly IMessageBuilder _messageBuilder;

    private readonly ILogger _logger;

    private readonly IMeasurementStateMachineRegistry _measurementStateMachineRegistry;

    private readonly ServerDeviceConfiguration _configuration;

    private readonly TimeoutAction _timeoutAction;

    private bool _isAlive;

    public IoControllerMessageHandler(IServerMessageProvider messageProvider, IMessageBuilder messageBuilder,
      IIoControllerService controllerService, ILogger logger, IMeasurementStateMachineRegistry measurementStateMachineRegistry,
      IWeightMeterControllerService weightMeterControllerService, ServerDeviceConfiguration configuration)
    {
      _messageProvider = messageProvider;
      _messageBuilder = messageBuilder;
      _controllerService = controllerService;
      _logger = logger;
      _weightMeterControllerService = weightMeterControllerService;
      _measurementStateMachineRegistry = measurementStateMachineRegistry;
      _configuration = configuration;
      _timeoutAction = new TimeoutAction(OnBecameUnAlive, 5);
    }

    public void HandleOutputResetResponseMessage(OutputResetConfirmationMessage message)
    {
      _controllerService.ConfirmCommandResponse<ResetOutputMessage>();
    }

    public void HandleOutputStatesMessage(InputOutputStatesResponseMessage message)
    {
      if (message == null)
      {
        _logger.Error($"Cannot broadcast input states message because it is not valid.");
        return;
      }

      _controllerService.ConfirmCommandResponse<GetInputStatesMessage>();
      var payloads = new List<DeviceStatePayload>();
      var weightMeterIds = _configuration.WeightMeters.Where(c => c.IsActive).Select(c => c.Id).ToList();
      var ioControllers = _configuration.IoControllers.Where(c => c.IsActive && c.IsWeighingDeviceActive)
        .Select(c => c.Id);
      weightMeterIds.AddRange(ioControllers);
      foreach (var weightMeterId in weightMeterIds)
      {
        var outputStates = new[]
        {
          message.Output1,
          message.Output2,
          message.Output3,
          message.Output4,
          message.Output5,
          message.Output6,
          message.Output7,
          message.Output8
        };

        var barrierState = GetBarrierState(weightMeterId, outputStates);
        var trafficState = GetTrafficState(weightMeterId, outputStates);
        payloads.Add(new DeviceStatePayload
        {
          DeviceId = weightMeterId,
          BarrierState = barrierState,
          TrafficState = trafficState
        });
      }

      var outputStateNotification = _messageBuilder.CreateNew<DeviceStateNotification>()
        .Broadcasting()
        .WithProperty(c => c.Payloads, payloads)
        .Build();

      _messageProvider.BroadcastNotification(outputStateNotification);
    }

    public void HandleOutputSetResponseMessage(OutputSetConfirmationMessage message)
    {
      _controllerService.ConfirmCommandResponse<SetOutputMessage>();
    }

    public void HandleWeightMeterMessage(WeightMeterResponseMessage message)
    {
      if (message == null)
      {
        _logger.Error($"Cannot broadcast weight meter message because it is not valid.");
        return;
      }

      if (!_isAlive && message.Corrupted)
      {
        _measurementStateMachineRegistry.Restart();
        return;
      }

      if (!_isAlive)
        OnBecameAlive();
      else _timeoutAction.Bump();

      _weightMeterControllerService.UpdateWeightValue(message.DeviceId, message.Value, message.IsStable);
      _controllerService.ConfirmCommandResponse<GetWeightMeterValueMessage>();
      var forwardingMessage = _messageBuilder.CreateNew<WeighingValueUpdatedNotification>()
        .Broadcasting()
        .WithProperty(c => c.DeviceId, message.DeviceId)
        .WithProperty(c => c.Status, message.IsStable ? WeighingDeviceStatus.Stable : WeighingDeviceStatus.NotStable)
        .WithProperty(c => c.Value, message.Value)
        .Build();

      if (forwardingMessage.Value < 0)
        forwardingMessage.Status = WeighingDeviceStatus.Unknown;

      _messageProvider.BroadcastMessage(forwardingMessage);
      _logger.Debug($"Successfully broadcast weight meter message from device with ID: {message.DeviceId}");
    }

    private void OnBecameAlive()
    {
      _isAlive = true;
      _measurementStateMachineRegistry.Restart();

      _timeoutAction.StartTimeout();
    }

    private void OnBecameUnAlive()
    {
      _isAlive = false;
      _measurementStateMachineRegistry.Stop();
    }

    private BarrierState? GetBarrierState(int weightMeterId, bool[] states)
    {
      BarrierState? state = null;
      var ioController = _configuration.IoControllers.FirstOrDefault(c => c.IsActive);
      if (ioController == null)
        return state;


      var barriers = ioController.Barriers.Where(c => c.WeightMeterId == weightMeterId);
      foreach (var barrier in barriers)
      {
        var downState = states.ElementAtOrDefault(barrier.DownOutputNumber - 1);
        if (downState)
          state = BarrierState.Down;

        var upState = states.ElementAtOrDefault(barrier.UpOutputNumber - 1);
        if (upState)
          state = BarrierState.Up;
      }

      return state;
    }

    private TrafficState? GetTrafficState(int weightMeterId, bool[] states)
    {
      TrafficState? state = null;
      var ioController = _configuration.IoControllers.FirstOrDefault(c => c.IsActive);
      if (ioController == null)
        return state;


      var barriers = ioController.TrafficLights.Where(c => c.WeightMeterId == weightMeterId);
      foreach (var barrier in barriers)
      {
        var redState = states.ElementAtOrDefault(barrier.RedOutputNumber - 1);
        if (redState)
          state = TrafficState.Red;

        var greenState = states.ElementAtOrDefault(barrier.GreenOutputNumber - 1);
        if (greenState)
          state = TrafficState.Green;
      }

      return state;
    }
  }
}
