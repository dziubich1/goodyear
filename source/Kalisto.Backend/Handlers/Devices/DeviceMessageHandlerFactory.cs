﻿using Kalisto.Devices.BeltConveyorCounter.Handlers;
using Kalisto.Devices.BeltConveyorCounter.Messages.Base;
using Kalisto.Devices.CardReader.Handlers;
using Kalisto.Devices.CardReader.Messages;
using Kalisto.Devices.Core.Handlers;
using Kalisto.Devices.Core.Messages;
using Kalisto.Devices.IOController.Handlers;
using Kalisto.Devices.IOController.Messages.Base;
using Kalisto.Devices.WeightMeter.Handlers;
using Kalisto.Devices.WeightMeter.Messages;
using Unity;

namespace Kalisto.Backend.Handlers.Devices
{
  public interface IDeviceMessageHandlerFactory
  {
    IDeviceMessageHandler GetHandler(DeviceMessage message);
  }

  public class DeviceMessageHandlerFactory : IDeviceMessageHandlerFactory
  {
    private readonly IUnityContainer _container;

    public DeviceMessageHandlerFactory(IUnityContainer container)
    {
      _container = container;
    }

    public IDeviceMessageHandler GetHandler(DeviceMessage message)
    {
      if (message is BeltConveyorSilentMessage)
        return _container.Resolve<IBeltConveyorMessageHandler>();
      if (message is WeightMeterMessage)
        return _container.Resolve<IWeightMeterMessageHandler>();
      if (message is CardReaderMessage)
        return _container.Resolve<ICardReaderMessageHandler>();
      if (message is ExternalControllerSilentMessage)
        return _container.Resolve<IExternalControllerMessageHandler>();

      return null;
    }
  }
}
