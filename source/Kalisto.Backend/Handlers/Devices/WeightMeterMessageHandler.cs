﻿using Kalisto.Backend.Services;
using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Devices.WeightMeter.Handlers;
using Kalisto.Devices.WeightMeter.Messages;
using Kalisto.Frontend.Notifications;
using Kalisto.Frontend.Notifications.Enums;
using Kalisto.Logging;

namespace Kalisto.Backend.Handlers.Devices
{
  public class WeightMeterMessageHandler : IWeightMeterMessageHandler
  {
    private readonly IWeightMeterControllerService _weightMeterControllerService;

    private readonly IServerMessageProvider _messageProvider;

    private readonly IMessageBuilder _messageBuilder;

    private readonly ILogger _logger;

    public WeightMeterMessageHandler(IServerMessageProvider messageProvider, IMessageBuilder messageBuilder, ILogger logger, IWeightMeterControllerService weightMeterControllerService)
    {
      _messageProvider = messageProvider;
      _messageBuilder = messageBuilder;
      _logger = logger;
      _weightMeterControllerService = weightMeterControllerService;
    }

    public void HandleWeightMeterMessage(WeightMeterMessage message)
    {
      if (message == null)
      {
        _logger.Error($"Cannot broadcast weight meter message because it is not valid.");
        return;
      }

      _weightMeterControllerService.UpdateWeightValue(message.DeviceId, message.Value, message.IsStable);
      var forwardingMessage = _messageBuilder.CreateNew<WeighingValueUpdatedNotification>()
        .Broadcasting()
        .WithProperty(c => c.DeviceId, message.DeviceId)
        .WithProperty(c => c.Status, message.IsStable ? WeighingDeviceStatus.Stable : WeighingDeviceStatus.NotStable)
        .WithProperty(c => c.Value, message.Value)
        .Build();

      if (forwardingMessage.Value < 0)
        forwardingMessage.Status = WeighingDeviceStatus.Unknown;

      _messageProvider.BroadcastMessage(forwardingMessage);
      _logger.Debug($"Successfully broadcast weight meter message from device with ID: {message.DeviceId}");
    }
  }
}
