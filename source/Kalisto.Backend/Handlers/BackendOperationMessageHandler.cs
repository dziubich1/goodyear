﻿using Kalisto.Backend.Configuration;
using Kalisto.Backend.DataAccess.Model;
using Kalisto.Communication.Core.Exceptions;
using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Frontend.Notifications;
using Kalisto.Logging;
using System;
using System.Threading.Tasks;

namespace Kalisto.Backend.Handlers
{
  public class BackendOperationMessageHandler : MessageHandlerBase<BackendDbOperationMessage>
  {
    private readonly ILogger _logger;

    private readonly IServerMessageProvider _messageProvider;

    private readonly IOperationMessageHandlerRegistry _operationMessageRegistry;

    private readonly IDbContextFactory _dbContextFactory;

    private readonly BackendConfiguration _configuration;

    public BackendOperationMessageHandler(ILogger logger, IMessageBuilder messageBuilder, IServerMessageProvider messageProvider,
      IOperationMessageHandlerRegistry commandMessageRegistry, IDbContextFactory dbContextFactory,
      BackendConfiguration backendConfiguration)
      : base(messageBuilder)
    {
      _logger = logger;
      _messageProvider = messageProvider;
      _operationMessageRegistry = commandMessageRegistry;
      _dbContextFactory = dbContextFactory;
      _configuration = backendConfiguration;
    }

    protected override void HandleMessage(BackendDbOperationMessage message)
    {
      Exception exception = null;
      object result = null;
      var command = message;
      var dbContext = _dbContextFactory.CreateDbContext(_configuration.DbConnection.ConnectionString);

      try
      {
        result = _operationMessageRegistry.ExecuteOperation(dbContext, command, message.PartnerId);
      }
      catch (ExceptionBase e)
      {
        var logMessage = $"Error occurred while executing database operation. Exception was thrown: {e.Message}. StackTrace: {e.StackTrace}.";
        if (e.InnerException != null)
          logMessage += $" Inner exception: {e.InnerException.Message}.";

        _logger.Error(logMessage);
        exception = e;
      }
      catch (Exception e)
      {
        var logMessage = $"Error occurred while executing database operation. Exception was thrown: {e.Message}. StackTrace: {e.StackTrace}.";
        if (e.InnerException != null)
          logMessage += $" Inner exception: {e.InnerException.Message}.";

        _logger.Error(logMessage);

        if (e is InvalidOperationException && e.Source == "EntityFramework")
          exception = new DbException();
        else exception = new Exception(e.Message);
      }

      if (!string.IsNullOrEmpty(message.PartnerId))
      {
        var response = MessageBuilder.CreateNew<BackendDbOperationResponseMessage>()
          .BasedOn(message)
          .WithProperty(c => c.Result, result)
          .WithProperty(c => c.Exception, exception)
          .Build();

        _messageProvider.SendMessage(response);
      }

      if (!(command is IAssociatedCommand cmd))
        return;

      // inform all endpoints about db change
      var notification = MessageBuilder.CreateNew<DbUpdatedNotification>()
        .Broadcasting()
        .WithProperty(c => c.AssociatedCommandMetadata, Activator.CreateInstance(cmd.GetType()))
        .Build();

      Task.Run(async () =>
      {
        // delay notification a little bit to avoid race condition
        await Task.Delay(100);
        _messageProvider.BroadcastNotification(notification);
      });
    }
  }
}
