﻿using Kalisto.Backend.Configuration;
using Kalisto.Backend.DataAccess.Firebird;
using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.DataAccess.Model.Logging;
using Kalisto.Backend.DataAccess.MsSql;
using Kalisto.Backend.Enum;
using Kalisto.Backend.Handlers;
using Kalisto.Backend.Handlers.Devices;
using Kalisto.Backend.Jobs;
using Kalisto.Backend.Logging;
using Kalisto.Backend.Operations.BeltConveyorMeasurements.Commands;
using Kalisto.Backend.Operations.BeltConveyorMeasurements.Queries;
using Kalisto.Backend.Operations.Cards.Commands;
using Kalisto.Backend.Operations.Cards.Queries;
using Kalisto.Backend.Operations.Cards.Services;
using Kalisto.Backend.Operations.Cargoes.Commands;
using Kalisto.Backend.Operations.Cargoes.Queries;
using Kalisto.Backend.Operations.Contractors.Commands;
using Kalisto.Backend.Operations.Contractors.Queries;
using Kalisto.Backend.Operations.DbEventLogs.Queries;
using Kalisto.Backend.Operations.Drivers.Commands;
using Kalisto.Backend.Operations.Drivers.Queries;
using Kalisto.Backend.Operations.Measurements.Commands;
using Kalisto.Backend.Operations.Measurements.Queries;
using Kalisto.Backend.Operations.Shell.Commands;
using Kalisto.Backend.Operations.Shell.Queries;
using Kalisto.Backend.Operations.SystemEventLogs.Queries;
using Kalisto.Backend.Operations.Users.Commands;
using Kalisto.Backend.Operations.Users.Queries;
using Kalisto.Backend.Operations.Vehicles.Commands;
using Kalisto.Backend.Operations.Vehicles.Queries;
using Kalisto.Backend.Services;
using Kalisto.Backend.StateMachine;
using Kalisto.Communication.Core;
using Kalisto.Communication.Core.Communication;
using Kalisto.Communication.Core.Logging;
using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Communication.Core.Registries;
using Kalisto.Communication.Core.Services;
using Kalisto.Communication.RabbitMq;
using Kalisto.Configuration.CompanyInfo;
using Kalisto.Configuration.Devices;
using Kalisto.Configuration.Permissions;
using Kalisto.Core;
using Kalisto.Devices.BeltConveyorCounter.Handlers;
using Kalisto.Devices.Camera.Drivers;
using Kalisto.Devices.Camera.Services;
using Kalisto.Devices.CardReader.Handlers;
using Kalisto.Devices.Core;
using Kalisto.Devices.IOController.Handlers;
using Kalisto.Devices.WeightMeter.Handlers;
using Kalisto.Hmi.Dialogs.Common;
using Kalisto.Licensing;
using Kalisto.Mailing;
using Kalisto.Mailing.Configuration;
using Kalisto.Mailing.Notifications;
using Kalisto.Mailing.Notifications.Configuration;
using Kalisto.Mailing.Notifications.WeighingList;
using nQuant;
using Quartz;
using Quartz.Impl;
using Quartz.Impl.Triggers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Kalisto.Backend.Operations.Dashboard.Commands;
using Kalisto.Backend.Operations.Dashboard.Queries;
using Kalisto.Communication.Local;
using Kalisto.Devices.IOController.Interfaces;
using Unity;
using Unity.Injection;
using Unity.Lifetime;
using DeviceConfiguration = Kalisto.Configuration.Devices.DeviceConfiguration;
using Kalisto.Configuration.MeasurementExportSettings;

namespace Kalisto.Backend
{
  public class LocalServerEndpoint : EndpointModule<BackendConfiguration>
  {
    private readonly PermissionRulesConfigurationManager _ruleConfigurationManager;

    private readonly SmtpSettingsConfigurationManager _smtpSettingsConfigurationManager;

    private readonly NotificationSettingsConfigurationManager _notificationSettingsConfigurationManager;

    private readonly ServerDeviceConfigurationManager _serverDeviceConfigurationManager;

    private readonly CompanyInfoConfigurationManager _companyInfoConfigurationManager;

    private readonly MeasurementExportConfigurationManager _measurementExportConfigurationManager;

    private IMeasurementStateMachineRegistry _stateMachineRegistry;

    private PermissionRules _permissionRulesConfiguration;

    public LocalServerEndpoint(IUnityContainer container, string domainName, string appName)
      : base(container, domainName, appName)
    {
      _ruleConfigurationManager = new PermissionRulesConfigurationManager(domainName, appName);
      _smtpSettingsConfigurationManager = new SmtpSettingsConfigurationManager(domainName, appName);
      _notificationSettingsConfigurationManager = new NotificationSettingsConfigurationManager(domainName, appName);
      _serverDeviceConfigurationManager = new ServerDeviceConfigurationManager(domainName, appName);
      _companyInfoConfigurationManager = new CompanyInfoConfigurationManager(domainName, appName);
      _measurementExportConfigurationManager = new MeasurementExportConfigurationManager(domainName, appName);
    }

    protected override void Bind(IUnityContainer container)
    {
      base.Bind(container);

      container.RegisterType<BackendConfiguration>(new InjectionFactory(c => Configuration));
      container.RegisterType<PermissionRules>(new InjectionFactory(c =>
      {
        if (_permissionRulesConfiguration != null)
          return _permissionRulesConfiguration;

        _ruleConfigurationManager.Save(PermissionRules.Default);
        _permissionRulesConfiguration = _ruleConfigurationManager.Load();
        return _permissionRulesConfiguration;
      }));
      container.RegisterType<RabbitMqConnectionConfiguration>(new InjectionFactory(c =>
        Configuration.RabbitMqConnection));

      if (Configuration.DbConnection.DatabaseType == DbTypeEnum.Firebird)
      {
        container.RegisterType<IDbContext, FirebirdDbContext>("stringConstructor",
          new InjectionConstructor(typeof(string)));
      }
      else if (Configuration.DbConnection.DatabaseType == DbTypeEnum.MsSql)
      {
        container.RegisterType<IDbContext, MsSqlDbContext>("stringConstructor",
          new InjectionConstructor(typeof(string)));
      }

      container.RegisterType<ISubscriberRegistry, ServerSubscriberRegistry>(new ContainerControlledLifetimeManager());
      container.RegisterType<IMessageProviderRegistry, ServerProviderRegistry>(
        new ContainerControlledLifetimeManager());
      container.RegisterType<IServerMessageProvider, ServerMessageProvider>(new ContainerControlledLifetimeManager());

      container.RegisterType<PartnerRegistry>(new ContainerControlledLifetimeManager());
      container.RegisterType<IPartnerRegistry, PartnerRegistry>();
      container.RegisterType<IPartnerRegistryReader, PartnerRegistry>();

      container.RegisterType<IMessageReceiver, MessageLocalReceiver>();
      container.RegisterType<IMessageSender, MessageLocalSender>();

      container.RegisterType<ICommunicationComponentFactory, CommunicationComponentFactory>();
      container.RegisterType<IDbContextFactory, DbContextFactory>();
      container.RegisterType<MessageHandlerFactory>();

      container.RegisterType<RegistrationMessageHandler>(new ContainerControlledLifetimeManager());
      container.RegisterType<HeartbeatMessageHandler>(new ContainerControlledLifetimeManager());
      container.RegisterType<BackendOperationMessageHandler>(new ContainerControlledLifetimeManager());
      container.RegisterType<ClientLogoRequestHandler>(new ContainerControlledLifetimeManager());
      container.RegisterSingleton<ServerImageRequestHandler>();
      container.RegisterSingleton<ServerImageThumbnailRequestHandler>();
      container.RegisterType<IOperationMessageHandlerRegistry, OperationMessageHandlerRegistry>(new ContainerControlledLifetimeManager());

      container.RegisterType<IMessageBuilder, MessageBuilder>();
      container.RegisterType<IBeltConveyorService, BeltConveyorService>();

      // Logging
      container.RegisterSingleton<ISystemEventLogger, SystemEventLogger>();
      container.RegisterType<IDbInsertEventLogger, DbInsertEventLogger>();
      container.RegisterType<IDbUpdateEventLogger, DbUpdateEventLogger>();
      container.RegisterType<IDbDeleteEventLogger, DbDeleteEventLogger>();

      // Notifications
      container.RegisterType<SmtpSettingsConfigurationManager>(new InjectionFactory(c =>
        _smtpSettingsConfigurationManager));
      container.RegisterType<NotificationSettingsConfigurationManager>(new InjectionFactory(c =>
        _notificationSettingsConfigurationManager));
      container.RegisterSingleton<ISmtpSettingsService, SmtpSettingsService>();
      container.RegisterSingleton<IEmailService, EmailService>();
      container.RegisterSingleton<INotificationService, NotificationService>();
      container.RegisterType<WeighingListNotification>();
      container.RegisterType<IWeighingListService, WeighingListService>();
      container.RegisterType<IUnitTypeService>(new InjectionFactory(c =>
        new UnitTypeService(new UnitPreferencesService())));

      // Licensing
      container.RegisterSingleton<CurrentUserRegistryAccess>();
      container.RegisterType<ISystemRegistryReader, CurrentUserRegistryAccess>();
      container.RegisterType<ISystemRegistryWriter, CurrentUserRegistryAccess>();
      container.RegisterType<ILicenseRegistryAccess, LicenseRegistryService>();
      container.RegisterType<ILicenseInfo, LicenseService>();
      container.RegisterType<IMachineInfo, MachineInfoService>();

      // Company info
      container.RegisterType<CompanyInfoConfigurationManager>(new InjectionFactory(c =>
        _companyInfoConfigurationManager));

      //Measurement export
      container.RegisterType<MeasurementExportConfigurationManager>(new InjectionFactory(c =>
        _measurementExportConfigurationManager));

      // register command/query handlers
      container.RegisterType<DbOperationHandlerFactory>();
      container.RegisterType<GetAllUsersQueryHandler>();
      container.RegisterType<CreateUserCommandHandler>();
      container.RegisterType<UpdateUserCommandHandler>();
      container.RegisterType<DeleteUserCommandHandler>();
      container.RegisterType<LoginCommandHandler>();
      container.RegisterType<GetSmtpSettingsQueryHandler>();
      container.RegisterType<StoreSmtpSettingsCommandHandler>();
      container.RegisterType<GetNotificationSettingsQueryHandler>();
      container.RegisterType<StoreNotificationSettingsCommandHandler>();
      container.RegisterType<GenerateLicensePreKeyCommandHandler>();
      container.RegisterType<AssignLicenseKeyCommandHandler>();
      container.RegisterType<GetSystemEventLogsQueryHandler>();
      container.RegisterType<GetDbEventLogsQueryHandler>();
      container.RegisterType<GetAllContractorsQueryHandler>();
      container.RegisterType<CreateContractorCommandHandler>();
      container.RegisterType<UpdateContractorCommandHandler>();
      container.RegisterType<DeleteContractorCommandHandler>();
      container.RegisterType<CreateCargoCommandHandler>();
      container.RegisterType<UpdateCargoCommandHandler>();
      container.RegisterType<DeleteCargoCommandHandler>();
      container.RegisterType<GetAllCargoesQueryHandler>();
      container.RegisterType<GetAllDriversQueryHandler>();
      container.RegisterType<CreateDriverCommandHandler>();
      container.RegisterType<DeleteDriverCommandHandler>();
      container.RegisterType<UpdateDriverCommandHandler>();
      container.RegisterType<GetMeasurementsQueryHandler>();
      container.RegisterType<GetAllCardsQueryHandler>();
      container.RegisterType<GetMaxCardNumberQueryHandler>();
      container.RegisterType<CreateCardCommandHandler>();
      container.RegisterType<UpdateCardCommandHandler>();
      container.RegisterType<DeleteCardCommandHandler>();
      container.RegisterType<GetAllVehiclesQueryHandler>();
      container.RegisterType<CreateVehicleCommandHandler>();
      container.RegisterType<DeleteVehicleCommandHandler>();
      container.RegisterType<UpdateVehicleCommandHandler>();
      container.RegisterType<GetFilteredMeasurementsQueryHandler>();
      container.RegisterType<UpdateVehicleTareCommandHandler>();
      container.RegisterType<GetNotCompletedMeasurementsQueryHandler>();
      container.RegisterType<CancelMeasurementCommandHandler>();
      container.RegisterType<GetNextMeasurementNumberQueryHandler>();
      container.RegisterType<CreateSingleMeasurementCommandHandler>();
      container.RegisterType<CreateDoubleMeasurementCommandHandler>();
      container.RegisterType<CompleteDoubleMeasurementCommandHandler>();
      container.RegisterType<GetAllSemiTrailersQueryHandler>();
      container.RegisterType<GetMeasurementByIdQueryHandler>();
      container.RegisterType<DeleteMeasurementByIdCommandHandler>();
      container.RegisterType<GetSequenceNumberByDateQueryHandler>();
      container.RegisterType<LogoutCommandHandler>();
      container.RegisterType<UpdateMeasurementCommandHandler>();
      container.RegisterType<GetDeviceConfigurationQueryHandler>();
      container.RegisterType<GetMeasurementPhotosByIdQueryHandler>();
      container.RegisterType<GetBeltConveyorMeasurementsQueryHandler>();
      container.RegisterType<UpdateBeltConveyorMeasurementCommandHandler>();
      container.RegisterType<GetCompanyInfoConfigurationQueryHandler>();
      container.RegisterType<StoreCompanyInfoConfigurationCommandHandler>();
      container.RegisterType<GetAllCargoesInventoryQueryHandler>();
      container.RegisterType<GetMeasurementExportSettingsQueryHandler>();
      container.RegisterType<StoreMeasurementExportSettingsCommandHandler>();
      container.RegisterType<EnableDisableCardReaderProgrammingModeCommandHandler>();
      container.RegisterType<CreateOrCompleteMeasurementWithCardCommandHandler>();
      container.RegisterType<ChangeAutoModeCommandHandler>();
      container.RegisterType<RaiseOrLeaveBarrierCommandHandler>();
      container.RegisterType<TurnGreenOrRedLightCommandHandler>();
      container.RegisterType<GetStateMachineStatusQueryHandler>();
      container.RegisterType<GetCargoInventoryChartReportDataQueryHandler>();
      container.RegisterType<RequestOutputStatesCommandHandler>();

      // devices
      container.RegisterType<ServerDeviceConfigurationManager>(new InjectionFactory(c =>
        _serverDeviceConfigurationManager));
      container.RegisterSingleton<ServerDeviceConfiguration>(new InjectionFactory(c => _serverDeviceConfigurationManager.Load()));
      container.RegisterType<ICameraService, CameraService>();
      container.RegisterType<ICameraDriver, CameraDriver>();
      container.RegisterType<IWebImageService, WebImageService>();
      container.RegisterType<IMeasurementPhotoService, MeasurementPhotoService>();
      container.RegisterSingleton<IDeviceMessageHandlerFactory, DeviceMessageHandlerFactory>();
      container.RegisterSingleton<IWeightMeterMessageHandler, WeightMeterMessageHandler>();
      container.RegisterSingleton<IBeltConveyorMessageHandler, BeltConveyorMessageHandler>();
      container.RegisterSingleton<ICardReaderMessageHandler, CardReaderMessageHandler>();
      container.RegisterSingleton<IExternalControllerMessageHandler, IoControllerMessageHandler>();
      container.RegisterSingleton<IIoControllerService, IoControllerService>();
      container.RegisterSingleton<CardReaderService>();
      container.RegisterSingleton<ICardReaderService, CardReaderService>();
      container.RegisterSingleton<ICardService, CardReaderService>();
      container.RegisterSingleton<IDeviceSessionManager, DeviceSessionManager>();
      container.RegisterSingleton<IWeightMeterControllerService, WeightMeterControllerService>();
      container.RegisterSingleton<ITrafficLightsService, TrafficLightsService>();
      container.RegisterSingleton<IBarrierService, BarrierService>();

      // state machine
      container.RegisterSingleton<IMeasurementStateMachineRegistry, MeasurementStateMachineRegistry>();
      container.RegisterSingleton<IMeasurementStateMachineFactory, MeasurementStateMachineFactory>();
      container.RegisterSingleton<IMeasurementStateMachineEventHandler, MeasurementStateMachineEventHandler>();

      // utils
      container.RegisterType<WuQuantizer>();
      container.RegisterType<IXmlExporterService, XmlExporterService>();
    }

    protected override async Task ScheduleJobs(IScheduler scheduler)
    {
      await base.ScheduleJobs(scheduler);
      await scheduler.ScheduleJob(
        new JobDetailImpl(nameof(PartnerRegistryCleanJob), typeof(PartnerRegistryCleanJob)),
        new CalendarIntervalTriggerImpl("Trigger_1", IntervalUnit.Second, 15));

      await scheduler.ScheduleJob(
        new JobDetailImpl(nameof(SendNotificationsJob), typeof(SendNotificationsJob)),
        new CalendarIntervalTriggerImpl("Trigger_2", IntervalUnit.Hour, 1));

      var ioControllerConfig = _serverDeviceConfigurationManager.Load()?.IoControllers.FirstOrDefault(c => c.IsActive);
      if (ioControllerConfig != null)
      {
        if (ioControllerConfig.IsWeighingDeviceActive)
        {
          await scheduler.ScheduleJob(
            new JobDetailImpl(nameof(IoControllerWeightRequestJob), typeof(IoControllerWeightRequestJob)),
            new CalendarIntervalTriggerImpl("Trigger_4", IntervalUnit.Second, 1));
        }
      }

      // VER_SPEC: belt conveyor devices
      // await scheduler.ScheduleJob(
      //  new JobDetailImpl(nameof(BeltConveyorCounterImportRequestJob), typeof(BeltConveyorCounterImportRequestJob)),
      //  new CalendarIntervalTriggerImpl("Trigger_3", IntervalUnit.Day, 1));
    }

    protected override void Resolve(IUnityContainer container)
    {
      base.Resolve(container);

      var assemblyVerifier = container.Resolve<IAssemblyVerifier>();
      var areAssembliesConsistent = assemblyVerifier.CheckAssemblyConsistency();
      if (!areAssembliesConsistent)
        throw new Exception("Server endpoint assemblies are not consistent.");

      var notificationService = container.Resolve<INotificationService>();
      notificationService.RegisterNotificationType<WeighingListNotification>();

      Logger.Info(
        $"Server endpoint assemblies OK - using version {assemblyVerifier.GetAssembliesVersion()}");

      _stateMachineRegistry = container.Resolve<IMeasurementStateMachineRegistry>();
      InitializeStateMachines(container);
    }

    protected override void OnInitialized()
    {
      base.OnInitialized();

      ValidateConfigurations();
      Logger.Info("Server endpoint has been initialized.");
    }

    protected override void OnDisconnect()
    {
      base.OnDisconnect();

      _stateMachineRegistry.Stop();
    }

    private void ValidateConfigurations()
    {
      Logger.Info("Validating server configuration.");
      var deviceConfig = _serverDeviceConfigurationManager.Load();
      if (deviceConfig == null)
      {
        Logger.Error("Devices configuration is invalid. Could not load it from file.");
        Environment.Exit(0);
        return;
      }

      var deviceConfigs = deviceConfig.GetAllDevices();
      var uniqueIdsCount = deviceConfigs.Select(c => c.Id).Distinct().Count();
      if (uniqueIdsCount != deviceConfigs.Count)
      {
        Logger.Error("Devices configuration is invalid. ID uniqueness was not fulfilled.");
        Environment.Exit(0);
        return;
      }

      Logger.Info("Server configuration is valid.");
    }

    private void InitializeStateMachines(IUnityContainer container)
    {
      var weighingMetersConfigsIds = _serverDeviceConfigurationManager.Load()?.WeightMeters.Where(c => c.IsActive && c.IsStateMachineActive).Select(c => c.Id).ToList();
      var ioWeightMetersConfig = _serverDeviceConfigurationManager.Load()?.IoControllers
        .FirstOrDefault(c => c.IsWeighingDeviceActive);

      if (weighingMetersConfigsIds == null && ioWeightMetersConfig != null)
        weighingMetersConfigsIds = new List<int>();

      if (ioWeightMetersConfig != null)
        weighingMetersConfigsIds.Add(ioWeightMetersConfig.Id);

      if (weighingMetersConfigsIds == null || !weighingMetersConfigsIds.Any())
      {
        Logger.Info("No active state machines defined in configuration file.");
        return;
      }

      var smFactory = container.Resolve<IMeasurementStateMachineFactory>();
      var weightControllerService = container.Resolve<IWeightMeterControllerService>();
      foreach (var weighingConfigId in weighingMetersConfigsIds)
      {
        var stateMachine = smFactory.Create(weighingConfigId);
        _stateMachineRegistry.Register(stateMachine);
        weightControllerService.Register(weighingConfigId);
      }

      _stateMachineRegistry.Initialize();
      _stateMachineRegistry.Start();
      Logger.Info(
        $"State machines for weighing meter devices has been initialized and started (count: {weighingMetersConfigsIds.Count}).");
    }
  }
}

