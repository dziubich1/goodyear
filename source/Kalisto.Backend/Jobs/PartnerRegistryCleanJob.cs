﻿using System.Threading.Tasks;
using Kalisto.Communication.Core.Registries;
using Kalisto.Logging;
using Quartz;

namespace Kalisto.Backend.Jobs
{
  public class PartnerRegistryCleanJob : IJob
  {
    private readonly ILogger _logger;
    private readonly IPartnerRegistry _partnerRegistry;

    public PartnerRegistryCleanJob(ILogger logger, IPartnerRegistry partnerRegistry)
    {
      _logger = logger;
      _partnerRegistry = partnerRegistry;
    }

    public Task Execute(IJobExecutionContext context)
    {
      _partnerRegistry.UnregisterInactivePartners();
      _logger.Debug("PartnerRegistry job has been completed.");

      return Task.FromResult(string.Empty);
    }
  }
}
