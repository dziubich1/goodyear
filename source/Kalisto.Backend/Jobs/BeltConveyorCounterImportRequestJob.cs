﻿using Kalisto.Backend.Services;
using Kalisto.Configuration.Devices;
using Kalisto.Logging;
using Quartz;
using System.Linq;
using System.Threading.Tasks;

namespace Kalisto.Backend.Jobs
{
  public class BeltConveyorCounterImportRequestJob : IJob
  {
    private readonly ILogger _logger;

    private readonly IBeltConveyorService _beltConveyorService;

    private readonly ServerDeviceConfigurationManager _configurationManager;

    public BeltConveyorCounterImportRequestJob(ILogger logger, IBeltConveyorService beltConveyorService, ServerDeviceConfigurationManager configurationManager)
    {
      _logger = logger;
      _beltConveyorService = beltConveyorService;
      _configurationManager = configurationManager;
    }

    public Task Execute(IJobExecutionContext context)
    {
      _logger.Debug("Sending belt conveyor counter import request message.");

      var beltConveyorConfigs = _configurationManager.Load().BeltConveyorCounters.Where(c => c.IsActive).ToList();
      if (!beltConveyorConfigs.Any())
      {
        _logger.Info("There is no active belt conveyor counters");
        return Task.FromResult(string.Empty);
      }

      foreach (var cfg in beltConveyorConfigs)
        _beltConveyorService.SendDataImportRequest(cfg.Id);

      return Task.FromResult(string.Empty);
    }
  }
}
