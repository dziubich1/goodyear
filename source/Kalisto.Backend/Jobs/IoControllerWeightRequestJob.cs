﻿using Kalisto.Backend.Services;
using Kalisto.Configuration.Devices;
using Quartz;
using System.Linq;
using System.Threading.Tasks;

namespace Kalisto.Backend.Jobs
{
  public class IoControllerWeightRequestJob : IJob
  {
    private readonly IIoControllerService _ioControllerService;

    private readonly ServerDeviceConfiguration _configuration;

    public IoControllerWeightRequestJob(IIoControllerService ioControllerService, ServerDeviceConfiguration configuration)
    {
      _ioControllerService = ioControllerService;
      _configuration = configuration;
    }

    public async Task Execute(IJobExecutionContext context)
    {
      var device = _configuration.IoControllers.FirstOrDefault(c => c.IsActive && c.IsWeighingDeviceActive);
      if (device == null)
        return;

      _ioControllerService.SendGetWeightCommand(device.Id);

      await Task.FromResult(0);
    }
  }
}
