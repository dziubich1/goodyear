﻿using System.Threading.Tasks;
using Kalisto.Logging;
using Kalisto.Mailing.Notifications;
using Quartz;

namespace Kalisto.Backend.Jobs
{
  public class SendNotificationsJob : IJob
  {
    private readonly ILogger _logger;

    private readonly INotificationService _notificationService;

    public SendNotificationsJob(ILogger logger, INotificationService notificationService)
    {
      _logger = logger;
      _notificationService = notificationService;
    }

    public async Task Execute(IJobExecutionContext context)
    {
      if (_notificationService.ShouldSendNotification())
        await _notificationService.SendNotificationsAsync();

      _logger.Debug("SendNotifications job has been completed.");
    }
  }
}
