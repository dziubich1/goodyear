﻿using Kalisto.Communication.Core.Communication;
using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Registries;

namespace Kalisto.Backend
{
  public class ServerProviderRegistry : MessageProviderRegistry
  {
    private readonly ICommunicationComponentFactory _communicationComponentFactory;

    private readonly IPartnerRegistryReader _partnerRegistryReader;

    public ServerProviderRegistry(ICommunicationComponentFactory communicationComponentFactory,
      IPartnerRegistryReader partnerRegistryReader)
    {
      _communicationComponentFactory = communicationComponentFactory;

      _partnerRegistryReader = partnerRegistryReader;
      _partnerRegistryReader.PartnerRegistryEntryCreated += OnPartnerRegistryEntryCreated;
      _partnerRegistryReader.PartnerRegistryEntryRemoved += OnPartnerRegistryEntryRemoved;
    }

    public override void Initialize()
    {
      // server endpoint does not have any providers by default
    }

    private void OnPartnerRegistryEntryCreated(string partnerId, ClientType type)
    {
      // additional security check
      if (!_partnerRegistryReader.IsPartnerActive(partnerId))
      {
        return;
      }

      var sender = _communicationComponentFactory.GetServerMessageSender(partnerId);
      RegisterProvider(partnerId, sender, type);
    }

    private void OnPartnerRegistryEntryRemoved(string partnerId)
    {
      // additional security check
      if (_partnerRegistryReader.IsPartnerActive(partnerId))
      {
        return;
      }

      UnregisterProvider(partnerId);
    }
  }
}
