﻿using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Frontend.Notifications;
using Kalisto.Hmi.Core.Communication;
using Kalisto.Hmi.Core.Services;
using System;
using System.Threading.Tasks;
using Kalisto.Backend.Operations.Dashboard.Commands;
using Kalisto.Backend.Operations.Dashboard.Queries;

namespace Kalisto.Hmi.Dialogs.Dashboard.Services
{
  public interface IDashboardService
  {
    event Action<WeighingValueUpdatedNotification> WeighingValueUpdated;

    event Action<DeviceStateNotification> DeviceStatesUpdated;

    Task<bool> ChangeAutoMode(int deviceId, bool autoModeEnabled);

    Task<bool> RaiseOrLeaveBarrier(int deviceId, bool barrierUp);

    Task<bool> TurnGreenOrRedLight(int deviceId, bool greenLightOn);

    Task<bool> IsStateMachineRunning(int deviceId);

    Task RequestOutputStatesInfoAsync();
  }

  public class DashboardService : BackendOperationService, IDashboardService
  {
    public event Action<WeighingValueUpdatedNotification> WeighingValueUpdated;

    public event Action<DeviceStateNotification> DeviceStatesUpdated;

    public DashboardService(ClientInfo clientInfo, IMessageBuilder messageBuilder, IFrontendMessageBridge messageBridge, IDialogService dialogService)
      : base(clientInfo, messageBuilder, messageBridge, dialogService)
    {
      RegisterNotificationHandler<WeighingValueUpdatedNotification>(OnWeighingValueUpdated);
      RegisterNotificationHandler<DeviceStateNotification>(OnDeviceStatesUpdated);
    }

    public async Task<bool> ChangeAutoMode(int deviceId, bool autoModeEnabled)
    {
      var message = MessageBuilder.CreateNew<ChangeAutoModeCommand>()
        .BasedOn(ClientInfo)
        .WithProperty(c => c.AutoModeEnabled, autoModeEnabled)
        .WithProperty(c => c.WeightMeterDeviceId, deviceId)
        .Build();

      var result = await MessageBridge.SendMessageWithResultAsync<bool>(message);
      return result?.Response ?? false;
    }

    public async Task<bool> RaiseOrLeaveBarrier(int deviceId, bool barrierUp)
    {
      var message = MessageBuilder.CreateNew<RaiseOrLeaveBarrierCommand>()
        .BasedOn(ClientInfo)
        .WithProperty(c => c.BarrierUp, barrierUp)
        .WithProperty(c => c.WeightMeterDeviceId, deviceId)
        .Build();

      var result = await MessageBridge.SendMessageWithResultAsync<bool>(message);
      return result?.Response ?? false;
    }

    public async Task<bool> TurnGreenOrRedLight(int deviceId, bool greenLightOn)
    {
      var message = MessageBuilder.CreateNew<TurnGreenOrRedLightCommand>()
        .BasedOn(ClientInfo)
        .WithProperty(c => c.GreenLightEnabled, greenLightOn)
        .WithProperty(c => c.WeightMeterDeviceId, deviceId)
        .Build();

      var result = await MessageBridge.SendMessageWithResultAsync<bool>(message);
      return result?.Response ?? false;
    }

    public async Task<bool> IsStateMachineRunning(int deviceId)
    {
      var message = MessageBuilder.CreateNew<GetStateMachineStatusQuery>()
        .BasedOn(ClientInfo)
        .WithProperty(c => c.DeviceId, deviceId)
        .Build();

      var result = await MessageBridge.SendMessageWithResultAsync<bool>(message);
      return result?.Response ?? false;
    }

    private void OnWeighingValueUpdated(object obj)
    {
      var notification = obj as WeighingValueUpdatedNotification;
      if (notification == null)
        return;

      WeighingValueUpdated?.Invoke(notification);
    }

    private void OnDeviceStatesUpdated(object obj)
    {
      var notification = obj as DeviceStateNotification;
      if (notification == null)
        return;

      DeviceStatesUpdated?.Invoke(notification);
    }

    public async Task RequestOutputStatesInfoAsync()
    {
      var message = MessageBuilder.CreateNew<RequestOutputStatesCommand>()
        .BasedOn(ClientInfo)
        .Build();

      await MessageBridge.SendMessageWithResultAsync<bool>(message);
    }
  }
}
