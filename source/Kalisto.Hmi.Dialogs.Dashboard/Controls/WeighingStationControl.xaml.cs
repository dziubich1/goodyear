﻿using Kalisto.Hmi.Dialogs.Dashboard.Models;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;

namespace Kalisto.Hmi.Dialogs.Dashboard.Controls
{
  /// <summary>
  /// Interaction logic for WeighingStationControl.xaml
  /// </summary>
  public partial class WeighingStationControl : UserControl
  {
    private readonly System.Windows.Threading.DispatcherTimer _timer
      = new System.Windows.Threading.DispatcherTimer();

    private Image _clickedImage;

    public WeighingStationControl()
    {
      InitializeComponent();
      _timer.Interval = TimeSpan.FromSeconds(0.2); //wait for the other click for 200ms
      _timer.Tick += OnTimerTick;
    }

    public static readonly DependencyProperty StationInfoProperty =
      DependencyProperty.Register("StationInfo", typeof(WeighingStationInfo),
        typeof(WeighingStationControl), new PropertyMetadata(null));

    public WeighingStationInfo StationInfo
    {
      get => (WeighingStationInfo)GetValue(StationInfoProperty);
      set => SetValue(StationInfoProperty, value);
    }

    public static readonly DependencyProperty PreviewDoubleClickProperty =
      DependencyProperty.Register("PreviewDoubleClick", typeof(ICommand),
        typeof(WeighingStationControl), new PropertyMetadata(null));

    public ICommand PreviewDoubleClick
    {
      get => (ICommand)GetValue(PreviewDoubleClickProperty);
      set => SetValue(PreviewDoubleClickProperty, value);
    }

    public static readonly DependencyProperty RaiseBarrierUpCommandProperty =
      DependencyProperty.Register("RaiseBarrierUpCommand", typeof(ICommand),
        typeof(WeighingStationControl), new PropertyMetadata(null));

    public ICommand RaiseBarrierUpCommand
    {
      get => (ICommand)GetValue(RaiseBarrierUpCommandProperty);
      set => SetValue(RaiseBarrierUpCommandProperty, value);
    }

    public static readonly DependencyProperty LeaveBarrierDownCommandProperty =
      DependencyProperty.Register("LeaveBarrierDownCommand", typeof(ICommand),
        typeof(WeighingStationControl), new PropertyMetadata(null));

    public ICommand LeaveBarrierDownCommand
    {
      get => (ICommand)GetValue(LeaveBarrierDownCommandProperty);
      set => SetValue(LeaveBarrierDownCommandProperty, value);
    }

    public static readonly DependencyProperty TurnGreenLightCommandProperty =
      DependencyProperty.Register("TurnGreenLightCommand", typeof(ICommand),
        typeof(WeighingStationControl), new PropertyMetadata(null));

    public ICommand TurnGreenLightCommand
    {
      get => (ICommand)GetValue(TurnGreenLightCommandProperty);
      set => SetValue(TurnGreenLightCommandProperty, value);
    }

    public static readonly DependencyProperty TurnRedLightCommandProperty =
      DependencyProperty.Register("TurnRedLightCommand", typeof(ICommand),
        typeof(WeighingStationControl), new PropertyMetadata(null));

    public ICommand TurnRedLightCommand
    {
      get => (ICommand)GetValue(TurnRedLightCommandProperty);
      set => SetValue(TurnRedLightCommandProperty, value);
    }

    public static readonly DependencyProperty CheckChangedCommandProperty =
      DependencyProperty.Register("CheckChangedCommand", typeof(ICommand),
        typeof(WeighingStationControl), new PropertyMetadata(null));

    public ICommand CheckChangedCommand
    {
      get => (ICommand)GetValue(CheckChangedCommandProperty);
      set => SetValue(CheckChangedCommandProperty, value);
    }

    public static readonly DependencyProperty SoundEnabledProperty =
      DependencyProperty.Register("SoundEnabled", typeof(bool),
        typeof(WeighingStationControl), new FrameworkPropertyMetadata(true, PropertyChangedCallback)
        {
          BindsTwoWayByDefault = true,
          DefaultUpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged
        });

    public bool SoundEnabled
    {
      get => (bool)GetValue(SoundEnabledProperty);
      set => SetValue(SoundEnabledProperty, value);
    }

    private static void PropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      var ctrl = d as WeighingStationControl;
      if (ctrl == null)
        return;

      ctrl.WeighingControl.SoundEnabled = (bool)e.NewValue;
    }

    private void OnPreview1Click(object sender, MouseButtonEventArgs e)
    {
      if (e.ClickCount == 2)
      {
        _timer.Stop();
        OnImageDoubleClick();
      }
      else
      {
        _clickedImage = (Image)sender;
        _timer.Start();
      }
    }

    private void OnTimerTick(object sender, EventArgs e)
    {
      _timer.Stop();
      OnImageSingleClick();
    }

    private void OnImageDoubleClick()
    {
      CameraPreviewModel clickedPreview = null;
      if (_clickedImage.Source == StationInfo.Camera1Preview?.Image)
        clickedPreview = StationInfo.Camera1Preview;
      if (_clickedImage.Source == StationInfo.Camera2Preview?.Image)
        clickedPreview = StationInfo.Camera2Preview;
      if (_clickedImage.Source == StationInfo.Camera3Preview?.Image)
        clickedPreview = StationInfo.Camera3Preview;

      PreviewDoubleClick?.Execute(clickedPreview);
    }

    private void OnImageSingleClick()
    {
      //MainPreviewImage.Source = _clickedImage.Source;
      var binding = new Binding
      {
        ElementName = _clickedImage.Name,
        Path = new PropertyPath("Source")
      };
      MainPreviewImage.SetBinding(Image.SourceProperty, binding);
    }
  }
}
