﻿using Kalisto.Frontend.Notifications.Enums;
using Kalisto.Hmi.Dialogs.Common;
using Kalisto.Hmi.Dialogs.Common.Services;
using Kalisto.Hmi.Dialogs.Dashboard.Models;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
using Kalisto.Devices.IOController.Interfaces;

namespace Kalisto.Hmi.Dialogs.Dashboard.Controls
{
  /// <summary>
  /// Interaction logic for DeviceControl.xaml
  /// </summary>
  public partial class DeviceControl : UserControl
  {
    public DeviceControl()
    {
      InitializeComponent();
    }

    public static readonly DependencyProperty AutoModeEnabledProperty =
      DependencyProperty.Register("AutoModeEnabled", typeof(bool),
        typeof(DeviceControl), new FrameworkPropertyMetadata(true)
        {
          BindsTwoWayByDefault = true,
          DefaultUpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged
        });

    public bool AutoModeEnabled
    {
      get => (bool)GetValue(AutoModeEnabledProperty);
      set => SetValue(AutoModeEnabledProperty, value);
    }

    public static readonly DependencyProperty TrafficStateProperty =
      DependencyProperty.Register("TrafficState", typeof(TrafficState?),
        typeof(DeviceControl), new FrameworkPropertyMetadata(null)
        {
          BindsTwoWayByDefault = true,
          DefaultUpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged
        });

    public TrafficState? TrafficState
    {
      get => (TrafficState?)GetValue(TrafficStateProperty);
      set => SetValue(TrafficStateProperty, value);
    }

    public static readonly DependencyProperty BarrierStateProperty =
      DependencyProperty.Register("BarrierState", typeof(BarrierState?),
        typeof(DeviceControl), new FrameworkPropertyMetadata(null)
        {
          BindsTwoWayByDefault = true,
          DefaultUpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged
        });

    public BarrierState? BarrierState
    {
      get => (BarrierState?)GetValue(BarrierStateProperty);
      set => SetValue(BarrierStateProperty, value);
    }

    public static readonly DependencyProperty WeighingDeviceInfoProperty =
      DependencyProperty.Register("WeighingDeviceInfo", typeof(WeighingDeviceInfo),
        typeof(DeviceControl), new PropertyMetadata(new WeighingDeviceInfo
        {
          DeviceStatus = WeighingDeviceStatus.Unknown,
          MeasuredValue = new UnitOfMeasurement()
        }));

    public WeighingDeviceInfo WeighingDeviceInfo
    {
      get => (WeighingDeviceInfo)GetValue(WeighingDeviceInfoProperty);
      set => SetValue(WeighingDeviceInfoProperty, value);
    }

    public static readonly DependencyProperty RaiseBarrierUpCommandProperty =
      DependencyProperty.Register("RaiseBarrierUpCommand", typeof(ICommand),
        typeof(DeviceControl), new PropertyMetadata(null));

    public ICommand RaiseBarrierUpCommand
    {
      get => (ICommand)GetValue(RaiseBarrierUpCommandProperty);
      set => SetValue(RaiseBarrierUpCommandProperty, value);
    }

    public static readonly DependencyProperty LeaveBarrierDownCommandProperty =
      DependencyProperty.Register("LeaveBarrierDownCommand", typeof(ICommand),
        typeof(DeviceControl), new PropertyMetadata(null));

    public ICommand LeaveBarrierDownCommand
    {
      get => (ICommand)GetValue(LeaveBarrierDownCommandProperty);
      set => SetValue(LeaveBarrierDownCommandProperty, value);
    }

    public static readonly DependencyProperty TurnGreenLightCommandProperty =
      DependencyProperty.Register("TurnGreenLightCommand", typeof(ICommand),
        typeof(DeviceControl), new PropertyMetadata(null));

    public ICommand TurnGreenLightCommand
    {
      get => (ICommand)GetValue(TurnGreenLightCommandProperty);
      set => SetValue(TurnGreenLightCommandProperty, value);
    }

    public static readonly DependencyProperty TurnRedLightCommandProperty =
      DependencyProperty.Register("TurnRedLightCommand", typeof(ICommand),
        typeof(DeviceControl), new PropertyMetadata(null));

    public ICommand TurnRedLightCommand
    {
      get => (ICommand)GetValue(TurnRedLightCommandProperty);
      set => SetValue(TurnRedLightCommandProperty, value);
    }

    public static readonly DependencyProperty CheckChangedCommandProperty =
      DependencyProperty.Register("CheckChangedCommand", typeof(ICommand),
        typeof(DeviceControl), new PropertyMetadata(null));

    public ICommand CheckChangedCommand
    {
      get => (ICommand)GetValue(CheckChangedCommandProperty);
      set => SetValue(CheckChangedCommandProperty, value);
    }

    private void RaiseBarrierUp(object sender, RoutedEventArgs e)
    {
      RaiseBarrierUpCommand?.Execute(WeighingDeviceInfo.Id);
    }

    private void LeaveBarrierDown(object sender, RoutedEventArgs e)
    {
      LeaveBarrierDownCommand?.Execute(WeighingDeviceInfo.Id);
    }

    private void SetGreenLight(object sender, RoutedEventArgs e)
    {
      TurnGreenLightCommand?.Execute(WeighingDeviceInfo.Id);
    }

    private void SetRedLight(object sender, RoutedEventArgs e)
    {
      TurnRedLightCommand?.Execute(WeighingDeviceInfo.Id);
    }

    private void CheckChanged(object sender, EventArgs e)
    {
      CheckChangedCommand?.Execute(new DeviceModeModel
      {
        AutoModeEnabled = ToggleSwitch.IsChecked ?? false,
        DeviceId = WeighingDeviceInfo.Id
      });
    }
  }
}
