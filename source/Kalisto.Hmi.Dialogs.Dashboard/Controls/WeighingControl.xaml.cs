﻿using Kalisto.Frontend.Notifications.Enums;
using Kalisto.Hmi.Dialogs.Common;
using Kalisto.Hmi.Dialogs.Dashboard.Models;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using Kalisto.Hmi.Dialogs.Common.Services;

namespace Kalisto.Hmi.Dialogs.Dashboard.Controls
{
  /// <summary>
  /// Interaction logic for WeighingControl.xaml
  /// </summary>
  public partial class WeighingControl : UserControl
  {
    public WeighingControl()
    {
      InitializeComponent();
    }

    public static readonly DependencyProperty WeighingDeviceInfoProperty =
      DependencyProperty.Register("WeighingDeviceInfo", typeof(WeighingDeviceInfo),
        typeof(WeighingControl), new PropertyMetadata(new WeighingDeviceInfo
        {
          DeviceStatus = WeighingDeviceStatus.Unknown,
          MeasuredValue = new UnitOfMeasurement()
        }));

    public WeighingDeviceInfo WeighingDeviceInfo
    {
      get => (WeighingDeviceInfo)GetValue(WeighingDeviceInfoProperty);
      set => SetValue(WeighingDeviceInfoProperty, value);
    }

    public static readonly DependencyProperty SoundEnabledProperty =
      DependencyProperty.Register("SoundEnabled", typeof(bool),
        typeof(WeighingControl), new FrameworkPropertyMetadata(true, PropertyChangedCallback)
        {
          BindsTwoWayByDefault = true,
          DefaultUpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged
        });

    public bool SoundEnabled
    {
      get => (bool)GetValue(SoundEnabledProperty);
      set => SetValue(SoundEnabledProperty, value);
    }

    private static void PropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      var ctrl = d as WeighingControl;
      if (ctrl == null)
        return;

      ctrl.SoundButton.SoundEnabled = (bool) e.NewValue;
    }
  }
}
