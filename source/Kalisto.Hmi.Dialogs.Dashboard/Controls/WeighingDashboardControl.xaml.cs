﻿using System.Collections;
using Kalisto.Hmi.Core;
using Kalisto.Hmi.Dialogs.Dashboard.Models;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;

namespace Kalisto.Hmi.Dialogs.Dashboard.Controls
{
  /// <summary>
  /// Interaction logic for WeighingDashboardControl.xaml
  /// </summary>
  public partial class WeighingDashboardControl : UserControl
  {
    public WeighingDashboardControl()
    {
      InitializeComponent();
    }

    public static readonly DependencyProperty StationInfosProperty =
      DependencyProperty.Register("StationInfos", typeof(IObservableCollection),
        typeof(WeighingDashboardControl), new PropertyMetadata(null, PropertyChangedCallback));

    private static void PropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
    {
      var ctrl = d as WeighingDashboardControl;
      if (ctrl == null)
        return;

      var oldCollection = e.OldValue as IObservableCollection;
      if (oldCollection != null)
        oldCollection.CollectionChanged -= ctrl.OnCollectionChanged;

      var newCollection = e.NewValue as IObservableCollection;
      if (newCollection != null)
      {
        newCollection.CollectionChanged += ctrl.OnCollectionChanged;
        ctrl.GenerateControl(newCollection.Cast<WeighingStationInfo>());
      }
    }

    private void OnCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
    {
      if (e.Action != NotifyCollectionChangedAction.Add && e.Action != NotifyCollectionChangedAction.Reset)
        return;

      if (e.NewItems != null)
        GenerateControl(e.NewItems.Cast<WeighingStationInfo>());
      else
      {
        var collection = sender as IList;
        if (collection == null)
          return;

        GenerateControl(collection.Cast<WeighingStationInfo>());
      }
    }

    private void GenerateControl(IEnumerable<WeighingStationInfo> infos)
    {
      foreach (var stationInfo in infos)
      {
        MainGrid.ColumnDefinitions.Add(new ColumnDefinition
        {
          Width = new GridLength(1, GridUnitType.Star)
        });

        var newColumnIndex = MainGrid.ColumnDefinitions.Count - 1;
        var ctrl = new WeighingStationControl();
        ctrl.SetValue(Grid.ColumnProperty, newColumnIndex);
        ctrl.SetBinding(WeighingStationControl.StationInfoProperty, new Binding
        {
          Source = stationInfo
        });
        ctrl.SetBinding(WeighingStationControl.PreviewDoubleClickProperty, "CameraPreviewDoubleClickedCommand");
        ctrl.SetBinding(WeighingStationControl.SoundEnabledProperty, "SoundNotification.SoundEnabled");
        ctrl.SetBinding(WeighingStationControl.RaiseBarrierUpCommandProperty, "RaiseBarrierUpCommand");
        ctrl.SetBinding(WeighingStationControl.LeaveBarrierDownCommandProperty, "LeaveBarrierDownCommand");
        ctrl.SetBinding(WeighingStationControl.TurnGreenLightCommandProperty, "TurnGreenLightCommand");
        ctrl.SetBinding(WeighingStationControl.TurnRedLightCommandProperty, "TurnRedLightCommand");
        ctrl.SetBinding(WeighingStationControl.CheckChangedCommandProperty, "CheckChangedCommand");
        MainGrid.Children.Add(ctrl);
      }
    }

    public IObservableCollection StationInfos
    {
      get => (IObservableCollection)GetValue(StationInfosProperty);
      set => SetValue(StationInfosProperty, value);
    }
  }
}
