﻿using Kalisto.Frontend.Notifications.Enums;
using Kalisto.Localization;
using System;
using System.Globalization;
using System.Windows.Data;

namespace Kalisto.Hmi.Dialogs.Dashboard.Converters
{
  public class WeighingDeviceStatusToTextConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      var status = value ?? WeighingDeviceStatus.Unknown;
      var statusEnum = (WeighingDeviceStatus)status;
      return new LocalizedEnum(statusEnum).LocalizedEnumValue;
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      return new LocalizedEnum(WeighingDeviceStatus.Unknown).LocalizedEnumValue;
    }
  }
}
