﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;
using Kalisto.Frontend.Notifications.Enums;

namespace Kalisto.Hmi.Dialogs.Dashboard.Converters
{
  public class WeighingDeviceStatusToColorConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      var color = Colors.Red;
      if (value == null)
        return new SolidColorBrush(color);

      var status = (WeighingDeviceStatus) value;
      switch (status)
      {
        case WeighingDeviceStatus.Stable:
          color = Colors.MediumSeaGreen;
          break;
        case WeighingDeviceStatus.NotStable:
          color = Colors.SandyBrown;
          break;
        case WeighingDeviceStatus.Unknown:
          color = Colors.Crimson;
          break;
        default:
          color = Colors.Red;
          break;
      }

      return new SolidColorBrush(color);
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      return null;
    }
  }
}
