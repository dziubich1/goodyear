﻿using Kalisto.Devices.IOController.Interfaces;
using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace Kalisto.Hmi.Dialogs.Dashboard.Converters
{
  public class TrafficStateToColorConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      var color = Colors.SandyBrown;
      if (value == null)
        return new SolidColorBrush(color);

      var status = (TrafficState)value;
      switch (status)
      {
        case TrafficState.Green:
          color = Colors.LimeGreen;
          break;
        case TrafficState.Red:
          color = Colors.Red;
          break;
        default:
          color = Colors.SandyBrown;
          break;
      }

      return new SolidColorBrush(color);
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      return null;
    }
  }
}
