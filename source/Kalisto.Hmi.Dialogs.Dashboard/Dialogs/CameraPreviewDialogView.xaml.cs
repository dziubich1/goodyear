﻿using MahApps.Metro.Controls;

namespace Kalisto.Hmi.Dialogs.Dashboard.Dialogs
{
  /// <summary>
  /// Interaction logic for CameraPreviewDialog.xaml
  /// </summary>
  public partial class CameraPreviewDialogView : MetroWindow
  {
    public CameraPreviewDialogView()
    {
      InitializeComponent();
    }
  }
}
