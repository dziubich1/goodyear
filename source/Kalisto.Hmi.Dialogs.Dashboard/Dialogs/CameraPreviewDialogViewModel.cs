﻿using Caliburn.Micro;
using Kalisto.Devices.Camera.Models;
using Kalisto.Devices.Camera.Services;
using Kalisto.Hmi.Dialogs.Dashboard.Models;
using Kalisto.Localization;
using PropertyChanged;

namespace Kalisto.Hmi.Dialogs.Dashboard.Dialogs
{
  [AddINotifyPropertyChangedInterface]
  public class CameraPreviewDialogViewModel : Screen
  {
    private readonly ICameraService _cameraService;

    private readonly ILocalizer<Kalisto.Localization.Common.Common> _localizer;

    private CameraInfo _cameraInfo;

    public CameraPreviewModel CameraPreview { get; set; }

    public CameraPreviewDialogViewModel(ICameraService cameraService)
    {
      _cameraService = cameraService;
      _localizer = new LocalizationManager<Kalisto.Localization.Common.Common>();
    }

    public void OnCameraPreviewChanged()
    {
      DisplayName = string.Format(_localizer.Localize(() => Kalisto.Localization.Common.Common.CameraPreviewText), CameraPreview.CameraId);

      StartCameraPreview();
    }

    public void StartCameraPreview()
    {
      _cameraInfo = new CameraInfo
      {
        CameraId = CameraPreview.CameraId,
        CameraAddress = CameraPreview.ImageUrl,
        Username = CameraPreview.Username,
        Password = CameraPreview.Password
      };
      _cameraService.Configure(new[]
      {
        _cameraInfo
      });

      _cameraService.SnapshotReceived += OnSnapshotReceived;
      _cameraService.StartFetching();
    }

    private void OnSnapshotReceived(CameraResponse obj)
    {
      if (_cameraInfo.DeviceId != obj.CameraInfo.DeviceId)
        return;

      if (_cameraInfo.CameraId != obj.CameraInfo.CameraId)
        return;

      CameraPreview.Image = obj.Image;
    }
  }
}
