﻿using Kalisto.Configuration.Devices.Connection;
using Kalisto.Devices.Camera.Models;
using Kalisto.Devices.Camera.Services;
using Kalisto.Frontend.Notifications;
using Kalisto.Frontend.Notifications.Misc;
using Kalisto.Hmi.Core;
using Kalisto.Hmi.Core.Commands;
using Kalisto.Hmi.Core.Services;
using Kalisto.Hmi.Dialogs.Common;
using Kalisto.Hmi.Dialogs.Common.Services;
using Kalisto.Hmi.Dialogs.Dashboard.Dialogs;
using Kalisto.Hmi.Dialogs.Dashboard.Models;
using Kalisto.Hmi.Dialogs.Dashboard.Services;
using PropertyChanged;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Kalisto.Devices.IOController.Interfaces;

namespace Kalisto.Hmi.Dialogs.Dashboard.ViewModels
{
  [AddINotifyPropertyChangedInterface]
  public class DashboardViewModel : ViewModelBase
  {
    private readonly IDashboardService _dashboardService;

    private readonly ICameraService _cameraService;

    private readonly IUnitTypeService _unitTypeService;

    private readonly IServerDeviceConfiguration _serverDeviceConfiguration;

    private readonly IWeighingDeviceService _weighingDeviceService;

    private readonly IAsyncProgressService _asyncProgressService;

    public override string DialogTitleKey => "Dashboard";

    public ObservableCollectionEx<WeighingStationInfo> StationInfos { get; set; }

    public ICommand CameraPreviewDoubleClickedCommand { get; set; }

    public ISoundNotification SoundNotification { get; }

    public bool IsDeviceSectionVisible { get; set; } = false;

    public ICommand RaiseBarrierUpCommand { get; set; }

    public ICommand LeaveBarrierDownCommand { get; set; }

    public ICommand TurnGreenLightCommand { get; set; }

    public ICommand TurnRedLightCommand { get; set; }

    public ICommand CheckChangedCommand { get; set; }

    public DashboardViewModel(IDialogService dialogService, IDashboardService dashboardService,
      IUnitTypeService unitTypeService, ICameraService cameraService, ISoundNotification soundNotification,
      IServerDeviceConfiguration serverDeviceConfiguration, IWeighingDeviceService weighingDeviceService,
      IAsyncProgressService asyncProgressService)
      : base(dialogService)
    {
      _dashboardService = dashboardService;
      _dashboardService.WeighingValueUpdated += OnWeighingValueUpdated;
      _dashboardService.DeviceStatesUpdated += OnDeviceStatesUpdated;
      _unitTypeService = unitTypeService;
      _serverDeviceConfiguration = serverDeviceConfiguration;
      _weighingDeviceService = weighingDeviceService;
      _asyncProgressService = asyncProgressService;
      SoundNotification = soundNotification;

      _cameraService = cameraService;
      CameraPreviewDoubleClickedCommand = new DelegateCommand(OnCameraPreviewDoubleClicked);
      StationInfos = new ObservableCollectionEx<WeighingStationInfo>();
      RaiseBarrierUpCommand = new DelegateCommand(RaiseBarrierUp);
      LeaveBarrierDownCommand = new DelegateCommand(LeaveBarrierDown);
      TurnGreenLightCommand = new DelegateCommand(TurnGreenLight);
      TurnRedLightCommand = new DelegateCommand(TurnRedLight);
      CheckChangedCommand = new DelegateCommand(AutoModeChecked);
    }

    private void StartAsyncOperationWithTimeout()
    {
      _asyncProgressService.StartAsyncOperation();
      Task.Run(async () =>
      {
        await Task.Delay(10000);
        _asyncProgressService.FinishAsyncOperation();
      });
    }

    private async void AutoModeChecked(object obj)
    {
      var autoModeInfo = obj as DeviceModeModel;
      if (autoModeInfo == null)
        return;

      await _dashboardService.ChangeAutoMode(autoModeInfo.DeviceId, autoModeInfo.AutoModeEnabled);
    }

    private async void TurnRedLight(object obj)
    {
      StartAsyncOperationWithTimeout();
      var deviceId = (int)obj;
      await _dashboardService.TurnGreenOrRedLight(deviceId, false);
    }

    private async void TurnGreenLight(object obj)
    {
      StartAsyncOperationWithTimeout();
      var deviceId = (int)obj;
      await _dashboardService.TurnGreenOrRedLight(deviceId, true);
    }

    private async void LeaveBarrierDown(object obj)
    {
      StartAsyncOperationWithTimeout();
      var deviceId = (int)obj;
      await _dashboardService.RaiseOrLeaveBarrier(deviceId, false);
    }

    private async void RaiseBarrierUp(object obj)
    {
      StartAsyncOperationWithTimeout();
      var deviceId = (int)obj;
      await _dashboardService.RaiseOrLeaveBarrier(deviceId, true);
    }

    protected override async void OnViewLoaded()
    {
      base.OnViewLoaded();

      await LoadDeviceConfiguration();
      StartCameraRefreshing();
      await _dashboardService.RequestOutputStatesInfoAsync();
    }

    protected override void OnDeactivate(bool close)
    {
      base.OnDeactivate(close);

      _cameraService.StopFetching();
    }

    private async Task LoadDeviceConfiguration()
    {
      var deviceConfiguration = await _serverDeviceConfiguration.GetConfigurationAsync();
      var ioController = deviceConfiguration.IoControllers?.FirstOrDefault();
      if (ioController != null && (ioController.Barriers.Any() || ioController.TrafficLights.Any()))
        IsDeviceSectionVisible = true;

      // weight metering devices config
      var weightMeters = await _weighingDeviceService.GetWeighingDevicesAsync();
      foreach (var weightMeter in weightMeters)
      {
        var isStateMachineActive = await _dashboardService.IsStateMachineRunning(weightMeter.Id);
        StationInfos.Add(new WeighingStationInfo(_dashboardService, isStateMachineActive)
        {
          DeviceInfo = weightMeter,
          IsStateMachineActive = isStateMachineActive,
          BarrierState = null,
          TrafficState = null
        });
      }

      // camera devices config
      var cameras = deviceConfiguration.Cameras.Where(c => c.IsActive).Select(c =>
      {
        var connectionInfo = c.ConnectionConfiguration as HttpConnectionConfiguration;
        return new CameraInfo
        {
          CameraId = c.Id,
          DeviceId = c.WeightMeterId,
          CameraAddress = connectionInfo?.Url,
          Username = connectionInfo?.Username,
          Password = connectionInfo?.Password
        };
      }).ToList();

      // configure camera service
      _cameraService.Configure(cameras.ToArray());

      // aggregate weight metering devices with cameras
      foreach (var station in StationInfos)
      {
        var camerasForDevice = cameras.Where(c => c.DeviceId == station.DeviceInfo.Id).ToList();
        var cam = camerasForDevice.ElementAtOrDefault(0);
        if (cam != null)
          station.Camera1Preview = new CameraPreviewModel
          {
            CameraId = cam.CameraId,
            ImageUrl = cam.CameraAddress,
            Username = cam.Username,
            Password = cam.Password
          };
        cam = camerasForDevice.ElementAtOrDefault(1);
        if (cam != null)
          station.Camera2Preview = new CameraPreviewModel
          {
            CameraId = cam.CameraId,
            ImageUrl = cam.CameraAddress,
            Username = cam.Username,
            Password = cam.Password
          };
        cam = camerasForDevice.ElementAtOrDefault(2);
        if (cam != null)
          station.Camera3Preview = new CameraPreviewModel
          {
            CameraId = cam.CameraId,
            ImageUrl = cam.CameraAddress,
            Username = cam.Username,
            Password = cam.Password
          };
      }
    }

    private void OnCameraPreviewDoubleClicked(object obj)
    {
      var cameraPreviewModel = obj as CameraPreviewModel;
      if (cameraPreviewModel == null)
        return;

      DialogService.ShowDialog<CameraPreviewDialogViewModel>(vm =>
      {
        vm.CameraPreview = new CameraPreviewModel
        {
          Image = cameraPreviewModel.Image,
          CameraId = cameraPreviewModel.CameraId,
          ImageUrl = cameraPreviewModel.ImageUrl,
          Username = cameraPreviewModel.Username,
          Password = cameraPreviewModel.Password
        };
      });
    }

    private void OnWeighingValueUpdated(WeighingValueUpdatedNotification notification)
    {
      var device =
        StationInfos.FirstOrDefault(c => c.DeviceInfo.Id == notification.DeviceId);
      if (device == null)
        return;

      device.DeviceInfo.DeviceStatus = notification.Status;
      device.DeviceInfo.MeasuredValue
        = new UnitOfMeasurement(notification.Value, _unitTypeService);
    }

    private void StartCameraRefreshing()
    {
      _cameraService.SnapshotReceived += RefreshCameraPreviews;
      _cameraService.StartFetching();
    }

    private void RefreshCameraPreviews(CameraResponse res)
    {
      if (res.Image == null)
        return;

      var device =
        StationInfos.FirstOrDefault(c => c.DeviceInfo.Id == res.CameraInfo.DeviceId);
      if (device == null)
        return;

      if (device.Camera1Preview != null && device.Camera1Preview.CameraId == res.CameraInfo.CameraId)
        device.Camera1Preview.Image = res.Image;
      if (device.Camera2Preview != null && device.Camera2Preview.CameraId == res.CameraInfo.CameraId)
        device.Camera2Preview.Image = res.Image;
      if (device.Camera3Preview != null && device.Camera3Preview.CameraId == res.CameraInfo.CameraId)
        device.Camera3Preview.Image = res.Image;
    }

    private void OnDeviceStatesUpdated(DeviceStateNotification notification)
    {
      foreach (var payload in notification.Payloads)
      {
        var device =
          StationInfos.FirstOrDefault(c => c.DeviceInfo.Id == payload.DeviceId);
        if (device == null)
          return;

        device.BarrierState = payload.BarrierState;
        device.TrafficState = payload.TrafficState;
      }

      _asyncProgressService.FinishAsyncOperation();
    }
  }
}
