﻿using PropertyChanged;
using System.Windows.Media.Imaging;

namespace Kalisto.Hmi.Dialogs.Dashboard.Models
{
  [AddINotifyPropertyChangedInterface]
  public class CameraPreviewModel
  {
    public int CameraId { get; set; }

    public BitmapImage Image { get; set; }

    public string ImageUrl { get; set; }

    public string Username { get; set; }

    public string Password { get; set; }
  }
}
