﻿namespace Kalisto.Hmi.Dialogs.Dashboard.Models
{
  public class DeviceModeModel
  {
    public bool AutoModeEnabled { get; set; }

    public int DeviceId { get; set; }
  }
}
