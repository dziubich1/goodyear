﻿using Kalisto.Devices.IOController.Interfaces;
using Kalisto.Hmi.Dialogs.Common.Services;
using Kalisto.Hmi.Dialogs.Dashboard.Services;
using PropertyChanged;

namespace Kalisto.Hmi.Dialogs.Dashboard.Models
{
  [AddINotifyPropertyChangedInterface]
  public class WeighingStationInfo
  {
    public WeighingDeviceInfo DeviceInfo { get; set; }

    public CameraPreviewModel Camera1Preview { get; set; }

    public CameraPreviewModel Camera2Preview { get; set; }

    public CameraPreviewModel Camera3Preview { get; set; }

    public bool IsStateMachineActive { get; set; }

    public TrafficState? TrafficState { get; set; }

    public BarrierState? BarrierState { get; set; }

    private readonly IDashboardService _dashboardService;

    private bool _initialStateMachineValue;

    public WeighingStationInfo()
    {

    }

    public WeighingStationInfo(IDashboardService dashboardService, bool stateMachineActive)
    {
      _dashboardService = dashboardService;
      _initialStateMachineValue = stateMachineActive;
    }

    public async void OnIsStateMachineActiveChanged()
    {
      if (_dashboardService == null)
        return;

      if (IsStateMachineActive == _initialStateMachineValue)
        return;

      _initialStateMachineValue = IsStateMachineActive;
      await _dashboardService.ChangeAutoMode(DeviceInfo.Id, IsStateMachineActive);
    }
  }
}
