﻿using Kalisto.Backend.Operations.DbEventLogs.Dto;
using Kalisto.Backend.Operations.DbEventLogs.Queries;
using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Hmi.Core.Communication;
using Kalisto.Hmi.Core.Services;
using Kalisto.Hmi.Dialogs.DbEventLogs.Models;
using Kalisto.Localization;
using Kalisto.Localization.Common;
using Kalisto.Localization.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kalisto.Hmi.Dialogs.DbEventLogs.Services
{
  public interface IDbEventLogsService : IBackendOperationService
  {
    Task<RequestResult<List<DbEventLogModel>>> GetLogsAsync(DateTime from, DateTime to);
  }

  public class DbEventLogsService : BackendOperationService, IDbEventLogsService
  {
    private readonly ILocalizer<Messages> _localizer;

    public DbEventLogsService(ClientInfo clientInfo, IMessageBuilder messageBuilder, IFrontendMessageBridge messageBridge, IDialogService dialogService)
      : base(clientInfo, messageBuilder, messageBridge, dialogService)
    {
      _localizer = new LocalizationManager<Messages>();
    }

    public async Task<RequestResult<List<DbEventLogModel>>> GetLogsAsync(DateTime from, DateTime to)
    {
      var fromUtc = from.ToUniversalTime();
      var toUtc = to.ToUniversalTime();
      var message = MessageBuilder.CreateNew<GetDbEventLogsQuery>()
        .BasedOn(ClientInfo)
        .WithProperty(c => c.FromUtc, fromUtc)
        .WithProperty(c => c.ToUtc, toUtc)
        .Build();

      var result = await MessageBridge.SendMessageWithResultAsync<List<DbEventLogDto>>(message);
      return new RequestResult<List<DbEventLogModel>>
      {
        Succeeded = result?.Succeeded ?? false,
        Response = result?.Response?.Select((s, index) => new DbEventLogModel
        {
          Id = index + 1,
          LocalizedDescription = GetLocalizedEventDescription(s),
          LocalizedEventType = _localizer.Localize(s.EventType.ToString()),
          Source = s.Source,
          Date = s.EventDateUtc.ToLocalTime(),
        }).ToList() ?? new List<DbEventLogModel>()
      };
    }

    private string GetLocalizedEventDescription(DbEventLogDto dbEvent)
    {
      var placeHolderType = $"{dbEvent.EventType.ToString()}Placeholder";
      return string.Format(_localizer.Localize(placeHolderType), dbEvent.Source, dbEvent.TableName, dbEvent.EntityId);
    }
  }
}
