﻿using Kalisto.Hmi.Core;
using Kalisto.Hmi.Core.Services;
using Kalisto.Hmi.Dialogs.DbEventLogs.Models;
using Kalisto.Hmi.Dialogs.DbEventLogs.Services;
using PropertyChanged;
using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Kalisto.Communication.Core.Messages;
using Kalisto.Hmi.Core.Commands;

namespace Kalisto.Hmi.Dialogs.DbEventLogs.ViewModels
{
  [AddINotifyPropertyChangedInterface]
  public class DbEventLogsViewModel : DataGridViewModel<DbEventLogModel>
  {
    private readonly TimeSpan _initialLogsTimespan = TimeSpan.FromDays(1);

    private readonly IDbEventLogsService _dbEventLogService;

    public override string DialogTitleKey => "DbEventLogs";

    public ICommand SyncItemsCommand { get; set; }

    public ObservableCollectionEx<DbEventLogModel> DbEventLogs { get; set; }

    public DateTime FromDate { get; set; }

    public DateTime ToDate { get; set; }

    protected override Type[] AssociatedCommandTypes => new[] { typeof(IAssociatedCommand) }; // interested in all of associated command types

    public DbEventLogsViewModel(IDialogService dialogService, IAuthorizationService authorizationService, IDbEventLogsService dbEventLogService)
      : base(dialogService, authorizationService)
    {
      _dbEventLogService = dbEventLogService;

      SyncItemsCommand = new DelegateCommand(SyncLogs);
      DbEventLogs = new ObservableCollectionEx<DbEventLogModel>();
      FromDate = DateTime.Now - _initialLogsTimespan;
      ToDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);
    }

    public async void OnFromDateChanged()
    {
      if (!ViewLoaded)
        return;

      await LoadDbEventLogsAsync();
    }

    public async void OnToDateChanged()
    {
      if (!ViewLoaded)
        return;

      await LoadDbEventLogsAsync();
    }

    protected override ObservableCollectionEx<DbEventLogModel> GetItemsSource() => DbEventLogs;

    public override async Task RefreshDataAsync()
    {
      await base.RefreshDataAsync();

      await LoadDbEventLogsAsync();
    }

    public override bool IsAssociatedWithCommand(IAssociatedCommand command)
    {
      return true;
    }

    private async Task LoadDbEventLogsAsync()
    {
      var result = await _dbEventLogService.GetLogsAsync(FromDate, ToDate);
      if (!result.Succeeded)
        return;

      DbEventLogs.Clear();
      DbEventLogs.AddRange(result.Response);
    }

    private async void SyncLogs(object obj)
    {
      await LoadDbEventLogsAsync();
    }
  }
}
