﻿using System;

namespace Kalisto.Hmi.Dialogs.DbEventLogs.Models
{
  public class DbEventLogModel
  {
    public long Id { get; set; }

    public string LocalizedEventType { get; set; }

    public string LocalizedDescription { get; set; }

    public string Source { get; set; }

    public DateTime Date { get; set; }
  }
}
