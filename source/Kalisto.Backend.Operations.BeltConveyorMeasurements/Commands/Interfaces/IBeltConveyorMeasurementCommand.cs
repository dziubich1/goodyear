﻿using Kalisto.Communication.Core.Messages;

namespace Kalisto.Backend.Operations.BeltConveyorMeasurements.Commands.Interfaces
{
  public interface IBeltConveyorMeasurementCommand : IAssociatedCommand
  {
  }
}
