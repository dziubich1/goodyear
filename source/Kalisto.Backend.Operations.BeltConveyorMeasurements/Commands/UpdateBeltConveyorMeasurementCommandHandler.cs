﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.DataAccess.Model.Logging;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Logging;
using System;
using System.Data.Entity;
using System.Linq;

namespace Kalisto.Backend.Operations.BeltConveyorMeasurements.Commands
{
  public class UpdateBeltConveyorMeasurementCommandHandler : CommandHandlerBase<UpdateBeltConveyorMeasurementCommand>
  {
    private readonly IDbUpdateEventLogger _dbLogger;
    private readonly ILogger _logger;

    public UpdateBeltConveyorMeasurementCommandHandler(IDbUpdateEventLogger dbLogger, ILogger logger)
    {
      _dbLogger = dbLogger;
      _logger = logger;
    }

    public override object Execute(IDbContext context, UpdateBeltConveyorMeasurementCommand command, string partnerId)
    {
      var measurement = context.BeltConveyorMeasurements.FirstOrDefault(x => x.Id == command.MeasurementId);
      if (measurement == null)
      {
        _logger.Error($"Cannot update measurement with ID: {command.MeasurementId} because it does not exist");
        throw new ArgumentException("Measurement with given identifier does not exist.");
      }

      // only for getting current measurement old values
      var oldMeasurement = context.BeltConveyorMeasurements.AsNoTracking().FirstOrDefault(c => c.Id == command.MeasurementId);

      measurement.ConfigId = command.ConfigId;
      measurement.B1 = command.B1;
      measurement.B2 = command.B2;
      measurement.B3 = command.B3;
      measurement.B4 = command.B4;
      measurement.IncrementSum = command.IncrementWeightSum;
      measurement.TotalSum = command.B1 + command.B2 + command.B3 + command.B4;
      measurement.DateUtc = command.MeasurementDateUtc;

      context.SaveChanges();
      _dbLogger.LogEvent(partnerId, ctx => ctx.BeltConveyorMeasurements, measurement.Id, measurement, oldMeasurement);

      _logger.Debug($"Successfully updated measurement with ID: {measurement.Id}");
      return measurement.Id;
    }
  }
}
