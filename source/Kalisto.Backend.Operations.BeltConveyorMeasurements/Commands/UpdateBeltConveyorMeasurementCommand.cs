﻿using Kalisto.Communication.Core.Messages;
using System;
using Kalisto.Backend.Operations.BeltConveyorMeasurements.Commands.Interfaces;

namespace Kalisto.Backend.Operations.BeltConveyorMeasurements.Commands
{
  public class UpdateBeltConveyorMeasurementCommand : BackendDbOperationMessage, IBeltConveyorMeasurementCommand
  {
    public long MeasurementId { get; set; }

    public int ConfigId { get; set; }

    //Weight for Furnace/BeltConveyor no. 1
    public decimal B1 { get; set; }

    //Weight for Furnace/BeltConveyor no. 2
    public decimal B2 { get; set; }

    //Weight for Furnace/BeltConveyor no. 3
    public decimal B3 { get; set; }

    //Weight for Furnace/BeltConveyor no. 4
    public decimal B4 { get; set; }

    public decimal IncrementWeightSum { get; set; }

    public DateTime MeasurementDateUtc { get; set; }
  }
}
