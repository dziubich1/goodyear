﻿using Kalisto.Communication.Core.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kalisto.Backend.Operations.BeltConveyorMeasurements.Queries
{
  public class GetBeltConveyorMeasurementsQuery : BackendDbOperationMessage
  {
    public DateTime FromDateUtc { get; set; }

    public DateTime ToDateUtc { get; set; }
  }
}
