﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Communication.Core.Messaging;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kalisto.Backend.Operations.BeltConveyorMeasurements.Queries
{
  public class GetBeltConveyorMeasurementsQueryHandler : QueryHandlerBase<GetBeltConveyorMeasurementsQuery>
  {
    public override object Execute(IDbContext context, GetBeltConveyorMeasurementsQuery query, string partnerId)
    {
      return context.BeltConveyorMeasurements
        .Where(c => c.DateUtc >= query.FromDateUtc && c.DateUtc <= query.ToDateUtc)
        .OrderBy(c=>c.DateUtc)
        .AsNoTracking()
        .ToList();
    }
  }
}
