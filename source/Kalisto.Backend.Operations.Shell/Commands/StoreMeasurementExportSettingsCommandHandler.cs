﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Configuration.MeasurementExportSettings;
using Kalisto.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kalisto.Backend.Operations.Shell.Commands
{
  public class StoreMeasurementExportSettingsCommandHandler : CommandHandlerBase<StoreMeasurementExportSettingsCommand>
  {
    private readonly MeasurementExportConfigurationManager _configurationManager;
    private readonly ILogger _logger;

    public StoreMeasurementExportSettingsCommandHandler(MeasurementExportConfigurationManager configurationManager, ILogger logger)
    {
      _configurationManager = configurationManager;
      _logger = logger;
    }
    public override object Execute(IDbContext context, StoreMeasurementExportSettingsCommand command, string partnerId)
    {
      try
      {
        _configurationManager.Save(command.MeasurementExportConfiguration);
        _logger.Info(@"Successfully stored Measurement Export configuration.");
        return true;
      }
      catch (Exception e)
      {
        _logger.Error($@"Could not store Measurement Export configuration. Given exception was thrown: {e.Message}");
        throw;
      }
    }
  }
}
