﻿using System;
using Kalisto.Communication.Core.Messages;

namespace Kalisto.Backend.Operations.Shell.Commands
{
  [Serializable]
  public class AssignLicenseKeyCommand : BackendDbOperationMessage
  {
    public string LicenseKey { get; set; }
  }
}
