﻿using System;
using Kalisto.Communication.Core.Messages;
using Kalisto.Mailing.Configuration;

namespace Kalisto.Backend.Operations.Shell.Commands
{
  [Serializable]
  public class StoreSmtpSettingsCommand : BackendDbOperationMessage
  {
    public SmtpSettings SmtpSettings { get; set; }
  }
}
