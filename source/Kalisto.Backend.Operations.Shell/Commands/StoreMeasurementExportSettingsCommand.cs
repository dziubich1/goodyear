﻿using Kalisto.Communication.Core.Messages;
using Kalisto.Configuration.MeasurementExportSettings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kalisto.Backend.Operations.Shell.Commands
{
  public class StoreMeasurementExportSettingsCommand : BackendDbOperationMessage
  {
    public MeasurementExportConfiguration MeasurementExportConfiguration { get; set; }
  }
}
