﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.Operations.Shell.Exceptions;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Licensing;
using Kalisto.Logging;

namespace Kalisto.Backend.Operations.Shell.Commands
{
  public class AssignLicenseKeyCommandHandler : CommandHandlerBase<AssignLicenseKeyCommand>
  {
    private readonly ILicenseInfo _licenseInfo;
    private readonly ILogger _logger;

    public AssignLicenseKeyCommandHandler(ILicenseInfo licenseInfo, ILogger logger)
    {
      _licenseInfo = licenseInfo;
      _logger = logger;
    }

    public override object Execute(IDbContext context, AssignLicenseKeyCommand command, string partnerId)
    {
      if(string.IsNullOrEmpty(command.LicenseKey))
      {
        _logger.Error($"Cannot assign license key because is null or empty");
        throw new InvalidLicenseKeyException();
      }

      if (!_licenseInfo.IsLicenseKeyValid(command.LicenseKey))
      {
        _logger.Error($"Cannot assign license key: {command.LicenseKey} because is not valid");
        throw new InvalidLicenseKeyException();
      }

      return _licenseInfo.AssignLicenseKey(command.LicenseKey);
    }
  }
}
