﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Communication.Core.Logging;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Communication.Core.Registries;
using Kalisto.Logging;
using System;

namespace Kalisto.Backend.Operations.Shell.Commands
{
  public class LogoutCommandHandler : CommandHandlerBase<LogoutCommand>
  {
    private readonly IPartnerRegistry _partnerRegistry;

    private readonly ISystemEventLogger _systemEventLogger;

    private readonly ILogger _logger;

    public LogoutCommandHandler(IPartnerRegistry partnerRegistry, ISystemEventLogger systemEventLogger, ILogger logger)
    {
      _partnerRegistry = partnerRegistry;
      _systemEventLogger = systemEventLogger;
      _logger = logger;
    }

    public override object Execute(IDbContext context, LogoutCommand command, string partnerId)
    {
      try
      {
        _systemEventLogger.LogEvent(partnerId, "LOGOUT"); // make sure it's first
        _partnerRegistry.LogoutUser(partnerId);
        _logger.Debug($"Successfully logout user with partner ID: {partnerId}");
        return true;
      }
      catch (Exception ex)
      {
        _logger.Error($"Failed to logout user with partner ID: {partnerId}. Exception was thrown: {ex.Message}");
        return false;
      }
    }
  }
}
