﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Logging;
using Kalisto.Mailing.Notifications.Configuration;
using System;

namespace Kalisto.Backend.Operations.Shell.Commands
{
  public class StoreNotificationSettingsCommandHandler : CommandHandlerBase<StoreNotificationSettingsCommand>
  {
    private readonly ILogger _logger;

    private readonly NotificationSettingsConfigurationManager _configurationManager;

    public StoreNotificationSettingsCommandHandler(ILogger logger, NotificationSettingsConfigurationManager configurationManager)
    {
      _logger = logger;
      _configurationManager = configurationManager;
    }

    public override object Execute(IDbContext context, StoreNotificationSettingsCommand command, string partnerId)
    {
      try
      {
        if (command.NotificationSettings == null)
        {
          _logger.Error("Cannot store notification settings because it is null");
          throw new ArgumentException(@"Notification settings cannot be null", nameof(command.NotificationSettings));
        }

        _configurationManager.Save(command.NotificationSettings);
        _logger.Debug(@"Successfully stored notification settings.");

        return true;
      }
      catch (Exception e)
      {
        _logger.Error($@"Could not store notification settings. Given exception was thrown: {e.Message}");
        throw;
      }
    }
  }
}
