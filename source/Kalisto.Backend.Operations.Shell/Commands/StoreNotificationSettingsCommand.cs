﻿using Kalisto.Communication.Core.Messages;
using System;
using Kalisto.Mailing.Notifications.Configuration;

namespace Kalisto.Backend.Operations.Shell.Commands
{
  [Serializable]
  public class StoreNotificationSettingsCommand : BackendDbOperationMessage
  {
    public NotificationSettings NotificationSettings { get; set; }
  }
}
