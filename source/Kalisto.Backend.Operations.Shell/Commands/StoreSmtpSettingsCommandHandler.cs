﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Logging;
using Kalisto.Mailing.Configuration;
using System;

namespace Kalisto.Backend.Operations.Shell.Commands
{
  public class StoreSmtpSettingsCommandHandler : CommandHandlerBase<StoreSmtpSettingsCommand>
  {
    private readonly ILogger _logger;

    private readonly SmtpSettingsConfigurationManager _configurationManager;

    public StoreSmtpSettingsCommandHandler(ILogger logger, SmtpSettingsConfigurationManager configurationManager)
    {
      _logger = logger;
      _configurationManager = configurationManager;
    }

    public override object Execute(IDbContext context, StoreSmtpSettingsCommand command, string partnerId)
    {
      try
      {
        if (command.SmtpSettings == null)
        {
          _logger.Error("Cannot store SMPT settings because it is null");
          throw new ArgumentException(@"SMTP settings cannot be null", nameof(command.SmtpSettings));
        }

        _configurationManager.Save(command.SmtpSettings);
        _logger.Debug(@"Successfully stored SMTP settings.");
        return true;
      }
      catch (Exception e)
      {
        _logger.Error($@"Could not store SMTP settings. Given exception was thrown: {e.Message}");
        throw;
      }
    }
  }
}
