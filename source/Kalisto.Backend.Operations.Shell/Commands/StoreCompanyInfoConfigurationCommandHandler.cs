﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Configuration.CompanyInfo;
using Kalisto.Logging;
using System;

namespace Kalisto.Backend.Operations.Shell.Commands
{
  public class StoreCompanyInfoConfigurationCommandHandler : CommandHandlerBase<StoreCompanyInfoConfigurationCommand>
  {
    private readonly ILogger _logger;

    private readonly CompanyInfoConfigurationManager _configurationManager;

    public StoreCompanyInfoConfigurationCommandHandler(ILogger logger, CompanyInfoConfigurationManager configurationManager)
    {
      _logger = logger;
      _configurationManager = configurationManager;
    }

    public override object Execute(IDbContext context, StoreCompanyInfoConfigurationCommand command, string partnerId)
    {
      try
      {
        if (command.CompanyInfoConfiguration == null)
        {
          _logger.Error("Cannot store company info configuration because it is null");
          throw new ArgumentException(@"Company Info configuration cannot be null", nameof(command.CompanyInfoConfiguration));
        }

        _configurationManager.Save(command.CompanyInfoConfiguration);
        _logger.Debug(@"Successfully stored Company Info configuration.");
        return true;
      }
      catch (Exception e)
      {
        _logger.Error($@"Could not store Company Info configuration. Given exception was thrown: {e.Message}");
        throw;
      }
    }
  }
}
