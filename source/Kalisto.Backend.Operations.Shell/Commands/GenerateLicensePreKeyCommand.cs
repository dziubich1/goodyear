﻿using System;
using Kalisto.Communication.Core.Messages;

namespace Kalisto.Backend.Operations.Shell.Commands
{
  [Serializable]
  public class GenerateLicensePreKeyCommand : BackendDbOperationMessage
  {
    public int StationsCount { get; set; }
  }
}
