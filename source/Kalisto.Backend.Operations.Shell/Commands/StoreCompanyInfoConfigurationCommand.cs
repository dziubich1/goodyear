﻿using Kalisto.Communication.Core.Messages;
using Kalisto.Configuration.CompanyInfo;
using System;

namespace Kalisto.Backend.Operations.Shell.Commands
{
  [Serializable]
  public class StoreCompanyInfoConfigurationCommand : BackendDbOperationMessage
  {
    public CompanyInfoConfiguration CompanyInfoConfiguration { get; set; }
  }
}
