﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Licensing;

namespace Kalisto.Backend.Operations.Shell.Commands
{
  public class GenerateLicensePreKeyCommandHandler : CommandHandlerBase<GenerateLicensePreKeyCommand>
  {
    private readonly ILicenseInfo _licenseInfo;

    public GenerateLicensePreKeyCommandHandler(ILicenseInfo licenseInfo)
    {
      _licenseInfo = licenseInfo;
    }

    public override object Execute(IDbContext context, GenerateLicensePreKeyCommand command, string partnerId)
    {
      return _licenseInfo.GetLicensePreKey(command.StationsCount);
    }
  }
}
