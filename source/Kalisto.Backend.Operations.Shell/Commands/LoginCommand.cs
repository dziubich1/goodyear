﻿using Kalisto.Communication.Core.Messages;

namespace Kalisto.Backend.Operations.Shell.Commands
{
  public class LoginCommand : BackendDbOperationMessage
  {
    public string Username { get; set; }

    public string Password { get; set; }
  }
}
