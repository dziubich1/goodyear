﻿using System.Data.Entity;
using Kalisto.Backend.DataAccess.Model;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Core.Utils;
using System.Linq;
using Kalisto.Backend.Operations.Shell.Exceptions;
using Kalisto.Configuration.Permissions;
using Kalisto.Communication.Core.Registries;
using Kalisto.Communication.Core.Logging;
using Kalisto.Licensing;
using Kalisto.Logging;

namespace Kalisto.Backend.Operations.Shell.Commands
{
  public class LoginCommandHandler : CommandHandlerBase<LoginCommand>
  {
    private readonly PermissionRules _permissionRulesConfiguration;
    private readonly IPartnerRegistry _partnerRegistry;
    private readonly ISystemEventLogger _systemEventLogger;
    private readonly ILicenseInfo _licenseInfo;
    private readonly ILogger _logger;

    public LoginCommandHandler(PermissionRules permissionRulesConfiguration,
      IPartnerRegistry partnerRegistry, ISystemEventLogger systemEventLogger, ILicenseInfo licenseInfo, ILogger logger)
    {
      _permissionRulesConfiguration = permissionRulesConfiguration;
      _partnerRegistry = partnerRegistry;
      _systemEventLogger = systemEventLogger;
      _licenseInfo = licenseInfo;
      _logger = logger;
    }

    public override object Execute(IDbContext context, LoginCommand command, string partnerId)
    {
      if(_licenseInfo.GetLicenseInfo().Type == LicenseType.None)
      {
        _logger.Error("Cannot login user because there is no license key");
        throw new NoLicenseKeyDetectedException();
      }

      var user = context.Users.Include(c => c.Role)
        .FirstOrDefault(c => c.Name == command.Username);
      if (user == null)
        return null;

      var isPasswordValid = PasswordHasher.VerifyPassword(command.Password, user.PasswordHash);
      if (!isPasswordValid)
        return null;

      _partnerRegistry.LoginUser(partnerId, user);
      _systemEventLogger.LogEvent(partnerId, "LOGIN");
      _logger.Debug($"Successfully login user: {user.Name} with partner ID: {partnerId}");

      return new UserInfo
      {
        UserRole = user.Role.Type,
        Username = user.Name,
        Permissions = _permissionRulesConfiguration.UserPermissions
          .FirstOrDefault(c => c.UserRole == user.Role.Type)?.Permissions
      };
    }
  }
}
