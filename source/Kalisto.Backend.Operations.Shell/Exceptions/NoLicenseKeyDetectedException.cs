﻿using Kalisto.Communication.Core.Exceptions;
using System;
using System.Runtime.Serialization;

namespace Kalisto.Backend.Operations.Shell.Exceptions
{
  [Serializable]
  public class NoLicenseKeyDetectedException : ExceptionBase
  {
    public NoLicenseKeyDetectedException()
    {
    }

    // REMARKS: below constructor need to be added due to the serialization/deserialization issues
    public NoLicenseKeyDetectedException(SerializationInfo info, StreamingContext context)
    {
    }

    public override string ExceptionCode => "LICENSING_1002";

    public override string Message => GetLocalizedMessage();
  }
}
