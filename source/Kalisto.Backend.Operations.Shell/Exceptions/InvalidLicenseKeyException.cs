﻿using System;
using System.Runtime.Serialization;
using Kalisto.Communication.Core.Exceptions;

namespace Kalisto.Backend.Operations.Shell.Exceptions
{
  [Serializable]
  public class InvalidLicenseKeyException : ExceptionBase
  {
    public InvalidLicenseKeyException()
    {
    }

    // REMARKS: below constructor need to be added due to the serialization/deserialization issues
    public InvalidLicenseKeyException(SerializationInfo info, StreamingContext context)
    {
    }

    public override string ExceptionCode => "LICENSING_1001";

    public override string Message => GetLocalizedMessage();
  }
}
