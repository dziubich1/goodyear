﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Mailing.Notifications.Configuration;

namespace Kalisto.Backend.Operations.Shell.Queries
{
  public class GetNotificationSettingsQueryHandler : QueryHandlerBase<GetNotificationSettingsQuery>
  {
    private readonly NotificationSettingsConfigurationManager _configurationManager;

    public GetNotificationSettingsQueryHandler(NotificationSettingsConfigurationManager configurationManager)
    {
      _configurationManager = configurationManager;
    }

    public override object Execute(IDbContext context, GetNotificationSettingsQuery query, string partnerId)
    {
      return _configurationManager.Load<NotificationSettings>();
    }
  }
}
