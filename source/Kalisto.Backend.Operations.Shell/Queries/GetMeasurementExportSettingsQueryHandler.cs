﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Configuration.MeasurementExportSettings;

namespace Kalisto.Backend.Operations.Shell.Queries
{
  public class GetMeasurementExportSettingsQueryHandler : QueryHandlerBase<GetMeasurementExportSettingsQuery>
  {
    private readonly MeasurementExportConfigurationManager _configurationManager;

    public GetMeasurementExportSettingsQueryHandler(MeasurementExportConfigurationManager configurationManager)
    {
      _configurationManager = configurationManager;
    }

    public override object Execute(IDbContext context, GetMeasurementExportSettingsQuery query, string partnerId)
    {
      return _configurationManager.Load();
    }
  }
}
