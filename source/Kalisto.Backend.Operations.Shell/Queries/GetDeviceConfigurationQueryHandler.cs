﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Configuration.Devices;

namespace Kalisto.Backend.Operations.Shell.Queries
{
  public class GetDeviceConfigurationQueryHandler : QueryHandlerBase<GetDeviceConfigurationQuery>
  {
    private readonly ServerDeviceConfigurationManager _serverDeviceConfigurationManager;

    public GetDeviceConfigurationQueryHandler(ServerDeviceConfigurationManager deviceConfigurationManager)
    {
      _serverDeviceConfigurationManager = deviceConfigurationManager;
    }

    public override object Execute(IDbContext context, GetDeviceConfigurationQuery query, string partnerId)
    {
      return _serverDeviceConfigurationManager.Load();
    }
  }
}
