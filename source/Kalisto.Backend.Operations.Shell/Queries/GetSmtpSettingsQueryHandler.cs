﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Logging;
using Kalisto.Mailing.Configuration;

namespace Kalisto.Backend.Operations.Shell.Queries
{
  public class GetSmtpSettingsQueryHandler : QueryHandlerBase<GetSmtpSettingsQuery>
  {
    private readonly SmtpSettingsConfigurationManager _configurationManager;

    public GetSmtpSettingsQueryHandler(SmtpSettingsConfigurationManager configurationManager)
    {
      _configurationManager = configurationManager;
    }

    public override object Execute(IDbContext context, GetSmtpSettingsQuery query, string partnerId)
    {
      return _configurationManager.Load<SmtpSettings>();
    }
  }
}
