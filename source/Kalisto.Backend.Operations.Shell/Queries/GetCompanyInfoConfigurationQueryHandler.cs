﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Configuration.CompanyInfo;

namespace Kalisto.Backend.Operations.Shell.Queries
{
  public class GetCompanyInfoConfigurationQueryHandler : QueryHandlerBase<GetCompanyInfoConfigurationQuery>
  {
    private readonly CompanyInfoConfigurationManager _configurationManager;

    public GetCompanyInfoConfigurationQueryHandler(CompanyInfoConfigurationManager configurationManager)
    {
      _configurationManager = configurationManager;
    }

    public override object Execute(IDbContext context, GetCompanyInfoConfigurationQuery query, string partnerId)
    {
      return _configurationManager.Load();
    }
  }
}
