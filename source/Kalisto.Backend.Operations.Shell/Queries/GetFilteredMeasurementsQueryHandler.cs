﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Communication.Core.Messaging;
using System.Data.Entity;
using System.Linq;

namespace Kalisto.Backend.Operations.Shell.Queries
{
  public class GetFilteredMeasurementsQueryHandler : QueryHandlerBase<GetFilteredMeasurementsQuery>
  {
    public override object Execute(IDbContext context, GetFilteredMeasurementsQuery query, string partnerId)
    {
      var dataQuery = context.Measurements
        .Include(m => m.Cargo)
        .Include(m => m.Contractor)
        .Include(m => m.Vehicle)
        .Include(m => m.Driver)
        .AsQueryable();

      dataQuery = dataQuery
        .Where(m => m.Date1Utc >= query.FromUtc && m.Date1Utc <= query.ToUtc)
        .Where(m => m.Date2Utc == null || m.Date2Utc >= query.FromUtc && m.Date2Utc <= query.ToUtc);

      if (query.MeasurementClass != null)
        dataQuery = dataQuery
          .Where(m => m.Class == query.MeasurementClass);

      if (query.MeasurementType != null)
        dataQuery = dataQuery
          .Where(m => m.Type == query.MeasurementType);

      if (query.Cargoes != null)
        dataQuery = dataQuery
          .Where(m => query.Cargoes.Contains(m.Cargo.Id));

      if (query.Contractors != null)
        dataQuery = dataQuery
          .Where(m => query.Contractors.Contains(m.Contractor.Id));

      if (query.Vehicles != null)
        dataQuery = dataQuery
          .Where(m => query.Vehicles.Contains(m.Vehicle.Id));

      if (query.Drivers != null)
        dataQuery = dataQuery
          .Where(m => query.Drivers.Contains(m.Driver.Id));

      if (query.SortByOption == DataAccess.Model.Enum.SortBy.MeasurementDate)
        dataQuery = dataQuery
          .OrderBy(m => m.Date1Utc)
          .ThenBy(m => m.Date2Utc);
      else if (query.SortByOption == DataAccess.Model.Enum.SortBy.CargoName)
        dataQuery = dataQuery
          .OrderBy(m => m.Cargo.Name);

      var measurements = dataQuery.AsNoTracking().ToList();
      return measurements;
    }
  }
}
