﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.DataAccess.Model.Enum;
using Kalisto.Communication.Core.Messages;
using System;
using System.Collections.Generic;

namespace Kalisto.Backend.Operations.Shell.Queries
{
  public class GetFilteredMeasurementsQuery : BackendDbOperationMessage
  {
    public DateTime FromUtc { get; set; }

    public DateTime ToUtc { get; set; }

    public MeasurementClass? MeasurementClass { get; set; }

    public MeasurementType? MeasurementType { get; set; }

    public SortBy? SortByOption { get; set; }

    public IEnumerable<long> Cargoes { get; set; }

    public IEnumerable<long> Contractors { get; set; }

    public IEnumerable<long> Drivers { get; set; }

    public IEnumerable<long> Vehicles { get; set; }
  }
}
