﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace Kalisto.Configuration
{
  [Obsolete]
  public abstract class ConfigurationManager
  {
    private readonly string _appName;

    private readonly string _domainName;

    protected abstract string DirectoryDataPath { get; }

    protected virtual string ConfigFileName => "Config.xml";

    public string ConfigDirectory => Path.Combine(DirectoryDataPath, _domainName, _appName);

    public ConfigurationManager(string domainName, string appName)
    {
      _appName = appName;
      _domainName = domainName;
    }

    public void Save<T>(T configuration)
    {
      var serializer = new XmlSerializer(typeof(T));
      var path = Path.Combine(ConfigDirectory, ConfigFileName);
      object existingFileContent = null;
      if (File.Exists(path))
      {
        using (var reader = new StreamReader(path))
          existingFileContent = serializer.Deserialize(reader);

        File.SetAttributes(path, FileAttributes.Normal);
        File.Delete(path);
      }

      try
      {
        var directoryPath = Path.GetDirectoryName(path);
        if (string.IsNullOrEmpty(directoryPath))
          throw new InvalidOperationException();

        if (!Directory.Exists(directoryPath))
          Directory.CreateDirectory(directoryPath);

        using (var writer = new StreamWriter(path))
        {
          serializer.Serialize(writer, configuration);
        }
      }
      catch
      {
        using (var writer = new StreamWriter(path))
        {
          if (existingFileContent != null)
            serializer.Serialize(writer, existingFileContent);
        }
      }
    }

    public T Load<T>() where T : ConfigurationBase, new()
    {
      var serializer = new XmlSerializer(typeof(T));
      var path = Path.Combine(DirectoryDataPath, _domainName, _appName, ConfigFileName);
      if (!File.Exists(path))
      {
        var defaultConfiguration = new T().GetDefaultConfiguration() as T;
        var directoryPath = Path.GetDirectoryName(path);
        if (string.IsNullOrEmpty(directoryPath))
          throw new InvalidOperationException();

        if (!Directory.Exists(directoryPath))
          Directory.CreateDirectory(directoryPath);

        using (var writer = new StreamWriter(path))
        {
          serializer.Serialize(writer, defaultConfiguration);
        }
      }

      using (var reader = new StreamReader(path))
      {
        return (T)serializer.Deserialize(reader);
      }
    }
  }

  public abstract class ConfigurationManager<TConfig> where TConfig : ConfigurationBase, new()
  {
    private readonly string _appName;

    private readonly string _domainName;

    protected abstract string DirectoryDataPath { get; }

    protected abstract string ConfigFileName { get; }

    public string ConfigDirectory => Path.Combine(DirectoryDataPath, _domainName, _appName);

    protected ConfigurationManager(string domainName, string appName)
    {
      _appName = appName;
      _domainName = domainName;
    }

    public virtual void Save(TConfig configuration)
    {
      var serializer = new XmlSerializer(typeof(TConfig));
      var path = Path.Combine(ConfigDirectory, ConfigFileName);
      object existingFileContent = null;
      if (File.Exists(path))
      {
        using (var reader = new StreamReader(path))
          existingFileContent = serializer.Deserialize(reader);

        File.SetAttributes(path, FileAttributes.Normal);
        File.Delete(path);
      }

      try
      {
        var directoryPath = Path.GetDirectoryName(path);
        if (string.IsNullOrEmpty(directoryPath))
          throw new InvalidOperationException();

        if (!Directory.Exists(directoryPath))
          Directory.CreateDirectory(directoryPath);

        using (var writer = new StreamWriter(path))
        {
          serializer.Serialize(writer, configuration);
        }
      }
      catch
      {
        using (var writer = new StreamWriter(path))
        {
          if (existingFileContent != null)
            serializer.Serialize(writer, existingFileContent);
        }
      }
    }

    public virtual TConfig Load()
    {
      var serializer = new XmlSerializer(typeof(TConfig));
      var path = Path.Combine(DirectoryDataPath, _domainName, _appName, ConfigFileName);
      if (!File.Exists(path))
      {
        var defaultConfiguration = new TConfig().GetDefaultConfiguration() as TConfig;
        var directoryPath = Path.GetDirectoryName(path);
        if (string.IsNullOrEmpty(directoryPath))
          throw new InvalidOperationException();

        if (!Directory.Exists(directoryPath))
          Directory.CreateDirectory(directoryPath);

        using (var writer = new StreamWriter(path))
        {
          serializer.Serialize(writer, defaultConfiguration);
        }
      }

      using (var reader = new StreamReader(path))
      {
        return (TConfig)serializer.Deserialize(reader);
      }
    }
  }
}
