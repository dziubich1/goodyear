﻿namespace Kalisto.Configuration.Enums
{
  public enum DeviceType
  {
    External, Local, Transparent
  }
}
