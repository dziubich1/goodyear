﻿namespace Kalisto.Configuration.Enums
{
  public enum ConnectionType
  {
    Tcp, Https, Serial
  }
}
