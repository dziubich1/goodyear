﻿using System;

namespace Kalisto.Configuration
{
  [Serializable]
  public abstract class ConfigurationBase
  {
    public abstract object GetDefaultConfiguration();
  }
}
