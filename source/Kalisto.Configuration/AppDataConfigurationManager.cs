﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kalisto.Configuration
{
  [Obsolete]
  public class AppDataConfigurationManager : ConfigurationManager
  {
    public AppDataConfigurationManager(string domainName, string appName) : base(domainName, appName)
    {
    }

    protected override string DirectoryDataPath => Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
  }

  public abstract class AppDataConfigurationManager<TConfig> : ConfigurationManager<TConfig> where TConfig : ConfigurationBase, new()
  {
    protected AppDataConfigurationManager(string domainName, string appName) : base(domainName, appName)
    {
    }

    protected override string DirectoryDataPath => Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
  }
}
