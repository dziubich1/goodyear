﻿using System;

namespace Kalisto.Configuration.CompanyInfo
{
  [Serializable]
  public class CompanyInfoConfiguration : ConfigurationBase
  {
    public string TaxNumber { get; set; }

    public string Name { get; set; }

    public string Street { get; set; }

    public string Postcode { get; set; }

    public string City { get; set; }

    public string AdditionalInfo { get; set; }

    public override object GetDefaultConfiguration()
    {
      return new CompanyInfoConfiguration();
    }
  }
}
