﻿namespace Kalisto.Configuration.CompanyInfo
{
  public class CompanyInfoConfigurationManager : ProgramDataConfigurationManager<CompanyInfoConfiguration>
  {
    protected override string ConfigFileName => "CompanyInfo.xml";

    public CompanyInfoConfigurationManager(string domainName, string appName)
      : base(domainName, appName)
    {
    }
  }
}
