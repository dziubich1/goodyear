﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kalisto.Configuration.MeasurementExportSettings
{
  public class MeasurementExportConfigurationManager : ProgramDataConfigurationManager<MeasurementExportConfiguration>
  {
    protected override string ConfigFileName => "MeasurementExportSettings.xml";


    public MeasurementExportConfigurationManager(string domainName, string appName)
      : base(domainName, appName)
    {
    }
  }
}
