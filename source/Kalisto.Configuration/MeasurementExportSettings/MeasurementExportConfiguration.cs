﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kalisto.Configuration.MeasurementExportSettings
{
  public class MeasurementExportConfiguration : ConfigurationBase
  {
    public bool IsEnabled { get; set; }

    public override object GetDefaultConfiguration()
    {
      return new MeasurementExportConfiguration()
      {
        IsEnabled = false
      };
    }
  }
}
