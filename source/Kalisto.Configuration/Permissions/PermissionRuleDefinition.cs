﻿using System;

namespace Kalisto.Configuration.Permissions
{
  [Serializable]
  public class PermissionRuleDefinition
  {
    public string Name { get; set; }
  }
}
