﻿namespace Kalisto.Configuration.Permissions
{
  public enum PermissionType
  {
    None, ReadOnly, Full
  }
}
