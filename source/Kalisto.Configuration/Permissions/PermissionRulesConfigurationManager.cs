﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace Kalisto.Configuration.Permissions
{
  public class PermissionRulesConfigurationManager
  {
    private readonly string _appName;

    private readonly string _domainName;

    private string ConfigFileName => "PermissionRules.xml";

    private string AppDataPath => Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);

    public string ConfigDirectory => Path.Combine(AppDataPath, _domainName, _appName);

    public PermissionRulesConfigurationManager(string domainName, string appName)
    {
      _appName = appName;
      _domainName = domainName;
    }

    public void Save(PermissionRules rules)
    {
      var serializer = new XmlSerializer(typeof(PermissionRules));
      var path = Path.Combine(ConfigDirectory, ConfigFileName);
      object existingFileContent = null;
      if (File.Exists(path))
      {
        using (var reader = new StreamReader(path))
          existingFileContent = serializer.Deserialize(reader);

        File.SetAttributes(path, FileAttributes.Normal);
        File.Delete(path);
      }

      try
      {
        var directoryPath = Path.GetDirectoryName(path);
        if (string.IsNullOrEmpty(directoryPath))
          throw new InvalidOperationException();

        if (!Directory.Exists(directoryPath))
          Directory.CreateDirectory(directoryPath);

        using (var writer = new StreamWriter(path))
        {
          serializer.Serialize(writer, rules);
        }
      }
      catch
      {
        using (var writer = new StreamWriter(path))
        {
          if (existingFileContent != null)
            serializer.Serialize(writer, existingFileContent);
        }
      }
    }

    public PermissionRules Load()
    {
      var serializer = new XmlSerializer(typeof(PermissionRules));
      var path = Path.Combine(AppDataPath, _domainName, _appName, ConfigFileName);
      if (!File.Exists(path))
      {
        var defaultConfiguration = PermissionRules.Default;
        var directoryPath = Path.GetDirectoryName(path);
        if (string.IsNullOrEmpty(directoryPath))
          throw new InvalidOperationException();

        if (!Directory.Exists(directoryPath))
          Directory.CreateDirectory(directoryPath);

        using (var writer = new StreamWriter(path))
        {
          serializer.Serialize(writer, defaultConfiguration);
        }
      }

      using (var reader = new StreamReader(path))
      {
        return (PermissionRules)serializer.Deserialize(reader);
      }
    }
  }
}
