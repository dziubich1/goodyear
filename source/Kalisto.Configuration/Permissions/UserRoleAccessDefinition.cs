﻿using System;
using System.Collections.Generic;

namespace Kalisto.Configuration.Permissions
{
  [Serializable]
  public class UserRoleAccessDefinition
  {
    public int UserRole { get; set; }

    public List<PermissionRule> Permissions { get; set; }
  }
}
