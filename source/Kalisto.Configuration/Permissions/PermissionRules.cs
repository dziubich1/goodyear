﻿using System;
using System.Collections.Generic;

namespace Kalisto.Configuration.Permissions
{
  [Serializable]
  public class PermissionRules
  {
    public const string UserManagementRule = "USER_MANAGEMENT";

    public const string AddWeighingRule = "ADD_WEIGHING";

    public const string CancelWeighingRule = "CANCEL_WEIGHING_RULE";

    public const string UpdateWeighingRule = "UPDATE_WEIGHING_RULE";

    public const string DeleteWeighingRule = "DELETE_WEIGHING_RULE";

    public const string LogsBrowsingRule = "LOGS_BROWSING";

    public const string MeasurementDateChange = "MEASUREMENT_DATE_CHANGE";

    public const string DictionaryDataManagement = "DICT_DATA_MANAGEMENT";

    public const string LicenseManagement = "LICENSE_MANAGEMENT";

    public const string EmailPreferencesManagement = "EMAIL_PREFERENCES_MANAGEMENT";

    public const string MeasurementsCollationRule = "MEASUREMENT_COLLATION";

    public const string UpdateBeltConveyorMeasurementRule = "UPDATE_BELT_CONVEYOR_MEAS";

    public const string UpdateCompanyData = "UPDATE_COMPANY_DATA";

    public const string UpdateWeightToleranceRule = "UPDATE_WEIGHT_TOLERANCE_RULE";

    public const string ExportXmlMeasurements = "EXPORT_XML_MEASUREMENTS";

    public List<UserRoleAccessDefinition> UserPermissions { get; set; }

    public PermissionRules()
    {
      UserPermissions = new List<UserRoleAccessDefinition>();
    }

    public static PermissionRules Default => GetDefaultRules();

    private static PermissionRules GetDefaultRules()
    {
      return new PermissionRules
      {
        UserPermissions = new List<UserRoleAccessDefinition>
        {
          new UserRoleAccessDefinition
          {
            UserRole = 0,
            Permissions = new List<PermissionRule>
            {
              new PermissionRule
              {
                Type = PermissionType.Full,
                Definition = new PermissionRuleDefinition
                {
                  Name = UserManagementRule
                }
              },
              new PermissionRule
              {
                Type = PermissionType.Full,
                Definition = new PermissionRuleDefinition
                {
                  Name = AddWeighingRule
                }
              },
              new PermissionRule
              {
                Type = PermissionType.Full,
                Definition = new PermissionRuleDefinition
                {
                  Name = CancelWeighingRule
                }
              },
              new PermissionRule
              {
                Type = PermissionType.Full,
                Definition = new PermissionRuleDefinition
                {
                  Name = DeleteWeighingRule
                }
              },
              new PermissionRule
              {
                Type = PermissionType.Full,
                Definition = new PermissionRuleDefinition
                {
                  Name = UpdateWeighingRule
                }
              },
              new PermissionRule
              {
                Type = PermissionType.Full,
                Definition = new PermissionRuleDefinition
                {
                  Name = LogsBrowsingRule
                }
              },
              new PermissionRule
              {
                Type = PermissionType.Full,
                Definition = new PermissionRuleDefinition
                {
                  Name = DictionaryDataManagement
                }
              },
              new PermissionRule
              {
                Type = PermissionType.Full,
                Definition = new PermissionRuleDefinition
                {
                  Name = LicenseManagement
                }
              },
              new PermissionRule
              {
                Type = PermissionType.Full,
                Definition = new PermissionRuleDefinition
                {
                  Name = EmailPreferencesManagement
                }
              },
              new PermissionRule
              {
                Type = PermissionType.Full,
                Definition = new PermissionRuleDefinition
                {
                  Name = MeasurementsCollationRule
                }
              },
              new PermissionRule
              {
                Type = PermissionType.Full,
                Definition = new PermissionRuleDefinition
                {
                  Name = MeasurementDateChange
                }
              },
              new PermissionRule
              {
                Type = PermissionType.Full,
                Definition = new PermissionRuleDefinition
                {
                  Name = UpdateBeltConveyorMeasurementRule
                }
              },
              new PermissionRule
              {
                Type = PermissionType.Full,
                Definition = new PermissionRuleDefinition
                {
                  Name = UpdateCompanyData
                }
              },
              new PermissionRule
              {
                Type = PermissionType.Full,
                Definition = new PermissionRuleDefinition
                {
                  Name = UpdateWeightToleranceRule
                }
              },
              new PermissionRule
              {
                Type = PermissionType.Full,
                Definition = new PermissionRuleDefinition
                {
                  Name = ExportXmlMeasurements
                }
              }
            }
          },
          new UserRoleAccessDefinition
          {
            UserRole = 1,
            Permissions = new List<PermissionRule>
            {
              new PermissionRule
              {
                Type = PermissionType.Full,
                Definition = new PermissionRuleDefinition
                {
                  Name = AddWeighingRule
                }
              },
              new PermissionRule
              {
                Type = PermissionType.Full,
                Definition = new PermissionRuleDefinition
                {
                  Name = DictionaryDataManagement
                }
              },
              new PermissionRule
              {
                Type = PermissionType.Full,
                Definition = new PermissionRuleDefinition
                {
                  Name = CancelWeighingRule
                }
              },
              new PermissionRule
              {
                Type = PermissionType.ReadOnly,
                Definition = new PermissionRuleDefinition
                {
                  Name = DeleteWeighingRule
                }
              },
              new PermissionRule
              {
                Type = PermissionType.Full,
                Definition = new PermissionRuleDefinition
                {
                  Name = UpdateWeighingRule
                }
              },
              new PermissionRule
              {
                Type = PermissionType.None,
                Definition = new PermissionRuleDefinition
                {
                  Name = LogsBrowsingRule
                }
              },
              new PermissionRule
              {
                Type = PermissionType.None,
                Definition = new PermissionRuleDefinition
                {
                  Name = LicenseManagement
                }
              },
              new PermissionRule
              {
                Type = PermissionType.None,
                Definition = new PermissionRuleDefinition
                {
                  Name = EmailPreferencesManagement
                }
              },
              new PermissionRule
              {
                Type = PermissionType.Full,
                Definition = new PermissionRuleDefinition
                {
                  Name = MeasurementsCollationRule
                }
              },
              new PermissionRule
              {
                Type = PermissionType.ReadOnly,
                Definition = new PermissionRuleDefinition
                {
                  Name = MeasurementDateChange
                }
              },
              new PermissionRule
              {
                Type = PermissionType.ReadOnly,
                Definition = new PermissionRuleDefinition
                {
                  Name = UpdateBeltConveyorMeasurementRule
                }
              },
              new PermissionRule
              {
                Type = PermissionType.None,
                Definition = new PermissionRuleDefinition
                {
                  Name = UpdateCompanyData
                }
              },
              new PermissionRule
              {
                Type = PermissionType.ReadOnly,
                Definition = new PermissionRuleDefinition
                {
                  Name = UpdateWeightToleranceRule
                }
              },
              new PermissionRule
              {
                Type = PermissionType.None,
                Definition = new PermissionRuleDefinition
                {
                  Name = ExportXmlMeasurements
                }
              }
            }
          },
          new UserRoleAccessDefinition
          {
            UserRole = 2,
            Permissions = new List<PermissionRule>
            {
              new PermissionRule
              {
                Type = PermissionType.Full,
                Definition = new PermissionRuleDefinition
                {
                  Name = AddWeighingRule
                }
              },
              new PermissionRule
              {
                Type = PermissionType.None,
                Definition = new PermissionRuleDefinition
                {
                  Name = LogsBrowsingRule
                }
              },
              new PermissionRule
              {
                Type = PermissionType.ReadOnly,
                Definition = new PermissionRuleDefinition
                {
                  Name = CancelWeighingRule
                }
              },
              new PermissionRule
              {
                Type = PermissionType.ReadOnly,
                Definition = new PermissionRuleDefinition
                {
                  Name = DeleteWeighingRule
                }
              },
              new PermissionRule
              {
                Type = PermissionType.ReadOnly,
                Definition = new PermissionRuleDefinition
                {
                  Name = UpdateWeighingRule
                }
              },
              new PermissionRule
              {
                Type = PermissionType.ReadOnly,
                Definition = new PermissionRuleDefinition
                {
                  Name = DictionaryDataManagement
                }
              },
              new PermissionRule
              {
                Type = PermissionType.None,
                Definition = new PermissionRuleDefinition
                {
                  Name = LicenseManagement
                }
              },
              new PermissionRule
              {
                Type = PermissionType.None,
                Definition = new PermissionRuleDefinition
                {
                  Name = EmailPreferencesManagement
                }
              },
              new PermissionRule
              {
                Type = PermissionType.Full,
                Definition = new PermissionRuleDefinition
                {
                  Name = MeasurementsCollationRule
                }
              },
              new PermissionRule
              {
                Type = PermissionType.ReadOnly,
                Definition = new PermissionRuleDefinition
                {
                  Name = MeasurementDateChange
                }
              },
              new PermissionRule
              {
                Type = PermissionType.ReadOnly,
                Definition = new PermissionRuleDefinition
                {
                  Name = UpdateBeltConveyorMeasurementRule
                }
              },
              new PermissionRule
              {
                Type = PermissionType.None,
                Definition = new PermissionRuleDefinition
                {
                  Name = UpdateCompanyData
                }
              },
              new PermissionRule
              {
                Type = PermissionType.ReadOnly,
                Definition = new PermissionRuleDefinition
                {
                  Name = UpdateWeightToleranceRule
                }
              },
              new PermissionRule
              {
                Type = PermissionType.None,
                Definition = new PermissionRuleDefinition
                {
                  Name = ExportXmlMeasurements
                }
              },
            }
          }
        }
      };
    }
  }
}
