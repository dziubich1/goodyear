﻿using System;

namespace Kalisto.Configuration.Permissions
{
  [Serializable]
  public class PermissionRule
  {
    public PermissionRuleDefinition Definition { get; set; }

    public PermissionType Type { get; set; }
  }
}
