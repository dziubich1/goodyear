﻿using System;
using System.IO;
using System.Xml.Serialization;

namespace Kalisto.Configuration
{
  [Obsolete]
  public class ProgramDataConfigurationManager : ConfigurationManager
  {
    public ProgramDataConfigurationManager(string domainName, string appName) : base(domainName, appName)
    {
    }

    protected override string DirectoryDataPath => Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);
  }

  public abstract class ProgramDataConfigurationManager<TConfig> : ConfigurationManager<TConfig> where TConfig : ConfigurationBase, new() 
  {
    protected ProgramDataConfigurationManager(string domainName, string appName) : base(domainName, appName)
    {
    }

    protected override string DirectoryDataPath => Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);
  }
}
