﻿using System;
using System.Xml.Serialization;
using Kalisto.Configuration.Devices.Connection;
using Kalisto.Configuration.Enums;

namespace Kalisto.Configuration.Devices
{
  [Serializable]
  [XmlInclude(typeof(WeightMeterConfiguration))]
  [XmlInclude(typeof(CameraConfiguration))]
  [XmlInclude(typeof(BarcodeScannerConfiguration))]
  [XmlInclude(typeof(BeltConveyorCounterConfiguration))]
  [XmlInclude(typeof(CardReaderConfiguration))]
  [XmlInclude(typeof(IOControllerConfiguration))]
  public class DeviceConfiguration
  {
    public int Id { get; set; }

    public string Name { get; set; }

    public DeviceType Type { get; set; }

    public ConnectionType ConnectionType { get; set; }

    public ConnectionConfiguration ConnectionConfiguration { get; set; }

    public bool IsActive { get; set; }
  }
}
