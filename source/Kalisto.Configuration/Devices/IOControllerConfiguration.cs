﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kalisto.Configuration.Devices
{
  public class IOControllerConfiguration : DeviceConfiguration
  {
    public bool IsWeighingDeviceActive { get; set; }

    public List<BarrierConfiguration> Barriers { get; set; }

    public List<TrafficLightsConfiguration> TrafficLights { get; set; }
  }
}
