﻿using System;

namespace Kalisto.Configuration.Devices
{
  [Serializable]
  public class TrafficLightsConfiguration
  {
    public int WeightMeterId { get; set; }

    public int? CardReaderId { get; set; }

    public int GreenOutputNumber { get; set; }

    public int RedOutputNumber { get; set; }
  }
}
