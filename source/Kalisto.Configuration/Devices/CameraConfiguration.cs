﻿using System;

namespace Kalisto.Configuration.Devices
{
  [Serializable]
  public class CameraConfiguration : DeviceConfiguration
  {
    public int WeightMeterId { get; set; }
  }
}
