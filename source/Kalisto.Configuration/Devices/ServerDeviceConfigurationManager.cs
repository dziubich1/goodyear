﻿namespace Kalisto.Configuration.Devices
{
  public class ServerDeviceConfigurationManager : ProgramDataConfigurationManager<ServerDeviceConfiguration>
  {
    protected override string ConfigFileName => "Devices.xml";

    public ServerDeviceConfigurationManager(string domainName, string appName)
      : base(domainName, appName)
    {
    }
  }
}
