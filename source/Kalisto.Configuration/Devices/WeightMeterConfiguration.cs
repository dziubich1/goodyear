﻿using System;

namespace Kalisto.Configuration.Devices
{
  [Serializable]
  public class WeightMeterConfiguration : DeviceConfiguration
  {
    public bool IsStateMachineActive { get; set; }
  }
}
