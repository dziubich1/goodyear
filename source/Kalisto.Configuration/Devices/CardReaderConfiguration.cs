﻿using System;

namespace Kalisto.Configuration.Devices
{
  [Serializable]
  public class CardReaderConfiguration : DeviceConfiguration
  {
    public int WeightMeterId { get; set; }
  }
}
