﻿using Kalisto.Configuration.Devices.Connection;
using Kalisto.Configuration.Enums;
using System;
using System.Collections.Generic;

namespace Kalisto.Configuration.Devices
{
  [Serializable]
  public class ServerDeviceConfiguration : ConfigurationBase
  {
    public List<WeightMeterConfiguration> WeightMeters { get; set; }

    public List<CameraConfiguration> Cameras { get; set; }

    public List<BeltConveyorCounterConfiguration> BeltConveyorCounters { get; set; }

    public List<CardReaderConfiguration> CardReaders { get; set; }

    public List<IOControllerConfiguration> IoControllers { get; set; }

    public List<DeviceConfiguration> GetAllDevices()
    {
      var devices = new List<DeviceConfiguration>();
      devices.AddRange(WeightMeters);
      devices.AddRange(Cameras);
      devices.AddRange(BeltConveyorCounters);
      devices.AddRange(IoControllers);
      devices.AddRange(CardReaders);

      return devices;
    }

    public override object GetDefaultConfiguration()
    {
      return new ServerDeviceConfiguration
      {
        //WeightMeters = new List<WeightMeterConfiguration>
        //{
        //  new WeightMeterConfiguration
        //  {
        //    Id = 1,
        //    Type = DeviceType.External,
        //    ConnectionType = ConnectionType.Tcp,
        //    ConnectionConfiguration = new TcpConnectionConfiguration
        //    {
        //      IpAddress = "127.0.0.1",
        //      Port = 23,
        //    },
        //    IsActive = true
        //  }
        //},
        //BeltConveyorCounters = new List<BeltConveyorCounterConfiguration>
        //{
        //  new BeltConveyorCounterConfiguration
        //  {
        //    Id = 2,
        //    Type = DeviceType.External,
        //    ConnectionType = ConnectionType.Tcp,
        //    ConnectionConfiguration = new TcpConnectionConfiguration
        //    {
        //      IpAddress = "127.0.0.1",
        //      Port = 25,
        //    },
        //    IsActive = false
        //  }
        //},
        //Cameras = new List<CameraConfiguration>
        //{
        //  new CameraConfiguration
        //  {
        //    Id = 3,
        //    WeightMeterId = 1,
        //    Type = DeviceType.Transparent,
        //    ConnectionType = ConnectionType.Https,
        //    ConnectionConfiguration = new HttpConnectionConfiguration
        //    {
        //      Url = "https://placeimg.com/350/250/tech/grayscale",
        //      Username = "admin",
        //      Password = "admin"
        //    },
        //    IsActive = true
        //  }
        //},
        //CardReaders = new List<CardReaderConfiguration>
        //{
        //  new CardReaderConfiguration
        //  {
        //    Id = 4,
        //    WeightMeterId = 1,
        //    Type = DeviceType.External,
        //    ConnectionType = ConnectionType.Serial,
        //    ConnectionConfiguration = new SerialPortConnectionConfiguration
        //    {
        //      Port = "COM3",
        //      BaudRate = 9600,
        //      DataBits = 8,
        //      Parity = 0,
        //      StopBits = 1
        //    },
        //    IsActive = true
        //  }
        //},
        IoControllers = new List<IOControllerConfiguration>
        {
          new IOControllerConfiguration
          {
            Id = 3,
            IsWeighingDeviceActive = true,
            Type = DeviceType.External,
            ConnectionType = ConnectionType.Serial,
            ConnectionConfiguration = new SerialPortConnectionConfiguration
            {
              Port = "COM3",
              BaudRate = 9600,
              DataBits = 8,
              Parity = 0,
              StopBits = 1
            },
            IsActive = true,
            TrafficLights = new List<TrafficLightsConfiguration>
            {
              new TrafficLightsConfiguration
              {
                CardReaderId = 4,
                GreenOutputNumber = 1,
                RedOutputNumber = 2
              }
            }
          }
        }
      };
    }
  }
}
