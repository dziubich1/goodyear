﻿using System;

namespace Kalisto.Configuration.Devices.Connection
{
  [Serializable]
  public class TcpConnectionConfiguration : ConnectionConfiguration
  {
    public string IpAddress { get; set; }

    public int Port { get; set; }
  }
}
