﻿using System;

namespace Kalisto.Configuration.Devices.Connection
{
  [Serializable]
  public class HttpConnectionConfiguration : ConnectionConfiguration
  {
    public string Url { get; set; }

    public string Username { get; set; }

    public string Password { get; set; }
  }
}
