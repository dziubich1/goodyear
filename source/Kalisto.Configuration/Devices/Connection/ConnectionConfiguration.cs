﻿using System;
using System.Xml.Serialization;

namespace Kalisto.Configuration.Devices.Connection
{
  [Serializable]
  [XmlInclude(typeof(TcpConnectionConfiguration))]
  [XmlInclude(typeof(HttpConnectionConfiguration))]
  [XmlInclude(typeof(SerialPortConnectionConfiguration))]
  public class ConnectionConfiguration
  {

  }
}
