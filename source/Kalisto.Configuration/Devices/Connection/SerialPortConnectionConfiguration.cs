﻿using System;

namespace Kalisto.Configuration.Devices.Connection
{
  [Serializable]
  public class SerialPortConnectionConfiguration : ConnectionConfiguration
  {
    //string portName, int baudRate, Parity parity, int dataBits, StopBits stopBits
    public string Port { get; set; }

    public int BaudRate { get; set; }

    /// <summary>No parity check occurs.</summary>
    /// 0 - None,
    /// <summary>Sets the parity bit so that the count of bits set is an odd number.</summary>
    /// 1 - Odd,
    /// <summary>Sets the parity bit so that the count of bits set is an even number.</summary>
    /// 2 - Even,
    /// <summary>Leaves the parity bit set to 1.</summary>
    /// 3 - Mark,
    /// <summary>Leaves the parity bit set to 0.</summary>
    /// 4 - Space,
    public int Parity { get; set; }

    /// <summary>No stop bits are used. </summary>
    /// 0 - None,
    /// <summary>One stop bit is used.</summary>
    /// 1 - One,
    /// <summary>Two stop bits are used.</summary>
    /// 2 - Two,
    /// <summary>1.5 stop bits are used.</summary>
    /// 3 - OnePointFive,
    public int StopBits { get; set; }

    public int DataBits { get; set; }
  }
}
