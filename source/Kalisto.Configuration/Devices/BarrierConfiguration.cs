﻿using System;

namespace Kalisto.Configuration.Devices
{
  [Serializable]
  public class BarrierConfiguration
  {
    public int WeightMeterId { get; set; }

    public int? CardReaderId { get; set; }

    public int UpOutputNumber { get; set; }

    public int DownOutputNumber { get; set; }
  }
}
