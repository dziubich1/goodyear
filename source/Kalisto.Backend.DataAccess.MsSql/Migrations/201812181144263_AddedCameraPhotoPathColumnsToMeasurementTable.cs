namespace Kalisto.Backend.DataAccess.MsSql.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedCameraPhotoPathColumnsToMeasurementTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Measurements", "DeviceId", c => c.Int(nullable: false));
            AddColumn("dbo.Measurements", "Photo1RelPath", c => c.String());
            AddColumn("dbo.Measurements", "Photo2RelPath", c => c.String());
            AddColumn("dbo.Measurements", "Photo3RelPath", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Measurements", "Photo3RelPath");
            DropColumn("dbo.Measurements", "Photo2RelPath");
            DropColumn("dbo.Measurements", "Photo1RelPath");
            DropColumn("dbo.Measurements", "DeviceId");
        }
    }
}
