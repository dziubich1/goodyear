namespace Kalisto.Backend.DataAccess.MsSql.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ModifiedAllEntityIdsToLong : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Cards", "Cargo_Id", "dbo.Cargoes");
            DropForeignKey("dbo.Measurements", "Cargo_Id", "dbo.Cargoes");
            DropForeignKey("dbo.Cards", "Driver_Id", "dbo.Drivers");
            DropForeignKey("dbo.Vehicles", "Driver_Id", "dbo.Drivers");
            DropForeignKey("dbo.Measurements", "Driver_Id", "dbo.Drivers");
            DropForeignKey("dbo.Cards", "Vehicle_Id", "dbo.Vehicles");
            DropForeignKey("dbo.Measurements", "Vehicle_Id", "dbo.Vehicles");
            DropForeignKey("dbo.Measurements", "Operator_Id", "dbo.Users");
            DropForeignKey("dbo.Users", "Role_Id", "dbo.UserRoles");
            DropForeignKey("dbo.Measurements", "SemiTrailer_Id", "dbo.SemiTrailers");
            DropIndex("dbo.Cards", new[] { "Cargo_Id" });
            DropIndex("dbo.Cards", new[] { "Driver_Id" });
            DropIndex("dbo.Cards", new[] { "Vehicle_Id" });
            DropIndex("dbo.Vehicles", new[] { "Driver_Id" });
            DropIndex("dbo.Measurements", new[] { "Cargo_Id" });
            DropIndex("dbo.Measurements", new[] { "Driver_Id" });
            DropIndex("dbo.Measurements", new[] { "Operator_Id" });
            DropIndex("dbo.Measurements", new[] { "SemiTrailer_Id" });
            DropIndex("dbo.Measurements", new[] { "Vehicle_Id" });
            DropIndex("dbo.Users", new[] { "Role_Id" });
            DropPrimaryKey("dbo.Cards");
            DropPrimaryKey("dbo.Cargoes");
            DropPrimaryKey("dbo.Drivers");
            DropPrimaryKey("dbo.Vehicles");
            DropPrimaryKey("dbo.Users");
            DropPrimaryKey("dbo.UserRoles");
            DropPrimaryKey("dbo.SemiTrailers");
            DropPrimaryKey("dbo.SystemLogs");
            AlterColumn("dbo.Cards", "Id", c => c.Long(nullable: false, identity: true));
            AlterColumn("dbo.Cards", "Cargo_Id", c => c.Long());
            AlterColumn("dbo.Cards", "Driver_Id", c => c.Long());
            AlterColumn("dbo.Cards", "Vehicle_Id", c => c.Long());
            AlterColumn("dbo.Cargoes", "Id", c => c.Long(nullable: false, identity: true));
            AlterColumn("dbo.Drivers", "Id", c => c.Long(nullable: false, identity: true));
            AlterColumn("dbo.Vehicles", "Id", c => c.Long(nullable: false, identity: true));
            AlterColumn("dbo.Vehicles", "Driver_Id", c => c.Long());
            AlterColumn("dbo.DbEventLogs", "EntityId", c => c.Long(nullable: false));
            AlterColumn("dbo.Measurements", "Cargo_Id", c => c.Long());
            AlterColumn("dbo.Measurements", "Driver_Id", c => c.Long());
            AlterColumn("dbo.Measurements", "Operator_Id", c => c.Long());
            AlterColumn("dbo.Measurements", "SemiTrailer_Id", c => c.Long());
            AlterColumn("dbo.Measurements", "Vehicle_Id", c => c.Long());
            AlterColumn("dbo.Users", "Id", c => c.Long(nullable: false, identity: true));
            AlterColumn("dbo.Users", "Role_Id", c => c.Long());
            AlterColumn("dbo.UserRoles", "Id", c => c.Long(nullable: false, identity: true));
            AlterColumn("dbo.SemiTrailers", "Id", c => c.Long(nullable: false, identity: true));
            AlterColumn("dbo.SystemLogs", "Id", c => c.Long(nullable: false, identity: true));
            AddPrimaryKey("dbo.Cards", "Id");
            AddPrimaryKey("dbo.Cargoes", "Id");
            AddPrimaryKey("dbo.Drivers", "Id");
            AddPrimaryKey("dbo.Vehicles", "Id");
            AddPrimaryKey("dbo.Users", "Id");
            AddPrimaryKey("dbo.UserRoles", "Id");
            AddPrimaryKey("dbo.SemiTrailers", "Id");
            AddPrimaryKey("dbo.SystemLogs", "Id");
            CreateIndex("dbo.Cards", "Cargo_Id");
            CreateIndex("dbo.Cards", "Vehicle_Id");
            CreateIndex("dbo.Cards", "Driver_Id");
            CreateIndex("dbo.Measurements", "Cargo_Id");
            CreateIndex("dbo.Measurements", "Driver_Id");
            CreateIndex("dbo.Measurements", "Vehicle_Id");
            CreateIndex("dbo.Measurements", "Operator_Id");
            CreateIndex("dbo.Measurements", "SemiTrailer_Id");
            CreateIndex("dbo.Vehicles", "Driver_Id");
            CreateIndex("dbo.Users", "Role_Id");
            AddForeignKey("dbo.Cards", "Cargo_Id", "dbo.Cargoes", "Id");
            AddForeignKey("dbo.Measurements", "Cargo_Id", "dbo.Cargoes", "Id");
            AddForeignKey("dbo.Cards", "Driver_Id", "dbo.Drivers", "Id");
            AddForeignKey("dbo.Measurements", "Driver_Id", "dbo.Drivers", "Id");
            AddForeignKey("dbo.Vehicles", "Driver_Id", "dbo.Drivers", "Id");
            AddForeignKey("dbo.Cards", "Vehicle_Id", "dbo.Vehicles", "Id");
            AddForeignKey("dbo.Measurements", "Vehicle_Id", "dbo.Vehicles", "Id");
            AddForeignKey("dbo.Measurements", "Operator_Id", "dbo.Users", "Id");
            AddForeignKey("dbo.Users", "Role_Id", "dbo.UserRoles", "Id");
            AddForeignKey("dbo.Measurements", "SemiTrailer_Id", "dbo.SemiTrailers", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Measurements", "SemiTrailer_Id", "dbo.SemiTrailers");
            DropForeignKey("dbo.Users", "Role_Id", "dbo.UserRoles");
            DropForeignKey("dbo.Measurements", "Operator_Id", "dbo.Users");
            DropForeignKey("dbo.Measurements", "Vehicle_Id", "dbo.Vehicles");
            DropForeignKey("dbo.Cards", "Vehicle_Id", "dbo.Vehicles");
            DropForeignKey("dbo.Vehicles", "Driver_Id", "dbo.Drivers");
            DropForeignKey("dbo.Measurements", "Driver_Id", "dbo.Drivers");
            DropForeignKey("dbo.Cards", "Driver_Id", "dbo.Drivers");
            DropForeignKey("dbo.Measurements", "Cargo_Id", "dbo.Cargoes");
            DropForeignKey("dbo.Cards", "Cargo_Id", "dbo.Cargoes");
            DropIndex("dbo.Users", new[] { "Role_Id" });
            DropIndex("dbo.Vehicles", new[] { "Driver_Id" });
            DropIndex("dbo.Measurements", new[] { "SemiTrailer_Id" });
            DropIndex("dbo.Measurements", new[] { "Operator_Id" });
            DropIndex("dbo.Measurements", new[] { "Vehicle_Id" });
            DropIndex("dbo.Measurements", new[] { "Driver_Id" });
            DropIndex("dbo.Measurements", new[] { "Cargo_Id" });
            DropIndex("dbo.Cards", new[] { "Driver_Id" });
            DropIndex("dbo.Cards", new[] { "Vehicle_Id" });
            DropIndex("dbo.Cards", new[] { "Cargo_Id" });
            DropPrimaryKey("dbo.SystemLogs");
            DropPrimaryKey("dbo.SemiTrailers");
            DropPrimaryKey("dbo.UserRoles");
            DropPrimaryKey("dbo.Users");
            DropPrimaryKey("dbo.Vehicles");
            DropPrimaryKey("dbo.Drivers");
            DropPrimaryKey("dbo.Cargoes");
            DropPrimaryKey("dbo.Cards");
            AlterColumn("dbo.SystemLogs", "Id", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.SemiTrailers", "Id", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.UserRoles", "Id", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.Users", "Role_Id", c => c.Int());
            AlterColumn("dbo.Users", "Id", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.Measurements", "Vehicle_Id", c => c.Int());
            AlterColumn("dbo.Measurements", "SemiTrailer_Id", c => c.Int());
            AlterColumn("dbo.Measurements", "Operator_Id", c => c.Int());
            AlterColumn("dbo.Measurements", "Driver_Id", c => c.Int());
            AlterColumn("dbo.Measurements", "Cargo_Id", c => c.Int());
            AlterColumn("dbo.DbEventLogs", "EntityId", c => c.Int(nullable: false));
            AlterColumn("dbo.Vehicles", "Driver_Id", c => c.Int());
            AlterColumn("dbo.Vehicles", "Id", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.Drivers", "Id", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.Cargoes", "Id", c => c.Int(nullable: false, identity: true));
            AlterColumn("dbo.Cards", "Vehicle_Id", c => c.Int());
            AlterColumn("dbo.Cards", "Driver_Id", c => c.Int());
            AlterColumn("dbo.Cards", "Cargo_Id", c => c.Int());
            AlterColumn("dbo.Cards", "Id", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.SystemLogs", "Id");
            AddPrimaryKey("dbo.SemiTrailers", "Id");
            AddPrimaryKey("dbo.UserRoles", "Id");
            AddPrimaryKey("dbo.Users", "Id");
            AddPrimaryKey("dbo.Vehicles", "Id");
            AddPrimaryKey("dbo.Drivers", "Id");
            AddPrimaryKey("dbo.Cargoes", "Id");
            AddPrimaryKey("dbo.Cards", "Id");
            CreateIndex("dbo.Users", "Role_Id");
            CreateIndex("dbo.Measurements", "Vehicle_Id");
            CreateIndex("dbo.Measurements", "SemiTrailer_Id");
            CreateIndex("dbo.Measurements", "Operator_Id");
            CreateIndex("dbo.Measurements", "Driver_Id");
            CreateIndex("dbo.Measurements", "Cargo_Id");
            CreateIndex("dbo.Vehicles", "Driver_Id");
            CreateIndex("dbo.Cards", "Vehicle_Id");
            CreateIndex("dbo.Cards", "Driver_Id");
            CreateIndex("dbo.Cards", "Cargo_Id");
            AddForeignKey("dbo.Measurements", "SemiTrailer_Id", "dbo.SemiTrailers", "Id");
            AddForeignKey("dbo.Users", "Role_Id", "dbo.UserRoles", "Id");
            AddForeignKey("dbo.Measurements", "Operator_Id", "dbo.Users", "Id");
            AddForeignKey("dbo.Measurements", "Vehicle_Id", "dbo.Vehicles", "Id");
            AddForeignKey("dbo.Cards", "Vehicle_Id", "dbo.Vehicles", "Id");
            AddForeignKey("dbo.Measurements", "Driver_Id", "dbo.Drivers", "Id");
            AddForeignKey("dbo.Vehicles", "Driver_Id", "dbo.Drivers", "Id");
            AddForeignKey("dbo.Cards", "Driver_Id", "dbo.Drivers", "Id");
            AddForeignKey("dbo.Measurements", "Cargo_Id", "dbo.Cargoes", "Id");
            AddForeignKey("dbo.Cards", "Cargo_Id", "dbo.Cargoes", "Id");
        }
    }
}
