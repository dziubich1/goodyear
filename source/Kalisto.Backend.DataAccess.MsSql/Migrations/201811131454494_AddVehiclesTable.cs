namespace Kalisto.Backend.DataAccess.MsSql.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddVehiclesTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Vehicles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        PlateNumber = c.String(),
                        Tare = c.Int(nullable: false),
                        TareDate = c.DateTime(nullable: false),
                        MaxVehicleWeight = c.Int(nullable: false),
                        Contractor_Id = c.Long(),
                        Driver_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Contractors", t => t.Contractor_Id)
                .ForeignKey("dbo.Drivers", t => t.Driver_Id)
                .Index(t => t.Contractor_Id)
                .Index(t => t.Driver_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Vehicles", "Driver_Id", "dbo.Drivers");
            DropForeignKey("dbo.Vehicles", "Contractor_Id", "dbo.Contractors");
            DropIndex("dbo.Vehicles", new[] { "Driver_Id" });
            DropIndex("dbo.Vehicles", new[] { "Contractor_Id" });
            DropTable("dbo.Vehicles");
        }
    }
}
