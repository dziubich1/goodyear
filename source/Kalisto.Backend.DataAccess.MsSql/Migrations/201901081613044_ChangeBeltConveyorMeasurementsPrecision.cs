namespace Kalisto.Backend.DataAccess.MsSql.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeBeltConveyorMeasurementsPrecision : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.BeltConveyorMeasurements", "B1", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.BeltConveyorMeasurements", "B2", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.BeltConveyorMeasurements", "B3", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.BeltConveyorMeasurements", "B4", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.BeltConveyorMeasurements", "TotalSum", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.BeltConveyorMeasurements", "IncrementSum", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.BeltConveyorMeasurements", "IncrementSum", c => c.Double(nullable: false));
            AlterColumn("dbo.BeltConveyorMeasurements", "TotalSum", c => c.Double(nullable: false));
            AlterColumn("dbo.BeltConveyorMeasurements", "B4", c => c.Double(nullable: false));
            AlterColumn("dbo.BeltConveyorMeasurements", "B3", c => c.Double(nullable: false));
            AlterColumn("dbo.BeltConveyorMeasurements", "B2", c => c.Double(nullable: false));
            AlterColumn("dbo.BeltConveyorMeasurements", "B1", c => c.Double(nullable: false));
        }
    }
}
