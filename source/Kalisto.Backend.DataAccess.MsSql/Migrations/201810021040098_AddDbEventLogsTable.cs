namespace Kalisto.Backend.DataAccess.MsSql.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDbEventLogsTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DbEventLogs",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        EntityId = c.Int(nullable: false),
                        TableName = c.String(),
                        OldValue = c.String(),
                        NewValue = c.String(),
                        EventType = c.Int(nullable: false),
                        Source = c.String(),
                        DateUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.DbEventLogs");
        }
    }
}
