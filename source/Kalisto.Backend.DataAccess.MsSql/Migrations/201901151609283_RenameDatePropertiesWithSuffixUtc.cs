namespace Kalisto.Backend.DataAccess.MsSql.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RenameDatePropertiesWithSuffixUtc : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Vehicles", "TareDateUtc", c => c.DateTime());
            AddColumn("dbo.Measurements", "Date1Utc", c => c.DateTime(nullable: false));
            AddColumn("dbo.Measurements", "Date2Utc", c => c.DateTime());
            DropColumn("dbo.Vehicles", "TareDate");
            DropColumn("dbo.Measurements", "Date1");
            DropColumn("dbo.Measurements", "Date2");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Measurements", "Date2", c => c.DateTime());
            AddColumn("dbo.Measurements", "Date1", c => c.DateTime(nullable: false));
            AddColumn("dbo.Vehicles", "TareDate", c => c.DateTime());
            DropColumn("dbo.Measurements", "Date2Utc");
            DropColumn("dbo.Measurements", "Date1Utc");
            DropColumn("dbo.Vehicles", "TareDateUtc");
        }
    }
}
