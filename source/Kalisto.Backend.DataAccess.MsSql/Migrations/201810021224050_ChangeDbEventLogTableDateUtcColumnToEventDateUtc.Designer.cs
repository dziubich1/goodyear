// <auto-generated />
namespace Kalisto.Backend.DataAccess.MsSql.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class ChangeDbEventLogTableDateUtcColumnToEventDateUtc : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(ChangeDbEventLogTableDateUtcColumnToEventDateUtc));
        
        string IMigrationMetadata.Id
        {
            get { return "201810021224050_ChangeDbEventLogTableDateUtcColumnToEventDateUtc"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
