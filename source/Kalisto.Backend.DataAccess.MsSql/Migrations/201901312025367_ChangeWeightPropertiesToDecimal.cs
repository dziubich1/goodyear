namespace Kalisto.Backend.DataAccess.MsSql.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeWeightPropertiesToDecimal : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Measurements", "Tare", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.Measurements", "Measurement1", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.Measurements", "Measurement2", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("dbo.Measurements", "NetWeight", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.Vehicles", "Tare", c => c.Decimal(precision: 18, scale: 2));
            AlterColumn("dbo.Vehicles", "MaxVehicleWeight", c => c.Decimal(precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Vehicles", "MaxVehicleWeight", c => c.Int());
            AlterColumn("dbo.Vehicles", "Tare", c => c.Int());
            AlterColumn("dbo.Measurements", "NetWeight", c => c.Int(nullable: false));
            AlterColumn("dbo.Measurements", "Measurement2", c => c.Int());
            AlterColumn("dbo.Measurements", "Measurement1", c => c.Int(nullable: false));
            AlterColumn("dbo.Measurements", "Tare", c => c.Int(nullable: false));
        }
    }
}
