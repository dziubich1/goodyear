namespace Kalisto.Backend.DataAccess.MsSql.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddBeltConveyorMeasurementTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BeltConveyorMeasurements",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Number = c.Long(nullable: false),
                        ConfigId = c.Int(nullable: false),
                        B1 = c.Double(nullable: false),
                        B2 = c.Double(nullable: false),
                        B3 = c.Double(nullable: false),
                        B4 = c.Double(nullable: false),
                        TotalSum = c.Double(nullable: false),
                        IncrementSum = c.Double(nullable: false),
                        DateUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.BeltConveyorMeasurements");
        }
    }
}
