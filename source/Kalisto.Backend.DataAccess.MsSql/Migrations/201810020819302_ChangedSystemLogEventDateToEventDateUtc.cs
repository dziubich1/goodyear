namespace Kalisto.Backend.DataAccess.MsSql.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedSystemLogEventDateToEventDateUtc : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SystemLogs", "EventDateUtc", c => c.DateTime(nullable: false));
            DropColumn("dbo.SystemLogs", "EventDate");
        }
        
        public override void Down()
        {
            AddColumn("dbo.SystemLogs", "EventDate", c => c.DateTime(nullable: false));
            DropColumn("dbo.SystemLogs", "EventDateUtc");
        }
    }
}
