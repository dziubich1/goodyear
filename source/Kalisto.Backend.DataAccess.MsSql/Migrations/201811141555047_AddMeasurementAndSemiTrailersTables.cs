namespace Kalisto.Backend.DataAccess.MsSql.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMeasurementAndSemiTrailersTables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Measurements",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Number = c.Long(nullable: false),
                        Type = c.Int(nullable: false),
                        Class = c.Int(nullable: false),
                        Date1 = c.DateTime(nullable: false),
                        Date2 = c.DateTime(),
                        Tare = c.Int(nullable: false),
                        Measurement1 = c.Int(nullable: false),
                        Measurement2 = c.Int(),
                        NetWeight = c.Int(nullable: false),
                        IsCancelled = c.Boolean(nullable: false),
                        IsManual = c.Boolean(nullable: false),
                        Comment = c.String(),
                        Cargo_Id = c.Int(),
                        Contractor_Id = c.Long(),
                        Driver_Id = c.Int(),
                        Operator_Id = c.Int(),
                        SemiTrailer_Id = c.Int(),
                        Vehicle_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cargoes", t => t.Cargo_Id)
                .ForeignKey("dbo.Contractors", t => t.Contractor_Id)
                .ForeignKey("dbo.Drivers", t => t.Driver_Id)
                .ForeignKey("dbo.Users", t => t.Operator_Id)
                .ForeignKey("dbo.SemiTrailers", t => t.SemiTrailer_Id)
                .ForeignKey("dbo.Vehicles", t => t.Vehicle_Id)
                .Index(t => t.Cargo_Id)
                .Index(t => t.Contractor_Id)
                .Index(t => t.Driver_Id)
                .Index(t => t.Operator_Id)
                .Index(t => t.SemiTrailer_Id)
                .Index(t => t.Vehicle_Id);
            
            CreateTable(
                "dbo.SemiTrailers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Number = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Measurements", "Vehicle_Id", "dbo.Vehicles");
            DropForeignKey("dbo.Measurements", "SemiTrailer_Id", "dbo.SemiTrailers");
            DropForeignKey("dbo.Measurements", "Operator_Id", "dbo.Users");
            DropForeignKey("dbo.Measurements", "Driver_Id", "dbo.Drivers");
            DropForeignKey("dbo.Measurements", "Contractor_Id", "dbo.Contractors");
            DropForeignKey("dbo.Measurements", "Cargo_Id", "dbo.Cargoes");
            DropIndex("dbo.Measurements", new[] { "Vehicle_Id" });
            DropIndex("dbo.Measurements", new[] { "SemiTrailer_Id" });
            DropIndex("dbo.Measurements", new[] { "Operator_Id" });
            DropIndex("dbo.Measurements", new[] { "Driver_Id" });
            DropIndex("dbo.Measurements", new[] { "Contractor_Id" });
            DropIndex("dbo.Measurements", new[] { "Cargo_Id" });
            DropTable("dbo.SemiTrailers");
            DropTable("dbo.Measurements");
        }
    }
}
