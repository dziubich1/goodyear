namespace Kalisto.Backend.DataAccess.MsSql.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedDeclaredWeightToMeasurementsTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Measurements", "DeclaredWeight", c => c.Decimal(precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Measurements", "DeclaredWeight");
        }
    }
}
