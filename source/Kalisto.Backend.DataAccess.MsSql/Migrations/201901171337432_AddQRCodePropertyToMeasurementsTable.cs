namespace Kalisto.Backend.DataAccess.MsSql.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddQRCodePropertyToMeasurementsTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Measurements", "QRCode", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Measurements", "QRCode");
        }
    }
}
