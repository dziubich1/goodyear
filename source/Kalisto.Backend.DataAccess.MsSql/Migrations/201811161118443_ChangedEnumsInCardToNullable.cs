namespace Kalisto.Backend.DataAccess.MsSql.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedEnumsInCardToNullable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Cards", "Lifetime", c => c.Int());
            AlterColumn("dbo.Cards", "MeasurementClass", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Cards", "MeasurementClass", c => c.Int(nullable: false));
            AlterColumn("dbo.Cards", "Lifetime", c => c.Int(nullable: false));
        }
    }
}
