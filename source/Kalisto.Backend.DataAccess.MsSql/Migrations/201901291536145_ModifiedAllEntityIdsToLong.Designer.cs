// <auto-generated />
namespace Kalisto.Backend.DataAccess.MsSql.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class ModifiedAllEntityIdsToLong : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(ModifiedAllEntityIdsToLong));
        
        string IMigrationMetadata.Id
        {
            get { return "201901291536145_ModifiedAllEntityIdsToLong"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
