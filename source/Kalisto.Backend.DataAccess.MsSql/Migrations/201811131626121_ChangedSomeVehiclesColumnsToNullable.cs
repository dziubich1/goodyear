namespace Kalisto.Backend.DataAccess.MsSql.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedSomeVehiclesColumnsToNullable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Vehicles", "Tare", c => c.Int());
            AlterColumn("dbo.Vehicles", "TareDate", c => c.DateTime());
            AlterColumn("dbo.Vehicles", "MaxVehicleWeight", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Vehicles", "MaxVehicleWeight", c => c.Int(nullable: false));
            AlterColumn("dbo.Vehicles", "TareDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.Vehicles", "Tare", c => c.Int(nullable: false));
        }
    }
}
