namespace Kalisto.Backend.DataAccess.MsSql.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCardsTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Cards",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Number = c.Int(nullable: false),
                        Code = c.String(),
                        Lifetime = c.Int(nullable: false),
                        MeasurementClass = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        Cargo_Id = c.Int(),
                        Contractor_Id = c.Long(),
                        Driver_Id = c.Int(),
                        Vehicle_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cargoes", t => t.Cargo_Id)
                .ForeignKey("dbo.Contractors", t => t.Contractor_Id)
                .ForeignKey("dbo.Drivers", t => t.Driver_Id)
                .ForeignKey("dbo.Vehicles", t => t.Vehicle_Id)
                .Index(t => t.Cargo_Id)
                .Index(t => t.Contractor_Id)
                .Index(t => t.Driver_Id)
                .Index(t => t.Vehicle_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Cards", "Vehicle_Id", "dbo.Vehicles");
            DropForeignKey("dbo.Cards", "Driver_Id", "dbo.Drivers");
            DropForeignKey("dbo.Cards", "Contractor_Id", "dbo.Contractors");
            DropForeignKey("dbo.Cards", "Cargo_Id", "dbo.Cargoes");
            DropIndex("dbo.Cards", new[] { "Vehicle_Id" });
            DropIndex("dbo.Cards", new[] { "Driver_Id" });
            DropIndex("dbo.Cards", new[] { "Contractor_Id" });
            DropIndex("dbo.Cards", new[] { "Cargo_Id" });
            DropTable("dbo.Cards");
        }
    }
}
