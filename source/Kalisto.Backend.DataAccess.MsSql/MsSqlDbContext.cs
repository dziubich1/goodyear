﻿using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.DataAccess.MsSql.Migrations;

namespace Kalisto.Backend.DataAccess.MsSql
{
  public class MsSqlDbContext : DbContext, IDbContext
  {
    public IDbSet<User> Users { get; set; }

    public IDbSet<UserRole> UserRoles { get; set; }

    public IDbSet<SystemLog> SystemLogs { get; set; }

    public IDbSet<DbEventLog> DbEventLogs { get; set; }

    public IDbSet<Contractor> Contractors { get; set; }

    public IDbSet<Cargo> Cargoes { get; set; }

    public IDbSet<Driver> Drivers { get; set; }

    public IDbSet<Vehicle> Vehicles { get; set; }

    public IDbSet<Measurement> Measurements { get; set; }

    public IDbSet<SemiTrailer> SemiTrailers { get; set; }

    public IDbSet<Card> Cards { get; set; }

    public IDbSet<BeltConveyorMeasurement> BeltConveyorMeasurements { get; set; }

    public MsSqlDbContext() : base("MigrationConnectionString")
    {
      Database.SetInitializer(new MigrateDatabaseToLatestVersion<MsSqlDbContext, Configuration>());
    }

    public MsSqlDbContext(string connectionString)
      : base(GetConnection(connectionString), true)
    {
      Database.SetInitializer(new CreateDatabaseIfNotExists<MsSqlDbContext>());
      Configuration.ProxyCreationEnabled = false;
    }

    private static DbConnection GetConnection(string connectionString)
    {
      var factory = new SqlConnectionFactory(connectionString); 
      return factory.CreateConnection(connectionString);
    }
  }
}
