﻿using System.Data.Common;
using System.Data.Entity;
using Kalisto.Backend.DataAccess.Model;

namespace Kalisto.Backend.DataAccess.Effort
{
  public class EffortDbContext : DbContext, IDbContext
  {
    public IDbSet<User> Users { get; set; }

    public IDbSet<UserRole> UserRoles { get; set; }

    public IDbSet<SystemLog> SystemLogs { get; set; }

    public IDbSet<DbEventLog> DbEventLogs { get; set; }

    public IDbSet<Contractor> Contractors { get; set; }

    public IDbSet<Cargo> Cargoes { get; set; }

    public IDbSet<Driver> Drivers { get; set; }

    public IDbSet<Vehicle> Vehicles { get; set; }

    public IDbSet<Measurement> Measurements { get; set; }

    public IDbSet<SemiTrailer> SemiTrailers { get; set; }

    public IDbSet<Card> Cards { get; set; }

    public IDbSet<BeltConveyorMeasurement> BeltConveyorMeasurements { get; set; }

    public EffortDbContext(DbConnection connection)
      : base(connection, true)
    {
    }
  }
}
