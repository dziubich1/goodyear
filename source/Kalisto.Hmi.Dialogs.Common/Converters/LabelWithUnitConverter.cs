﻿using Kalisto.Hmi.Core.Helpers;
using System;
using System.Globalization;
using System.Windows.Data;

namespace Kalisto.Hmi.Dialogs.Common.Converters
{
  public class LabelWithUnitConverter : IValueConverter
  {
    private readonly IUnitTypeService _unitTypeService;

    public LabelWithUnitConverter()
    {
      _unitTypeService = FrameworkHelper.Resolve<IUnitTypeService>();
    }

    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if (value == null || _unitTypeService == null)
        return null;

      var str = value.ToString();
      if (string.IsNullOrEmpty(str))
        return null;

      var unitType = _unitTypeService.GetLocalizedCurrentUnit();
      if (str.Contains(":"))
        return str.Replace(":", $" [{unitType}]:");
      return $"{str} [{unitType}]";
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      return null;
    }
  }
}
