﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace Kalisto.Hmi.Dialogs.Common.Converters
{
  public class BoolToMeasurementClassConverter : IValueConverter
  {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if (value == null)
        return false;

      return value.Equals(parameter);
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
      if (value == null)
        return Binding.DoNothing;

      return (bool)value ? parameter : Binding.DoNothing;
    }
  }
}
