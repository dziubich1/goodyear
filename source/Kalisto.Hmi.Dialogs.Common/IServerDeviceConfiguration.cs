﻿using Kalisto.Configuration.Devices;
using System.Threading.Tasks;

namespace Kalisto.Hmi.Dialogs.Common
{
  public interface IServerDeviceConfiguration
  {
    Task<ServerDeviceConfiguration> GetConfigurationAsync();
  }
}
