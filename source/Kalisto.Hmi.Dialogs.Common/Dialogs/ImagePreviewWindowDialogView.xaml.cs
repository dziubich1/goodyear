﻿using MahApps.Metro.Controls;

namespace Kalisto.Hmi.Dialogs.Common.Dialogs
{
  public partial class ImagePreviewWindowDialogView : MetroWindow
  {
    public ImagePreviewWindowDialogView()
    {
      InitializeComponent();
      Height = (System.Windows.SystemParameters.PrimaryScreenHeight * 0.75);
      Width = (System.Windows.SystemParameters.PrimaryScreenWidth * 0.75);
    }
  }
}
