﻿using Caliburn.Micro;
using PropertyChanged;
using System.Windows.Media.Imaging;
using Kalisto.Localization;
using Kalisto.Localization.DialogTitles;

namespace Kalisto.Hmi.Dialogs.Common.Dialogs
{
  [AddINotifyPropertyChangedInterface]
  public class ImagePreviewWindowDialogViewModel : Screen
  {
    private readonly ILocalizer<DialogTitles> _localizer;

    public BitmapImage Image { get; set; }

    public ImagePreviewWindowDialogViewModel()
    {
      _localizer = new LocalizationManager<DialogTitles>();
    }

    protected override void OnViewLoaded(object view)
    {
      base.OnViewLoaded(view);
      DisplayName = _localizer.Localize(() => DialogTitles.Dashboard);
    }
  }
}
