﻿namespace Kalisto.Hmi.Dialogs.Common.Models
{
  public class ReferenceModel
  {
    public long Id { get; set; }

    public string DisplayValue { get; set; }

    public object RawData { get; set; }
  }
}
