﻿using Kalisto.Localization;

namespace Kalisto.Hmi.Dialogs.Common.Models
{
  public class WeighingModel
  {
    public int OrdinalId { get; set; }

    public LocalizedDate WeighingDate { get; set; }

    public string Cargo { get; set; }

    public string Contractor { get; set; }

    public string Vehicle { get; set; }

    public string Driver { get; set; }

    public UnitOfMeasurement Gross { get; set; }

    public UnitOfMeasurement Net { get; set; }
  }
}
