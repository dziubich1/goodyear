﻿using Kalisto.Localization;

namespace Kalisto.Hmi.Dialogs.Common.Models
{
  public class WeightDevice
  {
    public int Id { get; set; }

    public string Name { get; set; }

    public string DisplayName => GetDisplayName();

    private string GetDisplayName()
    {
      if (string.IsNullOrEmpty(Name))
        return $"{Localization.Common.Common.DeviceNameHeader} {Id}";

      return Name;
    }
  }
}
