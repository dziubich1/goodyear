﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Kalisto.Frontend.Notifications.Enums;
using PropertyChanged;

namespace Kalisto.Hmi.Dialogs.Common.Services
{
  [AddINotifyPropertyChangedInterface]
  public class WeighingDeviceInfo
  {
    public int Id { get; set; }

    public string Name { get; set; }

    public UnitOfMeasurement MeasuredValue { get; set; }

    public WeighingDeviceStatus DeviceStatus { get; set; }
  }

  public interface IWeighingDeviceService
  {
    Task<List<WeighingDeviceInfo>> GetWeighingDevicesAsync();
  }

  public class WeighingDeviceService : IWeighingDeviceService
  {
    private readonly IServerDeviceConfiguration _serverDeviceConfiguration;

    public WeighingDeviceService(IServerDeviceConfiguration serverDeviceConfiguration)
    {
      _serverDeviceConfiguration = serverDeviceConfiguration;
    }

    public async Task<List<WeighingDeviceInfo>> GetWeighingDevicesAsync()
    {
      var deviceConfiguration = await _serverDeviceConfiguration.GetConfigurationAsync();

      // weight metering devices config
      var devices = deviceConfiguration.WeightMeters.Where(c => c.IsActive).Select(c => new WeighingDeviceInfo
      {
        Id = c.Id,
        Name = string.IsNullOrEmpty(c.Name) ? $"{Localization.Common.Common.DeviceDefaultNameLabel} {c.Id}" : c.Name,
        DeviceStatus = WeighingDeviceStatus.Unknown
      }).ToList();

      devices.AddRange(deviceConfiguration.IoControllers.Where(c => c.IsActive && c.IsWeighingDeviceActive).Select(c => new WeighingDeviceInfo
      {
        Id = c.Id,
        Name = string.IsNullOrEmpty(c.Name) ? $"{Localization.Common.Common.DeviceDefaultNameLabel} {c.Id}" : c.Name,
        DeviceStatus = WeighingDeviceStatus.Unknown
      }));

      return devices;
    }
  }
}
