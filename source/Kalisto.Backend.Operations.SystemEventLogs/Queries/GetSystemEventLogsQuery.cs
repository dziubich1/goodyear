﻿using Kalisto.Communication.Core.Messages;
using System;

namespace Kalisto.Backend.Operations.SystemEventLogs.Queries
{
  [Serializable]
  public class GetSystemEventLogsQuery: BackendDbOperationMessage
  {
    public DateTime FromUtc { get; set; }
    public DateTime ToUtc { get; set; }
  }
}
