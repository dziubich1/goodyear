﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Communication.Core.Messaging;
using System.Data.Entity;
using System.Linq;

namespace Kalisto.Backend.Operations.SystemEventLogs.Queries
{
  public class GetSystemEventLogsQueryHandler : QueryHandlerBase<GetSystemEventLogsQuery>
  {
    public override object Execute(IDbContext context, GetSystemEventLogsQuery query, string partnerId)
    {
      var logs = context.SystemLogs
        .Where(l => l.EventDateUtc >= query.FromUtc)
        .Where(l => l.EventDateUtc <= query.ToUtc)
        .AsNoTracking()
        .OrderByDescending(c => c.EventDateUtc)
        .ToList();

      return logs;
    }
  }
}
