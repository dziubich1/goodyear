﻿using System;
using System.Collections.Generic;
using Kalisto.Devices.Core.Messages;

namespace Kalisto.Devices.BarcodeScanner.Messages
{
  public class BarcodeScannerMessage : DeviceMessageBase
  {
    private readonly Dictionary<string, string> _mines = new Dictionary<string, string>
    {
      { "481", "KWK ROW RUCH \"JANKOWICE\"" },
      { "501", "KWK ROW RUCH \"RYDUŁTOWY\"" },
      { "601", "KWK \"MYSŁOWICE-WESOŁA\"" },
      { "621", "KWK \"MURCKI-STASZIC\"" },
      { "651", "KWK \"WUJEK\"" }
    };

    public string PortName { get; set; }

    public string ReceiptNumber { get; set; }

    public string Receiver { get; set; }

    public DateTime IssuedDate { get; set; }

    public string Contractor
    {
      get
      {
        if (string.IsNullOrEmpty(ReceiptNumber) || ReceiptNumber.Length < 3)
          return string.Empty;

        var mineCode = ReceiptNumber.Substring(0, 3);
        if (!_mines.ContainsKey(mineCode))
          return string.Empty;

        return _mines[mineCode];
      }
    }

    public string Cargo { get; set; }

    public decimal Tare { get; set; }
    //Gross vehicle weight (declared gross weight, not needed for our purpose)
    public decimal Gross { get; set; }
    //Net weight - weight of material on the vehicle. Declared weight in our system.
    public decimal Net { get; set; }

    public bool IsValid { get; set; }

    public static BarcodeScannerMessage Invalid => new BarcodeScannerMessage {IsValid = false};
  }
}
