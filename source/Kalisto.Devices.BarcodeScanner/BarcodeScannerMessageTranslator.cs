﻿using Kalisto.Devices.BarcodeScanner.Messages;
using Kalisto.Devices.Core;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Kalisto.Devices.BarcodeScanner
{
  public class BarcodeScannerMessageTranslator : IMessageTranslator<BarcodeScannerMessage>, IMessageValidator<BarcodeScannerMessage>
  {
    private const string ReceiptLabel = "Kwit:";
    private const string IssuedLabel = "Wystawiony:";
    private const string ReceiverLabel = "Odbiorca:";
    private const string GrossLabel = "Brutto:";
    private const string NetLabel = "Ilość:";
    private const string TareLabel = "Tara:";
    private const string CargoLabel = "Sort:";

    public bool Validate(byte[] bytes)
    {
      var message = Encoding.UTF8.GetString(bytes);
      if (string.IsNullOrEmpty(message))
        return false;

      if (!message.Contains(ReceiptLabel))
        return false;

      if (!message.Contains(IssuedLabel))
        return false;

      if (!message.Contains(ReceiverLabel))
        return false;

      if (!message.Contains(GrossLabel))
        return false;

      if (!message.Contains(NetLabel))
        return false;

      if (!message.Contains(TareLabel))
        return false;

      if (!message.Contains(CargoLabel))
        return false;

      return true;
    }

    public BarcodeScannerMessage Translate(byte[] bytes)
    {
      try
      {
        var message = Encoding.UTF8.GetString(bytes);
        var parts = SplitMessageParts(message);
        var keyValues = new List<string>();
        foreach (var part in parts)
          keyValues.AddRange(GetKeyValueChunks(part));

        return CreateMessage(keyValues);
      }
      catch (Exception)
      {
        return BarcodeScannerMessage.Invalid;
      }
    }

    private BarcodeScannerMessage CreateMessage(List<string> keyValues)
    {
      var message = new BarcodeScannerMessage { IsValid = true };
      foreach (var keyValue in keyValues)
      {
        if (keyValue.StartsWith(ReceiptLabel))
        {
          message.ReceiptNumber = ExtractValueFromString(keyValue, ReceiptLabel);
        }
        if (keyValue.StartsWith(IssuedLabel))
        {
          var formats = new[] { "M-d-yyyy", "dd-MM-yyyy", "MM-dd-yyyy", "M.d.yyyy", "dd.MM.yyyy", "MM.dd.yyyy" }
            .Union(CultureInfo.CurrentCulture.DateTimeFormat.GetAllDateTimePatterns()).ToArray();
          var dateString = ExtractValueFromString(keyValue, IssuedLabel);
          // we do not know exact date format
          message.IssuedDate = DateTime.ParseExact(dateString, formats, CultureInfo.CurrentCulture, DateTimeStyles.AssumeLocal);
        }
        if (keyValue.StartsWith(ReceiverLabel))
        {
          message.Receiver = ExtractValueFromString(keyValue, ReceiverLabel);
        }
        if (keyValue.StartsWith(CargoLabel))
        {
          message.Cargo = ExtractValueFromString(keyValue, CargoLabel);
        }
        if (keyValue.StartsWith(GrossLabel))
        {
          var value = ExtractNumberFromString(keyValue, GrossLabel);
          message.Gross =
            decimal.Parse(value, NumberStyles.AllowDecimalPoint) * 1000;
        }
        if (keyValue.StartsWith(NetLabel))
        {
          var value = ExtractNumberFromString(keyValue, NetLabel);
          message.Net =
            decimal.Parse(value, NumberStyles.AllowDecimalPoint) * 1000;
        }
        if (keyValue.StartsWith(TareLabel))
        {
          var value = ExtractNumberFromString(keyValue, TareLabel);
          message.Tare =
            decimal.Parse(value, NumberStyles.AllowDecimalPoint) * 1000;
        }
      }

      return message;
    }

    private string ExtractValueFromString(string keyValue, string label)
    {
      return keyValue.Replace(label, string.Empty).Trim();
    }

    private string ExtractNumberFromString(string keyValue, string label)
    {
      string value = keyValue.Replace(label, string.Empty).Trim().Split(' ')[0].Trim();
      return GetValidNumberString(value);
    }

    private string GetValidNumberString(string value)
    {
      if (NumberFormatInfo.CurrentInfo.NumberDecimalSeparator == "." && value.Contains(","))
        value = value.Replace(",", ".");
      if (NumberFormatInfo.CurrentInfo.NumberDecimalSeparator == "," && value.Contains("."))
        value = value.Replace(".", ",");

      return value;
    }

    private IEnumerable<string> SplitMessageParts(string message)
    {
      return message.Split(new[] { "\r\n" }, StringSplitOptions.None).Select(part => part.Trim().TrimEnd('\r', '\n'))
        .ToArray();
    }

    private IEnumerable<string> GetKeyValueChunks(string value)
    {
      var keyValues = new List<string>();
      var colonParts = value.Split(':');
      if (colonParts.Length == 2)
        keyValues.Add($"{colonParts[0]}:{colonParts[1]}");
      else if (colonParts.Length > 2)
      {
        var key = colonParts[0];
        for (int i = 1; i < colonParts.Length; i++)
        {
          var spaceParts = colonParts[i].Split(' ');
          var val = string.Empty;
          for (int j = 0; j < spaceParts.Length - 1; j++)
          {
            val += $"{spaceParts[j]} ";
            if (i + 1 == colonParts.Length && j + 1 == spaceParts.Length - 1)
              val += $"{spaceParts[j + 1]}";
          }
          keyValues.Add($"{key}:{val}");
          key = spaceParts.Last();
        }
      }

      return keyValues;
    }
  }
}
