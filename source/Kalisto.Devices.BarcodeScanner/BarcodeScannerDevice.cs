﻿using Kalisto.Devices.BarcodeScanner.Messages;
using Kalisto.Devices.Core;
using Kalisto.Devices.Core.Communication;
using Kalisto.Devices.Core.Communication.Interfaces;
using Kalisto.Devices.Core.Messages;
using System;

namespace Kalisto.Devices.BarcodeScanner
{
  public class BarcodeScannerDevice : IDevice<BarcodeScannerMessage>
  {
    private readonly IDeviceClientConnection _clientConnection;

    private readonly IMessageChunkAggregator<BarcodeScannerMessage> _messageChunkAggregator;

    private string _endpointAddress;

    public int Id { get; set; }

    public event Action<BarcodeScannerMessage> MessageReceived;

    public event Action<ConnectionState> ConnectionStateChanged;

    public BarcodeScannerDevice(IDeviceClientConnection clientConnection,
      IMessageChunkAggregator<BarcodeScannerMessage> messageFactory)
    {
      _clientConnection = clientConnection;
      _clientConnection.BytesMessageReceived += OnBytesReceived;
      _clientConnection.ConnectionStateChanged += OnConnectionStateChanged;
      _messageChunkAggregator = messageFactory;
      _messageChunkAggregator.MessageReady += OnMessageReady;
    }

    private void OnBytesReceived(object sender, BytesMessageReceivedEventArgs e)
    {
      _endpointAddress = e.EndpointId;
      _messageChunkAggregator.AddChunk(e.Bytes);
    }

    private void OnConnectionStateChanged(ConnectionState obj)
    {
      ConnectionStateChanged?.Invoke(obj);
    }

    private void OnMessageReady(BarcodeScannerMessage message)
    {
      if (message == null)
        return;

      message.PortName = _endpointAddress;
      message.DeviceId = Id;
      MessageReceived?.Invoke(message);
    }

    public void StartSampling()
    {
      _clientConnection.Connect(true);
    }

    public void StopSampling()
    {
      _clientConnection.Disconnect();
    }

    public void SendCommand<TCommand>(TCommand command) 
      where TCommand : DeviceCommandMessageBase
    {
    }
  }
}
