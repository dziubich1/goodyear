﻿using Kalisto.Devices.BarcodeScanner.Messages;
using Kalisto.Devices.Core;
using System;

namespace Kalisto.Devices.BarcodeScanner
{
  public class BarcodeScannerMessageChunkAggregator : IMessageChunkAggregator<BarcodeScannerMessage>
  {
    private readonly IMessageTranslator<BarcodeScannerMessage> _messageTranslator;

    private readonly IMessageValidator<BarcodeScannerMessage> _messageValidator;

    public BarcodeScannerMessageChunkAggregator(IMessageTranslator<BarcodeScannerMessage> messageTranslator, IMessageValidator<BarcodeScannerMessage> validator)
    {
      _messageTranslator = messageTranslator;
      _messageValidator = validator;
    }
    public event Action<BarcodeScannerMessage> MessageReady;

    public void AddChunk(byte[] bytes)
    {
      if (!_messageValidator.Validate(bytes))
      {
        MessageReady?.Invoke(BarcodeScannerMessage.Invalid);
        return;
      }

      MessageReady?.Invoke(_messageTranslator.Translate(bytes));
    }
  }
}
