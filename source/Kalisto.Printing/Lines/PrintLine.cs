﻿using System.Drawing;
using System.Drawing.Drawing2D;

namespace Kalisto.Printouts.Lines
{
  public class PrintLine
  {
    public Point StartPoint { get; set; }
    
    public Point EndPoint { get; set; }
    
    public float Thicnkess { get; set; } 

    public DashStyle DashStyle { get; set; }
  }
}
