﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Text;

namespace Kalisto.Printouts.Lines
{
  public class PrintLineBuilder
  {
    private readonly PrintLine _line;

    public PrintLineBuilder()
    {
      _line = new PrintLine
      {
        Thicnkess = 1f,
        DashStyle = DashStyle.Solid
      };
    }

    public PrintLineBuilder WithThickness(float thickness)
    {
      _line.Thicnkess = thickness;
      return this;
    }

    public PrintLineBuilder StartsAt(int x, int y)
    {
      _line.StartPoint = new Point(x, y);
      return this;
    }

    public PrintLineBuilder EndsAt(int x, int y)
    {
      _line.EndPoint = new Point(x, y);
      return this;
    }

    public PrintLineBuilder Dashed()
    {
      _line.DashStyle = DashStyle.Dash;
      return this;
    }

    public PrintLine Submit()
    {
      return _line;
    }
  }
}
