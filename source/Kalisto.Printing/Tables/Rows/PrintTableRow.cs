﻿using Kalisto.Printouts.Tables.Cells;

namespace Kalisto.Printouts.Tables.Rows
{
  public class PrintTableRow
  {
    public PrintTableCell[] Cells { get; set; }
  }
}
