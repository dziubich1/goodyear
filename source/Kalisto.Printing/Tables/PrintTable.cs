﻿using System.Linq;
using Kalisto.Printouts.Enums;
using Kalisto.Printouts.Labels.Styles;
using Kalisto.Printouts.Tables.Columns;
using Kalisto.Printouts.Tables.Rows;

namespace Kalisto.Printouts.Tables
{
  public class PrintTable
  {
    public PrintTableColumn[] Columns { get; set; }

    public PrintTableRow[] Rows { get; set; }

    public PrintLabelStyle HeaderStyle { get; set; }

    public PrintLabelStyle CellStyle { get; set; }

    public AlignmentEnum Alignment { get; set; }

    public float BorderThickness { get; set; }

    public float CellHeight { get; set; }

    public float HeaderHeight { get; set; }

    public float VerticalOffset { get; set; }

    public float TotalWidth
    {
      get { return Columns.Sum(c => c.Width); }
    }

    public float TotalHeight
    {
      get { return (Rows.Length + 1)*20; }
    }
  }
}
