﻿namespace Kalisto.Printouts.Tables.Cells
{
  public class PrintTableCellSplitter
  {
    public string Text { get; set; }

    public float Width { get; set; }
  }
}
