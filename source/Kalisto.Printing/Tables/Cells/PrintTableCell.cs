﻿using Kalisto.Printouts.Enums;

namespace Kalisto.Printouts.Tables.Cells
{
  public class PrintTableCell
  {
    public string Text { get; set; }

    public AlignmentEnum Alignment { get; set; }

    public PrintTableCellSplitter Splitter { get; set; }

    public bool IsBlank { get; set; }
  }
}
