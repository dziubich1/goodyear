﻿using Kalisto.Printouts.Enums;

namespace Kalisto.Printouts.Tables.Cells
{
  public class PrintTableCellBuilder
  {
    private readonly PrintTableCell _tableCell;

    public PrintTableCellBuilder()
    {
      _tableCell = new PrintTableCell
      {
        Alignment = AlignmentEnum.Center,
        IsBlank = false
      };
    }

    public PrintTableCellBuilder WithText(string text)
    {
      _tableCell.Text = text;
      return this;
    }

    public PrintTableCellBuilder AlignLeft()
    {
      _tableCell.Alignment = AlignmentEnum.Left;
      return this;
    }

    public PrintTableCellBuilder AlignRight()
    {
      _tableCell.Alignment = AlignmentEnum.Right;
      return this;
    }

    public PrintTableCellBuilder WithSplitter(string text, float width)
    {
      _tableCell.Splitter = new PrintTableCellSplitter
      {
        Text = text,
        Width = width
      };
      return this;
    }

    public PrintTableCellBuilder Blank()
    {
      _tableCell.IsBlank = true;
      return this;
    }

    public PrintTableCell Submit()
    {
      return _tableCell;
    }
  }
}
