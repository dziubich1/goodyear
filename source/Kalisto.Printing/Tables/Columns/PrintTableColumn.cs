﻿namespace Kalisto.Printouts.Tables.Columns
{
  public class PrintTableColumn
  {
    public string Text { get; set; }

    public float Width { get; set; }
  }
}
