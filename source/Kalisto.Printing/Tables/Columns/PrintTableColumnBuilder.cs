﻿namespace Kalisto.Printouts.Tables.Columns
{
  public class PrintTableColumnBuilder
  {
    private readonly PrintTableColumn _column;

    public PrintTableColumnBuilder()
    {
      _column = new PrintTableColumn();
    }

    public PrintTableColumnBuilder WithWidth(float width)
    {
      _column.Width = width;
      return this;
    }

    public PrintTableColumnBuilder WithText(string text)
    {
      _column.Text = text;
      return this;
    }

    public PrintTableColumn Submit()
    {
      return _column;
    }
  }
}
