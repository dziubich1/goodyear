﻿using Kalisto.Printouts.Enums;
using Kalisto.Printouts.Labels.Styles;
using Kalisto.Printouts.Tables.Columns;
using Kalisto.Printouts.Tables.Rows;

namespace Kalisto.Printouts.Tables
{
  public class PrintTableBuilder
  {
    private readonly PrintTable _table;

    public PrintTableBuilder()
    {
      _table = new PrintTable
      {
        BorderThickness = 1f,
        HeaderHeight = 20f,
        CellHeight = 20f,
        Alignment = AlignmentEnum.Center
      };
    }

    public PrintTableBuilder WithColumns(PrintTableColumn[] columns)
    {
      _table.Columns = columns;
      return this;
    }

    public PrintTableBuilder WithBorderThickness(float thickness)
    {
      _table.BorderThickness = thickness;
      return this;
    }

    public PrintTableBuilder WithDataSource(PrintTableRow[] rows)
    {
      _table.Rows = rows;
      return this;
    }

    public PrintTableBuilder WithHeaderStyle(PrintLabelStyle style)
    {
      _table.HeaderStyle = style;
      return this;
    }

    public PrintTableBuilder WithCellStyle(PrintLabelStyle style)
    {
      _table.CellStyle = style;
      return this;
    }

    public PrintTableBuilder WithHeaderHeight(float height)
    {
      _table.HeaderHeight = height;
      return this;
    }

    public PrintTableBuilder WithCellHeight(float height)
    {
      _table.CellHeight = height;
      return this;
    }

    public PrintTableBuilder WithVerticalOffset(float offset)
    {
      _table.VerticalOffset = offset;
      return this;
    }

    public PrintTableBuilder AlignLeft()
    {
      _table.Alignment = AlignmentEnum.Left;
      return this;
    }

    public PrintTable Submit()
    {
      return _table;
    }
  }
}
