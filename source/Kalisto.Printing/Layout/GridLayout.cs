﻿using System;
using System.Drawing;

namespace Kalisto.Printing.Layout
{
  public class GridLayout
  {
    public int RowCount { get; set; }

    public int ColumnCount { get; set; }

    public int Width { get; set; }

    public int Height { get; set; }

    public int RowHeight => ((Height - Margin * 2) / RowCount) - 2 * Padding;

    public int ColumnWidth => ((Width - Margin * 2) / ColumnCount) - 2 * Padding;

    public int X { get; set; }

    public int Y { get; set; }

    public int Margin { get; set; }

    public int Padding { get; set; }

    public Point GetPosition(int gridRowIndex, int gridColumnIndex, Size elementSize)
    {
      if (RowCount - 1 < gridRowIndex)
        throw new ArgumentOutOfRangeException();

      if (ColumnCount - 1 < gridColumnIndex)
        throw new ArgumentOutOfRangeException();

      var rowHeightOffset = gridRowIndex * RowHeight;
      var columnWidthOffset = gridColumnIndex * ColumnWidth;

      var centeringVerticalOffset = (RowHeight - elementSize.Height) / 2;
      rowHeightOffset += centeringVerticalOffset;
      rowHeightOffset += Margin;
      rowHeightOffset += Padding * ((gridRowIndex + 1) * 2 - 1);
      rowHeightOffset += Y;

      var centeringHorizontalOffset = (ColumnWidth - elementSize.Width) / 2;
      columnWidthOffset += centeringHorizontalOffset;
      columnWidthOffset += Margin;
      columnWidthOffset += Padding * ((gridColumnIndex + 1) * 2 - 1);
      columnWidthOffset += X;

      return new Point(columnWidthOffset, rowHeightOffset);
    }
  }

  public class GridLayoutBuilder
  {
    private int _width;

    private int _height;

    private int _columnCount;

    private int _rowCount;

    private int _margin;
    private int _padding;

    private int _horizontalOffset;
    private int _verticalOffset;

    public GridLayoutBuilder WithWidth(int width)
    {
      _width = width;
      return this;
    }

    public GridLayoutBuilder WithHeight(int height)
    {
      _height = height;
      return this;
    }

    public GridLayoutBuilder WithColumns(int count)
    {
      _columnCount = count;
      return this;
    }

    public GridLayoutBuilder WithRows(int count)
    {
      _rowCount = count;
      return this;
    }

    public GridLayoutBuilder WithMargin(int margin)
    {
      _margin = margin;
      return this;
    }

    public GridLayoutBuilder WithPadding(int padding)
    {
      _padding = padding;
      return this;
    }

    public GridLayoutBuilder WithHorizontalOffset(int offset)
    {
      _horizontalOffset = offset;
      return this;
    }

    public GridLayoutBuilder WithVerticalOffset(int offset)
    {
      _verticalOffset = offset;
      return this;
    }

    public GridLayout Build()
    {
      return new GridLayout
      {
        Width = _width,
        Height = _height,
        ColumnCount = _columnCount,
        RowCount = _rowCount,
        Margin = _margin,
        X = _horizontalOffset,
        Y = _verticalOffset,
        Padding = _padding
      };
    }
  }
}
