﻿using System.Drawing;
using Kalisto.Printouts.Enums;

namespace Kalisto.Printouts.Labels
{
  public class PrintLabel
  {
    public string Text { get; set; }

    public Font Font { get; set; }

    public Color Color { get; set; }

    public float X { get; set; }

    public float Y { get; set; }

    public AlignmentEnum Align { get; set; }

    public VAlignmentEnum VAlign { get; set; }

    public float GetLabelWidth(Graphics graphics)
    {
      return graphics.MeasureString(Text, Font).Width;
    }

    public float GetLabelWidth(Graphics graphics, StringFormat format)
    {
      return graphics.MeasureString(Text, Font, new PointF(0, 0), format).Width;
    }

    public float GetLabelHeight(Graphics graphics)
    {
      return graphics.MeasureString(Text, Font).Height;
    }
  }
}
