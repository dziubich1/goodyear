﻿using System;
using System.Drawing;
using Kalisto.Printouts.Enums;

namespace Kalisto.Printouts.Labels
{
  public class PrintLabelBuilder
  {
    private string _text;
    private string _fontFamily;

    private int _fontSize;
    private float _horizontalOffset;
    private float _verticalOffset;

    private FontStyle _fontStyle;
    private Color _color;
    private AlignmentEnum _alignment = AlignmentEnum.Left;
    private VAlignmentEnum _vAlignment = VAlignmentEnum.Top;

    public PrintLabelBuilder WithText(string text)
    {
      _text = text;
      return this;
    }

    public PrintLabelBuilder WithFontColor(Color color)
    {
      _color = color;
      return this;
    }

    public PrintLabelBuilder WithFontFamily(string fontFamily)
    {
      _fontFamily = fontFamily;
      return this;
    }

    public PrintLabelBuilder WithFontSize(int fontSize)
    {
      _fontSize = fontSize;
      return this;
    }

    public PrintLabelBuilder AsRegular()
    {
      _fontStyle = FontStyle.Regular;
      return this;
    }

    public PrintLabelBuilder AsBold()
    {
      _fontStyle = FontStyle.Bold;
      return this;
    }

    public PrintLabelBuilder WithHorizontalOffset(float offset)
    {
      _horizontalOffset = offset;
      return this;
    }

    public PrintLabelBuilder WithHorizontalOffset(Func<PrintLabel, float> func)
    {
      _horizontalOffset = func(Submit());
      return this;
    }

    public PrintLabelBuilder WithVerticalOffset(float offset)
    {
      _verticalOffset = offset;
      return this;
    }

    public PrintLabelBuilder AlignRight()
    {
      _alignment = AlignmentEnum.Right;
      return this;
    }

    public PrintLabelBuilder Center()
    {
      _alignment = AlignmentEnum.Center;
      return this;
    }

    public PrintLabelBuilder AlignBottom()
    {
      _vAlignment = VAlignmentEnum.Bottom;
      return this;
    }

    public PrintLabel Submit()
    {
      var label = new PrintLabel
      {
        Font = new Font(_fontFamily, _fontSize, _fontStyle),
        Color = _color,
        X = _horizontalOffset,
        Y = _verticalOffset,
        Text = _text,
        Align = _alignment,
        VAlign = _vAlignment
      };
      return label;
    }
  }

}
