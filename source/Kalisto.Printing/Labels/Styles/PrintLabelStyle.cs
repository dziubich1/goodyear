﻿using System.Drawing;

namespace Kalisto.Printouts.Labels.Styles
{
  public class PrintLabelStyle
  {
    public Font Font { get; set; }

    public Color Color { get; set; }
  }
}
