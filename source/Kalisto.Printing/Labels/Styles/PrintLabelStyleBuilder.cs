﻿using System.Drawing;

namespace Kalisto.Printouts.Labels.Styles
{
  public class PrintLabelStyleBuilder
  {
    private Color _color;
    private string _fontFamily;
    private float _fontSize;
    private FontStyle _fontStyle;

    public PrintLabelStyleBuilder WithFontColor(Color color)
    {
      _color = color;
      return this;
    }

    public PrintLabelStyleBuilder WithFontFamily(string fontFamily)
    {
      _fontFamily = fontFamily;
      return this;
    }

    public PrintLabelStyleBuilder WithFontSize(int fontSize)
    {
      _fontSize = fontSize;
      return this;
    }

    public PrintLabelStyleBuilder AsRegular()
    {
      _fontStyle = FontStyle.Regular;
      return this;
    }

    public PrintLabelStyleBuilder AsBold()
    {
      _fontStyle = FontStyle.Bold;
      return this;
    }

    public PrintLabelStyle Submit()
    {
      return new PrintLabelStyle
      {
        Font = new Font(_fontFamily, _fontSize, _fontStyle),
        Color = _color
      };
    }
  }
}
