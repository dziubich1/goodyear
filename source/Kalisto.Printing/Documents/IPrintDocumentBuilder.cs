﻿using Kalisto.Printouts.Images;
using Kalisto.Printouts.Labels;
using Kalisto.Printouts.Lines;
using Kalisto.Printouts.Tables;

namespace Kalisto.Printouts.Documents
{
  public interface IPrintDocumentBuilder
  {
    IPrintDocumentBuilder WithLabel(PrintLabel label);

    IPrintDocumentBuilder WithLine(PrintLine line);

    IPrintDocumentBuilder WithImage(PrintImage image);

    IPrintDocumentBuilder WithTable(PrintTable table);
  }
}
