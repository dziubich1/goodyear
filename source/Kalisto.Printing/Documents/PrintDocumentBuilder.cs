﻿using System.Drawing;
using System.Linq;
using Kalisto.Printouts.Enums;
using Kalisto.Printouts.Images;
using Kalisto.Printouts.Labels;
using Kalisto.Printouts.Lines;
using Kalisto.Printouts.Tables;

namespace Kalisto.Printouts.Documents
{
  public class PrintDocumentBuilder : IPrintDocumentBuilder
  {
    private readonly PrintDocument _printDocument;
    private readonly RectangleF _printingArea;
    private readonly Graphics _graphics;
    private float _defaultMarginLeft = 5;

    public PrintDocumentBuilder(Graphics g, RectangleF printArea)
    {
      g.Clear(Color.White);
      _printDocument = new PrintDocument(g);
      _printingArea = printArea;
      _graphics = g;
    }

    public IPrintDocumentBuilder WithLabel(PrintLabel label)
    {
      var xOffset = 0f;
      if (label.Align == AlignmentEnum.Center)
        xOffset = (_printingArea.Width -
                            _graphics.MeasureString(label.Text, label.Font).Width) / 2;
      else if (label.Align == AlignmentEnum.Right)
        xOffset = _printingArea.Width - _graphics.MeasureString(label.Text, label.Font).Width;

      var yOffset = 0f;
      if (label.VAlign == VAlignmentEnum.Bottom)
        yOffset = _printingArea.Bottom - _graphics.MeasureString(label.Text, label.Font).Height;

      //todo: top alignment

      _printDocument.Graphics.DrawString(label.Text, label.Font, new SolidBrush(label.Color), xOffset + label.X,
        yOffset + label.Y);
      return this;
    }

    public IPrintDocumentBuilder WithLine(PrintLine line)
    {
      var pen = new Pen(Color.Black, line.Thicnkess);
      pen.DashStyle = line.DashStyle;
      _graphics.DrawLine(pen, line.StartPoint.X, line.StartPoint.Y, line.EndPoint.X, line.EndPoint.Y);
      return this;
    }

    public IPrintDocumentBuilder WithImage(PrintImage image)
    {
      var xOffset = 0;
      if (image.Align == AlignmentEnum.Center)
        xOffset = ((int)_printingArea.Width -
                            image.Source.Width) / 2;

      var yOffset = 0;
      if (image.VAlign == VAlignmentEnum.Bottom)
        yOffset = (int)_printingArea.Bottom - image.Source.Height;

      _printDocument.Graphics.DrawImage(image.Source,
        new Rectangle(xOffset + (int) image.X, yOffset + (int) image.Y, image.Source.Width, image.Source.Height));
      return this;
    }

    public IPrintDocumentBuilder WithTable(PrintTable table)
    {
      var tableTotalWidth = table.Columns.Sum(c => c.Width);
      var horizontalOffset = 1f;
      if(table.Alignment == AlignmentEnum.Center)
        horizontalOffset = _printingArea.Left + (_printingArea.Right - tableTotalWidth) / 2;
      else if (table.Alignment == AlignmentEnum.Left)
        horizontalOffset = _printingArea.Left;

      for (int iCol = 0; iCol < table.Columns.Length; iCol++)
      {
        if (iCol > 0)
          horizontalOffset += table.Columns[iCol - 1].Width;

        _graphics.DrawRectangle(new Pen(Color.Black, table.BorderThickness), horizontalOffset, table.VerticalOffset, table.Columns[iCol].Width, table.HeaderHeight);
        _graphics.DrawString(table.Columns[iCol].Text, table.HeaderStyle.Font, new SolidBrush(table.HeaderStyle.Color),
          horizontalOffset +
          (table.Columns[iCol].Width - _graphics.MeasureString(table.Columns[iCol].Text, table.HeaderStyle.Font).Width) /
          2,
          table.VerticalOffset + 1 + (table.HeaderHeight - (2 * table.BorderThickness) - _graphics.MeasureString("x", table.HeaderStyle.Font).Height) / 2);

        var rowIndex = 0;
        foreach (var row in table.Rows)
        {
          if(row.Cells[iCol].IsBlank)
            continue;

          _graphics.DrawRectangle(new Pen(Color.Black, table.BorderThickness), horizontalOffset, table.VerticalOffset + table.HeaderHeight + (table.CellHeight * rowIndex), table.Columns[iCol].Width, table.CellHeight);
          if (row.Cells[iCol].Splitter == null)
          {
            var xOffset = horizontalOffset +
                          (table.Columns[iCol].Width -
                           _graphics.MeasureString(row.Cells[iCol].Text, table.CellStyle.Font).Width)/2;
            if (row.Cells[iCol].Alignment == AlignmentEnum.Left)
              xOffset = horizontalOffset + _defaultMarginLeft;
            else if (row.Cells[iCol].Alignment == AlignmentEnum.Right)
              xOffset = horizontalOffset + table.Columns[iCol].Width -
                        _graphics.MeasureString(row.Cells[iCol].Text, table.CellStyle.Font).Width - _defaultMarginLeft;

            _graphics.DrawString(row.Cells[iCol].Text, table.CellStyle.Font, new SolidBrush(table.CellStyle.Color),
              new RectangleF(xOffset, table.VerticalOffset + table.HeaderHeight + (table.CellHeight * rowIndex++) + 1 +
              (table.CellHeight - (2 * table.BorderThickness) - _graphics.MeasureString("x", table.CellStyle.Font).Height) / 2, table.Columns[iCol].Width - 2, table.CellHeight - (2 * table.BorderThickness)));
          }
          else
          {
            _graphics.DrawRectangle(new Pen(Color.Black, table.BorderThickness), horizontalOffset + table.Columns[iCol].Width - row.Cells[iCol].Splitter.Width, table.VerticalOffset + table.HeaderHeight + (table.CellHeight * rowIndex), row.Cells[iCol].Splitter.Width, table.CellHeight);
            _graphics.DrawString(row.Cells[iCol].Splitter.Text, table.CellStyle.Font, new SolidBrush(table.CellStyle.Color),
              horizontalOffset +
              (table.Columns[iCol].Width - row.Cells[iCol].Splitter.Width + (row.Cells[iCol].Splitter.Width - _graphics.MeasureString(row.Cells[iCol].Splitter.Text, table.CellStyle.Font).Width) / 2),
              table.VerticalOffset + table.HeaderHeight + (table.CellHeight * rowIndex) + 1 +
              (table.CellHeight - (2 * table.BorderThickness) - _graphics.MeasureString("x", table.CellStyle.Font).Height) / 2);

            _graphics.DrawString(row.Cells[iCol].Text, table.CellStyle.Font, new SolidBrush(table.CellStyle.Color),
              horizontalOffset +
              (table.Columns[iCol].Width - row.Cells[iCol].Splitter.Width - _graphics.MeasureString(row.Cells[iCol].Text, table.CellStyle.Font).Width) / 2,
              table.VerticalOffset + table.HeaderHeight + (table.CellHeight * rowIndex++) + 1 +
              (table.CellHeight - (2 * table.BorderThickness) - _graphics.MeasureString("x", table.CellStyle.Font).Height) / 2);
          }
        }
      }

      return this;
    }
  }
}
