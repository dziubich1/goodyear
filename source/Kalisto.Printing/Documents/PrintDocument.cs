﻿using System.Drawing;

namespace Kalisto.Printouts.Documents
{
  public class PrintDocument
  {
    public Graphics Graphics { get; }

    public PrintDocument(Graphics graphics)
    {
      Graphics = graphics;
    }
  }
}
