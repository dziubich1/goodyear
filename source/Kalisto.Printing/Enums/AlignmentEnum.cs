﻿namespace Kalisto.Printouts.Enums
{
  public enum AlignmentEnum
  {
    Left, Right, Center
  }

  public enum VAlignmentEnum
  {
    Top, Bottom
  }
}
