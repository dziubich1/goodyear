﻿using System.Drawing;
using Kalisto.Core;
using Kalisto.Printing.Layout;
using Kalisto.Printouts.Enums;

namespace Kalisto.Printouts.Images
{
  public class PrintImageBuilder
  {
    private Image _image;
    private float _horizontalOffset;
    private float _verticalOffset;

    private AlignmentEnum _alignment = AlignmentEnum.Left;
    private VAlignmentEnum _vAlignment = VAlignmentEnum.Top;

    private GridLayout _grid;
    private int _columnIndex;
    private int _rowIndex;

    public PrintImageBuilder WithImageSource(Image image)
    {
      _image = image;
      return this;
    }

    public PrintImageBuilder WithHorizontalOffset(float offset)
    {
      _horizontalOffset = offset;
      return this;
    }

    public PrintImageBuilder WithVerticalOffset(float offset)
    {
      _verticalOffset = offset;
      return this;
    }

    public PrintImageBuilder Center()
    {
      _alignment = AlignmentEnum.Center;
      return this;
    }

    public PrintImageBuilder AlignBottom()
    {
      _vAlignment = VAlignmentEnum.Bottom;
      return this;
    }

    public PrintImageBuilder OnGridLayout(GridLayout layout)
    {
      _grid = layout;
      return this;
    }

    public PrintImageBuilder AtColumn(int index)
    {
      _columnIndex = index;
      return this;
    }

    public PrintImageBuilder AtRow(int index)
    {
      _rowIndex = index;
      return this;
    }

    public PrintImage Submit()
    {
      var image = new PrintImage
      {
        X = _horizontalOffset,
        Y = _verticalOffset,
        Align = _alignment,
        VAlign = _vAlignment,
        Source = _image
      };

      if (_grid != null)
      {
        var resizedSource = _image.ResizeImage(_grid.ColumnWidth, _grid.RowHeight);
        var position = _grid.GetPosition(_rowIndex, _columnIndex, resizedSource.Size);
        image.X = position.X;
        image.Y = position.Y;
        image.Source = resizedSource;
      }
      
      return image;
    }
  }
}
