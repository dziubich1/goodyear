﻿using System.Drawing;
using Kalisto.Printouts.Enums;

namespace Kalisto.Printouts.Images
{
  public class PrintImage
  {
    public AlignmentEnum Align { get; set; }

    public VAlignmentEnum VAlign { get; set; }

    public Image Source { get; set; }

    public float X { get; set; }

    public float Y { get; set; }
  }
}
