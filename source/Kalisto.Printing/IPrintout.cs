﻿using System.Drawing.Printing;

namespace Kalisto.Printouts
{
  public interface IPrintout
  {
    bool Print(int page);

    void Initialize(PrintPageEventArgs printSettings);
  }
}
