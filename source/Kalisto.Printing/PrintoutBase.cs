﻿using System.Drawing;
using System.Drawing.Printing;

namespace Kalisto.Printouts
{
  public abstract class PrintoutBase : IPrintout
  {
    protected  Graphics Graphics { get; set; }

    protected PageSettings PageSettings { get; set; }

    protected Rectangle PageBounds { get; set; }

    protected RectangleF PrintingArea { get; set; }

    protected float PageMiddleHeight
    {
      get { return (PrintingArea.Bottom - PrintingArea.Top)/2; }
    }

    public abstract bool Print(int page);

    public void Initialize(PrintPageEventArgs printSettings)
    {
      var printableArea = printSettings.PageSettings.PrintableArea;
      printableArea.Width -= printSettings.PageSettings.HardMarginX * 2;
      printableArea.Height -= printSettings.PageSettings.HardMarginY * 2;
      printSettings.PageSettings.Margins = new Margins(0, 0, 0, 0);

      Graphics = printSettings.Graphics;
      PageSettings = printSettings.PageSettings;
      PageBounds = printSettings.PageBounds;
      PrintingArea = printableArea;
    }
  }
}
