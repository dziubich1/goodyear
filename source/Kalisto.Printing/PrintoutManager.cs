﻿using Kalisto.Printouts;
using System;
using System.Drawing.Printing;

namespace Kalisto.Printing
{
  public interface IPrintoutManager
  {
    void Print(IPrintout printout);
  }

  public class PrintoutManager : IPrintoutManager
  {
    private readonly PrintDocument _printingDialog;

    private int _currentPage;

    private IPrintout _currentPrintout;

    public bool IsBusy { get; private set; }

    public PrintoutManager()
    {
      _printingDialog = new PrintDocument();
      _printingDialog.PrintPage += OnPagePrinting;
      _printingDialog.EndPrint += OnPagePrintingEnd;
    }

    public void Print(IPrintout printout)
    {
      if (printout == null)
        throw new ArgumentNullException();

      if (IsBusy)
        return;

      IsBusy = true;
      _currentPrintout = printout;
      _currentPage = 0;
      _printingDialog.Print();
    }

    private void OnPagePrinting(object sender, PrintPageEventArgs e)
    {
      _currentPage++;
      _currentPrintout.Initialize(e);

      var hasMorePages = _currentPrintout.Print(_currentPage);
      e.HasMorePages = hasMorePages;
    }

    private void OnPagePrintingEnd(object sender, PrintEventArgs e)
    {
      IsBusy = false;
    }
  }
}
