﻿using Kalisto.Backend.DataAccess.Effort;
using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.DataAccess.Model.Logging;
using Kalisto.Backend.Operations.Cargoes.Commands;
using Kalisto.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Linq;

namespace Kalisto.Backend.Operations.Cargoes.UnitTests
{
  [TestClass]
  public class DeleteCargoCommandHandlerTests
  {
    private IDbContext _dbContext;

    private readonly string _partnerId = "unknown";

    private Mock<IDbDeleteEventLogger> _dbEventLoggerMock;

    // component under tests
    private DeleteCargoCommandHandler _deleteCargoCommandHandler;

    [TestInitialize]
    public void Initialize()
    {
      var effortConnection = Effort.DbConnectionFactory.CreateTransient();
      _dbContext = new EffortDbContext(effortConnection);

      _dbEventLoggerMock = new Mock<IDbDeleteEventLogger>();

      var logger = new Mock<ILogger>();

      _deleteCargoCommandHandler = new DeleteCargoCommandHandler(_dbEventLoggerMock.Object, logger.Object);
    }

    [TestCleanup]
    public void CleanUp()
    {
      _dbContext.Dispose();
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void Execute_ShouldThrowArgumentException_WhenCargoWithGivenIdDoesNotExist()
    {
      // Setup
      var testName = "test";
      _dbContext.Cargoes.Add(new Cargo { Name = testName }); // generated id: 1
      _dbContext.SaveChanges();

      // Arrange
      var command = new DeleteCargoCommand
      {
        CargoId = 2 // not existing contractor id
      };

      // Act & Assert
      _deleteCargoCommandHandler.Execute(_dbContext, command, _partnerId);
    }

    [TestMethod]
    public void Execute_ShouldRemoveCargo()
    {
      // Setup
      _dbContext.Cargoes.Add(new Cargo
      {
        Name = "TestName",
        Price = 425
      }); // generated id: 1
      _dbContext.SaveChanges();

      // Arrange
      var command = new DeleteCargoCommand
      {
        CargoId = 1
      };

      // Act
      _deleteCargoCommandHandler.Execute(_dbContext, command, _partnerId);

      // Assert
      Assert.AreEqual(0, _dbContext.Cargoes.Count());
    }

    [TestMethod]
    public void Execute_ShouldCreateDbDeleteEventLog()
    {
      // Setup
      _dbEventLoggerMock.Setup(c => c.LogEvent(_partnerId, ctx => ctx.Cargoes,
          It.IsAny<int>(), null, It.IsAny<Cargo>()))
        .Verifiable();

      _dbContext.Cargoes.Add(new Cargo
      {
        Name = "TestName",
        Price = 425
      }); // generated id: 1
      _dbContext.SaveChanges();

      // Arrange
      var command = new DeleteCargoCommand
      {
        CargoId = 1
      };

      // Act
      _deleteCargoCommandHandler.Execute(_dbContext, command, "unknown");

      // Assert
      _dbEventLoggerMock.Verify(c => c.LogEvent(_partnerId, ctx => ctx.Cargoes,
        1, null, It.IsAny<Cargo>()), Times.Once);
    }
  }
}
