﻿using Kalisto.Backend.DataAccess.Effort;
using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.DataAccess.Model.Logging;
using Kalisto.Backend.Operations.Cargoes.Commands;
using Kalisto.Backend.Operations.Cargoes.Exceptions;
using Kalisto.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Linq;

namespace Kalisto.Backend.Operations.Cargoes.UnitTests
{
  [TestClass]
  public class UpdateCargoCommandHandlerTests
  {
    private IDbContext _dbContext;

    private readonly string _partnerId = "unknown";

    private Mock<IDbUpdateEventLogger> _dbEventLoggerMock;

    // component under tests
    private UpdateCargoCommandHandler _updateCargoCommandHandler;

    [TestInitialize]
    public void Initialize()
    {
      var effortConnection = Effort.DbConnectionFactory.CreateTransient();
      _dbContext = new EffortDbContext(effortConnection);

      _dbEventLoggerMock = new Mock<IDbUpdateEventLogger>();

      var logger = new Mock<ILogger>();

      _updateCargoCommandHandler = new UpdateCargoCommandHandler(_dbEventLoggerMock.Object, logger.Object);
    }

    [TestCleanup]
    public void CleanUp()
    {
      _dbContext.Dispose();
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void Execute_ShouldThrowArgumentException_WhenCargoWithGivenIdDoesNotExist()
    {
      // Arrange
      var notExistingCargoId = 5;
      var command = new UpdateCargoCommand { CargoId = notExistingCargoId };

      // Act & Assert
      _updateCargoCommandHandler.Execute(_dbContext, command, _partnerId);
    }

    [TestMethod]
    [ExpectedException(typeof(CargoExistsException))]
    public void Execute_ShouldThrowCargoExistsException_WhenNameOfCurrentCargoHasChangedButCargoWithSameNameAlreadyExists()
    {
      // Setup
      var testName = "test";
      _dbContext.Cargoes.Add(new Cargo { Name = testName }); // generated id: 1
      var currentName = "current";
      _dbContext.Cargoes.Add(new Cargo { Name = currentName }); // generated id: 2
      _dbContext.SaveChanges();

      // Arrange
      var command = new UpdateCargoCommand { CargoId = 2, Name = testName };

      // Act & Assert
      _updateCargoCommandHandler.Execute(_dbContext, command, _partnerId);
    }

    [TestMethod]
    public void Execute_ShouldUpdateCargo()
    {
      // Setup
      var currentName = "currentName";
      var currentPrice = 425;
      _dbContext.Cargoes.Add(new Cargo
      {
        Name = currentName,
        Price = currentPrice
      }); // generated id: 1
      _dbContext.SaveChanges();

      // Arrange
      var newName = "newName";
      var newPrice = 6969;
      var command = new UpdateCargoCommand
      {
        CargoId = 1,
        Name = newName,
        Price = newPrice
      };

      // Act
      _updateCargoCommandHandler.Execute(_dbContext, command, _partnerId);

      // Assert
      var cargo = _dbContext.Cargoes.Single();
      Assert.AreEqual(newName, cargo.Name);
      Assert.AreEqual(newPrice, cargo.Price);
    }

    [TestMethod]
    public void Execute_ShouldReturnUpdatedCargoId()
    {
      // Setup
      var currentName = "currentName";
      var currentPrice = 425;
      _dbContext.Cargoes.Add(new Cargo
      {
        Name = currentName,
        Price = currentPrice
      }); // generated id: 1
      _dbContext.SaveChanges();

      // Arrange
      var newName = "newName";
      var newPrice = 6969;
      var command = new UpdateCargoCommand
      {
        CargoId = 1,
        Name = newName,
        Price = newPrice
      };

      // Act
      var resultId = _updateCargoCommandHandler.Execute(_dbContext, command, _partnerId);

      // Assert
      var cargo = _dbContext.Cargoes.Single();
      Assert.AreEqual(1L, cargo.Id);
      Assert.AreEqual(1L, resultId);
    }

    [TestMethod]
    public void Execute_ShouldCreateDbUpdateEventLog()
    {
      // Setup
      _dbEventLoggerMock.Setup(c => c.LogEvent(_partnerId, ctx => ctx.Cargoes,
          It.IsAny<long>(), It.IsAny<Cargo>()))
        .Verifiable();

      _dbContext.Cargoes.Add(new Cargo
      {
        Name = "currentName",
        Price = 425
      }); // generated id: 1
      _dbContext.SaveChanges();

      // Arrange
      var command = new UpdateCargoCommand
      {
        CargoId = 1,
        Name = "newName",
        Price = 6969
      };

      // Act
      _updateCargoCommandHandler.Execute(_dbContext, command, _partnerId);

      // Assert
      _dbEventLoggerMock.Verify(c => c.LogEvent(_partnerId, ctx => ctx.Cargoes,
        It.IsAny<long>(), It.IsAny<Cargo>(), It.IsAny<Cargo>()), Times.Once);
    }
  }
}
