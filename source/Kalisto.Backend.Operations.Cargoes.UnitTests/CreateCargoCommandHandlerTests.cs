﻿using Kalisto.Backend.DataAccess.Effort;
using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.DataAccess.Model.Logging;
using Kalisto.Backend.Operations.Cargoes.Commands;
using Kalisto.Backend.Operations.Cargoes.Exceptions;
using Kalisto.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;

namespace Kalisto.Backend.Operations.Cargoes.UnitTests
{
  [TestClass]
  public class CreateCargoCommandHandlerTests
  {
    private IDbContext _dbContext;

    private readonly string _partnerId = "unknown";

    private Mock<IDbInsertEventLogger> _dbEventLoggerMock;

    // component under tests
    private CreateCargoCommandHandler _createCargoCommandHandler;

    [TestInitialize]
    public void Initialize()
    {
      var effortConnection = Effort.DbConnectionFactory.CreateTransient();
      _dbContext = new EffortDbContext(effortConnection);

      _dbEventLoggerMock = new Mock<IDbInsertEventLogger>();

      var logger = new Mock<ILogger>();

      _createCargoCommandHandler = new CreateCargoCommandHandler(_dbEventLoggerMock.Object, logger.Object);
    }

    [TestCleanup]
    public void CleanUp()
    {
      _dbContext.Dispose();
    }

    [TestMethod]
    [ExpectedException(typeof(CargoExistsException))]
    public void Execute_ShouldThrowCargoExistsException_WhenCargoWithGivenNameAlreadyExists()
    {
      // Setup
      var testName = "test";
      _dbContext.Cargoes.Add(new Cargo { Name = testName });
      _dbContext.SaveChanges();

      // Arrange
      var createCargoCommand = new CreateCargoCommand
      {
        Name = testName
      };

      // Act & Assert
      _createCargoCommandHandler.Execute(_dbContext,
        createCargoCommand, _partnerId);
    }

    [TestMethod]
    public void Execute_ShouldNotCreateCargo_WhenCargoWithGivenNameAlreadyExists()
    {
      // Setup
      var testName = "test";
      _dbContext.Cargoes.Add(new Cargo { Name = testName });
      _dbContext.SaveChanges();
      var expectedCargoesCount = _dbContext.Cargoes.Count();

      // Arrange
      var createCargoCommand = new CreateCargoCommand
      {
        Name = testName
      };

      try
      {
        // Act
        _createCargoCommandHandler.Execute(_dbContext,
          createCargoCommand, _partnerId);
      }
      catch (CargoExistsException)
      {
        // Assert
        Assert.AreEqual(expectedCargoesCount,
          _dbContext.Cargoes.Count());
      }
    }

    [TestMethod]
    public void Execute_ShouldCreateCargo()
    {
      // Arrange
      var createCargoCommand = new CreateCargoCommand
      {
        Name = "TestName",
        Price = 452
      };

      // Act
      _createCargoCommandHandler.Execute(_dbContext,
        createCargoCommand, _partnerId);

      // Assert
      Assert.AreEqual(1, _dbContext.Cargoes.Count(),
        "Cargoes table should contain one contractor");

      var createdCargo = _dbContext.Cargoes.Single();
      Assert.AreEqual(1, createdCargo.Id,
        "Id of created cargo should be auto generated");
      Assert.AreEqual(createCargoCommand.Name, createdCargo.Name,
        "Name of created cargo should be the same like in command");
      Assert.AreEqual(createCargoCommand.Price, createdCargo.Price,
        "Price of created cargo should be the same like in command");
    }

    [TestMethod]
    public void Execute_ShouldReturnIdentifierOfNewlyCreatedCargo()
    {
      // Arrange
      var createCargoCommand = new CreateCargoCommand
      {
        Name = "TestName",
        Price = 452
      };

      // Act
      var id = _createCargoCommandHandler.Execute(_dbContext,
        createCargoCommand, _partnerId);

      // Assert
      var createdContractor = _dbContext.Cargoes.Single();
      Assert.AreEqual(1L, id, "Id of created cargo should be auto generated and returned properly by Execute(...) method");
      Assert.AreEqual(1L, createdContractor.Id, "Id of created cargo entity should be auto generated and returned properly by Execute(...) method");
    }

    [TestMethod]
    public void Execute_ShouldCreateDbInsertEventLog()
    {
      // Setup
      _dbEventLoggerMock.Setup(c => c.LogEvent(_partnerId, ctx => ctx.Cargoes,
          It.IsAny<int>(), It.IsAny<Cargo>()))
        .Verifiable();

      // Arrange
      var createCargoCommand = new CreateCargoCommand
      {
        Name = "TestName",
        Price = 452
      };

      // Act
      _createCargoCommandHandler.Execute(_dbContext,
        createCargoCommand, _partnerId);

      // Assert
      _dbEventLoggerMock.Verify(c => c.LogEvent(_partnerId, ctx => ctx.Cargoes,
        1, It.IsAny<Cargo>()), Times.Once);
    }
  }
}
