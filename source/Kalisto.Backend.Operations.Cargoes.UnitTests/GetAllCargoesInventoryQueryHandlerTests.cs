﻿using System;
using System.Collections.Generic;
using System.Linq;
using Kalisto.Backend.DataAccess.Effort;
using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.DataAccess.Model.Logging;
using Kalisto.Backend.Operations.Cargoes.Queries;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Kalisto.Backend.Operations.Cargoes.UnitTests
{
  [TestClass]
  public class GetAllCargoesInventoryQueryHandlerTests
  {
    private IDbContext _dbContext;

    private readonly string _partnerId = "unknown";

    // component under tests
    private GetAllCargoesInventoryQueryHandler _getAllCargoesInventoryQueryHandler;

    [TestInitialize]
    public void Initialize()
    {
      var effortConnection = Effort.DbConnectionFactory.CreateTransient();
      _dbContext = new EffortDbContext(effortConnection);
      _getAllCargoesInventoryQueryHandler = new GetAllCargoesInventoryQueryHandler();
    }

    [TestMethod]
    public void Execute_ShouldReturnActualAmountProperly()
    {
      // Setup
      var cargo = new Cargo
      {
        Name = "Test1",
        Price = 1M,
      };
      _dbContext.Cargoes.Add(cargo);

      for (var i = 1; i <= 5; i++)
      {
        _dbContext.Measurements.Add(new Measurement
        {
          Cargo = cargo,
          Class = MeasurementClass.Income,
          NetWeight = i
        });
      }
      for (var i = 1; i <= 3; i++)
      {
        _dbContext.Measurements.Add(new Measurement
        {
          Cargo = cargo,
          Class = MeasurementClass.Outcome,
          NetWeight = i
        });
      }

      _dbContext.SaveChanges();

      // Arrange
      var getAllCargoesInventoryQuery = new GetAllCargoesInventoryQuery();

      // Act
      var result = _getAllCargoesInventoryQueryHandler.Execute(_dbContext, getAllCargoesInventoryQuery, _partnerId) as List<CargoInventoryDto>;

      // Assert
      Assert.AreEqual(9M, result.First().ActualAmount);
      Assert.AreEqual(cargo.Name, result.First().Name);
      Assert.AreEqual(cargo.Price, result.First().Price);
    }
  }
}
