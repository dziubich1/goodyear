﻿using System;

namespace Kalisto.Printing.WeighingList
{
  public class WeighingListItemDto
  {
    public int Index { get; set; }

    public int Id { get; set; }

    public string Cargo { get; set; }

    public string MeasurementDate { get; set; }

    public string Contractor { get; set; }

    public string Car { get; set; }

    public string Driver { get; set; }

    public decimal? Gross { get; set; }

    public decimal? Net { get; set; }
  }
}
