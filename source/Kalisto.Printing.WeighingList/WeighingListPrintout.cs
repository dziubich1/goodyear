﻿using System;
using System.Drawing;
using System.Linq;
using Kalisto.Localization;
using Kalisto.Localization.Common;
using Kalisto.Printouts;
using Kalisto.Printouts.Documents;
using Kalisto.Printouts.Labels;
using Kalisto.Printouts.Labels.Styles;
using Kalisto.Printouts.Lines;
using Kalisto.Printouts.Tables;
using Kalisto.Printouts.Tables.Cells;
using Kalisto.Printouts.Tables.Columns;
using Kalisto.Printouts.Tables.Rows;

namespace Kalisto.Printing.WeighingList
{
  public class WeighingListPrintout : PrintoutBase
  {
    private readonly WeighingListDto _dto;

    private readonly ILocalizer<Localization.Printouts.Printouts> _printoutLocalizer;
    private readonly ILocalizer<Common> _commonLocalizer;

    public WeighingListPrintout(WeighingListDto dto)
    {
      _dto = dto;
      _printoutLocalizer = new LocalizationManager<Localization.Printouts.Printouts>();
      _commonLocalizer = new LocalizationManager<Common>();
    }

    public override bool Print(int page)
    {
      var hasMorePages = false;
      var printoutBuilder = new PrintDocumentBuilder(Graphics, PrintingArea);

      printoutBuilder.WithLabel(new PrintLabelBuilder()
        .WithText(DateTime.Now.ToString("dd.MM.yyyy r."))
        .AsRegular()
        .WithFontColor(Color.Black)
        .WithFontFamily("Arial")
        .WithFontSize(9)
        .WithVerticalOffset(PrintingArea.Top)
        .AlignRight()
        .Submit());

      printoutBuilder.WithLabel(new PrintLabelBuilder()
        .WithText(_dto.Company)
        .AsRegular()
        .WithFontColor(Color.Black)
        .WithFontFamily("Arial")
        .WithFontSize(9)
        .WithVerticalOffset(PrintingArea.Top)
        .WithHorizontalOffset(PrintingArea.Left)
        .Submit());

      printoutBuilder.WithLabel(new PrintLabelBuilder()
        .WithText(string.Format(_printoutLocalizer.Localize(() => Localization.Printouts.Printouts.WeighingListPrintoutHeader), _dto.FromDateFormatted, _dto.ToDateFormatted))
        .AsBold()
        .WithFontColor(Color.Black)
        .WithFontFamily("Arial")
        .WithFontSize(15)
        .WithVerticalOffset(PrintingArea.Top + 100)
        .Center()
        .Submit());

      var footerLabel = new PrintLabelBuilder()
        .WithText(_printoutLocalizer.Localize(() => Localization.Printouts.Printouts.PrintedByWagaPro))
        .AsRegular()
        .WithFontColor(Color.Black)
        .WithFontFamily("Arial")
        .WithFontSize(7)
        .WithHorizontalOffset(PrintingArea.Left)
        .AlignBottom()
        .Submit();
      printoutBuilder.WithLabel(footerLabel);

      printoutBuilder.WithLine(new PrintLineBuilder()
        .StartsAt((int)PrintingArea.Left, (int)(PrintingArea.Bottom - footerLabel.GetLabelHeight(Graphics) - 3))
        .EndsAt((int)(PrintingArea.Width / 2), (int)(PrintingArea.Bottom - footerLabel.GetLabelHeight(Graphics) - 3))
        .WithThickness(0.25f)
        .Submit());

      var tablePlaceholderHeight =
        Convert.ToInt32(PrintingArea.Bottom - footerLabel.GetLabelHeight(Graphics) - 50 - 150 - 20);
      var itemsPerPage = (int)Math.Floor(tablePlaceholderHeight / 20.0);

      decimal pagesCountDecimal = (decimal)_dto.EvidenceList.Count / (decimal)itemsPerPage;
      var totalPages = (int)Math.Round(pagesCountDecimal);
      if (_dto.EvidenceList.Count - itemsPerPage * totalPages > 0)
        totalPages++;

      printoutBuilder.WithLabel(new PrintLabelBuilder()
        .WithText(string.Format(_printoutLocalizer.Localize(() => Localization.Printouts.Printouts.PageNumber), page, totalPages))
        .AsRegular()
        .WithFontColor(Color.Black)
        .WithFontFamily("Arial")
        .WithFontSize(7)
        .AlignRight()
        .AlignBottom()
        .Submit());

      var table = new PrintTableBuilder()
        .WithColumns(new[]
        {
          new PrintTableColumnBuilder()
            .WithText(_commonLocalizer.Localize(() => Common.OrdinalIdHeader))
            .WithWidth(50)
            .Submit(),
          new PrintTableColumnBuilder()
            .WithText(_commonLocalizer.Localize(() => Common.CargoHeader))
            .WithWidth(160)
            .Submit(),
          new PrintTableColumnBuilder()
            .WithText(_commonLocalizer.Localize(() => Common.MeasurementDateHeader))
            .WithWidth(120)
            .Submit(),
          new PrintTableColumnBuilder()
            .WithText(_commonLocalizer.Localize(() => Common.ContractorHeader))
            .WithWidth(125)
            .Submit(),
          new PrintTableColumnBuilder()
            .WithText(_commonLocalizer.Localize(() => Common.VehicleHeader))
            .WithWidth(75)
            .Submit(),
          new PrintTableColumnBuilder()
            .WithText(_commonLocalizer.Localize(() => Common.DriverHeader))
            .WithWidth(120)
            .Submit(),
          new PrintTableColumnBuilder()
            .WithText(_commonLocalizer.Localize(() => Common.GrossHeader))
            .WithWidth(65)
            .Submit(),
          new PrintTableColumnBuilder()
            .WithText(_commonLocalizer.Localize(() => Common.NetHeader))
            .WithWidth(65)
            .Submit()
        })
        .WithHeaderStyle(new PrintLabelStyleBuilder()
          .AsBold()
          .WithFontColor(Color.Black)
          .WithFontFamily("Arial")
          .WithFontSize(9)
          .Submit())
        .WithCellStyle(new PrintLabelStyleBuilder()
          .AsRegular()
          .WithFontColor(Color.Black)
          .WithFontFamily("Arial")
          .WithFontSize(7)
          .Submit())
          .WithBorderThickness(0.25f)
        .WithVerticalOffset(PrintingArea.Top + 150)
        .WithDataSource(
          _dto.EvidenceList.Skip((page - 1) * itemsPerPage).Take(itemsPerPage).Select(c => new PrintTableRow
          {
            Cells = new[]
              {
              new PrintTableCellBuilder()
                .WithText(c.Index.ToString())
                .AlignLeft()
                .Submit(),
              new PrintTableCellBuilder()
                .WithText(c.Cargo)
                .AlignLeft()
                .Submit(),
              new PrintTableCellBuilder()
                .WithText(c.MeasurementDate)
                .Submit(),
              new PrintTableCellBuilder()
                .WithText(c.Contractor)
                .AlignLeft()
                .Submit(),
              new PrintTableCellBuilder()
                .WithText(c.Car)
                .AlignLeft()
                .Submit(),
              new PrintTableCellBuilder()
                .WithText(c.Driver)
                .AlignLeft()
                .Submit(),
              new PrintTableCellBuilder()
                .WithText(c.Gross.ToString())
                .AlignRight()
                .Submit(),
              new PrintTableCellBuilder()
                .WithText(c.Net.ToString())
                .AlignRight()
                .Submit(),
            }
          }).ToArray())
        .Submit();
      printoutBuilder.WithTable(table);

      if (page < totalPages)
        hasMorePages = true;
      else
      {
        var summaryRightOffset = (PrintingArea.Width - table.TotalWidth) / 2;
        var summaryVerticalOffset = 150 + table.TotalHeight;
        printoutBuilder.WithLabel(new PrintLabelBuilder()
          .WithText($"{_commonLocalizer.Localize(() => Common.NetSumLabel)} " + _dto.NetSumFormatted)
          .AsBold()
          .WithFontColor(Color.Black)
          .WithFontFamily("Arial")
          .WithFontSize(9)
          .AlignRight()
          .WithHorizontalOffset(-summaryRightOffset)
          .WithVerticalOffset(summaryVerticalOffset + 10)
          .Submit());
      }

      return hasMorePages;
    }
  }
}
