﻿using System.Collections.Generic;

namespace Kalisto.Printing.WeighingList
{
  public class WeighingListDto
  {
    public List<WeighingListItemDto> EvidenceList { get; set; }

    public string NetSumFormatted { get; set; }

    public string Company { get; set; }

    public string FromDateFormatted { get; set; }

    public string ToDateFormatted { get; set; }
  }
}
