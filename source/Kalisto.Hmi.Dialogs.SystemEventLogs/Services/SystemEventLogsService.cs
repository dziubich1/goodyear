﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.Operations.SystemEventLogs.Queries;
using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Hmi.Core.Communication;
using Kalisto.Hmi.Core.Services;
using Kalisto.Hmi.Dialogs.SystemEventLogs.Models;
using Kalisto.Localization;
using Kalisto.Localization.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kalisto.Hmi.Dialogs.SystemEventLogs.Services
{
  public interface ISystemEventLogsService : IBackendOperationService
  {
    Task<RequestResult<List<SystemEventLogModel>>> GetLogsAsync(DateTime from, DateTime to);
  }

  public class SystemEventLogsService : BackendOperationService, ISystemEventLogsService
  {
    private readonly ILocalizer<Messages> _localizer;

    public SystemEventLogsService(ClientInfo clientInfo, IMessageBuilder messageBuilder, IFrontendMessageBridge messageBridge, IDialogService dialogService)
      : base(clientInfo, messageBuilder, messageBridge, dialogService)
    {
      _localizer = new LocalizationManager<Messages>();
    }

    public async Task<RequestResult<List<SystemEventLogModel>>> GetLogsAsync(DateTime from, DateTime to)
    {
      var fromUtc = from.ToUniversalTime();
      var toUtc = to.ToUniversalTime();
      var message = MessageBuilder.CreateNew<GetSystemEventLogsQuery>()
        .BasedOn(ClientInfo)
        .WithProperty(c => c.FromUtc, fromUtc)
        .WithProperty(c => c.ToUtc, toUtc)
        .Build();

      var result = await MessageBridge.SendMessageWithResultAsync<List<SystemLog>>(message);
      return new RequestResult<List<SystemEventLogModel>>
      {
        Succeeded = result?.Succeeded ?? false,
        Response = result?.Response?.Select((s, index) =>
        {
          var parameters = s.Parameters.Split(',').Select(c => (object)c).ToArray();
          return new SystemEventLogModel
          {
            Id = index + 1,
            Date = s.EventDateUtc.ToLocalTime(),
            LocalizedDescription = string.Format(_localizer.Localize(s.LogResourceKey), parameters),
            Source = s.Source
          };
        }).ToList() ?? new List<SystemEventLogModel>()
      };
    }
  }
}
