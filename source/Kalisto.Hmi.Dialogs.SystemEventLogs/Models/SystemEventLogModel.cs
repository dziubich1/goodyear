﻿using System;

namespace Kalisto.Hmi.Dialogs.SystemEventLogs.Models
{
  public class SystemEventLogModel
  {
    public int Id { get; set; }
    public string LocalizedDescription { get; set; }
    public string Source { get; set; }
    public DateTime Date { get; set; }
  }
}
