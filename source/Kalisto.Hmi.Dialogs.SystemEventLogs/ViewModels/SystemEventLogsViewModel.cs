﻿using Kalisto.Hmi.Core;
using Kalisto.Hmi.Core.Services;
using Kalisto.Hmi.Dialogs.SystemEventLogs.Models;
using Kalisto.Hmi.Dialogs.SystemEventLogs.Services;
using PropertyChanged;
using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Kalisto.Hmi.Core.Commands;

namespace Kalisto.Hmi.Dialogs.SystemEventLogs.ViewModels
{
  [AddINotifyPropertyChangedInterface]
  public class SystemEventLogsViewModel : DataGridViewModel<SystemEventLogModel>
  {
    private readonly TimeSpan _initialLogsTimespan = TimeSpan.FromDays(1);

    private readonly ISystemEventLogsService _systemEventLogService;

    public override string DialogTitleKey => "SystemEventsLogs";

    public ICommand SyncItemsCommand { get; set; }

    public ObservableCollectionEx<SystemEventLogModel> SystemEventLogs { get; set; }

    public DateTime FromDate { get; set; }

    public DateTime ToDate { get; set; }

    public SystemEventLogsViewModel(IDialogService dialogService, IAuthorizationService authorizationService, ISystemEventLogsService systemEventLogService)
      : base(dialogService, authorizationService)
    {
      _systemEventLogService = systemEventLogService;

      SyncItemsCommand = new DelegateCommand(SyncLogs);
      SystemEventLogs = new ObservableCollectionEx<SystemEventLogModel>();
      FromDate = DateTime.Now - _initialLogsTimespan;
      ToDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);
    }

    public async void OnFromDateChanged()
    {
      if (!ViewLoaded)
        return;

      await LoadSystemEventLogsAsync();
    }

    public async void OnToDateChanged()
    {
      if (!ViewLoaded)
        return;

      await LoadSystemEventLogsAsync();
    }

    protected override ObservableCollectionEx<SystemEventLogModel> GetItemsSource() => SystemEventLogs;

    public override async Task RefreshDataAsync()
    {
      await base.RefreshDataAsync();

      await LoadSystemEventLogsAsync();
    }

    private async Task LoadSystemEventLogsAsync()
    {
      var result = await _systemEventLogService.GetLogsAsync(FromDate, ToDate);
      if (!result.Succeeded)
        return;

      SystemEventLogs.Clear();
      SystemEventLogs.AddRange(result.Response);
    }

    private async void SyncLogs(object obj)
    {
      await LoadSystemEventLogsAsync();
    }
  }
}
