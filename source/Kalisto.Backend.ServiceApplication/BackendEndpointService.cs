﻿using System;
using Topshelf;

namespace Kalisto.Backend.ServiceApplication
{
  public class BackendEndpointService
  {
    public string AppName { get; }

    public string Description => "Kalisto server representing the main core of distributed weighing system.";

    private ServerEndpoint _serverEndpoint;

    public BackendEndpointService()
    {
      AppName = typeof(ServerEndpoint).Assembly.GetName().Name;
    }

    public bool Start(HostControl hostControl)
    {
      try
      {
        if(_serverEndpoint == null)
          _serverEndpoint = new ServerEndpoint("Kalisto", AppName);

        _serverEndpoint.Configure();
        _serverEndpoint.Initialize();
        return true;
      }
      catch
      {
        return false;
      }
    }

    public bool Stop(HostControl hostControl)
    {
      try
      {
        _serverEndpoint.Dispose();
        _serverEndpoint = null;
        return true;
      }
      catch
      {
        return true;
      }
    }

    public void HandleException(Exception exception)
    {
      _serverEndpoint.HandleException(exception);
    }
  }
}
