﻿using log4net.Config;
using Topshelf;

namespace Kalisto.Backend.ServiceApplication
{
  static class Program
  {
    /// <summary>
    /// The main entry point for the application.
    /// </summary>
    static void Main()
    {
      XmlConfigurator.Configure();
      HostFactory.Run(configure =>
      {
        var backendService = new BackendEndpointService();
        configure.Service<BackendEndpointService>(service =>
        {
          service.ConstructUsing(s => backendService);
          service.WhenStarted((s, c) => s.Start(c));
          service.WhenStopped((s, c) => s.Stop(c));
        });

        configure.RunAsLocalSystem();
        configure.SetServiceName(backendService.AppName);
        configure.SetDisplayName(backendService.AppName);
        configure.SetDescription(backendService.Description);
        configure.DependsOn("RabbitMQ");

        configure.StartAutomaticallyDelayed();
        configure.OnException(exc => backendService.HandleException(exc));
        configure.EnableServiceRecovery(rc =>
        {
          rc.RestartService(1);
        });
      });
    }
  }
}
