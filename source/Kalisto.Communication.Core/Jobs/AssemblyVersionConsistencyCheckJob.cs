﻿using Kalisto.Communication.Core.Jobs.Base;
using Kalisto.Communication.Core.Services;
using Kalisto.Localization;
using System;
using System.Threading.Tasks;

namespace Kalisto.Communication.Core.Jobs
{
  public class AssemblyVersionConsistencyCheckJob : IInitializationJob
  {
    private readonly ILocalizer<Localization.Messages.Messages> _jobLocalizer;

    private readonly IAssemblyVerifier _assemblyVerifier;

    public string LocalizedMessage =>
      _jobLocalizer.Localize(() => Localization.Messages.Messages.ClientAssembliesVersionCheckMessage);

    public string LocalizedExceptionMessage =>
      _jobLocalizer.Localize(() => Localization.Messages.Messages.ClientAssembliesVersionCheckExceptionMessage);

    public string FailureCode { get; private set; }

    public AssemblyVersionConsistencyCheckJob(ILocalizer<Localization.Messages.Messages> jobLocalizer,
      IAssemblyVerifier assemblyVerifier)
    {
      _jobLocalizer = jobLocalizer;
      _assemblyVerifier = assemblyVerifier;
    }

    public async Task ExecuteAsync()
    {
      await Task.Delay(1000);
      var isConsistent = _assemblyVerifier.CheckAssemblyConsistency();
      if (!isConsistent)
        throw new InvalidOperationException();

      await Task.FromResult(0);
    }
  }
}
