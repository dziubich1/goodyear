﻿using Kalisto.Communication.Core.Extensions;
using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Localization;
using System;
using System.Threading;
using System.Threading.Tasks;
using Kalisto.Communication.Core.Jobs.Base;

namespace Kalisto.Communication.Core.Jobs
{
  public class ClientRegistrationJob : IInitializationJob
  {
    private readonly TimeSpan _registrationTimeout = TimeSpan.FromSeconds(5);

    private readonly ILocalizer<Localization.Messages.Messages> _jobLocalizer;

    private readonly AutoResetEvent _resetEvent = new AutoResetEvent(false);

    private readonly CancellationToken _cancellationToken = new CancellationToken(false);

    private readonly IClientMessageProvider _messageProvider;

    private readonly ClientInfo _clientInfo;

    public string LocalizedMessage =>
      _jobLocalizer.Localize(() => Localization.Messages.Messages.ClientRegistrationMessage);

    public string LocalizedExceptionMessage =>
      _jobLocalizer.Localize(() => Localization.Messages.Messages.ClientRegistrationExceptionMessage);

    public string FailureCode { get; private set; }

    public ClientRegistrationJob(ILocalizer<Localization.Messages.Messages> jobLocalizer, IClientMessageProvider messageProvider,
      IRegisterClient clientRegistration, ClientInfo clientInfo)
    {
      _jobLocalizer = jobLocalizer;
      _messageProvider = messageProvider;
      clientRegistration.ClientRegistered += OnClientRegistered;
      clientRegistration.ClientRegistrationFailure += OnClientRegistrationFailed;
      _clientInfo = clientInfo;
    }

    public async Task ExecuteAsync()
    {
      await Task.Run(async () =>
      {
        await Task.Delay(1000, _cancellationToken);
        _messageProvider.SendRegistrationMessage(new PartnerRegistrationMessage
        {
          PartnerId = _clientInfo.Id,
          OwnerGuid = Guid.NewGuid().ToString(),
          Type = _clientInfo.Type
        });
      }, _cancellationToken);

      if (!await _resetEvent.WaitOneAsync((int)_registrationTimeout.TotalMilliseconds, _cancellationToken))
        throw new TimeoutException();

      // todo: log service message here later
    }

    private void OnClientRegistered()
    {
      _resetEvent.Set();
    }

    private void OnClientRegistrationFailed(string failureCode)
    {
      FailureCode = failureCode;
      throw new Exception();
    }
  }
}
