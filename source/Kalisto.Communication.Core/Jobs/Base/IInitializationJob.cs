﻿using System.Threading.Tasks;

namespace Kalisto.Communication.Core.Jobs.Base
{
  public interface IInitializationJob : IInitializationJobInfo
  {
    Task ExecuteAsync();
  }
}
