﻿namespace Kalisto.Communication.Core.Jobs.Base
{
  public interface IInitializationJobInfo
  {
    string LocalizedMessage { get; }

    string LocalizedExceptionMessage { get; }

    string FailureCode { get; }
  }
}
