﻿using System.Threading.Tasks;

namespace Kalisto.Communication.Core.Jobs.Base
{
  public interface IInitializationJobScheduler : IInitializationJobSchedulerInfo
  {
    void ScheduleJob(IInitializationJob job);

    Task RunAsync();
  }
}
