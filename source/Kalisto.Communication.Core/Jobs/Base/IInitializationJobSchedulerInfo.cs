﻿using System;

namespace Kalisto.Communication.Core.Jobs.Base
{
  public interface IInitializationJobSchedulerInfo
  {
    event Action<int, IInitializationJobInfo> InitializationProgressChanged;

    event Action<IInitializationJobInfo> InitializationJobExceptionOccurred;

    event Action<bool> InitializationCompleted;
  }
}
