﻿using Kalisto.Communication.Core.Jobs.Base;
using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Communication.Core.Services;
using Kalisto.Hmi.Core.Communication;
using Kalisto.Localization;
using System;
using System.Threading.Tasks;

namespace Kalisto.Communication.Core.Jobs
{
  public class AssemblyVersionCompatibilityCheckJob : IInitializationJob
  {
    private readonly ILocalizer<Localization.Messages.Messages> _jobLocalizer;

    private readonly IFrontendMessageBridge _messageBridge;

    private readonly IMessageBuilder _messageBuilder;

    private readonly ClientInfo _clientInfo;

    private readonly IAssemblyVerifier _assemblyVerifier;

    private string _serverAssembliesVersion;

    private string _clientAssembliesVersion;

    public string LocalizedMessage =>
      _jobLocalizer.Localize(() => Localization.Messages.Messages.AssembliesVersionCheckMessage);

    public string LocalizedExceptionMessage =>
      GetExceptionMessage();

    public string FailureCode { get; private set; }

    public AssemblyVersionCompatibilityCheckJob(ILocalizer<Localization.Messages.Messages> jobLocalizer,
      IFrontendMessageBridge messageBridge, IMessageBuilder messageBuilder, ClientInfo clientInfo,
      IAssemblyVerifier assemblyVerifier)
    {
      _jobLocalizer = jobLocalizer;
      _messageBridge = messageBridge;
      _messageBuilder = messageBuilder;
      _clientInfo = clientInfo;
      _assemblyVerifier = assemblyVerifier;
    }

    public async Task ExecuteAsync()
    {
      await Task.Delay(1000);
      _clientAssembliesVersion = _assemblyVerifier.GetAssembliesVersion();
      var request = _messageBuilder.CreateNew<SystemVersionCompatibilityCheckRequestMessage>()
        .BasedOn(_clientInfo)
        .WithProperty(c => c.ClientVersion, _clientAssembliesVersion)
        .Build();

      var result = await _messageBridge.SendMessageWithResultAsync<SystemVersionCompatibilityDto>(request);
      if (!result.Succeeded || result.Response == null)
        throw new InvalidOperationException();

      _serverAssembliesVersion = result.Response.ServerVersion;
      if (!result.Response.IsCompatible)
        throw new InvalidOperationException();
    }

    private string GetExceptionMessage()
    {
      if (string.IsNullOrEmpty(_serverAssembliesVersion))
        return _jobLocalizer.Localize(() => Localization.Messages.Messages.AssembliesVersionCheckExceptionMessage);

      return string.Format(_jobLocalizer.Localize(() => Localization.Messages.Messages.AssembliesVersionCheckExceptionMessageWithParams),
        _clientAssembliesVersion, _serverAssembliesVersion);
    }
  }
}
