﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Kalisto.Communication.Core.Jobs.Base;

namespace Kalisto.Communication.Core.Jobs
{
  public class InitializationJobScheduler : IInitializationJobScheduler
  {
    private readonly List<IInitializationJob> _initializationJobs = new List<IInitializationJob>();

    public event Action<int, IInitializationJobInfo> InitializationProgressChanged;

    public event Action<IInitializationJobInfo> InitializationJobExceptionOccurred;

    public event Action<bool> InitializationCompleted;

    public void ScheduleJob(IInitializationJob job)
    {
      if (!_initializationJobs.Contains(job))
        _initializationJobs.Add(job);
    }

    public async Task RunAsync()
    {
      var exceptionOccurred = false;
      foreach (var initializationJob in _initializationJobs)
      {
        var progressValue = GetProgressValueForJob(initializationJob);
        InitializationProgressChanged?.Invoke(progressValue, initializationJob);

        try
        {
          await initializationJob.ExecuteAsync();
        }
        catch (Exception)
        {
          exceptionOccurred = true;
          InitializationJobExceptionOccurred?.Invoke(initializationJob);
          break;
        }
      }

      InitializationCompleted?.Invoke(exceptionOccurred);
    }

    private int GetProgressValueForJob(IInitializationJob job)
    {
      var jobIndex = _initializationJobs.IndexOf(job);
      if (jobIndex < 0)
        throw new InvalidOperationException();

      var progress = ((float)(jobIndex + 1) / _initializationJobs.Count) * 100;
      if (Math.Abs(progress - 100) < 0.001)
        return (int)(progress) - 1;
      return (int)progress;
    }
  }
}
