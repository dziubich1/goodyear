﻿using Kalisto.Communication.Core.Communication;
using Kalisto.Communication.Core.Messages;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Kalisto.Communication.Core.Registries
{
  public abstract class MessageProviderRegistry : IMessageProviderRegistry
  {
    private readonly List<ProviderEntry> _providers = new List<ProviderEntry>();

    public abstract void Initialize();

    public void RegisterProvider(IMessageSender messageSender)
    {
      RegisterProvider(Guid.NewGuid().ToString(), messageSender, null);
    }

    public void RegisterProvider(string partnerId, IMessageSender messageSender, ClientType? type)
    {
      if (_providers.Any(c => c.MessageSender.QueueName == messageSender.QueueName))
        return;

      _providers.Add(new ProviderEntry
      {
        PartnerId = partnerId,
        MessageSender = messageSender,
        Type = type
      });
    }

    public void UnregisterProvider(string partnerId)
    {
      var provider = _providers.FirstOrDefault(c => c.PartnerId == partnerId);
      if (provider == null)
        return;

      provider.Dispose();
      _providers.Remove(provider);
    }

    public IMessageSender GetProviderByQueueName(string queueName)
    {
      return _providers.FirstOrDefault(c => c.MessageSender.QueueName == queueName)?.MessageSender;
    }

    public IEnumerable<IMessageSender> GetAllProviders()
    {
      return _providers.Select(c => c.MessageSender).ToArray();
    }

    public IEnumerable<IMessageSender> GetAllBroadcastingMessageProviders()
    {
      return _providers.Where(c => c.Type == ClientType.Frontend).Select(c => c.MessageSender).ToArray();
    }

    public IEnumerable<IMessageSender> GetAllNotificationMessageProviders(string partnerId)
    {
      return _providers.Where(c => c.Type == ClientType.Frontend && c.PartnerId != partnerId)
        .Select(c => c.MessageSender).ToArray();
    }

    public IEnumerable<IMessageSender> GetAllDeviceMessageProviders()
    {
      return _providers.Where(c => c.Type == ClientType.Device).Select(c => c.MessageSender).ToArray();
    }

    private class ProviderEntry : IDisposable
    {
      public string PartnerId { get; set; }

      public IMessageSender MessageSender { get; set; }

      public ClientType? Type { get; set; }

      public void Dispose()
      {
        MessageSender?.Dispose();
      }
    }
  }
}
