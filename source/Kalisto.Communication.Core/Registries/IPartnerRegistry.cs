﻿using Kalisto.Backend.DataAccess.Model;
using System;
using Kalisto.Communication.Core.Messages;

namespace Kalisto.Communication.Core.Registries
{
  public interface IPartnerRegistry
  {
    void RegisterPartner(string partnerId, ClientType type);

    void UnregisterInactivePartners();

    bool UpdatePartnerHeartbeat(string partnerId);

    void LoginUser(string partnerId, User user);

    void LogoutUser(string partnerId);
  }

  public interface IPartnerRegistryReader
  {
    int RegisteredLicensingPartnersCount { get; }

    bool IsPartnerActive(string partnerId);

    event Action<string, ClientType> PartnerRegistryEntryCreated;

    event Action<string> PartnerRegistryEntryRemoved;

    User GetCurrentUser(string partnerId);

    bool IsPartnerTypeAlreadyRegistered(ClientType clientType);
  }
}
