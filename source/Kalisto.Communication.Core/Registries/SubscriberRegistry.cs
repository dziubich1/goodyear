﻿using System;
using System.Collections.Generic;
using System.Linq;
using Kalisto.Communication.Core.Communication;
using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;

namespace Kalisto.Communication.Core.Registries
{
  public abstract class SubscriberRegistry : ISubscriberRegistry
  {
    private readonly List<SubscriberEntry> _subscribers = new List<SubscriberEntry>();

    public abstract void Initialize();

    public void RegisterSubscriber(IMessageReceiver messageReceiver, string messageId, IMessageHandler messageHandler)
    {
      RegisterSubscriber(Guid.NewGuid().ToString(), messageReceiver, messageId, messageHandler);
    }

    public void RegisterSubscriber(string partnerId, IMessageReceiver messageReceiver, string messageId, IMessageHandler messageHandler)
    {
      _subscribers.Add(new SubscriberEntry
      {
        PartnerId = partnerId,
        MessageHandler = messageHandler,
        MessageId = messageId,
        QueueName = messageReceiver.QueueName,
        MessageReceiver = messageReceiver
      });

      if (_subscribers.Count(c => c.QueueName == messageReceiver.QueueName) > 1)
        return; // we do not want to attach to OnMessageReceived event multiple times when the same receiver detected

      messageReceiver.OnMessageReceived += OnMessageReceived;
    }

    public void UnregisterSubscribers(string partnerId)
    {
      var subscribers = _subscribers.Where(c => c.PartnerId == partnerId).ToList();
      if (!subscribers.Any())
        return;

      foreach(var subscriber in subscribers)
        subscriber.Dispose();

      _subscribers.RemoveAll(c => c.PartnerId == partnerId);
    }

    private void OnMessageReceived(string queueName, string message)
    {
      var subscribers = _subscribers.Where(c => c.QueueName == queueName).ToList();
      if (!subscribers.Any())
        return;

      var messageBase = MessageSerializer.Deserialize<EndpointMessageBase>(message);
      if (messageBase == null)
        return;

      subscribers = subscribers.Where(c => c.MessageId == messageBase.Id).ToList();
      foreach(var subscriber in subscribers)
        subscriber.MessageHandler.HandleMessage(messageBase);
    }

    private class SubscriberEntry : IDisposable
    {
      public string PartnerId { get; set; }

      public string QueueName { get; set; }

      public string MessageId { get; set; }

      public IMessageHandler MessageHandler { get; set; }

      public IMessageReceiver MessageReceiver { get; set; }

      public void Dispose()
      {
        MessageReceiver?.Dispose();
      }
    }
  }
}
