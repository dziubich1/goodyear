﻿namespace Kalisto.Communication.Core.Registries
{
  public interface ISubscriberRegistry
  {
    void Initialize();
  }
}
