﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Communication.Core.Messages;
using Kalisto.Logging;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Kalisto.Communication.Core.Registries
{
  public class PartnerRegistry : IPartnerRegistry, IPartnerRegistryReader
  {
    private readonly TimeSpan _defaultHeartbeatValue = TimeSpan.FromSeconds(15);

    private readonly ILogger _logger;

    private readonly List<PartnerRegistryEntry> _partners = new List<PartnerRegistryEntry>();

    public PartnerRegistry(ILogger logger)
    {
      _logger = logger;
    }

    public event Action<string, ClientType> PartnerRegistryEntryCreated;

    public event Action<string> PartnerRegistryEntryRemoved;

    public int RegisteredLicensingPartnersCount => _partners.Count(c => c.Type == ClientType.Frontend);

    public User GetCurrentUser(string partnerId)
    {
      var partner = _partners.FirstOrDefault(c => c.Id == partnerId);
      if (partner == null)
        return null;

      return partner.CurrentUser;
    }

    public bool IsPartnerActive(string partnerId)
    {
      var partner = _partners.FirstOrDefault(c => c.Id == partnerId);
      if (partner == null)
        return false;

      if (partner.HeartbeatDate + _defaultHeartbeatValue >= DateTime.Now)
        return true;

      return false;
    }

    public void LoginUser(string partnerId, User user)
    {
      var partner = GetPartnerRegistryEntry(partnerId);

      partner.CurrentUser = user;
    }

    public void LogoutUser(string partnerId)
    {
      var partner = GetPartnerRegistryEntry(partnerId);

      partner.CurrentUser = null;
    }

    public void RegisterPartner(string partnerId, ClientType type)
    {
      if (_partners.Any(c => c.Id == partnerId))
        return;

      _partners.Add(new PartnerRegistryEntry
      {
        Id = partnerId,
        RegisterDate = DateTime.Now,
        HeartbeatDate = DateTime.Now,
        Type = type
      });

      PartnerRegistryEntryCreated?.Invoke(partnerId, type);
      _logger.Info($"New communication partner with ID:{partnerId} has been registered successfully.");
    }

    public void UnregisterInactivePartners()
    {
      var inactivePartnersIds =
        _partners.Where(c => c.HeartbeatDate + _defaultHeartbeatValue < DateTime.Now).Select(c => c.Id).ToList();
      if (!inactivePartnersIds.Any())
        return;

      _partners.RemoveAll(c => inactivePartnersIds.Contains(c.Id));
      foreach (var partnerId in inactivePartnersIds)
        PartnerRegistryEntryRemoved?.Invoke(partnerId);
    }

    public bool UpdatePartnerHeartbeat(string partnerId)
    {
      var partner = _partners.FirstOrDefault(c => c.Id == partnerId);
      if (partner == null)
        return false;

      partner.HeartbeatDate = DateTime.Now;
      return true;
    }

    public bool IsPartnerTypeAlreadyRegistered(ClientType clientType)
    {
      return _partners.Any(c => c.Type == clientType);
    }

    private class PartnerRegistryEntry
    {
      public string Id { get; set; }

      public ClientType Type { get; set; }

      public DateTime RegisterDate { get; set; }

      public DateTime HeartbeatDate { get; set; }

      public User CurrentUser { get; set; }
    }

    private PartnerRegistryEntry GetPartnerRegistryEntry(string partnerId)
    {
      var partner = _partners.FirstOrDefault(c => c.Id == partnerId);
      if (partner == null)
        throw new ArgumentException(@"Partner with specified id not found", nameof(partnerId));

      return partner;
    }
  }
}
