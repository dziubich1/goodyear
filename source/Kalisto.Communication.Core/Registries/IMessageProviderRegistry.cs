﻿using System.Collections.Generic;
using Kalisto.Communication.Core.Communication;
using Kalisto.Communication.Core.Messages;

namespace Kalisto.Communication.Core.Registries
{
  public interface IMessageProviderRegistry
  {
    void Initialize();

    void RegisterProvider(IMessageSender sender);

    void RegisterProvider(string partnerId, IMessageSender sender, ClientType? type);

    void UnregisterProvider(string partnerId);

    IMessageSender GetProviderByQueueName(string queueName);

    IEnumerable<IMessageSender> GetAllProviders();

    IEnumerable<IMessageSender> GetAllBroadcastingMessageProviders();

    IEnumerable<IMessageSender> GetAllNotificationMessageProviders(string partnerId);

    IEnumerable<IMessageSender> GetAllDeviceMessageProviders();
  }
}
