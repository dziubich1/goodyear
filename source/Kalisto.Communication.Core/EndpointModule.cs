﻿using Kalisto.Communication.Core.Registries;
using Kalisto.Communication.Core.Services;
using Kalisto.Configuration;
using Kalisto.Logging;
using Quartz;
using Quartz.Unity;
using System;
using System.Threading;
using System.Threading.Tasks;
using Unity;
using Unity.Lifetime;

namespace Kalisto.Communication.Core
{
  public abstract class EndpointModule<TConfig> : IDisposable where TConfig : ConfigurationBase, new()
  {
    private readonly IUnityContainer _dependencyContainer;

    protected ConfigurationManager _configurationManager;

    private ISubscriberRegistry _subscriberStrategy;

    private IMessageProviderRegistry _providerRegistry;

    protected ILogger Logger { get; private set; }

    protected TConfig Configuration { get; set; }

    protected EndpointModule(string domainName, string appName)
    {
      _dependencyContainer = new UnityContainer();
      _configurationManager = new ProgramDataConfigurationManager(domainName, appName);
    }

    protected EndpointModule(IUnityContainer container, string domainName, string appName)
    {
      _dependencyContainer = container;
      _configurationManager = new ProgramDataConfigurationManager(domainName, appName);
    }

    public void Configure()
    {
      LoadConfiguration();
      Bind(_dependencyContainer);
      Resolve(_dependencyContainer);
      OnConfigured();
    }

    public void Initialize()
    {
      OnInitialized();
    }

    protected virtual void OnInitialized()
    {
    }

    protected virtual void OnConfigured()
    {
    }

    protected virtual void OnConfigurationLoaded()
    {

    }

    protected virtual void Bind(IUnityContainer container)
    {
      container.RegisterType<ILogger, Logger>(new ContainerControlledLifetimeManager());
      container.RegisterType<IAssemblyVerifier, AssemblyVersionVerifyingService>(new ContainerControlledLifetimeManager());
      container.RegisterInstance<ConfigurationManager>(_configurationManager);
      container.AddNewExtension<QuartzUnityExtension>();
    }

    protected virtual async void Resolve(IUnityContainer container)
    {
      Logger = container.Resolve<ILogger>();

      var schedulerFactory = container.Resolve<ISchedulerFactory>();
      var scheduler = await schedulerFactory.GetScheduler(CancellationToken.None);
      await ScheduleJobs(scheduler);
      await scheduler.Start();

      _subscriberStrategy = container.Resolve<ISubscriberRegistry>();
      _subscriberStrategy.Initialize();

      _providerRegistry = container.Resolve<IMessageProviderRegistry>();
      _providerRegistry.Initialize();
    }

    protected virtual async Task ScheduleJobs(IScheduler scheduler)
    {

    }

    protected virtual void OnDisconnect()
    {

    }

    private void LoadConfiguration()
    {
      Configuration = _configurationManager.Load<TConfig>();
      OnConfigurationLoaded();
    }

    public void HandleException(Exception exception)
    {
      Logger.Fatal($"Fatal exception occurred: {exception.Message}. StackTrace: {exception.StackTrace}. Source: {exception.Source}.");
      if (exception.InnerException != null)
        HandleException(exception.InnerException);
    }

    public void Dispose()
    {
      OnDisconnect();
    }
  }
}
