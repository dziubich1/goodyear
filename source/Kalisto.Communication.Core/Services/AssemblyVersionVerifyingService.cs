﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Kalisto.Communication.Core.Services
{
  public interface IAssemblyVerifier
  {
    bool CheckAssemblyConsistency();

    string GetAssembliesVersion();
  }

  public class AssemblyVersionVerifyingService : IAssemblyVerifier
  {
    public bool CheckAssemblyConsistency()
    {
      var allAssemblies = new List<Assembly>();
      var path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
      if (string.IsNullOrEmpty(path))
        return false;

      foreach (var dll in Directory.GetFiles(path, "Kalisto.*.dll"))
        allAssemblies.Add(Assembly.LoadFile(dll));

      return allAssemblies.Select(c => c.GetName().Version.ToString())
               .Distinct().Count() == 1;
    }

    public string GetAssembliesVersion()
    {
      if(!CheckAssemblyConsistency())
        throw new InvalidOperationException();

      var path = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
      if (string.IsNullOrEmpty(path))
        return "UNKNOWN";

      var dll = Directory.GetFiles(path, "Kalisto.*.dll").First();
      return Assembly.LoadFile(dll).GetName().Version.ToString();
    }
  }
}
