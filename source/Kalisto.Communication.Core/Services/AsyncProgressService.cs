﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kalisto.Hmi.Core.Services
{
  public interface IAsyncProgressService
  {
    event Action AsyncOperationStarted;
    event Action AsyncOperationFinished;

    void StartAsyncOperation();
    void FinishAsyncOperation();
  }

  public class AsyncProgressService : IAsyncProgressService
  {
    public event Action AsyncOperationStarted;
    public event Action AsyncOperationFinished;

    public void FinishAsyncOperation()
    {
      AsyncOperationFinished?.Invoke();
    }

    public void StartAsyncOperation()
    {
      AsyncOperationStarted?.Invoke();
    }
  }
}
