﻿namespace Kalisto.Communication.Core.Logging
{
  public interface ISystemEventLogger
  {
    void LogEvent(string resourceKey, string[] parameters = null);
    void LogEvent(string partnerId, string resourceKey, string[] parameters = null);
  }
}
