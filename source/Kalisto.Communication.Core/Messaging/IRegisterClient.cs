﻿using System;

namespace Kalisto.Communication.Core.Messaging
{
  public interface IRegisterClient
  {
    event Action ClientRegistered;

    event Action<string> ClientRegistrationFailure;
  }
}
