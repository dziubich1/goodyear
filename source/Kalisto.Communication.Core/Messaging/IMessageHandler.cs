﻿namespace Kalisto.Communication.Core.Messaging
{
  public interface IMessageHandler
  {
    void HandleMessage(object message);
  }
}
