﻿using Kalisto.Communication.Core.Messages;

namespace Kalisto.Communication.Core.Messaging
{
  public abstract class MessageHandlerBase<TMessage> : IMessageHandler where TMessage : EndpointMessageBase
  {
    protected IMessageBuilder MessageBuilder { get; }

    protected MessageHandlerBase()
    {
    }

    protected MessageHandlerBase(IMessageBuilder messageBuilder)
    {
      MessageBuilder = messageBuilder;
    }

    public void HandleMessage(object message)
    {
      var endpointMessage = message as TMessage;
      if (endpointMessage == null)
        return;

      HandleMessage(endpointMessage);
    }

    protected abstract void HandleMessage(TMessage message);
  }
}
