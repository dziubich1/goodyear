﻿using Kalisto.Communication.Core.Messages;

namespace Kalisto.Communication.Core.Messaging
{
  public interface IClientMessageProvider : IMessageProvider
  {
    void SendRegistrationMessage(PartnerRegistrationMessage message);

    void SendHeartbeatMessage(HeartbeatMessage message);
  }
}
