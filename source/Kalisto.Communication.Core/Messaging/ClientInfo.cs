﻿using System;
using Kalisto.Communication.Core.Messages;

namespace Kalisto.Communication.Core.Messaging
{
  public class ClientInfo
  {
    public string Id { get; }

    public string QueueName => GetQueueName(Id);

    public string FeedbackQueueName => GetFeedbackQueueName(Id);

    public ClientType Type { get; set; }

    public ClientInfo(ClientType type)
    {
      Id = Guid.NewGuid().ToString();
      Type = type;
    }

    public static string GetQueueName(string clientId)
    {
      return $"{clientId}_QUEUE";
    }

    public static string GetFeedbackQueueName(string clientId)
    {
      return $"{GetQueueName(clientId)}_FEEDBACK";
    }

    public static string GetRegistrationQueueName()
    {
      return "REGISTRATION_QUEUE";
    }

    public static string GetHeartbeatQueueName()
    {
      return "HEARTBEAT_QUEUE";
    }
  }
}
