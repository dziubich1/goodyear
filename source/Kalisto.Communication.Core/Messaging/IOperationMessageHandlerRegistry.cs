﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Communication.Core.Messages;

namespace Kalisto.Communication.Core.Messaging
{
  public interface IOperationMessageHandlerRegistry
  {
    void RegisterOperationHandler<TOperation>(IOperationMessageHandler<TOperation> operationHandler)
      where TOperation : IBackendOperation;

    object ExecuteOperation(IDbContext dbContext, IBackendOperation operation, string partnerId);
  }
}
