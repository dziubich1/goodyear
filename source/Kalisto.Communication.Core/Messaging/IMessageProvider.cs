﻿using Kalisto.Communication.Core.Messages;

namespace Kalisto.Communication.Core.Messaging
{
  public interface IMessageProvider
  {
    void SendMessage<T>(T message) where T : EndpointMessageBase;
  }
}
