﻿using Kalisto.Communication.Core.Messages;
using Kalisto.Logging;
using System;

namespace Kalisto.Communication.Core.Messaging
{
  public class RegistrationResponseMessageHandler
    : MessageHandlerBase<PartnerRegistrationResponseMessage>, IRegisterClient
  {
    private readonly ILogger _logger;

    public event Action ClientRegistered;

    public event Action<string> ClientRegistrationFailure;

    public RegistrationResponseMessageHandler(ILogger logger)
    {
      _logger = logger;
    }

    protected override void HandleMessage(PartnerRegistrationResponseMessage message)
    {
      if (message.Succeeded)
      {
        _logger.Info("Client was registered successfully.");
        ClientRegistered?.Invoke();
      }
      else
      {
        ClientRegistrationFailure?.Invoke(message.FailureReasonCode);
        _logger.Error("Could not register client.");
      }
    }
  }
}
