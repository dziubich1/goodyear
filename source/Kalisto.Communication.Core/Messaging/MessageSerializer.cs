﻿using Kalisto.Communication.Core.Messages;
using Newtonsoft.Json;

namespace Kalisto.Communication.Core.Messaging
{
  public class MessageSerializer
  {
    public static string Serialize(object message)
    {
      return JsonConvert.SerializeObject(message, new JsonSerializerSettings
      {
        TypeNameHandling = TypeNameHandling.All,
        ReferenceLoopHandling = ReferenceLoopHandling.Ignore
      });
    }

    public static T Deserialize<T>(string message) where T : EndpointMessageBase
    {
      return JsonConvert.DeserializeObject<T>(message, new JsonSerializerSettings
      {
        TypeNameHandling = TypeNameHandling.All,
        ReferenceLoopHandling = ReferenceLoopHandling.Ignore
      });
    }
  }
}
