﻿using Kalisto.Communication.Core.Communication;
using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Registries;

namespace Kalisto.Communication.Core.Messaging
{
  public class ClientMessageProvider : IClientMessageProvider
  {
    private readonly IMessageProviderRegistry _messageProviderRegistry;

    public ClientMessageProvider(IMessageProviderRegistry providerRegistry)
    {
      _messageProviderRegistry = providerRegistry;
    }

    public void SendMessage<T>(T message) where T : EndpointMessageBase
    {
      // client endpoint always uses regular queue for regular messages
      var queueName = ClientInfo.GetQueueName(message.PartnerId);
      var provider = _messageProviderRegistry.GetProviderByQueueName(queueName);
      if (provider == null)
        return;

      SendMessage(provider, message);
    }

    public void SendRegistrationMessage(PartnerRegistrationMessage message)
    {
      var queueName = ClientInfo.GetRegistrationQueueName();
      var provider = _messageProviderRegistry.GetProviderByQueueName(queueName);
      if (provider == null)
        return;

      SendMessage(provider, message);
    }

    public void SendHeartbeatMessage(HeartbeatMessage message)
    {
      var queueName = ClientInfo.GetHeartbeatQueueName();
      var provider = _messageProviderRegistry.GetProviderByQueueName(queueName);
      if (provider == null)
        return;

      SendMessage(provider, message);
    }

    private void SendMessage<T>(IMessageSender sender, T message)
    {
      var jsonMessage = MessageSerializer.Serialize(message);
      sender.SendMessage(jsonMessage);
    }
  }
}
