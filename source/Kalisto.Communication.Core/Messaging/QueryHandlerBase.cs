﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Communication.Core.Messages;

namespace Kalisto.Communication.Core.Messaging
{
  public abstract class QueryHandlerBase<TQuery> : IOperationMessageHandler<TQuery> where TQuery : class, IBackendOperation
  {
    public abstract object Execute(IDbContext context, TQuery query, string partnerId);

    public object Execute(IDbContext context, object query, string partnerId)
    {
      var qry = query as TQuery;
      if (qry == null)
        return null;

      return Execute(context, qry, partnerId);
    }
  }
}
