﻿using System;
using System.Collections.Generic;
using Kalisto.Backend.DataAccess.Model;
using Kalisto.Communication.Core.Messages;

namespace Kalisto.Communication.Core.Messaging
{
  public class OperationMessageHandlerRegistry : IOperationMessageHandlerRegistry
  {
    private readonly Dictionary<Type, IOperationMessageHandler> _operationHandlers
      = new Dictionary<Type, IOperationMessageHandler>();

    public void RegisterOperationHandler<TOperation>(IOperationMessageHandler<TOperation> operationHandler)
      where TOperation : IBackendOperation
    {
      if (_operationHandlers.ContainsKey(typeof(TOperation)))
        return;

      _operationHandlers.Add(typeof(TOperation), operationHandler);
    }

    public object ExecuteOperation(IDbContext dbContext, IBackendOperation operation, string partnerId)
    {
      if (!_operationHandlers.ContainsKey(operation.GetType()))
        return null;

      return _operationHandlers[operation.GetType()].Execute(dbContext, operation, partnerId);
    }
  }
}
