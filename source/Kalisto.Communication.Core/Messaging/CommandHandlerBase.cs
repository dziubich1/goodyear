﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Communication.Core.Messages;

namespace Kalisto.Communication.Core.Messaging
{
  public abstract class CommandHandlerBase<TCommand> 
    : IOperationMessageHandler<TCommand> where TCommand : class, IBackendOperation
  {
    public abstract object Execute(IDbContext context, TCommand command, 
      string partnerId);

    public object Execute(IDbContext context, object command, 
      string partnerId)
    {
      var cmd = command as TCommand;
      if (cmd == null)
        return null;

      return Execute(context, cmd, partnerId);
    }
  }
}
