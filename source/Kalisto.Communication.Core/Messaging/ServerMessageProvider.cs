﻿using Kalisto.Communication.Core.Communication;
using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Registries;
using Kalisto.Frontend.Notifications.Base;
using System;

namespace Kalisto.Communication.Core.Messaging
{
  public class ServerMessageProvider : IServerMessageProvider
  {
    private readonly IMessageProviderRegistry _messageProviderRegistry;

    public ServerMessageProvider(IMessageProviderRegistry providerRegistry)
    {
      _messageProviderRegistry = providerRegistry;
    }

    public void BroadcastMessage<T>(T message) where T : EndpointMessageBase
    {
      var providers = _messageProviderRegistry.GetAllBroadcastingMessageProviders();
      foreach (var provider in providers)
        SendMessage(provider, message);
    }

    public void BroadcastNotification<T>(T notification) where T : NotificationBase
    {
      notification.Broadcasting = true;
      var providers = _messageProviderRegistry.GetAllBroadcastingMessageProviders();
      foreach (var provider in providers)
        SendMessage(provider, notification);
    }

    public void BroadcastDeviceMessage<T>(T message) where T : EndpointMessageBase
    {
      var providers = _messageProviderRegistry.GetAllDeviceMessageProviders();
      foreach (var provider in providers)
        SendMessage(provider, message);
    }

    public void SendMessage<T>(T message) where T : EndpointMessageBase
    {
      // server endpoint always uses feedback queue
      var queueName = ClientInfo.GetFeedbackQueueName(message.PartnerId);
      var provider = _messageProviderRegistry.GetProviderByQueueName(queueName);
      if (provider == null)
        return;

      SendMessage(provider, message);
    }

    private void SendMessage<T>(IMessageSender sender, T message)
    {
      var jsonMessage = MessageSerializer.Serialize(message);
      sender.SendMessage(jsonMessage);
    }
  }
}
