﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Communication.Core.Messages;

namespace Kalisto.Communication.Core.Messaging
{
  public interface IOperationMessageHandler
  {
    object Execute(IDbContext context, object operation, string partnerId);
  }

  public interface IOperationMessageHandler<in TOperation> 
    : IOperationMessageHandler where TOperation : IBackendOperation
  {
    object Execute(IDbContext context, TOperation operation, string partnerId);
  }
}
