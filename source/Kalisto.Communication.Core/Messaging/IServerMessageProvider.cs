﻿using Kalisto.Communication.Core.Messages;
using Kalisto.Frontend.Notifications.Base;

namespace Kalisto.Communication.Core.Messaging
{
  public interface IServerMessageProvider : IMessageProvider
  {
    void BroadcastMessage<T>(T message) where T : EndpointMessageBase;

    void BroadcastNotification<T>(T notification) where T : NotificationBase;

    void BroadcastDeviceMessage<T>(T message) where T : EndpointMessageBase;
  }
}
