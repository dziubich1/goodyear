﻿using System;
using System.Collections.Generic;
using Kalisto.Configuration.Permissions;

namespace Kalisto.Communication.Core.Messaging
{
  [Serializable]
  public class UserInfo
  {
    public int? UserRole { get; set; }

    public string Username { get; set; }

    public List<PermissionRule> Permissions { get; set; }

    public void Update(int? userRole, string username, List<PermissionRule> permissions)
    {
      UserRole = userRole;
      Username = username;
      Permissions = permissions;
    }

    public void Clear()
    {
      UserRole = null;
      Username = string.Empty;
      Permissions.Clear();
    }
  }
}
