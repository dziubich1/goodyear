﻿using System;
using System.Runtime.Serialization;

namespace Kalisto.Communication.Core.Exceptions
{
  [Serializable]
  public class DbException : ExceptionBase
  {
    public DbException()
    {
    }

    // REMARKS: below constructor need to be added due to the serialization/deserialization issues
    public DbException(SerializationInfo info, StreamingContext context)
    {
    }

    public override string ExceptionCode => "DB_1001";

    public override string Message => GetLocalizedMessage();
  }
}
