﻿namespace Kalisto.Communication.Core.Exceptions
{
  public class CommunicationException : ExceptionBase
  {
    public override string ExceptionCode => "COMMUNICATION_1001";

    public override string Message => GetLocalizedMessage();
  }
}
