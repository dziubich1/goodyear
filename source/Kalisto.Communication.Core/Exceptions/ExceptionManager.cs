﻿using Kalisto.Localization;
using System;

namespace Kalisto.Communication.Core.Exceptions
{
  [Serializable]
  public class ExceptionManager
  {
    private readonly ILocalizer<Localization.Exceptions.Exceptions> _exceptionLocalizer;

    public ExceptionManager()
    {
      _exceptionLocalizer = new LocalizationManager<Localization.Exceptions.Exceptions>();
    }

    public string GetExceptionMessage(string exceptionInternalCode)
    {
      return _exceptionLocalizer.Localize(exceptionInternalCode);
    }
  }
}
