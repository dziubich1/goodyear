﻿using System;

namespace Kalisto.Communication.Core.Exceptions
{
  [Serializable]
  public abstract class ExceptionBase : Exception
  {
    private readonly ExceptionManager _exceptionManager;

    public abstract string ExceptionCode { get; }

    protected ExceptionBase()
    {
      _exceptionManager = new ExceptionManager();
    }

    protected string GetLocalizedMessage()
    {
      return _exceptionManager.GetExceptionMessage(ExceptionCode);
    }
  }
}
