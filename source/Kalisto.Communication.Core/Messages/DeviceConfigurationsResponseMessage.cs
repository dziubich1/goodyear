﻿using System;

namespace Kalisto.Communication.Core.Messages
{
  [Serializable]
  public class DeviceConfigurationsResponseMessage : EndpointMessageBase, IResponse
  {
    public const string Identifier = nameof(DeviceConfigurationsResponseMessage);

    public sealed override string Id => Identifier;

    public object Result { get; set; }

    public Exception Exception { get; set; }

    public bool HasError => Exception != null;
  }
}
