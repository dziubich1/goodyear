﻿using System;
using Kalisto.Mailing.Configuration;

namespace Kalisto.Communication.Core.Messages
{
  [Serializable]
  public class SmtpSettingsMessage : EndpointMessageBase
  {
    public const string Identifier = nameof(SmtpSettingsMessage);

    public override string Id => Identifier;

    public SmtpSettings SmtpSettings { get; set; }
  }
}
