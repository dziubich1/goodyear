﻿using System;

namespace Kalisto.Communication.Core.Messages
{
  [Serializable]
  public class ServerImageThumbnailResponse : EndpointMessageBase, IResponse
  {
    public const string Identifier = nameof(ServerImageThumbnailRequest);

    public sealed override string Id => Identifier;

    public object Result { get; set; }

    public Exception Exception { get; set; }

    public bool HasError => Exception != null;
  }
}
