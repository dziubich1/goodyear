﻿using System;

namespace Kalisto.Communication.Core.Messages
{
  public class BackendDbOperationResponseMessage : EndpointMessageBase, IResponse
  {
    public const string Identifier = nameof(BackendDbOperationResponseMessage);

    public override string Id => Identifier;

    public object Result { get; set; }

    public Exception Exception { get; set; }

    public bool HasError => Exception != null;
  }
}
