﻿using System;
using System.Linq.Expressions;
using System.Reflection;
using Kalisto.Communication.Core.Messaging;

namespace Kalisto.Communication.Core.Messages
{
  public interface IMessageBuilder
  {
    INewMessageSyntax<T> CreateNew<T>() where T : EndpointMessageBase, new();
  }

  public interface INewMessageSyntax<TMessage> where TMessage : EndpointMessageBase
  {
    IMessagePropertySyntax<TMessage> BasedOn(EndpointMessageBase baseMessage);

    IMessagePropertySyntax<TMessage> BasedOn(ClientInfo clientInfo);

    IMessagePropertySyntax<TMessage> Broadcasting();
  }

  public interface IMessagePropertySyntax<TMessage> : IMessageResult<TMessage> where TMessage : EndpointMessageBase
  {
    IMessagePropertySyntax<TMessage> WithProperty<TValue>(Expression<Func<TMessage, TValue>> propertyLambda, TValue value);
  }

  public interface IMessageResult<out TMessage>
  {
    TMessage Build();
  }

  public class MessageBuilder : IMessageBuilder
  {
    public INewMessageSyntax<T> CreateNew<T>() where T : EndpointMessageBase, new()
    {
      return new MessageSyntax<T>(new T());
    }

    private class MessageSyntax<TMessage> : INewMessageSyntax<TMessage> where TMessage : EndpointMessageBase
    {
      private readonly TMessage _message;

      public MessageSyntax(TMessage message)
      {
        _message = message;
      }

      public IMessagePropertySyntax<TMessage> BasedOn(EndpointMessageBase baseMessage)
      {
        _message.PartnerId = baseMessage.PartnerId;
        _message.OwnerGuid = baseMessage.OwnerGuid;
        _message.MessageId = baseMessage.MessageId;
        _message.Broadcasting = false;

        return new PropertySyntax(_message);
      }

      public IMessagePropertySyntax<TMessage> BasedOn(ClientInfo clientInfo)
      {
        _message.PartnerId = clientInfo.Id;
        _message.Broadcasting = false;

        return new PropertySyntax(_message);
      }

      public IMessagePropertySyntax<TMessage> Broadcasting()
      {
        _message.Broadcasting = true;

        return new PropertySyntax(_message);
      }

      private class PropertySyntax : IMessagePropertySyntax<TMessage>
      {
        private readonly TMessage _message;

        public PropertySyntax(TMessage message)
        {
          _message = message;
        }

        public IMessagePropertySyntax<TMessage> WithProperty<TValue>(Expression<Func<TMessage, TValue>> propertyLambda, TValue value)
        {
          if (!(propertyLambda.Body is MemberExpression memberSelectorExpression))
            return new PropertySyntax(_message);

          var property = memberSelectorExpression.Member as PropertyInfo;
          if (property != null)
            property.SetValue(_message, value, null);

          return new PropertySyntax(_message);
        }

        public TMessage Build()
        {
          if (string.IsNullOrEmpty(_message.PartnerId) && !_message.Broadcasting)
            throw new InvalidOperationException("Given message does not have a client ID specified.");

          return _message;
        }
      }
    }
  }
}
