﻿using System;

namespace Kalisto.Communication.Core.Messages
{
  [Serializable]
  public class LoginRequiredMessage : EndpointMessageBase
  {
    public const string Identifier = nameof(LoginRequiredMessage);

    public override string Id => Identifier;
  }
}
