﻿using System;

namespace Kalisto.Communication.Core.Messages
{
    [Serializable]
    public class ClientLogoRequestMessage : EndpointMessageBase
    {
        public const string Identifier = nameof(ClientLogoRequestMessage);

        public sealed override string Id => Identifier;

        public string LogoType;
    }
}
