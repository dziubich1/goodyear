﻿namespace Kalisto.Communication.Core.Messages
{
  public class BackendDbOperationMessage : EndpointMessageBase, IBackendOperation
  {
    public const string Identifier = nameof(BackendDbOperationMessage);

    public sealed override string Id => Identifier;
  }
}
