﻿using System;

namespace Kalisto.Communication.Core.Messages
{
  [Serializable]
  public class EndpointMessageBase
  {
    public virtual string Id { get; }

    public string PartnerId { get; set; }

    public string OwnerGuid { get; set; }

    public string MessageId { get; set; }

    public bool Broadcasting { get; set; }
  }
}
