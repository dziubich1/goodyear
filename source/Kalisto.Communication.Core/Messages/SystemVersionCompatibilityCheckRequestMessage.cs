﻿using System;

namespace Kalisto.Communication.Core.Messages
{
  [Serializable]
  public class SystemVersionCompatibilityCheckRequestMessage : EndpointMessageBase
  {
    public const string Identifier = nameof(SystemVersionCompatibilityCheckRequestMessage);

    public sealed override string Id => Identifier;

    public string ClientVersion { get; set; }
  }

  [Serializable]
  public class SystemVersionCompatibilityCheckResponseMessage : EndpointMessageBase, IResponse
  {
    public const string Identifier = nameof(SystemVersionCompatibilityCheckResponseMessage);

    public sealed override string Id => Identifier;

    public object Result { get; set; }
  
    public Exception Exception { get; set; }

    public bool HasError => Exception != null;
  }

  [Serializable]
  public class SystemVersionCompatibilityDto
  {
    public bool IsCompatible { get; set; }

    public string ServerVersion { get; set; }
  }
}
