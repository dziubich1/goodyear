﻿using System;

namespace Kalisto.Communication.Core.Messages
{
  [Serializable]
  public class ServerImageThumbnailRequest : EndpointMessageBase
  {
    public const string Identifier = nameof(ServerImageThumbnailRequest);

    public sealed override string Id => Identifier;

    public string RelativePath { get; set; }
  }
}
