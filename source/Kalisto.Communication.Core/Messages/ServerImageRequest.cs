﻿using System;

namespace Kalisto.Communication.Core.Messages
{
  [Serializable]
  public class ServerImageRequest : EndpointMessageBase
  {
    public const string Identifier = nameof(ServerImageRequest);

    public sealed override string Id => Identifier;

    public string RelativePath { get; set; }
  }
}
