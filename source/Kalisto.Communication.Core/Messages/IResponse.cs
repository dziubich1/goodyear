﻿using System;

namespace Kalisto.Communication.Core.Messages
{
  public interface IResponse
  {
    object Result { get; set; }

    Exception Exception { get; set; }

    bool HasError { get; }
  }
}
