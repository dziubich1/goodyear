﻿using System;

namespace Kalisto.Communication.Core.Messages
{
  [Serializable]
  public class DeviceConfigurationsRequestMessage : EndpointMessageBase
  {
    public const string Identifier = nameof(DeviceConfigurationsRequestMessage);

    public sealed override string Id => Identifier;
  }
}
