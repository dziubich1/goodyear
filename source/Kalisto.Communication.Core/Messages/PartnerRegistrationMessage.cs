﻿using System;

namespace Kalisto.Communication.Core.Messages
{
  public enum ClientType
  {
    Frontend, Device
  }

  [Serializable]
  public class PartnerRegistrationMessage : EndpointMessageBase
  {
    public const string Identifier = nameof(PartnerRegistrationMessage);

    public override string Id => Identifier;

    public ClientType Type { get; set; }
  }
}
