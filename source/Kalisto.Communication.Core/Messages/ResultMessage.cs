﻿using System;

namespace Kalisto.Communication.Core.Messages
{
  public class ResultMessage : EndpointMessageBase, IResponse
  {
    public const string Identifier = nameof(ResultMessage);

    public override string Id => Identifier;

    public object Result { get; set; }

    public Exception Exception { get; set; }

    public bool HasError => Exception != null;
  }
}
