﻿using System;

namespace Kalisto.Communication.Core.Messages
{
  [Serializable]
  public class PartnerRegistrationResponseMessage : EndpointMessageBase
  {
    public const string Identifier = nameof(PartnerRegistrationResponseMessage);

    public override string Id => Identifier;

    public bool Succeeded { get; set; }

    public string FailureReasonCode { get; set; }
  }
}
