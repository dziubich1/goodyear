﻿using System;

namespace Kalisto.Communication.Core.Messages
{
  [Serializable]
  public class HeartbeatMessage : EndpointMessageBase
  {
    public const string Identifier = nameof(HeartbeatMessage);

    public override string Id => Identifier;

    public ClientType Type { get; set; }
  }
}
