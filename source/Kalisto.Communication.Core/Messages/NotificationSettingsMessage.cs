﻿using Kalisto.Mailing.Notifications.Configuration;
using System;

namespace Kalisto.Communication.Core.Messages
{
  [Serializable]
  public class NotificationSettingsMessage : EndpointMessageBase
  {
    public const string Identifier = nameof(NotificationSettingsMessage);

    public override string Id => Identifier;

    public NotificationSettings NotificationSettings { get; set; }
  }
}
