﻿using Kalisto.Communication.Core.Communication;
using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Configuration;
using System;
using System.Threading.Tasks;
using Kalisto.Communication.Core.Jobs;
using Kalisto.Localization;
using Unity;
using Unity.Injection;
using Unity.Lifetime;

namespace Kalisto.Communication.Core
{
  public abstract class ClientEndpoint<TConfig>
    : EndpointModule<TConfig> where TConfig : ConfigurationBase, new()
  {
    private ClientInfo _clientInfo;

    private IRegisterClient _registerClient;

    private IClientMessageProvider _messageProvider;

    private bool _heartBeatingAlreadyStarted;

    protected ClientEndpoint(string domainName, string appName)
      : base(domainName, appName)
    {
    }

    protected ClientEndpoint(IUnityContainer container, string domainName, string appName)
      : base(container, domainName, appName)
    {
    }

    protected abstract ClientType GetClientType();

    protected override void Bind(IUnityContainer container)
    {
      base.Bind(container);

      container.RegisterSingleton<ClientInfo>(new InjectionFactory(c =>
        new ClientInfo(GetClientType())));

      container.RegisterType<ICommunicationComponentFactory, CommunicationComponentFactory>();
      container.RegisterSingleton<IClientMessageProvider, ClientMessageProvider>();
      container.RegisterType<IMessageBuilder, MessageBuilder>();
      container.RegisterSingleton<RegistrationResponseMessageHandler>();
      container.RegisterType<IRegisterClient, RegistrationResponseMessageHandler>();

      container.RegisterType<ILocalizer<Localization.Messages.Messages>, LocalizationManager<Localization.Messages.Messages>>();
    }

    protected override void Resolve(IUnityContainer container)
    {
      base.Resolve(container);

      _clientInfo = container.Resolve<ClientInfo>();

      _messageProvider = container.Resolve<IClientMessageProvider>();
      _registerClient = container.Resolve<IRegisterClient>();
      _registerClient.ClientRegistered += StartHeartBeating;
    }

    protected override void OnConfigured()
    {
      Logger.Info("Client endpoint has been configured.");
    }

    protected override void OnInitialized()
    {
      Logger.Info("Client endpoint has been initialized.");
    }

    private void StartHeartBeating()
    {
      if (!_heartBeatingAlreadyStarted)
        _heartBeatingAlreadyStarted = true;
      else return;

      Task.Run(async () =>
      {
        while (true)
        {
          await Task.Delay(5000);
          _messageProvider.SendHeartbeatMessage(new HeartbeatMessage
          {
            PartnerId = _clientInfo.Id,
            OwnerGuid = Guid.NewGuid().ToString(),
            Type = _clientInfo.Type
          });
        }
      });
    }
  }
}
