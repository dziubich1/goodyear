﻿using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Hmi.Core.Communication;

namespace Kalisto.Frontend.Handlers
{
  public class ResultMessageHandler : MessageHandlerBase<ResultMessage>
  {
    private readonly IClientMessageRouter _messageAggregator;

    public ResultMessageHandler(IClientMessageRouter messageAggregator)
    {
      _messageAggregator = messageAggregator;
    }

    protected override void HandleMessage(ResultMessage message)
    {
      _messageAggregator.Route(message);
    }
  }
}
