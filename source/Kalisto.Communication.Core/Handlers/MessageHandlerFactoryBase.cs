﻿using Kalisto.Communication.Core.Messaging;
using Unity;

namespace Kalisto.Frontend.Handlers
{
  public abstract class MessageHandlerFactoryBase
  {
    protected readonly IUnityContainer DependencyContainer;

    protected MessageHandlerFactoryBase(IUnityContainer container)
    {
      DependencyContainer = container;
    }

    public IMessageHandler GetRegistrationMessageHandler()
    {
      return DependencyContainer.Resolve<RegistrationResponseMessageHandler>();
    }

    public IMessageHandler GetSystemVersionCheckResponseMessageHandler()
    {
      return DependencyContainer.Resolve<SystemVersionCheckResponseHandler>();
    }

    public IMessageHandler GetResultMessageHandler()
    {
      return DependencyContainer.Resolve<ResultMessageHandler>();
    }
  }
}
