﻿using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Hmi.Core.Communication;

namespace Kalisto.Frontend.Handlers
{
  public class SystemVersionCheckResponseHandler
    : MessageHandlerBase<SystemVersionCompatibilityCheckResponseMessage>
  {
    private readonly IClientMessageRouter _messageAggregator;

    public SystemVersionCheckResponseHandler(IClientMessageRouter messageAggregator)
    {
      _messageAggregator = messageAggregator;
    }

    protected override void HandleMessage(SystemVersionCompatibilityCheckResponseMessage message)
    {
      _messageAggregator.Route(message);
    }
  }
}
