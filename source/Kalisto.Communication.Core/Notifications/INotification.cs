﻿namespace Kalisto.Frontend.Notifications.Base
{
  public interface INotification
  {
    string NotificationId { get; }
  }
}
