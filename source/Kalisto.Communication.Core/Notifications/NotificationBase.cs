﻿using Kalisto.Communication.Core.Messages;

namespace Kalisto.Frontend.Notifications.Base
{
  public abstract class NotificationBase : EndpointMessageBase, INotification
  {
    public const string Identifier = nameof(NotificationBase);

    public sealed override string Id => Identifier;

    public abstract string NotificationId { get; }
  }
}
