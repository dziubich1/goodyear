﻿using System;

namespace Kalisto.Communication.Core.Communication
{
  public interface IMessageReceiver : IMessageTransport, IDisposable
  {
    event Action<string, string> OnMessageReceived;
  }
}
