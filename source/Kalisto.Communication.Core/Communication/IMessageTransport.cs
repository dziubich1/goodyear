﻿namespace Kalisto.Communication.Core.Communication
{
  public interface IMessageTransport
  {
    string QueueName { get; }

    void Initialize(string topic);
  }
}
