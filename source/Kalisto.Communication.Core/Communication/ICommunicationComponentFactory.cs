﻿namespace Kalisto.Communication.Core.Communication
{
  public interface ICommunicationComponentFactory
  {
    IMessageReceiver GetRegistrationMessageReceiver();

    IMessageReceiver GetHeartbeatMessageReceiver();

    IMessageSender GetRegistrationMessageSender();

    IMessageSender GetHeartbeatMessageSender();

    IMessageReceiver GetClientMessageReceiver(string clientId);

    IMessageSender GetClientMessageSender(string clientId);

    IMessageSender GetServerMessageSender(string clientId);

    IMessageReceiver GetServerMessageReceiver(string clientId);
  }
}
