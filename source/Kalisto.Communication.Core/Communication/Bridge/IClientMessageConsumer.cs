﻿using Kalisto.Communication.Core.Messages;

namespace Kalisto.Hmi.Core.Communication
{
  public interface IClientMessageConsumer
  {
    string Guid { get; }

    void ProcessMessage(EndpointMessageBase message);
  }
}
