﻿using Kalisto.Communication.Core.Messages;

namespace Kalisto.Hmi.Core.Communication
{
  public interface IClientMessageRouter
  {
    void Route(EndpointMessageBase message);

    void Subscribe(IClientMessageConsumer consumer);
  }
}
