﻿using Kalisto.Communication.Core.Exceptions;
using Kalisto.Communication.Core.Extensions;
using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Frontend.Notifications.Base;
using Kalisto.Hmi.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Kalisto.Hmi.Core.Communication
{
  public class RequestResult<T>
  {
    public T Response { get; set; }

    public bool Succeeded { get; set; }

    public Exception ThrownException { get; set; }

    public static RequestResult<T> Default => new RequestResult<T>
    {
      Response = default(T),
      Succeeded = false,
      ThrownException = null
    };
  }

  public class BasicMessageBridge : IFrontendMessageBridge, IClientMessageConsumer
  {
    private readonly AutoResetEvent _messageReceivedEvent = new AutoResetEvent(false);

    private readonly CancellationToken _cancellationToken = new CancellationToken(false);

    private readonly TimeSpan _defaultRequestTimeout = TimeSpan.FromSeconds(60);

    private readonly IClientMessageProvider _clientMessageProvider;

    private readonly IAsyncProgressService _asyncProgressService;

    private readonly Dictionary<EndpointMessageBase, IResponse> _pendingOperations =
      new Dictionary<EndpointMessageBase, IResponse>();

    private readonly object _operationSyncObj = new object();

    private IExceptionHandler _exceptionHandler;

    public string Guid { get; }

    public event Action<INotification> NotificationReceived;

    public event Action LoginRequiredReceived;

    public BasicMessageBridge(IClientMessageProvider clientMessageProvider,
      IClientMessageRouter clientMessageRouter, IAsyncProgressService asyncProgressService)
    {
      Guid = System.Guid.NewGuid().ToString();

      _clientMessageProvider = clientMessageProvider;
      _asyncProgressService = asyncProgressService;

      clientMessageRouter.Subscribe(this);
    }

    public void RegisterExceptionHandler(IExceptionHandler exceptionHandler)
    {
      _exceptionHandler = exceptionHandler;
    }

    public async Task<RequestResult<TO>> SendMessageWithResultAsync<TO>(EndpointMessageBase message)
    {
      if (_pendingOperations.Count != 0)
        throw new InvalidOperationException();

      _asyncProgressService.StartAsyncOperation();

      message.OwnerGuid = Guid;
      message.MessageId = System.Guid.NewGuid().ToString();

      _pendingOperations.Add(message, null);
      _clientMessageProvider.SendMessage(message);

      var operationResponse = default(RequestResult<TO>);
      if (await _messageReceivedEvent.WaitOneAsync((int)_defaultRequestTimeout.TotalMilliseconds,
        _cancellationToken))
      {
        if (_pendingOperations.ContainsKey(message) && _pendingOperations[message] != null)
        {
          var response = _pendingOperations[message];
          _pendingOperations.Clear();

          operationResponse = new RequestResult<TO>
          {
            Succeeded = true,
            Response = response.Result != null ? (TO)response.Result : default(TO)
          };

          if (response.HasError)
          {
            operationResponse.Succeeded = false;
            operationResponse.ThrownException = response.Exception;
            if (_exceptionHandler != null)
              await _exceptionHandler.HandleExceptionAsync(response.Exception);
          }
        }
      }

      _asyncProgressService.FinishAsyncOperation();
      if (operationResponse == default(RequestResult<TO>) && _exceptionHandler != null)
        await _exceptionHandler.HandleExceptionAsync(new CommunicationException());

      if (operationResponse == default(RequestResult<TO>))
        _pendingOperations.Clear();

      return operationResponse;
    }

    public void ProcessMessage(EndpointMessageBase message)
    {
      lock (_operationSyncObj)
      {
        if (message is INotification notification)
        {
          NotificationReceived?.Invoke(notification);
          return;
        }

        if (message is LoginRequiredMessage)
        {
          LoginRequiredReceived?.Invoke();
          return;
        }

        if (_pendingOperations.Count == 0)
          return;

        if (message is IResponse response)
        {
          var key = _pendingOperations.First().Key;
          if (key.MessageId != message.MessageId)
            return;

          _pendingOperations[key] = response;
          _messageReceivedEvent.Set();
        }
      }
    }
  }
}
