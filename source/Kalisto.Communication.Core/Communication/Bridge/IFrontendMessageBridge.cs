﻿using System;
using System.Threading.Tasks;
using Kalisto.Communication.Core.Messages;
using Kalisto.Frontend.Notifications.Base;

namespace Kalisto.Hmi.Core.Communication
{
  public interface IFrontendMessageBridge
  {
    void RegisterExceptionHandler(IExceptionHandler exceptionHandler);

    Task<RequestResult<TO>> SendMessageWithResultAsync<TO>(EndpointMessageBase message);

    event Action<INotification> NotificationReceived;

    event Action LoginRequiredReceived;
  }
}
