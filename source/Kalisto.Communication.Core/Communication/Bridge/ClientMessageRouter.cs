﻿using System;
using System.Collections.Generic;
using System.Linq;
using Kalisto.Communication.Core.Messages;

namespace Kalisto.Hmi.Core.Communication
{
  public class ClientMessageRouter : IClientMessageRouter
  {
    private readonly List<IClientMessageConsumer> _consumers = new List<IClientMessageConsumer>();

    public void Route(EndpointMessageBase message)
    {
      if (message == null)
        return;

      if (message.Broadcasting)
      {
        foreach (var consumer in _consumers)
          consumer.ProcessMessage(message);
      }
      else
      {
        if (string.IsNullOrEmpty(message.OwnerGuid))
          throw new InvalidOperationException("Received message does not have an owner GUID specified.");

        foreach (var consumer in _consumers.Where(c => c.Guid == message.OwnerGuid))
          consumer.ProcessMessage(message);
      }
    }

    public void Subscribe(IClientMessageConsumer consumer)
    {
      if (_consumers.Contains(consumer))
        return;

      _consumers.Add(consumer);
    }
  }
}
