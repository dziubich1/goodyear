﻿using Kalisto.Communication.Core.Messaging;
using Unity;

namespace Kalisto.Communication.Core.Communication
{
  public class CommunicationComponentFactory : ICommunicationComponentFactory
  {
    private readonly IUnityContainer _dependencyContainer;

    public CommunicationComponentFactory(IUnityContainer container)
    {
      _dependencyContainer = container;
    }

    public IMessageReceiver GetRegistrationMessageReceiver()
    {
      var registrationMessageReceiver = _dependencyContainer.Resolve<IMessageReceiver>();
      registrationMessageReceiver.Initialize(ClientInfo.GetRegistrationQueueName());

      return registrationMessageReceiver;
    }

    public IMessageReceiver GetHeartbeatMessageReceiver()
    {
      var heartbeatMessageReceiver = _dependencyContainer.Resolve<IMessageReceiver>();
      heartbeatMessageReceiver.Initialize(ClientInfo.GetHeartbeatQueueName());

      return heartbeatMessageReceiver;
    }

    public IMessageSender GetRegistrationMessageSender()
    {
      var registrationMessageSender = _dependencyContainer.Resolve<IMessageSender>();
      registrationMessageSender.Initialize(ClientInfo.GetRegistrationQueueName());

      return registrationMessageSender;
    }

    public IMessageSender GetHeartbeatMessageSender()
    {
      var heartbeatMessageSender = _dependencyContainer.Resolve<IMessageSender>();
      heartbeatMessageSender.Initialize(ClientInfo.GetHeartbeatQueueName());

      return heartbeatMessageSender;
    }

    public IMessageReceiver GetClientMessageReceiver(string clientId)
    {
      var feedbackQueueName = ClientInfo.GetFeedbackQueueName(clientId);
      return GetMessageReceiver(feedbackQueueName);
    }

    public IMessageSender GetClientMessageSender(string clientId)
    {
      var queueName = ClientInfo.GetQueueName(clientId);
      return GetMessageSender(queueName);
    }

    public IMessageReceiver GetServerMessageReceiver(string clientId)
    {
      var queueName = ClientInfo.GetQueueName(clientId);
      return GetMessageReceiver(queueName);
    }

    public IMessageSender GetServerMessageSender(string clientId)
    {
      var feedbackQueueName = ClientInfo.GetFeedbackQueueName(clientId);
      return GetMessageSender(feedbackQueueName);
    }

    private IMessageReceiver GetMessageReceiver(string topic)
    {
      var receiver = _dependencyContainer.Resolve<IMessageReceiver>();
      receiver.Initialize(topic);

      return receiver;
    }

    private IMessageSender GetMessageSender(string topic)
    {
      var sender = _dependencyContainer.Resolve<IMessageSender>();
      sender.Initialize(topic);

      return sender;
    }
  }
}
