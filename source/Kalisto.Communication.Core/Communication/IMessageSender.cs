﻿using System;

namespace Kalisto.Communication.Core.Communication
{
  public interface IMessageSender : IMessageTransport, IDisposable
  {
    void SendMessage(string message);
  }
}
