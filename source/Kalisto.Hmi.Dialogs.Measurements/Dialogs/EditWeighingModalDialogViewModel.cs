﻿using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Core;
using Kalisto.Hmi.Core;
using Kalisto.Hmi.Core.Commands;
using Kalisto.Hmi.Core.Communication;
using Kalisto.Hmi.Core.Controls;
using Kalisto.Hmi.Core.Services;
using Kalisto.Hmi.Dialogs.Cargoes.Services;
using Kalisto.Hmi.Dialogs.Common;
using Kalisto.Hmi.Dialogs.Common.Dialogs;
using Kalisto.Hmi.Dialogs.Contractors.Services;
using Kalisto.Hmi.Dialogs.Drivers.Services;
using Kalisto.Hmi.Dialogs.Measurements.Services;
using Kalisto.Hmi.Dialogs.Vehicles.Services;
using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Input;
using System.Windows.Media.Imaging;

namespace Kalisto.Hmi.Dialogs.Measurements.Dialogs
{
  [AddINotifyPropertyChangedInterface]
  public class EditWeighingModalDialogViewModel : MeasurementModalViewModelBase<EditWeighingModalDialogViewModel>
  {
    private readonly IAsyncProgressService _asyncProgressService;

    private readonly IServerImageService _serverImageService;

    public ObservableCollectionEx<BitmapImage> FirstPhotoGallery { get; set; }

    public ObservableCollectionEx<BitmapImage> SecondPhotoGallery { get; set; }

    public int FirstPhotoGalleryCurrentItemIndex { get; set; }

    public int SecondPhotoGalleryCurrentItemIndex { get; set; }

    public ICommand FirstThumbnailClicked { get; set; }

    public ICommand SecondThumbnailClicked { get; set; }

    public EditWeighingModalDialogViewModel(IDialogService dialogService,
      IDriverService driverService, IContractorService contractorService, IMeasurementService measurementService,
      IVehicleService vehicleService, ICargoService cargoService, IUnitTypeService unitTypeService,
      IAsyncProgressService asyncProgressService, IMessageBuilder messageBuilder,
      IFrontendMessageBridge messageBridge, IServerImageService serverImageService, ClientInfo clientInfo)
      : base(dialogService, driverService, contractorService, measurementService, vehicleService,
          cargoService, unitTypeService, asyncProgressService, messageBuilder, messageBridge,
          clientInfo)
    {
      _asyncProgressService = asyncProgressService;
      _serverImageService = serverImageService;
      FirstPhotoGallery = new ObservableCollectionEx<BitmapImage>();
      SecondPhotoGallery = new ObservableCollectionEx<BitmapImage>();

      FirstThumbnailClicked = new DelegateCommand(OnFirstThumbnailClicked);
      SecondThumbnailClicked = new DelegateCommand(OnSecondThumbnailClicked);
    }

    protected override async void OnViewLoaded(object view)
    {
      base.OnViewLoaded(view);

      var photos = new List<Image>();
      foreach (var photoPath in FirstPhotoPaths)
        photos.Add(await _serverImageService.GetImageThumbnailAsync(photoPath));
      if (photos.Any())
        FirstPhotoGallery.AddRange(photos.Select(c => c.ToBitmapImage()));

      photos = new List<Image>();
      foreach (var photoPath in SecondPhotoPaths)
        photos.Add(await _serverImageService.GetImageThumbnailAsync(photoPath));
      if (photos.Any())
        SecondPhotoGallery.AddRange(photos.Select(c => c.ToBitmapImage()));
    }

    protected override void DefineValidationRules()
    {
      InvalidateProperty(() => Measurement1Value).When(() => !IsDecimalOrNull(Measurement1Value))
        .WithMessageKey("InvalidNumber").ButIgnoreOnLoad();

      InvalidateProperty(() => Measurement1Value).When(() => !IsGreaterOrEqualThan(Measurement1Value, 0) && !string.IsNullOrEmpty(Measurement1Value))
        .WithMessageKey("MustBeGreaterOrEqualThan", 0).ButIgnoreOnLoad();

      if (IsSecondMeasurementDone)
      {
        InvalidateProperty(() => Measurement2Value).When(() => !IsDecimalOrNull(Measurement2Value))
          .WithMessageKey("InvalidNumber").ButIgnoreOnLoad();
        InvalidateProperty(() => Measurement2Value).When(() => !IsGreaterOrEqualThan(Measurement2Value, 0) && !string.IsNullOrEmpty(Measurement1Value))
          .WithMessageKey("MustBeGreaterOrEqualThan", 0).ButIgnoreOnLoad();
      }
      else
      {
        RemovePropertyValidations(() => Measurement2Value);
      }

      InvalidateProperty(() => Tare).When(() => !IsDecimalOrNull(Tare))
        .WithMessageKey("InvalidNumber").ButIgnoreOnLoad();

      InvalidateProperty(() => Tare).When(() => !IsGreaterOrEqualThan(Tare, 0) && !string.IsNullOrEmpty(Tare))
        .WithMessageKey("MustBeGreaterOrEqualThan", 0).ButIgnoreOnLoad();

      InvalidateProperty(() => NetWeightValue).When(() => !IsDecimalOrNull(NetWeightValue))
        .WithMessageKey("InvalidNumber").ButIgnoreOnLoad();

      InvalidateProperty(() => NetWeightValue).When(() => !IsGreaterOrEqualThan(NetWeightValue, 0) && !string.IsNullOrEmpty(NetWeightValue))
        .WithMessageKey("MustBeGreaterOrEqualThan", 0).ButIgnoreOnLoad();

      InvalidateProperty(() => DeclaredWeightValue).When(() => !IsDecimalOrNull(DeclaredWeightValue))
        .WithMessageKey("InvalidNumber").ButIgnoreOnLoad();

      InvalidateProperty(() => DeclaredWeightValue).When(() => !IsGreaterOrEqualThan(DeclaredWeightValue, 0) && !string.IsNullOrEmpty(DeclaredWeightValue))
        .WithMessageKey("MustBeGreaterOrEqualThan", 0).ButIgnoreOnLoad();
    }

    private async void OnFirstThumbnailClicked(object obj)
    {
      try
      {
        var path = FirstPhotoPaths.ElementAtOrDefault(FirstPhotoGalleryCurrentItemIndex);
        if (string.IsNullOrEmpty(path))
          return;
        _asyncProgressService.StartAsyncOperation();
        var image = await _serverImageService.GetImageAsync(path);
        var bitmapImage = image.ToBitmapImage();
        DialogService.ShowDialog<ImagePreviewWindowDialogViewModel>(vm =>
        {
          vm.Image = bitmapImage;
          _asyncProgressService.FinishAsyncOperation();
        });
      }
      catch (Exception)
      {
        _asyncProgressService.FinishAsyncOperation();
      }
    }

    private async void OnSecondThumbnailClicked(object obj)
    {
      try
      {
        var path = SecondPhotoPaths.ElementAtOrDefault(SecondPhotoGalleryCurrentItemIndex);
        if (string.IsNullOrEmpty(path))
          return;
        _asyncProgressService.StartAsyncOperation();
        var image = await _serverImageService.GetImageAsync(path);
        var bitmapImage = image.ToBitmapImage();
        DialogService.ShowDialog<ImagePreviewWindowDialogViewModel>(vm =>
        {
          vm.Image = bitmapImage;
          _asyncProgressService.FinishAsyncOperation();
        });
      }
      catch (Exception)
      {
        _asyncProgressService.FinishAsyncOperation();
      }
    }
  }
}
