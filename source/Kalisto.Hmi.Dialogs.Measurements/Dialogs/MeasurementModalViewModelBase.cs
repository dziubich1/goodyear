﻿using System.Collections.Generic;
using Kalisto.Backend.DataAccess.Model;
using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Frontend.Notifications.Enums;
using Kalisto.Hmi.Core;
using Kalisto.Hmi.Core.Commands;
using Kalisto.Hmi.Core.Communication;
using Kalisto.Hmi.Core.Controls;
using Kalisto.Hmi.Core.Services;
using Kalisto.Hmi.Dialogs.Cargoes.Dialogs;
using Kalisto.Hmi.Dialogs.Cargoes.Services;
using Kalisto.Hmi.Dialogs.Common;
using Kalisto.Hmi.Dialogs.Common.Models;
using Kalisto.Hmi.Dialogs.Contractors.Dialogs;
using Kalisto.Hmi.Dialogs.Contractors.Services;
using Kalisto.Hmi.Dialogs.Drivers.Dialogs;
using Kalisto.Hmi.Dialogs.Drivers.Services;
using Kalisto.Hmi.Dialogs.Measurements.Services;
using Kalisto.Hmi.Dialogs.Vehicles.Dialogs;
using Kalisto.Hmi.Dialogs.Vehicles.Models;
using Kalisto.Hmi.Dialogs.Vehicles.Services;
using Kalisto.Localization;
using PropertyChanged;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Kalisto.Localization.Messages;

namespace Kalisto.Hmi.Dialogs.Measurements.Dialogs
{
  [AddINotifyPropertyChangedInterface]
  public abstract class MeasurementModalViewModelBase<TViewModel>
    : ModalViewModelBase<TViewModel> where TViewModel : class
  {
    private readonly ILocalizer<Messages> _localizer;

    public override string DialogNameKey { get; set; }

    public LocalizedDate MeasurementDate1 { get; set; }

    public LocalizedDate MeasurementDate2 { get; set; }

    public MeasurementClass? SelectedMeasurementClass { get; set; }

    public MeasurementType SelectedMeasurementType { get; set; }

    public string NetWeightValue { get; set; }

    public bool IsSingleMeasurement =>
      SelectedMeasurementType == MeasurementType.Single;

    public bool IsDoubleMeasurement =>
      SelectedMeasurementType == MeasurementType.Double;

    public bool IsDoubleMeasurementSecond =>
      IsDoubleMeasurement && IsSecondMeasurementDone;

    public bool IsFinalMeasurement =>
      IsSingleMeasurement || IsDoubleMeasurementSecond;

    public bool IsSecondMeasurementDone =>
      MeasurementDate2 != null && MeasurementDate2.Value != null;

    [AlsoNotifyFor(nameof(IsMeasurementNumberVisible))]
    public long? MeasurementNumber { get; set; }

    public bool IsMeasurementNumberVisible => MeasurementNumber.HasValue;

    public string Measurement1Value { get; set; }

    public string Measurement2Value { get; set; }

    public string Tare { get; set; }

    public bool IsCancelled { get; set; }

    public string Comment { get; set; }

    public ICommand DriverNotFoundCommand { get; set; }

    public ICommand ContractorNotFoundCommand { get; set; }

    public ICommand CargoNotFoundCommand { get; set; }

    public ICommand VehicleNotFoundCommand { get; set; }

    public ICommand AddDriverCommand { get; set; }

    public ICommand AddContractorCommand { get; set; }

    public ICommand AddVehicleCommand { get; set; }

    public ICommand AddCargoCommand { get; set; }

    public ObservableCollectionEx<ReferenceModel> Drivers { get; set; }

    public ObservableCollectionEx<ReferenceModel> Contractors { get; set; }

    public ObservableCollectionEx<ReferenceModel> Cargoes { get; set; }

    public ObservableCollectionEx<ReferenceModel> Vehicles { get; set; }

    public ObservableCollectionEx<ReferenceModel> SemiTrailers { get; set; }

    public WeighingMode WeighingMode { get; set; }

    public WeighingDeviceStatus DeviceStatus { get; set; }

    public ReferenceModel Driver { get; set; }

    public ReferenceModel Contractor { get; set; }

    public ReferenceModel Cargo { get; set; }

    public ReferenceModel SemiTrailer { get; set; }

    public string SemiTrailerValue { get; set; } // required for non-existing semi-trailers

    public ReferenceModel SelectedSemiTrailer
    {
      get
      {
        if (SemiTrailer == null)
          return new ReferenceModel
          {
            DisplayValue = SemiTrailerValue
          };

        return SemiTrailer;
      }
    }

    [AlsoNotifyFor(nameof(WeightExceeded))]
    public ReferenceModel Vehicle { get; set; }

    protected abstract override void DefineValidationRules();

    private readonly ReferenceModel _emptyItem;

    private readonly IDriverService _driverService;

    private readonly IContractorService _contractorService;

    private readonly IVehicleService _vehicleService;

    private readonly ICargoService _cargoService;

    protected IUnitTypeService UnitTypeService { get; }

    public string DeclaredWeightValue { get; set; }

    public long MeasurementId { get; set; }

    public UnitOfMeasurement TareUom =>
      decimal.TryParse(Tare, out decimal value) ? UnitTypeService.ConvertBackValue(value) : null;

    public UnitOfMeasurement Measurement1ValueUom =>
      decimal.TryParse(Measurement1Value, out decimal value) ? UnitTypeService.ConvertBackValue(value) : null;

    public UnitOfMeasurement Measurement2ValueUom =>
      decimal.TryParse(Measurement2Value, out decimal value) ? UnitTypeService.ConvertBackValue(value) : null;

    public UnitOfMeasurement NetWeightValueUom =>
      decimal.TryParse(NetWeightValue, out decimal value) ? UnitTypeService.ConvertBackValue(value) : null;

    public UnitOfMeasurement DeclaredWeightValueUom =>
      decimal.TryParse(DeclaredWeightValue, out decimal value) ? UnitTypeService.ConvertBackValue(value) : null;

    protected IMeasurementService MeasurementService { get; }

    public bool WeightExceeded => HasVehicleWeightExceeded();

    public bool IsDeviceIndicationValid => DeviceStatus == WeighingDeviceStatus.Stable || WeighingMode == WeighingMode.Manual;

    public List<string> FirstPhotoPaths { get; set; }

    public List<string> SecondPhotoPaths { get; set; }

    protected MeasurementModalViewModelBase(IDialogService dialogService,
      IDriverService driverService, IContractorService contractorService, IMeasurementService measurementService,
      IVehicleService vehicleService, ICargoService cargoService, IUnitTypeService unitTypeService,
      IAsyncProgressService asyncProgressService, IMessageBuilder messageBuilder,
      IFrontendMessageBridge messageBridge, ClientInfo clientInfo)
      : base(dialogService, asyncProgressService, messageBuilder, messageBridge,
          clientInfo)
    {
      _localizer = new LocalizationManager<Messages>();
      _driverService = driverService;
      _contractorService = contractorService;
      _vehicleService = vehicleService;
      _cargoService = cargoService;
      _emptyItem = new ReferenceModel() { DisplayValue = "", Id = -1 };
      MeasurementService = measurementService;
      UnitTypeService = unitTypeService;

      DriverNotFoundCommand = new DelegateCommand(OpenDriverNotFoundDialog);
      ContractorNotFoundCommand = new DelegateCommand(OpenContractorNotFoundDialog);
      CargoNotFoundCommand = new DelegateCommand(OpenCargoNotFoundDialog);
      VehicleNotFoundCommand = new DelegateCommand(OpenVehicleNotFoundDialog);

      AddDriverCommand = new DelegateCommand(OpenAddDriverDialog);
      AddContractorCommand = new DelegateCommand(OpenAddContractorDialog);
      AddVehicleCommand = new DelegateCommand(OpenAddVehicleDialog);
      AddCargoCommand = new DelegateCommand(OpenAddCargoDialog);

      Drivers = new ObservableCollectionEx<ReferenceModel>();
      Contractors = new ObservableCollectionEx<ReferenceModel>();
      Vehicles = new ObservableCollectionEx<ReferenceModel>();
      Cargoes = new ObservableCollectionEx<ReferenceModel>();
      SemiTrailers = new ObservableCollectionEx<ReferenceModel>();
    }

    protected override async void OnViewLoaded(object view)
    {
      base.OnViewLoaded(view);

      await LoadDrivers();
      await LoadContractors();
      await LoadVehicles();
      await LoadCargoes();
      await LoadSemiTrailers();

      SetSelectedItemsReferences();
    }

    private async void OpenDriverNotFoundDialog(object parameter)
    {
      await DialogService.ShowMessageAsync(_localizer.Localize(() => Messages.Error),
        _localizer.Localize(() => Messages.NoDriverFound), MessageDialogType.Ok);
    }

    private async void OpenContractorNotFoundDialog(object parameter)
    {
      await DialogService.ShowMessageAsync(_localizer.Localize(() => Messages.Error),
        _localizer.Localize(() => Messages.NoContractorFound), MessageDialogType.Ok);
    }

    private async void OpenCargoNotFoundDialog(object parameter)
    {
      await DialogService.ShowMessageAsync(_localizer.Localize(() => Messages.Error),
        _localizer.Localize(() => Messages.NoCargoFound), MessageDialogType.Ok);
    }

    private async void OpenVehicleNotFoundDialog(object parameter)
    {
      await DialogService.ShowMessageAsync(_localizer.Localize(() => Messages.Error),
        _localizer.Localize(() => Messages.NoVehicleFound), MessageDialogType.Ok);
    }

    private void SetSelectedItemsReferences()
    {
      if (Driver != null)
        Driver = Drivers.FirstOrDefault(d => d.Id == Driver.Id);
      else
        Driver = Drivers.FirstOrDefault();

      if (Contractor != null)
        Contractor = Contractors.FirstOrDefault(c => c.Id == Contractor.Id);
      else
        Contractor = Contractors.FirstOrDefault();

      if (Vehicle != null)
        Vehicle = Vehicles.FirstOrDefault(c => c.Id == Vehicle.Id);
      else
        Vehicle = Vehicles.FirstOrDefault();

      if (Cargo != null)
        Cargo = Cargoes.FirstOrDefault(c => c.Id == Cargo.Id);
      else
        Cargo = Cargoes.FirstOrDefault();

      if (SemiTrailer != null)
        SemiTrailer = SemiTrailers.FirstOrDefault(c => c.Id == SemiTrailer.Id);
      else
        SemiTrailer = SemiTrailers.FirstOrDefault();
    }

    private async Task LoadSemiTrailers()
    {
      var result = await MeasurementService.GetAllSemiTrailersAsync();
      if (!result.Succeeded)
        return;

      var mapped = result.Response.Select(item => new ReferenceModel()
      {
        Id = item.Id,
        DisplayValue = item.Number
      });

      SemiTrailers.Clear();
      SemiTrailers.Add(_emptyItem);
      SemiTrailers.AddRange(mapped);
    }

    private async Task LoadDrivers()
    {
      var result = await _driverService.GetAllDriversAsync();
      if (!result.Succeeded)
        return;

      var mapped = result.Response.Select(item => new ReferenceModel()
      {
        Id = item.Id,
        DisplayValue = item.Identifier
      });
      Drivers.Clear();
      Drivers.Add(_emptyItem);
      Drivers.AddRange(mapped);
    }

    private async Task LoadContractors()
    {
      var result = await _contractorService.GetAllContractorsAsync();
      if (!result.Succeeded)
        return;

      var mapped = result.Response.Select(item => new ReferenceModel()
      {
        Id = item.Id,
        DisplayValue = item.Name
      });
      Contractors.Clear();
      Contractors.Add(_emptyItem);
      Contractors.AddRange(mapped);
    }

    private async Task LoadVehicles()
    {
      var result = await _vehicleService.GetAllVehiclesAsync();
      if (!result.Succeeded)
        return;

      var mapped = result.Response.Select(item => new ReferenceModel()
      {
        Id = item.Id,
        DisplayValue = item.PlateNumber,
        RawData = item
      });
      Vehicles.Clear();
      Vehicles.Add(_emptyItem);
      Vehicles.AddRange(mapped);
    }

    public void OnVehicleChanged()
    {
      if (Vehicle?.RawData == null)
        return;

      var vehicleData = Vehicle.RawData as VehicleModel;

      if (vehicleData?.Driver != null)
        Driver = Drivers.FirstOrDefault(d => d.Id == vehicleData.Driver.Id);

      if (vehicleData?.Contractor != null)
        Contractor = Contractors.FirstOrDefault(d => d.Id == vehicleData.Contractor.Id);
    }

    private async Task LoadCargoes()
    {
      var result = await _cargoService.GetAllCargoesAsync();
      if (!result.Succeeded)
        return;

      var mapped = result.Response.Select(item => new ReferenceModel()
      {
        Id = item.Id,
        DisplayValue = item.Name
      });
      Cargoes.Clear();
      Cargoes.Add(_emptyItem);
      Cargoes.AddRange(mapped);
    }

    private async void OpenAddDriverDialog(object parameter)
    {
      long driverId = -1;
      var dlg = await DialogService.ShowModalDialogAsync<AddOrUpdateDriverModalDialogViewModel>(
        vm =>
        {
          vm.DialogNameKey = "AddDriver";
        },
        async vm =>
        {
          var result = await _driverService.AddDriverAsync(vm.Identifier, vm.PhoneNumber);

          if (result.Succeeded)
            driverId = result.Response;

          return result.Succeeded;
        });

      if (!dlg.Result.HasValue || !dlg.Result.Value)
      {
        return;
      }

      await LoadDrivers();
      Driver = Drivers.FirstOrDefault(c => c.Id == driverId);
    }

    private async void OpenAddContractorDialog(object parameter)
    {
      long contractorId = -1;
      var dlg = await DialogService.ShowModalDialogAsync<AddOrUpdateContractorModalDialogViewModel>(
      vm =>
      {
        vm.DialogNameKey = "AddContractor";
      },
      async vm =>
      {
        var result = await _contractorService.AddContractorAsync(vm.Name,
          vm.Address, vm.PostalCode, vm.City, vm.TaxId);

        if (result.Succeeded)
          contractorId = result.Response;

        return result.Succeeded;
      });

      if (!dlg.Result.HasValue || !dlg.Result.Value)
        return;

      await LoadContractors();
      Contractor = Contractors.FirstOrDefault(c => c.Id == contractorId);
    }

    private async void OpenAddVehicleDialog(object parameter)
    {
      long vehicleId = -1;
      long? driverId = -1;
      long? contractorId = -1;
      var dlg = await DialogService.ShowModalDialogAsync<AddOrUpdateVehicleModalDialogViewModel>(
      vm =>
      {
        vm.DialogNameKey = "AddVehicle";
        vm.IsTareVisible = false;
      },
      async vm =>
      {
        driverId = vm.Driver != null && vm.Driver.Id != -1 ? vm.Driver.Id : (long?)null;
        contractorId = vm.Contractor != null && vm.Contractor.Id != -1 ? vm.Contractor.Id : (long?)null;

        var result = await _vehicleService.AddVehicleAsync(vm.PlateNumber,
          null, vm.MaxVehicleWeightUom?.OriginalValue,
          driverId, contractorId);

        if (result.Succeeded)
          vehicleId = result.Response;

        return result.Succeeded;
      });

      if (!dlg.Result.HasValue || !dlg.Result.Value)
        return;

      await LoadVehicles();
      Vehicle = Vehicles.FirstOrDefault(c => c.Id == vehicleId);

      if(driverId != null && driverId != -1)
      {
        await LoadDrivers();
        Driver = Drivers.FirstOrDefault(x => x.Id == driverId);
      }
      if (contractorId != null && contractorId != -1)
      {
        await LoadContractors();
        Contractor = Contractors.FirstOrDefault(x => x.Id == contractorId);
      }
    }

    private async void OpenAddCargoDialog(object parameter)
    {
      long cargoId = -1;
      var dlg = await DialogService.ShowModalDialogAsync<AddOrUpdateCargoModalDialogViewModel>(
      vm =>
      {
        vm.DialogNameKey = "AddCargo";
      },
      async viewModel =>
      {
        var result = await _cargoService.AddCargoAsync(viewModel.Name, viewModel.PriceValue);

        if (result.Succeeded)
          cargoId = result.Response;

        return result.Succeeded;
      });

      if (!dlg.Result.HasValue || !dlg.Result.Value)
        return;

      await LoadCargoes();
      Cargo = Cargoes.FirstOrDefault(c => c.Id == cargoId);
    }

    private bool HasVehicleWeightExceeded()
    {
      if (Vehicle == null)
        return false;

      if (!(Vehicle.RawData is VehicleModel vehicle))
        return false;

      if (!vehicle.MaxVehicleWeight.OriginalValue.HasValue)
        return false;

      if (!Measurement1ValueUom.OriginalValue.HasValue)
        return false;

      if (Measurement1ValueUom.OriginalValue.Value > vehicle.MaxVehicleWeight.OriginalValue.Value)
        return true;

      if (Measurement2ValueUom != null && Measurement2ValueUom.OriginalValue.HasValue)
        return false;

      if (Measurement2ValueUom != null && Measurement2ValueUom?.OriginalValue.Value > vehicle.MaxVehicleWeight.OriginalValue.Value)
        return true;

      return false;
    }
  }
}
