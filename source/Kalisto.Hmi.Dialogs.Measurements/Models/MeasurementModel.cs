﻿using System.Collections.Generic;
using Kalisto.Backend.DataAccess.Model;
using Kalisto.Hmi.Dialogs.Common;
using Kalisto.Hmi.Dialogs.Common.Models;
using Kalisto.Localization;

namespace Kalisto.Hmi.Dialogs.Measurements.Models
{
  public class MeasurementModel
  {
    public long Id { get; set; }

    public long Number { get; set; }

    public LocalizedEnum Type { get; set; }

    public LocalizedEnum Class { get; set; }

    public LocalizedDate Date1 { get; set; }

    public LocalizedDate Date2 { get; set; }

    public ReferenceModel Contractor { get; set; }

    public ReferenceModel Cargo { get; set; }

    public ReferenceModel Driver { get; set; }

    public ReferenceModel Vehicle { get; set; }

    public ReferenceModel SemiTrailer { get; set; }

    public ReferenceModel Operator { get; set; }

    public UnitOfMeasurement Tare { get; set; }

    public UnitOfMeasurement Measurement1 { get; set; }

    public UnitOfMeasurement Measurement2 { get; set; }

    public UnitOfMeasurement NetWeight { get; set; }

    public UnitOfMeasurement DeclaredWeight { get; set; }

    public decimal WeighingThreshold { get; set; }

    public string QRCode { get; set; }

    public List<string> FirstPhotoPaths { get; set; }

    public List<string> SecondPhotoPaths { get; set; }

    public bool IsCancelled { get; set; }

    public bool IsManual { get; set; }

    public string Comment { get; set; }

    public string DeviceName { get; set; }
  }
}
