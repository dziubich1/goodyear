﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.Operations.Measurements;
using Kalisto.Backend.Operations.Measurements.Commands.Interfaces;
using Kalisto.Hmi.Core;
using Kalisto.Hmi.Core.Commands;
using Kalisto.Hmi.Core.Services;
using Kalisto.Hmi.Dialogs.Common.Configuration;
using Kalisto.Hmi.Dialogs.Common.Enum;
using Kalisto.Hmi.Dialogs.Measurements.Dialogs;
using Kalisto.Hmi.Dialogs.Measurements.Models;
using Kalisto.Hmi.Dialogs.Measurements.Services;
using MahApps.Metro.Controls.Dialogs;
using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;
using Kalisto.Configuration.Permissions;
using Kalisto.Localization;
using Kalisto.Localization.Messages;

namespace Kalisto.Hmi.Dialogs.Measurements.ViewModels
{
    [AddINotifyPropertyChangedInterface]
    public class MeasurementsViewModel : DataGridViewModel<MeasurementModel>
    {
        private readonly ILocalizer<Messages> _localizer;

        private readonly IWeighingReportService _weighingReportService;

        private readonly IMeasurementService _measurementService;

        private readonly IUserPreferencesService _userPreferencesService;
        public override string DialogTitleKey => "Measurements";

        public ObservableCollectionEx<MeasurementModel> MeasurementItems { get; set; }

        public ICommand PrintWeighingReceiptPrintoutCommand { get; set; }

        public ICommand EditItemCommand { get; set; }

        public ICommand CancelItemCommand { get; set; }

        public ICommand SyncItemsCommand { get; set; }

        public ICommand DeleteItemCommand { get; set; }

        public MeasurementModel SelectedMeasurement { get; set; }

        public DateTime FromDate { get; set; }

        public DateTime ToDate { get; set; }

        protected override Type[] AssociatedCommandTypes => new[] { typeof(IMeasurementCommand) };

        public MeasurementsViewModel(IDialogService dialogService, IAuthorizationService authorizationService,
          IWeighingReportService weighingReportService,
          IMeasurementService measurementService, IUserPreferencesService userPreferencesService)
          : base(dialogService, authorizationService)
        {
            _localizer = new LocalizationManager<Messages>();
            _weighingReportService = weighingReportService;
            _measurementService = measurementService;
            _userPreferencesService = userPreferencesService;

            EditItemCommand = new DelegateCommand(EditItem);
            CancelItemCommand = new DelegateCommand(CancelMeasurement);
            SyncItemsCommand = new DelegateCommand(SyncMeasurements);
            DeleteItemCommand = new DelegateCommand(DeleteMeasurement);

            FromDate = DateTime.Today.AddDays(-3);
            ToDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);
            MeasurementItems = new ObservableCollectionEx<MeasurementModel>();
            PrintWeighingReceiptPrintoutCommand = new DelegateCommand(PrintWeighingReceiptPrintout);
        }

        protected override ObservableCollectionEx<MeasurementModel> GetItemsSource()
        {
            return MeasurementItems;
        }

        protected override string GetEditRuleName()
        {
            return PermissionRules.UpdateWeighingRule;
        }

        public override async Task RefreshDataAsync()
        {
            await base.RefreshDataAsync();

            await LoadMeasurements();
        }

        protected override async void EditItem(object item)
        {
            base.EditItem(item);
            var selectedMeasurement = (item as MeasurementModel) ?? SelectedMeasurement;
            if (selectedMeasurement == null)
            {
                await DialogService.ShowMessageAsync(_localizer.Localize(() => Messages.Error),
                  _localizer.Localize(() => Messages.SelectMeasurementToUpdate), MessageDialogType.Ok);
                return;
            }

            await DialogService.ShowModalDialogAsync<EditWeighingModalDialogViewModel>(vm =>
            {
                vm.DialogNameKey = "EditMeasurement";
                vm.ValidateOnLoad = true;
                vm.MeasurementId = selectedMeasurement.Id;
                vm.MeasurementNumber = selectedMeasurement.Number;
                vm.MeasurementDate1 = selectedMeasurement.Date1;
                vm.MeasurementDate2 = selectedMeasurement.Date2;
                vm.NetWeightValue = selectedMeasurement.NetWeight.DisplayValueUnitless;
                vm.Tare = selectedMeasurement.Tare.DisplayValueUnitless;
                vm.DeclaredWeightValue = selectedMeasurement.DeclaredWeight.DisplayValueUnitless;
                vm.Cargo = selectedMeasurement.Cargo;
                vm.Driver = selectedMeasurement.Driver;
                vm.Vehicle = selectedMeasurement.Vehicle;
                vm.Contractor = selectedMeasurement.Contractor;
                vm.SemiTrailer = selectedMeasurement.SemiTrailer;
                vm.Comment = selectedMeasurement.Comment;
                vm.IsCancelled = selectedMeasurement.IsCancelled;
                vm.Measurement1Value = selectedMeasurement.Measurement1.DisplayValueUnitless;
                vm.Measurement2Value = selectedMeasurement.Measurement2.DisplayValueUnitless;
                vm.SelectedMeasurementClass = (MeasurementClass?)selectedMeasurement.Class.Enum;
                vm.SelectedMeasurementType = (MeasurementType)selectedMeasurement.Type.Enum;
                vm.FirstPhotoPaths = selectedMeasurement.FirstPhotoPaths;
                vm.SecondPhotoPaths = selectedMeasurement.SecondPhotoPaths;
            },
            async vm =>
            {
                if (string.IsNullOrEmpty(vm.Measurement1Value))
                    vm.Measurement1Value = 0.ToString();
                if (string.IsNullOrEmpty(vm.Measurement2Value) && vm.IsDoubleMeasurementSecond)
                    vm.Measurement2Value = 0.ToString();
                if (string.IsNullOrEmpty(vm.Tare))
                    vm.Tare = 0.ToString();
                if (string.IsNullOrEmpty(vm.NetWeightValue))
                    vm.NetWeightValue = 0.ToString();

                var result = await _measurementService.UpdateMeasurementAsync(
            vm.MeasurementId, vm.MeasurementDate1, vm.MeasurementDate2,
            vm.SelectedMeasurementClass,
            vm.Measurement1ValueUom, vm.Measurement2ValueUom,
            vm.DeclaredWeightValueUom, vm.NetWeightValueUom,
            vm.TareUom, vm.WeighingMode, vm.Comment,
            vm.Driver, vm.Contractor, vm.Cargo, vm.Vehicle,
            vm.SelectedSemiTrailer, vm.IsCancelled);
                return result.Succeeded;
            });
        }

        public async void OnFromDateChanged()
        {
            if (!ViewLoaded)
                return;

            await LoadMeasurements();
        }

        public async void OnToDateChanged()
        {
            if (!ViewLoaded)
                return;

            await LoadMeasurements();
        }

        private async Task LoadMeasurements()
        {
            var result = await _measurementService.GetMeasurements(FromDate, ToDate);
            if (!result.Succeeded)
                return;

            MeasurementItems.Clear();
            MeasurementItems.AddRange(result.Response);
        }

        private async void PrintWeighingReceiptPrintout(object obj)
        {
            if (SelectedMeasurement == null)
            {
                await DialogService.ShowMessageAsync(_localizer.Localize(() => Messages.Error),
                  _localizer.Localize(() => Messages.SelectMeasurement), MessageDialogType.Ok);
                return;
            }

            var photos = new List<MeasurementPhotoDto>();
            var downloadPhotos = _userPreferencesService.Configuration.IncludeImagesOnPrintouts;
            if (downloadPhotos == YesNo.Yes)
            {
                var photoResult = await _measurementService.GetMeasurementPhotosById(SelectedMeasurement.Id);
                if (photoResult.Succeeded)
                    photos.AddRange(photoResult.Response);
            }

            var companyData = await _measurementService.GetCompanyInfoAsync();

            await _weighingReportService.PrintWeighingReceiptAsync(SelectedMeasurement, photos, companyData?.Response, _userPreferencesService.Configuration.DoubleCopy == YesNo.Yes);
        }

        private async void CancelMeasurement(object obj)
        {
            var selectedMeasurement = (obj as MeasurementModel) ?? SelectedMeasurement;
            if (selectedMeasurement == null)
            {
                await DialogService.ShowMessageAsync(_localizer.Localize(() => Messages.Error),
                  _localizer.Localize(() => Messages.SelectMeasurement), MessageDialogType.Ok);
                return;
            }

            var dlgResult = await DialogService.ShowMessageAsync(_localizer.Localize(() => Messages.Attention),
              _localizer.Localize(() => Messages.CancelMeasurementQuestion), MessageDialogType.YesNo);

            if (dlgResult != MessageDialogResult.Affirmative)
            {
                return;
            }

            await _measurementService.CancelMeasurementAsync(selectedMeasurement.Id);
        }

        private async void SyncMeasurements(object obj)
        {
            await LoadMeasurements();
        }

        private async void DeleteMeasurement(object obj)
        {
            if (SelectedMeasurement == null)
            {
                await DialogService.ShowMessageAsync(_localizer.Localize(() => Messages.Error),
                  _localizer.Localize(() => Messages.SelectMeasurementToDelete), MessageDialogType.Ok);
                return;
            }

            var dlgResult = await DialogService.ShowMessageAsync(_localizer.Localize(() => Messages.Attention),
              _localizer.Localize(() => Messages.DeleteMeasurementQuestion),
              MessageDialogType.YesNo);

            if (dlgResult != MessageDialogResult.Affirmative)
                return;

            await _measurementService.DeleteMeasurementAsync(SelectedMeasurement.Id);
        }
    }
}
