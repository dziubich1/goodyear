﻿using System;
using System.Collections.Generic;
using System.Linq;
using Kalisto.Hmi.Dialogs.Measurements.Models;
using System.Threading.Tasks;
using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.Operations.Measurements;
using Kalisto.Configuration.CompanyInfo;
using Kalisto.Hmi.Dialogs.Common;
using Kalisto.Hmi.Dialogs.Common.Configuration;
using Kalisto.Hmi.Dialogs.Common.Enum;
using Kalisto.Hmi.Dialogs.Common.Models;
using Kalisto.Localization;
using Kalisto.Printing;
using Kalisto.Printing.WeighingList;
using Kalisto.Printing.WeighingReceipt;
using System.Drawing;
using System.Resources;
using Kalisto.Core;
using Kalisto.Hmi.Core.Services;

namespace Kalisto.Hmi.Dialogs.Measurements.Services
{
    public interface IWeighingReportService
    {
        Task PrintWeighingReceiptAsync(MeasurementModel model, List<MeasurementPhotoDto> photos, CompanyInfoConfiguration companyInfo, bool doubleCopy);

        Task PrintWeighingListAsync(List<WeighingModel> model, UnitOfMeasurement netSum, LocalizedDate fromDate,
          LocalizedDate toDate);
    }

    public class WeighingReportService : IWeighingReportService
    {
        private readonly IPrintoutManager _printoutManager;

        private readonly INumberingService _numberingService;

        private readonly IUserPreferencesService _userPreferencesService;

        private readonly ILocalizer<Localization.Common.Common> _localizer;

        private readonly IClientLogoService _clientLogoService;

        public WeighingReportService(IPrintoutManager printoutManager, INumberingService numberingService, IUserPreferencesService userPreferencesService, IClientLogoService clientLogoService)
        { 
            _printoutManager = printoutManager;
            _numberingService = numberingService;
            _userPreferencesService = userPreferencesService;
            _localizer = new LocalizationManager<Localization.Common.Common>();
            _clientLogoService = clientLogoService;
        }

        public async Task PrintWeighingListAsync(List<WeighingModel> model, UnitOfMeasurement netSum, LocalizedDate fromDate, LocalizedDate toDate)
        {
            var list = model.Select(c => new WeighingListItemDto
            {
                Id = c.OrdinalId,
                Contractor = c.Contractor,
                Cargo = c.Cargo,
                Driver = c.Driver,
                MeasurementDate = c.WeighingDate.LocalizedDateValue,
                Gross = decimal.Parse(c.Gross.DisplayValueUnitless),
                Net = decimal.Parse(c.Net.DisplayValueUnitless),
                Car = c.Vehicle,
                Index = c.OrdinalId
            }).ToList();

            var printout = new WeighingListDto
            {
                Company = "", // todo: implement later
                EvidenceList = list,
                FromDateFormatted = fromDate.LocalizedDateValue,
                ToDateFormatted = toDate.LocalizedDateValue,
                NetSumFormatted = netSum.DisplayValue
            };

            _printoutManager.Print(new WeighingListPrintout(printout));

            await Task.FromResult(true);
        }

        public async Task PrintWeighingReceiptAsync(MeasurementModel model, List<MeasurementPhotoDto> photos, CompanyInfoConfiguration companyInfo, bool doubleCopy)
        {
            int? year = null;
            if (model.Date2?.Value != null)
                year = model.Date2.Value.Value.Year;
            else if (model.Date1?.Value != null)
                year = model.Date1.Value.Value.Year;

            if (year == null)
                throw new InvalidOperationException();

            var companyAddress = string.Empty;
            if (companyInfo != null)
            {
                if (!string.IsNullOrEmpty(companyInfo.Name))
                    companyAddress += $"{companyInfo.Name}\n";
                if (!string.IsNullOrEmpty(companyInfo.Street))
                    companyAddress += $"{companyInfo.Street}\n";
                if (!string.IsNullOrEmpty(companyInfo.Postcode))
                    companyAddress += $"{companyInfo.Postcode} ";
                if (!string.IsNullOrEmpty(companyInfo.City))
                    companyAddress += $"{companyInfo.City}\n";
                if (!string.IsNullOrEmpty(companyInfo.TaxNumber))
                    companyAddress += $"{_localizer.Localize(() => Localization.Common.Common.TaxIdLabel)} {companyInfo.TaxNumber}\n";
                if (!string.IsNullOrEmpty(companyInfo.AdditionalInfo))
                    companyAddress += $"{companyInfo.AdditionalInfo}\n";
            }
            var receieptLogo = _clientLogoService.LoadLogo(LogoType.receipt);
            var number = await _numberingService.GetReceiptNumberAsync(model.Id, year.Value, (MeasurementClass)(model.Class?.Enum ?? MeasurementClass.Service));

            var printout = new WeighingReceiptDto
            {
                Contractor = model.Contractor != null ? GetContractorData(model.Contractor.RawData as Contractor) : string.Empty,
                Driver = model.Driver?.DisplayValue,
                Header = number,
                Operator = model.Operator?.DisplayValue,
                Car = model.Vehicle?.DisplayValue,
                MeasurementDate = model.Date2 != null
          ? model.Date2.LocalizedDateValue
          : model.Date1.LocalizedDateValue,
                FirstMeasurementDate = model.Date1?.LocalizedDateValue,
                SecondMeasurementDate = model.Date2?.LocalizedDateValue,
                Comment = model.Comment,
                CompanyAddress = companyAddress,
                WeightEntry = new WeightReceiptEntryDto
                {
                    Cargo = model.Cargo?.DisplayValue,
                    FirstMeasurement = model.Measurement1.DisplayValue,
                    SecondMeasurement = model.Measurement2.DisplayValue,
                    Tare = model.Tare.DisplayValue,
                    Gross = GetGrossValueString(model),
                    Net = model.NetWeight.DisplayValue
                },
                CompanyLogo = receieptLogo.ToBase64String(),
                FirstMeasurementPhoto1 = photos?.Where(c => c.Number == DoubleMeasurementNumber.First).ElementAtOrDefault(0)?.PhotoBase64String,
                FirstMeasurementPhoto2 = photos?.Where(c => c.Number == DoubleMeasurementNumber.First).ElementAtOrDefault(1)?.PhotoBase64String,
                FirstMeasurementPhoto3 = photos?.Where(c => c.Number == DoubleMeasurementNumber.First).ElementAtOrDefault(2)?.PhotoBase64String,
                SecondMeasurementPhoto1 = photos?.Where(c => c.Number == DoubleMeasurementNumber.Second).ElementAtOrDefault(0)?.PhotoBase64String,
                SecondMeasurementPhoto2 = photos?.Where(c => c.Number == DoubleMeasurementNumber.Second).ElementAtOrDefault(1)?.PhotoBase64String,
                SecondMeasurementPhoto3 = photos?.Where(c => c.Number == DoubleMeasurementNumber.Second).ElementAtOrDefault(2)?.PhotoBase64String
            };

            _printoutManager.Print(new WeightReceiptPrintout(printout, doubleCopy: doubleCopy,
              includeImages: _userPreferencesService.Configuration.IncludeImagesOnPrintouts == YesNo.Yes));
        }

        private string GetGrossValueString(MeasurementModel model)
        {
            if (model == null)
                return string.Empty;

            if (model.Measurement2?.OriginalValue == null || model.Measurement1.OriginalValue > model.Measurement2.OriginalValue)
                return model.Measurement1.DisplayValue;

            return model.Measurement2.DisplayValue;
        }

        private string GetContractorData(Contractor contractor)
        {
            if (contractor == null)
                return string.Empty;

            var data = string.Empty;
            if (!string.IsNullOrEmpty(contractor.Name))
                data += $"{contractor.Name}\n";

            if (!string.IsNullOrEmpty(contractor.Address))
                data += $"{contractor.Address}\n";

            if (!string.IsNullOrEmpty(contractor.City))
                data += $"{contractor.PostalCode} {contractor.City}\n";

            if (!string.IsNullOrEmpty(contractor.Name))
                data += $"{_localizer.Localize(() => Localization.Common.Common.TaxIdLabel)} {contractor.TaxId}";

            return data;
        }
    }
}
