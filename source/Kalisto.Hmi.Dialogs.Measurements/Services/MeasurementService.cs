﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.Operations.Measurements;
using Kalisto.Backend.Operations.Measurements.Commands;
using Kalisto.Backend.Operations.Measurements.Queries;
using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Hmi.Core.Communication;
using Kalisto.Hmi.Core.Controls;
using Kalisto.Hmi.Core.Services;
using Kalisto.Hmi.Dialogs.Common;
using Kalisto.Hmi.Dialogs.Common.Configuration;
using Kalisto.Hmi.Dialogs.Common.Models;
using Kalisto.Hmi.Dialogs.Measurements.Models;
using Kalisto.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Kalisto.Backend.Operations.Shell.Queries;
using Kalisto.Configuration.CompanyInfo;
using Kalisto.Hmi.Dialogs.Common.Services;

namespace Kalisto.Hmi.Dialogs.Measurements.Services
{
  public interface IMeasurementService
  {
    Task<RequestResult<List<MeasurementModel>>> GetMeasurements(DateTime from, DateTime to);

    Task<RequestResult<List<MeasurementModel>>> GetNotCompletedMeasurementsAsync();

    Task<RequestResult<long>> CancelMeasurementAsync(long id);

    Task<RequestResult<long>> DeleteMeasurementAsync(long id);

    Task<RequestResult<long>> GetNextMeasurementNumberAsync();

    Task<RequestResult<MeasurementModel>> GetMeasurementByIdAsync(long id);

    Task<RequestResult<MeasurementModel>> GetMeasurementByQrCodeAsync(string QRCode);

    Task<RequestResult<CompanyInfoConfiguration>> GetCompanyInfoAsync();

    Task<RequestResult<long>> CreateSingleMeasurementAsync(MeasurementClass measClass,
      UnitOfMeasurement measurement, DateTime measurementDate, UnitOfMeasurement declaredWeight,
      UnitOfMeasurement tare, WeighingMode mode, string comment,
      ReferenceModel driver, ReferenceModel contractor,
      ReferenceModel cargo, ReferenceModel vehicle, ReferenceModel semiTrailer, int deviceId);

    Task<RequestResult<long>> CreateDoubleMeasurementAsync(
      UnitOfMeasurement measurement, DateTime measurementDate, UnitOfMeasurement declaredWeight, WeighingMode mode, string comment,
      ReferenceModel driver, ReferenceModel contractor,
      ReferenceModel cargo, ReferenceModel vehicle, ReferenceModel semiTrailer, int deviceId);

    Task<RequestResult<long>> CreateDoubleMeasurementWithQRCodeAsync(
      UnitOfMeasurement measurement, DateTime measurementDate, UnitOfMeasurement declaredWeight, WeighingMode mode, string comment,
      ReferenceModel driver, ReferenceModel contractor,
      ReferenceModel cargo, ReferenceModel vehicle, ReferenceModel semiTrailer, int deviceId, string QRCode);



    Task<RequestResult<long>> CompleteDoubleMeasurementAsync(long measurementId, long measurementNumber,
      MeasurementClass measClass, UnitOfMeasurement measurement, DateTime measurementDate, UnitOfMeasurement declaredWeight, string comment,
      ReferenceModel driver, ReferenceModel contractor,
      ReferenceModel cargo, ReferenceModel vehicle, ReferenceModel semiTrailer, int deviceId, WeighingMode weighingMode);

    Task<RequestResult<List<SemiTrailer>>> GetAllSemiTrailersAsync();

    Task<RequestResult<long>> UpdateMeasurementAsync(long measurementId,
      LocalizedDate measurementDate1, LocalizedDate measurementDate2,
      MeasurementClass? measClass, UnitOfMeasurement measurement1,
      UnitOfMeasurement measurement2, UnitOfMeasurement declaredWeight, UnitOfMeasurement netWeight,
      UnitOfMeasurement tare, WeighingMode mode, string comment,
      ReferenceModel driver, ReferenceModel contractor, ReferenceModel cargo,
      ReferenceModel vehicle, ReferenceModel semiTrailer,
      bool isCancelled);

    Task<RequestResult<List<MeasurementPhotoDto>>> GetMeasurementPhotosById(long measurementId);

    bool IsMeasurementCompleted(MeasurementModel measurement);
  }

  public class MeasurementService : BackendOperationService, IMeasurementService
  {
    private readonly IUnitTypeService _unitTypeService;
    private readonly IUserPreferencesService _userPreferencesService;
    private readonly IWeighingDeviceService _weighingDeviceService;
    private readonly ILocalizer<Localization.Common.Common> _localizer;

    public MeasurementService(ClientInfo clientInfo, IMessageBuilder messageBuilder,
      IFrontendMessageBridge messageBridge, IDialogService dialogService, 
      IUnitTypeService unitTypeService, IUserPreferencesService userPreferencesService, IWeighingDeviceService weighingDeviceService)
      : base(clientInfo, messageBuilder, messageBridge, dialogService)
    {
      _unitTypeService = unitTypeService;
      _userPreferencesService = userPreferencesService;
      _weighingDeviceService = weighingDeviceService;
      _localizer = new LocalizationManager<Localization.Common.Common>(); ;
    }

    public async Task<RequestResult<List<MeasurementModel>>> GetMeasurements(DateTime from, DateTime to)
    {
      var message = MessageBuilder.CreateNew<GetMeasurementsQuery>()
        .BasedOn(ClientInfo)
        .WithProperty(c => c.DateFromUtc, from.ToUniversalTime())
        .WithProperty(c => c.DateToUtc, to.ToUniversalTime())
        .Build();

      var result = await MessageBridge.SendMessageWithResultAsync<List<Measurement>>(message);
      var configuration = await _weighingDeviceService.GetWeighingDevicesAsync();
      return new RequestResult<List<MeasurementModel>>
      {
        Succeeded = result?.Succeeded ?? false,
        Response = result?.Response?.Select(meas => new MeasurementModel
        {
          Id = meas.Id,
          Number = meas.Number,
          Type = new LocalizedEnum { Enum = meas.Type },
          Class = new LocalizedEnum { Enum = meas.Class },
          Date1 = new LocalizedDate(meas.Date1Utc.ToLocalTime()),
          Date2 = meas.Date2Utc != null ? new LocalizedDate(meas.Date2Utc.Value.ToLocalTime()) : null,
          Measurement1 = new UnitOfMeasurement(meas.Measurement1, _unitTypeService),
          Measurement2 = new UnitOfMeasurement(meas.Measurement2, _unitTypeService),
          DeclaredWeight = new UnitOfMeasurement(meas.DeclaredWeight, _unitTypeService),
          NetWeight = new UnitOfMeasurement(meas.NetWeight, _unitTypeService),
          Tare = new UnitOfMeasurement(meas.Tare, _unitTypeService),
          Driver = meas.Driver != null
            ? new ReferenceModel
            {
              Id = meas.Driver.Id,
              DisplayValue = meas.Driver.Identifier
            }
            : null,
          SemiTrailer = meas.SemiTrailer != null
            ? new ReferenceModel
            {
              Id = meas.SemiTrailer.Id,
              DisplayValue = meas.SemiTrailer.Number
            }
            : null,
          Cargo = meas.Cargo != null
            ? new ReferenceModel
            {
              Id = meas.Cargo.Id,
              DisplayValue = meas.Cargo.Name
            }
            : null,
          Contractor = meas.Contractor != null
            ? new ReferenceModel
            {
              Id = meas.Contractor.Id,
              DisplayValue = meas.Contractor.Name,
              RawData = meas.Contractor
            }
            : null,
          Vehicle = meas.Vehicle != null
            ? new ReferenceModel
            {
              Id = meas.Vehicle.Id,
              DisplayValue = meas.Vehicle.PlateNumber
            }
            : null,
          Operator = meas.Operator != null
            ? new ReferenceModel
            {
              Id = meas.Operator.Id,
              DisplayValue = meas.Operator.Name
            }
            : null,
          FirstPhotoPaths = new List<string>
          {
            meas.FirstPhoto1RelPath, meas.FirstPhoto2RelPath, meas.FirstPhoto3RelPath
          }.Where(s => !string.IsNullOrWhiteSpace(s)).Distinct().ToList(),
          SecondPhotoPaths = new List<string>
          {
            meas.SecondPhoto1RelPath, meas.SecondPhoto2RelPath, meas.SecondPhoto3RelPath
          }.Where(s => !string.IsNullOrWhiteSpace(s)).Distinct().ToList(),
          Comment = meas.Comment,
          IsCancelled = meas.IsCancelled,
          IsManual = meas.IsManual,
          QRCode = meas.QRCode,
          DeviceName = GetDeviceNameFromWeightMeterConfiguration(configuration.FirstOrDefault(d => d.Id == meas.DeviceId)),
        }).ToList() ?? new List<MeasurementModel>()
      };
    }

    public async Task<RequestResult<long>> CancelMeasurementAsync(long id)
    {
      var message = MessageBuilder.CreateNew<CancelMeasurementCommand>()
        .BasedOn(ClientInfo)
        .WithProperty(c => c.MeasurementId, id)
        .Build();

      var result = await MessageBridge.SendMessageWithResultAsync<long>(message);
      return result ?? RequestResult<long>.Default;
    }

    public async Task<RequestResult<long>> DeleteMeasurementAsync(long id)
    {
      var message = MessageBuilder.CreateNew<DeleteMeasurementByIdCommand>()
        .BasedOn(ClientInfo)
        .WithProperty(c => c.MeasurementId, id)
        .Build();

      var result = await MessageBridge.SendMessageWithResultAsync<long>(message);
      return result ?? RequestResult<long>.Default;
    }

    public async Task<RequestResult<List<MeasurementModel>>> GetNotCompletedMeasurementsAsync()
    {
      var message = MessageBuilder.CreateNew<GetNotCompletedMeasurementsQuery>()
        .BasedOn(ClientInfo)
        .Build();

      var result = await MessageBridge.SendMessageWithResultAsync<List<Measurement>>(message);

      return new RequestResult<List<MeasurementModel>>
      {
        Succeeded = result?.Succeeded ?? false,
        Response = result?.Response?.Select(meas => new MeasurementModel
        {
          Id = meas.Id,
          Number = meas.Number,
          Type = new LocalizedEnum { Enum = meas.Type },
          Class = new LocalizedEnum { Enum = meas.Class },
          Date1 = new LocalizedDate(meas.Date1Utc.ToLocalTime()),
          Date2 = meas.Date2Utc != null ? new LocalizedDate(meas.Date2Utc.Value.ToLocalTime()) : null,
          Measurement1 = new UnitOfMeasurement(meas.Measurement1, _unitTypeService),
          Measurement2 = new UnitOfMeasurement(meas.Measurement2, _unitTypeService),
          DeclaredWeight = new UnitOfMeasurement(meas.DeclaredWeight, _unitTypeService),
          NetWeight = new UnitOfMeasurement(meas.NetWeight, _unitTypeService),
          Tare = new UnitOfMeasurement(meas.Tare, _unitTypeService),
          Driver = meas.Driver != null
            ? new ReferenceModel
            {
              Id = meas.Driver.Id,
              DisplayValue = meas.Driver.Identifier
            } : null,
          SemiTrailer = meas.SemiTrailer != null
            ? new ReferenceModel
            {
              Id = meas.SemiTrailer.Id,
              DisplayValue = meas.SemiTrailer.Number
            } : null,
          Cargo = meas.Cargo != null
            ? new ReferenceModel
            {
              Id = meas.Cargo.Id,
              DisplayValue = meas.Cargo.Name
            } : null,
          Contractor = meas.Contractor != null
            ? new ReferenceModel
            {
              Id = meas.Contractor.Id,
              DisplayValue = meas.Contractor.Name,
              RawData = meas.Contractor
            }
            : null,
          Vehicle = meas.Vehicle != null
            ? new ReferenceModel
            {
              Id = meas.Vehicle.Id,
              DisplayValue = meas.Vehicle.PlateNumber
            } : null,
          Operator = meas.Operator != null
            ? new ReferenceModel
            {
              Id = meas.Operator.Id,
              DisplayValue = meas.Operator.Name
            } : null,
          FirstPhotoPaths = new List<string>
          {
            meas.FirstPhoto1RelPath, meas.FirstPhoto2RelPath, meas.FirstPhoto3RelPath
          }.Where(s => !string.IsNullOrWhiteSpace(s)).Distinct().ToList(),
          SecondPhotoPaths = new List<string>
          {
            meas.SecondPhoto1RelPath, meas.SecondPhoto2RelPath, meas.SecondPhoto3RelPath
          }.Where(s => !string.IsNullOrWhiteSpace(s)).Distinct().ToList(),
          Comment = meas.Comment,
          IsManual = meas.IsManual
        }).ToList() ?? new List<MeasurementModel>()
      };
    }

    public async Task<RequestResult<long>> GetNextMeasurementNumberAsync()
    {
      var message = MessageBuilder.CreateNew<GetNextMeasurementNumberQuery>()
        .BasedOn(ClientInfo)
        .Build();

      var result = await MessageBridge.SendMessageWithResultAsync<long>(message);
      return result ?? RequestResult<long>.Default;
    }

    public async Task<RequestResult<long>> CreateSingleMeasurementAsync(MeasurementClass measClass,
      UnitOfMeasurement measurement, DateTime measurementDate, UnitOfMeasurement declaredWeight, UnitOfMeasurement tare, WeighingMode mode, string comment,
      ReferenceModel driver, ReferenceModel contractor,
      ReferenceModel cargo, ReferenceModel vehicle, ReferenceModel semiTrailer, int deviceId)
    {
      var message = MessageBuilder.CreateNew<CreateSingleMeasurementCommand>()
        .BasedOn(ClientInfo)
        .WithProperty(c => c.Class, measClass)
        .WithProperty(c => c.Measurement, measurement?.OriginalValue ?? 0)
        .WithProperty(c => c.MeasurementDateUtc, measurementDate.ToUniversalTime())
        .WithProperty(c => c.DeclaredWeight, declaredWeight?.OriginalValue)
        .WithProperty(c => c.IsManual, mode == WeighingMode.Manual)
        .WithProperty(c => c.Tare, tare?.OriginalValue ?? 0)
        .WithProperty(c => c.Comment, comment)
        .WithProperty(c => c.DriverId, driver?.Id)
        .WithProperty(c => c.ContractorId, contractor?.Id)
        .WithProperty(c => c.CargoId, cargo?.Id)
        .WithProperty(c => c.VehicleId, vehicle?.Id)
        .WithProperty(c => c.SemiTrailerNumber, semiTrailer?.DisplayValue)
        .WithProperty(c => c.DeviceId, deviceId)
        .Build();

      var result = await MessageBridge.SendMessageWithResultAsync<long>(message);
      return result ?? RequestResult<long>.Default;
    }

    public async Task<RequestResult<long>> CreateDoubleMeasurementAsync(
      UnitOfMeasurement measurement, DateTime measurementDate, UnitOfMeasurement declaredWeight, WeighingMode mode, string comment,
      ReferenceModel driver, ReferenceModel contractor,
      ReferenceModel cargo, ReferenceModel vehicle, ReferenceModel semiTrailer, int deviceId)
    {
      var message = MessageBuilder.CreateNew<CreateDoubleMeasurementCommand>()
        .BasedOn(ClientInfo)
        .WithProperty(c => c.Measurement, measurement?.OriginalValue ?? 0)
        .WithProperty(c => c.MeasurementDateUtc, measurementDate.ToUniversalTime())
        .WithProperty(c => c.DeclaredWeight, declaredWeight?.OriginalValue)
        .WithProperty(c => c.IsManual, mode == WeighingMode.Manual)
        .WithProperty(c => c.Comment, comment)
        .WithProperty(c => c.DriverId, driver?.Id)
        .WithProperty(c => c.ContractorId, contractor?.Id)
        .WithProperty(c => c.CargoId, cargo?.Id)
        .WithProperty(c => c.VehicleId, vehicle?.Id)
        .WithProperty(c => c.SemiTrailerNumber, semiTrailer?.DisplayValue)
        .WithProperty(c => c.DeviceId, deviceId)
        .Build();

      var result = await MessageBridge.SendMessageWithResultAsync<long>(message);
      return result ?? RequestResult<long>.Default;
    }

    public async Task<RequestResult<long>> CreateDoubleMeasurementWithQRCodeAsync(
      UnitOfMeasurement measurement, DateTime measurementDate, UnitOfMeasurement declaredWeight, WeighingMode mode, string comment,
      ReferenceModel driver, ReferenceModel contractor,
      ReferenceModel cargo, ReferenceModel vehicle, ReferenceModel semiTrailer, int deviceId, string QRCode)
    {
      var message = MessageBuilder.CreateNew<CreateDoubleMeasurementWithQrCodeCommand>()
        .BasedOn(ClientInfo)
        .WithProperty(c => c.Measurement, measurement?.OriginalValue ?? 0)
        .WithProperty(c => c.MeasurementDateUtc, measurementDate.ToUniversalTime())
        .WithProperty(c => c.DeclaredWeight, declaredWeight?.OriginalValue)
        .WithProperty(c => c.IsManual, mode == WeighingMode.Manual)
        .WithProperty(c => c.Comment, comment)
        .WithProperty(c => c.DriverId, driver?.Id)
        .WithProperty(c => c.ContractorId, contractor?.Id)
        .WithProperty(c => c.CargoId, cargo?.Id)
        .WithProperty(c => c.VehicleId, vehicle?.Id)
        .WithProperty(c => c.SemiTrailerNumber, semiTrailer?.DisplayValue)
        .WithProperty(c => c.DeviceId, deviceId)
        .WithProperty(c => c.QrCode, QRCode)
        .Build();

      var result = await MessageBridge.SendMessageWithResultAsync<long>(message);
      return result ?? RequestResult<long>.Default;
    }

    public async Task<RequestResult<long>> CompleteDoubleMeasurementAsync(long measurementId, long measurementNumber,
      MeasurementClass measClass, UnitOfMeasurement measurement, DateTime measurementDate, UnitOfMeasurement declaredWeight, string comment,
      ReferenceModel driver, ReferenceModel contractor,
      ReferenceModel cargo, ReferenceModel vehicle, ReferenceModel semiTrailer, int deviceId, WeighingMode weighingMode)
    {
      var message = MessageBuilder.CreateNew<CompleteDoubleMeasurementCommand>()
        .BasedOn(ClientInfo)
        .WithProperty(c => c.MeasurementId, measurementId)
        .WithProperty(c => c.MeasurementNumber, measurementNumber)
        .WithProperty(c => c.Class, measClass)
        .WithProperty(c => c.Measurement, measurement?.OriginalValue ?? 0)
        .WithProperty(c => c.MeasurementDateUtc, measurementDate.ToUniversalTime())
        .WithProperty(c => c.DeclaredWeight, declaredWeight?.OriginalValue)
        .WithProperty(c => c.WeighingThreshold, weighingMode == WeighingMode.Auto ? (decimal?)_userPreferencesService.Configuration.WeighingThreshold : null)
        .WithProperty(c => c.Comment, comment)
        .WithProperty(c => c.DriverId, driver?.Id)
        .WithProperty(c => c.ContractorId, contractor?.Id)
        .WithProperty(c => c.CargoId, cargo?.Id)
        .WithProperty(c => c.VehicleId, vehicle?.Id)
        .WithProperty(c => c.SemiTrailerNumber, semiTrailer?.DisplayValue)
        .WithProperty(c => c.DeviceId, deviceId)
        .WithProperty(c => c.IsManual, weighingMode == WeighingMode.Manual)
        .Build();

      var result = await MessageBridge.SendMessageWithResultAsync<long>(message);
      return result ?? RequestResult<long>.Default;
    }

    public async Task<RequestResult<List<SemiTrailer>>> GetAllSemiTrailersAsync()
    {
      var message = MessageBuilder.CreateNew<GetAllSemiTrailersQuery>()
        .BasedOn(ClientInfo)
        .Build();

      var result = await MessageBridge.SendMessageWithResultAsync<List<SemiTrailer>>(message);
      return result ?? RequestResult<List<SemiTrailer>>.Default;
    }

    public async Task<RequestResult<MeasurementModel>> GetMeasurementByIdAsync(long id)
    {
      var message = MessageBuilder.CreateNew<GetMeasurementByIdQuery>()
        .BasedOn(ClientInfo)
        .WithProperty(c => c.MeasurementId, id)
        .Build();

      var result = await MessageBridge.SendMessageWithResultAsync<Measurement>(message);
      var meas = result.Response;
      return new RequestResult<MeasurementModel>
      {
        Succeeded = result?.Succeeded ?? false,
        Response = meas == null ? null : new MeasurementModel
        {
          Id = meas.Id,
          Number = meas.Number,
          Type = new LocalizedEnum { Enum = meas.Type },
          Class = new LocalizedEnum { Enum = meas.Class },
          Date1 = new LocalizedDate(meas.Date1Utc.ToLocalTime()),
          Date2 = meas.Date2Utc != null ? new LocalizedDate(meas.Date2Utc.Value.ToLocalTime()) : null,
          Measurement1 = new UnitOfMeasurement(meas.Measurement1, _unitTypeService),
          Measurement2 = new UnitOfMeasurement(meas.Measurement2, _unitTypeService),
          DeclaredWeight = new UnitOfMeasurement(meas.DeclaredWeight, _unitTypeService),
          NetWeight = new UnitOfMeasurement(meas.NetWeight, _unitTypeService),
          Tare = new UnitOfMeasurement(meas.Tare, _unitTypeService),
          Driver = meas.Driver != null
            ? new ReferenceModel
            {
              Id = meas.Driver.Id,
              DisplayValue = meas.Driver.Identifier
            } : null,
          SemiTrailer = meas.SemiTrailer != null
            ? new ReferenceModel
            {
              Id = meas.SemiTrailer.Id,
              DisplayValue = meas.SemiTrailer.Number
            } : null,
          Cargo = meas.Cargo != null
            ? new ReferenceModel
            {
              Id = meas.Cargo.Id,
              DisplayValue = meas.Cargo.Name
            } : null,
          Contractor = meas.Contractor != null
            ? new ReferenceModel
            {
              Id = meas.Contractor.Id,
              DisplayValue = meas.Contractor.Name,
              RawData = meas.Contractor
            }
            : null,
          Vehicle = meas.Vehicle != null
            ? new ReferenceModel
            {
              Id = meas.Vehicle.Id,
              DisplayValue = meas.Vehicle.PlateNumber
            } : null,
          Operator = meas.Operator != null
            ? new ReferenceModel
            {
              Id = meas.Operator.Id,
              DisplayValue = meas.Operator.Name
            } : null,
          FirstPhotoPaths = new List<string>
          {
            meas.FirstPhoto1RelPath, meas.FirstPhoto2RelPath, meas.FirstPhoto3RelPath
          }.Where(s => !string.IsNullOrWhiteSpace(s)).Distinct().ToList(),
          SecondPhotoPaths = new List<string>
          {
            meas.SecondPhoto1RelPath, meas.SecondPhoto2RelPath, meas.SecondPhoto3RelPath
          }.Where(s => !string.IsNullOrWhiteSpace(s)).Distinct().ToList(),
          Comment = meas.Comment,
          IsManual = meas.IsManual,
          QRCode = meas.QRCode
        }
      };
    }

    public async Task<RequestResult<long>> UpdateMeasurementAsync(long measurementId,
      LocalizedDate measurementDate1, LocalizedDate measurementDate2,
      MeasurementClass? measClass, UnitOfMeasurement measurement1,
      UnitOfMeasurement measurement2, UnitOfMeasurement declaredWeight, UnitOfMeasurement netWeight,
      UnitOfMeasurement tare, WeighingMode mode, string comment,
      ReferenceModel driver, ReferenceModel contractor, ReferenceModel cargo,
      ReferenceModel vehicle, ReferenceModel semiTrailer,
      bool isCancelled)
    {
      var message = MessageBuilder.CreateNew<UpdateMeasurementCommand>()
        .BasedOn(ClientInfo)
        .WithProperty(m => m.MeasurementId, measurementId)
        .WithProperty(m => m.MeasurementDate1Utc, measurementDate1?.Value?.ToUniversalTime() ?? DateTime.UtcNow)
        .WithProperty(m => m.MeasurementDate2Utc, measurementDate2?.Value?.ToUniversalTime())
        .WithProperty(m => m.Measurement1, measurement1?.OriginalValue ?? 0)
        .WithProperty(m => m.Measurement2, measurement2?.OriginalValue)
        .WithProperty(m => m.Tare, tare?.OriginalValue ?? 0)
        .WithProperty(m => m.DeclaredWeight, declaredWeight?.OriginalValue)
        .WithProperty(m => m.NetWeight, netWeight?.OriginalValue ?? 0)
        .WithProperty(m => m.DriverId, driver?.Id)
        .WithProperty(m => m.ContractorId, contractor?.Id)
        .WithProperty(m => m.CargoId, cargo?.Id)
        .WithProperty(m => m.VehicleId, vehicle?.Id)
        .WithProperty(m => m.SemiTrailerNumber, semiTrailer?.DisplayValue)
        .WithProperty(m => m.Class, measClass)
        .WithProperty(m => m.Comment, comment)
        .WithProperty(m => m.IsCancelled, isCancelled)
        .Build();

      var result = await MessageBridge.SendMessageWithResultAsync<long>(message);
      return result ?? RequestResult<long>.Default;
    }

    public async Task<RequestResult<List<MeasurementPhotoDto>>> GetMeasurementPhotosById(long measurementId)
    {
      var message = MessageBuilder.CreateNew<GetMeasurementPhotosByIdQuery>()
        .BasedOn(ClientInfo)
        .WithProperty(c => c.MeasurementId, measurementId)
        .Build();

      var result = await MessageBridge.SendMessageWithResultAsync<List<MeasurementPhotoDto>>(message);
      return result ?? RequestResult<List<MeasurementPhotoDto>>.Default;
    }

    // TODO: move below method to CompanyInfoService
    public async Task<RequestResult<CompanyInfoConfiguration>> GetCompanyInfoAsync()
    {
      var message = MessageBuilder.CreateNew<GetCompanyInfoConfigurationQuery>()
        .BasedOn(ClientInfo)
        .Build();

      var result = await MessageBridge.SendMessageWithResultAsync<CompanyInfoConfiguration>(message);
      return result ?? RequestResult<CompanyInfoConfiguration>.Default;
    }

    public async Task<RequestResult<MeasurementModel>> GetMeasurementByQrCodeAsync(string qrCode)
    {
      var message = MessageBuilder.CreateNew<GetMeasurementByQrCodeQuery>()
      .BasedOn(ClientInfo)
      .WithProperty(c => c.QrCode, qrCode)
      .Build();

      var result = await MessageBridge.SendMessageWithResultAsync<Measurement>(message);
      var meas = result?.Response;
      return new RequestResult<MeasurementModel>
      {
        Succeeded = result?.Succeeded ?? false,
        Response = meas == null ? null : new MeasurementModel
        {
          Id = meas.Id,
          Number = meas.Number,
          Type = new LocalizedEnum { Enum = meas.Type },
          Class = new LocalizedEnum { Enum = meas.Class },
          Date1 = new LocalizedDate(meas.Date1Utc.ToLocalTime()),
          Date2 = meas.Date2Utc != null ? new LocalizedDate(meas.Date2Utc.Value.ToLocalTime()) : null,
          Measurement1 = new UnitOfMeasurement(meas.Measurement1, _unitTypeService),
          Measurement2 = new UnitOfMeasurement(meas.Measurement2, _unitTypeService),
          DeclaredWeight = new UnitOfMeasurement(meas.DeclaredWeight, _unitTypeService),
          NetWeight = new UnitOfMeasurement(meas.NetWeight, _unitTypeService),
          Tare = new UnitOfMeasurement(meas.Tare, _unitTypeService),
          Driver = meas.Driver != null
            ? new ReferenceModel
            {
              Id = meas.Driver.Id,
              DisplayValue = meas.Driver.Identifier
            } : null,
          SemiTrailer = meas.SemiTrailer != null
            ? new ReferenceModel
            {
              Id = meas.SemiTrailer.Id,
              DisplayValue = meas.SemiTrailer.Number
            } : null,
          Cargo = meas.Cargo != null
            ? new ReferenceModel
            {
              Id = meas.Cargo.Id,
              DisplayValue = meas.Cargo.Name
            } : null,
          Contractor = meas.Contractor != null
            ? new ReferenceModel
            {
              Id = meas.Contractor.Id,
              DisplayValue = meas.Contractor.Name,
              RawData = meas.Contractor
            }
            : null,
          Vehicle = meas.Vehicle != null
            ? new ReferenceModel
            {
              Id = meas.Vehicle.Id,
              DisplayValue = meas.Vehicle.PlateNumber
            } : null,
          Operator = meas.Operator != null
            ? new ReferenceModel
            {
              Id = meas.Operator.Id,
              DisplayValue = meas.Operator.Name
            } : null,
          FirstPhotoPaths = new List<string>
          {
            meas.FirstPhoto1RelPath, meas.FirstPhoto2RelPath, meas.FirstPhoto3RelPath
          }.Where(s => !string.IsNullOrWhiteSpace(s)).Distinct().ToList(),
          SecondPhotoPaths = new List<string>
          {
            meas.SecondPhoto1RelPath, meas.SecondPhoto2RelPath, meas.SecondPhoto3RelPath
          }.Where(s => !string.IsNullOrWhiteSpace(s)).Distinct().ToList(),
          Comment = meas.Comment,
          IsManual = meas.IsManual,
          QRCode = meas.QRCode
        }
      };
    }

    public bool IsMeasurementCompleted(MeasurementModel measurement)
    {
      return measurement.Measurement2.OriginalValue != null && measurement.Date2 != null;
    }

    private string GetDeviceNameFromWeightMeterConfiguration(WeighingDeviceInfo configuration)
    {
      return configuration != null ? configuration.Name : _localizer.Localize(() => Localization.Common.Common.UnknownDevice);
    }
  }
}
