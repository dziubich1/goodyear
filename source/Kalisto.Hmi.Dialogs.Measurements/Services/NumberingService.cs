﻿using Kalisto.Backend.Operations.Measurements.Queries;
using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Hmi.Core.Communication;
using Kalisto.Hmi.Core.Services;
using System;
using System.Threading.Tasks;
using Kalisto.Backend.DataAccess.Model;
using Kalisto.Localization;

namespace Kalisto.Hmi.Dialogs.Measurements.Services
{
  public interface INumberingService
  {
    Task<string> GetReceiptNumberAsync(long measurementId, int year, MeasurementClass measClass);
  }

  public class NumberingService : BackendOperationService, INumberingService
  {
    private readonly ILocalizer<Localization.Common.Common> _localizer;

    public NumberingService(ClientInfo clientInfo, IMessageBuilder messageBuilder, IFrontendMessageBridge messageBridge,
      IDialogService dialogService) : base(clientInfo, messageBuilder, messageBridge, dialogService)
    {
      _localizer = new LocalizationManager<Localization.Common.Common>();
    }

    public async Task<string> GetReceiptNumberAsync(long measurementId, int year, MeasurementClass measClass)
    {
      var numberBase = await GetSequenceNumberByDateAsync(measurementId, year, measClass);

      var classStatement = GetClassStatement(measClass);

      //var receiptPrintoutText = _localizer.Localize("ReceiptPrintoutText");
      var numberPrintoutText = _localizer.Localize("NumberPrintoutText");

      //if (numberBase < 1)
      //  return $"{receiptPrintoutText}";

      //return $"{receiptPrintoutText} {classStatement} {numberPrintoutText} {numberBase}/{year}";
      return $"{classStatement} {numberPrintoutText} {numberBase}/{year}";
    }

    private async Task<long> GetSequenceNumberByDateAsync(long measurementId, int year, MeasurementClass measClass)
    {
      var message = MessageBuilder.CreateNew<GetSequenceNumberByDateQuery>()
        .BasedOn(ClientInfo)
        .WithProperty(c => c.MeasurementId, measurementId)
        .WithProperty(c => c.Class, measClass)
        .WithProperty(c => c.Year, year)
        .Build();

      var result = await MessageBridge.SendMessageWithResultAsync<long>(message);
      if (!result.Succeeded)
        throw new InvalidOperationException();

      return result.Response;
    }

    private string GetClassStatement(MeasurementClass measClass)
    {
      switch (measClass)
      {
        case MeasurementClass.Income:
          return "PZ";
        case MeasurementClass.Outcome:
          return "WZ";
        default:
          return string.Empty;
      }
    }
  }
}
