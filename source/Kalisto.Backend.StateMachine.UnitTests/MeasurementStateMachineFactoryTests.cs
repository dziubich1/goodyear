﻿using Appccelerate.StateMachine;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using Appccelerate.StateMachine.Machine.Events;
using Kalisto.Backend.StateMachine.Payloads;

namespace Kalisto.Backend.StateMachine.UnitTests
{
  [TestClass]
  public class MeasurementStateMachineFactoryTests
  {
    // component under tests
    private MeasurementStateMachineFactory _stateMachineFactory;

    private IStateMachine<MeasurementState, MeasurementEvent> _stateMachine;

    private Mock<IMeasurementStateMachineEventHandler> _measurementSmEventHandlerMock;

    private MeasurementState? _currentState;

    [TestInitialize]
    public void Initialize()
    {
      _measurementSmEventHandlerMock = new Mock<IMeasurementStateMachineEventHandler>();
      _measurementSmEventHandlerMock.Setup(c => c.HandleInitialState(It.IsAny<StateMachinePayloadBase>())).Verifiable();
      _measurementSmEventHandlerMock.Setup(c => c.HandleTruckArriving(It.IsAny<StateMachinePayloadBase>())).Verifiable();
      _measurementSmEventHandlerMock.Setup(c => c.HandleTruckArrived(It.IsAny<StateMachinePayloadBase>())).Verifiable();
      _measurementSmEventHandlerMock.Setup(c => c.HandleMeasurementStarted(It.IsAny<StateMachinePayloadBase>())).Verifiable();
      _measurementSmEventHandlerMock.Setup(c => c.HandleMeasurementCompleted(It.IsAny<StateMachinePayloadBase>())).Verifiable();
      _measurementSmEventHandlerMock.Setup(c => c.HandleTruckLeft(It.IsAny<StateMachinePayloadBase>())).Verifiable();

      _stateMachineFactory = new MeasurementStateMachineFactory(_measurementSmEventHandlerMock.Object);
    }

    private void InitializeAndRun(MeasurementState initialState)
    {
      if (_stateMachine != null && _stateMachine.IsRunning)
        _stateMachine.Stop();

      _currentState = null;
      _stateMachine = _stateMachineFactory.Create(1); // dummy state machine id
      _stateMachine.TransitionCompleted += delegate (object sender,
        TransitionCompletedEventArgs<MeasurementState, MeasurementEvent> args)
      {
        _currentState = args.NewStateId;
      };

      _stateMachine.Initialize(initialState);
      _stateMachine.Start();

      _measurementSmEventHandlerMock.Invocations.Clear();
    }

    #region Initial state

    [TestMethod]
    public void Firing_IncreasingWeightLimitExceeded_ShouldTurnStateMachineInto_TruckArriving_WhenStateMachineIsInState_Initial()
    {
      // Setup
      var parameter = new StateMachinePayloadBase();

      // Arrange
      InitializeAndRun(MeasurementState.Initial);

      // Act
      _stateMachine.Fire(MeasurementEvent.IncreasingWeightLimitExceeded, parameter);

      // Assert
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArriving(parameter), Times.Once);
      _measurementSmEventHandlerMock.Verify(c => c.HandleInitialState(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArrived(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementStarted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementCompleted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckLeft(It.IsAny<StateMachinePayloadBase>()), Times.Never);
    }

    [TestMethod]
    public void Firing_CardScanned_ShouldTurnStateMachineInto_TruckArriving_WhenStateMachineIsInState_Initial()
    {
      // Setup
      var parameter = new StateMachinePayloadBase();

      // Arrange
      InitializeAndRun(MeasurementState.Initial);

      // Act
      _stateMachine.Fire(MeasurementEvent.CardScanned, parameter);

      // Assert
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementStarted(parameter), Times.Once);
      _measurementSmEventHandlerMock.Verify(c => c.HandleInitialState(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArrived(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArriving(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementCompleted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckLeft(It.IsAny<StateMachinePayloadBase>()), Times.Never);
    }

    [TestMethod]
    public void Firing_LightCurtainValidated_ShouldTurnStateMachineInto_TruckArrived_WhenStateMachineIsInState_Initial()
    {
      // Setup
      var parameter = new StateMachinePayloadBase();

      // Arrange
      InitializeAndRun(MeasurementState.Initial);

      // Act
      _stateMachine.Fire(MeasurementEvent.LightCurtainValidated, parameter);

      // Assert
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArrived(parameter), Times.Once);
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementStarted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleInitialState(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArriving(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementCompleted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckLeft(It.IsAny<StateMachinePayloadBase>()), Times.Never);
    }

    [TestMethod]
    public void Firing_MeasurementStored_ShouldNotAffectStateMachine_WhenStateMachineIsInState_Initial()
    {
      // Setup
      var parameter = Guid.NewGuid().ToString();

      // Arrange
      InitializeAndRun(MeasurementState.Initial);

      // Act
      _stateMachine.Fire(MeasurementEvent.MeasurementStored, parameter);

      // Assert
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementStarted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleInitialState(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArrived(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArriving(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementCompleted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckLeft(It.IsAny<StateMachinePayloadBase>()), Times.Never);
    }

    [TestMethod]
    public void Firing_DecreasingWeightLimitExceeded_ShouldNotAffectStateMachine_WhenStateMachineIsInState_Initial()
    {
      // Setup
      var parameter = Guid.NewGuid().ToString();

      // Arrange
      InitializeAndRun(MeasurementState.Initial);

      // Act
      _stateMachine.Fire(MeasurementEvent.DecreasingWeightLimitExceeded, parameter);

      // Assert
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementStarted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleInitialState(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArrived(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArriving(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementCompleted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckLeft(It.IsAny<StateMachinePayloadBase>()), Times.Never);
    }

    [TestMethod]
    public void Firing_Reset_ShouldNotAffectStateMachine_WhenStateMachineIsInState_Initial()
    {
      // Setup
      var parameter = Guid.NewGuid().ToString();

      // Arrange
      InitializeAndRun(MeasurementState.Initial);

      // Act
      _stateMachine.Fire(MeasurementEvent.Reset, parameter);

      // Assert
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementStarted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleInitialState(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArrived(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArriving(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementCompleted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckLeft(It.IsAny<StateMachinePayloadBase>()), Times.Never);
    }

    #endregion

    #region TruckArriving state

    [TestMethod]
    public void Firing_LightCurtainValidated_ShouldTurnStateMachineInto_TruckArrived_WhenStateMachineIsInState_TruckArriving()
    {
      // Setup
      var parameter = new StateMachinePayloadBase();

      // Arrange
      InitializeAndRun(MeasurementState.TruckArriving);

      // Act
      _stateMachine.Fire(MeasurementEvent.LightCurtainValidated, parameter);

      // Assert
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArrived(parameter), Times.Once);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArriving(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleInitialState(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementStarted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementCompleted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckLeft(It.IsAny<StateMachinePayloadBase>()), Times.Never);
    }

    [TestMethod]
    public void Firing_CardScanned_ShouldTurnStateMachineInto_MeasurementStarted_WhenStateMachineIsInState_TruckArriving()
    {
      // Setup
      var parameter = new StateMachinePayloadBase();

      // Arrange
      InitializeAndRun(MeasurementState.TruckArriving);

      // Act
      _stateMachine.Fire(MeasurementEvent.CardScanned, parameter);

      // Assert
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementStarted(parameter), Times.Once);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArrived(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArriving(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleInitialState(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementCompleted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckLeft(It.IsAny<StateMachinePayloadBase>()), Times.Never);
    }

    [TestMethod]
    public void Firing_MeasurementStored_ShouldNotAffectStateMachine_WhenStateMachineIsInState_TruckArriving()
    {
      // Setup
      var parameter = Guid.NewGuid().ToString();

      // Arrange
      InitializeAndRun(MeasurementState.TruckArriving);

      // Act
      _stateMachine.Fire(MeasurementEvent.MeasurementStored, parameter);

      // Assert
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementStarted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleInitialState(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArrived(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArriving(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementCompleted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckLeft(It.IsAny<StateMachinePayloadBase>()), Times.Never);
    }

    [TestMethod]
    public void Firing_DecreasingWeightLimitExceeded_ShouldNotAffectStateMachine_WhenStateMachineIsInState_TruckArriving()
    {
      // Setup
      var parameter = Guid.NewGuid().ToString();

      // Arrange
      InitializeAndRun(MeasurementState.TruckArriving);

      // Act
      _stateMachine.Fire(MeasurementEvent.DecreasingWeightLimitExceeded, parameter);

      // Assert
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementStarted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleInitialState(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArrived(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArriving(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementCompleted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckLeft(It.IsAny<StateMachinePayloadBase>()), Times.Never);
    }

    [TestMethod]
    public void Firing_Reset_ShouldNotAffectStateMachine_WhenStateMachineIsInState_TruckArriving()
    {
      // Setup
      var parameter = Guid.NewGuid().ToString();

      // Arrange
      InitializeAndRun(MeasurementState.TruckArriving);

      // Act
      _stateMachine.Fire(MeasurementEvent.Reset, parameter);

      // Assert
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementStarted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleInitialState(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArrived(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArriving(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementCompleted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckLeft(It.IsAny<StateMachinePayloadBase>()), Times.Never);
    }

    #endregion

    #region TruckArrived state

    [TestMethod]
    public void Firing_CardScanned_ShouldTurnStateMachineInto_MeasurementStarted_WhenStateMachineIsInState_TruckArrived()
    {
      // Setup
      var parameter = new StateMachinePayloadBase();

      // Arrange
      InitializeAndRun(MeasurementState.TruckArrived);

      // Act
      _stateMachine.Fire(MeasurementEvent.CardScanned, parameter);

      // Assert
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementStarted(parameter), Times.Once);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArrived(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArriving(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleInitialState(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementCompleted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckLeft(It.IsAny<StateMachinePayloadBase>()), Times.Never);
    }

    [TestMethod]
    public void Firing_IncreasingWeightLimitExceeded_ShouldNotAffectStateMachine_WhenStateMachineIsInState_TruckArrived()
    {
      // Setup
      var parameter = new StateMachinePayloadBase();

      // Arrange
      InitializeAndRun(MeasurementState.TruckArrived);

      // Act
      _stateMachine.Fire(MeasurementEvent.IncreasingWeightLimitExceeded, parameter);

      // Assert
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementStarted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArrived(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArriving(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleInitialState(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementCompleted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckLeft(It.IsAny<StateMachinePayloadBase>()), Times.Never);
    }

    [TestMethod]
    public void Firing_LightCurtainValidated_ShouldNotAffectStateMachine_WhenStateMachineIsInState_TruckArrived()
    {
      // Setup
      var parameter = Guid.NewGuid().ToString();

      // Arrange
      InitializeAndRun(MeasurementState.TruckArrived);

      // Act
      _stateMachine.Fire(MeasurementEvent.LightCurtainValidated, parameter);

      // Assert
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementStarted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArrived(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArriving(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleInitialState(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementCompleted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckLeft(It.IsAny<StateMachinePayloadBase>()), Times.Never);
    }

    [TestMethod]
    public void Firing_MeasurementStored_ShouldNotAffectStateMachine_WhenStateMachineIsInState_TruckArrived()
    {
      // Setup
      var parameter = Guid.NewGuid().ToString();

      // Arrange
      InitializeAndRun(MeasurementState.TruckArrived);

      // Act
      _stateMachine.Fire(MeasurementEvent.MeasurementStored, parameter);

      // Assert
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementStarted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArrived(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArriving(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleInitialState(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementCompleted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckLeft(It.IsAny<StateMachinePayloadBase>()), Times.Never);
    }

    [TestMethod]
    public void Firing_DecreasingWeightLimitExceeded_ShouldNotAffectStateMachine_WhenStateMachineIsInState_TruckArrived()
    {
      // Setup
      var parameter = Guid.NewGuid().ToString();

      // Arrange
      InitializeAndRun(MeasurementState.TruckArrived);

      // Act
      _stateMachine.Fire(MeasurementEvent.DecreasingWeightLimitExceeded, parameter);

      // Assert
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementStarted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArrived(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArriving(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleInitialState(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementCompleted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckLeft(It.IsAny<StateMachinePayloadBase>()), Times.Never);
    }

    [TestMethod]
    public void Firing_Reset_ShouldNotAffectStateMachine_WhenStateMachineIsInState_TruckArrived()
    {
      // Setup
      var parameter = Guid.NewGuid().ToString();

      // Arrange
      InitializeAndRun(MeasurementState.TruckArrived);

      // Act
      _stateMachine.Fire(MeasurementEvent.Reset, parameter);

      // Assert
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementStarted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArrived(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArriving(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleInitialState(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementCompleted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckLeft(It.IsAny<StateMachinePayloadBase>()), Times.Never);
    }

    #endregion

    #region MeasurementStarted state

    [TestMethod]
    public void Firing_MeasurementStored_ShouldTurnStateMachineInto_MeasurementCompleted_WhenStateMachineIsInState_MeasurementStarted()
    {
      // Setup
      var parameter = new StateMachinePayloadBase();

      // Arrange
      InitializeAndRun(MeasurementState.MeasurementStarted);

      // Act
      _stateMachine.Fire(MeasurementEvent.MeasurementStored, parameter);

      // Assert
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementCompleted(parameter), Times.Once);
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementStarted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArrived(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArriving(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleInitialState(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckLeft(It.IsAny<StateMachinePayloadBase>()), Times.Never);
    }

    [TestMethod]
    public void Firing_IncreasingWeightLimitExceeded_ShouldNotAffectStateMachine_WhenStateMachineIsInState_MeasurementStarted()
    {
      // Setup
      var parameter = Guid.NewGuid().ToString();

      // Arrange
      InitializeAndRun(MeasurementState.MeasurementStarted);

      // Act
      _stateMachine.Fire(MeasurementEvent.IncreasingWeightLimitExceeded, parameter);

      // Assert
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementCompleted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementStarted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArrived(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArriving(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleInitialState(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckLeft(It.IsAny<StateMachinePayloadBase>()), Times.Never);
    }

    [TestMethod]
    public void Firing_LightCurtainValidated_ShouldNotAffectStateMachine_WhenStateMachineIsInState_MeasurementStarted()
    {
      // Setup
      var parameter = Guid.NewGuid().ToString();

      // Arrange
      InitializeAndRun(MeasurementState.MeasurementStarted);

      // Act
      _stateMachine.Fire(MeasurementEvent.LightCurtainValidated, parameter);

      // Assert
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementCompleted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementStarted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArrived(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArriving(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleInitialState(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckLeft(It.IsAny<StateMachinePayloadBase>()), Times.Never);
    }

    [TestMethod]
    public void Firing_CardScanned_ShouldNotAffectStateMachine_WhenStateMachineIsInState_MeasurementStarted()
    {
      // Setup
      var parameter = Guid.NewGuid().ToString();

      // Arrange
      InitializeAndRun(MeasurementState.MeasurementStarted);

      // Act
      _stateMachine.Fire(MeasurementEvent.CardScanned, parameter);

      // Assert
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementCompleted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementStarted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArrived(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArriving(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleInitialState(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckLeft(It.IsAny<StateMachinePayloadBase>()), Times.Never);
    }

    [TestMethod]
    public void Firing_DecreasingWeightLimitExceeded_ShouldNotAffectStateMachine_WhenStateMachineIsInState_MeasurementStarted()
    {
      // Setup
      var parameter = Guid.NewGuid().ToString();

      // Arrange
      InitializeAndRun(MeasurementState.MeasurementStarted);

      // Act
      _stateMachine.Fire(MeasurementEvent.DecreasingWeightLimitExceeded, parameter);

      // Assert
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementCompleted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementStarted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArrived(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArriving(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleInitialState(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckLeft(It.IsAny<StateMachinePayloadBase>()), Times.Never);
    }

    [TestMethod]
    public void Firing_Reset_ShouldNotAffectStateMachine_WhenStateMachineIsInState_MeasurementStarted()
    {
      // Setup
      var parameter = Guid.NewGuid().ToString();

      // Arrange
      InitializeAndRun(MeasurementState.MeasurementStarted);

      // Act
      _stateMachine.Fire(MeasurementEvent.Reset, parameter);

      // Assert
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementCompleted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementStarted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArrived(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArriving(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleInitialState(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckLeft(It.IsAny<StateMachinePayloadBase>()), Times.Never);
    }

    #endregion

    #region MeasurementCompleted state

    [TestMethod]
    public void Firing_DecreasingWeightLimitExceeded_ShouldTurnStateMachineInto_TruckLeft_WhenStateMachineIsInState_MeasurementCompleted()
    {
      // Setup
      var parameter = new StateMachinePayloadBase();

      // Arrange
      InitializeAndRun(MeasurementState.MeasurementCompleted);

      // Act
      _stateMachine.Fire(MeasurementEvent.DecreasingWeightLimitExceeded, parameter);

      // Assert
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckLeft(parameter), Times.Once);
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementCompleted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementStarted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArrived(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArriving(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleInitialState(It.IsAny<StateMachinePayloadBase>()), Times.Never);
    }

    [TestMethod]
    public void Firing_IncreasingWeightLimitExceeded_ShouldNotAffectStateMachine_WhenStateMachineIsInState_MeasurementCompleted()
    {
      // Setup
      var parameter = Guid.NewGuid().ToString();

      // Arrange
      InitializeAndRun(MeasurementState.MeasurementCompleted);

      // Act
      _stateMachine.Fire(MeasurementEvent.IncreasingWeightLimitExceeded, parameter);

      // Assert
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementCompleted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementStarted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArrived(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArriving(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleInitialState(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckLeft(It.IsAny<StateMachinePayloadBase>()), Times.Never);
    }

    [TestMethod]
    public void Firing_LightCurtainValidated_ShouldNotAffectStateMachine_WhenStateMachineIsInState_MeasurementCompleted()
    {
      // Setup
      var parameter = Guid.NewGuid().ToString();

      // Arrange
      InitializeAndRun(MeasurementState.MeasurementCompleted);

      // Act
      _stateMachine.Fire(MeasurementEvent.LightCurtainValidated, parameter);

      // Assert
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementCompleted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementStarted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArrived(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArriving(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleInitialState(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckLeft(It.IsAny<StateMachinePayloadBase>()), Times.Never);
    }

    [TestMethod]
    public void Firing_CardScanned_ShouldNotAffectStateMachine_WhenStateMachineIsInState_MeasurementCompleted()
    {
      // Setup
      var parameter = Guid.NewGuid().ToString();

      // Arrange
      InitializeAndRun(MeasurementState.MeasurementCompleted);

      // Act
      _stateMachine.Fire(MeasurementEvent.CardScanned, parameter);

      // Assert
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementCompleted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementStarted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArrived(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArriving(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleInitialState(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckLeft(It.IsAny<StateMachinePayloadBase>()), Times.Never);
    }

    [TestMethod]
    public void Firing_MeasurementStored_ShouldNotAffectStateMachine_WhenStateMachineIsInState_MeasurementCompleted()
    {
      // Setup
      var parameter = Guid.NewGuid().ToString();

      // Arrange
      InitializeAndRun(MeasurementState.MeasurementCompleted);

      // Act
      _stateMachine.Fire(MeasurementEvent.MeasurementStored, parameter);

      // Assert
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementCompleted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementStarted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArrived(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArriving(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleInitialState(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckLeft(It.IsAny<StateMachinePayloadBase>()), Times.Never);
    }

    [TestMethod]
    public void Firing_Reset_ShouldNotAffectStateMachine_WhenStateMachineIsInState_MeasurementCompleted()
    {
      // Setup
      var parameter = Guid.NewGuid().ToString();

      // Arrange
      InitializeAndRun(MeasurementState.MeasurementCompleted);

      // Act
      _stateMachine.Fire(MeasurementEvent.Reset, parameter);

      // Assert
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementCompleted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementStarted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArrived(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArriving(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleInitialState(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckLeft(It.IsAny<StateMachinePayloadBase>()), Times.Never);
    }

    #endregion

    #region TruckLeft state

    [TestMethod]
    public void Firing_Reset_ShouldTurnStateMachineInto_Initial_WhenStateMachineIsInState_TruckLeft()
    {
      // Setup
      var parameter = new StateMachinePayloadBase();

      // Arrange
      InitializeAndRun(MeasurementState.TruckLeft);

      // Act
      _stateMachine.Fire(MeasurementEvent.Reset, parameter);

      // Assert
      _measurementSmEventHandlerMock.Verify(c => c.HandleInitialState(parameter), Times.Once);
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementCompleted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementStarted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArrived(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArriving(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckLeft(It.IsAny<StateMachinePayloadBase>()), Times.Never);
    }

    [TestMethod]
    public void Firing_IncreasingWeightLimitExceeded_ShouldNotAffectStateMachine_WhenStateMachineIsInState_TruckLeft()
    {
      // Setup
      var parameter = Guid.NewGuid().ToString();

      // Arrange
      InitializeAndRun(MeasurementState.TruckLeft);

      // Act
      _stateMachine.Fire(MeasurementEvent.IncreasingWeightLimitExceeded, parameter);

      // Assert
      _measurementSmEventHandlerMock.Verify(c => c.HandleInitialState(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementCompleted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementStarted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArrived(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArriving(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckLeft(It.IsAny<StateMachinePayloadBase>()), Times.Never);
    }

    [TestMethod]
    public void Firing_LightCurtainValidated_ShouldNotAffectStateMachine_WhenStateMachineIsInState_TruckLeft()
    {
      // Setup
      var parameter = Guid.NewGuid().ToString();

      // Arrange
      InitializeAndRun(MeasurementState.TruckLeft);

      // Act
      _stateMachine.Fire(MeasurementEvent.LightCurtainValidated, parameter);

      // Assert
      _measurementSmEventHandlerMock.Verify(c => c.HandleInitialState(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementCompleted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementStarted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArrived(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArriving(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckLeft(It.IsAny<StateMachinePayloadBase>()), Times.Never);
    }

    [TestMethod]
    public void Firing_CardScanned_ShouldNotAffectStateMachine_WhenStateMachineIsInState_TruckLeft()
    {
      // Setup
      var parameter = Guid.NewGuid().ToString();

      // Arrange
      InitializeAndRun(MeasurementState.TruckLeft);

      // Act
      _stateMachine.Fire(MeasurementEvent.CardScanned, parameter);

      // Assert
      _measurementSmEventHandlerMock.Verify(c => c.HandleInitialState(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementCompleted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementStarted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArrived(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArriving(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckLeft(It.IsAny<StateMachinePayloadBase>()), Times.Never);
    }

    [TestMethod]
    public void Firing_MeasurementStored_ShouldNotAffectStateMachine_WhenStateMachineIsInState_TruckLeft()
    {
      // Setup
      var parameter = Guid.NewGuid().ToString();

      // Arrange
      InitializeAndRun(MeasurementState.TruckLeft);

      // Act
      _stateMachine.Fire(MeasurementEvent.MeasurementStored, parameter);

      // Assert
      _measurementSmEventHandlerMock.Verify(c => c.HandleInitialState(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementCompleted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementStarted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArrived(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArriving(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckLeft(It.IsAny<StateMachinePayloadBase>()), Times.Never);
    }

    [TestMethod]
    public void Firing_DecreasingWeightLimitExceeded_ShouldNotAffectStateMachine_WhenStateMachineIsInState_TruckLeft()
    {
      // Setup
      var parameter = Guid.NewGuid().ToString();

      // Arrange
      InitializeAndRun(MeasurementState.TruckLeft);

      // Act
      _stateMachine.Fire(MeasurementEvent.DecreasingWeightLimitExceeded, parameter);

      // Assert
      _measurementSmEventHandlerMock.Verify(c => c.HandleInitialState(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementCompleted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleMeasurementStarted(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArrived(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckArriving(It.IsAny<StateMachinePayloadBase>()), Times.Never);
      _measurementSmEventHandlerMock.Verify(c => c.HandleTruckLeft(It.IsAny<StateMachinePayloadBase>()), Times.Never);
    }

    #endregion

    [TestMethod]
    public void StateMachine_ShouldBehaveCorrectlyInPathV1()
    {
      // Arrange
      InitializeAndRun(MeasurementState.Initial);

      // Act & Assert

      // Transition Initial -> TruckArriving
      _stateMachine.Fire(MeasurementEvent.IncreasingWeightLimitExceeded);
      Assert.AreEqual(MeasurementState.TruckArriving, _currentState);

      // Transition TruckArriving -> TruckArrived
      _stateMachine.Fire(MeasurementEvent.LightCurtainValidated);
      Assert.AreEqual(MeasurementState.TruckArrived, _currentState);

      // Transition TruckArrived -> MeasurementStarted
      _stateMachine.Fire(MeasurementEvent.CardScanned);
      Assert.AreEqual(MeasurementState.MeasurementStarted, _currentState);

      // Transition MeasurementStarted -> MeasurementCompleted
      _stateMachine.Fire(MeasurementEvent.MeasurementStored);
      Assert.AreEqual(MeasurementState.MeasurementCompleted, _currentState);

      // Transition MeasurementCompleted -> TruckLeft
      _stateMachine.Fire(MeasurementEvent.DecreasingWeightLimitExceeded);
      Assert.AreEqual(MeasurementState.TruckLeft, _currentState);

      // Transition TruckLeft -> Initial
      _stateMachine.Fire(MeasurementEvent.Reset);
      Assert.AreEqual(MeasurementState.Initial, _currentState);
    }

    [TestMethod]
    public void StateMachine_ShouldBehaveCorrectlyInPathV2()
    {
      // Arrange
      InitializeAndRun(MeasurementState.Initial);

      // Act & Assert

      // Transition Initial -> TruckArrived
      _stateMachine.Fire(MeasurementEvent.LightCurtainValidated);
      Assert.AreEqual(MeasurementState.TruckArrived, _currentState);

      // Transition TruckArrived -> MeasurementStarted
      _stateMachine.Fire(MeasurementEvent.CardScanned);
      Assert.AreEqual(MeasurementState.MeasurementStarted, _currentState);

      // Transition MeasurementStarted -> MeasurementCompleted
      _stateMachine.Fire(MeasurementEvent.MeasurementStored);
      Assert.AreEqual(MeasurementState.MeasurementCompleted, _currentState);

      // Transition MeasurementCompleted -> TruckLeft
      _stateMachine.Fire(MeasurementEvent.DecreasingWeightLimitExceeded);
      Assert.AreEqual(MeasurementState.TruckLeft, _currentState);

      // Transition TruckLeft -> Initial
      _stateMachine.Fire(MeasurementEvent.Reset);
      Assert.AreEqual(MeasurementState.Initial, _currentState);
    }

    [TestMethod]
    public void StateMachine_ShouldBehaveCorrectlyInPathV3()
    {
      // Arrange
      InitializeAndRun(MeasurementState.Initial);

      // Act & Assert

      // Transition Initial -> MeasurementStarted
      _stateMachine.Fire(MeasurementEvent.CardScanned);
      Assert.AreEqual(MeasurementState.MeasurementStarted, _currentState);

      // Transition MeasurementStarted -> MeasurementCompleted
      _stateMachine.Fire(MeasurementEvent.MeasurementStored);
      Assert.AreEqual(MeasurementState.MeasurementCompleted, _currentState);

      // Transition MeasurementCompleted -> TruckLeft
      _stateMachine.Fire(MeasurementEvent.DecreasingWeightLimitExceeded);
      Assert.AreEqual(MeasurementState.TruckLeft, _currentState);

      // Transition TruckLeft -> Initial
      _stateMachine.Fire(MeasurementEvent.Reset);
      Assert.AreEqual(MeasurementState.Initial, _currentState);
    }

    [TestMethod]
    public void StateMachine_ShouldBehaveCorrectlyInPathV4()
    {
      // Arrange
      InitializeAndRun(MeasurementState.Initial);

      // Act & Assert

      // Transition Initial -> TruckArriving
      _stateMachine.Fire(MeasurementEvent.IncreasingWeightLimitExceeded);
      Assert.AreEqual(MeasurementState.TruckArriving, _currentState);

      // Transition TruckArriving -> MeasurementStarted
      _stateMachine.Fire(MeasurementEvent.CardScanned);
      Assert.AreEqual(MeasurementState.MeasurementStarted, _currentState);

      // Transition MeasurementStarted -> MeasurementCompleted
      _stateMachine.Fire(MeasurementEvent.MeasurementStored);
      Assert.AreEqual(MeasurementState.MeasurementCompleted, _currentState);

      // Transition MeasurementCompleted -> TruckLeft
      _stateMachine.Fire(MeasurementEvent.DecreasingWeightLimitExceeded);
      Assert.AreEqual(MeasurementState.TruckLeft, _currentState);

      // Transition TruckLeft -> Initial
      _stateMachine.Fire(MeasurementEvent.Reset);
      Assert.AreEqual(MeasurementState.Initial, _currentState);
    }
  }
}
