﻿using System;
using System.Reflection;
using log4net.Config;
using Unity;

namespace Kalisto.Frontend.ConsoleApplication
{
  class Program
  {
    static void Main(string[] args)
    {
      XmlConfigurator.Configure();

      Console.WriteLine("Kalisto.Frontend has been started successfully.");
      Console.WriteLine("Press ENTER to stop.");

      var appName = Assembly.GetExecutingAssembly().GetName().Name;
      new FrontendEndpoint(new UnityContainer(), "Kalisto", appName).Initialize();

      Console.ReadLine();
    }
  }
}
