﻿using System.ComponentModel.DataAnnotations;

namespace Kalisto.Backend.DataAccess.Model
{
  public enum CardLifetime
  {
    Disposable, Reusable
  }

  public class Card
  {
    [Key]
    public long Id { get; set; }

    public int Number { get; set; }

    public string Code { get; set; }

    public virtual Driver Driver { get; set; }

    public virtual Vehicle Vehicle { get; set; }

    public virtual Cargo Cargo { get; set; }

    public virtual Contractor Contractor { get; set; }

    public CardLifetime? Lifetime { get; set; }

    public MeasurementClass? MeasurementClass { get; set; }

    public bool IsActive { get; set; }
  }
}
