﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;

namespace Kalisto.Backend.DataAccess.Model
{
  public class Vehicle
  {
    [Key]
    public long Id { get; set; }

    public string PlateNumber { get; set; }

    public decimal? Tare { get; set; }

    public DateTime? TareDateUtc { get; set; }

    public decimal? MaxVehicleWeight { get; set; }

    [XmlIgnore]
    public virtual Driver Driver { get; set; }

    [XmlIgnore]
    public virtual Contractor Contractor { get; set; }

    [XmlIgnore]
    public virtual ICollection<Measurement> Measurements { get; set; }

    [XmlIgnore]
    public virtual ICollection<Card> Cards { get; set; }
  }
}