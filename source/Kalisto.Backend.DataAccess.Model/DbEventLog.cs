﻿using System;
using System.ComponentModel.DataAnnotations;
using Kalisto.Backend.DataAccess.Model.Enum;

namespace Kalisto.Backend.DataAccess.Model
{ 
  public class DbEventLog
  {
    [Key]
    public long Id { get; set; }

    public long EntityId { get; set; }

    public string TableName { get; set; }

    public string OldValue { get; set; }

    public string NewValue { get; set; }

    public DbEventType EventType { get; set; }

    public string Source { get; set; }

    public DateTime EventDateUtc { get; set; }
  }
}
