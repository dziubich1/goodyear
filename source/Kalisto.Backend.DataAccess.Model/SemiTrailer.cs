﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Kalisto.Backend.DataAccess.Model
{
  public class SemiTrailer
  {
    [Key]
    public long Id { get; set; }

    public string Number { get; set; }
  }
}
