﻿using System.ComponentModel.DataAnnotations;

namespace Kalisto.Backend.DataAccess.Model
{
  public class UserRole
  {
    [Key]
    public long Id { get; set; }

    public int Type { get; set; }
  }
}
