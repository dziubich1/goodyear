﻿namespace Kalisto.Backend.DataAccess.Model.Enum
{
  public enum DbEventType
  {
    Insert, Update, Delete
  }
}
