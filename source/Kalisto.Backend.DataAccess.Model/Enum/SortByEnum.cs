﻿namespace Kalisto.Backend.DataAccess.Model.Enum
{
  public enum SortBy
  {
    CargoName, MeasurementDate
  }
}
