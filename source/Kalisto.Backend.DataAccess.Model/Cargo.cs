﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;

namespace Kalisto.Backend.DataAccess.Model
{
  public class Cargo
  {
    [Key]
    public long Id { get; set; }

    public string Name { get; set; }

    public decimal? Price { get; set; }

    [XmlIgnore]
    public virtual ICollection<Measurement> Measurements { get; set; }

    [XmlIgnore]
    public virtual ICollection<Card> Cards { get; set; }
  }
}
