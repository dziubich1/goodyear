﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Kalisto.Backend.DataAccess.Model
{
  public class SystemLog
  {
    [Key]
    public long Id { get; set; }

    public string LogResourceKey { get; set; }

    public DateTime EventDateUtc { get; set; }

    public string Source { get; set; }

    public string Parameters { get; set; }
  }
}
