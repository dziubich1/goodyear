﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;

namespace Kalisto.Backend.DataAccess.Model
{
  public class User
  {
    [Key]
    public long Id { get; set; }

    public string Name { get; set; }

    public string PasswordHash { get; set; }

    public UserRole Role { get; set; }

    [XmlIgnore]
    public virtual ICollection<Measurement> Measurements { get; set; }
  }
}
