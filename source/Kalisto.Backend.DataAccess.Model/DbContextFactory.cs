﻿using Unity;
using Unity.Resolution;

namespace Kalisto.Backend.DataAccess.Model
{
  public class DbContextFactory : IDbContextFactory
  {
    private readonly IUnityContainer _container;

    public DbContextFactory(IUnityContainer container)
    {
      _container = container;
    }

    public IDbContext CreateDbContext(string connectionString)
    {
      return _container.Resolve<IDbContext>("stringConstructor",
        new DependencyOverride(typeof(string), connectionString));
    }
  }
}
