﻿namespace Kalisto.Backend.DataAccess.Model
{
  public interface IDbContextFactory
  {
    IDbContext CreateDbContext(string connectionString);
  }
}
