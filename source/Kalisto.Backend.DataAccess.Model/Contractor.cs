﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;

namespace Kalisto.Backend.DataAccess.Model
{
  public class Contractor
  {
    [Key]
    public long Id { get; set; }

    public string Name { get; set; }

    public string Address { get; set; }

    public string PostalCode { get; set; }

    public string City { get; set; }

    public string TaxId { get; set; }

    [XmlIgnore]
    public virtual ICollection<Measurement> Measurements { get; set; }

    [XmlIgnore]
    public virtual ICollection<Card> Cards { get; set; }

    [XmlIgnore]
    public virtual ICollection<Vehicle> Vehicles { get; set; }
  }
}
