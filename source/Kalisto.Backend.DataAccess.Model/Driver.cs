﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Xml.Serialization;

namespace Kalisto.Backend.DataAccess.Model
{
  public class Driver
  {
    [Key]
    public long Id { get; set; }

    public string Identifier { get; set; }

    public string PhoneNumber { get; set; }

    [XmlIgnore]
    public virtual ICollection<Measurement> Measurements { get; set; }

    [XmlIgnore]
    public virtual ICollection<Card> Cards { get; set; }

    [XmlIgnore]
    public virtual ICollection<Vehicle> Vehicles { get; set; }
  }
}
