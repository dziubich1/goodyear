﻿using System;
using System.Data.Entity;

namespace Kalisto.Backend.DataAccess.Model
{
  public interface IDbContext : IDisposable
  {
    IDbSet<User> Users { get; set; }

    IDbSet<UserRole> UserRoles { get; set; }

    IDbSet<DbEventLog> DbEventLogs { get; set; }

    IDbSet<SystemLog> SystemLogs { get; set; }

    IDbSet<Contractor> Contractors { get; set; }

    IDbSet<Cargo> Cargoes { get; set; }

    IDbSet<Driver> Drivers { get; set; }

    IDbSet<Vehicle> Vehicles { get; set; }

    IDbSet<Measurement> Measurements { get; set; }

    IDbSet<SemiTrailer> SemiTrailers { get; set; }

    IDbSet<Card> Cards { get; set; }

    IDbSet<BeltConveyorMeasurement> BeltConveyorMeasurements { get; set; }

    int SaveChanges();
  }
}
