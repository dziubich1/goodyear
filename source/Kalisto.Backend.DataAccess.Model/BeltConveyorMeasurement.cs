﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kalisto.Backend.DataAccess.Model
{
  public class BeltConveyorMeasurement
  {
    [Key]
    public long Id { get; set; }

    public long Number { get; set; }

    public int ConfigId { get; set; }

    public decimal B1 { get; set; }

    public decimal B2 { get; set; }

    public decimal B3 { get; set; }

    public decimal B4 { get; set; }

    public decimal TotalSum { get; set; }

    public decimal IncrementSum { get; set; }

    public DateTime DateUtc { get; set; }
  }
}
