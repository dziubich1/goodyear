﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Kalisto.Backend.DataAccess.Model
{
  public enum MeasurementType
  {
    Single, Double
  }

  public enum MeasurementClass
  {
    Income, Outcome, Service
  }

  public class Measurement
  {
    [Key]
    public long Id { get; set; }

    public long Number { get; set; }

    public MeasurementType Type { get; set; }

    public MeasurementClass? Class { get; set; }

    public DateTime Date1Utc { get; set; }

    public DateTime? Date2Utc { get; set; }

    public int DeviceId { get; set; }

    public string FirstPhoto1RelPath { get; set; }

    public string FirstPhoto2RelPath { get; set; }

    public string FirstPhoto3RelPath { get; set; }

    public string SecondPhoto1RelPath { get; set; }

    public string SecondPhoto2RelPath { get; set; }

    public string SecondPhoto3RelPath { get; set; }

    public virtual Contractor Contractor { get; set; }

    public virtual Cargo Cargo { get; set; }

    public virtual Driver Driver { get; set; }

    public virtual Vehicle Vehicle { get; set; }

    public virtual SemiTrailer SemiTrailer { get; set; }

    public virtual User Operator { get; set; }

    public decimal Tare { get; set; }

    public decimal Measurement1 { get; set; }

    public decimal? Measurement2 { get; set; }

    public decimal NetWeight { get; set; }

    public decimal? DeclaredWeight { get; set; }

    public decimal? WeighingThreshold { get; set; }

    public string QRCode { get; set; }

    public string CardCode { get; set; }

    public bool IsCancelled { get; set; }

    public bool IsManual { get; set; }

    public string Comment { get; set; }
  }
}
