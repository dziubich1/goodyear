﻿using Kalisto.Backend.DataAccess.Model.Logging.Base;

namespace Kalisto.Backend.DataAccess.Model.Logging
{
  public interface IDbDeleteEventLogger : IDbEventLogger
  {
  }
}
