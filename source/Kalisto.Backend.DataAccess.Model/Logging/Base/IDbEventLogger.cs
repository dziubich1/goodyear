﻿using System;
using System.Data.Entity;
using System.Linq.Expressions;

namespace Kalisto.Backend.DataAccess.Model.Logging.Base
{
  public interface IDbEventLogger
  {
    void LogEvent<TTable, TEntity>(string partnerId, Expression<Func<IDbContext, TTable>> propertyLambda, long entityId,
      TEntity newValue)
      where TTable : IDbSet<TEntity> where TEntity : class;

    void LogEvent<TTable, TEntity>(string partnerId, Expression<Func<IDbContext, TTable>> propertyLambda, long entityId,
      TEntity newValue, TEntity oldValue)
      where TTable : IDbSet<TEntity> where TEntity : class;
  }
}
