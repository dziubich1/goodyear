﻿using Kalisto.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Kalisto.Backend.DataAccess.Model
{
  [Serializable]
  public class MeasurementExport
  {
    public long Id { get; set; }
    public long Number { get; set; }
    public string Type { get; set; }
    public string Class { get; set; }
    public int DeviceId { get; set; }
    public long? ContractorId { get; set; }
    public string ContractorName { get; set; }
    public long? CargoId { get; set; }
    public string CargoName { get; set; }
    public long? DriverId { get; set; }
    public string DriverIdentifier { get; set; }
    public long? VehicleId { get; set; }
    public string VehiclePlateNumber { get; set; }
    public long? SemiTralierId { get; set; }
    public string SemiTrailerNumber { get; set; }
    public long? OperatorId { get; set; }
    public string OperatorName { get; set; }
    public decimal Tare { get; set; }
    public decimal Measurement1 { get; set; }
    public string Date1Utc { get; set; }
    public decimal? Measurement2 { get; set; }
    public string Date2Utc { get; set; }
    public decimal NetWeight { get; set; }
    public decimal? DeclaredWeight { get; set; }
    public bool IsCancelled { get; set; }
    public bool IsManual { get; set; }
    public string Comment { get; set; }

    [XmlAttribute(AttributeName = "OperationType")]
    public string OperationType { get; set; }

    public MeasurementExport()
    {

    }
    public MeasurementExport(Measurement measurement, EntityOperationType entityOperationType)
    {
      Id = measurement.Id;
      Number = measurement.Number;
      Type = measurement.Type.ToString();
      Class = measurement.Class.ToString();
      DeviceId = measurement.DeviceId;
      ContractorId = measurement.Contractor?.Id;
      ContractorName = measurement.Contractor?.Name;
      CargoId = measurement.Cargo?.Id;
      CargoName = measurement.Cargo?.Name;
      DriverId = measurement.Driver?.Id;
      DriverIdentifier = measurement.Driver?.Identifier;
      VehicleId = measurement.Vehicle?.Id;
      VehiclePlateNumber = measurement.Vehicle?.PlateNumber;
      SemiTralierId = measurement.SemiTrailer?.Id;
      SemiTrailerNumber = measurement.SemiTrailer?.Number;
      OperatorId = measurement.Operator?.Id;
      OperatorName = measurement.Operator?.Name;
      Tare = measurement.Tare;
      Measurement1 = measurement.Measurement1;
      Date1Utc = measurement.Date1Utc.ToString();
      Measurement2 = measurement.Measurement2;
      Date2Utc = measurement.Date2Utc?.ToString();
      NetWeight = measurement.NetWeight;
      DeclaredWeight = measurement.DeclaredWeight;
      IsCancelled = measurement.IsCancelled;
      IsManual = measurement.IsManual;
      Comment = measurement.Comment;

      OperationType = entityOperationType.ToString();
    }
  }
}
