﻿using Kalisto.Backend.Operations.Drivers.Commands.Interfaces;
using Kalisto.Communication.Core.Messages;

namespace Kalisto.Backend.Operations.Drivers.Commands
{
  public class CreateDriverCommand : BackendDbOperationMessage, IDriverCommand
  {
    public string DriverIdentifier { get; set; }

    public string DriverPhoneNumber { get; set; }
  }
}
