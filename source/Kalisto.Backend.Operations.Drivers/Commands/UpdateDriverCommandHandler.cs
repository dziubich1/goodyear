﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.DataAccess.Model.Logging;
using Kalisto.Backend.Operations.Drivers.Exceptions;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Logging;
using System;
using System.Data.Entity;
using System.Linq;

namespace Kalisto.Backend.Operations.Drivers.Commands
{
  public class UpdateDriverCommandHandler : CommandHandlerBase<UpdateDriverCommand>
  {
    private readonly IDbUpdateEventLogger _dbEventLogger;
    private readonly ILogger _logger;

    public UpdateDriverCommandHandler(IDbUpdateEventLogger dbEventLogger, ILogger logger)
    {
      _dbEventLogger = dbEventLogger;
      _logger = logger;
    }

    public override object Execute(IDbContext context, UpdateDriverCommand command, string partnerId)
    {
      var driver = context.Drivers.FirstOrDefault(c => c.Id == command.DriverId);
      if (driver == null)
      {
        _logger.Info($"Cannot update driver with ID: {command.DriverId} because it does not exist");
        throw new ArgumentException("Driver with given identifier does not exist.");
      }

      if (context.Drivers.Any(c => c.Id != driver.Id && c.Identifier.ToLower() == command.DriverIdentifier.Trim().ToLower()))
      {
        _logger.Debug($"Cannot update driver identifier because driver with identifier: {command.DriverIdentifier} already exists. Updated driver ID: {driver.Id}");
        throw new DriverExistsException();
      }

      // only for getting current driver old values
      var oldDriver = context.Drivers.AsNoTracking().FirstOrDefault(c => c.Id == command.DriverId);

      driver.Identifier = command.DriverIdentifier.Trim();
      driver.PhoneNumber = command.DriverPhoneNumber?.Trim();

      context.SaveChanges();

      _dbEventLogger.LogEvent(partnerId, ctx => ctx.Drivers, driver.Id, driver, oldDriver);
      _logger.Debug($"Successfully updated driver with ID: {driver.Id}");

      return driver.Id;
    }
  }
}
