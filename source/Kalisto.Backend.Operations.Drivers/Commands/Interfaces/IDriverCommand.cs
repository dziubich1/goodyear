﻿using Kalisto.Communication.Core.Messages;

namespace Kalisto.Backend.Operations.Drivers.Commands.Interfaces
{
  public interface IDriverCommand : IAssociatedCommand
  {
  }
}
