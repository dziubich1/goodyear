﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.DataAccess.Model.Logging;
using Kalisto.Backend.Operations.Drivers.Exceptions;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Logging;
using System;
using System.Data.Entity;
using System.Linq;

namespace Kalisto.Backend.Operations.Drivers.Commands
{
  public class DeleteDriverCommandHandler : CommandHandlerBase<DeleteDriverCommand>
  {
    private readonly IDbDeleteEventLogger _dbEventLogger;
    private readonly ILogger _logger;

    public DeleteDriverCommandHandler(IDbDeleteEventLogger dbEventLogger, ILogger logger)
    {
      _dbEventLogger = dbEventLogger;
      _logger = logger;
    }

    public override object Execute(IDbContext context, DeleteDriverCommand command, string partnerId)
    {
      var driver = context.Drivers
        .Include(d => d.Measurements)
        .Include(d => d.Cards)
        .Include(d => d.Vehicles)
        .FirstOrDefault(d => d.Id == command.DriverId);

      if (driver == null)
      {
        _logger.Error($"Cannot delete driver with ID: {command.DriverId} because it does not exist");
        throw new ArgumentException("Driver with given identifier does not exist.");
      }

      if (driver.Measurements.Count > 0)
      {
        _logger.Info($"Cannot delete driver with ID: {command.DriverId} because it is used in measurements");
        throw new DriverLockedByMeasurementException();
      }

      if (driver.Cards.Count > 0)
      {
        _logger.Debug($"Cannot delete driver with ID: {command.DriverId} because it is used in cards");
        throw new DriverLockedByCardException();
      }

      if (driver.Vehicles.Count > 0)
      {
        _logger.Debug($"Cannot delete driver with ID: {command.DriverId} because it is used in vehicles");
        throw new DriverLockedByVehicleException();
      }

      context.Drivers.Remove(driver);
      context.SaveChanges();

      _dbEventLogger.LogEvent(partnerId, ctx => ctx.Drivers, driver.Id, null, driver);
      _logger.Debug($"Successfully deleted driver with ID: {driver.Id}");

      return command.DriverId;
    }
  }
}
