﻿using Kalisto.Backend.Operations.Drivers.Commands.Interfaces;
using Kalisto.Communication.Core.Messages;

namespace Kalisto.Backend.Operations.Drivers.Commands
{
  public class DeleteDriverCommand : BackendDbOperationMessage, IDriverCommand
  {
    public long DriverId { get; set; }
  }
}
