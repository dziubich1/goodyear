﻿using Kalisto.Backend.Operations.Drivers.Commands.Interfaces;
using Kalisto.Communication.Core.Messages;

namespace Kalisto.Backend.Operations.Drivers.Commands
{
  public class UpdateDriverCommand : BackendDbOperationMessage, IDriverCommand
  {
    public long DriverId { get; set; }

    public string DriverIdentifier { get; set; }

    public string DriverPhoneNumber { get; set; }
  }
}
