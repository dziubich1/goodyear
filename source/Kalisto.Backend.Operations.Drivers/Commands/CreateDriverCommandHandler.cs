﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.DataAccess.Model.Logging;
using Kalisto.Backend.Operations.Drivers.Exceptions;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Logging;
using System.Linq;

namespace Kalisto.Backend.Operations.Drivers.Commands
{
  public class CreateDriverCommandHandler : CommandHandlerBase<CreateDriverCommand>
  {
    private readonly IDbInsertEventLogger _dbEventLogger;
    private readonly ILogger _logger;

    public CreateDriverCommandHandler(IDbInsertEventLogger dbEventLogger, ILogger logger)
    {
      _dbEventLogger = dbEventLogger;
      _logger = logger;
    }

    public override object Execute(IDbContext context, CreateDriverCommand command, string partnerId)
    {
      if (context.Drivers.Any(d => d.Identifier.ToLower().Trim() == command.DriverIdentifier.ToLower().Trim()))
      {
        _logger.Debug($"Cannot create driver with identifier: {command.DriverIdentifier} because it already exists");
        throw new DriverExistsException();
      }

      var driver = new Driver
      {
        Identifier = command.DriverIdentifier.Trim(),
        PhoneNumber = command.DriverPhoneNumber?.Trim()
      };

      context.Drivers.Add(driver);
      context.SaveChanges();

      _dbEventLogger.LogEvent(partnerId, ctx => ctx.Drivers, driver.Id, driver);
      _logger.Debug($"Successfully created driver with ID: {driver.Id}");

      return driver.Id;
    }
  }
}
