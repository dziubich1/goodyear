﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Communication.Core.Messaging;
using System.Data.Entity;
using System.Linq;

namespace Kalisto.Backend.Operations.Drivers.Queries
{
  public class GetAllDriversQueryHandler : QueryHandlerBase<GetAllDriversQuery>
  {
    public override object Execute(IDbContext context, GetAllDriversQuery query, string partnerId)
    {
      return context.Drivers.AsNoTracking().ToList();
    }
  }
}
