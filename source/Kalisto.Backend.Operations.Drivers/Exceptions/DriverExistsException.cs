﻿using Kalisto.Communication.Core.Exceptions;
using System;
using System.Runtime.Serialization;

namespace Kalisto.Backend.Operations.Drivers.Exceptions
{
  [Serializable]
  public class DriverExistsException : ExceptionBase
  {
    public DriverExistsException()
    {
    }

    // REMARKS: below constructor need to be added due to the serialization/deserialization issues
    public DriverExistsException(SerializationInfo info, StreamingContext context)
    {
    }

    public override string ExceptionCode => "DRIVERS_1001";

    public override string Message => GetLocalizedMessage();
  }
}
