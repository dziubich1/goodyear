﻿using Kalisto.Communication.Core.Exceptions;
using System;
using System.Runtime.Serialization;

namespace Kalisto.Backend.Operations.Drivers.Exceptions
{
  [Serializable]
  public class DriverLockedByCardException : ExceptionBase
  {
    public DriverLockedByCardException()
    {
    }

    // REMARKS: below constructor need to be added due to the serialization/deserialization issues
    public DriverLockedByCardException(SerializationInfo info, StreamingContext context)
    {
    }

    public override string ExceptionCode => "DRIVERS_1003";

    public override string Message => GetLocalizedMessage();
  }
}
