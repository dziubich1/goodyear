﻿using Kalisto.Communication.Core.Exceptions;
using System;
using System.Runtime.Serialization;

namespace Kalisto.Backend.Operations.Drivers.Exceptions
{
  [Serializable]
  public class DriverLockedByMeasurementException : ExceptionBase
  {
    public DriverLockedByMeasurementException()
    {
    }

    // REMARKS: below constructor need to be added due to the serialization/deserialization issues
    public DriverLockedByMeasurementException(SerializationInfo info, StreamingContext context)
    {
    }

    public override string ExceptionCode => "DRIVERS_1002";

    public override string Message => GetLocalizedMessage();
  }
}
