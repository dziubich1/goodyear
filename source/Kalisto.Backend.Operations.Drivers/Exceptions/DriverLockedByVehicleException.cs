﻿using Kalisto.Communication.Core.Exceptions;
using System;
using System.Runtime.Serialization;

namespace Kalisto.Backend.Operations.Drivers.Exceptions
{
  [Serializable]
  public class DriverLockedByVehicleException : ExceptionBase
  {
    public DriverLockedByVehicleException()
    {
    }

    // REMARKS: below constructor need to be added due to the serialization/deserialization issues
    public DriverLockedByVehicleException(SerializationInfo info, StreamingContext context)
    {
    }

    public override string ExceptionCode => "DRIVERS_1004";

    public override string Message => GetLocalizedMessage();
  }
}
