﻿using Kalisto.Backend.DataAccess.Effort;
using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.DataAccess.Model.Logging;
using Kalisto.Backend.Operations.Measurements.Commands;
using Kalisto.Communication.Core.Logging;
using Kalisto.Core;
using Kalisto.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kalisto.Backend.Operations.Measurements.UnitTest
{
  [TestClass]
  public class UpdateMeasurementCommandHandlerTests
  {
    private IDbContext _dbContext;

    private readonly string _partnerId = "unknown";

    private Mock<IDbUpdateEventLogger> _dbEventLoggerMock;

    private Mock<ISystemEventLogger> _eventLoggerMock;

    private Mock<IXmlExporterService> _xmlExporterService;

    // component under tests
    private UpdateMeasurementCommandHandler _updateMeasurementCommandHandler;

    [TestInitialize]
    public void Initialize()
    {
      var effortConnection = Effort.DbConnectionFactory.CreateTransient();
      _dbContext = new EffortDbContext(effortConnection);

      _dbEventLoggerMock = new Mock<IDbUpdateEventLogger>();
      _eventLoggerMock = new Mock<ISystemEventLogger>();
      _xmlExporterService = new Mock<IXmlExporterService>();

      var logger = new Mock<ILogger>();

      _updateMeasurementCommandHandler = new UpdateMeasurementCommandHandler(_dbEventLoggerMock.Object, _eventLoggerMock.Object, logger.Object, _xmlExporterService.Object);
    }

    [TestCleanup]
    public void CleanUp()
    {
      _dbContext.Dispose();
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void Execute_ShouldThrowArgumentException_WhenMeasurementWithGivenIdDoesNotExist()
    {
      // Arrange
      var notExistingMeasurementId = 44;
      var deleteMeasurementByIdCommand = new UpdateMeasurementCommand
      {
        MeasurementId = notExistingMeasurementId
      };

      // Act & Assert
      _updateMeasurementCommandHandler.Execute(_dbContext, deleteMeasurementByIdCommand, _partnerId);
    }

    [TestMethod]
    public void Execute_ShouldUpdateProperties()
    {
      // Setup 
      var utcYesterday = DateTime.UtcNow.AddDays(-1);
      var utcNow = DateTime.UtcNow;

      var measurement = new Measurement
      {
        Number = 1L,
        Type = MeasurementType.Double,
        Class = MeasurementClass.Service,
        Date1Utc = utcYesterday,
        Date2Utc = utcNow,
        DeviceId = 1,
        Measurement1 = 0M,
        Measurement2 = 0M,
        DeclaredWeight = 0M,
        Tare = 0M,
        NetWeight = 0M,
        Comment = "First comment",
        IsManual = true,
        IsCancelled = false
      };

      _dbContext.Measurements.Add(measurement);
      _dbContext.SaveChanges();

      // Arrange
      var updateMeaurementCommand = new UpdateMeasurementCommand
      {
        MeasurementId = 1L,
        MeasurementDate1Utc = utcYesterday.AddMonths(-1),
        MeasurementDate2Utc = utcNow.AddMonths(-1),
        Measurement1 = 11.11M,
        Measurement2 = 22.22M,
        NetWeight = 3.14M,
        DeclaredWeight = 6.28M,
        Tare = 1M,
        Class = MeasurementClass.Outcome,
        Comment = "dummy comment",
        IsCancelled = true
      };

      // Act
      _updateMeasurementCommandHandler.Execute(_dbContext, updateMeaurementCommand, _partnerId);

      // Assert
      var createdMeasurement = _dbContext.Measurements.Single();

      Assert.AreEqual(updateMeaurementCommand.MeasurementDate1Utc, createdMeasurement.Date1Utc,
        "MeasurementDate1Utc of updated measurement should be the same like in command");
      Assert.AreEqual(updateMeaurementCommand.MeasurementDate2Utc, createdMeasurement.Date2Utc,
        "MeasurementDate1Utc of updated measurement should be the same like in command");
      Assert.AreEqual(updateMeaurementCommand.Measurement1, createdMeasurement.Measurement1,
        "MeasurementDate1Utc of updated measurement should be the same like in command");
      Assert.AreEqual(updateMeaurementCommand.Measurement2, createdMeasurement.Measurement2,
        "MeasurementDate1Utc of updated measurement should be the same like in command");
      Assert.AreEqual(updateMeaurementCommand.NetWeight, createdMeasurement.NetWeight,
        "MeasurementDate1Utc of updated measurement should be the same like in command");
      Assert.AreEqual(updateMeaurementCommand.DeclaredWeight, createdMeasurement.DeclaredWeight,
        "MeasurementDate1Utc of updated measurement should be the same like in command");
      Assert.AreEqual(updateMeaurementCommand.Tare, createdMeasurement.Tare,
        "MeasurementDate1Utc of updated measurement should be the same like in command");
      Assert.AreEqual(updateMeaurementCommand.Class, createdMeasurement.Class,
        "MeasurementDate1Utc of updated measurement should be the same like in command");
      Assert.AreEqual(updateMeaurementCommand.Comment, createdMeasurement.Comment,
        "MeasurementDate1Utc of updated measurement should be the same like in command");
      Assert.AreEqual(updateMeaurementCommand.IsCancelled, createdMeasurement.IsCancelled,
        "MeasurementDate1Utc of updated measurement should be the same like in command");
    }

    [TestMethod]
    public void Execute_ShouldUpdateProperEntities_WhenEntitiesWithGivenIdentifierExist()
    {
      // Setup
      var utcYesterday = DateTime.UtcNow.AddDays(-1);
      var utcNow = DateTime.UtcNow;

      var cargo = new Cargo
      {
        Name = "test"
      };
      _dbContext.Cargoes.Add(cargo);

      var driver = new Driver
      {
        Identifier = "test"
      };
      _dbContext.Drivers.Add(driver);

      var vehicle = new Vehicle
      {
        PlateNumber = "test"
      };
      _dbContext.Vehicles.Add(vehicle);

      var contractor = new Contractor
      {
        Name = "test"
      };
      _dbContext.Contractors.Add(contractor);

      var semiTrailer = new SemiTrailer
      {
        Number = "existing number"
      };
      _dbContext.SemiTrailers.Add(semiTrailer);

      var measurement = new Measurement
      {
        Number = 1L,
        Type = MeasurementType.Double,
        Class = MeasurementClass.Service,
        Date1Utc = utcYesterday,
        Date2Utc = utcNow,
        DeviceId = 1,
        Measurement1 = 0M,
        Measurement2 = 0M,
        DeclaredWeight = 0M,
        Tare = 0M,
        NetWeight = 0M,
        Comment = "First comment",
        IsManual = true,
        IsCancelled = false
      };
      _dbContext.Measurements.Add(measurement);

      _dbContext.SaveChanges();

      // Arrange
      var updateMeaurementCommand = new UpdateMeasurementCommand
      {
        MeasurementId = 1L,
        MeasurementDate1Utc = utcYesterday.AddMonths(-1),
        MeasurementDate2Utc = utcNow.AddMonths(-1),
        Measurement1 = 11.11M,
        Measurement2 = 22.22M,
        NetWeight = 3.14M,
        DeclaredWeight = 6.28M,
        Tare = 1M,
        Class = MeasurementClass.Outcome,
        Comment = "dummy comment",
        IsCancelled = true,
        CargoId = cargo.Id,
        ContractorId = contractor.Id,
        DriverId = driver.Id,
        VehicleId = vehicle.Id,
        SemiTrailerNumber = semiTrailer.Number
      };

      // Act
      _updateMeasurementCommandHandler.Execute(_dbContext, updateMeaurementCommand, _partnerId);

      // Assert
      var createdMeasurement = _dbContext.Measurements.Single();

      Assert.AreEqual(updateMeaurementCommand.MeasurementDate1Utc, createdMeasurement.Date1Utc,
        "MeasurementDate1Utc of updated measurement should be the same like in command");
      Assert.AreEqual(updateMeaurementCommand.MeasurementDate2Utc, createdMeasurement.Date2Utc,
        "MeasurementDate1Utc of updated measurement should be the same like in command");
      Assert.AreEqual(updateMeaurementCommand.Measurement1, createdMeasurement.Measurement1,
        "MeasurementDate1Utc of updated measurement should be the same like in command");
      Assert.AreEqual(updateMeaurementCommand.Measurement2, createdMeasurement.Measurement2,
        "MeasurementDate1Utc of updated measurement should be the same like in command");
      Assert.AreEqual(updateMeaurementCommand.NetWeight, createdMeasurement.NetWeight,
        "MeasurementDate1Utc of updated measurement should be the same like in command");
      Assert.AreEqual(updateMeaurementCommand.DeclaredWeight, createdMeasurement.DeclaredWeight,
        "MeasurementDate1Utc of updated measurement should be the same like in command");
      Assert.AreEqual(updateMeaurementCommand.Tare, createdMeasurement.Tare,
        "MeasurementDate1Utc of updated measurement should be the same like in command");
      Assert.AreEqual(updateMeaurementCommand.Class, createdMeasurement.Class,
        "MeasurementDate1Utc of updated measurement should be the same like in command");
      Assert.AreEqual(updateMeaurementCommand.Comment, createdMeasurement.Comment,
        "MeasurementDate1Utc of updated measurement should be the same like in command");
      Assert.AreEqual(updateMeaurementCommand.IsCancelled, createdMeasurement.IsCancelled,
        "MeasurementDate1Utc of updated measurement should be the same like in command");
      Assert.AreEqual(updateMeaurementCommand.ContractorId, createdMeasurement.Contractor.Id,
        "Contractor id of updated measurement should be the same like in command");
      Assert.AreEqual(updateMeaurementCommand.CargoId, createdMeasurement.Cargo.Id,
        "Cargo id of updated measurement should be the same like in command");
      Assert.AreEqual(updateMeaurementCommand.DriverId, createdMeasurement.Driver.Id,
        "Driver id of updated measurement should be the same like in command");
      Assert.AreEqual(updateMeaurementCommand.VehicleId, createdMeasurement.Vehicle.Id,
        "Vehicle id of updated measurement should be the same like in command");
      Assert.AreEqual(updateMeaurementCommand.SemiTrailerNumber, createdMeasurement.SemiTrailer.Number,
        "SemiTrailer number of updated measurement should be the same like in command");
    }

    [TestMethod]
    public void Execute_ShouldUpdateEntitiesToNull_WhenEntitiesIdentifiersAreNotSpecified()
    {
      // Setup
      var utcYesterday = DateTime.UtcNow.AddDays(-1);
      var utcNow = DateTime.UtcNow;

      var cargo = new Cargo
      {
        Name = "test"
      };
      _dbContext.Cargoes.Add(cargo);

      var driver = new Driver
      {
        Identifier = "test"
      };
      _dbContext.Drivers.Add(driver);

      var vehicle = new Vehicle
      {
        PlateNumber = "test"
      };
      _dbContext.Vehicles.Add(vehicle);

      var contractor = new Contractor
      {
        Name = "test"
      };
      _dbContext.Contractors.Add(contractor);

      var semiTrailer = new SemiTrailer
      {
        Number = "existing number"
      };
      _dbContext.SemiTrailers.Add(semiTrailer);

      var measurement = new Measurement
      {
        Number = 1L,
        Type = MeasurementType.Double,
        Class = MeasurementClass.Service,
        Date1Utc = utcYesterday,
        Date2Utc = utcNow,
        DeviceId = 1,
        Measurement1 = 0M,
        Measurement2 = 0M,
        DeclaredWeight = 0M,
        Tare = 0M,
        NetWeight = 0M,
        Comment = "First comment",
        IsManual = true,
        IsCancelled = false,
        Cargo = cargo,
        Contractor = contractor,
        Driver = driver,
        Vehicle = vehicle,
        SemiTrailer = semiTrailer
      };
      _dbContext.Measurements.Add(measurement);

      _dbContext.SaveChanges();

      // Arrange
      var updateMeaurementCommand = new UpdateMeasurementCommand
      {
        MeasurementId = 1L,
        MeasurementDate1Utc = utcYesterday.AddMonths(-1),
        MeasurementDate2Utc = utcNow.AddMonths(-1),
        Measurement1 = 11.11M,
        Measurement2 = 22.22M,
        NetWeight = 3.14M,
        DeclaredWeight = 6.28M,
        Tare = 1M,
        Class = MeasurementClass.Outcome,
        Comment = "dummy comment",
        IsCancelled = true
      };

      // Act
      _updateMeasurementCommandHandler.Execute(_dbContext, updateMeaurementCommand, _partnerId);

      // Assert
      var createdMeasurement = _dbContext.Measurements.Single();

      Assert.AreEqual(updateMeaurementCommand.Measurement1, createdMeasurement.Measurement1,
        "Measurement1 of updated measurement should be the same like passed in command");
      Assert.AreEqual(null, createdMeasurement.Contractor, 
        "Contractor of updated measurement should be the same as before");
      Assert.AreEqual(null, createdMeasurement.Cargo, 
        "Cargo of updated measurement should be the same as before");
      Assert.AreEqual(null, createdMeasurement.Driver, 
        "Driver of updated measurement should be the same as before");
      Assert.AreEqual(null, createdMeasurement.Vehicle, 
        "Vehicle of updated measurement should be the same as before");
    }

    [TestMethod]
    public void Execute_ShouldUpdateEntitiesToNull_WhenEntitiesWithGivenIdentifiersDoNotExist()
    {
      // Setup
      var utcYesterday = DateTime.UtcNow.AddDays(-1);
      var utcNow = DateTime.UtcNow;

      var cargo = new Cargo
      {
        Name = "test"
      };
      _dbContext.Cargoes.Add(cargo);

      var driver = new Driver
      {
        Identifier = "test"
      };
      _dbContext.Drivers.Add(driver);

      var vehicle = new Vehicle
      {
        PlateNumber = "test"
      };
      _dbContext.Vehicles.Add(vehicle);

      var contractor = new Contractor
      {
        Name = "test"
      };
      _dbContext.Contractors.Add(contractor);

      var semiTrailer = new SemiTrailer
      {
        Number = "existing number"
      };
      _dbContext.SemiTrailers.Add(semiTrailer);

      var measurement = new Measurement
      {
        Number = 1L,
        Type = MeasurementType.Double,
        Class = MeasurementClass.Service,
        Date1Utc = utcYesterday,
        Date2Utc = utcNow,
        DeviceId = 1,
        Measurement1 = 0M,
        Measurement2 = 0M,
        DeclaredWeight = 0M,
        Tare = 0M,
        NetWeight = 0M,
        Comment = "First comment",
        IsManual = true,
        IsCancelled = false,
        Cargo = cargo,
        Contractor = contractor,
        Driver = driver,
        Vehicle = vehicle,
        SemiTrailer = semiTrailer
      };
      _dbContext.Measurements.Add(measurement);

      _dbContext.SaveChanges();

      // Arrange
      var updateMeaurementCommand = new UpdateMeasurementCommand
      {
        MeasurementId = 1L,
        MeasurementDate1Utc = utcYesterday.AddMonths(-1),
        MeasurementDate2Utc = utcNow.AddMonths(-1),
        Measurement1 = 11.11M,
        Measurement2 = 22.22M,
        NetWeight = 3.14M,
        DeclaredWeight = 6.28M,
        Tare = 1M,
        Class = MeasurementClass.Outcome,
        Comment = "dummy comment",
        IsCancelled = true,
        ContractorId = 55,
        CargoId = 55,
        DriverId = 55,
        VehicleId = 55
      };

      // Act
      _updateMeasurementCommandHandler.Execute(_dbContext, updateMeaurementCommand, _partnerId);

      // Assert
      var createdMeasurement = _dbContext.Measurements.Single();

      Assert.AreEqual(updateMeaurementCommand.Measurement1, createdMeasurement.Measurement1,
        "Measurement1 of updated measurement should be the same like passed in command");
      Assert.AreEqual(null, createdMeasurement.Contractor, "Contractor of updated measurement should be null");
      Assert.AreEqual(null, createdMeasurement.Cargo, "Cargo of updated measurement should be null");
      Assert.AreEqual(null, createdMeasurement.Driver, "Driver of updated measurement should be null");
      Assert.AreEqual(null, createdMeasurement.Vehicle, "Vehicle of updated measurement should be null");
    }

    [TestMethod]
    public void Execute_ShouldNotUpdateAndDoNotCreateNewSemiTrailer_WhenSemiTrailerNumberIsNotSpecified()
    {
      // Setup
      var semiTrailer = new SemiTrailer
      {
        Number = "existing number"
      };
      _dbContext.SemiTrailers.Add(semiTrailer);

      var measurement = new Measurement
      {
        Number = 1L,
        Type = MeasurementType.Double,
        Class = MeasurementClass.Service,
        Date1Utc = DateTime.UtcNow,
        Date2Utc = DateTime.UtcNow,
        DeviceId = 1,
        Measurement1 = 0M,
        Measurement2 = 0M,
        DeclaredWeight = 0M,
        Tare = 0M,
        NetWeight = 0M,
        Comment = "First comment",
        IsManual = true,
        IsCancelled = false,
        SemiTrailer = semiTrailer
      };
      _dbContext.Measurements.Add(measurement);

      _dbContext.SaveChanges();

      // Arrange
      var updateMeaurementCommand = new UpdateMeasurementCommand
      {
        MeasurementId = 1L,
        Measurement1 = 11.11M,
      };

      // Act
      _updateMeasurementCommandHandler.Execute(_dbContext, updateMeaurementCommand, _partnerId);

      // Assert
      var createdMeasurement = _dbContext.Measurements.Single();

      Assert.AreEqual(1, _dbContext.SemiTrailers.Count(), "New SemiTrailer should be added to database");
      Assert.AreEqual(semiTrailer, createdMeasurement.SemiTrailer,
        "SemiTrailer of created measurement should be the same like passed by number in command");
    }

    [TestMethod]
    public void Execute_ShouldUpdateToNewlyCreatedSemiTrailer_WhenSemiTrailerWithGivenNumberDoesNotExist()
    {
      // Setup
      var semiTrailer = new SemiTrailer
      {
        Number = "existing number"
      };
      _dbContext.SemiTrailers.Add(semiTrailer);

      var measurement = new Measurement
      {
        Number = 1L,
        Type = MeasurementType.Double,
        Class = MeasurementClass.Service,
        Date1Utc = DateTime.UtcNow,
        Date2Utc = DateTime.UtcNow,
        DeviceId = 1,
        Measurement1 = 0M,
        Measurement2 = 0M,
        DeclaredWeight = 0M,
        Tare = 0M,
        NetWeight = 0M,
        Comment = "First comment",
        IsManual = true,
        IsCancelled = false,
        SemiTrailer = semiTrailer
      };
      _dbContext.Measurements.Add(measurement);

      _dbContext.SaveChanges();

      // Arrange
      var updateMeaurementCommand = new UpdateMeasurementCommand
      {
        MeasurementId = 1L,
        Measurement1 = 11.11M,
        SemiTrailerNumber = "not existing number"
      };

      // Act
      _updateMeasurementCommandHandler.Execute(_dbContext, updateMeaurementCommand, _partnerId);

      // Assert
      var createdMeasurement = _dbContext.Measurements.Single();

      Assert.AreEqual(2, _dbContext.SemiTrailers.Count(), "New SemiTrailer should be added to database");
      Assert.AreEqual(updateMeaurementCommand.SemiTrailerNumber, createdMeasurement.SemiTrailer.Number,
        "SemiTrailer of created measurement should be the same like passed by number in command");
    }

    [TestMethod]
    public void Execute_ShouldCreateDbUpdateEventLog()
    {
      // Setup
      _dbEventLoggerMock.Setup(c => c.LogEvent(_partnerId, ctx => ctx.Measurements,
          It.IsAny<long>(), It.IsAny<Measurement>()))
        .Verifiable();

      _dbContext.Measurements.Add(new Measurement
      {
        Measurement1 = 344,
        Measurement2 = 455
      }); // generated id: 1
      _dbContext.SaveChanges();

      // Arrange
      var command = new UpdateMeasurementCommand
      {
        MeasurementId = 1,
        Measurement1 = 455,
        Measurement2 = 344
      };

      // Act
      _updateMeasurementCommandHandler.Execute(_dbContext, command, _partnerId);

      // Assert
      _dbEventLoggerMock.Verify(c => c.LogEvent(_partnerId, ctx => ctx.Measurements,
        It.IsAny<long>(), It.IsAny<Measurement>(), It.IsAny<Measurement>()), Times.Once);
    }
  }
}
