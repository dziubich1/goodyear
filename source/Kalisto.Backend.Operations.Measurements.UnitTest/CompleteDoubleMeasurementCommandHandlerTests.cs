﻿using Kalisto.Backend.DataAccess.Effort;
using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.DataAccess.Model.Logging;
using Kalisto.Backend.Operations.Measurements.Commands;
using Kalisto.Communication.Core.Logging;
using Kalisto.Communication.Core.Registries;
using Kalisto.Configuration.Devices;
using Kalisto.Configuration.Devices.Connection;
using Kalisto.Configuration.Enums;
using Kalisto.Core;
using Kalisto.Devices.Camera.Models;
using Kalisto.Devices.Camera.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Media.Imaging;
using Kalisto.Backend.StateMachine;
using Kalisto.Configuration.CompanyInfo;
using Kalisto.Logging;
using Kalisto.Configuration.MeasurementExportSettings;

namespace Kalisto.Backend.Operations.Measurements.UnitTest
{
  [TestClass]
  public class CompleteDoubleMeasurementCommandHandlerTests
  {
    private IDbContext _dbContext;

    private readonly string _partnerId = "unknown";

    private readonly User _user = new User { Name = "username" };

    private Mock<IDbUpdateEventLogger> _dbEventLoggerMock;

    private Mock<ISystemEventLogger> _eventLoggerMock;

    private Mock<IPartnerRegistryReader> _partnerRegistryReaderMock;

    private Mock<IMeasurementPhotoService> _measurementPhotoServiceMock;

    private Mock<ICameraService> _cameraServiceMock;

    private Mock<IXmlExporterService> _xmlExporterService;

    private Mock<IMeasurementStateMachineRegistry> _stateMachineRegistryMock;

    private Mock<ServerDeviceConfigurationManager> _deviceConfigurationManagerMock;

    private Mock<ServerDeviceConfiguration> _deviceConfigurationMock;

    private Mock<CompanyInfoConfigurationManager> _companyInfoConfigurationManagerMock;

    // component under tests
    private CompleteDoubleMeasurementCommandHandler _completeDoubleMeasurementCommandHandler;

    [TestInitialize]
    public void Initialize()
    {
      var effortConnection = Effort.DbConnectionFactory.CreateTransient();
      _dbContext = new EffortDbContext(effortConnection);

      _dbEventLoggerMock = new Mock<IDbUpdateEventLogger>();
      _eventLoggerMock = new Mock<ISystemEventLogger>();
      _partnerRegistryReaderMock = new Mock<IPartnerRegistryReader>();
      _partnerRegistryReaderMock.Setup(c => c.GetCurrentUser(_partnerId)).Returns(_user);

      _measurementPhotoServiceMock = new Mock<IMeasurementPhotoService>();
      _cameraServiceMock = new Mock<ICameraService>();
      _xmlExporterService = new Mock<IXmlExporterService>();
      _stateMachineRegistryMock = new Mock<IMeasurementStateMachineRegistry>();

      _deviceConfigurationMock = new Mock<ServerDeviceConfiguration>();
      _deviceConfigurationMock.Object.Cameras = new List<CameraConfiguration>
        {
          new CameraConfiguration
          {
            Id = 2,
            WeightMeterId = 1,
            Type = DeviceType.Transparent,
            ConnectionType = ConnectionType.Https,
            ConnectionConfiguration = new HttpConnectionConfiguration
            {
              Url = "dummy",
              Username = "dummy",
              Password = "dummy"
            },
            IsActive = true
          }
        };

      _deviceConfigurationManagerMock = new Mock<ServerDeviceConfigurationManager>("dummy_string", "dummy_string");
      _deviceConfigurationManagerMock.Setup(c => c.Load()).Returns(_deviceConfigurationMock.Object);
      _companyInfoConfigurationManagerMock = new Mock<CompanyInfoConfigurationManager>("dummy_string", "dummy_string");
      var logger = new Mock<ILogger>();

      _completeDoubleMeasurementCommandHandler = new CompleteDoubleMeasurementCommandHandler(_dbEventLoggerMock.Object, _eventLoggerMock.Object,
        _partnerRegistryReaderMock.Object, _measurementPhotoServiceMock.Object, _cameraServiceMock.Object, _deviceConfigurationManagerMock.Object,
        _companyInfoConfigurationManagerMock.Object, logger.Object, _xmlExporterService.Object, _stateMachineRegistryMock.Object);
    }

    [TestCleanup]
    public void CleanUp()
    {
      _dbContext.Dispose();
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void Execute_ShouldThrowArgumentException_WhenMeasurementWithGivenIdDoesNotExist()
    {
      // Arrange
      var notExistingMeasurementId = 44;
      var completeDoubleMeasurementCommand = new CompleteDoubleMeasurementCommand
      {
        MeasurementId = notExistingMeasurementId
      };

      // Act & Assert
      _completeDoubleMeasurementCommandHandler.Execute(_dbContext, completeDoubleMeasurementCommand, _partnerId);
    }

    [TestMethod]
    public void Execute_ShouldReturnIdentifierOfCompletedMeasurement()
    {
      // Setup
      var utcYesterday = DateTime.UtcNow.AddDays(-1);
      var utcNow = DateTime.UtcNow;
      var measurement = new Measurement
      {
        Number = 1L,
        Type = MeasurementType.Double,
        Class = null,
        Date1Utc = utcYesterday,
        Date2Utc = null,
        DeviceId = 1,
        Measurement1 = 55.44M,
        Measurement2 = null,
        DeclaredWeight = 112.69M,
        Tare = 0,
        NetWeight = 0,
        Comment = "First comment",
        IsManual = true,
        IsCancelled = false,
      };

      _dbContext.Measurements.Add(measurement);
      _dbContext.SaveChanges();

      // Arrange
      var completeDoubleMeasurementCommand = new CompleteDoubleMeasurementCommand
      {
        MeasurementId = 1,
        MeasurementNumber = 1,
        Class = MeasurementClass.Outcome,
        DeviceId = 2,
        DeclaredWeight = 533.55M,
        Measurement = 444.44M,
        MeasurementDateUtc = utcNow,
        WeighingThreshold = 3.14M,
        Comment = "Second comment",
      };

      // Act
      _completeDoubleMeasurementCommandHandler.Execute(_dbContext, completeDoubleMeasurementCommand, _partnerId);

      // Assert
      var completedMeasurement = _dbContext.Measurements.Single();
      var expectedMaxWeight = new[] { measurement.Measurement1, completeDoubleMeasurementCommand.Measurement }.Max();
      var expectedMinWeight = new[] { measurement.Measurement1, completeDoubleMeasurementCommand.Measurement }.Min();

      Assert.AreEqual(1L, completedMeasurement.Id);
      Assert.AreEqual(1L, completedMeasurement.Number);
      Assert.AreEqual(2, completedMeasurement.DeviceId);
      Assert.AreEqual(completeDoubleMeasurementCommand.Class, completedMeasurement.Class);
      Assert.AreEqual(measurement.Date1Utc, completedMeasurement.Date1Utc);
      Assert.AreEqual(completeDoubleMeasurementCommand.MeasurementDateUtc, completedMeasurement.Date2Utc);
      Assert.AreEqual(measurement.Measurement1, completedMeasurement.Measurement1);
      Assert.AreEqual(completeDoubleMeasurementCommand.Measurement, completedMeasurement.Measurement2);
      Assert.AreEqual(completeDoubleMeasurementCommand.DeclaredWeight, completedMeasurement.DeclaredWeight);
      Assert.AreEqual(completeDoubleMeasurementCommand.WeighingThreshold, completedMeasurement.WeighingThreshold);
      Assert.AreEqual(expectedMinWeight, completedMeasurement.Tare);
      Assert.AreEqual(expectedMaxWeight - expectedMinWeight, completedMeasurement.NetWeight);
      Assert.AreEqual(completeDoubleMeasurementCommand.Comment, completedMeasurement.Comment);
      Assert.AreEqual(null, completedMeasurement.Cargo);
      Assert.AreEqual(null, completedMeasurement.Driver);
      Assert.AreEqual(null, completedMeasurement.Vehicle);
      Assert.AreEqual(null, completedMeasurement.Contractor);
      Assert.AreEqual(null, completedMeasurement.SemiTrailer);
      Assert.AreEqual(null, completedMeasurement.Operator);
      Assert.AreEqual(MeasurementType.Double, completedMeasurement.Type);
    }

    [TestMethod]
    [ExpectedException(typeof(InvalidOperationException))]
    public void Execute_ShouldThrowInvalidOperationException_WhenMeasurementTypeIsSingle()
    {
      var singleMeasurement = new Measurement
      {
        Type = MeasurementType.Single,
        Number = 1,
      };
      _dbContext.Measurements.Add(singleMeasurement);
      _dbContext.SaveChanges();

      // Arrange
      var completeDoubleMeasurementCommand = new CompleteDoubleMeasurementCommand
      {
        MeasurementId = 1,
        MeasurementNumber = 1,
        Measurement = 123.33M
      };

      // Act & Assert
      _completeDoubleMeasurementCommandHandler.Execute(_dbContext, completeDoubleMeasurementCommand, _partnerId);
    }

    [TestMethod]
    public void Execute_ShouldAssignProperEntitiesToMeasurement_WhenEntitiesWithGivenIdentifiersExists()
    {
      // Setup
      var utcYesterday = DateTime.UtcNow.AddDays(-1);
      var utcNow = DateTime.UtcNow;

      var cargo = new Cargo
      {
        Name = "test"
      };
      _dbContext.Cargoes.Add(cargo);

      var driver = new Driver
      {
        Identifier = "test"
      };
      _dbContext.Drivers.Add(driver);

      var vehicle = new Vehicle
      {
        PlateNumber = "test"
      };
      _dbContext.Vehicles.Add(vehicle);

      var contractor = new Contractor
      {
        Name = "test"
      };
      _dbContext.Contractors.Add(contractor);

      var semiTrailer = new SemiTrailer
      {
        Number = "existing number"
      };
      _dbContext.SemiTrailers.Add(semiTrailer);

      _dbContext.Users.Add(_user);

      var measurement = new Measurement
      {
        Number = 1L,
        Type = MeasurementType.Double,
        Class = null,
        Date1Utc = utcYesterday,
        Date2Utc = null,
        DeviceId = 1,
        Measurement1 = 55.44M,
        Measurement2 = null,
        DeclaredWeight = 112.69M,
        Tare = 0,
        NetWeight = 0,
        Comment = "First comment",
        IsManual = true,
        IsCancelled = false,
        Cargo = cargo,
        Driver = driver,
        Vehicle = vehicle,
        Contractor = contractor,
        SemiTrailer = semiTrailer,
        Operator = _user
      };

      _dbContext.Measurements.Add(measurement);
      _dbContext.SaveChanges();

      // Arrange
      var completeDoubleMeasurementCommand = new CompleteDoubleMeasurementCommand
      {
        MeasurementId = 1,
        MeasurementNumber = 1,
        CargoId = cargo.Id,
        DriverId = driver.Id,
        VehicleId = vehicle.Id,
        ContractorId = contractor.Id,
        SemiTrailerNumber = semiTrailer.Number
      };

      // Act
      _completeDoubleMeasurementCommandHandler.Execute(_dbContext, completeDoubleMeasurementCommand, _partnerId);

      // Assert
      var completedMeasurement = _dbContext.Measurements.Single();
      var expectedMaxWeight = new[] { measurement.Measurement1, completeDoubleMeasurementCommand.Measurement }.Max();
      var expectedMinWeight = new[] { measurement.Measurement1, completeDoubleMeasurementCommand.Measurement }.Min();

      Assert.AreEqual(cargo, completedMeasurement.Cargo);
      Assert.AreEqual(driver, completedMeasurement.Driver);
      Assert.AreEqual(vehicle, completedMeasurement.Vehicle);
      Assert.AreEqual(contractor, completedMeasurement.Contractor);
      Assert.AreEqual(semiTrailer, completedMeasurement.SemiTrailer);
      Assert.AreEqual(_user, completedMeasurement.Operator);
    }

    [TestMethod]
    public void Execute_ShouldAssignEntitiesToNull_WhenEntitiesWithGivenIdentifiersDoNotExist()
    {
      // Setup
      var cargo = new Cargo
      {
        Name = "test"
      };
      _dbContext.Cargoes.Add(cargo);

      var driver = new Driver
      {
        Identifier = "test"
      };
      _dbContext.Drivers.Add(driver);

      var vehicle = new Vehicle
      {
        PlateNumber = "test"
      };
      _dbContext.Vehicles.Add(vehicle);

      var contractor = new Contractor
      {
        Name = "test"
      };
      _dbContext.Contractors.Add(contractor);

      var semiTrailer = new SemiTrailer
      {
        Number = "existing number"
      };
      _dbContext.SemiTrailers.Add(semiTrailer);

      _dbContext.Users.Add(_user);

      var measurement = new Measurement
      {
        Number = 1L,
        Type = MeasurementType.Double,
        Class = null,
        Date1Utc = DateTime.UtcNow.AddDays(-1),
        Date2Utc = null,
        DeviceId = 1,
        Measurement1 = 55.44M,
        Measurement2 = null,
        DeclaredWeight = 112.69M,
        Tare = 0,
        NetWeight = 0,
        Comment = "First comment",
        IsManual = true,
        IsCancelled = false,
        Cargo = cargo,
        Driver = driver,
        Vehicle = vehicle,
        Contractor = contractor,
        SemiTrailer = semiTrailer,
        Operator = _user
      };

      _dbContext.Measurements.Add(measurement);

      _dbContext.SaveChanges(); // generated id: 1

      var partnerId = "dummy";
      var dummyUser = new User
      {
        Name = "dummy"
      };
      _partnerRegistryReaderMock.Setup(c => c.GetCurrentUser(partnerId)).Returns(dummyUser);

      // Arrange
      var completeDoubleMeasurementCommand = new CompleteDoubleMeasurementCommand
      {
        CargoId = 55, // not existing id
        DriverId = 55, // not existing id
        VehicleId = 55, // not existing id
        ContractorId = 55, // not existing id
        SemiTrailerNumber = string.Empty,
        MeasurementId = 1,
        MeasurementNumber = 1,
      };
      // Act
      _completeDoubleMeasurementCommandHandler.Execute(_dbContext,
        completeDoubleMeasurementCommand, partnerId);

      var createdMeasurement = _dbContext.Measurements.Single();

      // Assert
      Assert.AreEqual(1, _dbContext.Measurements.Count(),
        "Measurements table should contain one measurement");
      Assert.AreEqual(null, createdMeasurement.Cargo,
        "Cargo of completed measurement should be null because of not existing CargoId in command");
      Assert.AreEqual(null, createdMeasurement.Driver,
        "Driver of completed measurement should be null because of not existing DriverId in command");
      Assert.AreEqual(null, createdMeasurement.Vehicle,
        "Vehicle of completed measurement should be null because of not existing VehicleId in command");
      Assert.AreEqual(null, createdMeasurement.Contractor,
        "Contractor of completed measurement should be null because of not existing ContractorId in command");
      Assert.AreEqual(null, createdMeasurement.SemiTrailer,
        "SemiTrailer of completed measurement should be null because of empty SemiTrailerNumber in command");
      Assert.AreEqual(null, createdMeasurement.Operator,
        "Operator of completed measurement should be null because of not existing user with name taken from passed partner id");
    }

    [TestMethod]
    public void Execute_ShouldAssignNewlyCreatedSemiTrailerToMeasurement_WhenSemiTrailerWithGivenNumberDoesNotExist()
    {
      // Setup
      var semiTrailer = new SemiTrailer
      {
        Number = "existing number"
      };
      _dbContext.SemiTrailers.Add(semiTrailer);

      var measurement = new Measurement
      {
        Type = MeasurementType.Double,
        Number = 1L
      };
      _dbContext.Measurements.Add(measurement);

      _dbContext.SaveChanges();

      // Arrange
      var completeDoubleMeasurementCommand = new CompleteDoubleMeasurementCommand
      {
        MeasurementNumber = 1L,
        MeasurementId = 1L,
        SemiTrailerNumber = "not existing number"
      };

      // Act & Assert
      Assert.AreEqual(1, _dbContext.SemiTrailers.Count());
      _completeDoubleMeasurementCommandHandler.Execute(_dbContext,
        completeDoubleMeasurementCommand, _partnerId);

      var createdMeasurement = _dbContext.Measurements.Single();

      Assert.AreEqual(2, _dbContext.SemiTrailers.Count(), "New SemiTrailer should be added to database");
      Assert.AreEqual(completeDoubleMeasurementCommand.SemiTrailerNumber, createdMeasurement.SemiTrailer.Number,
        "SemiTrailer of completed measurement should be the same like passed by number in command");
    }

    [TestMethod]
    public void Execute_ShouldCallMeasurementPhotoServiceToStorePhoto_WithValidParameters()
    {
      // Setup
      var measurement = new Measurement
      {
        Type = MeasurementType.Double,
        Number = 1L
      };
      _dbContext.Measurements.Add(measurement);

      _dbContext.SaveChanges();

      var cameraReponse = new Mock<CameraResponse>();
      cameraReponse.Object.Image = new BitmapImage();
      _cameraServiceMock.Setup(cs => cs.TakeSnapshot(It.IsAny<CameraInfo>())).Returns(cameraReponse.Object);

      // Arrange
      var completeDoubleMeasurementCommand = new CompleteDoubleMeasurementCommand
      {
        MeasurementId = 1L,
        MeasurementNumber = 1L,
        Measurement = 123.55M,
        DeviceId = 1
      };

      // Act
      _completeDoubleMeasurementCommandHandler.Execute(_dbContext,
        completeDoubleMeasurementCommand, _partnerId);

      _measurementPhotoServiceMock.Verify(mps => mps.Store(It.IsAny<BitmapImage>(), It.IsAny<string>(), It.IsAny<long>(), It.IsAny<DateTime>()), Times.Once());
    }

    [TestMethod]
    public void Execute_ShouldCreateDbUpdateEventLog()
    {
      // Setup
      _dbEventLoggerMock.Setup(c => c.LogEvent(_partnerId, ctx => ctx.Measurements,
          It.IsAny<long>(), It.IsAny<Measurement>()))
        .Verifiable();

      _dbContext.Measurements.Add(new Measurement
      {
        Measurement1 = 344,
        Number = 1L,
        Type = MeasurementType.Double
      }); // generated id: 1
      _dbContext.SaveChanges();

      // Arrange
      var command = new CompleteDoubleMeasurementCommand
      {
        MeasurementId = 1L,
        MeasurementNumber = 1L,
        Measurement = 344,
        DeviceId = 1
      };

      // Act
      _completeDoubleMeasurementCommandHandler.Execute(_dbContext, command, _partnerId);

      // Assert
      _dbEventLoggerMock.Verify(c => c.LogEvent(_partnerId, ctx => ctx.Measurements,
        It.IsAny<long>(), It.IsAny<Measurement>(), It.IsAny<Measurement>()), Times.Once);
    }
  }
}
