﻿using Kalisto.Backend.DataAccess.Effort;
using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.DataAccess.Model.Logging;
using Kalisto.Backend.Operations.Measurements.Commands;
using Kalisto.Communication.Core.Logging;
using Kalisto.Communication.Core.Registries;
using Kalisto.Configuration.Devices;
using Kalisto.Configuration.Devices.Connection;
using Kalisto.Configuration.Enums;
using Kalisto.Core;
using Kalisto.Devices.Camera.Models;
using Kalisto.Devices.Camera.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using Kalisto.Backend.StateMachine;
using Kalisto.Configuration.CompanyInfo;
using Kalisto.Logging;

namespace Kalisto.Backend.Operations.Measurements.UnitTest
{
  [TestClass]
  public class CreateDoubleMeasurementCommandHandlerTests
  {
    private IDbContext _dbContext;

    private readonly string _partnerId = "unknown";

    private readonly User _user = new User { Name = "username" };

    private Mock<IDbInsertEventLogger> _dbEventLoggerMock;

    private Mock<ISystemEventLogger> _eventLoggerMock;

    private Mock<IPartnerRegistryReader> _partnerRegistryReaderMock;

    private Mock<IMeasurementPhotoService> _measurementPhotoServiceMock;

    private Mock<ICameraService> _cameraServiceMock;

    private Mock<IMeasurementStateMachineRegistry> _stateMachineRegistryMock;

    private Mock<ServerDeviceConfigurationManager> _deviceConfigurationManagerMock;

    private Mock<ServerDeviceConfiguration> _deviceConfigurationMock;

    private Mock<CompanyInfoConfigurationManager> _companyInfoConfigurationManagerMock;

    // component under tests
    private CreateDoubleMeasurementCommandHandler _createDoubleMeasurementCommandHandler;

    [TestInitialize]
    public void Initialize()
    {
      var effortConnection = Effort.DbConnectionFactory.CreateTransient();
      _dbContext = new EffortDbContext(effortConnection);

      _dbEventLoggerMock = new Mock<IDbInsertEventLogger>();
      _eventLoggerMock = new Mock<ISystemEventLogger>();
      _partnerRegistryReaderMock = new Mock<IPartnerRegistryReader>();
      _partnerRegistryReaderMock.Setup(c => c.GetCurrentUser(_partnerId)).Returns(_user);

      _measurementPhotoServiceMock = new Mock<IMeasurementPhotoService>();
      _cameraServiceMock = new Mock<ICameraService>();
      _stateMachineRegistryMock = new Mock<IMeasurementStateMachineRegistry>();

      _deviceConfigurationMock = new Mock<ServerDeviceConfiguration>();
      _deviceConfigurationMock.Object.Cameras = new List<CameraConfiguration>
        {
          new CameraConfiguration
          {
            Id = 2,
            WeightMeterId = 1,
            Type = DeviceType.Transparent,
            ConnectionType = ConnectionType.Https,
            ConnectionConfiguration = new HttpConnectionConfiguration
            {
              Url = "dummy",
              Username = "dummy",
              Password = "dummy"
            },
            IsActive = true
          }
        };

      _deviceConfigurationManagerMock = new Mock<ServerDeviceConfigurationManager>("dummy_string", "dummy_string");
      _deviceConfigurationManagerMock.Setup(c => c.Load()).Returns(_deviceConfigurationMock.Object);
      _companyInfoConfigurationManagerMock = new Mock<CompanyInfoConfigurationManager>("dummy_string", "dummy_string");
      var logger = new Mock<ILogger>();

      _createDoubleMeasurementCommandHandler = new CreateDoubleMeasurementCommandHandler(_dbEventLoggerMock.Object, _eventLoggerMock.Object,
        _partnerRegistryReaderMock.Object, _measurementPhotoServiceMock.Object, _cameraServiceMock.Object, _deviceConfigurationManagerMock.Object,
        _companyInfoConfigurationManagerMock.Object, logger.Object, _stateMachineRegistryMock.Object);
    }

    [TestCleanup]
    public void CleanUp()
    {
      _dbContext.Dispose();
    }

    [TestMethod]
    public void Execute_ShouldReturnIdentifierOfNewlyCreatedMeasurement()
    {
      // Setup
      var utcNow = DateTime.UtcNow;

      // Arrange
      var createDoubleMeasurementCommand = new CreateDoubleMeasurementCommand
      {
        IsManual = true,
        Measurement = 666.69M,
        MeasurementDateUtc = utcNow,
        DeclaredWeight = 112.11M,
        DeviceId = 1,
        Comment = "dummy string"
      };

      // Act
      _createDoubleMeasurementCommandHandler.Execute(_dbContext,
        createDoubleMeasurementCommand, _partnerId);

      // Assert
      Assert.AreEqual(1, _dbContext.Measurements.Count(),
        "Measurements table should contain one measurement");

      var createdMeasurement = _dbContext.Measurements.Single();
      Assert.AreEqual(1, createdMeasurement.Id,
        "Id of created measurement should be auto generated");
      Assert.AreEqual(createDoubleMeasurementCommand.IsManual, createdMeasurement.IsManual,
        "IsManual of created measurement should be the same like in command");
      Assert.AreEqual(createDoubleMeasurementCommand.Measurement, createdMeasurement.Measurement1,
        "First measurement of created measurement should be the same like in command");
      Assert.AreEqual(null, createdMeasurement.Measurement2,
        "Second measurement of created measurement should be null");
      Assert.AreEqual(createDoubleMeasurementCommand.MeasurementDateUtc, createdMeasurement.Date1Utc,
        "Date of first measurement in created measurement should be the same like in command");
      Assert.AreEqual(null, createdMeasurement.Date2Utc,
        "Date of second measurement in created measurement should be null");
      Assert.AreEqual(createDoubleMeasurementCommand.DeclaredWeight, createdMeasurement.DeclaredWeight,
        "DeclaredWeight of created measurement should be the same like in command");
      Assert.AreEqual(false, createdMeasurement.IsCancelled,
       "IsCancelled of created measurement should be initialized with false value");
      Assert.AreEqual(createDoubleMeasurementCommand.Comment, createdMeasurement.Comment,
        "Comment of created measurement should be the same like in command");
      Assert.AreEqual(createDoubleMeasurementCommand.DeviceId, createdMeasurement.DeviceId,
      "DeviceId of created measurement should be the same like in command");
      Assert.AreEqual(1L, createdMeasurement.Number,
      "Number of created measurement should be set as 1 because it is first measurement");
      Assert.AreEqual(null, createdMeasurement.Cargo);
      Assert.AreEqual(null, createdMeasurement.Driver);
      Assert.AreEqual(null, createdMeasurement.Vehicle);
      Assert.AreEqual(null, createdMeasurement.Contractor);
      Assert.AreEqual(null, createdMeasurement.SemiTrailer);
      Assert.AreEqual(null, createdMeasurement.Operator);
      Assert.AreEqual(MeasurementType.Double, createdMeasurement.Type);
      Assert.AreEqual(0, createdMeasurement.Tare);
      Assert.AreEqual(0, createdMeasurement.NetWeight);
    }

    [TestMethod]
    public void Execute_ShouldAutoincrementNumberOfNewlyCreatedMeasurements()
    {
      // Arrange
      for (int i = 0; i < 3; i++)
      {
        // Arrange
        var createDoubleMeasurementCommand = new CreateDoubleMeasurementCommand
        {
          IsManual = true,
          Measurement = 666.69M,
          MeasurementDateUtc = DateTime.UtcNow,
          DeclaredWeight = 112.11M,
          DeviceId = 1,
          Comment = "dummy string"
        };

        // Act
        _createDoubleMeasurementCommandHandler.Execute(_dbContext,
          createDoubleMeasurementCommand, _partnerId);
      }

      var createdMeasurementNumbers = _dbContext.Measurements.Select(c => c.Number);

      long expectedNumber = 1L;
      foreach (var number in createdMeasurementNumbers)
      {
        Assert.AreEqual(expectedNumber, number);
        expectedNumber++;
      }
    }

    [TestMethod]
    public void Execute_ShouldAssignProperEntitiesToMeasurement_WhenEntitiesWithGivenIdentifiersExists()
    {
      // Setup
      var cargo = new Cargo
      {
        Name = "test"
      };
      _dbContext.Cargoes.Add(cargo);

      var driver = new Driver
      {
        Identifier = "test"
      };
      _dbContext.Drivers.Add(driver);

      var vehicle = new Vehicle
      {
        PlateNumber = "test"
      };
      _dbContext.Vehicles.Add(vehicle);

      var contractor = new Contractor
      {
        Name = "test"
      };
      _dbContext.Contractors.Add(contractor);

      var semiTrailer = new SemiTrailer
      {
        Number = "existing number"
      };
      _dbContext.SemiTrailers.Add(semiTrailer);

      _dbContext.Users.Add(_user);

      _dbContext.SaveChanges(); // generated id: 1

      // Arrange
      var createDoubleMeasurementCommand = new CreateDoubleMeasurementCommand
      {
        CargoId = cargo.Id, // not existing id
        DriverId = driver.Id, // not existing id
        VehicleId = vehicle.Id, // not existing id
        ContractorId = contractor.Id, // not existing id
        SemiTrailerNumber = "existing number"
      };

      // Act
      _createDoubleMeasurementCommandHandler.Execute(_dbContext,
        createDoubleMeasurementCommand, _partnerId);

      var createdMeasurement = _dbContext.Measurements.Single();

      // Assert
      Assert.AreEqual(1, _dbContext.Measurements.Count(),
        "Measurements table should contain one measurement");
      Assert.AreEqual(cargo, createdMeasurement.Cargo,
        "Cargo of created measurement should be the same like passed by id in command");
      Assert.AreEqual(driver, createdMeasurement.Driver,
        "Driver of created measurement should be the same like passed by id in command");
      Assert.AreEqual(vehicle, createdMeasurement.Vehicle,
        "Vehicle of created measurement should be the same like passed by id in command");
      Assert.AreEqual(contractor, createdMeasurement.Contractor,
        "Contractor of created measurement should be the same like passed by id in command");
      Assert.AreEqual(semiTrailer, createdMeasurement.SemiTrailer,
        "SemiTrailer of created measurement should be the same like passed by number in command");
      Assert.AreEqual(_user, createdMeasurement.Operator,
      "Operator of created measurement should be assigned from passed partner id");
    }

    [TestMethod]
    public void Execute_ShouldAssignEntitiesToNull_WhenEntitiesWithGivenIdentifiersDoNotExist()
    {
      // Setup
      var cargo = new Cargo
      {
        Name = "test"
      };
      _dbContext.Cargoes.Add(cargo);

      var driver = new Driver
      {
        Identifier = "test"
      };
      _dbContext.Drivers.Add(driver);

      var vehicle = new Vehicle
      {
        PlateNumber = "test"
      };
      _dbContext.Vehicles.Add(vehicle);

      var contractor = new Contractor
      {
        Name = "test"
      };
      _dbContext.Contractors.Add(contractor);

      var semiTrailer = new SemiTrailer
      {
        Number = "existing number"
      };
      _dbContext.SemiTrailers.Add(semiTrailer);

      _dbContext.SaveChanges(); // generated id: 1

      // Arrange
      var createDoubleMeasurementCommand = new CreateDoubleMeasurementCommand
      {
        CargoId = 55, // not existing id
        DriverId = 55, // not existing id
        VehicleId = 55, // not existing id
        ContractorId = 55, // not existing id
        SemiTrailerNumber = string.Empty
      };
      // Act
      _createDoubleMeasurementCommandHandler.Execute(_dbContext,
        createDoubleMeasurementCommand, _partnerId);

      var createdMeasurement = _dbContext.Measurements.Single();

      // Assert
      Assert.AreEqual(1, _dbContext.Measurements.Count(),
        "Measurements table should contain one measurement");
      Assert.AreEqual(null, createdMeasurement.Cargo,
        "Cargo of created measurement should be null because of not existing CargoId in command");
      Assert.AreEqual(null, createdMeasurement.Driver,
        "Driver of created measurement should be null because of not existing DriverId in command");
      Assert.AreEqual(null, createdMeasurement.Vehicle,
        "Vehicle of created measurement should be null because of not existing VehicleId in command");
      Assert.AreEqual(null, createdMeasurement.Contractor,
        "Contractor of created measurement should be null because of not existing ContractorId in command");
      Assert.AreEqual(null, createdMeasurement.SemiTrailer,
        "SemiTrailer of created measurement should be null because of empty SemiTrailerNumber in command");
      Assert.AreEqual(null, createdMeasurement.Operator,
        "Operator of created measurement should be null because of not existing user with name taken from passed partner id");
    }

    [TestMethod]
    public void Execute_ShouldAssignNewlyCreatedSemiTrailerToMeasurement_WhenSemiTrailerWithGivenNumberDoesNotExist()
    {
      // Setup
      var semiTrailer = new SemiTrailer
      {
        Number = "existing number"
      };
      _dbContext.SemiTrailers.Add(semiTrailer);
      _dbContext.SaveChanges();

      // Arrange
      var createDoubleMeasurementCommand = new CreateDoubleMeasurementCommand
      {
        SemiTrailerNumber = "not existing number"
      };

      // Act & Assert
      Assert.AreEqual(1, _dbContext.SemiTrailers.Count());
      _createDoubleMeasurementCommandHandler.Execute(_dbContext,
        createDoubleMeasurementCommand, _partnerId);

      var createdMeasurement = _dbContext.Measurements.Single();

      Assert.AreEqual(2, _dbContext.SemiTrailers.Count(), "New SemiTrailer should be added to database");
      Assert.AreEqual(createDoubleMeasurementCommand.SemiTrailerNumber, createdMeasurement.SemiTrailer.Number,
        "SemiTrailer of created measurement should be the same like passed by number in command");
    }

    [TestMethod]
    public void Execute_ShouldCallMeasurementPhotoServiceToStorePhoto_WithValidParameters()
    {
      // Setup
      var cameraReponse = new Mock<CameraResponse>();
      cameraReponse.Object.Image = new BitmapImage();
      _cameraServiceMock.Setup(cs => cs.TakeSnapshot(It.IsAny<CameraInfo>())).Returns(cameraReponse.Object);

      // Arrange
      var createMeasurementCommand = new CreateDoubleMeasurementCommand
      {
        Measurement = 123.55M,
        DeviceId = 1
      };

      // Act
      _createDoubleMeasurementCommandHandler.Execute(_dbContext,
        createMeasurementCommand, _partnerId);

      _measurementPhotoServiceMock.Verify(mps => mps.Store(It.IsAny<BitmapImage>(), It.IsAny<string>(), It.IsAny<long>(), It.IsAny<DateTime>()), Times.Once());
    }

    [TestMethod]
    public void Execute_ShouldCreateDbInsertEventLog()
    {
      // Setup
      _dbEventLoggerMock.Setup(c => c.LogEvent(_partnerId, ctx => ctx.Measurements,
          It.IsAny<int>(), It.IsAny<Measurement>()))
        .Verifiable();

      // Arrange
      var createMeasurementCommand = new CreateDoubleMeasurementCommand
      {
        Measurement = 123.55M
      };

      // Act
      _createDoubleMeasurementCommandHandler.Execute(_dbContext,
        createMeasurementCommand, _partnerId);

      // Assert
      _dbEventLoggerMock.Verify(c => c.LogEvent(_partnerId, ctx => ctx.Measurements,
        1, It.IsAny<Measurement>()), Times.Once);
    }
  }
}
