﻿using Kalisto.Backend.DataAccess.Effort;
using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.DataAccess.Model.Logging;
using Kalisto.Backend.Operations.Measurements.Commands;
using Kalisto.Communication.Core.Logging;
using Kalisto.Core;
using Kalisto.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Linq;

namespace Kalisto.Backend.Operations.Measurements.UnitTest
{
  [TestClass]
  public class DeleteMeasurementByIdCommandHandlerTests
  {
    private IDbContext _dbContext;

    private readonly string _partnerId = "unknown";

    private Mock<IDbDeleteEventLogger> _dbEventLoggerMock;

    private Mock<ISystemEventLogger> _eventLoggerMock;

    private Mock<IXmlExporterService> _xmlExporterService;

    // component under tests
    private DeleteMeasurementByIdCommandHandler _deleteMeasurementByIdCommandHandler;

    [TestInitialize]
    public void Initialize()
    {
      var effortConnection = Effort.DbConnectionFactory.CreateTransient();
      _dbContext = new EffortDbContext(effortConnection);

      _dbEventLoggerMock = new Mock<IDbDeleteEventLogger>();
      _eventLoggerMock = new Mock<ISystemEventLogger>();
      _xmlExporterService = new Mock<IXmlExporterService>();

      var logger = new Mock<ILogger>();

      _deleteMeasurementByIdCommandHandler = new DeleteMeasurementByIdCommandHandler(_dbEventLoggerMock.Object, _eventLoggerMock.Object, logger.Object, _xmlExporterService.Object);
    }

    [TestCleanup]
    public void CleanUp()
    {
      _dbContext.Dispose();
    }

    [TestMethod]
    public void Execute_ShouldRemoveMeasurement()
    {
      // Setup
      _dbContext.Measurements.Add(new Measurement());  // generated id: 1
      _dbContext.SaveChanges();

      // Arrange
      var command = new DeleteMeasurementByIdCommand
      {
        MeasurementId = 1
      };

      // Act
      _deleteMeasurementByIdCommandHandler.Execute(_dbContext, command, _partnerId);

      // Assert
      Assert.AreEqual(0, _dbContext.Measurements.Count());
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void Execute_ShouldThrowArgumentException_WhenMeasurementWithGivenIdDoesNotExist()
    {
      // Arrange
      var notExistingMeasurementId = 44;
      var deleteMeasurementByIdCommand = new DeleteMeasurementByIdCommand
      {
        MeasurementId = notExistingMeasurementId
      };

      // Act & Assert
      _deleteMeasurementByIdCommandHandler.Execute(_dbContext, deleteMeasurementByIdCommand, _partnerId);
    }

    [TestMethod]
    public void Execute_ShouldCreateDbDeleteEventLog()
    {
      // Setup
      _dbEventLoggerMock.Setup(c => c.LogEvent(_partnerId, ctx => ctx.Measurements,
          It.IsAny<long>(), null, It.IsAny<Measurement>()))
        .Verifiable();

      _dbContext.Measurements.Add(new Measurement()); // generated id: 1
      _dbContext.SaveChanges();

      // Arrange
      var command = new DeleteMeasurementByIdCommand
      {
        MeasurementId = 1
      };

      // Act
      _deleteMeasurementByIdCommandHandler.Execute(_dbContext, command, "unknown");

      // Assert
      _dbEventLoggerMock.Verify(c => c.LogEvent(_partnerId, ctx => ctx.Measurements,
        It.IsAny<long>(), null, It.IsAny<Measurement>()), Times.Once);
    }
  }
}
