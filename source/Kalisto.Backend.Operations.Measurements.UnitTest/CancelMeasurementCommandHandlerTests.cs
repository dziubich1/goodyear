﻿using System;
using System.Linq;
using Kalisto.Backend.DataAccess.Effort;
using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.DataAccess.Model.Logging;
using Kalisto.Backend.Operations.Measurements.Commands;
using Kalisto.Communication.Core.Logging;
using Kalisto.Core;
using Kalisto.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Kalisto.Backend.Operations.Measurements.UnitTest
{
  [TestClass]
  public class CancelMeasurementCommandHandlerTests
  {
    private IDbContext _dbContext;

    private readonly string _partnerId = "unknown";

    private Mock<IDbUpdateEventLogger> _dbEventLoggerMock;

    private Mock<ISystemEventLogger> _eventLoggerMock;

    private Mock<IXmlExporterService> _xmlExporterMock;

    // component under tests
    private CancelMeasurementCommandHandler _cancelMeasurementCommandHandler;

    [TestInitialize]
    public void Initialize()
    {
      var effortConnection = Effort.DbConnectionFactory.CreateTransient();
      _dbContext = new EffortDbContext(effortConnection);

      _dbEventLoggerMock = new Mock<IDbUpdateEventLogger>();
      _eventLoggerMock = new Mock<ISystemEventLogger>();
      _xmlExporterMock = new Mock<IXmlExporterService>();

      var logger = new Mock<ILogger>();

      _cancelMeasurementCommandHandler = new CancelMeasurementCommandHandler(_dbEventLoggerMock.Object, _eventLoggerMock.Object, logger.Object, _xmlExporterMock.Object);
    }

    [TestCleanup]
    public void CleanUp()
    {
      _dbContext.Dispose();
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void Execute_ShouldThrowArgumentException_WhenMeasurementWithGivenIdDoesNotExist()
    {
      // Arrange
      var notExistingMeasurementId = 99;
      var cancelMeasurementCommand = new CancelMeasurementCommand
      {
        MeasurementId = notExistingMeasurementId
      };

      // Act & Assert
      _cancelMeasurementCommandHandler.Execute(_dbContext, cancelMeasurementCommand, _partnerId);
    }

    [TestMethod]
    public void Execute_ShouldCancelMeasurement()
    {
      // Setup
      var currentMeasurement = _dbContext.Measurements.Add(
        new Measurement
        {
          IsCancelled = false
        });
      _dbContext.SaveChanges();

      // Arrange
      var cancelMeasurementCommand = new CancelMeasurementCommand
      {
        MeasurementId = currentMeasurement.Id
      };

      Assert.IsFalse(currentMeasurement.IsCancelled);
      // Act
      _cancelMeasurementCommandHandler.Execute(_dbContext, cancelMeasurementCommand, _partnerId);

      // Assert
      var newMeasurement = _dbContext.Measurements.Single();
      Assert.IsTrue(newMeasurement.IsCancelled);
    }

    [TestMethod]
    public void Execute_ShouldCancelMeasurement_WhenItIsAlreadyCancelled()
    {
      // Setup
      var currentMeasurement = _dbContext.Measurements.Add(
        new Measurement
        {
          IsCancelled = true
        });
      _dbContext.SaveChanges();

      // Arrange
      var cancelMeasurementCommand = new CancelMeasurementCommand
      {
        MeasurementId = currentMeasurement.Id
      };

      Assert.IsTrue(currentMeasurement.IsCancelled);
      // Act
      _cancelMeasurementCommandHandler.Execute(_dbContext, cancelMeasurementCommand, _partnerId);

      // Assert
      var newMeasurement = _dbContext.Measurements.Single();
      Assert.IsTrue(newMeasurement.IsCancelled);
    }

    [TestMethod]
    public void Execute_ShouldCreateDbUpdateEventLog()
    {
      // Setup
      _dbEventLoggerMock.Setup(c => c.LogEvent(_partnerId, ctx => ctx.Measurements,
          It.IsAny<long>(), It.IsAny<Measurement>()))
          .Verifiable();

      var currentMeasurement = _dbContext.Measurements.Add(
        new Measurement
        {
          IsCancelled = true
        });
      _dbContext.SaveChanges();

      // Arrange
      var cancelMeasurementCommand = new CancelMeasurementCommand
      {
        MeasurementId = currentMeasurement.Id
      };


      // Act
      _cancelMeasurementCommandHandler.Execute(_dbContext, cancelMeasurementCommand, _partnerId);

      // Assert
      _dbEventLoggerMock.Verify(c => c.LogEvent(_partnerId, ctx => ctx.Measurements,
        It.IsAny<long>(), It.IsAny<Measurement>(), It.IsAny<Measurement>()), Times.Once);
    }
  }
}
