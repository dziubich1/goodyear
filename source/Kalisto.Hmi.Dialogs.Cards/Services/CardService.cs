﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.Operations.Cards.Commands;
using Kalisto.Backend.Operations.Cards.Queries;
using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Hmi.Core.Communication;
using Kalisto.Hmi.Core.Services;
using Kalisto.Hmi.Dialogs.Cards.Models;
using Kalisto.Localization;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Kalisto.Frontend.Notifications;
using Kalisto.Hmi.Dialogs.Common.Models;
using System;

namespace Kalisto.Hmi.Dialogs.Cards.Services
{
  public interface ICardService : IBackendOperationService
  {
    Task<RequestResult<List<CardModel>>> GetAllCardsAsync();

    Task<RequestResult<long>> AddCardAsync(string code);

    Task<RequestResult<long>> DeleteCardAsync(long id);

    Task<RequestResult<long>> UpdateCardAsync(long id, long? cargoId, long? contractorId, long? driverId, long? vehicleId,
      int number, string code, bool isCardActive, long? lifetimeId, long? measurementClassId);

    Task<RequestResult<int?>> GetMaxCardNumberAsync();

    Task<RequestResult<bool>> SetCardReaderProgrammingModeAsync(bool enabled);

    event Action<CardReaderProgrammingModeStateChangedNotification> ProgrammingModeStateChanged;

    event Action<CardScannedNotification> CardScanned;
  }

  public class CardService : BackendOperationService, ICardService
  {
    public event Action<CardReaderProgrammingModeStateChangedNotification> ProgrammingModeStateChanged;

    public event Action<CardScannedNotification> CardScanned;

    public CardService(ClientInfo clientInfo, IMessageBuilder messageBuilder, IFrontendMessageBridge messageBridge, IDialogService dialogService)
      : base(clientInfo, messageBuilder, messageBridge, dialogService)
    {
      RegisterNotificationHandler<CardReaderProgrammingModeStateChangedNotification>(OnCardReaderProgrammingModeStateChanged);
      RegisterNotificationHandler<CardScannedNotification>(OnCardScanned);
    }

    public async Task<RequestResult<List<CardModel>>> GetAllCardsAsync()
    {
      var message = MessageBuilder.CreateNew<GetAllCardsQuery>()
        .BasedOn(ClientInfo)
        .Build();

      var result = await MessageBridge.SendMessageWithResultAsync<List<Card>>(message);

      return new RequestResult<List<CardModel>>
      {
        Succeeded = result?.Succeeded ?? false,
        Response = result?.Response?.Select(card => new CardModel
        {
          Id = card.Id,
          Cargo = card.Cargo != null ? new ReferenceModel
          {
            Id = card.Cargo.Id,
            DisplayValue = card.Cargo?.Name
          } : null,
          Code = card.Code,
          Contractor = card.Contractor != null ? new ReferenceModel
          {
            Id = card.Contractor.Id,
            DisplayValue = card.Contractor.Name
          } : null,
          Driver = card.Driver != null ? new ReferenceModel
          {
            Id = card.Driver.Id,
            DisplayValue = card.Driver?.Identifier
          } : null,
          IsActive = card.IsActive,
          Lifetime = new LocalizedEnum
          {
            Enum = card.Lifetime,
          },
          MeasurementClass = new LocalizedEnum
          {
            Enum = card.MeasurementClass,
          },
          Number = card.Number,
          Vehicle = card.Vehicle != null ? new ReferenceModel
          {
            Id = card.Vehicle.Id,
            DisplayValue = card.Vehicle?.PlateNumber
          } : null
        }).ToList() ?? new List<CardModel>()
      };
    }

    public async Task<RequestResult<int?>> GetMaxCardNumberAsync()
    {
      var message = MessageBuilder.CreateNew<GetMaxCardNumberQuery>()
        .BasedOn(ClientInfo)
        .Build();

      var result = await MessageBridge.SendMessageWithResultAsync<long>(message);

      return new RequestResult<int?>
      {
        Succeeded = result?.Succeeded ?? false,
        Response = (int?)result?.Response
      };
    }

    public async Task<RequestResult<long>> AddCardAsync(string code)
    {
      var message = MessageBuilder.CreateNew<CreateCardCommand>()
        .BasedOn(ClientInfo)
        .WithProperty(c => c.Code, code)
        .Build();

      var result = await MessageBridge.SendMessageWithResultAsync<long>(message);
      return result ?? RequestResult<long>.Default;
    }

    public async Task<RequestResult<long>> UpdateCardAsync(long id, long? cargoId, long? contractorId, long? driverId, long? vehicleId,
      int number, string code, bool isCardActive, long? lifetimeId, long? measurementClassId)
    {
      var message = MessageBuilder.CreateNew<UpdateCardCommand>()
        .BasedOn(ClientInfo)
        .WithProperty(c => c.CardId, id)
        .WithProperty(c => c.Code, code)
        .WithProperty(c => c.DriverId, driverId)
        .WithProperty(c => c.VehicleId, vehicleId)
        .WithProperty(c => c.CargoId, cargoId)
        .WithProperty(c => c.ContractorId, contractorId)
        .WithProperty(c => c.IsActive, isCardActive)
        .WithProperty(c => c.LifetimeId, lifetimeId)
        .WithProperty(c => c.MeasurmentClassId, measurementClassId)
        .Build();

      var result = await MessageBridge.SendMessageWithResultAsync<long>(message);
      return result ?? RequestResult<long>.Default;
    }

    public async Task<RequestResult<long>> DeleteCardAsync(long id)
    {
      var message = MessageBuilder.CreateNew<DeleteCardCommand>()
        .BasedOn(ClientInfo)
        .WithProperty(c => c.CardId, id)
        .Build();

      var result = await MessageBridge.SendMessageWithResultAsync<long>(message);
      return result ?? RequestResult<long>.Default;
    }

    public async Task<RequestResult<bool>> SetCardReaderProgrammingModeAsync(bool enabled)
    {
      var message = MessageBuilder.CreateNew<EnableDisableCardReaderProgrammingModeCommand>()
        .BasedOn(ClientInfo)
        .WithProperty(c => c.ProgrammingModeEnabled, enabled)
        .Build();

      var result = await MessageBridge.SendMessageWithResultAsync<bool>(message);
      return result ?? RequestResult<bool>.Default;
    }

    private void OnCardScanned(object obj)
    {
      var notification = obj as CardScannedNotification;
      if (notification == null)
        return;

      CardScanned?.Invoke(notification);
    }

    private void OnCardReaderProgrammingModeStateChanged(object obj)
    {
      var notification = obj as CardReaderProgrammingModeStateChangedNotification;
      if (notification == null)
        return;

      ProgrammingModeStateChanged?.Invoke(notification);
    }
  }
}
