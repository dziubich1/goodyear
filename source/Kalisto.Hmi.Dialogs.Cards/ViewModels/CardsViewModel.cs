﻿using Kalisto.Backend.Operations.Cards.Commands.Interfaces;
using Kalisto.Configuration.Permissions;
using Kalisto.Hmi.Core;
using Kalisto.Hmi.Core.Commands;
using Kalisto.Hmi.Core.Services;
using Kalisto.Hmi.Dialogs.Cards.Dialogs;
using Kalisto.Hmi.Dialogs.Cards.Models;
using Kalisto.Hmi.Dialogs.Cards.Services;
using MahApps.Metro.Controls.Dialogs;
using PropertyChanged;
using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Kalisto.Localization;
using Kalisto.Localization.Messages;

namespace Kalisto.Hmi.Dialogs.Cards.ViewModels
{
  [AddINotifyPropertyChangedInterface]
  public class CardsViewModel : DataGridViewModel<CardModel>
  {
    private readonly ILocalizer<Messages> _localizer;

    private readonly ICardService _cardService;

    public override string DialogTitleKey => "Cards";

    public ICommand AddItemCommand { get; set; }

    public ICommand EditItemCommand { get; set; }

    public ICommand DeleteItemCommand { get; set; }

    public ICommand SyncItemsCommand { get; set; }

    public ObservableCollectionEx<CardModel> Cards { get; set; }

    public CardModel SelectedCard { get; set; }

    protected override Type[] AssociatedCommandTypes => new[] { typeof(ICardCommand) };

    public CardsViewModel(IDialogService dialogService, IAuthorizationService authorizationService, ICardService cardService)
      : base(dialogService, authorizationService)
    {
      _localizer = new LocalizationManager<Messages>();
      _cardService = cardService;

      AddItemCommand = new DelegateCommand(AddCard);
      EditItemCommand = new DelegateCommand(EditItem);
      DeleteItemCommand = new DelegateCommand(DeleteCard);
      SyncItemsCommand = new DelegateCommand(SyncCards);

      Cards = new ObservableCollectionEx<CardModel>();
    }

    protected override ObservableCollectionEx<CardModel> GetItemsSource()
    {
      return Cards;
    }

    protected override string GetEditRuleName()
    {
      return PermissionRules.DictionaryDataManagement;
    }

    public override async Task RefreshDataAsync()
    {
      await base.RefreshDataAsync();

      await LoadCards();
    }

    private async Task LoadCards()
    {
      var result = await _cardService.GetAllCardsAsync();
      if (!result.Succeeded)
      {
        return;
      }

      Cards.Clear();
      Cards.AddRange(result.Response);
    }

    private async Task<int?> GetMaxCardNumber()
    {
      var result = await _cardService.GetMaxCardNumberAsync();
      if (!result.Succeeded)
      {
        return null;
      }

      return result.Response;
    }

    private async void AddCard(object parameter)
    {
      var maxCardNumber = await GetMaxCardNumber();
      if (!maxCardNumber.HasValue)
      {
        return;
      }

      await DialogService.ShowModalDialogAsync<AddCardModalDialogViewModel>(
        vm =>
        {
          vm.DialogNameKey = "AddCard";
          vm.Number = maxCardNumber.Value + 1;
        },
        async viewModel =>
        {
          var result = await _cardService.AddCardAsync(viewModel.Code);
          return result.Succeeded;
        });
    }

    protected override async void EditItem(object parameter)
    {
      var selectedCard = parameter as CardModel ?? SelectedCard;
      if (selectedCard == null)
      {
        await DialogService.ShowMessageAsync(_localizer.Localize(() => Messages.Error),
          _localizer.Localize(() => Messages.SelectCardToUpdate), MessageDialogType.Ok);

        return;
      }

      await DialogService.ShowModalDialogAsync<UpdateCardModalDialogViewModel>(vm =>
      {
        vm.DialogNameKey = "EditCard";
        vm.ValidateOnLoad = true;
        vm.Code = selectedCard.Code;
        vm.Number = selectedCard.Number;
        vm.IsCardActive = selectedCard.IsActive;
        vm.CardToBeUpdated = selectedCard;
      },
      async viewModel =>
      {
        long? vehicleId = null;
        if (viewModel.Vehicle != null && viewModel.Vehicle.Id != -1)
        {
          vehicleId = viewModel.Vehicle.Id;
        }

        long? cargoId = null;
        if (viewModel.Cargo != null && viewModel.Cargo.Id != -1)
        {
          cargoId = viewModel.Cargo.Id;
        }

        long? contractorId = null;
        if (viewModel.Contractor != null && viewModel.Contractor.Id != -1)
        {
          contractorId = viewModel.Contractor.Id;
        }

        long? driverId = null;
        if (viewModel.Driver != null && viewModel.Driver.Id != -1)
        {
          driverId = viewModel.Driver.Id;
        }

        long? lifetimeId = null;
        if (viewModel.Lifetime != null && viewModel.Lifetime.Id != -1)
        {
          lifetimeId = viewModel.Lifetime.Id;
        }

        long? measurementClassId = null;
        if (viewModel.MeasurementClass != null && viewModel.MeasurementClass.Id != -1)
        {
          measurementClassId = viewModel.MeasurementClass.Id;
        }

        var result = await _cardService.UpdateCardAsync(selectedCard.Id, cargoId, contractorId, driverId, vehicleId,
          viewModel.Number, viewModel.Code, viewModel.IsCardActive, lifetimeId, measurementClassId);
        return result.Succeeded;
      });
    }

    private async void DeleteCard(object parameter)
    {
      if (SelectedCard == null)
      {
        await DialogService.ShowMessageAsync(_localizer.Localize(() => Messages.Error),
          _localizer.Localize(() => Messages.SelectCardToDelete), MessageDialogType.Ok);
        return;
      }

      var dlgResult = await DialogService.ShowMessageAsync(_localizer.Localize(() => Messages.Attention),
        _localizer.Localize(() => Messages.DeleteCardQuestion), MessageDialogType.YesNo);

      if (dlgResult != MessageDialogResult.Affirmative)
      {
        return;
      }

      await _cardService.DeleteCardAsync(SelectedCard.Id);
    }

    private async void SyncCards(object obj)
    {
      await LoadCards();
    }
  }
}
