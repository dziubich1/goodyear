﻿using MahApps.Metro.Controls;

namespace Kalisto.Hmi.Dialogs.Cards.Dialogs
{
  /// <summary>
  /// Interaction logic for UpateCardModalDialogView.xaml
  /// </summary>
  public partial class UpdateCardModalDialogView : MetroWindow
  {
    public UpdateCardModalDialogView()
    {
      InitializeComponent();
    }
  }
}
