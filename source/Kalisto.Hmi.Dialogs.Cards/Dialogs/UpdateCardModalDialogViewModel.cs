﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Hmi.Core;
using Kalisto.Hmi.Core.Communication;
using Kalisto.Hmi.Core.Services;
using Kalisto.Hmi.Dialogs.Cards.Models;
using Kalisto.Hmi.Dialogs.Cargoes.Services;
using Kalisto.Hmi.Dialogs.Contractors.Services;
using Kalisto.Hmi.Dialogs.Drivers.Services;
using Kalisto.Hmi.Dialogs.Vehicles.Services;
using Kalisto.Localization;
using PropertyChanged;
using System;
using System.Linq;
using System.Threading.Tasks;
using Kalisto.Hmi.Dialogs.Vehicles.Models;
using Kalisto.Hmi.Dialogs.Common.Models;

namespace Kalisto.Hmi.Dialogs.Cards.Dialogs
{
  [AddINotifyPropertyChangedInterface]
  public class UpdateCardModalDialogViewModel : ModalViewModelBase<UpdateCardModalDialogViewModel>
  {
    private readonly ICargoService _cargoService;
    private readonly IContractorService _contractorService;
    private readonly IDriverService _driverService;
    private readonly IVehicleService _vehicleService;

    public override string DialogNameKey { get; set; }

    public int Number { get; set; }

    public string Code { get; set; }

    public bool IsCardActive { get; set; }

    public ReferenceModel Lifetime { get; set; }

    public ReferenceModel MeasurementClass { get; set; }

    public ObservableCollectionEx<ReferenceModel> LifetimeSource { get; set; }

    public ObservableCollectionEx<ReferenceModel> MeasurementClassSource { get; set; }

    public ObservableCollectionEx<ReferenceModel> Drivers { get; set; }

    public ReferenceModel Driver { get; set; }

    public ObservableCollectionEx<ReferenceModel> Vehicles { get; set; }

    public ReferenceModel Vehicle { get; set; }

    public ObservableCollectionEx<ReferenceModel> Cargoes { get; set; }

    public ReferenceModel Cargo { get; set; }

    public ObservableCollectionEx<ReferenceModel> Contractors { get; set; }

    public ReferenceModel Contractor { get; set; }

    public CardModel CardToBeUpdated { get; set; }

    public UpdateCardModalDialogViewModel(IDialogService dialogService, IDriverService driverService,
      ICargoService cargoService, IContractorService contractorService, IVehicleService vehicleService,
      IAsyncProgressService asyncProgressService, IMessageBuilder messageBuilder,
      IFrontendMessageBridge messageBridge, ClientInfo clientInfo)
      : base(dialogService, asyncProgressService, messageBuilder, messageBridge, clientInfo)
    {
      _cargoService = cargoService;
      _contractorService = contractorService;
      _driverService = driverService;
      _vehicleService = vehicleService;

      LifetimeSource = new ObservableCollectionEx<ReferenceModel>();
      MeasurementClassSource = new ObservableCollectionEx<ReferenceModel>();
      Drivers = new ObservableCollectionEx<ReferenceModel>();
      Cargoes = new ObservableCollectionEx<ReferenceModel>();
      Contractors = new ObservableCollectionEx<ReferenceModel>();
      Vehicles = new ObservableCollectionEx<ReferenceModel>();
    }

    protected override void DefineValidationRules()
    {
    }

    protected override async void OnViewLoaded(object view)
    {
      base.OnViewLoaded(view);

      LoadCardLifetimes();
      LoadMeasurementClasses();
      await LoadDriversAsync();
      await LoadCargoesAsync();
      await LoadContractorsAsync();
      await LoadVehiclesAsync();

      SetupSelectedValues();
    }

    private async Task LoadCargoesAsync()
    {
      var result = await _cargoService.GetAllCargoesAsync();
      if (!result.Succeeded)
        return;

      var mappedCargoes = result.Response.Select(d => new ReferenceModel()
      {
        Id = d.Id,
        DisplayValue = d.Name
      });

      ClearCargoes();
      Cargoes.AddRange(mappedCargoes);
    }

    private void ClearCargoes()
    {
      Cargoes.Clear();
      Cargoes.Add(
        new ReferenceModel
        {
          Id = -1,
          DisplayValue = string.Empty
        });
    }

    private async Task LoadContractorsAsync()
    {
      var result = await _contractorService.GetAllContractorsAsync();
      if (!result.Succeeded)
        return;

      var mappedContractors = result.Response.Select(d => new ReferenceModel()
      {
        Id = d.Id,
        DisplayValue = d.Name
      });

      ClearContractors();
      Contractors.AddRange(mappedContractors);
    }

    private void ClearContractors()
    {
      Contractors.Clear();
      Contractors.Add(
        new ReferenceModel
        {
          Id = -1,
          DisplayValue = string.Empty
        });
    }

    private async Task LoadDriversAsync()
    {
      var result = await _driverService.GetAllDriversAsync();
      if (!result.Succeeded)
        return;

      var mappedDrivers = result.Response.Select(d => new ReferenceModel()
      {
        Id = d.Id,
        DisplayValue = d.Identifier
      });

      ClearDrivers();
      Drivers.AddRange(mappedDrivers);
    }

    private void ClearDrivers()
    {
      Drivers.Clear();
      Drivers.Add(
        new ReferenceModel
        {
          Id = -1,
          DisplayValue = string.Empty
        });
    }

    private async Task LoadVehiclesAsync()
    {
      var result = await _vehicleService.GetAllVehiclesAsync();
      if (!result.Succeeded)
      {
        ClearVehicles();
        return;
      }

      var mappedVehicles = result.Response.Select(d => new ReferenceModel
      {
        Id = d.Id,
        DisplayValue = d.PlateNumber
      });

      ClearVehicles();
      Vehicles.AddRange(mappedVehicles);
    }

    private void ClearVehicles()
    {
      Vehicles.Clear();
      Vehicles.Add(
        new ReferenceModel
        {
          Id = -1,
          DisplayValue = string.Empty
        });
    }

    private void LoadMeasurementClasses()
    {
      var result = Enum.GetValues(typeof(MeasurementClass)).Cast<MeasurementClass>().Select(c => new LocalizedEnum
      {
        Enum = c
      }).Select(l => new ReferenceModel()
      {
        Id = Convert.ToInt32(l.Enum),
        DisplayValue = l.LocalizedEnumValue
      });

      ClearMeasurementClasses();
      MeasurementClassSource.AddRange(result);
    }

    private void ClearMeasurementClasses()
    {
      MeasurementClassSource.Clear();
      MeasurementClassSource.Add(
        new ReferenceModel
        {
          Id = -1,
          DisplayValue = string.Empty
        });
    }

    private void LoadCardLifetimes()
    {
      var result = Enum.GetValues(typeof(CardLifetime)).Cast<CardLifetime>().Select(c => new LocalizedEnum
      {
        Enum = c
      }).Select(l => new ReferenceModel()
      {
        Id = Convert.ToInt32(l.Enum),
        DisplayValue = l.LocalizedEnumValue
      });


      ClearCardLifetimeValues();
      LifetimeSource.AddRange(result);
    }

    private void ClearCardLifetimeValues()
    {
      LifetimeSource.Clear();
      LifetimeSource.Add(
        new ReferenceModel
        {
          Id = -1,
          DisplayValue = string.Empty
        });
    }

    private void SetupSelectedValues()
    {
      int lifetimeId = -1;
      if (CardToBeUpdated.Lifetime.Enum != null)
        lifetimeId = Convert.ToInt32(CardToBeUpdated.Lifetime.Enum);

      int measurementClassId = -1;
      if (CardToBeUpdated.MeasurementClass.Enum != null)
        measurementClassId = Convert.ToInt32(CardToBeUpdated.MeasurementClass.Enum);

      long cargoId = -1;
      if (CardToBeUpdated.Cargo != null)
        cargoId = CardToBeUpdated.Cargo.Id;

      long contractorId = -1;
      if (CardToBeUpdated.Contractor != null)
        contractorId = CardToBeUpdated.Contractor.Id;

      long driverId = -1;
      if (CardToBeUpdated.Driver != null)
        driverId = CardToBeUpdated.Driver.Id;

      long vehicleId = -1;
      if (CardToBeUpdated.Vehicle != null)
        vehicleId = CardToBeUpdated.Vehicle.Id;

      Lifetime = LifetimeSource.FirstOrDefault(l => l.Id == lifetimeId);
      MeasurementClass = MeasurementClassSource.FirstOrDefault(m => m.Id == measurementClassId);
      Cargo = Cargoes.FirstOrDefault(d => d.Id == cargoId);
      Contractor = Contractors.FirstOrDefault(d => d.Id == contractorId);
      Driver = Drivers.FirstOrDefault(d => d.Id == driverId);
      Vehicle = Vehicles.FirstOrDefault(d => d.Id == vehicleId);
    }
  }
}
