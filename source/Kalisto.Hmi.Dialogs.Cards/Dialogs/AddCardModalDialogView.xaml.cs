﻿using MahApps.Metro.Controls;

namespace Kalisto.Hmi.Dialogs.Cards.Dialogs
{
  /// <summary>
  /// Interaction logic for AddCardModalDialogView.xaml
  /// </summary>
  public partial class AddCardModalDialogView : MetroWindow
  {
    public AddCardModalDialogView()
    {
      InitializeComponent();
    }
  }
}
