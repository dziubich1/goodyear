﻿using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Hmi.Core;
using Kalisto.Hmi.Core.Communication;
using Kalisto.Hmi.Core.Services;
using Kalisto.Hmi.Dialogs.Cards.Services;
using PropertyChanged;

namespace Kalisto.Hmi.Dialogs.Cards.Dialogs
{
  [AddINotifyPropertyChangedInterface]
  public class AddCardModalDialogViewModel : ModalViewModelBase<AddCardModalDialogViewModel>
  {
    private readonly ICardService _cardService;

    public override string DialogNameKey { get; set; }

    public int Number { get; set; }

    public string Code { get; set; }

    public AddCardModalDialogViewModel(IDialogService dialogService, IAsyncProgressService asyncProgressService,
    IMessageBuilder messageBuilder, IFrontendMessageBridge messageBridge, ClientInfo clientInfo, ICardService cardService)
      : base(dialogService, asyncProgressService, messageBuilder, messageBridge, clientInfo)
    {
      _cardService = cardService;
      _cardService.ProgrammingModeStateChanged += OnCardReaderProgrammingModeStateChanged;
      _cardService.CardScanned += OnCardScanned;
    }

    protected override async void OnViewReady(object view)
    {
      base.OnViewReady(view);

      await _cardService.SetCardReaderProgrammingModeAsync(true);
    }

    protected override async void OnDeactivate(bool close)
    {
      base.OnDeactivate(close);

      await _cardService.SetCardReaderProgrammingModeAsync(false);
      _cardService.ProgrammingModeStateChanged -= OnCardReaderProgrammingModeStateChanged;
      _cardService.CardScanned -= OnCardScanned;
    }

    protected override void DefineValidationRules()
    {
      InvalidateProperty(() => Code).When(() => Code.Length < 3)
        .WithMessageKey("TooShort").ButIgnoreOnLoad();
    }

    private void OnCardScanned(Frontend.Notifications.CardScannedNotification notification)
    {
      Code = notification.CardCode;
    }

    private void OnCardReaderProgrammingModeStateChanged(Frontend.Notifications.CardReaderProgrammingModeStateChangedNotification notification)
    {
      if (!notification.ProgrammingModeEnabled)
        TryClose();
    }
  }
}
