﻿using Kalisto.Hmi.Dialogs.Common.Models;
using Kalisto.Hmi.Dialogs.Vehicles.Models;
using Kalisto.Localization;

namespace Kalisto.Hmi.Dialogs.Cards.Models
{
  public class CardModel
  {
    public long Id { get; set; }

    public string Code { get; set; }

    public LocalizedEnum Lifetime { get; set; }

    public LocalizedEnum MeasurementClass { get; set; }

    public ReferenceModel Cargo { get; set; }

    public ReferenceModel Contractor { get; set; }

    public ReferenceModel Driver { get; set; }

    public ReferenceModel Vehicle { get; set; }

    public bool IsActive { get; set; }

    public int Number { get; set; }
  }
}
