﻿using Kalisto.Backend.DataAccess.Effort;
using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.DataAccess.Model.Logging;
using Kalisto.Backend.Operations.Contractors.Commands;
using Kalisto.Backend.Operations.Contractors.Exceptions;
using Kalisto.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;

namespace Kalisto.Backend.Operations.Contractors.UnitTests
{
  [TestClass]
  public class CreateContractorCommandHandlerTests
  {
    private IDbContext _dbContext;

    private readonly string _partnerId = "unknown";

    private Mock<IDbInsertEventLogger> _dbEventLoggerMock;

    // component under tests
    private CreateContractorCommandHandler _createContractorCommandHandler;

    [TestInitialize]
    public void Initialize()
    {
      var effortConnection = Effort.DbConnectionFactory.CreateTransient();
      _dbContext = new EffortDbContext(effortConnection);

      _dbEventLoggerMock = new Mock<IDbInsertEventLogger>();

      var logger = new Mock<ILogger>();

      _createContractorCommandHandler = new CreateContractorCommandHandler(_dbEventLoggerMock.Object, logger.Object);
    }

    [TestCleanup]
    public void CleanUp()
    {
      _dbContext.Dispose();
    }

    [TestMethod]
    [ExpectedException(typeof(ContractorWithGivenTaxIdExistsException))]
    public void Execute_ShouldThrowContractorWithGivenTaxIdExistsException_WhenContractorWithGivenTaxIdAlreadyExists()
    {
      // Setup
      var testTaxId = "test";
      _dbContext.Contractors.Add(new Contractor { TaxId = testTaxId });
      _dbContext.SaveChanges();

      // Arrange
      var createContractorCommand = new CreateContractorCommand
      {
        TaxId = testTaxId
      };

      // Act & Assert
      _createContractorCommandHandler.Execute(_dbContext, 
        createContractorCommand, _partnerId);
    }

    [TestMethod]
    public void Execute_ShouldNotCreateContractor_WhenContractorWithGivenTaxIdAlreadyExists()
    {
      // Setup
      var testTaxId = "test";
      _dbContext.Contractors.Add(new Contractor { TaxId = testTaxId });
      _dbContext.SaveChanges();
      var expectedContractorsCount = _dbContext.Contractors.Count();

      // Arrange
      var createContractorCommand = new CreateContractorCommand
      {
        TaxId = testTaxId
      };

      try
      {
        // Act
        _createContractorCommandHandler.Execute(_dbContext,
          createContractorCommand, _partnerId);
      }
      catch (ContractorWithGivenTaxIdExistsException)
      {
        // Assert
        Assert.AreEqual(expectedContractorsCount,
          _dbContext.Contractors.Count());
      }
    }

    [TestMethod]
    [ExpectedException(typeof(ContractorWithGivenNameExistsException))]
    public void Execute_ShouldThrowContractorWithGivenNameExistsException_WhenTaxIdIsNotSpecifiedAndContractorWithGivenNameAlreadyExists()
    {
      // Setup
      var testName = "test";
      _dbContext.Contractors.Add(new Contractor { Name = testName });
      _dbContext.SaveChanges();

      // Arrange
      var createContractorCommand = new CreateContractorCommand
      {
        Name = testName
      };

      // Act & Assert
      _createContractorCommandHandler.Execute(_dbContext,
        createContractorCommand, _partnerId);
    }

    [TestMethod]
    public void Execute_ShouldNotCreateContractor_WhenTaxIdIsNotSpecifiedAndContractorWithGivenNameAlreadyExists()
    {
      // Setup
      var testName = "test";
      _dbContext.Contractors.Add(new Contractor { Name = testName });
      _dbContext.SaveChanges();
      var expectedContractorsCount = _dbContext.Contractors.Count();

      // Arrange
      var createContractorCommand = new CreateContractorCommand
      {
        Name = testName
      };

      try
      {
        // Act
        _createContractorCommandHandler.Execute(_dbContext,
          createContractorCommand, _partnerId);
      }
      catch (ContractorWithGivenNameExistsException)
      {
        // Assert
        Assert.AreEqual(expectedContractorsCount,
          _dbContext.Contractors.Count());
      }
    }

    [TestMethod]
    public void Execute_ShouldCreateContractor_WhenTaxIdIsNotSpecified()
    {
      // Arrange
      var createContractorCommand = new CreateContractorCommand
      {
        Name = "TestName",
        Address = "TestAddress",
        City = "TestCity",
        PostalCode = "TestPostalCode"
      };

      // Act
      _createContractorCommandHandler.Execute(_dbContext,
        createContractorCommand, _partnerId);

      // Assert
      Assert.AreEqual(1, _dbContext.Contractors.Count(),
        "Contractors table should contain one contractor");

      var createdContractor = _dbContext.Contractors.Single();
      Assert.AreEqual(1, createdContractor.Id,
        "Id of created contractor should be auto generated");
      Assert.AreEqual(createContractorCommand.Name, createdContractor.Name,
        "Name of created contractor should be the same like in command");
      Assert.AreEqual(true, string.IsNullOrEmpty(createdContractor.TaxId),
        "TaxId of created contractor should be null or empty string");
      Assert.AreEqual(createContractorCommand.Address, createdContractor.Address,
        "Address of created contractor should be the same like in command");
      Assert.AreEqual(createContractorCommand.City, createdContractor.City,
        "City of created contractor should be the same like in command");
      Assert.AreEqual(createContractorCommand.PostalCode, createdContractor.PostalCode,
        "PostalCode of created contractor should be the same like in command");
    }

    [TestMethod]
    public void Execute_ShouldCreateContractor()
    {
      // Arrange
      var createContractorCommand = new CreateContractorCommand
      {
        Name = "TestName",
        TaxId = "TestTaxId",
        Address = "TestAddress",
        City = "TestCity",
        PostalCode = "TestPostalCode"
      };

      // Act
      _createContractorCommandHandler.Execute(_dbContext, 
        createContractorCommand, _partnerId);

      // Assert
      Assert.AreEqual(1, _dbContext.Contractors.Count(), 
        "Contractors table should contain one contractor");

      var createdContractor = _dbContext.Contractors.Single();
      Assert.AreEqual(1, createdContractor.Id, 
        "Id of created contractor should be auto generated");
      Assert.AreEqual(createContractorCommand.Name, createdContractor.Name, 
        "Name of created contractor should be the same like in command");
      Assert.AreEqual(createContractorCommand.TaxId, createdContractor.TaxId,
        "TaxId of created contractor should be the same like in command");
      Assert.AreEqual(createContractorCommand.Address, createdContractor.Address,
        "Address of created contractor should be the same like in command");
      Assert.AreEqual(createContractorCommand.City, createdContractor.City,
        "City of created contractor should be the same like in command");
      Assert.AreEqual(createContractorCommand.PostalCode, createdContractor.PostalCode,
        "PostalCode of created contractor should be the same like in command");
    }

    [TestMethod]
    public void Execute_ShouldReturnIdentifierOfNewlyCreatedContractor()
    {
      // Arrange
      var createContractorCommand = new CreateContractorCommand
      {
        Name = "TestName",
        TaxId = "TestTaxId",
        Address = "TestAddress",
        City = "TestCity",
        PostalCode = "TestPostalCode"
      };

      // Act
      var id = _createContractorCommandHandler.Execute(_dbContext,
        createContractorCommand, _partnerId);

      // Assert
      var createdContractor = _dbContext.Contractors.Single();
      Assert.AreEqual(1L, id, "Id of created contractor should be auto generated and returned properly by Execute(...) method");
      Assert.AreEqual(1, createdContractor.Id, "Id of created contractor entity should be auto generated and returned properly by Execute(...) method");
    }

    [TestMethod]
    public void Execute_ShouldCreateDbInsertEventLog()
    {
      // Setup
      _dbEventLoggerMock.Setup(c => c.LogEvent(_partnerId, ctx => ctx.Contractors,
          It.IsAny<int>(), It.IsAny<Contractor>()))
        .Verifiable();

      // Arrange
      var createContractorCommand = new CreateContractorCommand
      {
        Name = "TestName",
        TaxId = "TestTaxId",
        Address = "TestAddress",
        City = "TestCity",
        PostalCode = "TestPostalCode"
      };

      // Act
      _createContractorCommandHandler.Execute(_dbContext,
        createContractorCommand, _partnerId);

      // Assert
      _dbEventLoggerMock.Verify(c => c.LogEvent(_partnerId, ctx => ctx.Contractors,
        1, It.IsAny<Contractor>()), Times.Once);
    }
  }
}
