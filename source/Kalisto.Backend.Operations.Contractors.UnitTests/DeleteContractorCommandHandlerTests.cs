﻿using Kalisto.Backend.DataAccess.Effort;
using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.DataAccess.Model.Logging;
using Kalisto.Backend.Operations.Contractors.Commands;
using Kalisto.Backend.Operations.Contractors.Exceptions;
using Kalisto.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Linq;

namespace Kalisto.Backend.Operations.Contractors.UnitTests
{
  [TestClass]
  public class DeleteContractorCommandHandlerTests
  {
    private IDbContext _dbContext;

    private readonly string _partnerId = "unknown";

    private Mock<IDbDeleteEventLogger> _dbEventLoggerMock;

    // component under tests
    private DeleteContractorCommandHandler _deleteContractorCommandHandler;

    [TestInitialize]
    public void Initialize()
    {
      var effortConnection = Effort.DbConnectionFactory.CreateTransient();
      _dbContext = new EffortDbContext(effortConnection);

      _dbEventLoggerMock = new Mock<IDbDeleteEventLogger>();

      var logger = new Mock<ILogger>();

      _deleteContractorCommandHandler = new DeleteContractorCommandHandler(_dbEventLoggerMock.Object, logger.Object);
    }

    [TestCleanup]
    public void CleanUp()
    {
      _dbContext.Dispose();
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void Execute_ShouldThrowArgumentException_WhenContractorWithGivenIdDoesNotExist()
    {
      // Setup
      var testTaxId = "test";
      _dbContext.Contractors.Add(new Contractor { TaxId = testTaxId }); // generated id: 1
      _dbContext.SaveChanges();

      // Arrange
      var command = new DeleteContractorCommand
      {
        ContractorId = 2 // not existing contractor id
      };

      // Act & Assert
      _deleteContractorCommandHandler.Execute(_dbContext, command, _partnerId);
    }

    [TestMethod]
    public void Execute_ShouldRemoveContractor()
    {
      // Setup
      _dbContext.Contractors.Add(new Contractor
      {
        Name = "TestName",
        TaxId = "TestTaxId",
        Address = "TestAddress",
        City = "TestCity",
        PostalCode = "TestPostalCode"
      }); // generated id: 1
      _dbContext.SaveChanges();

      // Arrange
      var command = new DeleteContractorCommand
      {
        ContractorId = 1
      };

      // Act
      _deleteContractorCommandHandler.Execute(_dbContext, command, _partnerId);

      // Assert
      Assert.AreEqual(0, _dbContext.Contractors.Count());
    }

    [TestMethod]
    public void Execute_ShouldCreateDbDeleteEventLog()
    {
      // Setup
      _dbEventLoggerMock.Setup(c => c.LogEvent(_partnerId, ctx => ctx.Contractors,
          It.IsAny<int>(), null, It.IsAny<Contractor>()))
        .Verifiable();

      _dbContext.Contractors.Add(new Contractor
      {
        Name = "TestName",
        TaxId = "TestTaxId",
        Address = "TestAddress",
        City = "TestCity",
        PostalCode = "TestPostalCode"
      }); // generated id: 1
      _dbContext.SaveChanges();

      // Arrange
      var command = new DeleteContractorCommand
      {
        ContractorId = 1
      };

      // Act
      _deleteContractorCommandHandler.Execute(_dbContext, command, "unknown");

      // Assert
      _dbEventLoggerMock.Verify(c => c.LogEvent(_partnerId, ctx => ctx.Contractors,
        1, null, It.IsAny<Contractor>()), Times.Once);
    }
  }
}
