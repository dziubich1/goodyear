﻿using Kalisto.Backend.DataAccess.Effort;
using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.DataAccess.Model.Logging;
using Kalisto.Backend.Operations.Contractors.Commands;
using Kalisto.Backend.Operations.Contractors.Exceptions;
using Kalisto.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Linq;

namespace Kalisto.Backend.Operations.Contractors.UnitTests
{
  [TestClass]
  public class UpdateContractorCommandHandlerTests
  {
    private IDbContext _dbContext;

    private readonly string _partnerId = "unknown";

    private Mock<IDbUpdateEventLogger> _dbEventLoggerMock;

    // component under tests
    private UpdateContractorCommandHandler _updateContractorCommandHandler;

    [TestInitialize]
    public void Initialize()
    {
      var effortConnection = Effort.DbConnectionFactory.CreateTransient();
      _dbContext = new EffortDbContext(effortConnection);

      _dbEventLoggerMock = new Mock<IDbUpdateEventLogger>();

      var logger = new Mock<ILogger>();

      _updateContractorCommandHandler = new UpdateContractorCommandHandler(_dbEventLoggerMock.Object, logger.Object);
    }

    [TestCleanup]
    public void CleanUp()
    {
      _dbContext.Dispose();
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void Execute_ShouldThrowArgumentException_WhenContractorWithGivenIdDoesNotExist()
    {
      // Arrange
      var notExistingContractorId = 5;
      var command = new UpdateContractorCommand { ContractorId = notExistingContractorId };

      // Act & Assert
      _updateContractorCommandHandler.Execute(_dbContext, command, _partnerId);
    }

    [TestMethod]
    [ExpectedException(typeof(ContractorWithGivenTaxIdExistsException))]
    public void Execute_ShouldThrowContractorExistsException_WhenNameOfCurrentContractorHasChangedButContractorWithSameTaxIdAlreadyExists()
    {
      // Setup
      var testTaxId = "test";
      _dbContext.Contractors.Add(new Contractor { TaxId = testTaxId }); // generated id: 1
      var currentTaxId = "current";
      _dbContext.Contractors.Add(new Contractor { TaxId = currentTaxId }); // generated id: 2
      _dbContext.SaveChanges();

      // Arrange
      var command = new UpdateContractorCommand { ContractorId = 2, TaxId = testTaxId };

      // Act & Assert
      _updateContractorCommandHandler.Execute(_dbContext, command, _partnerId);
    }

    [TestMethod]
    public void Execute_ShouldUpdateContractor()
    {
      // Setup
      var currentName = "currentName";
      var currentTaxId = "currentTaxId";
      var currentAddress = "currentAddress";
      var currentCity = "currentCity";
      var currentPostalCode = "currentPostalCode";
      _dbContext.Contractors.Add(new Contractor
      {
        Name = currentName,
        TaxId = currentTaxId,
        Address = currentAddress,
        City = currentCity,
        PostalCode = currentPostalCode
      }); // generated id: 1
      _dbContext.SaveChanges();

      // Arrange
      var newName = "newName";
      var newTaxId = "newTaxId";
      var newAddress = "newAddress";
      var newCity = "newCity";
      var newPostalCode = "PostalCode";
      var command = new UpdateContractorCommand
      {
        ContractorId = 1,
        Name = newName,
        TaxId = newTaxId,
        Address = newAddress,
        City = newCity,
        PostalCode = newPostalCode
      };

      // Act
      _updateContractorCommandHandler.Execute(_dbContext, command, _partnerId);

      // Assert
      var contractor = _dbContext.Contractors.Single();
      Assert.AreEqual(newName, contractor.Name);
      Assert.AreEqual(newTaxId, contractor.TaxId);
      Assert.AreEqual(newAddress, contractor.Address);
      Assert.AreEqual(newCity, contractor.City);
      Assert.AreEqual(newPostalCode, contractor.PostalCode);
    }

    [TestMethod]
    public void Execute_ShouldReturnUpdatedContractorId()
    {
      // Setup
      var currentName = "currentName";
      var currentTaxId = "currentTaxId";
      var currentAddress = "currentAddress";
      var currentCity = "currentCity";
      var currentPostalCode = "currentPostalCode";
      _dbContext.Contractors.Add(new Contractor
      {
        Name = currentName,
        TaxId = currentTaxId,
        Address = currentAddress,
        City = currentCity,
        PostalCode = currentPostalCode
      }); // generated id: 1
      _dbContext.SaveChanges();

      // Arrange
      var newName = "newName";
      var newTaxId = "newTaxId";
      var newAddress = "newAddress";
      var newCity = "newCity";
      var newPostalCode = "PostalCode";
      var command = new UpdateContractorCommand
      {
        ContractorId = 1,
        Name = newName,
        TaxId = newTaxId,
        Address = newAddress,
        City = newCity,
        PostalCode = newPostalCode
      };

      // Act
      var resultId = _updateContractorCommandHandler.Execute(_dbContext, command, _partnerId);

      // Assert
      var contractor = _dbContext.Contractors.Single();
      Assert.AreEqual(1, contractor.Id);
      Assert.AreEqual(1L, resultId);
    }

    [TestMethod]
    public void Execute_ShouldCreateDbUpdateEventLog()
    {
      // Setup
      _dbEventLoggerMock.Setup(c => c.LogEvent(_partnerId, ctx => ctx.Contractors,
          It.IsAny<long>(), It.IsAny<Contractor>()))
        .Verifiable();

      _dbContext.Contractors.Add(new Contractor
      {
        Name = "currentName",
        TaxId = "currentTaxId",
        Address = "currentAddress",
        City = "currentCity",
        PostalCode = "currentPostalCode"
      }); // generated id: 1
      _dbContext.SaveChanges();

      // Arrange
      var command = new UpdateContractorCommand
      {
        ContractorId = 1,
        Name = "newName",
        TaxId = "newTaxId",
        Address = "newAddress",
        City = "newCity",
        PostalCode = "PostalCode",
      };

      // Act
      _updateContractorCommandHandler.Execute(_dbContext, command, _partnerId);

      // Assert
      _dbEventLoggerMock.Verify(c => c.LogEvent(_partnerId, ctx => ctx.Contractors,
        It.IsAny<long>(), It.IsAny<Contractor>(), It.IsAny<Contractor>()), Times.Once);
    }
  }
}
