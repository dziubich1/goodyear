﻿using System.Collections.Generic;
using Kalisto.Devices.IOController.Interfaces;
using Kalisto.Frontend.Notifications.Base;

namespace Kalisto.Frontend.Notifications
{
  public class DeviceStateNotification : NotificationBase
  {
    public override string NotificationId => nameof(DeviceStateNotification);

    public List<DeviceStatePayload> Payloads { get; set; }
  }

  public class DeviceStatePayload
  {
    public int DeviceId { get; set; }

    public TrafficState? TrafficState { get; set; }

    public BarrierState? BarrierState { get; set; }
  }
}
