﻿using System.Media;
using System.Threading.Tasks;
using PropertyChanged;

namespace Kalisto.Frontend.Notifications.Misc
{
  public interface ISoundNotification
  {
    void Play();

    void Stop();

    bool IsPlaying { get; }

    bool SoundEnabled { get; set; }
  }

  [AddINotifyPropertyChangedInterface]
  public class SoundNotification : ISoundNotification
  {
    public bool IsPlaying { get; private set; }

    public bool SoundEnabled { get; set; }

    public void Play()
    {
      if (IsPlaying)
        return;

      IsPlaying = true;
      var stream = Properties.Resources.beep;
      var player = new SoundPlayer(stream);
      Task.Run(async () =>
      {
        while (IsPlaying)
        {
          if (!SoundEnabled)
            Stop();
          else
          {
            player.Play();
            await Task.Delay(1000);
          }
        }
      });
    }

    public void Stop()
    {
      IsPlaying = false;
    }
  }
}
