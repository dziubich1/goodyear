﻿using Kalisto.Frontend.Notifications.Base;

namespace Kalisto.Frontend.Notifications
{
  public class CardReaderProgrammingModeStateChangedNotification : NotificationBase
  {
    public override string NotificationId => nameof(CardReaderProgrammingModeStateChangedNotification);

    public bool ProgrammingModeEnabled { get; set; }
  }
}
