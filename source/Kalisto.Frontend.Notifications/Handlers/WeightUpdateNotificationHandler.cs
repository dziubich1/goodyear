﻿using Caliburn.Micro;
using Kalisto.Core;
using Kalisto.Frontend.Notifications.Base;
using Kalisto.Frontend.Notifications.Enums;
using Kalisto.Frontend.Notifications.Misc;

namespace Kalisto.Frontend.Notifications.Handlers
{
  public class WeightUpdateNotificationHandler : INotificationHandler<WeighingValueUpdatedNotification>
  {
    private readonly IWindowInputManager _windowInputManager;

    private readonly ISoundNotification _soundNotification;

    private WeighingDeviceStatus? _previousStatus;

    private IViewAware _viewAware;

    public WeightUpdateNotificationHandler(IWindowInputManager windowInputManager, ISoundNotification soundNotification)
    {
      _windowInputManager = windowInputManager;
      _windowInputManager.InputDetected += OnInputDetected;
      _soundNotification = soundNotification;
    }

    public void RegisterNotificationSource(IViewAware viewAware)
    {
      _viewAware = viewAware;
    }

    public void Handle(WeighingValueUpdatedNotification notification)
    {
      if ((!_previousStatus.HasValue || _previousStatus.Value == WeighingDeviceStatus.Stable) &&
          notification.Status == WeighingDeviceStatus.NotStable && !_soundNotification.IsPlaying)
      {
        if (_windowInputManager.RegisterView(_viewAware))
          _soundNotification.Play();
      }

      _previousStatus = notification.Status;
    }

    private void OnInputDetected()
    {
      _soundNotification.Stop();
    }
  }
}
