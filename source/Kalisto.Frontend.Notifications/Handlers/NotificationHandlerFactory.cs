﻿using Caliburn.Micro;
using Kalisto.Frontend.Notifications.Base;
using Unity;

namespace Kalisto.Frontend.Notifications.Handlers
{
  public interface INotificationHandlerFactory
  {
    void RegisterNotificationSource(IViewAware viewAware);

    INotificationHandler<WeighingValueUpdatedNotification> GetWeightUpdateNotificationHandler();
  }

  public class NotificationHandlerFactory : INotificationHandlerFactory
  {
    private readonly IUnityContainer _unityContainer;

    private IViewAware _viewAware;

    public NotificationHandlerFactory(IUnityContainer container)
    {
      _unityContainer = container;
    }

    public void RegisterNotificationSource(IViewAware viewAware)
    {
      _viewAware = viewAware;
    }

    public INotificationHandler<WeighingValueUpdatedNotification> GetWeightUpdateNotificationHandler()
    {
      var handler = _unityContainer.Resolve<WeightUpdateNotificationHandler>();
      handler.RegisterNotificationSource(_viewAware);

      return handler;
    }
  }
}
