﻿using Kalisto.Frontend.Notifications.Base;
using Kalisto.Frontend.Notifications.Enums;

namespace Kalisto.Frontend.Notifications
{
  public class WeighingValueUpdatedNotification : NotificationBase
  {
    public override string NotificationId => nameof(WeighingValueUpdatedNotification);

    public int DeviceId { get; set; }

    public WeighingDeviceStatus Status { get; set; }

    public decimal Value { get; set; }
  }
}
