﻿using System;
using Kalisto.Communication.Core.Messages;
using Kalisto.Frontend.Notifications.Base;

namespace Kalisto.Frontend.Notifications
{
  public class DbUpdatedNotification : NotificationBase
  {
    public override string NotificationId => nameof(DbUpdatedNotification);

    public string ViewId { get; set; }

    public IAssociatedCommand AssociatedCommandMetadata { get; set; }
  }
}
