﻿using Kalisto.Frontend.Notifications.Base;

namespace Kalisto.Frontend.Notifications
{
  public class CardScannedNotification : NotificationBase
  {
    public override string NotificationId => nameof(CardScannedNotification);

    public string CardCode { get; set; }
  }
}
