﻿namespace Kalisto.Frontend.Notifications.Enums
{
  public enum WeighingDeviceStatus
  {
    Stable, NotStable, Unknown
  }
}
