﻿using Kalisto.Frontend.Notifications.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kalisto.Communication.Core;

namespace Kalisto.Frontend.Notifications
{
    public class SecondMeasurementCompletedNotification : NotificationBase
    {
        public override string NotificationId => nameof(SecondMeasurementCompletedNotification);

        public long MeasurementId { get; set; }
    }
}