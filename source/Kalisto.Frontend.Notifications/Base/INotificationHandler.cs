﻿using Caliburn.Micro;

namespace Kalisto.Frontend.Notifications.Base
{
  public interface INotificationHandler<in TNotification> where TNotification : NotificationBase
  {
    void RegisterNotificationSource(IViewAware viewAware);

    void Handle(TNotification notification);
  }
}
