﻿using System;
using Kalisto.Devices.Core.Communication;
using Kalisto.Devices.Core.Messages;

namespace Kalisto.Devices.Core
{
  public interface IDevice
  {
    int Id { get; set; }

    void StartSampling();

    void StopSampling();

    event Action<ConnectionState> ConnectionStateChanged;

    void SendCommand<TCommand>(TCommand command)
      where TCommand : DeviceCommandMessageBase;
  }

  public interface IDevice<out TMessage> : IDevice where TMessage : DeviceMessageBase 
  {
    event Action<TMessage> MessageReceived;
  }
}
