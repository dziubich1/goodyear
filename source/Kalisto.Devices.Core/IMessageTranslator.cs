﻿namespace Kalisto.Devices.Core
{
  public interface IMessageTranslator<out TMessage>
  {
    TMessage Translate(byte[] bytes);
  }

  public interface IMessageTranslator<in TSource, out TMessage>
  {
    TMessage Translate(TSource source);
  }

  public interface IMessageValidator<TMessage>
  {
    bool Validate(byte[] bytes);
  }
}
