﻿using System.Collections.Generic;
using System.Linq;

namespace Kalisto.Devices.Core
{
  public class DeviceSessionManager : IDeviceSessionManager
  {
    private readonly List<DeviceSession> _sessions = new List<DeviceSession>();

    public DeviceSession GetDeviceSession(int deviceId)
    {
      if (_sessions.All(c => c.DeviceId != deviceId))
        _sessions.Add(new DeviceSession(deviceId));

      return _sessions.First(c => c.DeviceId == deviceId);
    }
  }
}
