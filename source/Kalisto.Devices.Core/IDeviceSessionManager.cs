﻿namespace Kalisto.Devices.Core
{
  public interface IDeviceSessionManager
  {
    DeviceSession GetDeviceSession(int deviceId);
  }
}
