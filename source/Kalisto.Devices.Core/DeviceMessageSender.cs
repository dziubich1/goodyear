﻿using Kalisto.Communication.Core.Messaging;
using Kalisto.Devices.Core.Messages;
using System;

namespace Kalisto.Devices.Core
{
  public class DeviceMessageSender<TMessage> where TMessage : DeviceMessageBase
  {
    private readonly IClientMessageProvider _messageProvider;

    private readonly ClientInfo _clientInfo;

    public DeviceMessageSender(IClientMessageProvider messageProvider, ClientInfo clientInfo)
    {
      _messageProvider = messageProvider;
      _clientInfo = clientInfo;
    }

    public void Initialize(IDevice<TMessage> device)
    {
      device.MessageReceived += OnMessageReceived;
    }

    private void OnMessageReceived(TMessage message)
    {
      message.PartnerId = _clientInfo.Id;
      message.OwnerGuid = Guid.NewGuid().ToString();

      _messageProvider.SendMessage(message);
    }
  }
}
