﻿using System.IO.Ports;
using Kalisto.Devices.Core.Communication.Interfaces;
using Kalisto.Devices.Core.Communication.SerialPort;
using Kalisto.Devices.Core.Communication.Sockets.Tcp;

namespace Kalisto.Devices.Core.Communication
{
  public interface IDeviceConnectionFactory
  {
    IDeviceClientConnection GetTcpConnection(string ipAddress, int port);

    IDeviceClientConnection GetSerialPortConnection(string portName, int baudRate, Parity parity, int dataBits, StopBits stopBits);
  }

  public class DeviceConnectionFactory : IDeviceConnectionFactory
  {
    public IDeviceClientConnection GetTcpConnection(string ipAddress, int port)
    {
      return new TcpClientConnection(ipAddress, port, sendTimeout: 5000, receiveTimeout: 15000, reconnectAfterSendingMessage: false);
    }

    public IDeviceClientConnection GetSerialPortConnection(string portName, int baudRate, Parity parity, int dataBits,
      StopBits stopBits)
    {
      return new SerialPortConnection(portName, baudRate, parity, dataBits, stopBits);
    }
  }
}
