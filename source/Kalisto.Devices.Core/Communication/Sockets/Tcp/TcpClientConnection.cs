﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using Kalisto.Devices.Core.Communication.Interfaces;

namespace Kalisto.Devices.Core.Communication.Sockets.Tcp
{
  public class TcpClientConnection : TcpConnection, IDeviceClientConnection
  {
    private readonly ManualResetEvent _newConnectEvent = new ManualResetEvent(true);

    private readonly ManualResetEvent _closingEvent = new ManualResetEvent(false);

    private const int WaitOneTimeout = 10000;

    private readonly bool _reconnectAfterSendingMessage;

    private bool _preventReconnecting;

    private bool _reconnectAutomatically;

    private int _reconnectDelay;

    private IPEndPoint _remoteEndpoint;

    public string ServerAddress { get; }

    public int Port { get; }

    public event Action<ConnectionState> ConnectionStateChanged;

    public TcpClientConnection(string server, int port, int sendTimeout, int receiveTimeout,
        bool reconnectAfterSendingMessage)
        : base(sendTimeout, receiveTimeout)
    {
      _reconnectAfterSendingMessage = reconnectAfterSendingMessage;

      ServerAddress = server;
      Port = port;
    }

    public void Connect()
    {
      Disconnect();

      _preventReconnecting = false;
      InternalClose = false;
      bool connected = false;
      bool finished = false;
      while (!finished)
      {
        try
        {
          connected = TryConnect();
        }
        catch (Exception)
        {
          // ignore
        }

        if (!connected)
        {
          if (!_preventReconnecting && _reconnectAutomatically && Environment.OSVersion.Version.Major >= 6)
          {
            Thread.Sleep(_reconnectDelay);
          }
          else
            finished = true;
        }
        else
          finished = true;
      }
    }

    public void Connect(bool reconnect)
    {
      _reconnectAutomatically = reconnect;
      Connect();
    }

    public void Connect(bool reconnect, int delay)
    {
      _reconnectAutomatically = reconnect;
      _reconnectDelay = delay;
      Connect();
    }

    public void Disconnect()
    {
      Disconnect(false);
    }

    public void Disconnect(bool waitForTermination)
    {
      Disconnect(waitForTermination, false);
    }

    public void Disconnect(bool waitForTermination, bool reconnectAfterSendingTelegram)
    {
      var wasConnected = ConnectionSocket?.Connected ?? false;
      _preventReconnecting = true;
      InternalClose = true;

      if (!reconnectAfterSendingTelegram)
      {
        _newConnectEvent.WaitOne();
      }
      TryDisconnect(reconnectAfterSendingTelegram);


      if (waitForTermination && !reconnectAfterSendingTelegram)
        _closingEvent.WaitOne(WaitOneTimeout);

      if (wasConnected && (ConnectionSocket == null || !ConnectionSocket.Connected))
        ConnectionStateChanged?.Invoke(ConnectionState.Disconnected);
    }

    protected void BeginConnect(IPEndPoint endpoint)
    {
      _newConnectEvent.Reset();

      ConnectionStateChanged?.Invoke(ConnectionState.Connecting);
      var result = ConnectionSocket.BeginConnect(endpoint, NewConnectionCallback, null);
      result.AsyncWaitHandle.WaitOne(5000, true);
      if (!ConnectionSocket.Connected)
        ConnectionSocket.Close();
    }

    private void NewConnectionCallback(IAsyncResult result)
    {
      var connectedSuccessfully = false;
      try
      {
        if (ConnectionSocket.Connected)
        {
          ConnectionSocket.EndConnect(result);
          _newConnectEvent.Set();
          OnConnectionEstablished(ConnectionSocket);
          connectedSuccessfully = true;
          ConnectionStateChanged?.Invoke(ConnectionState.Connected);
        }
        else ConnectionStateChanged?.Invoke(ConnectionState.NotAvailable);
      }
      catch (Exception)
      {
        ConnectionStateChanged?.Invoke(ConnectionState.NotAvailable);
      }

      if (!connectedSuccessfully)
        TryReconnect();
    }

    private void TryReconnect()
    {
      if (!_preventReconnecting && _reconnectAutomatically && Environment.OSVersion.Version.Major >= 6)
      {
        Thread.Sleep(_reconnectDelay);
        try
        {
          TryConnect();
        }
        catch (Exception)
        {
          _newConnectEvent.Set();
        }
      }
      else
      {
        _newConnectEvent.Set();
      }
    }

    private bool TryConnect()
    {
      string errorMessage;
      try
      {
        ConnectionSocket = GetSocket(out _remoteEndpoint, out errorMessage);
        SetKeepAliveForSocket(ConnectionSocket);
      }
      catch (Exception)
      {
        Disconnect();
        _remoteEndpoint = null;
        return false;
      }

      if (ConnectionSocket == null || !string.IsNullOrEmpty(errorMessage))
        return false;

      BeginConnect(_remoteEndpoint);
      return true;
    }

    private void ConnectSync(bool rethrowException = false)
    {
      Disconnect();

      _preventReconnecting = false;
      InternalClose = false;
      string errorMessage;

      try
      {
        ConnectionSocket = GetSocket(out _remoteEndpoint, out errorMessage);
        SetKeepAliveForSocket(ConnectionSocket);
      }
      catch (Exception)
      {
        Disconnect();
        _remoteEndpoint = null;
        return;
      }

      if (ConnectionSocket == null || !string.IsNullOrEmpty(errorMessage))
        return;

      try
      {
        ConnectionSocket.Connect(_remoteEndpoint);
      }
      catch (SocketException)
      {
        if (rethrowException)
          throw;

        if (_reconnectAutomatically && !_preventReconnecting && Environment.OSVersion.Version.Major >= 6)
        {
          Thread.Sleep(_reconnectDelay);
          Connect();
        }
      }
    }

    protected override Socket GetSocket(out IPEndPoint remoteEp, out string errorMessage)
    {
      errorMessage = string.Empty;

      var address = ResolveAddress();
      if (address == null)
      {
        throw new ArgumentException();
      }

      remoteEp = new IPEndPoint(address, Port);
      if (!ValidateEndpoint(remoteEp))
        throw new ArgumentException();

      var socket = CreateSocket(remoteEp.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
      return socket;
    }

    private IPAddress ResolveAddress()
    {
      if (IPAddress.TryParse(ServerAddress, out var adr))
        return adr;

      var ipHostInfo = Dns.GetHostEntry(ServerAddress);
      foreach (var resolvedAddress in ipHostInfo.AddressList)
      {
        adr = resolvedAddress;
        break;
      }

      return adr;
    }

    public override bool SendBytes(byte[] bytes)
    {
      _newConnectEvent.WaitOne();

      if (ConnectionSocket == null || !ConnectionSocket.Connected)
        return false;

      lock (SenderLock)
      {
        try
        {
          ConnectionSocket.Send(bytes);
        }
        catch (Exception)
        {
          // ignore
        }
      }

      if (!_reconnectAfterSendingMessage)
        return true;

      Disconnect(true, _reconnectAfterSendingMessage);
      ConnectSync();
      return true;
    }

    protected override void OnTcpConnectionTerminated(Socket socket, string endpointId)
    {
      bool reconnect = !_preventReconnecting && _reconnectAutomatically && Environment.OSVersion.Version.Major >= 6;
      if (reconnect)
      {
        Thread.Sleep(_reconnectDelay);
        Connect();
      }
      _closingEvent.Set();
    }

    protected override void OnBytesReceivingStarting()
    {
      _closingEvent.Reset();
    }
  }
}
