﻿using System;
using System.Net.Sockets;
using System.Threading;
using Kalisto.Devices.Core.Communication.Interfaces;

namespace Kalisto.Devices.Core.Communication.Sockets.Tcp
{
  public abstract class TcpConnection : SocketConnection, IDeviceConnection
  {
    private Thread _bytesReceivingThread;

    protected volatile bool InternalClose;

    public event BytesMessageReceivedEventHandler BytesMessageReceived;

    protected TcpConnection(int sendTimeout, int receiveTimeout)
      : base(sendTimeout, receiveTimeout)
    {
    }

    protected abstract void OnTcpConnectionTerminated(Socket socket, string endpointId);

    protected void OnConnectionEstablished(Socket socket)
    {
      InternalClose = false;

      //when connection is established bytes receiving thread is started asynchronously.
      StartReceivingBytes(socket);
    }

    protected override void ReceiveBytes(object source)
    {
      try
      {
        var socket = source as Socket;
        if (socket == null)
          throw new ArgumentException();

        var endpointId = socket.RemoteEndPoint.ToString();
        try
        {
          OnBytesReceivingStarting();
          while (!InternalClose)
          {
            var buffer = new byte[10];
            var bytesRead = socket.Receive(buffer);

            if (bytesRead == 0)
            {
              if (!InternalClose)
                OnTcpConnectionTerminated(socket, endpointId);

              return;
            }

            var bytes = new byte[bytesRead];
            Array.Copy(buffer, bytes, bytesRead);

            try
            {
              OnBytesMessageReceived(bytes, socket.RemoteEndPoint.ToString());
            }
            catch (OverflowException)
            {
            }
            catch (Exception e)
            {
            }
          }
        }
        catch (SocketException)
        {
          OnTcpConnectionTerminated(socket, endpointId);
        }
        catch (ThreadInterruptedException)
        {
          OnTcpConnectionTerminated(socket, endpointId);
        }
      }
      catch (ObjectDisposedException)
      {
      }
      catch (Exception)
      {
        // ignore so far
      }
    }

    protected virtual void OnBytesReceivingStarting()
    {
    }

    public void TryDisconnect(bool reconnectAfterSendingTelegram = false)
    {
      try
      {
        if (ConnectionSocket == null)
          return;

        var endpointId = string.Empty;
        if (ConnectionSocket.Connected)
        {
          try
          {
            endpointId = ConnectionSocket.RemoteEndPoint.ToString();
            ConnectionSocket.Shutdown(SocketShutdown.Both);
          }
          catch (SocketException)
          {
          }

          try
          {
            ConnectionSocket.Disconnect(false);
          }
          catch (SocketException)
          {
          }
        }
        ConnectionSocket.Close();
        if (!reconnectAfterSendingTelegram)
          OnTcpConnectionTerminated(ConnectionSocket, endpointId);
        ConnectionSocket = null;

        if (_bytesReceivingThread != null && _bytesReceivingThread.IsAlive)
          _bytesReceivingThread.Interrupt();

        _bytesReceivingThread = null;
      }
      catch (Exception ex)
      {
        throw;
      }
    }

    protected void OnBytesMessageReceived(byte[] bytes, string endpointId)
    {
      var h = BytesMessageReceived;
      h?.Invoke(this, new BytesMessageReceivedEventArgs(bytes, endpointId));
    }

    private void StartReceivingBytes(Socket s)
    {
      _bytesReceivingThread = new Thread(() => ReceiveBytes(s))
      {
        IsBackground = true
      };

      _bytesReceivingThread.Start();
    }
  }
}
