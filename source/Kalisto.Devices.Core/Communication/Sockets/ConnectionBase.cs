﻿using System;

namespace Kalisto.Devices.Core.Communication
{
  public enum ConnectionState
  {
    Connecting, Connected, NotAvailable, Disconnected
  }

  public abstract class ConnectionBase
  {
    protected object ConnectionSyncObj = new object();

    public int SendTimeout { get; private set; }

    public int ReceiveTimeout { get; private set; }

    protected ConnectionBase(int sendTimeout, int receiveTimeout)
    {
      SendTimeout = sendTimeout;
      ReceiveTimeout = receiveTimeout;
    }

    ~ConnectionBase()
    {
      Dispose(false);
    }

    protected abstract void ReceiveBytes(object source);

    public abstract bool SendBytes(byte[] bytes);

    #region IDisposable Members

    protected virtual void Dispose(bool disposing)
    {
      if (disposing)
      {
        // release managed resources
      }
    }

    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }

    #endregion
  }
}
