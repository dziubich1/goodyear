﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;

namespace Kalisto.Devices.Core.Communication.Sockets
{
  public abstract class SocketConnection : ConnectionBase
  {
    protected readonly object SenderLock = new object();

    protected Socket ConnectionSocket { get; set; }

    protected SocketConnection(int sendTimeout, int receiveTimeout)
      : base(sendTimeout, receiveTimeout)
    {
    }

    public abstract override bool SendBytes(byte[] bytes);

    protected abstract override void ReceiveBytes(object source);

    protected abstract Socket GetSocket(out IPEndPoint remoteEp, out string errorMessage);

    protected Socket CreateSocket(AddressFamily addressFamily, SocketType socketType, ProtocolType protocolType)
    {
      return new Socket(addressFamily, socketType, protocolType)
      {
        SendTimeout = SendTimeout,
        ReceiveTimeout = ReceiveTimeout
      };
    }

    protected void SetKeepAliveForSocket(Socket socket)
    {
      uint dummy = 0;
      byte[] inOptionValues = new byte[Marshal.SizeOf(dummy) * 3];
      //set keepalive on
      BitConverter.GetBytes((uint)1).CopyTo(inOptionValues, 0);
      //interval time between last operation on socket and first checking. example:5000ms=5s
      BitConverter.GetBytes((uint)5000).CopyTo(inOptionValues, Marshal.SizeOf(dummy));
      //after first checking, socket will check serval times by 1000ms.
      BitConverter.GetBytes((uint)1000).CopyTo(inOptionValues, Marshal.SizeOf(dummy) * 2);
      socket.IOControl(IOControlCode.KeepAliveValues, inOptionValues, null);
    }

    protected bool ValidateEndpoint(IPEndPoint remoteEp)
    {
      if (Environment.OSVersion.Version.Major >= 5 || remoteEp.AddressFamily != AddressFamily.InterNetworkV6)
        return true;

      return false;
    }

    protected override void Dispose(bool disposing)
    {
      if (disposing)
      {
        if (ConnectionSocket != null)
        {
          ConnectionSocket.Dispose();
          ConnectionSocket = null;
        }
      }

      base.Dispose(disposing);
    }
  }
}
