﻿namespace Kalisto.Devices.Core.Communication.Interfaces
{
  public interface IDeviceConnection
  {
    event BytesMessageReceivedEventHandler BytesMessageReceived;

    bool SendBytes(byte[] bytes);
  }
}
