﻿using System;

namespace Kalisto.Devices.Core.Communication.Interfaces
{
  public interface IDeviceClientConnection : IDeviceConnection
  {
    void Connect(bool reconnect);

    void Disconnect();

    event Action<ConnectionState> ConnectionStateChanged;
  }
}
