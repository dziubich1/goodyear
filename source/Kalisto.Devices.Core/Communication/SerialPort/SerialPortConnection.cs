﻿using Kalisto.Devices.Core.Communication.Interfaces;
using System;
using System.IO.Ports;
using System.Threading.Tasks;

namespace Kalisto.Devices.Core.Communication.SerialPort
{
  public class SerialPortConnection : IDeviceClientConnection
  {
    private readonly string _portName;

    private readonly System.IO.Ports.SerialPort _serialPort;

    private Task _reconnectingTask;

    public event Action<ConnectionState> ConnectionStateChanged;

    public event BytesMessageReceivedEventHandler BytesMessageReceived;

    public SerialPortConnection(string portName, int baudRate, Parity parity, int dataBits, StopBits stopBits)
    {
      _portName = portName;
      _serialPort = new System.IO.Ports.SerialPort(portName,
        baudRate, parity, dataBits, stopBits);
      _serialPort.DataReceived += OnDataReceived;
    }

    private void OnDataReceived(object sender, SerialDataReceivedEventArgs e)
    {
      if (_serialPort.BytesToRead <= 0)
        return;

      byte[] data = new byte[_serialPort.BytesToRead];
      _serialPort.Read(data, 0, data.Length);
      BytesMessageReceived?.Invoke(this, new BytesMessageReceivedEventArgs(data, _portName));
    }

    public void Connect(bool reconnect)
    {
      try
      {
        ConnectionStateChanged?.Invoke(ConnectionState.Connecting);

        if (!_serialPort.IsOpen)
          _serialPort.Open();
        else return;

        if (_serialPort.IsOpen)
          ConnectionStateChanged?.Invoke(ConnectionState.Connected);
        else ConnectionStateChanged?.Invoke(ConnectionState.Disconnected);

        if (reconnect)
        {
          if (_reconnectingTask != null)
            return;

          _reconnectingTask = Task.Factory.StartNew(async () =>
          {
            while (true)
            {
              await Task.Delay(5000);
              Connect(reconnect);
            }
          }, TaskCreationOptions.LongRunning);
        }
      }
      catch (Exception e)
      {
        ConnectionStateChanged?.Invoke(ConnectionState.NotAvailable);
      }
    }

    public void Disconnect()
    {
      try
      {
        _serialPort.Close();
        ConnectionStateChanged?.Invoke(ConnectionState.Disconnected);
      }
      catch (Exception e)
      {
        ConnectionStateChanged?.Invoke(ConnectionState.Disconnected);
      }
    }

    public bool SendBytes(byte[] bytes)
    {
      try
      {
        _serialPort.Write(bytes, 0, bytes.Length);
        return true;
      }
      catch
      {
        return false;
      }
    }
  }
}
