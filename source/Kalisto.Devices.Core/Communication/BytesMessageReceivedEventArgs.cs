﻿using System;

namespace Kalisto.Devices.Core.Communication
{
  public delegate void BytesMessageReceivedEventHandler(object sender, BytesMessageReceivedEventArgs e);

  public class BytesMessageReceivedEventArgs : EventArgs
  {
    public byte[] Bytes { get; }

    public string EndpointId { get; }

    public BytesMessageReceivedEventArgs(byte[] bytes, string endpointId)
    {
      Bytes = bytes;
      EndpointId = endpointId;
    }
  }
}
