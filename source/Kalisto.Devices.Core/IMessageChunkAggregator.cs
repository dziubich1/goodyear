﻿using System;

namespace Kalisto.Devices.Core
{
  public interface IMessageChunkAggregator<out TMessage>
  {
    void AddChunk(byte[] bytes);

    event Action<TMessage> MessageReady;
  }
}
