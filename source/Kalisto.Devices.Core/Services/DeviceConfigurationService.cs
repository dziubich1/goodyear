﻿using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Configuration.Devices;
using Kalisto.Hmi.Core.Communication;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Kalisto.Devices.Core.Services
{
  public class DeviceConfigurationService
  {
    private readonly IFrontendMessageBridge _messageBridge;

    private readonly IMessageBuilder _messageBuilder;

    private readonly ClientInfo _clientInfo;

    public DeviceConfigurationService(IFrontendMessageBridge messageBridge, IMessageBuilder messageBuilder, ClientInfo clientInfo)
    {
      _messageBridge = messageBridge;
      _messageBuilder = messageBuilder;
      _clientInfo = clientInfo;
    }

    public async Task<List<DeviceConfiguration>> GetDeviceConfigurationsAsync()
    {
      var message = _messageBuilder.CreateNew<DeviceConfigurationsRequestMessage>()
        .BasedOn(_clientInfo)
        .Build();

      var result = await _messageBridge.SendMessageWithResultAsync<List<DeviceConfiguration>>(message);
      if (result == null || !result.Succeeded)
        return new List<DeviceConfiguration>();

      return result.Response;
    }
  }
}
