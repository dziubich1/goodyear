﻿using Kalisto.Communication.Core.Messages;
using System;

namespace Kalisto.Devices.Core.Messages
{
  [Serializable]
  public class DeviceMessageBase : EndpointMessageBase
  {
    public const string Identifier = nameof(DeviceMessageBase);

    public override string Id => Identifier;

    public int DeviceId { get; set; }

    public string EndpointAddress { get; set; }

    public bool Corrupted { get; set; }
  }

  [Serializable]
  public class DeviceCommandMessageBase : DeviceMessageBase
  {
    public new const string Identifier = nameof(DeviceCommandMessageBase);

    public override string Id => Identifier;

    public virtual string Body { get; }
  }
}
