﻿using Kalisto.Devices.Core.Handlers;
using System;

namespace Kalisto.Devices.Core.Messages
{
  [Serializable]
  public class DeviceMessage : DeviceMessageBase
  {
    public new const string Identifier = nameof(DeviceMessage);

    public override string Id => Identifier;

    public virtual void Handle(IDeviceMessageHandler handler)
    {

    }
  }

  [Serializable]
  public class DeviceMessage<THandler> : DeviceMessage where THandler : class, IDeviceMessageHandler
  {
    public override void Handle(IDeviceMessageHandler handler)
    {
      var specHandler = handler as THandler;
      if (specHandler == null)
        return;

      Handle(specHandler);
    }

    protected virtual void Handle(THandler handler)
    {

    }
  }
}
