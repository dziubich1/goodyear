﻿namespace Kalisto.Devices.Core
{
  public class DeviceSession
  {
    private object _storedObject;

    public int DeviceId { get; }

    public DeviceSession(int deviceId)
    {
      DeviceId = deviceId;
    }

    public void StoreObject(object obj)
    {
      _storedObject = obj;
    }

    public T GetObject<T>() where T : new()
    {
      if (_storedObject is T obj)
        return obj;

      _storedObject = new T();
      return (T)_storedObject;
    }
  }
}
