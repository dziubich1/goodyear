﻿using System;
using Topshelf;

namespace Kalisto.Devices.Endpoint.ServiceApplication
{
  public class DevicesEndpointService
  {
    public string AppName { get; }

    public string Description => "Kalisto devices endpoint representing the device communication layer of distributed weighing system.";

    private DevicesEndpoint _devicesEndpoint;

    public DevicesEndpointService()
    {
      AppName = typeof(DevicesEndpoint).Assembly.GetName().Name;
    }

    public bool Start(HostControl hostControl)
    {
      try
      {
        if (_devicesEndpoint == null)
          _devicesEndpoint = new DevicesEndpoint("Kalisto", AppName);

        _devicesEndpoint.Configure();
        _devicesEndpoint.Initialize();
        return true;
      }
      catch
      {
        return false;
      }
    }

    public bool Stop(HostControl hostControl)
    {
      try
      {
        _devicesEndpoint.Dispose();
        _devicesEndpoint = null;
        return true;
      }
      catch
      {
        return true;
      }
    }

    public void HandleException(Exception exception)
    {
      _devicesEndpoint.HandleException(exception);
    }
  }
}
