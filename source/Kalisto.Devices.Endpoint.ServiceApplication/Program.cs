﻿using Kalisto.Backend.ServiceApplication;
using log4net.Config;
using Topshelf;

namespace Kalisto.Devices.Endpoint.ServiceApplication
{
  public class Program
  {
    static void Main(string[] args)
    {
      XmlConfigurator.Configure();
      HostFactory.Run(configure =>
      {
        var devicesEndpointService = new DevicesEndpointService();
        configure.Service<DevicesEndpointService>(service =>
        {
          service.ConstructUsing(s => devicesEndpointService);
          service.WhenStarted((s, c) => s.Start(c));
          service.WhenStopped((s, c) => s.Stop(c));
        });

        configure.RunAsLocalSystem();
        configure.SetServiceName(devicesEndpointService.AppName);
        configure.SetDisplayName(devicesEndpointService.AppName);
        configure.SetDescription(devicesEndpointService.Description);
        configure.DependsOn("RabbitMQ");
        configure.DependsOn(new BackendEndpointService().AppName); // only for getting service name

        configure.StartAutomaticallyDelayed();
        configure.OnException(exc => devicesEndpointService.HandleException(exc));
        configure.EnableServiceRecovery(rc =>
        {
          rc.RestartService(1);
        });
      });
    }
  }
}
