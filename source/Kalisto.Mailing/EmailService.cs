﻿using System;
using System.IO;
using System.Net.Mail;
using System.Threading.Tasks;
using Kalisto.Logging;

namespace Kalisto.Mailing
{
  public interface IEmailService
  {
    Task<bool> SendMessageAsync<T>(string from, string to, T message)
      where T : IHtmlMessage;
  }

  public class EmailService : IEmailService
  {
    private readonly ILogger _logger;

    private readonly ISmtpSettingsService _smtpSettingsService;

    public EmailService(ILogger logger, ISmtpSettingsService smtpSettingsService)
    {
      _logger = logger;
      _smtpSettingsService = smtpSettingsService;
    }

    public async Task<bool> SendMessageAsync<T>(string from, string to, T message) 
      where T : IHtmlMessage
    {
      if (message == null)
        throw new ArgumentNullException(nameof(message));

      var mailMessage = new MailMessage(from, to)
      {
        Subject = message.Subject,
        IsBodyHtml = true,
        Body = message.GetRawHtmlString()
      };

      var configuredClient = _smtpSettingsService.GetConfiguredClient();
      if (configuredClient == null)
      {
        _logger.Error("Could not get properly configured SMTP client.");
        return false;
      }

      try
      {
        Validate(configuredClient, mailMessage);

        await configuredClient.SendMailAsync(mailMessage);
        return true;
      }
      catch (Exception e)
      {
        _logger.Error($"Failed to send an email message. Given exception was thrown: {e.Message}");
        return false;
      }
      finally
      {
        configuredClient.Dispose();
        mailMessage.Dispose();
      }
    }

    private void Validate(SmtpClient client, MailMessage message)
    {
      if (string.IsNullOrEmpty(client.Host))
        throw new InvalidDataException("Host cannot be empty");

      // todo: add some more sophisticated validation here
    }
  }
}
