﻿using System;
using System.Net;
using System.Net.Mail;
using Kalisto.Configuration;

namespace Kalisto.Mailing.Configuration
{
  [Serializable]
  public class SmtpSettings : ConfigurationBase
  {
    public string Host { get; set; }

    public int Port { get; set; }

    public string From { get; set; }

    public string Password { get; set; }

    public SmtpClient GetConfiguredClient()
    {
      var client = new SmtpClient
      {
        Host = Host,
        Port = Port,
        UseDefaultCredentials = false,
        EnableSsl = true,
        DeliveryMethod = SmtpDeliveryMethod.Network
      };

      // IMPORTANT: make sure to assign credentials after setting UseDefaultCredentials to false!!
      client.Credentials = new NetworkCredential(From, Password);
      return client;
    }

    public override object GetDefaultConfiguration()
    {
      return new SmtpSettings();
    }
  }
}
