﻿using Kalisto.Configuration;

namespace Kalisto.Mailing.Configuration
{
  public class SmtpSettingsConfigurationManager 
    : ProgramDataConfigurationManager
  {
    protected override string ConfigFileName => "SmtpSettings.xml";

    public SmtpSettingsConfigurationManager(string domainName, string appName) 
      : base(domainName, appName)
    {
    }
  }
}
