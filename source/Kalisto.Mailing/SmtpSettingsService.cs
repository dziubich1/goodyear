﻿using System.Net.Mail;
using Kalisto.Mailing.Configuration;

namespace Kalisto.Mailing
{
  public interface ISmtpSettingsService
  {
    SmtpClient GetConfiguredClient();

    SmtpSettings GetSmtpSettings();

    bool StoreSmtpSettings(SmtpSettings settings);
  }

  public class SmtpSettingsService : ISmtpSettingsService
  {
    private readonly SmtpSettingsConfigurationManager _configurationManager;

    public SmtpSettingsService(SmtpSettingsConfigurationManager configurationManager)
    {
      _configurationManager = configurationManager;
    }

    public SmtpClient GetConfiguredClient()
    {
      var configuration = _configurationManager.Load<SmtpSettings>();
      return configuration.GetConfiguredClient();
    }

    public SmtpSettings GetSmtpSettings()
    {
      return _configurationManager.Load<SmtpSettings>();
    }

    public bool StoreSmtpSettings(SmtpSettings settings)
    {
      try
      {
        _configurationManager.Save(settings);
        return true;
      }
      catch
      {
        return false;
      }
    }
  }
}
