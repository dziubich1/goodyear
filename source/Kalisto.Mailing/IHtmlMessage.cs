﻿namespace Kalisto.Mailing
{
  public interface IHtmlMessage
  {
    string Subject { get; }

    string GetRawHtmlString();
  }
}
