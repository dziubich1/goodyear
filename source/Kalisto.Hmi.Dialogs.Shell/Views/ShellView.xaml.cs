﻿using System.Windows;
using Fluent;

namespace Kalisto.Hmi.Dialogs.Shell.Views
{
  /// <summary>
  /// Interaction logic for ShellView.xaml
  /// </summary>
  public partial class ShellView : MahApps.Metro.Controls.MetroWindow
  {
    public ShellView()
    {
      InitializeComponent();
    }
  }
}
