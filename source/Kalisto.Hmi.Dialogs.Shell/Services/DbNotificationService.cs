﻿using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Frontend.Notifications;
using Kalisto.Hmi.Core.Communication;
using Kalisto.Hmi.Core.Services;
using System;

namespace Kalisto.Hmi.Dialogs.Shell.Services
{
  public interface IDbNotificationService
  {
    event Action<DbUpdatedNotification> DbUpdated;
  }

  public class DbNotificationService : BackendOperationService, IDbNotificationService
  {
    public event Action<DbUpdatedNotification> DbUpdated;

    public DbNotificationService(ClientInfo clientInfo, IMessageBuilder messageBuilder,
      IFrontendMessageBridge messageBridge, IDialogService dialogService) : base(clientInfo, messageBuilder,
      messageBridge, dialogService)
    {
      RegisterNotificationHandler<DbUpdatedNotification>(OnDbUpdatedNotificationReceived);
    }

    private void OnDbUpdatedNotificationReceived(object obj)
    {
      var notification = obj as DbUpdatedNotification;
      if (notification == null)
        return;

      DbUpdated?.Invoke(notification);
    }
  }
}
