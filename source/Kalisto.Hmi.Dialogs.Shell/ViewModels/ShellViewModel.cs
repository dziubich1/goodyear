﻿using Caliburn.Micro;
using Kalisto.Backend.Operations.Measurements;
using Kalisto.Backend.Operations.Shell.Commands;
using Kalisto.Backend.Operations.Shell.Exceptions;
using Kalisto.Backend.Operations.Shell.Queries;
using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Configuration.CompanyInfo;
using Kalisto.Devices.BarcodeScanner.Messages;
using Kalisto.Devices.Endpoint;
using Kalisto.Frontend.Notifications;
using Kalisto.Frontend.Notifications.Base;
using Kalisto.Frontend.Notifications.Enums;
using Kalisto.Frontend.Notifications.Handlers;
using Kalisto.Frontend.Notifications.Misc;
using Kalisto.Hmi.Core;
using Kalisto.Hmi.Core.Commands;
using Kalisto.Hmi.Core.Communication;
using Kalisto.Hmi.Core.Services;
using Kalisto.Hmi.Dialogs.BeltConveyorMeasurements.ViewModels;
using Kalisto.Hmi.Dialogs.Cards.ViewModels;
using Kalisto.Hmi.Dialogs.Cargoes.Services;
using Kalisto.Hmi.Dialogs.Cargoes.ViewModels;
using Kalisto.Hmi.Dialogs.Common;
using Kalisto.Hmi.Dialogs.Common.Configuration;
using Kalisto.Hmi.Dialogs.Common.Enum;
using Kalisto.Hmi.Dialogs.Common.Models;
using Kalisto.Hmi.Dialogs.Contractors.Services;
using Kalisto.Hmi.Dialogs.Contractors.ViewModels;
using Kalisto.Hmi.Dialogs.Dashboard.Models;
using Kalisto.Hmi.Dialogs.Dashboard.Services;
using Kalisto.Hmi.Dialogs.Dashboard.ViewModels;
using Kalisto.Hmi.Dialogs.DbEventLogs.ViewModels;
using Kalisto.Hmi.Dialogs.Drivers.ViewModels;
using Kalisto.Hmi.Dialogs.Measurements.Models;
using Kalisto.Hmi.Dialogs.Measurements.Services;
using Kalisto.Hmi.Dialogs.Measurements.ViewModels;
using Kalisto.Hmi.Dialogs.Shell.Dialogs;
using Kalisto.Hmi.Dialogs.Shell.Services;
using Kalisto.Hmi.Dialogs.SystemEventLogs.ViewModels;
using Kalisto.Hmi.Dialogs.Users.ViewModels;
using Kalisto.Hmi.Dialogs.Vehicles.Services;
using Kalisto.Hmi.Dialogs.Vehicles.ViewModels;
using Kalisto.Logging;
using Kalisto.Mailing.Configuration;
using Kalisto.Mailing.Notifications.Configuration;
using MahApps.Metro.Controls.Dialogs;
using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Kalisto.Communication.Core.Exceptions;
using Kalisto.Hmi.Dialogs.Common.Services;
using Kalisto.Localization;
using Kalisto.Localization.Messages;
using Unity;
using Kalisto.Configuration.MeasurementExportSettings;

namespace Kalisto.Hmi.Dialogs.Shell.ViewModels
{
  [AddINotifyPropertyChangedInterface]
  public class ShellViewModel : ShellViewModelBase
  {
    private readonly ILocalizer<Localization.Common.Common> _commonLocalizer;

    private readonly ILocalizer<Messages> _messagesLocalizer;

    private readonly IClientLogoService _clientLogoService;

    private readonly IAuthorizationService _authorizationService;

    private readonly INotificationHandlerFactory _notificationHandlerFactory;

    private readonly IVehicleService _vehicleService;

    private readonly IMeasurementService _measurementService;

    private readonly ICargoService _cargoService;

    private readonly IContractorService _contractorService;

    private readonly INotificationHandler<WeighingValueUpdatedNotification> _weight1NotificationHandler;

    private readonly INotificationHandler<WeighingValueUpdatedNotification> _weight2NotificationHandler;

    private readonly IUnitTypeService _unitTypeService;

    private readonly IUserPreferencesService _userPreferencesService;

    private readonly IWeighingReportService _weighingReportService;

    private readonly ILocalDeviceMessageBus _localDeviceMessageBus;

    private readonly IWeighingDeviceService _weighingDeviceService;

    private readonly ILogger _logger;

    public ISoundNotification SoundNotification { get; }

    public Image LogoImage { get; set; }

    public WeighingDeviceInfo Device1Info { get; set; }

    [AlsoNotifyFor(nameof(SecondWeightControlVisible))]
    public WeighingDeviceInfo Device2Info { get; set; }

    public bool ShouldDisplayDashboardPreview { get; set; }

    public ICommand PerformSingleWeighingCommand { get; set; }

    public ICommand PerformFirstDoubleWeighingCommand { get; set; }

    public ICommand PerformSecondDoubleWeighingCommand { get; set; }

    public ICommand PerformTaringCommand { get; set; }

    public bool SecondWeightControlVisible => Device2Info != null;

    public ShellViewModel(IUnityContainer container, IDialogService dialogService, IFrontendMessageBridge messageBridge,
      IMessageBuilder messageBuilder, IAsyncProgressService asyncProgressService, ClientInfo clientInfo,
      IClientLogoService clientLogoService, IAuthorizationService authorizationService, IUserPreferencesService preferencesService, IWeighingReportService weighingReportService, IDbNotificationService dbNotificationService,
      INotificationHandlerFactory notificationHandlerFactory, IUnitTypeService unitTypeService, ISoundNotification soundNotification, IDashboardService dashboardService, IVehicleService vehicleService, IMeasurementService measurementService, ILocalDeviceMessageBus localDeviceMessageBus, ICargoService cargoService, IContractorService contractorService, ILogger logger, IWeighingDeviceService weighingDeviceService)
      : base(container, dialogService, messageBridge, messageBuilder, asyncProgressService, clientInfo)
    {
      _commonLocalizer = new LocalizationManager<Localization.Common.Common>();
      _messagesLocalizer = new LocalizationManager<Messages>();

      _clientLogoService = clientLogoService;
      _authorizationService = authorizationService;
      _unitTypeService = unitTypeService;
      _vehicleService = vehicleService;
      _measurementService = measurementService;
      _cargoService = cargoService;
      _contractorService = contractorService;
      _userPreferencesService = preferencesService;
      _weighingReportService = weighingReportService;
      _localDeviceMessageBus = localDeviceMessageBus;
      _localDeviceMessageBus.BarcodeScannerMessageReceived += OnBarcodeScannerMessageReceived;
      _weighingDeviceService = weighingDeviceService;
      _logger = logger;

      SoundNotification = soundNotification;
      _notificationHandlerFactory = notificationHandlerFactory;
      _notificationHandlerFactory.RegisterNotificationSource(this);
      _weight1NotificationHandler = _notificationHandlerFactory.GetWeightUpdateNotificationHandler();
      _weight2NotificationHandler = _notificationHandlerFactory.GetWeightUpdateNotificationHandler();

      PerformSingleWeighingCommand = new DelegateCommand(PerformSingleWeighing);
      PerformFirstDoubleWeighingCommand = new DelegateCommand(PerformFirstDoubleWeighing);
      PerformSecondDoubleWeighingCommand = new DelegateCommand(PerformSecondDoubleWeighing);
      PerformTaringCommand = new DelegateCommand(PerformTaring);

      dashboardService.WeighingValueUpdated += OnWeighingValueUpdated;
      dbNotificationService.DbUpdated += OnDbUpdated;
    }

    protected override void OnViewReady(object view)
    {
      base.OnViewReady(view);

      LogoImage = _clientLogoService.LoadLogo();
    }

    protected override async void OnViewLoaded(object view)
    {
      base.OnViewLoaded(view);

      await TryLogin();
      await LoadDeviceConfiguration();
      OpenMeasurementsView();
    }

    private async Task LoadDeviceConfiguration()
    {
      // weight metering devices config
      var weightMeters = await _weighingDeviceService.GetWeighingDevicesAsync();
      var weightConfigs = weightMeters.Select(c => new WeighingStationInfo
      {
        DeviceInfo = new WeighingDeviceInfo
        {
          Id = c.Id,
          Name = c.Name,
          DeviceStatus = WeighingDeviceStatus.Unknown
        }
      }).ToList();

      if (!weightConfigs.Any())
        return;

      var weight1 = weightConfigs.ElementAtOrDefault(0);
      if (weight1 != null)
      {
        Device1Info = new WeighingDeviceInfo
        {
          Id = weight1.DeviceInfo.Id,
          Name = weight1.DeviceInfo.Name,
          DeviceStatus = WeighingDeviceStatus.Unknown
        };
      }

      var weight2 = weightConfigs.ElementAtOrDefault(1);
      if (weight2 != null)
      {
        Device2Info = new WeighingDeviceInfo
        {
          Id = weight2.DeviceInfo.Id,
          Name = weight2.DeviceInfo.Name,
          DeviceStatus = WeighingDeviceStatus.Unknown
        };
      }
    }

    protected override void ChangeActiveItem(ViewModelBase newItem, bool closePrevious)
    {
      base.ChangeActiveItem(newItem, closePrevious);

      TryShowDashboardPreview(newItem);
    }

    protected override async void OnLoginRequiredReceived()
    {
      base.OnLoginRequiredReceived();

      await Application.Current.Dispatcher.Invoke(async () =>
      {
        await TryLogin();
      });
    }

    public void OpenUsersView()
    {
      ActivateItem<UsersViewModel>();
    }

    public void OpenDashboardView()
    {
      ActivateItem<DashboardViewModel>();
    }

    public void OpenMeasurementsView()
    {
      ActivateItem<MeasurementsViewModel>();
    }

    public void OpenBeltConveyorMeasurementsView()
    {
      ActivateItem<BeltConveyorMeasurementsViewModel>();
    }

    public void OpenSystemEventLogsView()
    {
      ActivateItem<SystemEventLogsViewModel>();
    }

    public void OpenDbEventLogsView()
    {
      ActivateItem<DbEventLogsViewModel>();
    }

    public void OpenContractorsView()
    {
      ActivateItem<ContractorsViewModel>();
    }

    public void OpenCargoesView()
    {
      ActivateItem<CargoesViewModel>();
    }

    public void OpenDriversView()
    {
      ActivateItem<DriversViewModel>();
    }

    public void OpenCardsView()
    {
      ActivateItem<CardsViewModel>();
    }

    public void OpenVehiclesView()
    {
      ActivateItem<VehiclesViewModel>();
    }

    public async void OpenSmtpSettingsDialog()
    {
      await DialogService.ShowModalDialogAsync<SmtpSettingsDialogViewModel>(
        null,
        async viewModel =>
        {
          var message = MessageBuilder.CreateNew<StoreSmtpSettingsCommand>()
            .BasedOn(ClientInfo)
            .WithProperty(c => c.SmtpSettings, new SmtpSettings
            {
              From = viewModel.From,
              Port = viewModel.PortValue,
              Host = viewModel.Host,
              Password = viewModel.Password
            }).Build();

          var result = await MessageBridge.SendMessageWithResultAsync<bool>(message);
          if (result == null || !result.Succeeded)
            return false;

          return result.Response;
        });
    }

    public async void OpenNotificationSettingsDialog()
    {
      await DialogService.ShowModalDialogAsync<NotificationSettingsDialogViewModel>(
        null,
        async viewModel =>
        {
          var message = MessageBuilder.CreateNew<StoreNotificationSettingsCommand>()
            .BasedOn(ClientInfo)
            .WithProperty(c => c.NotificationSettings, new NotificationSettings
            {
              Enabled = viewModel.NotificationsEnabled,
              Type = (NotificationType)viewModel.SelectedNotificationType.Enum,
              From = viewModel.From,
              To = viewModel.To
            }).Build();

          var result = await MessageBridge.SendMessageWithResultAsync<bool>(message);
          if (result == null || !result.Succeeded)
            return false;

          return result.Response;
        });
    }

    public async void OpenLicensePreKeyDialog()
    {
      await DialogService.ShowModalDialogAsync<LicensePreKeyDialogViewModel>(
        null,
        async vm =>
        {
          var message = MessageBuilder.CreateNew<GenerateLicensePreKeyCommand>()
            .BasedOn(ClientInfo)
            .WithProperty(c => c.StationsCount, vm.StationsCountValue).Build();

          var result = await MessageBridge.SendMessageWithResultAsync<string>(message);
          if (result == null || !result.Succeeded)
            return false;

          await DialogService.ShowModalDialogAsync<SingleKeyValueCopyableDialogViewModel>(vm2 =>
          {
            vm2.DialogNameKey = "LicensePreKeyTitle";
            vm2.Key = _commonLocalizer.Localize(() => Localization.Common.Common.LicensePreKeyLabel);
            vm2.Value = result.Response;
            vm2.IsReadOnly = true;
          });

          return result.Succeeded;
        });
    }

    public async void OpenLicenseKeyDialog()
    {
      await DialogService.ShowModalDialogAsync<LicenseKeyDialogViewModel>(
        null,
        async vm =>
        {
          var message = MessageBuilder.CreateNew<AssignLicenseKeyCommand>()
            .BasedOn(ClientInfo)
            .WithProperty(c => c.LicenseKey, vm.LicenseKey).Build();

          var result = await MessageBridge.SendMessageWithResultAsync<bool>(message);
          if (result == null || !result.Succeeded)
          {
            await DialogService.ShowMessageAsync(_messagesLocalizer.Localize(() => Messages.Error),
                _messagesLocalizer.Localize(() => Messages.InvalidLicenseKeyEntered), MessageDialogType.Ok);
            return false;
          }

          if (result.Response)
            await DialogService.ShowMessageAsync(_messagesLocalizer.Localize(() => Messages.ThankYou),
              _messagesLocalizer.Localize(() => Messages.ValidLicenseKeyEntered), MessageDialogType.Ok);

          return result.Succeeded;
        });
    }

    public async Task<DialogResult<LicenseKeyWithPreKeyDialogViewModel>> OpenLicenseKeyWithPreKeyDialog()
    {
      return await DialogService.ShowModalDialogAsync<LicenseKeyWithPreKeyDialogViewModel>(
        null,
        async vm =>
        {
          var licenseMessage = MessageBuilder.CreateNew<AssignLicenseKeyCommand>()
            .BasedOn(ClientInfo)
            .WithProperty(c => c.LicenseKey, vm.LicenseKey).Build();

          var result = await MessageBridge.SendMessageWithResultAsync<bool>(licenseMessage);
          if (result == null || !result.Succeeded)
          {
            await DialogService.ShowMessageAsync(_messagesLocalizer.Localize(() => Messages.Error), _messagesLocalizer.Localize(() => Messages.InvalidLicenseKeyEntered), MessageDialogType.Ok);
            return false;
          }

          if (result.Response)
            await DialogService.ShowMessageAsync(_messagesLocalizer.Localize(() => Messages.ThankYou), _messagesLocalizer.Localize(() => Messages.ValidLicenseKeyEntered), MessageDialogType.Ok);
          else
          {
            await DialogService.ShowMessageAsync(_messagesLocalizer.Localize(() => Messages.Error), _messagesLocalizer.Localize(() => Messages.InvalidLicenseKeyEntered), MessageDialogType.Ok);
            return false;
          }

          return result.Succeeded;
        });
    }

    public async Task OpenLogoutDialog()
    {
      var promptResult = await DialogService.ShowMessageAsync(_messagesLocalizer.Localize(() => Messages.LogoutText), _messagesLocalizer.Localize(() => Messages.LogoutQuestion),
        MessageDialogType.YesNo);
      if (promptResult != MessageDialogResult.Affirmative)
        return;

      var message = MessageBuilder.CreateNew<LogoutCommand>()
        .BasedOn(ClientInfo)
        .Build();

      var result = await MessageBridge.SendMessageWithResultAsync<bool>(message);
      var logoutSucceeded = result.Response;
      if (!result.Succeeded || !logoutSucceeded)
      {
        await DialogService.ShowMessageAsync(_messagesLocalizer.Localize(() => Messages.Error), _messagesLocalizer.Localize(() => Messages.LogoutErrorOccurred),
          MessageDialogType.Ok);
        return;
      }

      _authorizationService.ClearUserSession();
      await TryLogin();
      OpenMeasurementsView();
    }

    public async void OpenMeasurementsCollationDialog()
    {
      await DialogService.ShowModalDialogAsync<MeasurementsCollationDialogViewModel>(null);
    }

    public async void OpenUserPreferencesDialog()
    {
      var currentActiveItemType = ActiveItem.GetType();
      ActiveItem = null;

      await DialogService.ShowModalDialogAsync<UserPreferencesDialogViewModel>(vm => { vm.ValidateOnLoad = true; }, vm =>
        {
          _userPreferencesService.StorePreferences(vm.SelectedConfiguration);
          return Task.FromResult(true);
        });
      ActivateItem(currentActiveItemType);
    }

    public async void OpenCompanyInfoDialog()
    {
      await DialogService.ShowModalDialogAsync<CompanyInfoDialogViewModel>(
        async vm =>
        {
          vm.ValidateOnLoad = true;
          var message = MessageBuilder.CreateNew<GetCompanyInfoConfigurationQuery>()
            .BasedOn(ClientInfo)
            .Build();
          var result = await MessageBridge.SendMessageWithResultAsync<CompanyInfoConfiguration>(message);
          if (result != null && result.Succeeded && result.Response != null)
          {
            vm.CompanyTaxNumber = result.Response.TaxNumber;
            vm.CompanyName = result.Response.Name;
            vm.CompanyStreet = result.Response.Street;
            vm.CompanyPostcode = result.Response.Postcode;
            vm.CompanyCity = result.Response.City;
            vm.CompanyAdditionalInfo = result.Response.AdditionalInfo;
          }
        },
        async vm =>
        {
          var message = MessageBuilder.CreateNew<StoreCompanyInfoConfigurationCommand>()
            .BasedOn(ClientInfo)
            .WithProperty(m => m.CompanyInfoConfiguration, new CompanyInfoConfiguration()
            {
              TaxNumber = vm.CompanyTaxNumber,
              Name = vm.CompanyName,
              Street = vm.CompanyStreet,
              Postcode = vm.CompanyPostcode,
              City = vm.CompanyCity,
              AdditionalInfo = vm.CompanyAdditionalInfo
            })
            .Build();
          var result = await MessageBridge.SendMessageWithResultAsync<bool>(message);
          return result != null && result.Succeeded;
        });
    }


    public async void OpenAboutApplicationDialog()
    {
      await DialogService.ShowModalDialogAsync<AboutApplicationDialogViewModel>(null);
    }

    public async void OpenMeasurementExportSettingsDialog()
    {
      await DialogService.ShowModalDialogAsync<MeasurementExportSettingsViewModel>(
        async vm =>
        {
          vm.ValidateOnLoad = true;
          var message = MessageBuilder.CreateNew<GetMeasurementExportSettingsQuery>()
            .BasedOn(ClientInfo)
            .Build();

          var result = await MessageBridge.SendMessageWithResultAsync<MeasurementExportConfiguration>(message);
          if (result != null && result.Succeeded && result.Response != null)
          {
            vm.MeasurementExportEnabled = result.Response.IsEnabled;
          }
        },
        async vm =>
        {
          var message = MessageBuilder.CreateNew<StoreMeasurementExportSettingsCommand>()
            .BasedOn(ClientInfo)
            .WithProperty(m => m.MeasurementExportConfiguration, new MeasurementExportConfiguration()
            {
              IsEnabled = vm.MeasurementExportEnabled
            })
            .Build();
          var result = await MessageBridge.SendMessageWithResultAsync<bool>(message);
          return result != null && result.Succeeded;
        });
    }

    public override void CanClose(Action<bool> callback)
    {
      callback(true);
      Application.Current.Shutdown();
    }

    private async Task TryLogin()
    {
      var loginResult = await ShowLoginPrompt();
      if (loginResult == null)
      {
        TryClose();
        return;
      }

      var message = MessageBuilder.CreateNew<LoginCommand>()
        .BasedOn(ClientInfo)
        .WithProperty(c => c.Username, loginResult.Username)
        .WithProperty(c => c.Password, loginResult.Password)
        .Build();

      var result = await MessageBridge.SendMessageWithResultAsync<UserInfo>(message);
      if (result == null)
      {
        await DialogService.ShowMessageAsync(_messagesLocalizer.Localize(() => Messages.LoginError), _messagesLocalizer.Localize(() => Messages.DataFetchingError),
          MessageDialogType.Ok);
        await TryLogin();
        return;
      }

      if (!result.Succeeded || result.Response == null)
      {
        if (result.ThrownException != null)
        {
          switch (result.ThrownException)
          {
            case NoLicenseKeyDetectedException licenseException:
              {
                await DialogService.ShowMessageAsync(_messagesLocalizer.Localize(() => Messages.NoLicense), licenseException.Message,
                  MessageDialogType.Ok);

                var dlgResult = await OpenLicenseKeyWithPreKeyDialog();
                if (dlgResult.Result == null || !dlgResult.Result.Value)
                  TryClose();

                await TryLogin();
                return;
              }
            case ExceptionBase exception:
              {
                await DialogService.ShowMessageAsync(_messagesLocalizer.Localize(() => Messages.LoginError), exception.Message,
                  MessageDialogType.Ok);
                await TryLogin();
                return;
              }
          }
        }

        await DialogService.ShowMessageAsync(_messagesLocalizer.Localize(() => Messages.LoginError), _messagesLocalizer.Localize(() => Messages.InvalidCredentials),
          MessageDialogType.Ok);
        await TryLogin();
        return;
      }

      _authorizationService.CreateUserSession(result.Response);
      TryShowDashboardPreview(ActiveItem);
    }

    private async Task<LoginDialogData> ShowLoginPrompt()
    {
      return await DialogService.ShowLoginAsync();
    }

    private void OnWeighingValueUpdated(WeighingValueUpdatedNotification notification)
    {
      if (notification.DeviceId == 1)
      {
        _weight1NotificationHandler.Handle(notification);
        if (Device1Info == null)
          return;

        Device1Info.DeviceStatus = notification.Status;
        Device1Info.MeasuredValue
          = new UnitOfMeasurement(notification.Value, _unitTypeService);
      }

      if (notification.DeviceId == 2)
      {
        _weight2NotificationHandler.Handle(notification);
        if (Device2Info == null)
          return;

        Device2Info.DeviceStatus = notification.Status;
        Device2Info.MeasuredValue
          = new UnitOfMeasurement(notification.Value, _unitTypeService);
      }
    }

    private void TryShowDashboardPreview(ViewModelBase currentViewModel)
    {
      ShouldDisplayDashboardPreview = !(currentViewModel is DashboardViewModel);
    }

    private async void PerformTaring(object obj)
    {
      var devices = GetWeighingDevices();
      await DialogService.ShowModalDialogAsync<TareDialogViewModel>(vm =>
      {
        vm.DialogNameKey = "TaringDialogTitle";
        vm.SetWeighingDevices(devices);
      }, async vm =>
      {
        if (!vm.TareUom.OriginalValue.HasValue)
          return false;

        var selectedVehicle = vm.Vehicle;
        var result = await _vehicleService
          .UpdateVehicleTareAsync(selectedVehicle.Id, vm.TareUom.OriginalValue.Value);
        return result.Succeeded;
      });
    }

    private async void PerformSecondDoubleWeighing(object obj)
    {
      long? measurementId = null;
      var devices = GetWeighingDevices();
      await DialogService.ShowModalDialogAsync<DoubleWeighingSecondModalDialogViewModel>(vm =>
      {
        vm.DialogNameKey = "DoubleSecondWeighingDialogTitle";
        vm.SetWeighingDevices(devices);
      }, async vm =>
      {
        if (!vm.MeasurementNumber.HasValue)
          return false;

        if (!vm.IsDeviceIndicationValid && !await ConfirmInvalidIndication())
          return false;

        if (!vm.SelectedMeasurementClass.HasValue)
        {
          await DialogService.ShowMessageAsync(_messagesLocalizer.Localize(() => Messages.Error),
            _messagesLocalizer.Localize(() => Messages.MeasurementClassCannotBeEmpty), MessageDialogType.Ok);
          return false;
        }

        if (string.IsNullOrEmpty(vm.CurrentMeasurementValue))
          vm.CurrentMeasurementValue = 0.ToString();

        var result = await _measurementService.CompleteDoubleMeasurementAsync(vm.MeasurementId,
          vm.MeasurementNumber.Value, vm.SelectedMeasurementClass.Value, vm.CurrentMeasurementValueUom, vm.MeasurementDate, vm.DeclaredWeightValueUom, vm.Comment,
          vm.Driver, vm.Contractor, vm.Cargo, vm.Vehicle, vm.SelectedSemiTrailer, vm.DeviceId, vm.WeighingMode);

        if (result.Succeeded)
          measurementId = result.Response;

        return result.Succeeded;
      });

      if (measurementId.HasValue)
        await OnMeasurementCompleted(measurementId.Value);
    }

    private async void PerformQrCodeTriggeredSecondDoubleWeighing(MeasurementModel measurement)
    {
      long? measurementId = null;
      var devices = GetWeighingDevices();
      await DialogService.ShowModalDialogAsync<DoubleWeighingSecondModalDialogViewModel>(vm =>
      {
        vm.DialogNameKey = "DoubleSecondWeighingDialogTitle";
        vm.MeasurementId = measurement.Id;
        vm.MeasurementNumber = measurement.Number;
        vm.FirstMeasurementDate = measurement.Date1;
        if (measurement.Date1.Value.HasValue)
          vm.MeasurementDate = measurement.Date1.Value.Value;
        vm.PreviousMeasurementValue = measurement.Measurement1.DisplayValueUnitless;
        vm.DeclaredWeightValue = measurement.DeclaredWeight.DisplayValueUnitless;
        vm.Vehicle = measurement.Vehicle;
        vm.Contractor = measurement.Contractor;
        vm.Cargo = measurement.Cargo;
        vm.Driver = measurement.Driver;
        vm.SemiTrailer = measurement.SemiTrailer;
        vm.Comment = measurement.Comment;
        vm.SetWeighingDevices(devices);
        vm.ValidateOnLoad = true;
      }, async vm =>
      {
        if (!vm.MeasurementNumber.HasValue)
          return false;

        if (!vm.IsDeviceIndicationValid && !await ConfirmInvalidIndication())
          return false;

        if (!vm.SelectedMeasurementClass.HasValue)
        {
          await DialogService.ShowMessageAsync(_messagesLocalizer.Localize(() => Messages.Error),
            _messagesLocalizer.Localize(() => Messages.MeasurementClassCannotBeEmpty), MessageDialogType.Ok);
          return false;
        }

        if (string.IsNullOrEmpty(vm.CurrentMeasurementValue))
          vm.CurrentMeasurementValue = 0.ToString();

        var result = await _measurementService.CompleteDoubleMeasurementAsync(vm.MeasurementId,
          vm.MeasurementNumber.Value, vm.SelectedMeasurementClass.Value, vm.CurrentMeasurementValueUom, vm.MeasurementDate, vm.DeclaredWeightValueUom, vm.Comment,
          vm.Driver, vm.Contractor, vm.Cargo, vm.Vehicle, vm.SelectedSemiTrailer, vm.DeviceId, vm.WeighingMode);

        if (result.Succeeded)
          measurementId = result.Response;

        return result.Succeeded;
      });

      if (measurementId.HasValue)
        await OnMeasurementCompleted(measurementId.Value);
    }

    private IList<WeighingDeviceInfo> GetWeighingDevices()
    {
      var list = new List<WeighingDeviceInfo>();
      if (Device1Info != null)
        list.Add(Device1Info);
      if (Device2Info != null)
        list.Add(Device2Info);

      return list;
    }

    private async void PerformFirstDoubleWeighing(object obj)
    {
      var devices = GetWeighingDevices();
      await DialogService.ShowModalDialogAsync<DoubleWeighingFirstModalDialogViewModel>(vm =>
      {
        vm.DialogNameKey = "DoubleFirstWeighingDialogTitle";
        vm.SetWeighingDevices(devices);
        vm.ValidateOnLoad = true;
      }, async vm =>
      {
        if (!vm.IsDeviceIndicationValid && !await ConfirmInvalidIndication())
          return false;

        if (string.IsNullOrEmpty(vm.CurrentMeasurementValue))
          vm.CurrentMeasurementValue = 0.ToString();

        var result = await _measurementService.CreateDoubleMeasurementAsync(vm.CurrentMeasurementValueUom, vm.MeasurementDate, vm.DeclaredWeightValueUom, vm.WeighingMode, vm.Comment,
          vm.Driver, vm.Contractor, vm.Cargo, vm.Vehicle, vm.SelectedSemiTrailer, vm.DeviceId);

        return result.Succeeded;
      });
    }

    private async void PerformQrCodeTriggeredFirstDoubleWeighing(ReferenceModel cargo, ReferenceModel contractor, string qrCode, decimal declaredWeight)
    {
      var devices = GetWeighingDevices();
      await DialogService.ShowModalDialogAsync<DoubleWeighingFirstModalDialogViewModel>(vm =>
      {
        vm.DialogNameKey = "DoubleFirstWeighingDialogTitle";
        vm.Cargo = cargo;
        vm.Contractor = contractor;
        vm.DeclaredWeightValue = new UnitOfMeasurement(declaredWeight, _unitTypeService).DisplayValueUnitless;
        vm.SetWeighingDevices(devices);
        vm.ValidateOnLoad = true;
      }, async vm =>
      {
        if (!vm.IsDeviceIndicationValid && !await ConfirmInvalidIndication())
          return false;

        if (string.IsNullOrEmpty(vm.CurrentMeasurementValue))
          vm.CurrentMeasurementValue = 0.ToString();

        var result = await _measurementService.CreateDoubleMeasurementWithQRCodeAsync(vm.CurrentMeasurementValueUom, vm.MeasurementDate, vm.DeclaredWeightValueUom, vm.WeighingMode, vm.Comment,
          vm.Driver, vm.Contractor, vm.Cargo, vm.Vehicle, vm.SelectedSemiTrailer, vm.DeviceId, qrCode);

        return result.Succeeded;
      });
    }

    private async void PerformSingleWeighing(object obj)
    {
      long? measurementId = null;
      var devices = GetWeighingDevices();
      await DialogService.ShowModalDialogAsync<SingleWeighingModalDialogViewModel>(
        vm =>
        {
          vm.DialogNameKey = "SingleWeighingDialogTitle";
          vm.SetWeighingDevices(devices);
          vm.ValidateOnLoad = true;
        },
        async vm =>
        {
          if (!vm.IsDeviceIndicationValid && !await ConfirmInvalidIndication())
            return false;

          if (!vm.SelectedMeasurementClass.HasValue)
          {
            await DialogService.ShowMessageAsync(_messagesLocalizer.Localize(() => Messages.Error),
              _messagesLocalizer.Localize(() => Messages.MeasurementClassCannotBeEmpty), MessageDialogType.Ok);
            return false;
          }

          if (string.IsNullOrEmpty(vm.Tare))
            vm.Tare = 0.ToString();
          if (string.IsNullOrEmpty(vm.CurrentMeasurementValue))
            vm.CurrentMeasurementValue = 0.ToString();

          var result = await _measurementService.CreateSingleMeasurementAsync(
            vm.SelectedMeasurementClass.Value, vm.CurrentMeasurementValueUom, vm.MeasurementDate, vm.DeclaredWeightValueUom, vm.TareUom, vm.WeighingMode, vm.Comment,
            vm.Driver, vm.Contractor, vm.Cargo, vm.Vehicle, vm.SelectedSemiTrailer, vm.DeviceId);

          if (result.Succeeded)
            measurementId = result.Response;

          return result.Succeeded;
        });

      if (measurementId.HasValue)
        await OnMeasurementCompleted(measurementId.Value);
    }

    private async Task<bool> ConfirmInvalidIndication()
    {
      var dialogResult =
        await DialogService.ShowMessageAsync(_messagesLocalizer.Localize(() => Messages.MeasurementNotStable),
          _messagesLocalizer.Localize(() => Messages.ContinueQuestion),
          MessageDialogType.YesNo);
      if (dialogResult == MessageDialogResult.Negative)
        return false;

      return true;
    }

    private async Task OnMeasurementCompleted(long measurementId)
    {
      if (await ShouldPrintWeighingReceipt())
      {
        var result = await _measurementService.GetMeasurementByIdAsync(measurementId);
        if (result == null || !result.Succeeded)
          return;

        var photos = new List<MeasurementPhotoDto>();
        var downloadPhotos = _userPreferencesService.Configuration.IncludeImagesOnPrintouts;
        if (downloadPhotos == YesNo.Yes)
        {
          var photoResult = await _measurementService.GetMeasurementPhotosById(measurementId);
          if (photoResult.Succeeded)
            photos.AddRange(photoResult.Response);
        }

        var companyData = await _measurementService.GetCompanyInfoAsync();

        var measurement = result.Response;
                await _weighingReportService.PrintWeighingReceiptAsync(measurement, photos, companyData?.Response, _userPreferencesService.Configuration.DoubleCopy == YesNo.Yes);
            }
    }

    private async Task<bool> ShouldPrintWeighingReceipt()
    {
      var printOption = _userPreferencesService.Configuration.PrintWeighingReceiptAfterMeasurement;
      if (printOption == YesNoAsk.Yes)
        return true;

      if (printOption == YesNoAsk.No)
        return false;

      if (printOption != YesNoAsk.Ask)
        return false;

      var result = await DialogService.ShowMessageAsync(_messagesLocalizer.Localize(() => Messages.WeighingReceipt),
        _messagesLocalizer.Localize(() => Messages.PrintWeighingReceiptQuestion),
        MessageDialogType.YesNo);
      if (result == MessageDialogResult.Affirmative)
        return true;

      return false;
    }

    private async void OnBarcodeScannerMessageReceived(BarcodeScannerMessage message)
    {
      if (DialogService.IsModalOpened)
        return;

      await Execute.OnUIThreadAsync(async () =>
      {
        if (message == null || !message.IsValid)
        {
          await DialogService.ShowMessageAsync(_messagesLocalizer.Localize(() => Messages.Error),
            _messagesLocalizer.Localize(() => Messages.QrCodeInvalid), MessageDialogType.Ok);
          return;
        }

        await PerformQrCodeWeighing(message);
      });
    }

    private async Task PerformQrCodeWeighing(BarcodeScannerMessage message)
    {
      var cargo = await GetOrAddCargoIfNotExists(message.Cargo);
      if (cargo == null)
        _logger.Debug("There was some error while handling cargo received in QRCode.");

      var contractor = await GetOrAddContractorIfNotExists(message.Contractor);
      if (contractor == null)
        _logger.Debug("There was some error while handling contractor received in QRCode.");

      var measurementResult = await _measurementService.GetMeasurementByQrCodeAsync(message.ReceiptNumber);
      if (!measurementResult.Succeeded)
      {
        await DialogService.ShowMessageAsync(_messagesLocalizer.Localize(() => Messages.Error),
          _messagesLocalizer.Localize(() => Messages.QrCodeProcessingError), MessageDialogType.Ok);
        return;
      }

      if (measurementResult.Response == null)
        PerformQrCodeTriggeredFirstDoubleWeighing(cargo, contractor, message.ReceiptNumber, message.Net);
      else
      {
        var measurement = measurementResult.Response;
        if (!_measurementService.IsMeasurementCompleted(measurement))
          PerformQrCodeTriggeredSecondDoubleWeighing(measurement);
        else
          await DialogService.ShowMessageAsync(_messagesLocalizer.Localize(() => Messages.MeasurementComplete),
            _messagesLocalizer.Localize(() => Messages.QrCodeMeasurementCompleteText), MessageDialogType.Ok);
      }
    }

    private async Task<ReferenceModel> GetOrAddCargoIfNotExists(string name)
    {
      if (string.IsNullOrEmpty(name))
        return null;

      var result = await _cargoService.GetOrAddCargoAsync(name);
      return result.Response == null ? null : new ReferenceModel()
      {
        Id = result.Response.Id,
        DisplayValue = result.Response.Name
      };
    }

    private async Task<ReferenceModel> GetOrAddContractorIfNotExists(string name)
    {
      if (string.IsNullOrEmpty(name))
        return null;

      var result = await _contractorService.GetOrAddContractorAsync(name);
      return result.Response == null ? null : new ReferenceModel()
      {
        Id = result.Response.Id,
        DisplayValue = result.Response.Name
      };
    }

    private void OnDbUpdated(DbUpdatedNotification notification)
    {
      HandleDataUpdateNotification(notification);
    }
  }
}
