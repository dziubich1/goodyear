﻿using Kalisto.Backend.Operations.Shell.Queries;
using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Configuration.Devices;
using Kalisto.Hmi.Core.Communication;
using System;
using System.Threading.Tasks;
using Kalisto.Hmi.Dialogs.Common;
using System.Linq;

namespace Kalisto.Hmi.Dialogs.Shell
{
  public class ServerConfigurationLoader : IServerDeviceConfiguration
  {
    private readonly IMessageBuilder _messageBuilder;

    private readonly IFrontendMessageBridge _messageBridge;

    private readonly ClientInfo _clientInfo;

    private ServerDeviceConfiguration _configuration;

    public ServerConfigurationLoader(IMessageBuilder messageBuilder, IFrontendMessageBridge messageBridge, ClientInfo clientInfo)
    {
      _messageBuilder = messageBuilder;
      _messageBridge = messageBridge;
      _clientInfo = clientInfo;
    }

    public async Task<ServerDeviceConfiguration> GetConfigurationAsync()
    {
      if (_configuration != null)
        return _configuration;

      var message = _messageBuilder.CreateNew<GetDeviceConfigurationQuery>()
        .BasedOn(_clientInfo)
        .Build();

      var result = await _messageBridge.SendMessageWithResultAsync<ServerDeviceConfiguration>(message);
      if (result == null || !result.Succeeded)
        throw new InvalidOperationException();

      _configuration = result.Response;

      return _configuration;
    }
  }
}
