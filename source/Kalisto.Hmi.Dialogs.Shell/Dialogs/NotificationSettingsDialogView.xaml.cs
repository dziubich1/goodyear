﻿using MahApps.Metro.Controls;

namespace Kalisto.Hmi.Dialogs.Shell.Dialogs
{
  /// <summary>
  /// Interaction logic for NotificationSettingsDialogView.xaml
  /// </summary>
  public partial class NotificationSettingsDialogView : MetroWindow
  {
    public NotificationSettingsDialogView()
    {
      InitializeComponent();
    }
  }
}
