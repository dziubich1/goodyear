﻿using MahApps.Metro.Controls;

namespace Kalisto.Hmi.Dialogs.Shell.Dialogs
{
  /// <summary>
  /// Interaction logic for LicenseKeyWithPreKeyDialogView.xaml
  /// </summary>
  public partial class LicenseKeyWithPreKeyDialogView : MetroWindow
  {
    public LicenseKeyWithPreKeyDialogView()
    {
      InitializeComponent();
    }
  }
}
