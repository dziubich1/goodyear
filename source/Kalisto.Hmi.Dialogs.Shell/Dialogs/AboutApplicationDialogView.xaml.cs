﻿using MahApps.Metro.Controls;

namespace Kalisto.Hmi.Dialogs.Shell.Dialogs
{
  /// <summary>
  /// Interaction logic for AboutApplicationDialogView.xaml
  /// </summary>
  public partial class AboutApplicationDialogView : MetroWindow
  {
    public AboutApplicationDialogView()
    {
      InitializeComponent();
    }
  }
}
