﻿using MahApps.Metro.Controls;

namespace Kalisto.Hmi.Dialogs.Shell.Dialogs
{
  /// <summary>
  /// Interaction logic for WeighingListDialogView.xaml
  /// </summary>
  public partial class WeighingsListDialogView : MetroWindow
  {
    public WeighingsListDialogView()
    {
      InitializeComponent();
    }
  }
}
