﻿using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Hmi.Core;
using Kalisto.Hmi.Core.Communication;
using Kalisto.Hmi.Core.Services;
using Kalisto.Hmi.Dialogs.Common.Configuration;
using PropertyChanged;
using System;
using System.IO;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Kalisto.Hmi.Dialogs.Shell.Dialogs
{
  [AddINotifyPropertyChangedInterface]
  public class AboutApplicationDialogViewModel : ModalViewModelBase<AboutApplicationDialogViewModel>
  {
    private IFilesService _fileService;

    public sealed override string DialogNameKey { get; set; }

    public string ApplicationName => "Kalisto Waga Pro 2.0";
    public string ApplicationVersion => $"v{AssemblyName.GetAssemblyName(Assembly.GetExecutingAssembly().Location).Version.ToString()}";
    public string ThirdPartyLicensesText => GetLicencesText();

    public AboutApplicationDialogViewModel(IDialogService dialogService, IAsyncProgressService asyncProgressService,
      IMessageBuilder messageBuilder, IFrontendMessageBridge messageBridge, ClientInfo clientInfo, IFilesService fileService)
      : base(dialogService, asyncProgressService, messageBuilder, messageBridge, clientInfo)
    {
      _fileService = fileService;
      DialogNameKey = "AboutApplicationDialogTitle";
    }

    protected override void OnViewLoaded(object view)
    {
      base.OnViewLoaded(view);
    }

    protected override void DefineValidationRules()
    {
    }

    private string GetLicencesText()
    {
      string text = _fileService.ReadTextFromFile(@"Licence-desktop.txt");

      return text;
    }
  }
}
