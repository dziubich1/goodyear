﻿using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Hmi.Core.Communication;
using Kalisto.Hmi.Core.Services;
using Kalisto.Hmi.Dialogs.Cargoes.Services;
using Kalisto.Hmi.Dialogs.Common;
using Kalisto.Hmi.Dialogs.Contractors.Services;
using Kalisto.Hmi.Dialogs.Dashboard.Services;
using Kalisto.Hmi.Dialogs.Drivers.Services;
using Kalisto.Hmi.Dialogs.Measurements.Services;
using Kalisto.Hmi.Dialogs.Vehicles.Models;
using Kalisto.Hmi.Dialogs.Vehicles.Services;
using PropertyChanged;
using System;
using System.Globalization;
using Kalisto.Frontend.Notifications.Enums;
using Kalisto.Hmi.Core.Controls;
using Kalisto.Hmi.Dialogs.Common.Configuration;

namespace Kalisto.Hmi.Dialogs.Shell.Dialogs
{
  [AddINotifyPropertyChangedInterface]
  public class SingleWeighingModalDialogViewModel : MeasurementModalViewModelBase<SingleWeighingModalDialogViewModel>
  {
    [AlsoNotifyFor(nameof(NetValue))]
    public string Tare { get; set; }

    public UnitOfMeasurement TareUom =>
      decimal.TryParse(Tare, out decimal value) ? UnitTypeService.ConvertBackValue(value) : null;

    public SingleWeighingModalDialogViewModel(IDialogService dialogService, IDriverService driverService, IContractorService contractorService,
      IMeasurementService measurementService, IVehicleService vehicleService, ICargoService cargoService, IUnitTypeService unitTypeService,
      IDashboardService dashboardService, IAsyncProgressService asyncProgressService, IMessageBuilder messageBuilder, IFrontendMessageBridge messageBridge,
      ClientInfo clientInfo, IUserPreferencesService userPreferencesService)
      : base(dialogService, driverService, contractorService, measurementService, vehicleService, cargoService, unitTypeService,
          dashboardService, asyncProgressService, messageBuilder, messageBridge, clientInfo, userPreferencesService)
    {
    }


    protected override async void OnViewLoaded(object view)
    {
      base.OnViewLoaded(view);

      var result = await MeasurementService.GetNextMeasurementNumberAsync();
      if (!result.Succeeded)
        return;

      MeasurementNumber = result.Response;
      SelectedMeasurementClass = null;
    }

    protected override string GetNetValue()
    {
      if (!decimal.TryParse(Tare, out decimal val1))
        return string.Empty;

      if (!decimal.TryParse(CurrentMeasurementValue, out decimal val2))
        return string.Empty;

      return Math.Abs(val2 - val1).ToString(CultureInfo.InvariantCulture);
    }

    protected override void DefineValidationRules()
    {
      InvalidateProperty(() => CurrentMeasurementValue).When(() => !IsDecimalOrNull(CurrentMeasurementValue))
        .WithMessageKey("InvalidNumber").ButIgnoreOnLoad();

      InvalidateProperty(() => CurrentMeasurementValue).When(() => !IsGreaterOrEqualThan(CurrentMeasurementValue, 0) && !string.IsNullOrEmpty(CurrentMeasurementValue))
        .WithMessageKey("MustBeGreaterOrEqualThan", 0).ButIgnoreOnLoad();

      InvalidateProperty(() => Tare).When(() => !IsDecimalOrNull(Tare))
        .WithMessageKey("InvalidNumber").ButIgnoreOnLoad();

      InvalidateProperty(() => Tare).When(() => !IsGreaterOrEqualThan(Tare, 0) && !string.IsNullOrEmpty(Tare))
        .WithMessageKey("MustBeGreaterOrEqualThan", 0).ButIgnoreOnLoad();

      InvalidateProperty(() => DeclaredWeightValue).When(() => !IsDecimalOrNull(DeclaredWeightValue))
        .WithMessageKey("InvalidNumber").ButIgnoreOnLoad();

      InvalidateProperty(() => DeclaredWeightValue).When(() => !IsGreaterOrEqualThan(DeclaredWeightValue, 0) && !string.IsNullOrEmpty(DeclaredWeightValue))
        .WithMessageKey("MustBeGreaterOrEqualThan", 0).ButIgnoreOnLoad();
    }

    public override void OnVehicleChanged()
    {
      base.OnVehicleChanged();

      if (Vehicle == null)
      {
        Tare = string.Empty;
        return;
      }

      Tare = (Vehicle.RawData as VehicleModel)?.Tare?
        .DisplayValueUnitless;
    }
  }
}
