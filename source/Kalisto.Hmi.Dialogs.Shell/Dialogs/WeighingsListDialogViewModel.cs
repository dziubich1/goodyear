﻿using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Hmi.Core;
using Kalisto.Hmi.Core.Commands;
using Kalisto.Hmi.Core.Communication;
using Kalisto.Hmi.Core.Services;
using Kalisto.Hmi.Dialogs.Common;
using Kalisto.Hmi.Dialogs.Common.Models;
using Kalisto.Hmi.Dialogs.Measurements.Services;
using Kalisto.Localization;
using PropertyChanged;
using System;
using System.Linq;
using System.Windows.Input;

namespace Kalisto.Hmi.Dialogs.Shell.Dialogs
{
  [AddINotifyPropertyChangedInterface]
  public class WeighingsListDialogViewModel : ModalViewModelBase<WeighingsListDialogViewModel>
  {
    private readonly IWeighingReportService _weighingReportService;

    public sealed override string DialogNameKey { get; set; }

    public ObservableCollectionEx<WeighingModel> Weighings { get; set; }

    public LocalizedDate FromDate { get; set; }

    public LocalizedDate ToDate { get; set; }

    public ICommand PrintCommand { get; set; }

    public ICommand SaveToFileCommand { get; set; }

    public UnitOfMeasurement NetSum { get; set; }

    public Action<IFrontendMessageBridge> BackendOperationAction { get; set; }

    public WeighingsListDialogViewModel(IDialogService dialogService, IAsyncProgressService asyncProgressService,
      IMessageBuilder messageBuilder, IFrontendMessageBridge messageBridge, IWeighingReportService weighingReportService, ClientInfo clientInfo)
      : base(dialogService, asyncProgressService, messageBuilder, messageBridge, clientInfo)
    {
      _weighingReportService = weighingReportService;
      Weighings = new ObservableCollectionEx<WeighingModel>();

      PrintCommand = new DelegateCommand(Print);
      SaveToFileCommand = new DelegateCommand(SaveToFile);
    }

    protected override void OnViewLoaded(object view)
    {
      base.OnViewLoaded(view);

      BackendOperationAction?.Invoke(MessageBridge);
    }

    protected override void DefineValidationRules()
    {
    }

    private void SaveToFile(object obj)
    {
    }

    private async void Print(object obj)
    {
      await _weighingReportService.PrintWeighingListAsync(Weighings.ToList(), NetSum, FromDate, ToDate);
    }
  }
}
