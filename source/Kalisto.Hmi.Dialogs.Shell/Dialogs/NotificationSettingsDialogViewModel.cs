﻿using Kalisto.Backend.Operations.Shell.Queries;
using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Hmi.Core;
using Kalisto.Hmi.Core.Communication;
using Kalisto.Hmi.Core.Services;
using Kalisto.Localization;
using Kalisto.Mailing.Notifications.Configuration;
using PropertyChanged;
using System.Linq;
using Kalisto.Localization.Messages;

namespace Kalisto.Hmi.Dialogs.Shell.Dialogs
{
  [AddINotifyPropertyChangedInterface]
  public class NotificationSettingsDialogViewModel : ModalViewModelBase<NotificationSettingsDialogViewModel>
  {
    private readonly ILocalizer<Messages> _localizer;

    public sealed override string DialogNameKey { get; set; }

    public bool NotificationsEnabled { get; set; }

    public ObservableCollectionEx<LocalizedEnum> NotificationTypes { get; set; }

    public LocalizedEnum SelectedNotificationType { get; set; }

    public string From { get; set; }

    public string To { get; set; }

    public NotificationSettingsDialogViewModel(IDialogService dialogService, IAsyncProgressService asyncProgressService,
      IMessageBuilder messageBuilder, IFrontendMessageBridge messageBridge, ClientInfo clientInfo)
      : base(dialogService, asyncProgressService, messageBuilder, messageBridge, clientInfo)
    {
      _localizer = new LocalizationManager<Messages>();
      DialogNameKey = "NotificationSettingsDialogTitle";

      NotificationTypes = new ObservableCollectionEx<LocalizedEnum>
      {
        new LocalizedEnum { Enum = NotificationType.Daily },
        new LocalizedEnum { Enum = NotificationType.Weekly },
        new LocalizedEnum { Enum = NotificationType.Monthly }
      };
    }

    protected override async void OnViewLoaded(object view)
    {
      base.OnViewLoaded(view);

      var message = MessageBuilder.CreateNew<GetNotificationSettingsQuery>()
        .BasedOn(ClientInfo)
        .Build();

      var result = await MessageBridge.SendMessageWithResultAsync<NotificationSettings>(message);
      if (!result.Succeeded)
      {
        await DialogService.ShowMessageAsync(_localizer.Localize(() => Messages.Error), _localizer.Localize(() => Messages.DataFetchingError), MessageDialogType.Ok);
        return;
      }

      if (result.Response == null)
        return;

      NotificationsEnabled = result.Response.Enabled;
      SelectedNotificationType = NotificationTypes.First(c => (NotificationType)c.Enum == result.Response.Type);
      From = result.Response.From;
      To = result.Response.To;
    }

    protected override void DefineValidationRules()
    {
      InvalidateProperty(() => From).When(() => string.IsNullOrEmpty(From))
        .WithMessageKey("Required").ButIgnoreOnLoad();

      InvalidateProperty(() => To).When(() => string.IsNullOrEmpty(To))
        .WithMessageKey("Required").ButIgnoreOnLoad();

      InvalidateProperty(() => From).When(() => !IsEmailValid(From))
        .WithMessageKey("EmailInvalid").ButIgnoreOnLoad();

      InvalidateProperty(() => To).When(() => !IsEmailValid(To))
        .WithMessageKey("EmailInvalid").ButIgnoreOnLoad();
    }
  }
}
