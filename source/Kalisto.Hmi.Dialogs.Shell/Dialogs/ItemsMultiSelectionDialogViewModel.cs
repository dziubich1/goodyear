﻿using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Hmi.Core;
using Kalisto.Hmi.Core.Communication;
using Kalisto.Hmi.Core.Services;
using PropertyChanged;
using System;
using System.Windows.Input;
using Kalisto.Hmi.Core.Commands;
using Kalisto.Hmi.Dialogs.Common.Models;

namespace Kalisto.Hmi.Dialogs.Shell.Dialogs
{
  [AddINotifyPropertyChangedInterface]
  public class ItemsMultiSelectionDialogViewModel 
    : ModalViewModelBase<ItemsMultiSelectionDialogViewModel>
  {
    public override string DialogNameKey { get; set; }

    public ObservableCollectionEx<ReferenceModel> Items { get; set; }

    public ObservableCollectionEx<ReferenceModel> SelectedItems { get; set; }

    public ICommand SelectAllCommand { get; set; }

    public ICommand DeselectAllCommand { get; set; }

    public Action<IFrontendMessageBridge> BackendOperationAction { get; set; }

    public ItemsMultiSelectionDialogViewModel(IDialogService dialogService, IAsyncProgressService asyncProgressService,
      IMessageBuilder messageBuilder, IFrontendMessageBridge messageBridge, ClientInfo clientInfo)
      : base(dialogService, asyncProgressService, messageBuilder, messageBridge, clientInfo)
    {
      Items = new ObservableCollectionEx<ReferenceModel>();
      SelectedItems = new ObservableCollectionEx<ReferenceModel>();
      SelectAllCommand =
        new DelegateCommand(c => { SelectedItems = new ObservableCollectionEx<ReferenceModel>(Items); });
      DeselectAllCommand = new DelegateCommand(c => { SelectedItems = new ObservableCollectionEx<ReferenceModel>(); });
    }

    protected override void DefineValidationRules()
    {
    }

    protected override void OnViewLoaded(object view)
    {
      base.OnViewLoaded(view);

      BackendOperationAction?.Invoke(MessageBridge);
    }
  }
}
