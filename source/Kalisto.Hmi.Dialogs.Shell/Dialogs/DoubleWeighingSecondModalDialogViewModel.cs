﻿using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Hmi.Core.Commands;
using Kalisto.Hmi.Core.Communication;
using Kalisto.Hmi.Core.Services;
using Kalisto.Hmi.Dialogs.Cargoes.Services;
using Kalisto.Hmi.Dialogs.Common;
using Kalisto.Hmi.Dialogs.Common.Configuration;
using Kalisto.Hmi.Dialogs.Contractors.Services;
using Kalisto.Hmi.Dialogs.Dashboard.Services;
using Kalisto.Hmi.Dialogs.Drivers.Services;
using Kalisto.Hmi.Dialogs.Measurements.Services;
using Kalisto.Hmi.Dialogs.Vehicles.Services;
using PropertyChanged;
using System.Windows.Input;

namespace Kalisto.Hmi.Dialogs.Shell.Dialogs
{
  [AddINotifyPropertyChangedInterface]
  public class DoubleWeighingSecondModalDialogViewModel : MeasurementModalViewModelBase<DoubleWeighingSecondModalDialogViewModel>
  {
    public ICommand BlockMeasurementClassSetterAutoModeCommand { get; set; }

    private bool _isMeasurementClassSetterAutoModeEnabled;

    public DoubleWeighingSecondModalDialogViewModel(IDialogService dialogService, IDriverService driverService, IContractorService contractorService,
      IMeasurementService measurementService, IVehicleService vehicleService, ICargoService cargoService, IUnitTypeService unitTypeService,
      IDashboardService dashboardService, IAsyncProgressService asyncProgressService, IMessageBuilder messageBuilder,
      IFrontendMessageBridge messageBridge, ClientInfo clientInfo, IUserPreferencesService userPreferencesService)
      : base(dialogService, driverService, contractorService, measurementService, vehicleService, cargoService, unitTypeService,
          dashboardService, asyncProgressService, messageBuilder, messageBridge, clientInfo, userPreferencesService)
    {
      _isMeasurementClassSetterAutoModeEnabled = true;
      BlockMeasurementClassSetterAutoModeCommand = new DelegateCommand(BlockMeasurementClassSetterAutoMode);
    }

    protected override void DefineValidationRules()
    {
      InvalidateProperty(() => CurrentMeasurementValue).When(() => !IsDecimalOrNull(CurrentMeasurementValue))
        .WithMessageKey("InvalidNumber").ButIgnoreOnLoad();

      InvalidateProperty(() => CurrentMeasurementValue).When(() => !IsGreaterOrEqualThan(CurrentMeasurementValue, 0) && !string.IsNullOrEmpty(CurrentMeasurementValue))
        .WithMessageKey("MustBeGreaterOrEqualThan", 0).ButIgnoreOnLoad();

      InvalidateProperty(() => DeclaredWeightValue).When(() => !IsDecimalOrNull(DeclaredWeightValue))
        .WithMessageKey("InvalidNumber").ButIgnoreOnLoad();

      InvalidateProperty(() => DeclaredWeightValue).When(() => !IsGreaterOrEqualThan(DeclaredWeightValue, 0) && !string.IsNullOrEmpty(DeclaredWeightValue))
        .WithMessageKey("MustBeGreaterOrEqualThan", 0).ButIgnoreOnLoad();
    }

    public override void OnPreviousMeasurementValueChanged()
    {
      SetMeasurementClass();
    }

    public override void OnCurrentMeasurementValueChanged()
    {
      SetMeasurementClass();
    }

    private void SetMeasurementClass()
    {
      if (!_isMeasurementClassSetterAutoModeEnabled)
        return;

      var diff = CalculateDifferenceBetweenStringValues(PreviousMeasurementValue, CurrentMeasurementValue);

      if (!diff.HasValue)
        return;

      if (diff.Value > 0)
        SelectedMeasurementClass = Backend.DataAccess.Model.MeasurementClass.Outcome;
      else if (diff.Value < 0)
        SelectedMeasurementClass = Backend.DataAccess.Model.MeasurementClass.Income;
    }
    
    private void BlockMeasurementClassSetterAutoMode(object parameter)
    {
      _isMeasurementClassSetterAutoModeEnabled = false;
    }
  }
}
