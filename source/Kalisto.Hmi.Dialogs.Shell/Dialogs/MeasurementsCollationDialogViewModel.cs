﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.DataAccess.Model.Enum;
using Kalisto.Backend.Operations.Cargoes.Queries;
using Kalisto.Backend.Operations.Contractors.Queries;
using Kalisto.Backend.Operations.Drivers.Queries;
using Kalisto.Backend.Operations.Shell.Queries;
using Kalisto.Backend.Operations.Vehicles.Queries;
using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Hmi.Core;
using Kalisto.Hmi.Core.Commands;
using Kalisto.Hmi.Core.Communication;
using Kalisto.Hmi.Core.Services;
using Kalisto.Hmi.Dialogs.Common;
using Kalisto.Hmi.Dialogs.Common.Models;
using Kalisto.Localization;
using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Kalisto.Hmi.Dialogs.Shell.Dialogs
{
  [AddINotifyPropertyChangedInterface]
  public class MeasurementsCollationDialogViewModel : ModalViewModelBase<MeasurementsCollationDialogViewModel>
  {
    private readonly IUnitTypeService _unitTypeService;

    public sealed override string DialogNameKey { get; set; }

    public DateTime FromDate { get; set; }

    public DateTime ToDate { get; set; }

    public ObservableCollectionEx<LocalizedEnum> MeasurementClasses { get; set; }

    public LocalizedEnum MeasurementClass { get; set; }

    public ObservableCollectionEx<LocalizedEnum> MeasurementTypes { get; set; }

    public LocalizedEnum MeasurementType { get; set; }

    public ObservableCollectionEx<LocalizedEnum> SortByOptions { get; set; }

    public LocalizedEnum SortByOption { get; set; }

    public ICommand SelectCargoesCommand { get; set; }

    public ICommand SelectContractorsCommand { get; set; }

    public ICommand SelectDriversCommand { get; set; }

    public ICommand SelectVehiclesCommand { get; set; }

    private ObservableCollectionEx<ReferenceModel> _selectedCargoes;
    private ObservableCollectionEx<ReferenceModel> _selectedContractors;
    private ObservableCollectionEx<ReferenceModel> _selectedDrivers;
    private ObservableCollectionEx<ReferenceModel> _selectedVehicles;

    public MeasurementsCollationDialogViewModel(IDialogService dialogService, IAsyncProgressService asyncProgressService,
      IMessageBuilder messageBuilder, IFrontendMessageBridge messageBridge, IUnitTypeService unitTypeService, ClientInfo clientInfo)
      : base(dialogService, asyncProgressService, messageBuilder, messageBridge, clientInfo)
    {
      _unitTypeService = unitTypeService;
      DialogNameKey = "MeasurementsCollationDialogTitle";
      FromDate = DateTime.Now.AddDays(-1);
      ToDate = DateTime.Now;
      MeasurementTypes = new ObservableCollectionEx<LocalizedEnum>();
      MeasurementClasses = new ObservableCollectionEx<LocalizedEnum>();
      SortByOptions = new ObservableCollectionEx<LocalizedEnum>();

      SubmitCommand = new DelegateCommand(OpenWeighingListDialog);
      SelectCargoesCommand = new DelegateCommand(OpenCargoesMultiSelectionDialog);
      SelectContractorsCommand = new DelegateCommand(OpenContractorsMultiSelectionDialog);
      SelectDriversCommand = new DelegateCommand(OpenDriversMultiSelectionDialog);
      SelectVehiclesCommand = new DelegateCommand(OpenVehiclesMultiSelectionDialog);
    }

    protected override void OnViewLoaded(object view)
    {
      base.OnViewLoaded(view);

      LoadMeasurementTypes();
      LoadMeasurementClasses();
      LoadSortByOptions();
    }

    protected override void DefineValidationRules()
    {
    }

    private void LoadMeasurementTypes()
    {
      var result = Enum.GetValues(typeof(MeasurementType)).Cast<MeasurementType>()
        .Select(c => new LocalizedEnum
        {
          Enum = c
        });

      ClearMeasurementTypes();
      MeasurementTypes.AddRange(result);
      MeasurementType = MeasurementTypes.FirstOrDefault(c => c.Enum == null);
    }

    private void ClearMeasurementTypes()
    {
      MeasurementTypes.Clear();
      MeasurementTypes.Add(new LocalizedEnum()
      {
        Enum = null
      });
    }

    private void LoadMeasurementClasses()
    {
      var result = Enum.GetValues(typeof(MeasurementClass)).Cast<MeasurementClass>().Select(c => new LocalizedEnum
      {
        Enum = c
      });

      ClearMeasurementClasses();
      MeasurementClasses.AddRange(result);
      MeasurementClass = MeasurementClasses.FirstOrDefault(c => c.Enum == null);
    }

    private void ClearMeasurementClasses()
    {
      MeasurementClasses.Clear();
      MeasurementClasses.Add(
        new LocalizedEnum
        {
          Enum = null
        });
    }

    private void LoadSortByOptions()
    {
      var result = Enum.GetValues(typeof(SortBy)).Cast<SortBy>()
        .Select(c => new LocalizedEnum
        {
          Enum = c
        });

      SortByOptions.Clear();
      SortByOptions.AddRange(result);
      SortByOption = SortByOptions.FirstOrDefault();
    }

    protected async void OpenWeighingListDialog(object parameter)
    {
      await DialogService.ShowModalDialogAsync<WeighingsListDialogViewModel>(vm =>
      {
        vm.FromDate = new LocalizedDate(FromDate);
        vm.ToDate = new LocalizedDate(ToDate);
        vm.DialogNameKey = "WeighingsListDialogTitle";
        vm.ValidateOnLoad = true;
        vm.BackendOperationAction = async messageBridge =>
        {
          var message = MessageBuilder.CreateNew<GetFilteredMeasurementsQuery>()
            .BasedOn(ClientInfo)
            .WithProperty(m => m.FromUtc, FromDate.ToUniversalTime())
            .WithProperty(m => m.ToUtc, ToDate.ToUniversalTime())
            .WithProperty(m => m.MeasurementClass, (MeasurementClass?)MeasurementClass?.Enum)
            .WithProperty(m => m.MeasurementType, (MeasurementType?)MeasurementType?.Enum)
            .WithProperty(m => m.SortByOption, (SortBy?)SortByOption?.Enum)
            .WithProperty(m => m.Cargoes, ExtractIdsFromReferenceModelsCollection(_selectedCargoes))
            .WithProperty(m => m.Contractors, ExtractIdsFromReferenceModelsCollection(_selectedContractors))
            .WithProperty(m => m.Vehicles, ExtractIdsFromReferenceModelsCollection(_selectedVehicles))
            .WithProperty(m => m.Drivers, ExtractIdsFromReferenceModelsCollection(_selectedDrivers))
            .Build();

          var result = await messageBridge.SendMessageWithResultAsync<List<Measurement>>(message);
          if (result == null || !result.Succeeded)
            return;

          vm.Weighings.AddRange(result.Response.Select((m, i) =>
          {
            var gross = m.Measurement1;
            if (m.Measurement2.HasValue && m.Measurement2 > m.Measurement1)
              gross = m.Measurement2.Value;

            var weighingDate = new LocalizedDate(m.Date1Utc.ToLocalTime());
            if (m.Date2Utc.HasValue && m.Type == Backend.DataAccess.Model.MeasurementType.Double)
              weighingDate = new LocalizedDate(m.Date2Utc.Value.ToLocalTime());

            return new WeighingModel
            {
              OrdinalId = i + 1,
              Net = new UnitOfMeasurement(m.NetWeight, _unitTypeService),
              Driver = m.Driver?.Identifier,
              Vehicle = m.Vehicle?.PlateNumber,
              Cargo = m.Cargo?.Name,
              Contractor = m.Contractor?.Name,
              WeighingDate = weighingDate,
              Gross = new UnitOfMeasurement(gross, _unitTypeService)
            };
          }));

          vm.FromDate = new LocalizedDate(FromDate);
          vm.ToDate = new LocalizedDate(ToDate);
          vm.NetSum = new UnitOfMeasurement(
            vm.Weighings.Where(c => c.Net.OriginalValue != null).Sum(c => c.Net.OriginalValue.Value), _unitTypeService);

        };
      }, vm =>
      {
        return Task.FromResult(true);
      });
    }

    public async void OpenCargoesMultiSelectionDialog(object parameter)
    {
      await DialogService.ShowModalDialogAsync<ItemsMultiSelectionDialogViewModel>(vm =>
      {
        vm.DialogNameKey = "SelectCargoesDialogTitle";
        vm.ValidateOnLoad = true;
        vm.BackendOperationAction = async messageBridge =>
        {
          var message = MessageBuilder.CreateNew<GetAllCargoesQuery>()
            .BasedOn(ClientInfo)
            .Build();

          var result = await messageBridge.SendMessageWithResultAsync<List<Cargo>>(message);
          if (!result.Succeeded)
            return;

          vm.Items.AddRange(result.Response.Select(c => new ReferenceModel
          {
            Id = c.Id,
            DisplayValue = c.Name
          }));

          if (_selectedCargoes != null)
            vm.SelectedItems =
              new ObservableCollectionEx<ReferenceModel>(vm.Items.Where(c =>
                _selectedCargoes.Select(d => d.Id).Contains(c.Id)));
        };
      }, vm =>
      {
        _selectedCargoes = vm.SelectedItems;
        return Task.FromResult(true);
      });
    }

    public async void OpenContractorsMultiSelectionDialog(object parameter)
    {
      await DialogService.ShowModalDialogAsync<ItemsMultiSelectionDialogViewModel>(vm =>
      {
        vm.DialogNameKey = "SelectContractorsDialogTitle";
        vm.ValidateOnLoad = true;
        vm.BackendOperationAction = async messageBridge =>
        {
          var message = MessageBuilder.CreateNew<GetAllContractorsQuery>()
            .BasedOn(ClientInfo)
            .Build();

          var result = await messageBridge.SendMessageWithResultAsync<List<Contractor>>(message);
          if (!result.Succeeded)
            return;

          vm.Items.AddRange(result.Response.Select(c => new ReferenceModel
          {
            Id = c.Id,
            DisplayValue = c.Name
          }));

          if (_selectedContractors != null)
            vm.SelectedItems =
              new ObservableCollectionEx<ReferenceModel>(vm.Items.Where(c =>
                _selectedContractors.Select(d => d.Id).Contains(c.Id)));
        };
      }, vm =>
      {
        _selectedContractors = vm.SelectedItems;
        return Task.FromResult(true);
      });
    }

    public async void OpenDriversMultiSelectionDialog(object parameter)
    {
      await DialogService.ShowModalDialogAsync<ItemsMultiSelectionDialogViewModel>(vm =>
      {
        vm.DialogNameKey = "SelectDriversDialogTitle";
        vm.ValidateOnLoad = true;
        vm.BackendOperationAction = async messageBridge =>
        {
          var message = MessageBuilder.CreateNew<GetAllDriversQuery>()
            .BasedOn(ClientInfo)
            .Build();

          var result = await messageBridge.SendMessageWithResultAsync<List<Driver>>(message);
          if (!result.Succeeded)
            return;

          vm.Items.AddRange(result.Response.Select(c => new ReferenceModel
          {
            Id = c.Id,
            DisplayValue = c.Identifier
          }));

          if (_selectedDrivers != null)
            vm.SelectedItems =
              new ObservableCollectionEx<ReferenceModel>(vm.Items.Where(c =>
                _selectedDrivers.Select(d => d.Id).Contains(c.Id)));
        };
      }, vm =>
      {
        _selectedDrivers = vm.SelectedItems;
        return Task.FromResult(true);
      });
    }

    public async void OpenVehiclesMultiSelectionDialog(object parameter)
    {
      await DialogService.ShowModalDialogAsync<ItemsMultiSelectionDialogViewModel>(vm =>
      {
        vm.DialogNameKey = "SelectVehiclesDialogTitle";
        vm.ValidateOnLoad = true;
        vm.BackendOperationAction = async messageBridge =>
        {
          var message = MessageBuilder.CreateNew<GetAllVehiclesQuery>()
            .BasedOn(ClientInfo)
            .Build();

          var result = await messageBridge.SendMessageWithResultAsync<List<Vehicle>>(message);
          if (!result.Succeeded)
            return;

          vm.Items.AddRange(result.Response.Select(c => new ReferenceModel
          {
            Id = c.Id,
            DisplayValue = c.PlateNumber
          }));

          if (_selectedVehicles != null)
            vm.SelectedItems =
              new ObservableCollectionEx<ReferenceModel>(vm.Items.Where(c =>
                _selectedVehicles.Select(d => d.Id).Contains(c.Id)));
        };
      }, vm =>
      {
        _selectedVehicles = vm.SelectedItems;
        return Task.FromResult(true);
      });
    }

    private IEnumerable<long> ExtractIdsFromReferenceModelsCollection(IEnumerable<ReferenceModel> list)
    {
      if (list == null)
        return null;

      var extracted = list.Select(m => m.Id).ToList();
      if (extracted.Any())
        return extracted;

      return null;
    }
  }
}
