﻿using MahApps.Metro.Controls;

namespace Kalisto.Hmi.Dialogs.Shell.Dialogs
{
  /// <summary>
  /// Interaction logic for LicenseKeyDialogView.xaml
  /// </summary>
  public partial class LicenseKeyDialogView : MetroWindow
  {
    public LicenseKeyDialogView()
    {
      InitializeComponent();
    }
  }
}
