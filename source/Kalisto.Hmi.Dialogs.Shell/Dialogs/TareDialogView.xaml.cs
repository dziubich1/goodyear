﻿using MahApps.Metro.Controls;

namespace Kalisto.Hmi.Dialogs.Shell.Dialogs
{
  /// <summary>
  /// Interaction logic for TareDialogView.xaml
  /// </summary>
  public partial class TareDialogView : MetroWindow
  {
    public TareDialogView()
    {
      InitializeComponent();
    }
  }
}
