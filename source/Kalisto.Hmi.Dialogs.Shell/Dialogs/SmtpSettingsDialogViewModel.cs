﻿using Kalisto.Backend.Operations.Shell.Queries;
using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Hmi.Core;
using Kalisto.Hmi.Core.Communication;
using Kalisto.Hmi.Core.Services;
using Kalisto.Mailing.Configuration;
using PropertyChanged;
using System;
using Kalisto.Localization;
using Kalisto.Localization.Messages;

namespace Kalisto.Hmi.Dialogs.Shell.Dialogs
{
  [AddINotifyPropertyChangedInterface]
  public class SmtpSettingsDialogViewModel : ModalViewModelBase<SmtpSettingsDialogViewModel>
  {
    private readonly ILocalizer<Messages> _localizer;

    public sealed override string DialogNameKey { get; set; }

    public string Host { get; set; }

    public string Port { get; set; }

    public string From { get; set; }

    public string Password { get; set; }

    public int PortValue => Convert.ToInt32(Port);

    public SmtpSettingsDialogViewModel(IDialogService dialogService, IAsyncProgressService asyncProgressService,
      IMessageBuilder messageBuilder, IFrontendMessageBridge messageBridge, ClientInfo clientInfo)
      : base(dialogService, asyncProgressService, messageBuilder, messageBridge, clientInfo)
    {
      _localizer = new LocalizationManager<Messages>();
      DialogNameKey = "SmtpSettingsDialogTitle";
    }

    protected override async void OnViewLoaded(object view)
    {
      base.OnViewLoaded(view);

      var message = MessageBuilder.CreateNew<GetSmtpSettingsQuery>()
        .BasedOn(ClientInfo)
        .Build();

      var result = await MessageBridge.SendMessageWithResultAsync<SmtpSettings>(message);
      if (!result.Succeeded)
      {
        await DialogService.ShowMessageAsync(_localizer.Localize(() => Messages.Error), _localizer.Localize(() => Messages.DataFetchingError), MessageDialogType.Ok);
        return;
      }

      if (result.Response == null)
        return;

      Host = result.Response.Host;
      Port = result.Response.Port.ToString();
      From = result.Response.From;
      Password = result.Response.Password;
    }

    protected override void DefineValidationRules()
    {
      InvalidateProperty(() => Host).When(() => string.IsNullOrEmpty(Host))
        .WithMessageKey("Required").ButIgnoreOnLoad();

      InvalidateProperty(() => Port).When(() => !IsNumber(Port))
        .WithMessageKey("InvalidNumber").ButIgnoreOnLoad();

      InvalidateProperty(() => Port).When(() => !IsPortValid(Port))
        .WithMessageKey("PortInvalid").ButIgnoreOnLoad();

      InvalidateProperty(() => From).When(() => !IsEmailValid(From))
        .WithMessageKey("EmailInvalid").ButIgnoreOnLoad();

      InvalidateProperty(() => Password).When(() => string.IsNullOrEmpty(Password))
        .WithMessageKey("Required").ButIgnoreOnLoad();
    }

    private bool IsPortValid(string value)
    {
      if (string.IsNullOrEmpty(value))
        return false;

      if (int.TryParse(value, out int port))
      {
        if (port > 0 && port <= 65535)
          return true;
      }

      return false;
    }
  }
}
