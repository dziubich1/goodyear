﻿using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Hmi.Core.Communication;
using Kalisto.Hmi.Core.Services;
using Kalisto.Hmi.Dialogs.Cargoes.Services;
using Kalisto.Hmi.Dialogs.Common;
using Kalisto.Hmi.Dialogs.Common.Configuration;
using Kalisto.Hmi.Dialogs.Contractors.Services;
using Kalisto.Hmi.Dialogs.Dashboard.Services;
using Kalisto.Hmi.Dialogs.Drivers.Services;
using Kalisto.Hmi.Dialogs.Measurements.Services;
using Kalisto.Hmi.Dialogs.Vehicles.Services;
using PropertyChanged;

namespace Kalisto.Hmi.Dialogs.Shell.Dialogs
{
  [AddINotifyPropertyChangedInterface]
  public class DoubleWeighingFirstModalDialogViewModel : MeasurementModalViewModelBase<DoubleWeighingFirstModalDialogViewModel>
  {
    public DoubleWeighingFirstModalDialogViewModel(IDialogService dialogService, IDriverService driverService, IContractorService contractorService,
      IMeasurementService measurementService, IVehicleService vehicleService, ICargoService cargoService, IUnitTypeService unitTypeService,
      IDashboardService dashboardService, IAsyncProgressService asyncProgressService, IMessageBuilder messageBuilder, IFrontendMessageBridge messageBridge,
      ClientInfo clientInfo, IUserPreferencesService userPreferencesService)
      : base(dialogService, driverService, contractorService, measurementService, vehicleService, cargoService, unitTypeService,
          dashboardService, asyncProgressService, messageBuilder, messageBridge, clientInfo, userPreferencesService)
    {
    }

    protected override async void OnViewLoaded(object view)
    {
      base.OnViewLoaded(view);

      var result = await MeasurementService.GetNextMeasurementNumberAsync();
      if (!result.Succeeded)
        return;

      MeasurementNumber = result.Response;
    }

    protected override void DefineValidationRules()
    {
      InvalidateProperty(() => CurrentMeasurementValue).When(() => !IsDecimalOrNull(CurrentMeasurementValue))
        .WithMessageKey("InvalidNumber").ButIgnoreOnLoad();

      InvalidateProperty(() => CurrentMeasurementValue).When(() => !IsGreaterOrEqualThan(CurrentMeasurementValue, 0) && !string.IsNullOrEmpty(CurrentMeasurementValue))
        .WithMessageKey("MustBeGreaterOrEqualThan", 0).ButIgnoreOnLoad();

      InvalidateProperty(() => DeclaredWeightValue).When(() => !IsDecimalOrNull(DeclaredWeightValue))
        .WithMessageKey("InvalidNumber").ButIgnoreOnLoad();

      InvalidateProperty(() => DeclaredWeightValue).When(() => !IsGreaterOrEqualThan(DeclaredWeightValue, 0) && !string.IsNullOrEmpty(DeclaredWeightValue))
        .WithMessageKey("MustBeGreaterOrEqualThan", 0).ButIgnoreOnLoad();
    }
  }
}
