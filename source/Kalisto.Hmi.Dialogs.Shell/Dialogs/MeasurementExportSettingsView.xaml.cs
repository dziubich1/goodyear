﻿using MahApps.Metro.Controls;

namespace Kalisto.Hmi.Dialogs.Shell.Dialogs
{
  /// <summary>
  /// Interaction logic for MeasurementExportSettingsView.xaml
  /// </summary>
  public partial class MeasurementExportSettingsView : MetroWindow
  {
    public MeasurementExportSettingsView()
    {
      InitializeComponent();
    }
  }
}
