﻿using MahApps.Metro.Controls;

namespace Kalisto.Hmi.Dialogs.Shell.Dialogs
{
  /// <summary>
  /// Interaction logic for NotCompletedMeasurementsDialogView.xaml
  /// </summary>
  public partial class NotCompletedMeasurementsDialogView : MetroWindow
  {
    public NotCompletedMeasurementsDialogView()
    {
      InitializeComponent();
    }
  }
}
