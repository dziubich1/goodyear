﻿using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Hmi.Core;
using Kalisto.Hmi.Core.Communication;
using Kalisto.Hmi.Core.Services;
using PropertyChanged;

namespace Kalisto.Hmi.Dialogs.Shell.Dialogs
{
  [AddINotifyPropertyChangedInterface]
  public class LicenseKeyDialogViewModel : ModalViewModelBase<LicenseKeyDialogViewModel>
  {
    public sealed override string DialogNameKey { get; set; }

    public string LicenseKey { get; set; }

    public LicenseKeyDialogViewModel(IDialogService dialogService, IAsyncProgressService asyncProgressService,
      IMessageBuilder messageBuilder, IFrontendMessageBridge messageBridge, ClientInfo clientInfo)
      : base(dialogService, asyncProgressService, messageBuilder, messageBridge, clientInfo)
    {
      DialogNameKey = "LicenseKeyDialogTitle";
    }

    protected override void DefineValidationRules()
    {
      InvalidateProperty(() => LicenseKey).When(() => string.IsNullOrEmpty(LicenseKey))
        .WithMessageKey("Required").ButIgnoreOnLoad();
    }
  }
}
