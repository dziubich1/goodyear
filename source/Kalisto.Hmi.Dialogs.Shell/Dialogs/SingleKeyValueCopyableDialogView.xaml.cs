﻿using MahApps.Metro.Controls;

namespace Kalisto.Hmi.Dialogs.Shell.Dialogs
{
  /// <summary>
  /// Interaction logic for SingleKeyValueCopyableDialogView.xaml
  /// </summary>
  public partial class SingleKeyValueCopyableDialogView : MetroWindow
  {
    public SingleKeyValueCopyableDialogView()
    {
      InitializeComponent();
    }
  }
}
