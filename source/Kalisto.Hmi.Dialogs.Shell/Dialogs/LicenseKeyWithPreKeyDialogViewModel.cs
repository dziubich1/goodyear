﻿using Kalisto.Backend.Operations.Shell.Commands;
using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Hmi.Core;
using Kalisto.Hmi.Core.Commands;
using Kalisto.Hmi.Core.Communication;
using Kalisto.Hmi.Core.Services;
using PropertyChanged;
using System;
using Kalisto.Localization;

namespace Kalisto.Hmi.Dialogs.Shell.Dialogs
{
  [AddINotifyPropertyChangedInterface]
  public class LicenseKeyWithPreKeyDialogViewModel : ModalViewModelBase<LicenseKeyWithPreKeyDialogViewModel>
  {
    private readonly ILocalizer<Localization.Common.Common> _localizer;

    public sealed override string DialogNameKey { get; set; }

    public string StationsCount { get; set; }

    public int StationsCountValue => Convert.ToInt32(StationsCount);

    public string LicenseKey { get; set; }

    public DelegateCommand GeneratePreKeyCommand { get; set; }

    public LicenseKeyWithPreKeyDialogViewModel(IDialogService dialogService, IAsyncProgressService asyncProgressService,
      IMessageBuilder messageBuilder, IFrontendMessageBridge messageBridge, ClientInfo clientInfo)
      : base(dialogService, asyncProgressService, messageBuilder, messageBridge, clientInfo)
    {
      _localizer = new LocalizationManager<Localization.Common.Common>();
      DialogNameKey = "LicenseKeyDialogTitle";
      GeneratePreKeyCommand = new DelegateCommand(GeneratePreKey, IsStationsCountValid);
    }

    protected override void DefineValidationRules()
    {
      InvalidateProperty(() => StationsCount).When(() => string.IsNullOrEmpty(StationsCount))
        .WithMessageKey("Required").ButIgnoreOnLoad();

      InvalidateProperty(() => StationsCount).When(() => !IsNumber(StationsCount))
        .WithMessageKey("InvalidNumber").ButIgnoreOnLoad();

      InvalidateProperty(() => StationsCount).When(() => !IsGreaterThan(StationsCount, 0))
        .WithMessageKey("MustBeGreaterThan", 0).ButIgnoreOnLoad();

      InvalidateProperty(() => LicenseKey).When(() => string.IsNullOrEmpty(LicenseKey))
        .WithMessageKey("Required").ButIgnoreOnLoad();
    }

    private async void GeneratePreKey(object parameter)
    {
      var message = MessageBuilder.CreateNew<GenerateLicensePreKeyCommand>()
        .BasedOn(ClientInfo)
        .WithProperty(c => c.StationsCount, StationsCountValue).Build();

      var result = await MessageBridge.SendMessageWithResultAsync<string>(message);
      if (result == null || !result.Succeeded)
        return;

      await DialogService.ShowModalDialogAsync<SingleKeyValueCopyableDialogViewModel>(vm =>
      {
        vm.DialogNameKey = "LicensePreKeyTitle";
        vm.Key = _localizer.Localize(() => Localization.Common.Common.LicensePreKeyLabel);
        vm.Value = result.Response;
        vm.IsReadOnly = true;
      });
    }

    public void OnStationsCountChanged()
    {
      GeneratePreKeyCommand.RaiseCanExecuteChanged();
    }

    private bool IsStationsCountValid(object obj)
    {
      return !string.IsNullOrEmpty(StationsCount)
             && IsNumber(StationsCount)
             && IsGreaterThan(StationsCount, 0);
    }
  }
}
