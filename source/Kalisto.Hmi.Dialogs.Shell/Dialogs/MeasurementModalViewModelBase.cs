﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Frontend.Notifications;
using Kalisto.Frontend.Notifications.Enums;
using Kalisto.Hmi.Core;
using Kalisto.Hmi.Core.Commands;
using Kalisto.Hmi.Core.Communication;
using Kalisto.Hmi.Core.Controls;
using Kalisto.Hmi.Core.Services;
using Kalisto.Hmi.Dialogs.Cargoes.Dialogs;
using Kalisto.Hmi.Dialogs.Cargoes.Services;
using Kalisto.Hmi.Dialogs.Common;
using Kalisto.Hmi.Dialogs.Common.Configuration;
using Kalisto.Hmi.Dialogs.Common.Models;
using Kalisto.Hmi.Dialogs.Contractors.Dialogs;
using Kalisto.Hmi.Dialogs.Contractors.Services;
using Kalisto.Hmi.Dialogs.Dashboard.Models;
using Kalisto.Hmi.Dialogs.Dashboard.Services;
using Kalisto.Hmi.Dialogs.Drivers.Dialogs;
using Kalisto.Hmi.Dialogs.Drivers.Services;
using Kalisto.Hmi.Dialogs.Measurements.Models;
using Kalisto.Hmi.Dialogs.Measurements.Services;
using Kalisto.Hmi.Dialogs.Vehicles.Dialogs;
using Kalisto.Hmi.Dialogs.Vehicles.Models;
using Kalisto.Hmi.Dialogs.Vehicles.Services;
using Kalisto.Localization;
using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Kalisto.Hmi.Dialogs.Common.Services;
using Kalisto.Localization.Messages;

namespace Kalisto.Hmi.Dialogs.Shell.Dialogs
{
  [AddINotifyPropertyChangedInterface]
  public abstract class MeasurementModalViewModelBase<TViewModel>
    : ModalViewModelBase<TViewModel> where TViewModel : class
  {
    private readonly ILocalizer<Localization.Common.Common> _localizer;

    private readonly ILocalizer<Messages> _messageLocalizer;

    public override string DialogNameKey { get; set; }

    public DateTime MeasurementDate { get; set; } = DateTime.Now;

    public MeasurementClass? SelectedMeasurementClass { get; set; }

    [AlsoNotifyFor(nameof(IsMeasurementNumberVisible))]
    public long? MeasurementNumber { get; set; }

    public bool IsMeasurementNumberVisible => MeasurementNumber.HasValue;

    [AlsoNotifyFor(nameof(MeasurementText))]
    public LocalizedDate FirstMeasurementDate { get; set; }

    public string MeasurementText => GetMeasurementText();

    public string Comment { get; set; }

    public ICommand DriverNotFoundCommand { get; set; }

    public ICommand ContractorNotFoundCommand { get; set; }

    public ICommand CargoNotFoundCommand { get; set; }

    public ICommand VehicleNotFoundCommand { get; set; }

    public ICommand AddDriverCommand { get; set; }

    public ICommand AddContractorCommand { get; set; }

    public ICommand AddVehicleCommand { get; set; }

    public ICommand AddCargoCommand { get; set; }

    public ICommand ShowNotCompletedMeasurementsCommand { get; set; }

    public ObservableCollectionEx<ReferenceModel> Drivers { get; set; }

    public ObservableCollectionEx<ReferenceModel> Contractors { get; set; }

    public ObservableCollectionEx<ReferenceModel> Cargoes { get; set; }

    public ObservableCollectionEx<ReferenceModel> Vehicles { get; set; }

    public ObservableCollectionEx<ReferenceModel> SemiTrailers { get; set; }

    public ObservableCollectionEx<WeightDevice> DeviceCollection { get; set; }

    public WeightDevice SelectedDevice { get; set; }

    public WeighingMode WeighingMode { get; set; }

    public WeighingDeviceStatus DeviceStatus { get; set; }

    public ReferenceModel Driver { get; set; }

    public ReferenceModel Contractor { get; set; }

    public ReferenceModel Cargo { get; set; }

    public ReferenceModel SemiTrailer { get; set; }

    public string SemiTrailerValue { get; set; } // required for non-existing semi-trailers

    public int DeviceId => SelectedDevice?.Id ?? -1;

    public ReferenceModel SelectedSemiTrailer
    {
      get
      {
        if (SemiTrailer == null)
          return new ReferenceModel
          {
            DisplayValue = SemiTrailerValue
          };

        return SemiTrailer;
      }
    }

    [AlsoNotifyFor(nameof(WeightExceeded))]
    public ReferenceModel Vehicle { get; set; }

    protected abstract override void DefineValidationRules();

    private readonly ReferenceModel _emptyItem;

    private readonly IDriverService _driverService;

    private readonly IContractorService _contractorService;

    private readonly IVehicleService _vehicleService;

    private readonly ICargoService _cargoService;

    protected IUnitTypeService UnitTypeService { get; }

    [AlsoNotifyFor(nameof(NetValue), nameof(WeightExceeded), nameof(WeighingThresholdExceeded))]
    public string CurrentMeasurementValue { get; set; }

    [AlsoNotifyFor(nameof(NetValue))]
    public string PreviousMeasurementValue { get; set; }

    [AlsoNotifyFor(nameof(WeighingThresholdExceeded))]
    public string DeclaredWeightValue { get; set; }

    public virtual string NetValue => GetNetValue();

    public long MeasurementId { get; set; }

    public UnitOfMeasurement CurrentMeasurementValueUom =>
      decimal.TryParse(CurrentMeasurementValue, out decimal value) ? UnitTypeService.ConvertBackValue(value) : null;

    public UnitOfMeasurement DeclaredWeightValueUom =>
      decimal.TryParse(DeclaredWeightValue, out decimal value) ? UnitTypeService.ConvertBackValue(value) : null;

    public UnitOfMeasurement NetWeightValueUom =>
      decimal.TryParse(NetValue, out decimal value) ? UnitTypeService.ConvertBackValue(value) : null;

    protected IMeasurementService MeasurementService { get; }

    public bool WeightExceeded => HasVehicleWeightExceeded();

    public bool IsDeviceIndicationValid => DeviceStatus == WeighingDeviceStatus.Stable || WeighingMode == WeighingMode.Manual;

    private readonly IUserPreferencesService _userPreferencesService;

    public bool WeighingThresholdExceeded => HasWeightThresholdExceeded();

    public string ExceededWeightValue { get; set; }

    protected MeasurementModalViewModelBase(IDialogService dialogService,
      IDriverService driverService, IContractorService contractorService, IMeasurementService measurementService,
      IVehicleService vehicleService, ICargoService cargoService, IUnitTypeService unitTypeService, IDashboardService dashboardService,
      IAsyncProgressService asyncProgressService, IMessageBuilder messageBuilder,
      IFrontendMessageBridge messageBridge, ClientInfo clientInfo, IUserPreferencesService userPreferencesService)
      : base(dialogService, asyncProgressService, messageBuilder, messageBridge,
          clientInfo)
    {
      _localizer = new LocalizationManager<Localization.Common.Common>();
      _messageLocalizer = new LocalizationManager<Messages>();
      _driverService = driverService;
      _contractorService = contractorService;
      _vehicleService = vehicleService;
      _cargoService = cargoService;
      _emptyItem = new ReferenceModel() { DisplayValue = "", Id = -1 };
      MeasurementService = measurementService;
      UnitTypeService = unitTypeService;
      _userPreferencesService = userPreferencesService;

      DriverNotFoundCommand = new DelegateCommand(OpenDriverNotFoundDialog);
      ContractorNotFoundCommand = new DelegateCommand(OpenContractorNotFoundDialog);
      CargoNotFoundCommand = new DelegateCommand(OpenCargoNotFoundDialog);
      VehicleNotFoundCommand = new DelegateCommand(OpenVehicleNotFoundDialog);
      ShowNotCompletedMeasurementsCommand = new DelegateCommand(ShowNotCompletedMeasurements);

      AddDriverCommand = new DelegateCommand(OpenAddDriverDialog);
      AddContractorCommand = new DelegateCommand(OpenAddContractorDialog);
      AddVehicleCommand = new DelegateCommand(OpenAddVehicleDialog);
      AddCargoCommand = new DelegateCommand(OpenAddCargoDialog);

      Drivers = new ObservableCollectionEx<ReferenceModel>();
      Contractors = new ObservableCollectionEx<ReferenceModel>();
      Vehicles = new ObservableCollectionEx<ReferenceModel>();
      Cargoes = new ObservableCollectionEx<ReferenceModel>();
      SemiTrailers = new ObservableCollectionEx<ReferenceModel>();
      DeviceCollection = new ObservableCollectionEx<WeightDevice>();
      dashboardService.WeighingValueUpdated += OnWeighingValueUpdated;
      DeviceStatus = WeighingDeviceStatus.NotStable;
    }

    protected override async void OnViewLoaded(object view)
    {
      base.OnViewLoaded(view);

      await LoadDrivers();
      await LoadContractors();
      await LoadVehicles();
      await LoadCargoes();
      await LoadSemiTrailers();

      SetSelectedItemsReferences();
    }

    protected decimal? CalculateDifferenceBetweenStringValues(string previousMeasurementValue, string currentMeasurementValue)
    {
      if (!decimal.TryParse(PreviousMeasurementValue, out decimal val1))
        return null;

      if (!decimal.TryParse(CurrentMeasurementValue, out decimal val2))
        return null;

      var difference = UnitTypeService.ConvertBackValue(val2).OriginalValue - UnitTypeService.ConvertBackValue(val1).OriginalValue;

      return difference;
    }

    protected virtual string GetNetValue()
    {
      var diff = CalculateDifferenceBetweenStringValues(PreviousMeasurementValue, CurrentMeasurementValue);
      if (!diff.HasValue)
        return string.Empty;

      return new UnitOfMeasurement(Math.Abs(diff.Value), UnitTypeService).DisplayValueUnitless;
    }

    public void SetWeighingDevices(IList<WeighingDeviceInfo> deviceInfo)
    {
      if (!deviceInfo.Any())
        return;

      DeviceCollection.AddRange(deviceInfo.Select(c => new WeightDevice
      {
        Id = c.Id,
        Name = c.Name
      }));

      SelectedDevice = DeviceCollection.FirstOrDefault();
    }

    private async void OpenDriverNotFoundDialog(object parameter)
    {
      await DialogService.ShowMessageAsync(_messageLocalizer.Localize(() => Messages.Error),
        _messageLocalizer.Localize(() => Messages.NoDriverFound), MessageDialogType.Ok);
    }

    private async void OpenContractorNotFoundDialog(object parameter)
    {
      await DialogService.ShowMessageAsync(_messageLocalizer.Localize(() => Messages.Error),
        _messageLocalizer.Localize(() => Messages.NoContractorFound), MessageDialogType.Ok);
    }

    private async void OpenCargoNotFoundDialog(object parameter)
    {
      await DialogService.ShowMessageAsync(_messageLocalizer.Localize(() => Messages.Error),
        _messageLocalizer.Localize(() => Messages.NoCargoFound), MessageDialogType.Ok);
    }

    private async void OpenVehicleNotFoundDialog(object parameter)
    {
      await DialogService.ShowMessageAsync(_messageLocalizer.Localize(() => Messages.Error),
        _messageLocalizer.Localize(() => Messages.NoVehicleFound), MessageDialogType.Ok);
    }

    private void SetSelectedItemsReferences()
    {
      if (Driver != null)
        Driver = Drivers.FirstOrDefault(d => d.Id == Driver.Id);
      else
        Driver = Drivers.FirstOrDefault();

      if (Contractor != null)
        Contractor = Contractors.FirstOrDefault(c => c.Id == Contractor.Id);
      else
        Contractor = Contractors.FirstOrDefault();

      if (Vehicle != null)
        Vehicle = Vehicles.FirstOrDefault(c => c.Id == Vehicle.Id);
      else
        Vehicle = Vehicles.FirstOrDefault();

      if (Cargo != null)
        Cargo = Cargoes.FirstOrDefault(c => c.Id == Cargo.Id);
      else
        Cargo = Cargoes.FirstOrDefault();

      if (SemiTrailer != null)
        SemiTrailer = SemiTrailers.FirstOrDefault(c => c.Id == SemiTrailer.Id);
      else
        SemiTrailer = SemiTrailers.FirstOrDefault();
    }

    private async Task LoadSemiTrailers()
    {
      var result = await MeasurementService.GetAllSemiTrailersAsync();
      if (!result.Succeeded)
        return;

      var mapped = result.Response.Select(item => new ReferenceModel()
      {
        Id = item.Id,
        DisplayValue = item.Number
      });

      SemiTrailers.Clear();
      SemiTrailers.Add(_emptyItem);
      SemiTrailers.AddRange(mapped);
    }

    private async Task LoadDrivers()
    {
      var result = await _driverService.GetAllDriversAsync();
      if (!result.Succeeded)
        return;

      var mapped = result.Response.Select(item => new ReferenceModel()
      {
        Id = item.Id,
        DisplayValue = item.Identifier
      });
      Drivers.Clear();
      Drivers.Add(_emptyItem);
      Drivers.AddRange(mapped);
    }

    private async Task LoadContractors()
    {
      var result = await _contractorService.GetAllContractorsAsync();
      if (!result.Succeeded)
        return;

      var mapped = result.Response.Select(item => new ReferenceModel()
      {
        Id = item.Id,
        DisplayValue = item.Name
      });
      Contractors.Clear();
      Contractors.Add(_emptyItem);
      Contractors.AddRange(mapped);
    }

    private async Task LoadVehicles()
    {
      var result = await _vehicleService.GetAllVehiclesAsync();
      if (!result.Succeeded)
        return;

      var mapped = result.Response.Select(item => new ReferenceModel()
      {
        Id = item.Id,
        DisplayValue = item.PlateNumber,
        RawData = item
      });
      Vehicles.Clear();
      Vehicles.Add(_emptyItem);
      Vehicles.AddRange(mapped);
    }

    public virtual void OnVehicleChanged()
    {
      if (Vehicle?.RawData == null)
        return;

      var vehicleData = Vehicle.RawData as VehicleModel;

      if (vehicleData?.Driver != null)
        Driver = Drivers.FirstOrDefault(d => d.Id == vehicleData.Driver.Id);

      if (vehicleData?.Contractor != null)
        Contractor = Contractors.FirstOrDefault(d => d.Id == vehicleData.Contractor.Id);
    }

    private async Task LoadCargoes()
    {
      var result = await _cargoService.GetAllCargoesAsync();
      if (!result.Succeeded)
        return;

      var mapped = result.Response.Select(item => new ReferenceModel()
      {
        Id = item.Id,
        DisplayValue = item.Name
      });
      Cargoes.Clear();
      Cargoes.Add(_emptyItem);
      Cargoes.AddRange(mapped);
    }

    private async void OpenAddDriverDialog(object parameter)
    {
      long driverId = -1;
      var dlg = await DialogService.ShowModalDialogAsync<AddOrUpdateDriverModalDialogViewModel>(
        vm =>
        {
          vm.DialogNameKey = "AddDriver";
        },
        async vm =>
        {
          var result = await _driverService.AddDriverAsync(vm.Identifier, vm.PhoneNumber);

          if (result.Succeeded)
            driverId = result.Response;

          return result.Succeeded;
        });

      if (!dlg.Result.HasValue || !dlg.Result.Value)
      {
        return;
      }

      await LoadDrivers();
      Driver = Drivers.FirstOrDefault(c => c.Id == driverId);
    }

    private async void OpenAddContractorDialog(object parameter)
    {
      long contractorId = -1;
      var dlg = await DialogService.ShowModalDialogAsync<AddOrUpdateContractorModalDialogViewModel>(
      vm =>
      {
        vm.DialogNameKey = "AddContractor";
      },
      async vm =>
      {
        var result = await _contractorService.AddContractorAsync(vm.Name,
          vm.Address, vm.PostalCode, vm.City, vm.TaxId);

        if (result.Succeeded)
          contractorId = result.Response;

        return result.Succeeded;
      });

      if (!dlg.Result.HasValue || !dlg.Result.Value)
        return;

      await LoadContractors();
      Contractor = Contractors.FirstOrDefault(c => c.Id == contractorId);
    }

    private async void OpenAddVehicleDialog(object parameter)
    {
      long vehicleId = -1;
      long? driverId = -1;
      long? contractorId = -1;
      var dlg = await DialogService.ShowModalDialogAsync<AddOrUpdateVehicleModalDialogViewModel>(
      vm =>
      {
        vm.DialogNameKey = "AddVehicle";
        vm.IsTareVisible = false;
      },
      async vm =>
      {
        driverId = vm.Driver != null && vm.Driver.Id != -1 ? vm.Driver.Id : (long?)null;
        contractorId = vm.Contractor != null && vm.Contractor.Id != -1 ? vm.Contractor.Id : (long?)null;

        var result = await _vehicleService.AddVehicleAsync(vm.PlateNumber,
          null, vm.MaxVehicleWeightUom?.OriginalValue,
          driverId, contractorId);

        if (result.Succeeded)
          vehicleId = result.Response;

        return result.Succeeded;
      });

      if (!dlg.Result.HasValue || !dlg.Result.Value)
        return;

      await LoadVehicles();
      Vehicle = Vehicles.FirstOrDefault(c => c.Id == vehicleId);

      if (driverId != null && driverId != -1)
      {
        await LoadDrivers();
        Driver = Drivers.FirstOrDefault(x => x.Id == driverId);
      }
      if (contractorId != null && contractorId != -1)
      {
        await LoadContractors();
        Contractor = Contractors.FirstOrDefault(x => x.Id == contractorId);
      }
    }

    private async void OpenAddCargoDialog(object parameter)
    {
      long cargoId = -1;
      var dlg = await DialogService.ShowModalDialogAsync<AddOrUpdateCargoModalDialogViewModel>(
      vm =>
      {
        vm.DialogNameKey = "AddCargo";
      },
      async viewModel =>
      {
        var result = await _cargoService.AddCargoAsync(viewModel.Name, viewModel.PriceValue);

        if (result.Succeeded)
          cargoId = result.Response;

        return result.Succeeded;
      });

      if (!dlg.Result.HasValue || !dlg.Result.Value)
        return;

      await LoadCargoes();
      Cargo = Cargoes.FirstOrDefault(c => c.Id == cargoId);
    }

    private void UpdateMeasurementData(MeasurementModel model)
    {
      if (model == null)
        return;
      MeasurementId = model.Id;
      MeasurementNumber = model.Number;
      FirstMeasurementDate = model.Date1;
      PreviousMeasurementValue = model.Measurement1.DisplayValueUnitless;
      DeclaredWeightValue = model.DeclaredWeight.DisplayValueUnitless;
      Vehicle = Vehicles.FirstOrDefault(c => c.Id == model.Vehicle?.Id);
      Contractor = Contractors.FirstOrDefault(c => c.Id == model.Contractor?.Id);
      Cargo = Cargoes.FirstOrDefault(c => c.Id == model.Cargo?.Id);
      Driver = Drivers.FirstOrDefault(c => c.Id == model.Driver?.Id);
      SemiTrailer = SemiTrailers.FirstOrDefault(c => c.Id == model.SemiTrailer?.Id);
      Comment = model.Comment;
    }

    private string GetMeasurementText()
    {
      if (FirstMeasurementDate?.Value == null || MeasurementNumber == null)
        return string.Empty;

      return string.Format(_localizer.Localize(() => Localization.Common.Common.WeightFromDayText),
        MeasurementNumber, FirstMeasurementDate?.LocalizedDateValue);
    }

    private async void ShowNotCompletedMeasurements(object parameter)
    {
      await DialogService.ShowModalDialogAsync<NotCompletedMeasurementsDialogViewModel>(
        vm =>
        {
          vm.ValidateOnLoad = true;
        },
        vm =>
        {
          UpdateMeasurementData(vm.SelectedMeasurement);
          return Task.FromResult(vm.SelectedMeasurement != null);
        });
    }

    public void OnWeighingModeChanged()
    {
      CurrentMeasurementValue = 0.ToString();
      if (WeighingMode == WeighingMode.Manual)
        DeviceStatus = WeighingDeviceStatus.Stable;
      else DeviceStatus = WeighingDeviceStatus.NotStable;
    }

    public void OnSelectedDeviceChanged()
    {
      if (WeighingMode == WeighingMode.Manual)
        return;

      DeviceStatus = WeighingDeviceStatus.NotStable;
      CurrentMeasurementValue = 0.ToString();
    }

    private void OnWeighingValueUpdated(WeighingValueUpdatedNotification notification)
    {
      if (WeighingMode == WeighingMode.Manual)
        return;

      if (SelectedDevice == null || SelectedDevice.Id != notification.DeviceId)
        return;

      DeviceStatus = notification.Status;
      CurrentMeasurementValue = new UnitOfMeasurement(notification.Value, UnitTypeService).DashedDisplayValueUnitless;
    }

    private bool HasVehicleWeightExceeded()
    {
      if (Vehicle == null)
        return false;

      if (CurrentMeasurementValueUom == null)
        return false;

      if (!(Vehicle.RawData is VehicleModel vehicle))
        return false;

      if (!vehicle.MaxVehicleWeight.OriginalValue.HasValue)
        return false;

      if (!CurrentMeasurementValueUom.OriginalValue.HasValue)
        return false;

      if (CurrentMeasurementValueUom.OriginalValue.Value > vehicle.MaxVehicleWeight.OriginalValue.Value)
        return true;

      return false;
    }

    private bool HasWeightThresholdExceeded()
    {
      if (NetWeightValueUom == null)
        return false;

      if (DeclaredWeightValueUom == null)
        return false;

      if (!NetWeightValueUom.OriginalValue.HasValue)
        return false;

      if (!DeclaredWeightValueUom.OriginalValue.HasValue)
        return false;

      if (Math.Abs(NetWeightValueUom.OriginalValue.Value - DeclaredWeightValueUom.OriginalValue.Value) <=
          _userPreferencesService.Configuration.WeighingThreshold)
        return false;

      ExceededWeightValue = $"{new UnitOfMeasurement((NetWeightValueUom.OriginalValue.Value - DeclaredWeightValueUom.OriginalValue.Value), UnitTypeService).DisplayValue}.";
      return true;
    }

    public virtual void OnCurrentMeasurementValueChanged()
    {
    }

    public virtual void OnPreviousMeasurementValueChanged()
    {
    }
  }
}
