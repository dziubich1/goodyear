﻿using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Hmi.Core;
using Kalisto.Hmi.Core.Communication;
using Kalisto.Hmi.Core.Services;
using Kalisto.Hmi.Dialogs.Common.Configuration;
using PropertyChanged;

namespace Kalisto.Hmi.Dialogs.Shell.Dialogs
{
  [AddINotifyPropertyChangedInterface]
  public class CompanyInfoDialogViewModel : ModalViewModelBase<CompanyInfoDialogViewModel>
  {
    public sealed override string DialogNameKey { get; set; }

    public string CompanyTaxNumber { get; set; }

    public string CompanyName { get; set; }

    public string CompanyStreet { get; set; }

    public string CompanyPostcode { get; set; }

    public string CompanyCity { get; set; }

    public string CompanyAdditionalInfo { get; set; }

    public CompanyInfoDialogViewModel(IDialogService dialogService, IAsyncProgressService asyncProgressService,
      IMessageBuilder messageBuilder, IFrontendMessageBridge messageBridge, ClientInfo clientInfo)
      : base(dialogService, asyncProgressService, messageBuilder, messageBridge, clientInfo)
    {
      DialogNameKey = "CompanyInfoDialogTitle";
    }

    protected override void DefineValidationRules()
    {
    }
  }
}
