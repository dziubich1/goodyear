﻿using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Hmi.Core;
using Kalisto.Hmi.Core.Commands;
using Kalisto.Hmi.Core.Communication;
using Kalisto.Hmi.Core.Services;
using Kalisto.Hmi.Dialogs.Measurements.Services;
using PropertyChanged;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Kalisto.Hmi.Dialogs.Measurements.Models;

namespace Kalisto.Hmi.Dialogs.Shell.Dialogs
{
  [AddINotifyPropertyChangedInterface]
  public class NotCompletedMeasurementsDialogViewModel : ModalViewModelBase<NotCompletedMeasurementsDialogViewModel>
  {
    private readonly IMeasurementService _measurementService;

    public sealed override string DialogNameKey { get; set; }

    public ObservableCollectionEx<MeasurementModel> NotCompletedMeasurements { get; set; }

    public MeasurementModel SelectedMeasurement { get; set; }

    public new ICommand CancelCommand { get; set; }

    public ICommand GridItemDoubleClickCommand { get; set; }

    public NotCompletedMeasurementsDialogViewModel(IDialogService dialogService,
      IMeasurementService measurementService, IAsyncProgressService asyncProgressService,
      IMessageBuilder messageBuilder, IFrontendMessageBridge messageBridge, ClientInfo clientInfo)
      : base(dialogService, asyncProgressService, messageBuilder, messageBridge, clientInfo)
    {
      _measurementService = measurementService;
      NotCompletedMeasurements = new ObservableCollectionEx<MeasurementModel>();

      DialogNameKey = "NotCompletedMeasurementList";

      GridItemDoubleClickCommand = new DelegateCommand(PickItem);
      CancelCommand = new DelegateCommand(c =>
      {
        SelectedMeasurement = null;
        base.CancelCommand.Execute(c);
      });
    }

    protected override void DefineValidationRules()
    {
      InvalidateProperty(() => SelectedMeasurement).When(() => SelectedMeasurement == null)
        .WithMessageKey("Required");
    }

    protected override async void OnViewLoaded(object view)
    {
      base.OnViewLoaded(view);

      await LoadNotCompletedMeasurements();
    }

    public void OnSelectedMeasurementChanged()
    {
      Validate();
    }

    private void PickItem(object obj)
    {
      SubmitCommand.Execute(obj);
    }

    private async Task LoadNotCompletedMeasurements()
    {
      var result = await _measurementService.GetNotCompletedMeasurementsAsync();

      if (!result.Succeeded)
        return;

      var mappedMeasurements = result.Response.OrderBy(m => m.Number);

      NotCompletedMeasurements.Clear();
      NotCompletedMeasurements.AddRange(mappedMeasurements);
    }
  }
}
