﻿using MahApps.Metro.Controls;

namespace Kalisto.Hmi.Dialogs.Shell.Dialogs
{
  /// <summary>
  /// Interaction logic for LicensePreKeyDialogView.xaml
  /// </summary>
  public partial class LicensePreKeyDialogView : MetroWindow
  {
    public LicensePreKeyDialogView()
    {
      InitializeComponent();
    }
  }
}
