﻿using System.Drawing;

namespace Kalisto.Hmi.Dialogs.Shell.Dialogs
{
  public class ImagePreviewWindowDialogViewModel
  {
    public Image Image { get; set; }
  }
}
