﻿using System.Collections.Generic;
using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Frontend.Notifications;
using Kalisto.Frontend.Notifications.Enums;
using Kalisto.Hmi.Core;
using Kalisto.Hmi.Core.Communication;
using Kalisto.Hmi.Core.Controls;
using Kalisto.Hmi.Core.Services;
using Kalisto.Hmi.Dialogs.Common;
using Kalisto.Hmi.Dialogs.Vehicles.Services;
using PropertyChanged;
using System.Linq;
using System.Threading.Tasks;
using Kalisto.Hmi.Dialogs.Common.Models;
using Kalisto.Hmi.Dialogs.Common.Services;
using Kalisto.Hmi.Dialogs.Dashboard.Models;
using Kalisto.Hmi.Dialogs.Dashboard.Services;

namespace Kalisto.Hmi.Dialogs.Shell.Dialogs
{
  [AddINotifyPropertyChangedInterface]
  public class TareDialogViewModel : ModalViewModelBase<TareDialogViewModel>
  {
    private readonly IUnitTypeService _unitTypeService;

    private readonly IVehicleService _vehicleService;

    public sealed override string DialogNameKey { get; set; }

    public WeightDevice SelectedDevice { get; set; }

    public WeighingMode WeighingMode { get; set; }

    public WeighingDeviceStatus DeviceStatus { get; set; }

    public string Tare { get; set; }

    public ObservableCollectionEx<ReferenceModel> Vehicles { get; set; }

    public ObservableCollectionEx<WeightDevice> DeviceCollection { get; set; }

    public UnitOfMeasurement TareUom =>
     decimal.TryParse(Tare, out decimal value) ? _unitTypeService.ConvertBackValue(value) : null;

    public ReferenceModel Vehicle { get; set; }

    private readonly ReferenceModel _emptyItem;

    public TareDialogViewModel(IDialogService dialogService, IAsyncProgressService asyncProgressService,
      IMessageBuilder messageBuilder, IFrontendMessageBridge messageBridge, ClientInfo clientInfo, IVehicleService vehicleService, IUnitTypeService unitTypeService, IDashboardService dashboardService)
  : base(dialogService, asyncProgressService, messageBuilder, messageBridge, clientInfo)
    {
      DialogNameKey = "Tare";

      _vehicleService = vehicleService;
      _unitTypeService = unitTypeService;
      dashboardService.WeighingValueUpdated += OnWeighingValueUpdated;

      Vehicles = new ObservableCollectionEx<ReferenceModel>();
      _emptyItem = new ReferenceModel { DisplayValue = "", Id = -1 };

      DeviceCollection = new ObservableCollectionEx<WeightDevice>();
      DeviceStatus = WeighingDeviceStatus.NotStable;
    }

    protected override async void OnViewLoaded(object view)
    {
      base.OnViewLoaded(view);

      await LoadVehicles();
      SetSelectedItemsReferences();
    }

    public void SetWeighingDevices(IList<WeighingDeviceInfo> deviceInfo)
    {
      if (!deviceInfo.Any())
        return;

      DeviceCollection.AddRange(deviceInfo.Select(c => new WeightDevice
      {
        Id = c.Id,
        Name = c.Name
      }));

      SelectedDevice = DeviceCollection.FirstOrDefault();
    }

    private void SetSelectedItemsReferences()
    {
      if (Vehicle != null)
        Vehicle = Vehicles.FirstOrDefault(d => d.Id == Vehicle.Id);
      else
        Vehicle = Vehicles.FirstOrDefault();
    }

    private async Task LoadVehicles()
    {
      var result = await _vehicleService.GetAllVehiclesAsync();
      if (!result.Succeeded)
        return;

      var mappedVehicles = result.Response.Select(d => new ReferenceModel()
      {
        Id = d.Id,
        DisplayValue = d.PlateNumber
      });
      Vehicles.Clear();
      Vehicles.Add(_emptyItem);
      Vehicles.AddRange(mappedVehicles);
    }

    protected override void DefineValidationRules()
    {
      InvalidateProperty(() => Tare).When(() => !IsDecimalOrNull(Tare))
        .WithMessageKey("InvalidNullableNumber").ButIgnoreOnLoad();

      InvalidateProperty(() => Vehicle).When(() => Vehicle == null || string.IsNullOrEmpty(Vehicle.DisplayValue))
        .WithMessageKey("InvalidNullableNumber").ButIgnoreOnLoad();

      InvalidateProperty(() => DeviceStatus).When(() => DeviceStatus != WeighingDeviceStatus.Stable && WeighingMode != WeighingMode.Manual)
        .WithMessageKey("WeightDeviceNotStable");
    }

    public void OnWeighingModeChanged()
    {
      Tare = 0.ToString();
      if (WeighingMode == WeighingMode.Manual)
        DeviceStatus = WeighingDeviceStatus.Stable;
      else DeviceStatus = WeighingDeviceStatus.NotStable;
    }

    public void OnSelectedDeviceChanged()
    {
      if (WeighingMode == WeighingMode.Manual)
        return;

      DeviceStatus = WeighingDeviceStatus.NotStable;
      Tare = 0.ToString();
    }

    private void OnWeighingValueUpdated(WeighingValueUpdatedNotification notification)
    {
      if (WeighingMode == WeighingMode.Manual)
        return;

      if (SelectedDevice == null || SelectedDevice.Id != notification.DeviceId)
        return;

      DeviceStatus = notification.Status;
      Tare = new UnitOfMeasurement((decimal)notification.Value, _unitTypeService).DisplayValueUnitless;
    }
  }
}
