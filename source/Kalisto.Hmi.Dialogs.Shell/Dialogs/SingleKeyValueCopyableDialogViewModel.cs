﻿using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Hmi.Core;
using Kalisto.Hmi.Core.Communication;
using Kalisto.Hmi.Core.Services;
using PropertyChanged;

namespace Kalisto.Hmi.Dialogs.Shell.Dialogs
{
  [AddINotifyPropertyChangedInterface]
  public class SingleKeyValueCopyableDialogViewModel : ModalViewModelBase<SingleKeyValueCopyableDialogViewModel>
  {
    public sealed override string DialogNameKey { get; set; }

    public string Key { get; set; }

    public string Value { get; set; }

    public bool IsReadOnly { get; set; }

    public SingleKeyValueCopyableDialogViewModel(IDialogService dialogService, IAsyncProgressService asyncProgressService,
      IMessageBuilder messageBuilder, IFrontendMessageBridge messageBridge, ClientInfo clientInfo)
      : base(dialogService, asyncProgressService, messageBuilder, messageBridge, clientInfo)
    {
    }

    protected override void DefineValidationRules()
    {
    }
  }
}
