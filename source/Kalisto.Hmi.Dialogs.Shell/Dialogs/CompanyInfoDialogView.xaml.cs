﻿using MahApps.Metro.Controls;

namespace Kalisto.Hmi.Dialogs.Shell.Dialogs
{
  /// <summary>
  /// Interaction logic for CompanyInfoDialogView.xaml
  /// </summary>
  public partial class CompanyInfoDialogView : MetroWindow
  {
    public CompanyInfoDialogView()
    {
      InitializeComponent();
    }
  }
}
