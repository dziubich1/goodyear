﻿using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Hmi.Core;
using Kalisto.Hmi.Core.Communication;
using Kalisto.Hmi.Core.Services;
using PropertyChanged;
using System;

namespace Kalisto.Hmi.Dialogs.Shell.Dialogs
{
  [AddINotifyPropertyChangedInterface]
  public class LicensePreKeyDialogViewModel : ModalViewModelBase<LicensePreKeyDialogViewModel>
  {
    public sealed override string DialogNameKey { get; set; }

    public string StationsCount { get; set; }

    public int StationsCountValue => Convert.ToInt32(StationsCount);

    public LicensePreKeyDialogViewModel(IDialogService dialogService, IAsyncProgressService asyncProgressService,
      IMessageBuilder messageBuilder, IFrontendMessageBridge messageBridge, ClientInfo clientInfo)
      : base(dialogService, asyncProgressService, messageBuilder, messageBridge, clientInfo)
    {
      DialogNameKey = "LicensePreKeyDialogTitle";
    }

    protected override void DefineValidationRules()
    {
      InvalidateProperty(() => StationsCount).When(() => string.IsNullOrEmpty(StationsCount))
        .WithMessageKey("Required").ButIgnoreOnLoad();

      InvalidateProperty(() => StationsCount).When(() => !IsNumber(StationsCount))
        .WithMessageKey("InvalidNumber").ButIgnoreOnLoad();

      InvalidateProperty(() => StationsCount).When(() => !IsGreaterThan(StationsCount, 0))
        .WithMessageKey("MustBeGreaterThan", 0).ButIgnoreOnLoad();
    }
  }
}
