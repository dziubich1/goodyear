﻿using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Hmi.Core;
using Kalisto.Hmi.Core.Communication;
using Kalisto.Hmi.Core.Services;
using Kalisto.Hmi.Dialogs.Common.Configuration;
using Kalisto.Hmi.Dialogs.Common.Enum;
using Kalisto.Localization;
using PropertyChanged;
using System;
using System.Linq;

namespace Kalisto.Hmi.Dialogs.Shell.Dialogs
{
  [AddINotifyPropertyChangedInterface]
  public class UserPreferencesDialogViewModel : ModalViewModelBase<UserPreferencesDialogViewModel>
  {
    private readonly IUserPreferencesService _userPreferencesService;

    public sealed override string DialogNameKey { get; set; }

    public ObservableCollectionEx<LocalizedEnum> WeightUnits { get; set; }

    public LocalizedEnum SelectedWeightUnit { get; set; }

    public ObservableCollectionEx<LocalizedEnum> CameraPictureOnPrintoutOptions { get; set; }

    public LocalizedEnum SelectedCameraPictureOnPrintoutOption { get; set; }

    public ObservableCollectionEx<LocalizedEnum> PrintAfterWeighingMeasurementOptions { get; set; }

    public LocalizedEnum SelectedPrintAfterWeighingMeasurementOption { get; set; }

    public string WeighingThreshold { get; set; }

    public ObservableCollectionEx<LocalizedEnum> DoubleCopyOptions { get; set; }

    public LocalizedEnum SelectedDoubleCopyOption { get; set; }

    public UserPreferencesConfiguration SelectedConfiguration => new UserPreferencesConfiguration
    {
      UnitType = (UnitType)SelectedWeightUnit.Enum,
      IncludeImagesOnPrintouts = (YesNo)SelectedCameraPictureOnPrintoutOption.Enum,
      PrintWeighingReceiptAfterMeasurement = (YesNoAsk)SelectedPrintAfterWeighingMeasurementOption.Enum,
      WeighingThreshold = decimal.Parse(WeighingThreshold),
      DoubleCopy = (YesNo)SelectedDoubleCopyOption.Enum
    };

    public UserPreferencesDialogViewModel(IDialogService dialogService, IAsyncProgressService asyncProgressService,
      IMessageBuilder messageBuilder, IFrontendMessageBridge messageBridge, IUserPreferencesService userPreferencesService, ClientInfo clientInfo)
      : base(dialogService, asyncProgressService, messageBuilder, messageBridge, clientInfo)
    {
      _userPreferencesService = userPreferencesService;
      DialogNameKey = "UserPreferencesDialogTitle";
      WeightUnits = new ObservableCollectionEx<LocalizedEnum>();
      CameraPictureOnPrintoutOptions = new ObservableCollectionEx<LocalizedEnum>();
      PrintAfterWeighingMeasurementOptions = new ObservableCollectionEx<LocalizedEnum>();
      DoubleCopyOptions = new ObservableCollectionEx<LocalizedEnum>();
    }

    protected override void OnViewLoaded(object view)
    {
      base.OnViewLoaded(view);

      LoadWeightUnits();
      LoadPrintAfterWeighingMeasurementOptions();
      LoadCameraPictureOnPrintoutOptions();
      LoadDoubleCopyOptions();
      WeighingThreshold = _userPreferencesService.Configuration.WeighingThreshold.ToString();
    }

    private void LoadWeightUnits()
    {
      var result = Enum.GetValues(typeof(UnitType)).Cast<UnitType>()
        .Select(c => new LocalizedEnum
        {
          Enum = c
        });

      WeightUnits.Clear();
      WeightUnits.AddRange(result);
      SelectedWeightUnit =
        WeightUnits.FirstOrDefault(c => (UnitType) c.Enum == _userPreferencesService.Configuration.UnitType);
    }

    private void LoadCameraPictureOnPrintoutOptions()
    {
      var result = Enum.GetValues(typeof(YesNo)).Cast<YesNo>()
        .Select(c => new LocalizedEnum
        {
          Enum = c
        });


      CameraPictureOnPrintoutOptions.Clear();
      CameraPictureOnPrintoutOptions.AddRange(result);
      SelectedCameraPictureOnPrintoutOption = CameraPictureOnPrintoutOptions.FirstOrDefault(c =>
        (YesNo) c.Enum == _userPreferencesService.Configuration.IncludeImagesOnPrintouts);
    }

    private void LoadPrintAfterWeighingMeasurementOptions()
    {
      var result = Enum.GetValues(typeof(YesNoAsk)).Cast<YesNoAsk>()
        .Select(c => new LocalizedEnum
        {
          Enum = c
        });

      PrintAfterWeighingMeasurementOptions.Clear();
      PrintAfterWeighingMeasurementOptions.AddRange(result);
      SelectedPrintAfterWeighingMeasurementOption = PrintAfterWeighingMeasurementOptions.FirstOrDefault(c =>
        (YesNoAsk) c.Enum == _userPreferencesService.Configuration.PrintWeighingReceiptAfterMeasurement);
    }

    private void LoadDoubleCopyOptions()
    {
      var result = Enum.GetValues(typeof(YesNo)).Cast<YesNo>()
        .Select(c => new LocalizedEnum
        {
          Enum = c
        });

      DoubleCopyOptions.Clear();
      DoubleCopyOptions.AddRange(result);
      SelectedDoubleCopyOption = DoubleCopyOptions.FirstOrDefault(c =>
        (YesNo)c.Enum == _userPreferencesService.Configuration.DoubleCopy);
    }

    protected override void DefineValidationRules()
    {
      InvalidateProperty(() => WeighingThreshold).When(() => !IsDecimal(WeighingThreshold))
        .WithMessageKey("InvalidNumber").ButIgnoreOnLoad();
      InvalidateProperty(() => WeighingThreshold).When(() => !IsGreaterOrEqualThan(WeighingThreshold, 0) && !string.IsNullOrEmpty(WeighingThreshold))
        .WithMessageKey("MustBeGreaterOrEqualThan", 0).ButIgnoreOnLoad();
    }
  }
}
