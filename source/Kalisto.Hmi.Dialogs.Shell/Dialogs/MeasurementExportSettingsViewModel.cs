﻿using Kalisto.Backend.Operations.Shell.Queries;
using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Hmi.Core;
using Kalisto.Hmi.Core.Communication;
using Kalisto.Hmi.Core.Services;
using Kalisto.Localization;
using Kalisto.Mailing.Notifications.Configuration;
using PropertyChanged;
using System.Linq;
using Kalisto.Localization.Messages;
using Kalisto.Configuration.MeasurementExportSettings;

namespace Kalisto.Hmi.Dialogs.Shell.Dialogs
{
  [AddINotifyPropertyChangedInterface]
  public class MeasurementExportSettingsViewModel : ModalViewModelBase<MeasurementExportSettingsViewModel>
  {
    private readonly ILocalizer<Messages> _localizer;

    public sealed override string DialogNameKey { get; set; }

    public bool MeasurementExportEnabled { get; set; }

    public MeasurementExportSettingsViewModel(IDialogService dialogService, IAsyncProgressService asyncProgressService,
      IMessageBuilder messageBuilder, IFrontendMessageBridge messageBridge, ClientInfo clientInfo)
      : base(dialogService, asyncProgressService, messageBuilder, messageBridge, clientInfo)
    {
      _localizer = new LocalizationManager<Messages>();
      DialogNameKey = "MeasurementExportSettingsDialogTitle";
    }

    protected override void DefineValidationRules()
    {
    }
  }
}
