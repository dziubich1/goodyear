﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Kalisto.Hmi.Dialogs.Shell.Controls
{
  /// <summary>
  /// Interaction logic for WeighingPanelControl.xaml
  /// </summary>
  public partial class WeighingPanelControl : UserControl
  {
    public WeighingPanelControl()
    {
      InitializeComponent();
    }

    public static readonly DependencyProperty PerformSingleWeighingProperty =
      DependencyProperty.Register("PerformSingleWeighing", typeof(ICommand),
        typeof(WeighingPanelControl), new PropertyMetadata(null));

    public ICommand PerformSingleWeighing
    {
      get => (ICommand)GetValue(PerformSingleWeighingProperty);
      set => SetValue(PerformSingleWeighingProperty, value);
    }

    public static readonly DependencyProperty PerformSingleWeighingTextProperty =
      DependencyProperty.Register("PerformSingleWeighingText", typeof(string),
        typeof(WeighingPanelControl), new PropertyMetadata(null));

    public string PerformSingleWeighingText
    {
      get => (string)GetValue(PerformSingleWeighingTextProperty);
      set => SetValue(PerformSingleWeighingTextProperty, value);
    }

    public static readonly DependencyProperty PerformFirstDoubleWeighingProperty =
      DependencyProperty.Register("PerformFirstDoubleWeighing", typeof(ICommand),
        typeof(WeighingPanelControl), new PropertyMetadata(null));

    public ICommand PerformFirstDoubleWeighing
    {
      get => (ICommand)GetValue(PerformFirstDoubleWeighingProperty);
      set => SetValue(PerformFirstDoubleWeighingProperty, value);
    }

    public static readonly DependencyProperty PerformFirstDoubleWeighingTextProperty =
      DependencyProperty.Register("PerformFirstDoubleWeighingText", typeof(string),
        typeof(WeighingPanelControl), new PropertyMetadata(null));

    public string PerformFirstDoubleWeighingText
    {
      get => (string)GetValue(PerformFirstDoubleWeighingTextProperty);
      set => SetValue(PerformFirstDoubleWeighingTextProperty, value);
    }

    public static readonly DependencyProperty PerformSecondDoubleWeighingProperty =
      DependencyProperty.Register("PerformSecondDoubleWeighing", typeof(ICommand),
        typeof(WeighingPanelControl), new PropertyMetadata(null));

    public ICommand PerformSecondDoubleWeighing
    {
      get => (ICommand)GetValue(PerformSecondDoubleWeighingProperty);
      set => SetValue(PerformSecondDoubleWeighingProperty, value);
    }

    public static readonly DependencyProperty PerformSecondDoubleWeighingTextProperty =
      DependencyProperty.Register("PerformSecondDoubleWeighingText", typeof(string),
        typeof(WeighingPanelControl), new PropertyMetadata(null));

    public string PerformSecondDoubleWeighingText
    {
      get => (string)GetValue(PerformSecondDoubleWeighingTextProperty);
      set => SetValue(PerformSecondDoubleWeighingTextProperty, value);
    }

    public static readonly DependencyProperty PerformTaringProperty =
      DependencyProperty.Register("PerformTaring", typeof(ICommand),
        typeof(WeighingPanelControl), new PropertyMetadata(null));

    public ICommand PerformTaring
    {
      get => (ICommand)GetValue(PerformTaringProperty);
      set => SetValue(PerformTaringProperty, value);
    }

    public static readonly DependencyProperty PerformTaringTextProperty =
      DependencyProperty.Register("PerformTaringText", typeof(string),
        typeof(WeighingPanelControl), new PropertyMetadata(null));

    public string PerformTaringText
    {
      get => (string)GetValue(PerformTaringTextProperty);
      set => SetValue(PerformTaringTextProperty, value);
    }
  }
}
