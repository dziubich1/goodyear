﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Kalisto.Backend.Operations.Users.Commands.Interfaces;
using Kalisto.Configuration.Permissions;
using Kalisto.Hmi.Core;
using Kalisto.Hmi.Core.Commands;
using Kalisto.Hmi.Core.Services;
using Kalisto.Hmi.Dialogs.Users.Dialogs;
using Kalisto.Hmi.Dialogs.Users.Models;
using Kalisto.Hmi.Dialogs.Users.Services;
using Kalisto.Localization;
using Kalisto.Localization.Messages;
using MahApps.Metro.Controls.Dialogs;
using PropertyChanged;

namespace Kalisto.Hmi.Dialogs.Users.ViewModels
{
  [AddINotifyPropertyChangedInterface]
  public class UsersViewModel : DataGridViewModel<UserModel>
  {
    private readonly ILocalizer<Messages> _localizer;

    private readonly IUserService _userService;

    public override string DialogTitleKey => "Users";

    public ICommand AddItemCommand { get; set; }

    public ICommand EditItemCommand { get; set; }

    public ICommand DeleteItemCommand { get; set; }

    public ICommand SyncItemsCommand { get; set; }

    public ICommand GridRowDoubleClickCommand { get; set; }

    public ObservableCollectionEx<UserModel> Users { get; set; }

    public UserModel SelectedUser { get; set; }

    protected override Type[] AssociatedCommandTypes => new[] { typeof(IUserCommand) };

    public UsersViewModel(IDialogService dialogService, IAuthorizationService authorizationService, IUserService userService)
      : base(dialogService, authorizationService)
    {
      _localizer = new LocalizationManager<Messages>();
      _userService = userService;

      AddItemCommand = new DelegateCommand(AddUser);
      EditItemCommand = new DelegateCommand(EditUser);
      DeleteItemCommand = new DelegateCommand(DeleteUser);
      SyncItemsCommand = new DelegateCommand(SyncUsers);
      GridRowDoubleClickCommand = new DelegateCommand(EditUser);
      Users = new ObservableCollectionEx<UserModel>();
    }

    protected override ObservableCollectionEx<UserModel> GetItemsSource()
    {
      return Users;
    }

    protected override string GetEditRuleName()
    {
      return PermissionRules.UserManagementRule;
    }

    public override async Task RefreshDataAsync()
    {
      await base.RefreshDataAsync();

      await LoadUsers();
    }

    private async Task LoadUsers()
    {
      var result = await _userService.GetAllUsersAsync();
      if (!result.Succeeded)
        return;

      Users.Clear();
      Users.AddRange(result.Response);
    }

    private async void AddUser(object parameter)
    {
      await DialogService.ShowModalDialogAsync<AddOrUpdateUserModalDialogViewModel>(
      vm =>
      {
        vm.DialogNameKey = "AddUser";
      },
      async viewModel =>
      {
        var result = await _userService.AddUserAsync(viewModel.Username, viewModel.Password, viewModel.RoleNumber);
        return result.Succeeded;
      });
    }

    private async void EditUser(object parameter)
    {
      var selectedUser = (parameter as UserModel) ?? SelectedUser;
      if (selectedUser == null)
      {
        await DialogService.ShowMessageAsync(_localizer.Localize(() => Messages.Error),
          _localizer.Localize(() => Messages.SelectUserToUpdate), MessageDialogType.Ok);
        return;
      }

      await DialogService.ShowModalDialogAsync<AddOrUpdateUserModalDialogViewModel>(vm =>
      {
        vm.DialogNameKey = "EditUser";
        vm.Username = selectedUser.Name;
        vm.RoleNumber = selectedUser.RoleNumber;
        vm.RevealPassword = true;
        vm.ValidateOnLoad = true;
      },
      async viewModel =>
      {
        var result = await _userService.UpdateUserAsync(selectedUser.Id, viewModel.Username, viewModel.Password,
          viewModel.RoleNumber, viewModel.PasswordHasChanged);
        return result.Succeeded;
      });
    }

    private async void DeleteUser(object parameter)
    {
      if (SelectedUser == null)
      {
        await DialogService.ShowMessageAsync(_localizer.Localize(() => Messages.Error),
          _localizer.Localize(() => Messages.SelectUserToDelete), MessageDialogType.Ok);
        return;
      }

      var dlgResult = await DialogService.ShowMessageAsync(_localizer.Localize(() => Messages.Attention),
        _localizer.Localize(() => Messages.DeleteUserQuestion),
        MessageDialogType.YesNo);

      if (dlgResult != MessageDialogResult.Affirmative)
        return;

      await _userService.DeleteUserAsync(SelectedUser.Id);
    }

    private async void SyncUsers(object obj)
    {
      await LoadUsers();
    }
  }
}
