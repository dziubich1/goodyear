﻿namespace Kalisto.Hmi.Dialogs.Users.Models
{
  public class UserModel
  {
    public long Id { get; set; }

    public int OrdinalId { get; set; }

    public string Name { get; set; }

    public int RoleNumber { get; set; }
  }
}
