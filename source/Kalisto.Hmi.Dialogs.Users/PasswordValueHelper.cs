﻿using System;
using System.Linq;

namespace Kalisto.Hmi.Dialogs.Users
{
  public class PasswordValueHelper
  {
    private readonly string _generatedPassword;

    public PasswordValueHelper()
    {
      _generatedPassword = Guid.NewGuid().ToString().Take(8)
        .Select(c => c.ToString()).Aggregate((a, b) => a + b);
    }

    public string GetGeneratedPassword()
    {
      return _generatedPassword;
    }

    public bool PasswordHasChanged(string password)
    {
      return password != _generatedPassword;
    }
  }
}
