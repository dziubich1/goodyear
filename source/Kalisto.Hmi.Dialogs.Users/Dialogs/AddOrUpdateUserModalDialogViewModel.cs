﻿using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Hmi.Core;
using Kalisto.Hmi.Core.Communication;
using Kalisto.Hmi.Core.Services;
using PropertyChanged;

namespace Kalisto.Hmi.Dialogs.Users.Dialogs
{
  [AddINotifyPropertyChangedInterface]
  public class AddOrUpdateUserModalDialogViewModel : ModalViewModelBase<AddOrUpdateUserModalDialogViewModel>
  {
    private readonly PasswordValueHelper _passwordValueHelper;

    public override string DialogNameKey { get; set; }

    public string Username { get; set; }

    public string Password { get; set; }

    public string Repassword { get; set; }

    public int RoleNumber { get; set; }

    public bool RevealPassword { get; set; }

    public bool PasswordHasChanged { get; set; }

    public AddOrUpdateUserModalDialogViewModel(IDialogService dialogService, IAsyncProgressService asyncProgressService,
      IMessageBuilder messageBuilder, IFrontendMessageBridge messageBridge, ClientInfo clientInfo)
      : base(dialogService, asyncProgressService, messageBuilder, messageBridge, clientInfo)
    {
      _passwordValueHelper = new PasswordValueHelper();
    }

    public void OnPasswordChanged()
    {
      if (_passwordValueHelper.PasswordHasChanged(Password))
      {
        if (RevealPassword && !PasswordHasChanged)
          Repassword = string.Empty;
        PasswordHasChanged = true;
      }
      else PasswordHasChanged = false;
    }

    protected override void OnViewReady(object view)
    {
      base.OnViewReady(view);

      if (!RevealPassword)
        return;

      Password = _passwordValueHelper.GetGeneratedPassword();
      Repassword = Password;
    }

    protected override void DefineValidationRules()
    {
      InvalidateProperty(() => Username).When(() => string.IsNullOrEmpty(Username))
        .WithMessageKey("Required").ButIgnoreOnLoad();

      InvalidateProperty(() => Username).When(() => Username.Length < 3)
        .WithMessageKey("TooShort").ButIgnoreOnLoad();

      InvalidateProperty(() => Password).When(() => string.IsNullOrEmpty(Password))
        .WithMessageKey("Required").ButIgnoreOnLoad();

      InvalidateProperty(() => Password).When(() => Password.Length < 3)
        .WithMessageKey("TooShort").ButIgnoreOnLoad();

      InvalidateProperty(() => Repassword).When(() => Password != Repassword)
        .WithMessageKey("PasswordsNotMatching").ButIgnoreOnLoad();
    }
  }
}
