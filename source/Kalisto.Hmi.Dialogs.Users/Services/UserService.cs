﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.Operations.Users.Commands;
using Kalisto.Backend.Operations.Users.Queries;
using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Hmi.Core.Communication;
using Kalisto.Hmi.Core.Services;
using Kalisto.Hmi.Dialogs.Users.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kalisto.Hmi.Dialogs.Users.Services
{
  public interface IUserService : IBackendOperationService
  {
    Task<RequestResult<List<UserModel>>> GetAllUsersAsync();

    Task<RequestResult<long>> AddUserAsync(string username, string password, int roleNumber);

    Task<RequestResult<long>> UpdateUserAsync(long id, string username, string password, int roleNumber, bool passwordChanged);

    Task<RequestResult<long>> DeleteUserAsync(long id);
  }

  public class UserService : BackendOperationService, IUserService
  {
    public UserService(ClientInfo clientInfo, IMessageBuilder messageBuilder, IFrontendMessageBridge messageBridge, IDialogService dialogService)
      : base(clientInfo, messageBuilder, messageBridge, dialogService)
    {
    }

    public async Task<RequestResult<List<UserModel>>> GetAllUsersAsync()
    {
      var message = MessageBuilder.CreateNew<GetAllUsersQuery>()
        .BasedOn(ClientInfo)
        .Build();

      var result = await MessageBridge.SendMessageWithResultAsync<List<User>>(message);
      return new RequestResult<List<UserModel>>
      {
        Succeeded = result?.Succeeded ?? false,
        Response = result?.Response?.Select((c, index) => new UserModel
        {
          Id = c.Id,
          OrdinalId = index + 1,
          Name = c.Name,
          RoleNumber = c.Role.Type
        }).ToList() ?? new List<UserModel>()
      };
    }

    public async Task<RequestResult<long>> AddUserAsync(string username, string password, int roleNumber)
    {
      var message = MessageBuilder.CreateNew<CreateUserCommand>()
        .BasedOn(ClientInfo)
        .WithProperty(c => c.Username, username)
        .WithProperty(c => c.Password, password)
        .WithProperty(c => c.RoleNumber, roleNumber)
        .Build();

      var result = await MessageBridge.SendMessageWithResultAsync<long>(message);
      return result ?? RequestResult<long>.Default;
    }

    public async Task<RequestResult<long>> UpdateUserAsync(long id, string username, string password, int roleNumber,
      bool passwordChanged)
    {
      var message = MessageBuilder.CreateNew<UpdateUserCommand>()
        .BasedOn(ClientInfo)
        .WithProperty(c => c.UserId, id)
        .WithProperty(c => c.Username, username)
        .WithProperty(c => c.Password, password)
        .WithProperty(c => c.RoleNumber, roleNumber)
        .WithProperty(c => c.PasswordChanged, passwordChanged)
        .Build();

      var result = await MessageBridge.SendMessageWithResultAsync<long>(message);
      return result ?? RequestResult<long>.Default;
    }

    public async Task<RequestResult<long>> DeleteUserAsync(long id)
    {
      var message = MessageBuilder.CreateNew<DeleteUserCommand>()
        .BasedOn(ClientInfo)
        .WithProperty(c => c.UserId, id)
        .Build();

      var result = await MessageBridge.SendMessageWithResultAsync<long>(message);
      return result ?? RequestResult<long>.Default;
    }
  }
}
