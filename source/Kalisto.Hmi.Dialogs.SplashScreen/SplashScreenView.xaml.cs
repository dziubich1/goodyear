﻿using Kalisto.Communication.Core.Jobs.Base;
using System;
using System.Windows;

namespace Kalisto.Hmi.Dialogs.SplashScreen
{
  /// <summary>
  /// Interaction logic for SplashScreenView.xaml
  /// </summary>
  public partial class SplashScreenView : Window
  {
    private readonly Action _onShowAction;

    public SplashScreenView(IInitializationJobSchedulerInfo jobSchedulerInfo, Action onShowAction)
    {
      InitializeComponent();
      _onShowAction = onShowAction;

      var splashScreenViewModel = new SplashScreenViewModel(jobSchedulerInfo);
      splashScreenViewModel.InitializationCompleted += OnInitializationCompleted;
      splashScreenViewModel.InitializationFailed += OnInitializationFailed;
      DataContext = splashScreenViewModel;
    }

    protected override void OnContentRendered(EventArgs e)
    {
      base.OnContentRendered(e);

      _onShowAction?.Invoke();
    }

    private void OnInitializationCompleted()
    {
      DialogResult = true;
      Close();
    }

    private void OnInitializationFailed()
    {
      DialogResult = false;
      Close();
    }
  }
}
