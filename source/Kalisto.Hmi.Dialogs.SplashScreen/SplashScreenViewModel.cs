﻿using Kalisto.Communication.Core.Jobs.Base;
using Kalisto.Localization;
using Kalisto.Localization.Common;
using Kalisto.Localization.Exceptions;
using PropertyChanged;
using System;
using System.Windows;

namespace Kalisto.Hmi.Dialogs.SplashScreen
{
  [AddINotifyPropertyChangedInterface]
  public class SplashScreenViewModel
  {
    private readonly ILocalizer<Common> _localizer;
    private readonly ILocalizer<Exceptions> _errorLocalizer;

    public string StatusInformation { get; set; }

    public event Action InitializationCompleted;
    public event Action InitializationFailed;

    public SplashScreenViewModel(IInitializationJobSchedulerInfo jobSchedulerInfo)
    {
      jobSchedulerInfo.InitializationProgressChanged += OnInitializationJobProgressChanged;
      jobSchedulerInfo.InitializationJobExceptionOccurred += OnInitializationJobExceptionOccurred;
      jobSchedulerInfo.InitializationCompleted += OnInitializationCompleted;

      _localizer = new LocalizationManager<Common>();
      _errorLocalizer = new LocalizationManager<Exceptions>();
      StatusInformation = _localizer.Localize(() => Common.InitializationInProgress);
    }

    private void OnInitializationCompleted(bool errorOccurred)
    {
      InitializationCompleted?.Invoke();
    }

    private void OnInitializationJobExceptionOccurred(IInitializationJobInfo jobInfo)
    {
      if (string.IsNullOrEmpty(jobInfo.FailureCode))
      {
        MessageBox.Show(jobInfo.LocalizedExceptionMessage, _errorLocalizer.Localize(() => Exceptions.Error),
          MessageBoxButton.OK, MessageBoxImage.Error);
      }
      else
      {
        MessageBox.Show(_errorLocalizer.Localize(jobInfo.FailureCode), _errorLocalizer.Localize(() => Exceptions.Error),
          MessageBoxButton.OK, MessageBoxImage.Error);
      }

      InitializationFailed?.Invoke();
    }

    private void OnInitializationJobProgressChanged(int progress, IInitializationJobInfo jobInfo)
    {
      StatusInformation = $"{jobInfo.LocalizedMessage} ({progress}%)";
    }
  }
}
