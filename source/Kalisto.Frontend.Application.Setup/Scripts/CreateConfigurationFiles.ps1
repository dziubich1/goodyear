﻿param (
	$assemblyName = "Kalisto.Setup",
    $hostName = "127.0.0.1",
    $port = 5672,
    $userName = "admin",
    $password = "admin",
		$dllLocationBase = "C:\Program Files (x86)\Kalisto"
)
$configurationDll = $dllLocationBase + "Kalisto.Configuration.dll"
$frontendConfigurationDll = $dllLocationBase + "Kalisto.Frontend.dll"
$communicationRabbitMqDll = $dllLocationBase + "Kalisto.Communication.RabbitMq.dll"

[Reflection.Assembly]::LoadFrom($configurationDll)
[Reflection.Assembly]::LoadFrom($frontendConfigurationDll)
[Reflection.Assembly]::LoadFrom($communicationRabbitMqDll)

$configurationManager = new-object Kalisto.Configuration.AppDataConfigurationManager -ArgumentList "Kalisto", $assemblyName
$frontendConfiguration = new-object Kalisto.Frontend.FrontendConfiguration
$defaultConfiguration = $frontendConfiguration.GetDefaultConfiguration()
$defaultConfiguration.RabbitMqConnection.Hostname = $hostName
$defaultConfiguration.RabbitMqConnection.Port = $port
$defaultConfiguration.RabbitMqConnection.Username = $userName
$defaultConfiguration.RabbitMqConnection.Password = $password

$assemblyName
$dllLocationBase
$defaultConfiguration.RabbitMqConnection
$configurationManager.Save($defaultConfiguration)