﻿using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Hmi.Core;
using Kalisto.Hmi.Core.Communication;
using Kalisto.Hmi.Core.Services;
using PropertyChanged;

namespace Kalisto.Hmi.Dialogs.Drivers.Dialogs
{
  [AddINotifyPropertyChangedInterface]
  public class AddOrUpdateDriverModalDialogViewModel : ModalViewModelBase<AddOrUpdateDriverModalDialogViewModel>
  {
    public override string DialogNameKey { get; set; }

    public string Identifier { get; set; }

    public string PhoneNumber { get; set; }

    public AddOrUpdateDriverModalDialogViewModel(IDialogService dialogService, IAsyncProgressService asyncProgressService,
    IMessageBuilder messageBuilder, IFrontendMessageBridge messageBridge, ClientInfo clientInfo)
      : base(dialogService, asyncProgressService, messageBuilder, messageBridge, clientInfo)
    {
    }

    protected override void DefineValidationRules()
    {
      const int MIN_IDENTIFIER_LENGTH = 3;

      InvalidateProperty(() => Identifier).When(() => string.IsNullOrEmpty(Identifier))
        .WithMessageKey("Required").ButIgnoreOnLoad();

      InvalidateProperty(() => Identifier).When(() => Identifier.Length < MIN_IDENTIFIER_LENGTH)
        .WithMessageKey("TooShort").ButIgnoreOnLoad();
    }
  }
}
