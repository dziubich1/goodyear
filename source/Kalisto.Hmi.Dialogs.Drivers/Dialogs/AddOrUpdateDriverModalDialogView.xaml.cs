﻿using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;

namespace Kalisto.Hmi.Dialogs.Drivers.Dialogs
{
  /// <summary>
  /// Interaction logic for AddOrUpdateDialogModalDialogView.xaml
  /// </summary>
  public partial class AddOrUpdateDriverModalDialogView : MetroWindow
  {
    public AddOrUpdateDriverModalDialogView()
    {
      InitializeComponent();
    }
  }
}
