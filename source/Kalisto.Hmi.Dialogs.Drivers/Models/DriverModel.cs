﻿namespace Kalisto.Hmi.Dialogs.Drivers.Models
{
  public class DriverModel
  {
    public long Id { get; set; }

    public int OrdinalId { get; set; }

    public string Identifier { get; set; }

    public string PhoneNumber { get; set; }
  }
}
