﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.Operations.Drivers.Commands;
using Kalisto.Backend.Operations.Drivers.Queries;
using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Hmi.Core.Communication;
using Kalisto.Hmi.Core.Services;
using Kalisto.Hmi.Dialogs.Drivers.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kalisto.Hmi.Dialogs.Drivers.Services
{
  public interface IDriverService : IBackendOperationService
  {
    Task<RequestResult<List<DriverModel>>> GetAllDriversAsync();

    Task<RequestResult<long>> AddDriverAsync(string identifier, string phoneNumber);

    Task<RequestResult<long>> DeleteDriverAsync(long id);

    Task<RequestResult<long>> UpdateDriverAsync(long id, string identifier, string phoneNumber);
  }

  public class DriverService : BackendOperationService, IDriverService
  {
    public DriverService(ClientInfo clientInfo, IMessageBuilder messageBuilder, IFrontendMessageBridge messageBridge, IDialogService dialogService)
      : base(clientInfo, messageBuilder, messageBridge, dialogService)
    {
    }

    public async Task<RequestResult<List<DriverModel>>> GetAllDriversAsync()
    {
      var message = MessageBuilder.CreateNew<GetAllDriversQuery>()
        .BasedOn(ClientInfo)
        .Build();

      var result = await MessageBridge.SendMessageWithResultAsync<List<Driver>>(message);
      return new RequestResult<List<DriverModel>>
      {
        Succeeded = result?.Succeeded ?? false,
        Response = result?.Response?.Select((driver, index) => new DriverModel
        {
          Id = driver.Id,
          OrdinalId = index + 1,
          Identifier = driver.Identifier,
          PhoneNumber = driver.PhoneNumber
        }).ToList() ?? new List<DriverModel>()
      };
    }

    public async Task<RequestResult<long>> AddDriverAsync(string identifier, string phoneNumber)
    {
      var message = MessageBuilder.CreateNew<CreateDriverCommand>()
        .BasedOn(ClientInfo)
        .WithProperty(c => c.DriverIdentifier, identifier)
        .WithProperty(c => c.DriverPhoneNumber, phoneNumber)
        .Build();

      var result = await MessageBridge.SendMessageWithResultAsync<long>(message);
      return result ?? RequestResult<long>.Default;
    }

    public async Task<RequestResult<long>> DeleteDriverAsync(long id)
    {
      var message = MessageBuilder.CreateNew<DeleteDriverCommand>()
         .BasedOn(ClientInfo)
         .WithProperty(c => c.DriverId, id)
         .Build();

      var result = await MessageBridge.SendMessageWithResultAsync<long>(message);
      return result ?? RequestResult<long>.Default;
    }

    public async Task<RequestResult<long>> UpdateDriverAsync(long id, string identifier, string phoneNumber)
    {
      var message = MessageBuilder.CreateNew<UpdateDriverCommand>()
        .BasedOn(ClientInfo)
        .WithProperty(c => c.DriverId, id)
        .WithProperty(c => c.DriverIdentifier, identifier)
        .WithProperty(c => c.DriverPhoneNumber, phoneNumber)
        .Build();

      var result = await MessageBridge.SendMessageWithResultAsync<long>(message);
      return result ?? RequestResult<long>.Default;
    }
  }
}
