﻿using System;
using Kalisto.Configuration.Permissions;
using Kalisto.Hmi.Core;
using Kalisto.Hmi.Core.Commands;
using Kalisto.Hmi.Core.Services;
using Kalisto.Hmi.Dialogs.Drivers.Dialogs;
using Kalisto.Hmi.Dialogs.Drivers.Models;
using Kalisto.Hmi.Dialogs.Drivers.Services;
using MahApps.Metro.Controls.Dialogs;
using PropertyChanged;
using System.Threading.Tasks;
using System.Windows.Input;
using Kalisto.Backend.Operations.Drivers.Commands.Interfaces;
using Kalisto.Localization;
using Kalisto.Localization.Messages;

namespace Kalisto.Hmi.Dialogs.Drivers.ViewModels
{
  [AddINotifyPropertyChangedInterface]
  public class DriversViewModel : DataGridViewModel<DriverModel>
  {
    private readonly ILocalizer<Messages> _localizer;

    private readonly IDriverService _driverService;

    public override string DialogTitleKey => "Drivers";

    public ICommand AddItemCommand { get; set; }

    public ICommand EditItemCommand { get; set; }

    public ICommand DeleteItemCommand { get; set; }

    public ICommand SyncItemsCommand { get; set; }

    public ObservableCollectionEx<DriverModel> Drivers { get; set; }

    public DriverModel SelectedDriver { get; set; }

    protected override Type[] AssociatedCommandTypes => new[] { typeof(IDriverCommand) };

    public DriversViewModel(IDialogService dialogService, IAuthorizationService authorizationService, IDriverService driverService)
      : base(dialogService, authorizationService)
    {
      _localizer = new LocalizationManager<Messages>();
      _driverService = driverService;

      AddItemCommand = new DelegateCommand(AddDriver);
      EditItemCommand = new DelegateCommand(EditItem);
      DeleteItemCommand = new DelegateCommand(DeleteDriver);
      SyncItemsCommand = new DelegateCommand(SyncDrivers);

      Drivers = new ObservableCollectionEx<DriverModel>();
    }

    protected override ObservableCollectionEx<DriverModel> GetItemsSource()
    {
      return Drivers;
    }

    protected override string GetEditRuleName()
    {
      return PermissionRules.DictionaryDataManagement;
    }

    public override async Task RefreshDataAsync()
    {
      await base.RefreshDataAsync();

      await LoadDrivers();
    }

    private async Task LoadDrivers()
    {
      var result = await _driverService.GetAllDriversAsync();
      if (!result.Succeeded)
      {
        return;
      }

      Drivers.Clear();
      Drivers.AddRange(result.Response);
    }

    private async void AddDriver(object parameter)
    {
      await DialogService.ShowModalDialogAsync<AddOrUpdateDriverModalDialogViewModel>(
        vm =>
        {
          vm.DialogNameKey = "AddDriver";
        },
        async viewModel =>
        {
          var result = await _driverService.AddDriverAsync(viewModel.Identifier, viewModel.PhoneNumber);
          return result.Succeeded;
        });
    }

    protected override async void EditItem(object parameter)
    {
      var selectedDriver = (parameter as DriverModel) ?? SelectedDriver;
      if (selectedDriver == null)
      {
        await DialogService.ShowMessageAsync(_localizer.Localize(() => Messages.Error),
          _localizer.Localize(() => Messages.SelectDriverToUpdate), MessageDialogType.Ok);
        return;
      }

      await DialogService.ShowModalDialogAsync<AddOrUpdateDriverModalDialogViewModel>(vm =>
      {
        vm.DialogNameKey = "EditDriver";
        vm.Identifier = selectedDriver.Identifier;
        vm.PhoneNumber = selectedDriver.PhoneNumber;
        vm.ValidateOnLoad = true;
      },
      async viewModel =>
      {
        var result = await _driverService.UpdateDriverAsync(selectedDriver.Id, viewModel.Identifier, viewModel.PhoneNumber);
        return result.Succeeded;
      });
    }

    private async void DeleteDriver(object parameter)
    {
      if (SelectedDriver == null)
      {
        await DialogService.ShowMessageAsync(_localizer.Localize(() => Messages.Error),
          _localizer.Localize(() => Messages.SelectDriverToDelete), MessageDialogType.Ok);
        return;
      }

      var dlgResult = await DialogService.ShowMessageAsync(_localizer.Localize(() => Messages.Attention),
        _localizer.Localize(() => Messages.DeleteDriverQuestion), MessageDialogType.YesNo);

      if (dlgResult != MessageDialogResult.Affirmative)
      {
        return;
      }

      await _driverService.DeleteDriverAsync(SelectedDriver.Id);
    }

    private async void SyncDrivers(object obj)
    {
      await LoadDrivers();
    }
  }
}
