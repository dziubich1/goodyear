﻿using Kalisto.Communication.Core.Messages;
using System;

namespace Kalisto.Backend.Operations.DbEventLogs.Queries
{
  [Serializable]
  public class GetDbEventLogsQuery: BackendDbOperationMessage
  {
    public DateTime FromUtc { get; set; }
    public DateTime ToUtc { get; set; }
  }
}
