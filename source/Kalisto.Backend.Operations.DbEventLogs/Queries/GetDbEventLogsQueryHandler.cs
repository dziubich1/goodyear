﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.Operations.DbEventLogs.Dto;
using Kalisto.Communication.Core.Messaging;
using System.Data.Entity;
using System.Linq;

namespace Kalisto.Backend.Operations.DbEventLogs.Queries
{
  public class GetDbEventLogsQueryHandler : QueryHandlerBase<GetDbEventLogsQuery>
  {
    public override object Execute(IDbContext context, GetDbEventLogsQuery query, string partnerId)
    {
      var logs = context.DbEventLogs
        .Where(l => l.EventDateUtc >= query.FromUtc)
        .Where(l => l.EventDateUtc <= query.ToUtc)
        .OrderByDescending(c => c.EventDateUtc)
        .AsNoTracking()
        .Select(s => new DbEventLogDto
        {
          TableName = s.TableName,
          EntityId = s.EntityId,
          EventType = s.EventType,
          Source = s.Source,
          EventDateUtc = s.EventDateUtc
        })
        .ToList();

      return logs;
    }
  }
}
