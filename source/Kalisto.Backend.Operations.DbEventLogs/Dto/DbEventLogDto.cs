﻿using Kalisto.Backend.DataAccess.Model.Enum;
using System;

namespace Kalisto.Backend.Operations.DbEventLogs.Dto
{
  [Serializable]
  public class DbEventLogDto
  {
    public long EntityId { get; set; }

    public string TableName { get; set; }

    public DbEventType EventType { get; set; }

    public string Source { get; set; }

    public DateTime EventDateUtc { get; set; }
  }
}
