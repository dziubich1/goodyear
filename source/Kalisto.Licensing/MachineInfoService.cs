﻿using System;
using System.Management;

namespace Kalisto.Licensing
{
  public interface IMachineInfo
  {
    string GetMachineUniqueId();
  }

  public class MachineInfoService : IMachineInfo
  {
    public string GetMachineUniqueId()
    {
      var motherBoardSearcher = new ManagementObjectSearcher("Select * From Win32_BaseBoard");
      foreach (var searcher in motherBoardSearcher.Get())
        return searcher["SerialNumber"].ToString();

      throw new InvalidOperationException();
    }
  }
}
