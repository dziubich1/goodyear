﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kalisto.Licensing
{
  public enum LicenseType
  {
    Full, Demo, None
  }

  public class LicenseInfo
  {
    public LicenseType Type { get; set; }

    public int StationCount { get; set; }
  }
}
