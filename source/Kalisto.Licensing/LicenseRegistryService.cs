﻿using System;
using System.Globalization;
using Kalisto.Core;
using Kalisto.Logging;

namespace Kalisto.Licensing
{
  public interface ILicenseRegistryAccess
  {
    string GetLicenseKey();

    bool StoreLicenseKey(string licenseKey);

    DateTime? GetDemoExpirationDate();

    bool StoreDemoExpirationDate(DateTime expirationDate);
  }

  public class LicenseRegistryService : ILicenseRegistryAccess
  {
    private const string DatePattern = "yyyy-MM-dd HH:mm";

    private readonly ILogger _logger;

    private readonly ISystemRegistryReader _registryReader;

    private readonly ISystemRegistryWriter _registryWriter;

    public LicenseRegistryService(ILogger logger, ISystemRegistryReader registryReader,
      ISystemRegistryWriter registryWriter)
    {
      _logger = logger;
      _registryReader = registryReader;
      _registryWriter = registryWriter;
    }

    public string GetLicenseKey()
    {
      try
      {
        return _registryReader.GetRegistryValue("KALISTO", "LICENSE_KEY");
      }
      catch (Exception e)
      {
        _logger.Error($"Could not read license key. Given exception was thrown: {e.Message}");
        return string.Empty;
      }
    }

    public bool StoreLicenseKey(string licenseKey)
    {
      try
      {
        _registryWriter.StoreRegistryValue("KALISTO", "LICENSE_KEY", licenseKey);
        return true;
      }
      catch (Exception e)
      {
        _logger.Error($"Could not assign license key: {licenseKey}. Given exception was thrown: {e.Message}");
        return false;
      }
    }

    public DateTime? GetDemoExpirationDate()
    {
      try
      {
        var dateString = _registryReader.GetRegistryValue("KALISTO", "DEMO_EXP_DATE");
        if (string.IsNullOrEmpty(dateString))
          return null;

        return DateTime.ParseExact(dateString, DatePattern, CultureInfo.InvariantCulture);
      }
      catch (Exception e)
      {
        _logger.Error($"Could not read demo expiration date. Given exception was thrown: {e.Message}");
        return null;
      }
    }

    public bool StoreDemoExpirationDate(DateTime expirationDate)
    {
      try
      {
        var dateString = expirationDate.ToString(DatePattern, CultureInfo.InvariantCulture);
        _registryWriter.StoreRegistryValue("KALISTO", "DEMO_EXP_DATE", dateString);
        return true;
      }
      catch (Exception e)
      {
        _logger.Error($"Could not store demo expiration date. Given exception was thrown: {e.Message}");
        return false;
      }
    }
  }
}
