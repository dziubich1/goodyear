﻿using Kalisto.Logging;
using System;
using System.Linq;
using System.Text;

namespace Kalisto.Licensing
{
  public interface ILicenseInfo
  {
    string GetLicensePreKey(int stationCount);

    DateTime GetDemoExpirationDate();

    LicenseInfo GetLicenseInfo();

    bool IsLicenseKeyValid(string licenseKey);

    bool AssignLicenseKey(string licenseKey);
  }

  public class LicenseService : ILicenseInfo
  {
    private readonly ILogger _logger;

    private readonly IMachineInfo _machineInfo;

    private readonly ILicenseRegistryAccess _licenseRegistryAccess;

    public LicenseService(ILogger logger, IMachineInfo machineInfo, ILicenseRegistryAccess licenseRegistryAccess)
    {
      _logger = logger;
      _machineInfo = machineInfo;
      _licenseRegistryAccess = licenseRegistryAccess;
    }

    public string GetLicensePreKey(int stationCount)
    {
      var bytes = Encoding.UTF8.GetBytes($"{_machineInfo.GetMachineUniqueId()}-{stationCount}");
      return Convert.ToBase64String(bytes);
    }

    public DateTime GetDemoExpirationDate()
    {
      try
      {
        return _licenseRegistryAccess.GetDemoExpirationDate() ?? DateTime.Now.AddDays(-1);
      }
      catch (Exception e)
      {
        _logger.Error($"Could not read demo expiration date. Given exception was thrown: {e.Message}");
        return DateTime.Today.AddDays(-1);
      }
    }

    public LicenseInfo GetLicenseInfo()
    {
      var licenseInfo = new LicenseInfo
      {
        StationCount = 0,
        Type = LicenseType.None
      };

      var licenseKey = _licenseRegistryAccess.GetLicenseKey();
      if (string.IsNullOrEmpty(licenseKey))
        return GetDemoLicenseInfo();

      var licenseDecodedBytes = Convert.FromBase64String(licenseKey);
      var licenseDecodedString = Encoding.UTF8.GetString(licenseDecodedBytes);
      var licenseParts = licenseDecodedString.Split('-');
      if (licenseParts.Length < 3)
        return GetDemoLicenseInfo();

      var stationsCount = int.Parse(licenseParts.First());
      var machineId = GetMachineId(licenseDecodedString);
      var ack = licenseParts.Last();

      if (ack != "OK" || _machineInfo.GetMachineUniqueId() != machineId)
        return GetDemoLicenseInfo();

      licenseInfo.StationCount = stationsCount;
      licenseInfo.Type = LicenseType.Full;
      return licenseInfo;
    }

    public bool IsLicenseKeyValid(string licenseKey)
    {
      var licenseDecodedBytes = Convert.FromBase64String(licenseKey);
      var licenseDecodedString = Encoding.UTF8.GetString(licenseDecodedBytes);
      var licenseParts = licenseDecodedString.Split('-');
      if (licenseParts.Length < 3)
        return false;

      var stationsCount = licenseParts.First();
      var ack = licenseParts.Last();
      var machineId = GetMachineId(licenseDecodedString);
      if (ack != "OK" || _machineInfo.GetMachineUniqueId() != machineId)
        return false;

      if (!int.TryParse(stationsCount, out _))
        return false;

      return true;
    }

    public bool AssignLicenseKey(string licenseKey)
    {
      try
      {
        _licenseRegistryAccess.StoreLicenseKey(licenseKey);
        return true;
      }
      catch (Exception e)
      {
        _logger.Error($"Could not assign license key. Given exception was thrown: {e.Message}");
        return false;
      }
    }

    private LicenseInfo GetDemoLicenseInfo()
    {
      var licenseInfo = new LicenseInfo
      {
        StationCount = 0,
        Type = LicenseType.None
      };

      var demoExpirationDate = _licenseRegistryAccess.GetDemoExpirationDate();
      if (demoExpirationDate == null)
      {
        _licenseRegistryAccess.StoreDemoExpirationDate(DateTime.Now.AddMonths(1));
        return GetDemoLicenseInfo();
      }

      if (DateTime.Now > demoExpirationDate.Value)
        return licenseInfo;

      licenseInfo.StationCount = 1;
      licenseInfo.Type = LicenseType.Demo;
      return licenseInfo;
    }

    private string GetMachineId(string decodedLicenseKey)
    {
      var licenseParts = decodedLicenseKey.Split('-');
      var stationsCount = licenseParts.First();
      var ack = licenseParts.Last();
      // remove stations count value with dash: xx-yyyyyyy-OK -> yyyyyyy-OK
      var x1 = decodedLicenseKey.Remove(0, stationsCount.Length + 1);
      // second remove - remove ack value with dash: yyyyyyy-OK -> yyyyyyy
      var x2 = x1.Remove(x1.Length - 3, 3);
      var machineId = x2;

      return machineId;
    }
  }
}
