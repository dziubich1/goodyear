﻿using System;
using System.Linq;
using Kalisto.Backend.DataAccess.Effort;
using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.DataAccess.Model.Logging;
using Kalisto.Backend.Operations.Users.Commands;
using Kalisto.Backend.Operations.Users.Exceptions;
using Kalisto.Core.Utils;
using Kalisto.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Kalisto.Backend.Operations.Users.UnitTests
{
  [TestClass]
  public class UpdateUserCommandHandlerTests
  {
    private IDbContext _dbContext;

    private readonly string _partnerId = "unknown";

    private Mock<IDbUpdateEventLogger> _dbEventLoggerMock;

    // component under tests
    private UpdateUserCommandHandler _updateUserCommandHandler;

    [TestInitialize]
    public void Initialize()
    {
      var effortConnection = Effort.DbConnectionFactory.CreateTransient();
      _dbContext = new EffortDbContext(effortConnection);

      _dbEventLoggerMock = new Mock<IDbUpdateEventLogger>();

      var logger = new Mock<ILogger>();

      _updateUserCommandHandler = new UpdateUserCommandHandler(_dbEventLoggerMock.Object, logger.Object);

      InitializeUserRoles();
    }

    [TestCleanup]
    public void CleanUp()
    {
      _dbContext.Dispose();
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void Execute_ShouldThrowArgumentException_WhenUserWithGivenIdDoesNotExist()
    {
      // Arrange
      var notExistingUserId = 5;
      var command = new UpdateUserCommand {UserId = notExistingUserId };

      // Act & Assert
      _updateUserCommandHandler.Execute(_dbContext, command, _partnerId);
    }

    [TestMethod]
    [ExpectedException(typeof(UserExistsException))]
    public void Execute_ShouldThrowUserExistsException_WhenNameOfCurrentUserHasChangedButUserWithSameNameAlreadyExists()
    {
      // Setup
      var testUserName = "test";
      _dbContext.Users.Add(new User { Name = testUserName }); // generated id: 1
      var currentUserName = "current";
      _dbContext.Users.Add(new User { Name = currentUserName }); // generated id: 2
      _dbContext.SaveChanges();

      // Arrange
      var command = new UpdateUserCommand { UserId = 2, Username = testUserName};

      // Act & Assert
      _updateUserCommandHandler.Execute(_dbContext, command, _partnerId);
    }

    [TestMethod]
    public void Execute_ShouldUpdatePassword_WhenPasswordHasChanged()
    {
      // Setup
      var currentUserName = "current";
      var password = "password";
      var hashedPassword = PasswordHasher.CreateHash(password);
      _dbContext.Users.Add(new User
      {
        Name = currentUserName,
        PasswordHash = hashedPassword,
        Role = new UserRole
        {
          Type = 1
        }
      }); // generated id: 1
      _dbContext.SaveChanges();

      // Arrange
      var command = new UpdateUserCommand
      {
        UserId = 1,
        Username = currentUserName,
        Password = "new_password",
        PasswordChanged = true
      };

      // Act
      _updateUserCommandHandler.Execute(_dbContext, command, _partnerId);

      // Assert
      var user = _dbContext.Users.Single();
      Assert.AreNotEqual(hashedPassword, user.PasswordHash);
    }

    [TestMethod]
    public void Execute_ShouldNotUpdatePassword_WhenPasswordHasNotChanged()
    {
      // Setup
      var currentUserName = "current";
      var password = "password";
      var hashedPassword = PasswordHasher.CreateHash(password);
      _dbContext.Users.Add(new User
      {
        Name = currentUserName,
        PasswordHash = hashedPassword,
        Role = new UserRole
        {
          Type = 1
        }
      }); // generated id: 1
      _dbContext.SaveChanges();

      // Arrange
      var command = new UpdateUserCommand
      {
        UserId = 1,
        Username = currentUserName,
        Password = "new_password",
        PasswordChanged = false
      };

      // Act
      _updateUserCommandHandler.Execute(_dbContext, command, _partnerId);

      // Assert
      var user = _dbContext.Users.Single();
      Assert.AreEqual(hashedPassword, user.PasswordHash);
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void Execute_ShouldThrowArgumentException_WhenUserRoleHasChangedButGivenRoleDoesNotExist()
    {
      // Setup
      var currentUserName = "current";
      var password = "password";
      _dbContext.Users.Add(new User
      {
        Name = currentUserName,
        PasswordHash = PasswordHasher.CreateHash(password),
        Role = new UserRole
        {
          Type = 1
        }
      }); // generated id: 1
      _dbContext.SaveChanges();

      // Arrange
      var command = new UpdateUserCommand
      {
        UserId = 1,
        Username = currentUserName,
        Password = password,
        PasswordChanged = false,
        RoleNumber = 5 // not existing role
      };

      // Act & Assert
      _updateUserCommandHandler.Execute(_dbContext, command, _partnerId);
    }

    [TestMethod]
    public void Execute_ShouldUpdateUser()
    {
      // Setup
      var currentUserName = "current";
      var currentPassword = "password";
      var currentRole = 1;
      _dbContext.Users.Add(new User
      {
        Name = currentUserName,
        PasswordHash = PasswordHasher.CreateHash(currentPassword),
        Role = new UserRole
        {
          Type = currentRole
        }
      }); // generated id: 1
      _dbContext.SaveChanges();

      // Arrange
      var newUserName = "new";
      var newRole = 2;
      var command = new UpdateUserCommand
      {
        UserId = 1,
        Username = newUserName,
        PasswordChanged = false,
        RoleNumber = newRole
      };

      // Act
      _updateUserCommandHandler.Execute(_dbContext, command, _partnerId);

      // Assert
      var user = _dbContext.Users.Single();
      Assert.AreEqual(newRole, user.Role.Type);
      Assert.AreEqual(newUserName, user.Name);
    }

    [TestMethod]
    public void Execute_ShouldReturnUpdatedUserId()
    {
      // Setup
      var currentUserName = "current";
      var currentPassword = "password";
      var currentRole = 1;
      _dbContext.Users.Add(new User
      {
        Name = currentUserName,
        PasswordHash = PasswordHasher.CreateHash(currentPassword),
        Role = new UserRole
        {
          Type = currentRole
        }
      }); // generated id: 1
      _dbContext.SaveChanges();

      // Arrange
      var newUserName = "new";
      var newRole = 2;
      var command = new UpdateUserCommand
      {
        UserId = 1,
        Username = newUserName,
        PasswordChanged = false,
        RoleNumber = newRole
      };

      // Act
      var resultId = _updateUserCommandHandler.Execute(_dbContext, command, _partnerId);

      // Assert
      var user = _dbContext.Users.Single();
      Assert.AreEqual(1L, user.Id);
      Assert.AreEqual(1L, resultId);
    }

    [TestMethod]
    public void Execute_ShouldCreateDbUpdateEventLog()
    {
      // Setup
      _dbEventLoggerMock.Setup(c => c.LogEvent(_partnerId, ctx => ctx.Users,
          It.IsAny<long>(), It.IsAny<User>()))
        .Verifiable();

      var currentUserName = "current";
      var currentPassword = "password";
      var currentRole = 1;
      _dbContext.Users.Add(new User
      {
        Name = currentUserName,
        PasswordHash = PasswordHasher.CreateHash(currentPassword),
        Role = new UserRole
        {
          Type = currentRole
        }
      }); // generated id: 1
      _dbContext.SaveChanges();

      // Arrange
      var newUserName = "new";
      var newRole = 2;
      var command = new UpdateUserCommand
      {
        UserId = 1,
        Username = newUserName,
        PasswordChanged = false,
        RoleNumber = newRole
      };

      // Act
      _updateUserCommandHandler.Execute(_dbContext, command, _partnerId);

      // Assert
      _dbEventLoggerMock.Verify(c => c.LogEvent(_partnerId, ctx => ctx.Users,
        It.IsAny<long>(), It.IsAny<User>(), It.IsAny<User>()), Times.Once);
    }

    private void InitializeUserRoles()
    {
      _dbContext.UserRoles.Add(new UserRole { Type = 0 });
      _dbContext.UserRoles.Add(new UserRole { Type = 1 });
      _dbContext.UserRoles.Add(new UserRole { Type = 2 });
      _dbContext.SaveChanges();
    }
  }
}
