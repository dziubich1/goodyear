﻿using System;
using System.Linq;
using Kalisto.Backend.DataAccess.Effort;
using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.DataAccess.Model.Logging;
using Kalisto.Backend.Operations.Users.Commands;
using Kalisto.Backend.Operations.Users.Exceptions;
using Kalisto.Core.Utils;
using Kalisto.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Kalisto.Backend.Operations.Users.UnitTests
{
  [TestClass]
  public class CreateUserCommandHandlerTests
  {
    private IDbContext _dbContext;

    private readonly string _partnerId = "unknown";

    private Mock<IDbInsertEventLogger> _dbEventLoggerMock;

    // component under tests
    private CreateUserCommandHandler _createUserCommandHandler;

    [TestInitialize]
    public void Initialize()
    {
      var effortConnection = Effort.DbConnectionFactory.CreateTransient();
      _dbContext = new EffortDbContext(effortConnection);

      _dbEventLoggerMock = new Mock<IDbInsertEventLogger>();

      var logger = new Mock<ILogger>();

      _createUserCommandHandler = new CreateUserCommandHandler(_dbEventLoggerMock.Object, logger.Object);

      InitializeUserRoles();
    }

    [TestCleanup]
    public void CleanUp()
    {
      _dbContext.Dispose();
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void Execute_ShouldThrowArgumentException_WhenGivenRoleDoesNotExist()
    {
      // Arrange
      var createUserCommand = new CreateUserCommand
      {
        RoleNumber = 4
      };

      // Act
      _createUserCommandHandler.Execute(_dbContext, createUserCommand, _partnerId);
    }

    [TestMethod]
    public void Execute_ShouldNotCreateUser_WhenGivenRoleDoesNotExist()
    {
      // Arrange
      var createUserCommand = new CreateUserCommand
      {
        RoleNumber = 4
      };

      try
      {
        // Act
        _createUserCommandHandler.Execute(_dbContext, createUserCommand, _partnerId);
      }
      catch (ArgumentException)
      {
        // Assert
        Assert.AreEqual(0, _dbContext.Users.Count());
      }
    }

    [TestMethod]
    [ExpectedException(typeof(UserExistsException))]
    public void Execute_ShouldThrowUserExistsException_WhenUserWithGivenNameAlreadyExists()
    {
      // Setup
      var testUserName = "test";
      _dbContext.Users.Add(new User { Name = testUserName });
      _dbContext.SaveChanges();

      // Arrange
      var createUserCommand = new CreateUserCommand
      {
        Username = testUserName,
        RoleNumber = 1
      };

      // Act & Assert
      _createUserCommandHandler.Execute(_dbContext, createUserCommand, _partnerId);
    }

    [TestMethod]
    public void Execute_ShouldNotCreateUser_WhenUserWithGivenNameAlreadyExists()
    {
      // Setup
      var testUserName = "test";
      _dbContext.Users.Add(new User { Name = testUserName });
      _dbContext.SaveChanges();
      var expectedUserCount = _dbContext.Users.Count();

      // Arrange
      var createUserCommand = new CreateUserCommand
      {
        Username = testUserName,
        RoleNumber = 1
      };

      try
      {
        // Act
        _createUserCommandHandler.Execute(_dbContext, createUserCommand, _partnerId);
      }
      catch (UserExistsException)
      {
        // Assert
        Assert.AreEqual(expectedUserCount, _dbContext.Users.Count());
      }
    }

    [TestMethod]
    public void Execute_ShouldCreateUser()
    {
      // Arrange
      var createUserCommand = new CreateUserCommand
      {
        Username = "test",
        Password = "some_password",
        RoleNumber = 1
      };

      // Act
      _createUserCommandHandler.Execute(_dbContext, createUserCommand, _partnerId);

      // Assert
      Assert.AreEqual(1, _dbContext.Users.Count(), "Users table should contain one user");

      var createdUser = _dbContext.Users.Single();
      Assert.AreEqual(1, createdUser.Id, "Id of created user should be auto generated");
      Assert.AreEqual(createUserCommand.Username, createdUser.Name, "Name of created user should be the same like in command");
      Assert.IsFalse(string.IsNullOrEmpty(createdUser.PasswordHash), "Password hash should not be empty");
      Assert.IsTrue(PasswordHasher.VerifyPassword(createUserCommand.Password, createdUser.PasswordHash),
        "Password hash should be correct");
      Assert.IsNotNull(createdUser.Role, "User role should be assigned properly");
      Assert.AreEqual(createUserCommand.RoleNumber, createdUser.Role.Type, "Role type of created user should be the same like in command");
    }

    [TestMethod]
    public void Execute_ShouldReturnIdentifierOfNewlyCreatedUser()
    {
      // Arrange
      var createUserCommand = new CreateUserCommand
      {
        Username = "test",
        Password = "some_password",
        RoleNumber = 1
      };

      // Act
      var id = _createUserCommandHandler.Execute(_dbContext, createUserCommand, _partnerId);

      // Assert
      var createdUser = _dbContext.Users.Single();
      Assert.AreEqual(1, createdUser.Id, "Id of created user should be auto generated and returned properly by Execute(...) method");
    }

    [TestMethod]
    public void Execute_ShouldCreateDbInsertEventLog()
    {
      // Setup
      _dbEventLoggerMock.Setup(c => c.LogEvent(_partnerId, ctx => ctx.Users,
          It.IsAny<long>(), It.IsAny<User>()))
        .Verifiable();

      // Arrange
      var createUserCommand = new CreateUserCommand
      {
        Username = "test",
        Password = "some_password",
        RoleNumber = 1
      };

      // Act
      _createUserCommandHandler.Execute(_dbContext, createUserCommand, "unknown");

      // Assert
      _dbEventLoggerMock.Verify(c => c.LogEvent(_partnerId, ctx => ctx.Users,
        It.IsAny<long>(), It.IsAny<User>()), Times.Once);
    }

    private void InitializeUserRoles()
    {
      _dbContext.UserRoles.Add(new UserRole { Type = 0 });
      _dbContext.UserRoles.Add(new UserRole { Type = 1 });
      _dbContext.UserRoles.Add(new UserRole { Type = 2 });
      _dbContext.SaveChanges();
    }
  }
}
