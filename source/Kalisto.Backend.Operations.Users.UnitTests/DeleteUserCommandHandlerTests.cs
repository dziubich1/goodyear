﻿using System;
using System.Linq;
using Kalisto.Backend.DataAccess.Effort;
using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.DataAccess.Model.Logging;
using Kalisto.Backend.Operations.Users.Commands;
using Kalisto.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Kalisto.Backend.Operations.Users.UnitTests
{
  [TestClass]
  public class DeleteUserCommandHandlerTests
  {
    private IDbContext _dbContext;

    private readonly string _partnerId = "unknown";

    private Mock<IDbDeleteEventLogger> _dbEventLoggerMock;

    // component under tests
    private DeleteUserCommandHandler _deleteUserCommandHandler;

    [TestInitialize]
    public void Initialize()
    {
      var effortConnection = Effort.DbConnectionFactory.CreateTransient();
      _dbContext = new EffortDbContext(effortConnection);

      _dbEventLoggerMock = new Mock<IDbDeleteEventLogger>();

      var logger = new Mock<ILogger>();

      _deleteUserCommandHandler = new DeleteUserCommandHandler(_dbEventLoggerMock.Object, logger.Object);

      InitializeUserRoles();
    }

    [TestCleanup]
    public void CleanUp()
    {
      _dbContext.Dispose();
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void Execute_ShouldThrowArgumentException_WhenUserWithGivenIdDoesNotExist()
    {
      // Setup
      var testUserName = "test";
      _dbContext.Users.Add(new User { Name = testUserName }); // generated id: 1
      _dbContext.SaveChanges();

      // Arrange
      var command = new DeleteUserCommand
      {
        UserId = 2 // not existing user id
      };

      // Act & Assert
      _deleteUserCommandHandler.Execute(_dbContext, command, _partnerId);
    }

    [TestMethod]
    public void Execute_ShouldRemoveUser()
    {
      // Setup
      var testUserName = "test";
      var role = _dbContext.UserRoles.First();
      _dbContext.Users.Add(new User { Name = testUserName, PasswordHash = "some_hash", Role = role }); // generated id: 1
      _dbContext.SaveChanges();

      // Arrange
      var command = new DeleteUserCommand
      {
        UserId = 1
      };

      // Act
      _deleteUserCommandHandler.Execute(_dbContext, command, _partnerId);

      // Assert
      Assert.AreEqual(0, _dbContext.Users.Count());
    }

    [TestMethod]
    public void Execute_ShouldNotRemoveAnyUserRole()
    {
      // Setup
      var totalRolesCount = _dbContext.UserRoles.Count();
      var testUserName = "test";
      var role = _dbContext.UserRoles.First();
      _dbContext.Users.Add(new User { Name = testUserName, PasswordHash = "some_hash", Role = role }); // generated id: 1
      _dbContext.SaveChanges();

      // Arrange
      var command = new DeleteUserCommand
      {
        UserId = 1
      };

      // Act
      _deleteUserCommandHandler.Execute(_dbContext, command, _partnerId);

      // Assert
      Assert.AreEqual(totalRolesCount, _dbContext.UserRoles.Count());
    }

    [TestMethod]
    public void Execute_ShouldCreateDbDeleteEventLog()
    {
      // Setup
      _dbEventLoggerMock.Setup(c => c.LogEvent(_partnerId, ctx => ctx.Users,
          It.IsAny<long>(), null, It.IsAny<User>()))
        .Verifiable();

      var testUserName = "test";
      var role = _dbContext.UserRoles.First();
      _dbContext.Users.Add(new User { Name = testUserName, PasswordHash = "some_hash", Role = role }); // generated id: 1
      _dbContext.SaveChanges();

      // Arrange
      var command = new DeleteUserCommand
      {
        UserId = 1
      };

      // Act
      _deleteUserCommandHandler.Execute(_dbContext, command, "unknown");

      // Assert
      _dbEventLoggerMock.Verify(c => c.LogEvent(_partnerId, ctx => ctx.Users,
        It.IsAny<long>(), null, It.IsAny<User>()), Times.Once);
    }

    private void InitializeUserRoles()
    {
      _dbContext.UserRoles.Add(new UserRole { Type = 0 });
      _dbContext.UserRoles.Add(new UserRole { Type = 1 });
      _dbContext.UserRoles.Add(new UserRole { Type = 2 });
      _dbContext.SaveChanges();
    }
  }
}
