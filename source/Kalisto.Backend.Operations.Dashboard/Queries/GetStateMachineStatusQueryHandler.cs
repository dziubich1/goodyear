﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.StateMachine;
using Kalisto.Communication.Core.Messaging;

namespace Kalisto.Backend.Operations.Dashboard.Queries
{
  public class GetStateMachineStatusQueryHandler : QueryHandlerBase<GetStateMachineStatusQuery>
  {
    private readonly IMeasurementStateMachineRegistry _stateMachineRegistry;

    public GetStateMachineStatusQueryHandler(IMeasurementStateMachineRegistry stateMachineRegistry)
    {
      _stateMachineRegistry = stateMachineRegistry;
    }

    public override object Execute(IDbContext context, GetStateMachineStatusQuery query, string partnerId)
    {
      return _stateMachineRegistry.IsRunning(query.DeviceId);
    }
  }
}
