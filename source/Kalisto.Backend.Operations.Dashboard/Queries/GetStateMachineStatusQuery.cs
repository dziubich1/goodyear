﻿using Kalisto.Communication.Core.Messages;

namespace Kalisto.Backend.Operations.Dashboard.Queries
{
  public class GetStateMachineStatusQuery : BackendDbOperationMessage
  {
    public int DeviceId { get; set; }
  }
}
