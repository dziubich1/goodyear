﻿using Kalisto.Backend.StateMachine;
using Kalisto.Communication.Core.Messaging;

namespace Kalisto.Backend.Operations.Dashboard.Commands
{
  public class ChangeAutoModeCommandHandler : CommandHandlerBase<ChangeAutoModeCommand>
  {
    private readonly IMeasurementStateMachineRegistry _stateMachineRegistry;

    public ChangeAutoModeCommandHandler(IMeasurementStateMachineRegistry stateMachineRegistry)
    {
      _stateMachineRegistry = stateMachineRegistry;
    }

    public override object Execute(Backend.DataAccess.Model.IDbContext context, ChangeAutoModeCommand command, string partnerId)
    {
      if (command.AutoModeEnabled)
        _stateMachineRegistry.Start(command.WeightMeterDeviceId);
      else _stateMachineRegistry.Stop(command.WeightMeterDeviceId);

      return true;
    }
  }
}
