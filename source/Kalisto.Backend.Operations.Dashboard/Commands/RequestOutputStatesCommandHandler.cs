﻿using Kalisto.Communication.Core.Messaging;
using Kalisto.Devices.IOController.Interfaces;

namespace Kalisto.Backend.Operations.Dashboard.Commands
{
  public class RequestOutputStatesCommandHandler : CommandHandlerBase<RequestOutputStatesCommand>
  {
    private readonly IBarrierService _barrierService;

    public RequestOutputStatesCommandHandler(IBarrierService barrierService)
    {
      _barrierService = barrierService;
    }

    public override object Execute(Backend.DataAccess.Model.IDbContext context, RequestOutputStatesCommand command, string partnerId)
    {
      _barrierService.RequestBarrierStateAsync();

      return true;
    }
  }
}
