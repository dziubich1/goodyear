﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.StateMachine;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Devices.IOController.Interfaces;

namespace Kalisto.Backend.Operations.Dashboard.Commands
{
  public class RaiseOrLeaveBarrierCommandHandler : CommandHandlerBase<RaiseOrLeaveBarrierCommand>
  {
    private readonly IBarrierService _barrierService;

    private readonly IMeasurementStateMachineRegistry _stateMachineRegistry;

    public RaiseOrLeaveBarrierCommandHandler(IBarrierService barrierService, IMeasurementStateMachineRegistry stateMachineRegistry)
    {
      _barrierService = barrierService;
      _stateMachineRegistry = stateMachineRegistry;
    }

    public override object Execute(IDbContext context, RaiseOrLeaveBarrierCommand command, string partnerId)
    {
      if (_stateMachineRegistry.IsRunning(command.WeightMeterDeviceId))
        return false;

      HandleBarrier(command.BarrierUp, command.WeightMeterDeviceId);

      return true;
    }

    private async void HandleBarrier(bool barrierUp, int weightMeterId)
    {
      if (barrierUp)
        await _barrierService.RaiseBarrierUpAsync(weightMeterId);
      else await _barrierService.LeaveBarrierDownAsync(weightMeterId);
    }
  }
}
