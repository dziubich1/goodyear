﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.StateMachine;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Devices.IOController.Interfaces;

namespace Kalisto.Backend.Operations.Dashboard.Commands
{
  public class TurnGreenOrRedLightCommandHandler : CommandHandlerBase<TurnGreenOrRedLightCommand>
  {
    private readonly ITrafficLightsService _trafficLightsService;

    private readonly IMeasurementStateMachineRegistry _stateMachineRegistry;

    public TurnGreenOrRedLightCommandHandler(ITrafficLightsService trafficLightsService, IMeasurementStateMachineRegistry stateMachineRegistry)
    {
      _trafficLightsService = trafficLightsService;
      _stateMachineRegistry = stateMachineRegistry;
    }

    public override object Execute(IDbContext context, TurnGreenOrRedLightCommand command, string partnerId)
    {
      if (_stateMachineRegistry.IsRunning(command.WeightMeterDeviceId))
        return false;

      HandleTraffic(command.GreenLightEnabled, command.WeightMeterDeviceId);

      return true;
    }

    private async void HandleTraffic(bool greenLightEnabled, int weightMeterId)
    {
      if (greenLightEnabled)
        _trafficLightsService.SetGreenLight(weightMeterId);
      else _trafficLightsService.SetRedLight(weightMeterId);

      await _trafficLightsService.RequestTrafficStateAsync();
    }
  }
}
