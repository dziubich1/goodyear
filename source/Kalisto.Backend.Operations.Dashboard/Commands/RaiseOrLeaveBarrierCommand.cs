﻿using Kalisto.Communication.Core.Messages;

namespace Kalisto.Backend.Operations.Dashboard.Commands
{
  public class RaiseOrLeaveBarrierCommand : BackendDbOperationMessage
  {
    public int WeightMeterDeviceId { get; set; }

    public bool BarrierUp { get; set; }
  }
}
