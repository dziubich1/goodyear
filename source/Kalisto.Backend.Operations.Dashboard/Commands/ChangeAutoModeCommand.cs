﻿using Kalisto.Communication.Core.Messages;

namespace Kalisto.Backend.Operations.Dashboard.Commands
{
  public class ChangeAutoModeCommand : BackendDbOperationMessage
  {
    public int WeightMeterDeviceId { get; set; }

    public bool AutoModeEnabled { get; set; }
  }
}
