﻿using Kalisto.Communication.Core.Messages;

namespace Kalisto.Backend.Operations.Dashboard.Commands
{
  public class TurnGreenOrRedLightCommand : BackendDbOperationMessage
  {
    public int WeightMeterDeviceId { get; set; }

    public bool GreenLightEnabled { get; set; }
  }
}
