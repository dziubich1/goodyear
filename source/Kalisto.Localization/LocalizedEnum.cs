﻿using System;

namespace Kalisto.Localization
{
  public class LocalizedEnum
  {
    private readonly ILocalizer<Enums.Enums> _localizer;

    public Enum Enum { get; set; }

    public string LocalizedEnumValue => GetLocalizedValue();

    public LocalizedEnum()
    {
      _localizer = new LocalizationManager<Enums.Enums>();
    }

    public LocalizedEnum(Enum @enum) : this()
    {
      Enum = @enum;
    }

    private string GetLocalizedValue()
    {
      if (Enum == null)
      {
        return string.Empty;
      }

      var enumKey = $"{Enum.GetType().Name}_{Enum.ToString()}";
      return _localizer.Localize(enumKey);
    }
  }
}
