﻿using System;

namespace Kalisto.Localization
{
  public class LocalizedDate
  {
    public DateTime? Value { get; set; }

    public string LocalizedDateValue => Value?.ToString("dd.MM.yyyy HH:mm:ss");

    public string LocalizedDateOnlyValue => Value?.ToString("dd.MM.yyyy");

    public LocalizedDate(DateTime? date)
    {
      Value = date;
    }

    public LocalizedDate(DateTime date)
    {
      Value = date;
    }
  }
}
