﻿using System;
using System.Linq.Expressions;
using System.Reflection;
using System.Resources;

namespace Kalisto.Localization
{
  public interface ILocalizer<TResource>
  {
    string Localize(Expression<Func<string>> propertyLambda);

    string Localize(string resourceKey);
  }

  public class LocalizationManager<TResource> : ILocalizer<TResource>
  {
    private readonly ResourceManager _resourceManager;

    public LocalizationManager()
    {
      _resourceManager = new ResourceManager(typeof(TResource));
    }

    public string Localize(Expression<Func<string>> propertyLambda)
    {
      if(_resourceManager == null)
        throw new InvalidOperationException();

      if (!(propertyLambda.Body is MemberExpression memberSelectorExpression))
        throw new InvalidOperationException();

      var property = memberSelectorExpression.Member as PropertyInfo;
      if (property == null)
        throw new InvalidOperationException();

      var localizedValue = _resourceManager.GetString(property.Name);
      return string.IsNullOrEmpty(localizedValue) ? $"[[{property.Name}]]" : localizedValue;
    }

    public string Localize(string resourceKey)
    {
      if (_resourceManager == null || string.IsNullOrEmpty(resourceKey))
        throw new InvalidOperationException();

      var localizedValue = _resourceManager.GetString(resourceKey);
      return string.IsNullOrEmpty(localizedValue) ? $"[[{resourceKey}]]" : localizedValue;
    }
  }
}
