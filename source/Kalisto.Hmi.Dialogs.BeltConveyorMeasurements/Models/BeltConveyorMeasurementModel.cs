﻿using Kalisto.Hmi.Dialogs.Common;
using Kalisto.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kalisto.Hmi.Dialogs.BeltConveyorMeasurements.Models
{
  public class BeltConveyorMeasurementModel
  {
    public long Id { get; set; }

    public long Number { get; set; }

    public int ConfigId { get; set; }

    //Weight for Furnace/BeltConveyor no. 1
    public UnitOfMeasurement B1 { get; set; }

    //Weight for Furnace/BeltConveyor no. 2
    public UnitOfMeasurement B2 { get; set; }

    //Weight for Furnace/BeltConveyor no. 3
    public UnitOfMeasurement B3 { get; set; }

    //Weight for Furnace/BeltConveyor no. 4
    public UnitOfMeasurement B4 { get; set; }

    public UnitOfMeasurement TotalWeightSum { get; set; }

    public UnitOfMeasurement IncrementWeightSum { get; set; }

    public LocalizedDate MeasurementDate { get; set; }
  }
}
