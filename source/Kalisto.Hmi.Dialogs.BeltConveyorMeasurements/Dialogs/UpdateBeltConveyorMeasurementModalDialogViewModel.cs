﻿using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Hmi.Core;
using Kalisto.Hmi.Core.Commands;
using Kalisto.Hmi.Core.Communication;
using Kalisto.Hmi.Core.Controls;
using Kalisto.Hmi.Core.Services;
using Kalisto.Hmi.Dialogs.BeltConveyorMeasurements.Models;
using Kalisto.Hmi.Dialogs.Common;
using Kalisto.Hmi.Dialogs.Common.Models;
using Kalisto.Localization;
using PropertyChanged;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Kalisto.Hmi.Dialogs.Vehicles.Dialogs
{
  [AddINotifyPropertyChangedInterface]
  public class UpdateBeltConveyorMeasurementModalDialogViewModel : ModalViewModelBase<UpdateBeltConveyorMeasurementModalDialogViewModel>
  {
    private long _measurementId;
    private long _measurementNumber;
    private UnitOfMeasurement _totalWeightSum;

    public BeltConveyorMeasurementModel BeltConveyorMeasurement
    {
      get
      {
        return new BeltConveyorMeasurementModel()
        {
          Id = _measurementId,
          Number = _measurementNumber,
          TotalWeightSum = _totalWeightSum,
          B1 = decimal.TryParse(B1, out decimal valueB1) ? UnitTypeService.ConvertBackValue(valueB1) : null,
          B2 = decimal.TryParse(B2, out decimal valueB2) ? UnitTypeService.ConvertBackValue(valueB2) : null,
          B3 = decimal.TryParse(B3, out decimal valueB3) ? UnitTypeService.ConvertBackValue(valueB3) : null,
          B4 = decimal.TryParse(B4, out decimal valueB4) ? UnitTypeService.ConvertBackValue(valueB4) : null,
          ConfigId = int.TryParse(ConfigId, out int valueConfig) ? valueConfig : 0,
          IncrementWeightSum = decimal.TryParse(IncrementWeightSum, out decimal value) ? UnitTypeService.ConvertBackValue(value) : null,
          MeasurementDate = new LocalizedDate(MeasurementDate),
        };
      }
      set
      {
        if (value == null)
          return;
        _measurementId = value.Id;
        _measurementNumber = value.Number;
        _totalWeightSum = value.TotalWeightSum;
        B1 = value.B1.DisplayValueUnitless;
        B2 = value.B2.DisplayValueUnitless;
        B3 = value.B3.DisplayValueUnitless;
        B4 = value.B4.DisplayValueUnitless;
        IncrementWeightSum = value.IncrementWeightSum.DisplayValueUnitless;
        ConfigId = value.ConfigId.ToString();
        MeasurementDate = value.MeasurementDate.Value.Value;
      }
    }

    public string ConfigId { get; set; }

    public string B1 { get; set; }

    public string B2 { get; set; }

    public string B3 { get; set; }

    public string B4 { get; set; }

    public string IncrementWeightSum { get; set; }

    public DateTime MeasurementDate { get; set; }

    protected IUnitTypeService UnitTypeService { get; }

    public UpdateBeltConveyorMeasurementModalDialogViewModel(IDialogService dialogService, IAsyncProgressService asyncProgressService,
      IMessageBuilder messageBuilder, IFrontendMessageBridge messageBridge, ClientInfo clientInfo, IUnitTypeService unitTypeService)
      : base(dialogService, asyncProgressService, messageBuilder, messageBridge, clientInfo)
    {
      UnitTypeService = unitTypeService;
    }

    public override string DialogNameKey { get; set; }

    protected override void DefineValidationRules()
    {
      InvalidateProperty(() => B1).When(() => !IsDecimal(B1))
        .WithMessageKey("InvalidNumber").ButIgnoreOnLoad();
      InvalidateProperty(() => B1).When(() => !IsGreaterOrEqualThan(B1, 0) && !string.IsNullOrEmpty(B1))
        .WithMessageKey("MustBeGreaterOrEqualThan", 0).ButIgnoreOnLoad();

      InvalidateProperty(() => B2).When(() => !IsDecimal(B2))
        .WithMessageKey("InvalidNumber").ButIgnoreOnLoad();
      InvalidateProperty(() => B2).When(() => !IsGreaterOrEqualThan(B2, 0) && !string.IsNullOrEmpty(B2))
        .WithMessageKey("MustBeGreaterOrEqualThan", 0).ButIgnoreOnLoad();

      InvalidateProperty(() => B3).When(() => !IsDecimal(B3))
        .WithMessageKey("InvalidNumber").ButIgnoreOnLoad();
      InvalidateProperty(() => B3).When(() => !IsGreaterOrEqualThan(B3, 0) && !string.IsNullOrEmpty(B3))
        .WithMessageKey("MustBeGreaterOrEqualThan", 0).ButIgnoreOnLoad();

      InvalidateProperty(() => B4).When(() => !IsDecimal(B4))
        .WithMessageKey("InvalidNumber").ButIgnoreOnLoad();
      InvalidateProperty(() => B4).When(() => !IsGreaterOrEqualThan(B4, 0) && !string.IsNullOrEmpty(B4))
        .WithMessageKey("MustBeGreaterOrEqualThan", 0).ButIgnoreOnLoad();

      InvalidateProperty(() => IncrementWeightSum).When(() => !IsDecimal(IncrementWeightSum))
        .WithMessageKey("InvalidNumber").ButIgnoreOnLoad();
      InvalidateProperty(() => IncrementWeightSum).When(() => !IsGreaterOrEqualThan(IncrementWeightSum, 0) && !string.IsNullOrEmpty(IncrementWeightSum))
        .WithMessageKey("MustBeGreaterOrEqualThan", 0).ButIgnoreOnLoad();

      InvalidateProperty(() => ConfigId).When(() => !IsNumber(ConfigId))
        .WithMessageKey("InvalidNumber").ButIgnoreOnLoad();
      InvalidateProperty(() => ConfigId).When(() => !IsGreaterOrEqualThan(ConfigId, 0) && !string.IsNullOrEmpty(ConfigId))
        .WithMessageKey("MustBeGreaterOrEqualThan", 0).ButIgnoreOnLoad();
    }
  }
}
