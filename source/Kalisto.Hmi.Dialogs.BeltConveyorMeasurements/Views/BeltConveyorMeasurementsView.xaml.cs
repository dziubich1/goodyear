﻿using System.Windows.Controls;

namespace Kalisto.Hmi.Dialogs.BeltConveyorMeasurements.Views
{
  /// <summary>
  /// Interaction logic for BeltConveyorMeasurementsView.xaml
  /// </summary>
  public partial class BeltConveyorMeasurementsView : UserControl
  {
    public BeltConveyorMeasurementsView()
    {
      InitializeComponent();
    }
  }
}
