﻿using Kalisto.Hmi.Core.Communication;
using Kalisto.Hmi.Dialogs.BeltConveyorMeasurements.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kalisto.Hmi.Dialogs.BeltConveyorMeasurements.Services
{
  public interface IBeltConveyorMeasurementsService
  {
    Task<RequestResult<List<BeltConveyorMeasurementModel>>> GetMeasurements(DateTime from, DateTime to);
    Task<RequestResult<long>>UpdateMeasurementAsync(BeltConveyorMeasurementModel beltConveyorMeasurement);
  }
}
