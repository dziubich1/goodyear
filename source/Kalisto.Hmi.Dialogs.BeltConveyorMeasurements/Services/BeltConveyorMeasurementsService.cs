﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.Operations.BeltConveyorMeasurements.Commands;
using Kalisto.Backend.Operations.BeltConveyorMeasurements.Queries;
using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Hmi.Core.Communication;
using Kalisto.Hmi.Core.Services;
using Kalisto.Hmi.Dialogs.BeltConveyorMeasurements.Models;
using Kalisto.Hmi.Dialogs.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kalisto.Hmi.Dialogs.BeltConveyorMeasurements.Services
{
  public class BeltConveyorMeasurementsService : BackendOperationService, IBeltConveyorMeasurementsService
  {
    private readonly IUnitTypeService _unitTypeService;

    public BeltConveyorMeasurementsService(ClientInfo clientInfo, IMessageBuilder messageBuilder,
      IFrontendMessageBridge messageBridge, IDialogService dialogService, IUnitTypeService unitTypeService)
      : base(clientInfo, messageBuilder, messageBridge, dialogService)
    {
      _unitTypeService = unitTypeService;
    }

    public async Task<RequestResult<List<BeltConveyorMeasurementModel>>> GetMeasurements(DateTime fromDateLocalTime, DateTime toDateLocalTime)
    {
      var message = MessageBuilder.CreateNew<GetBeltConveyorMeasurementsQuery>()
        .BasedOn(ClientInfo)
        .WithProperty(c => c.FromDateUtc, fromDateLocalTime.ToUniversalTime())
        .WithProperty(c => c.ToDateUtc, toDateLocalTime.ToUniversalTime())
        .Build();

      var result = await MessageBridge.SendMessageWithResultAsync<List<BeltConveyorMeasurement>>(message);

      return new RequestResult<List<BeltConveyorMeasurementModel>>()
      {
        Succeeded = result?.Succeeded ?? false,
        Response = result?.Response?.Select(meas => new BeltConveyorMeasurementModel
        {
          Id = meas.Id,
          Number = meas.Number,
          ConfigId = meas.ConfigId,
          B1 = new UnitOfMeasurement(meas.B1, _unitTypeService),
          B2 = new UnitOfMeasurement(meas.B2, _unitTypeService),
          B3 = new UnitOfMeasurement(meas.B3, _unitTypeService),
          B4 = new UnitOfMeasurement(meas.B4, _unitTypeService),
          TotalWeightSum = new UnitOfMeasurement(meas.TotalSum, _unitTypeService),
          IncrementWeightSum = new UnitOfMeasurement(meas.IncrementSum, _unitTypeService),
          MeasurementDate = new Localization.LocalizedDate(meas.DateUtc.ToLocalTime())
        }).ToList() ?? new List<BeltConveyorMeasurementModel>()
      };
    }

    public async Task<RequestResult<long>> UpdateMeasurementAsync(BeltConveyorMeasurementModel measurement)
    {
      var message = MessageBuilder.CreateNew<UpdateBeltConveyorMeasurementCommand>()
        .BasedOn(ClientInfo)
        .WithProperty(c => c.MeasurementId, measurement.Id)
        .WithProperty(c => c.ConfigId, measurement.ConfigId)
        .WithProperty(c => c.B1, measurement.B1.OriginalValue.HasValue ? measurement.B1.OriginalValue.Value : 0)
        .WithProperty(c => c.B2, measurement.B2.OriginalValue.HasValue ? measurement.B2.OriginalValue.Value : 0)
        .WithProperty(c => c.B3, measurement.B3.OriginalValue.HasValue ? measurement.B3.OriginalValue.Value : 0)
        .WithProperty(c => c.B4, measurement.B4.OriginalValue.HasValue ? measurement.B4.OriginalValue.Value : 0)
        .WithProperty(c => c.IncrementWeightSum, measurement.IncrementWeightSum.OriginalValue.HasValue ? measurement.IncrementWeightSum.OriginalValue.Value : 0)
        .WithProperty(c => c.MeasurementDateUtc, measurement.MeasurementDate.Value.Value.ToUniversalTime())
        .Build();


      var result = await MessageBridge.SendMessageWithResultAsync<long>(message);
      return result ?? RequestResult<long>.Default;
    }
  }
}