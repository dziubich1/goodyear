﻿using Kalisto.Hmi.Core;
using Kalisto.Hmi.Core.Commands;
using Kalisto.Hmi.Core.Services;
using Kalisto.Hmi.Dialogs.BeltConveyorMeasurements.Models;
using Kalisto.Hmi.Dialogs.BeltConveyorMeasurements.Services;
using Kalisto.Hmi.Dialogs.Vehicles.Dialogs;
using PropertyChanged;
using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Kalisto.Backend.Operations.BeltConveyorMeasurements.Commands.Interfaces;
using Kalisto.Configuration.Permissions;
using Kalisto.Localization;
using Kalisto.Localization.Messages;

namespace Kalisto.Hmi.Dialogs.BeltConveyorMeasurements.ViewModels
{
  [AddINotifyPropertyChangedInterface]
  public class BeltConveyorMeasurementsViewModel : DataGridViewModel<BeltConveyorMeasurementModel>
  {
    private readonly ILocalizer<Messages> _localizer;

    private readonly IBeltConveyorMeasurementsService _beltConveyorMeasurementService;

    public ICommand EditItemCommand { get; set; }

    public ICommand SyncItemsCommand { get; set; }

    public DateTime FromDate { get; set; }

    public DateTime ToDate { get; set; }

    public BeltConveyorMeasurementModel SelectedMeasurement { get; set; }

    public ObservableCollectionEx<BeltConveyorMeasurementModel> BeltConveyorMeasurementsItems { get; set; }

    public override string DialogTitleKey => "BeltConveyorMeasurements";

    protected override Type[] AssociatedCommandTypes => new[] {typeof(IBeltConveyorMeasurementCommand)};

    public BeltConveyorMeasurementsViewModel(IDialogService dialogService, IAuthorizationService authorizationService,
      IBeltConveyorMeasurementsService beltConveyorMeasurementService)
      : base(dialogService, authorizationService)
    {
      _localizer = new LocalizationManager<Messages>();
      _beltConveyorMeasurementService = beltConveyorMeasurementService;
      EditItemCommand = new DelegateCommand(EditItem);
      SyncItemsCommand = new DelegateCommand(SyncMeasurements);
      FromDate = DateTime.Today.AddDays(-3);
      ToDate = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 23, 59, 59);
      BeltConveyorMeasurementsItems = new ObservableCollectionEx<BeltConveyorMeasurementModel>();
    }

    public override async Task RefreshDataAsync()
    {
      await base.RefreshDataAsync();

      await LoadMeasurements();
    }

    public async void OnFromDateChanged()
    {
      if (!ViewLoaded)
        return;

      await LoadMeasurements();
    }

    public async void OnToDateChanged()
    {
      if (!ViewLoaded)
        return;

      await LoadMeasurements();
    }

    private async Task LoadMeasurements()
    {
      var result = await _beltConveyorMeasurementService.GetMeasurements(FromDate, ToDate);
      if (!result.Succeeded || result.Response == null)
        return;

      BeltConveyorMeasurementsItems.Clear();
      BeltConveyorMeasurementsItems.AddRange(result.Response);
    }

    private async void SyncMeasurements(object parameter)
    {
      await LoadMeasurements();
    }

    protected override async void EditItem(object item)
    {
      base.EditItem(item);
      var selectedMeasurement = (item as BeltConveyorMeasurementModel) ?? SelectedMeasurement;
      if (selectedMeasurement == null)
      {
        await DialogService.ShowMessageAsync(_localizer.Localize(() => Messages.Error), _localizer.Localize(() => Messages.SelectMeasurementToUpdate), MessageDialogType.Ok);
        return;
      }

      await DialogService.ShowModalDialogAsync<UpdateBeltConveyorMeasurementModalDialogViewModel>(vm =>
      {
        vm.DialogNameKey = "EditMeasurement";
        vm.ValidateOnLoad = false;
        vm.BeltConveyorMeasurement = selectedMeasurement;
      },
      async vm =>
      {
        var result = await _beltConveyorMeasurementService.UpdateMeasurementAsync(vm.BeltConveyorMeasurement);
        return result.Succeeded;
      });
    }

    protected override ObservableCollectionEx<BeltConveyorMeasurementModel> GetItemsSource()
    {
      return BeltConveyorMeasurementsItems;
    }

    protected override string GetEditRuleName()
    {
      return PermissionRules.UpdateBeltConveyorMeasurementRule;
    }
  }
}
