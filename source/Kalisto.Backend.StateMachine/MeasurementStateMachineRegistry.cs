﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Kalisto.Backend.StateMachine.Payloads;

namespace Kalisto.Backend.StateMachine
{
  public interface IMeasurementStateMachineRegistry
  {
    void Register(IMeasurementStateMachine stateMachine);

    void Fire(MeasurementEvent eventName, StateMachinePayloadBase payload);

    void Initialize();

    void Start();

    void Start(int stateMachineId);

    void Stop();

    void Stop(int stateMachineId);

    void Restart();

    bool IsRunning(int stateMachineId);

    bool IsRunning();
  }

  public class MeasurementStateMachineRegistry : IMeasurementStateMachineRegistry
  {
    private readonly List<IMeasurementStateMachine> _stateMachines = new List<IMeasurementStateMachine>();

    public void Register(IMeasurementStateMachine stateMachine)
    {
      if (_stateMachines.Any(c => c.Id == stateMachine.Id))
        return;

      _stateMachines.Add(stateMachine);
    }

    public void Fire(MeasurementEvent eventName, StateMachinePayloadBase payload)
    {
      var sm = _stateMachines.FirstOrDefault(c => c.Id == payload.AssignedWeightMeterDeviceId);
      if (sm == null)
        return;

      sm.Fire(eventName, payload);
    }

    public void Initialize()
    {
      foreach (var sm in _stateMachines)
        sm.Initialize(MeasurementState.Initial);
    }

    public void Start()
    {
      foreach (var sm in _stateMachines)
      {
        sm.Start();
        sm.Fire(MeasurementEvent.ForceReset, new StateMachinePayloadBase
        {
          AssignedWeightMeterDeviceId = sm.Id
        });
      }
    }

    public void Start(int stateMachineId)
    {
      var sm = _stateMachines.FirstOrDefault(c => c.Id == stateMachineId);
      if (sm == null)
        return;

      sm.Start();
      sm.Fire(MeasurementEvent.ForceReset, new StateMachinePayloadBase
      {
        AssignedWeightMeterDeviceId = sm.Id
      });
    }

    public void Stop()
    {
      foreach (var sm in _stateMachines)
        sm.Stop();
    }

    public void Stop(int stateMachineId)
    {
      var sm = _stateMachines.FirstOrDefault(c => c.Id == stateMachineId);
      if (sm == null)
        return;

      sm.Stop();
    }

    public void Restart()
    {
      Stop();
      Start();
    }

    public bool IsRunning(int stateMachineId)
    {
      return _stateMachines.FirstOrDefault(c => c.Id == stateMachineId)?.IsRunning ?? false;
    }

    public bool IsRunning()
    {
      return _stateMachines.All(c => c.IsRunning);
    }
  }
}
