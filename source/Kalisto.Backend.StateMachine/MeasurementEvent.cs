﻿namespace Kalisto.Backend.StateMachine
{
  public enum MeasurementEvent
  {
    DecreasingWeightLimitExceeded,
    IncreasingWeightLimitExceeded,
    CardScanned,
    MeasurementStored,
    LightCurtainValidated,
    LightCurtainInvalidated,
    Reset, ForceReset
  }
}
