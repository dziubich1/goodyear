﻿using Appccelerate.StateMachine;
using Appccelerate.StateMachine.Extensions;
using Kalisto.Backend.StateMachine.Payloads;
using System.Timers;

namespace Kalisto.Backend.StateMachine
{
  public interface IMeasurementStateMachineFactory
  {
    IMeasurementStateMachine Create(int assignedWeighingMeterDeviceId);
  }

  public class MeasurementStateMachineFactory : IMeasurementStateMachineFactory
  {
    private readonly IMeasurementStateMachineEventHandler _eventHandler;

    public MeasurementStateMachineFactory(IMeasurementStateMachineEventHandler eventHandler)
    {
      _eventHandler = eventHandler;
    }

    public IMeasurementStateMachine Create(int assignedWeighingMeterDeviceId)
    {
      var fsm = new MeasurementStateMachine(assignedWeighingMeterDeviceId);

      fsm.AddExtension(new AutoResetExtension(fsm));
      fsm.In(MeasurementState.Initial)
        .ExecuteOnEntry<object>(HandleInitialState)
        .On(MeasurementEvent.IncreasingWeightLimitExceeded).Goto(MeasurementState.TruckArriving)
        .On(MeasurementEvent.LightCurtainValidated).Goto(MeasurementState.TruckArrived)
        .On(MeasurementEvent.CardScanned).Goto(MeasurementState.MeasurementStarted)
        .On(MeasurementEvent.ForceReset).Goto(MeasurementState.Initial);

      fsm.In(MeasurementState.TruckArriving)
        .ExecuteOnEntry<object>(HandleTruckArriving)
        .On(MeasurementEvent.LightCurtainValidated).Goto(MeasurementState.TruckArrived)
        .On(MeasurementEvent.CardScanned).Goto(MeasurementState.MeasurementStarted)
        .On(MeasurementEvent.MeasurementStored).Goto(MeasurementState.MeasurementCompleted)
        .On(MeasurementEvent.DecreasingWeightLimitExceeded).Goto(MeasurementState.TruckLeft)
        .On(MeasurementEvent.ForceReset).Goto(MeasurementState.Initial);

      fsm.In(MeasurementState.TruckArrived)
        .ExecuteOnEntry<object>(HandleTruckArrived)
        .On(MeasurementEvent.CardScanned).Goto(MeasurementState.MeasurementStarted)
        .On(MeasurementEvent.MeasurementStored).Goto(MeasurementState.MeasurementCompleted)
        .On(MeasurementEvent.DecreasingWeightLimitExceeded).Goto(MeasurementState.TruckLeft)
        .On(MeasurementEvent.ForceReset).Goto(MeasurementState.Initial);

      fsm.In(MeasurementState.MeasurementStarted)
        .ExecuteOnEntry<object>(HandleMeasurementStarted)
        .On(MeasurementEvent.MeasurementStored).Goto(MeasurementState.MeasurementCompleted)
        .On(MeasurementEvent.ForceReset).Goto(MeasurementState.Initial);

      fsm.In(MeasurementState.MeasurementCompleted)
        .ExecuteOnEntry<object>(HandleMeasurementCompleted)
        .On(MeasurementEvent.DecreasingWeightLimitExceeded).Goto(MeasurementState.TruckLeft)
        .On(MeasurementEvent.ForceReset).Goto(MeasurementState.Initial);

      fsm.In(MeasurementState.TruckLeft)
        .ExecuteOnEntry<object>(HandleTruckLeft)
        .On(MeasurementEvent.Reset).Goto(MeasurementState.Initial)
        .On(MeasurementEvent.ForceReset).Goto(MeasurementState.Initial);

      return fsm;
    }

    private void HandleTruckLeft(object parameter)
    {
      var payload = parameter as StateMachinePayloadBase;
      if (payload == null)
        return;

      _eventHandler.HandleTruckLeft(payload);
    }

    private void HandleMeasurementCompleted(object parameter)
    {
      var payload = parameter as StateMachinePayloadBase;
      if (payload == null)
        return;

      _eventHandler.HandleMeasurementCompleted(payload);
    }

    private void HandleMeasurementStarted(object parameter)
    {
      var payload = parameter as StateMachinePayloadBase;
      if (payload == null)
        return;

      _eventHandler.HandleMeasurementStarted(payload);
    }

    private void HandleTruckArrived(object parameter)
    {
      var payload = parameter as StateMachinePayloadBase;
      if (payload == null)
        return;

      _eventHandler.HandleTruckArrived(payload);
    }

    private void HandleTruckArriving(object parameter)
    {
      var payload = parameter as StateMachinePayloadBase;
      if (payload == null)
        return;

      _eventHandler.HandleTruckArriving(payload);
    }

    private void HandleInitialState(object parameter)
    {
      var payload = parameter as StateMachinePayloadBase;
      if (payload == null)
        return;

      _eventHandler.HandleInitialState(payload);
    }
  }

  public class AutoResetExtension : ExtensionBase<MeasurementState, MeasurementEvent>
  {
    private const int TimerInterval = 600000;

    private readonly IMeasurementStateMachine _stateMachine;

    private Timer _timer;

    public AutoResetExtension(IMeasurementStateMachine stateMachine)
    {
      _stateMachine = stateMachine;
      _timer = new Timer(TimerInterval);
      _timer.Elapsed += OnTimerElapsed;
    }

    public override void SwitchedState(IStateMachineInformation<MeasurementState, MeasurementEvent> stateMachine, Appccelerate.StateMachine.Machine.IState<MeasurementState, MeasurementEvent> oldState, Appccelerate.StateMachine.Machine.IState<MeasurementState, MeasurementEvent> newState)
    {
      if (_timer != null)
      {
        if (_timer.Enabled)
          _timer.Enabled = false;

        _timer.Elapsed -= OnTimerElapsed;
        _timer = new Timer(TimerInterval);
        _timer.Elapsed += OnTimerElapsed;
        _timer.Enabled = true;
      }
    }

    private void OnTimerElapsed(object sender, ElapsedEventArgs e)
    {
      _stateMachine.FirePriority(MeasurementEvent.ForceReset, new StateMachinePayloadBase
      {
        AssignedWeightMeterDeviceId = _stateMachine.Id
      });
    }
  }
}
