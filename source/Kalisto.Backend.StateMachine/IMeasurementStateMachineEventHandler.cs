﻿using Kalisto.Backend.StateMachine.Payloads;

namespace Kalisto.Backend.StateMachine
{
  public interface IMeasurementStateMachineEventHandler
  {
    void HandleTruckLeft(StateMachinePayloadBase parameter);

    void HandleMeasurementCompleted(StateMachinePayloadBase parameter);

    void HandleMeasurementStarted(StateMachinePayloadBase parameter);

    void HandleTruckArrived(StateMachinePayloadBase parameter);

    void HandleTruckArriving(StateMachinePayloadBase parameter);

    void HandleInitialState(StateMachinePayloadBase parameter);
  }
}
