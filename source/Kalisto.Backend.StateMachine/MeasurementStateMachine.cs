﻿using Appccelerate.StateMachine;

namespace Kalisto.Backend.StateMachine
{
  public interface IMeasurementStateMachine : IStateMachine<MeasurementState, MeasurementEvent>
  {
    int Id { get; }
  }

  public class MeasurementStateMachine : PassiveStateMachine<MeasurementState, MeasurementEvent>, IMeasurementStateMachine
  {
    public int Id { get; }

    // this class has been created for being extensible in the future
    public MeasurementStateMachine(int stateMachineId)
     : base(stateMachineId.ToString())
    {
      Id = stateMachineId;
    }
  }
}
