﻿namespace Kalisto.Backend.StateMachine.Payloads
{
  public class StateMachinePayloadBase
  {
    public int AssignedWeightMeterDeviceId { get; set; }
  }
}
