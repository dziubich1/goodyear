﻿namespace Kalisto.Backend.StateMachine.Payloads
{
  public class CardScannedPayload : StateMachinePayloadBase
  {
    public int DeviceId { get; set; }

    public string CardCode { get; set; }
  }
}
