﻿namespace Kalisto.Backend.StateMachine
{
  public enum MeasurementState
  {
    Initial,
    TruckArriving,
    TruckArrived,
    MeasurementStarted,
    MeasurementCompleted,
    TruckLeft
  }
}
