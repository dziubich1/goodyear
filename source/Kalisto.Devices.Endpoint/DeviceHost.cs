using Kalisto.Configuration.Devices;
using Kalisto.Configuration.Devices.Connection;
using Kalisto.Configuration.Enums;
using Kalisto.Devices.BarcodeScanner;
using Kalisto.Devices.BeltConveyorCounter;
using Kalisto.Devices.Core;
using Kalisto.Devices.Core.Communication;
using Kalisto.Devices.Core.Messages;
using Kalisto.Devices.WeightMeter;
using System;
using System.IO.Ports;
using Kalisto.Devices.CardReader;
using Kalisto.Devices.CardReader.Messages;
using Kalisto.Devices.Core.Communication.Interfaces;
using Kalisto.Devices.IOController;
using Kalisto.Devices.IOController.Messages.Base;
using Kalisto.Devices.WeightMeter.Messages;
using Kalisto.Logging;
using Unity;

namespace Kalisto.Devices.Endpoint
{
  public class DeviceHost
  {
    private readonly IUnityContainer _container;

    private readonly IDeviceConnectionFactory _connectionFactory;

    private readonly ILogger _logger;

    private IDevice _runningDevice;

    private string _endpointId;

    public int DeviceId => _runningDevice.Id;

    public DeviceHost(IUnityContainer container, ILogger logger, IDeviceConnectionFactory connectionFactory)
    {
      _container = container;
      _logger = logger;
      _connectionFactory = connectionFactory;
    }

    public void Configure(DeviceConfiguration configuration)
    {
      if (configuration is WeightMeterConfiguration weightMeterConfiguration)
      {
        var messageChunkAggregator = new WeightMeterMessageChunkAggregator(new WeightMeterMessageTranslator());
        var sender = _container.Resolve<DeviceMessageSender<WeightMeterMessage>>();
        IDeviceClientConnection connection = null;
        if (weightMeterConfiguration.ConnectionType == ConnectionType.Tcp)
        {
          var tcpConfiguration = weightMeterConfiguration.ConnectionConfiguration as TcpConnectionConfiguration;
          if (tcpConfiguration == null)
          {
            _logger.Error("TCP configuration of weight metering device is not available.");
            throw new InvalidOperationException();
          }

          connection = _connectionFactory.GetTcpConnection(tcpConfiguration.IpAddress, tcpConfiguration.Port);
          _endpointId = $"{tcpConfiguration.IpAddress}:{tcpConfiguration.Port}";
        }
        else if (weightMeterConfiguration.ConnectionType == ConnectionType.Serial)
        {
          var serialPortConfiguration = weightMeterConfiguration.ConnectionConfiguration as SerialPortConnectionConfiguration;
          if (serialPortConfiguration == null)
          {
            _logger.Error("Serial port configuration of weight metering device is not available.");
            throw new InvalidOperationException();
          }

          connection = _connectionFactory.GetSerialPortConnection(serialPortConfiguration.Port,
            serialPortConfiguration.BaudRate, GetParity(serialPortConfiguration.Parity),
            serialPortConfiguration.DataBits, GetStopBits(serialPortConfiguration.StopBits));
          _endpointId = $"{serialPortConfiguration.Port}";
        }
        else
          throw new InvalidOperationException(); // TCP & Serial only allowed so far

        if(connection == null)
          throw new InvalidOperationException("Could not create connection for weight metering device.");

        
        var device = new WeightMeteringDevice(connection, messageChunkAggregator);
        sender.Initialize(device);
        _runningDevice = device;
      }

      if (configuration is BarcodeScannerConfiguration scannerConfiguration)
      {
        var translator = new BarcodeScannerMessageTranslator();
        var validator = translator;
        var messageChunkAggregator = new BarcodeScannerMessageChunkAggregator(translator, validator);
        var propagator = _container.Resolve<ILocalDeviceMessageBusPropagator>();
        if (scannerConfiguration.ConnectionType != ConnectionType.Serial)
          throw new InvalidOperationException(); // Serial Port only allowed so far

        var serialPortConfiguration = (SerialPortConnectionConfiguration)scannerConfiguration.ConnectionConfiguration;
        _endpointId = $"{serialPortConfiguration.Port}";
        var device = new BarcodeScannerDevice(
          _connectionFactory.GetSerialPortConnection(serialPortConfiguration.Port,
            serialPortConfiguration.BaudRate, GetParity(serialPortConfiguration.Parity),
            serialPortConfiguration.DataBits, GetStopBits(serialPortConfiguration.StopBits)), messageChunkAggregator);
        device.MessageReceived += propagator.Propagate;
        _runningDevice = device;
      }

      if (configuration is BeltConveyorCounterConfiguration conveyorConfiguration)
      {
        var translator = new BeltConveyorMessageTranslator();
        var messageChunkAggregator = new BeltConveyorMessageChunkAggregator(translator);
        var sender = _container.Resolve<DeviceMessageSender<DeviceMessage>>();
        if (conveyorConfiguration.ConnectionType != ConnectionType.Tcp)
          throw new InvalidOperationException(); // TCP only allowed so far

        var tcpConfiguration = (TcpConnectionConfiguration)conveyorConfiguration.ConnectionConfiguration;
        _endpointId = $"{tcpConfiguration.IpAddress}:{tcpConfiguration.Port}";
        var device = new BeltConveyorCounterDevice(
          _connectionFactory.GetTcpConnection(tcpConfiguration.IpAddress, tcpConfiguration.Port), messageChunkAggregator);
        sender.Initialize(device);
        _runningDevice = device;
      }

      if (configuration is CardReaderConfiguration cardReaderConfiguration)
      {
        var messageChunkAggregator = new CardReaderMessageChunkAggregator(new CardReaderMessageTranslator());
        var sender = _container.Resolve<DeviceMessageSender<CardReaderMessage>>();
        IDeviceClientConnection connection = null;
        if (cardReaderConfiguration.ConnectionType == ConnectionType.Tcp)
        {
          var tcpConfiguration = cardReaderConfiguration.ConnectionConfiguration as TcpConnectionConfiguration;
          if (tcpConfiguration == null)
          {
            _logger.Error("TCP configuration of card reader device is not available.");
            throw new InvalidOperationException();
          }

          connection = _connectionFactory.GetTcpConnection(tcpConfiguration.IpAddress, tcpConfiguration.Port);
          _endpointId = $"{tcpConfiguration.IpAddress}:{tcpConfiguration.Port}";
        }
        else if (cardReaderConfiguration.ConnectionType == ConnectionType.Serial)
        {
          var serialPortConfiguration = cardReaderConfiguration.ConnectionConfiguration as SerialPortConnectionConfiguration;
          if (serialPortConfiguration == null)
          {
            _logger.Error("Serial port configuration of card reader device is not available.");
            throw new InvalidOperationException();
          }

          connection = _connectionFactory.GetSerialPortConnection(serialPortConfiguration.Port,
            serialPortConfiguration.BaudRate, GetParity(serialPortConfiguration.Parity),
            serialPortConfiguration.DataBits, GetStopBits(serialPortConfiguration.StopBits));
          _endpointId = $"{serialPortConfiguration.Port}";
        }
        else
          throw new InvalidOperationException(); // TCP & Serial only allowed so far

        if (connection == null)
          throw new InvalidOperationException("Could not create connection for card reader device.");


        var device = new CardReaderDevice(connection, messageChunkAggregator);
        sender.Initialize(device);
        _runningDevice = device;
      }

      if (configuration is IOControllerConfiguration ioControllerConfiguration)
      {
        var messageChunkAggregator = new IOControllerMessageChunkAggregator(new IOControllerMessageTranslator());
        var sender = _container.Resolve<DeviceMessageSender<ExternalControllerSilentMessage>>();
        IDeviceClientConnection connection = null;
        if (ioControllerConfiguration.ConnectionType == ConnectionType.Tcp)
        {
          var tcpConfiguration = ioControllerConfiguration.ConnectionConfiguration as TcpConnectionConfiguration;
          if (tcpConfiguration == null)
          {
            _logger.Error("TCP configuration of card reader device is not available.");
            throw new InvalidOperationException();
          }

          connection = _connectionFactory.GetTcpConnection(tcpConfiguration.IpAddress, tcpConfiguration.Port);
          _endpointId = $"{tcpConfiguration.IpAddress}:{tcpConfiguration.Port}";
        }
        else if (ioControllerConfiguration.ConnectionType == ConnectionType.Serial)
        {
          var serialPortConfiguration = ioControllerConfiguration.ConnectionConfiguration as SerialPortConnectionConfiguration;
          if (serialPortConfiguration == null)
          {
            _logger.Error("Serial port configuration of card reader device is not available.");
            throw new InvalidOperationException();
          }

          connection = _connectionFactory.GetSerialPortConnection(serialPortConfiguration.Port,
            serialPortConfiguration.BaudRate, GetParity(serialPortConfiguration.Parity),
            serialPortConfiguration.DataBits, GetStopBits(serialPortConfiguration.StopBits));
          _endpointId = $"{serialPortConfiguration.Port}";
        }
        else
          throw new InvalidOperationException(); // TCP & Serial only allowed so far

        if (connection == null)
          throw new InvalidOperationException("Could not create connection for card reader device.");


        var device = new IOControllerDevice(connection, messageChunkAggregator);
        sender.Initialize(device);
        _runningDevice = device;
      }

      if (_runningDevice != null)
        _runningDevice.Id = configuration.Id;
    }

    public void SendCommand<TCommand>(TCommand command)
      where TCommand : DeviceCommandMessageBase
    {
      if (_runningDevice != null)
        _runningDevice.SendCommand(command);
    }

    private void OnConnectionChanged(ConnectionState obj)
    {
      _logger.Debug($@"Device connection state changed ({_endpointId}): {obj.ToString()}");
    }

    private Parity GetParity(int parity)
    {
      switch (parity)
      {
        case 0: return Parity.None;
        case 1: return Parity.Odd;
        case 2: return Parity.Even;
        case 3: return Parity.Mark;
        case 4: return Parity.Space;
        default: return Parity.None;
      }
    }

    private StopBits GetStopBits(int stopBits)
    {
      switch (stopBits)
      {
        case 0: return StopBits.None;
        case 1: return StopBits.One;
        case 2: return StopBits.Two;
        case 3: return StopBits.OnePointFive;
        default: return StopBits.None;
      }
    }

    public void Run()
    {
      if (_runningDevice == null)
        return;

      _runningDevice.ConnectionStateChanged += OnConnectionChanged;
      _runningDevice.StartSampling();
    }

    public void Stop()
    {
      if (_runningDevice == null)
        return;

      _runningDevice.ConnectionStateChanged -= OnConnectionChanged;
      _runningDevice.StopSampling();
    }
  }
}
