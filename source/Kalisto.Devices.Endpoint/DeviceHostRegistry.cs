﻿using Kalisto.Devices.Core.Messages;
using Kalisto.Devices.Core.Services;
using Kalisto.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kalisto.Devices.Endpoint
{
  public interface IDeviceHostRegistry
  {
    Task InitializeAsync();

    void RunDeviceHosts();

    void StopDeviceHosts();

    void ForwardCommand<TCommand>(TCommand command)
      where TCommand : DeviceCommandMessageBase;
  }

  public class DeviceHostRegistry : IDeviceHostRegistry
  {
    private readonly List<DeviceHost> _hosts = new List<DeviceHost>();

    private readonly ILogger _logger;

    private readonly DeviceConfigurationService _deviceConfigurationService;

    private readonly DeviceHostFactory _deviceHostFactory;

    public DeviceHostRegistry(ILogger logger, DeviceConfigurationService deviceConfigurationService, DeviceHostFactory deviceHostFactory)
    {
      _logger = logger;
      _deviceConfigurationService = deviceConfigurationService;
      _deviceHostFactory = deviceHostFactory;
    }

    public async Task InitializeAsync()
    {
      if (_hosts.Any())
        ClearDeviceHosts();

      var configs = await _deviceConfigurationService.GetDeviceConfigurationsAsync();
      if (configs == null || !configs.Any())
        return;

      foreach (var cfg in configs)
      {   
        try
        {
          _hosts.Add(_deviceHostFactory.Create(cfg));
        }
        catch (Exception e)
        {
          _logger.Error($"Could not create device host (Device ID: {cfg.Id}). Following exception was thrown: {e.Message}");
        }
      }
    }

    public void RunDeviceHosts()
    {
      foreach (var host in _hosts)
        host.Run();
    }

    public void StopDeviceHosts()
    {
      foreach (var host in _hosts)
        host.Stop();
    }

    public void ForwardCommand<TCommand>(TCommand command)
      where TCommand : DeviceCommandMessageBase
    {
      var deviceHost = _hosts.FirstOrDefault(c => c.DeviceId == command.DeviceId);
      if (deviceHost == null)
        return;

      Task.Run(() => deviceHost.SendCommand(command));
    }

    private void ClearDeviceHosts()
    {
      StopDeviceHosts();

      _hosts.Clear();
    }
  }
}
