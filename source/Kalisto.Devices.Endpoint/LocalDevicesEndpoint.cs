﻿using System;
using System.Threading.Tasks;
using Kalisto.Communication.Core;
using Kalisto.Communication.Core.Communication;
using Kalisto.Communication.Core.Jobs;
using Kalisto.Communication.Core.Jobs.Base;
using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Communication.Core.Registries;
using Kalisto.Communication.Local;
using Kalisto.Communication.RabbitMq;
using Kalisto.Devices.Core;
using Kalisto.Devices.Core.Communication;
using Kalisto.Devices.Core.Messages;
using Kalisto.Devices.Core.Services;
using Kalisto.Devices.Endpoint.Handlers;
using Kalisto.Devices.WeightMeter.Messages;
using Kalisto.Hmi.Core.Communication;
using Kalisto.Hmi.Core.Services;
using Unity;
using Unity.Injection;
using Unity.Lifetime;

namespace Kalisto.Devices.Endpoint
{
  public class LocalDevicesEndpoint : ClientEndpoint<DevicesEndpointConfiguration>
  {
    private IInitializationJobScheduler _initJobScheduler;

    private IDeviceHostRegistry _hostRegistry;

    public LocalDevicesEndpoint(IUnityContainer container, string domainName, string appName)
      : base(container, domainName, appName)
    {
    }

    protected override ClientType GetClientType() => ClientType.Device;

    protected override void Bind(IUnityContainer container)
    {
      base.Bind(container);

      container.RegisterType<IMessageReceiver, MessageLocalReceiver>();
      container.RegisterType<IMessageSender, MessageLocalSender>();

      container.RegisterType<UserInfo>(new ContainerControlledLifetimeManager());
      container.RegisterType<DeviceEndpointMessageHandlerFactory>();
      container.RegisterType<ISubscriberRegistry, DevicesEndpointSubscriberRegistry>(new ContainerControlledLifetimeManager());
      container.RegisterType<IMessageProviderRegistry, DevicesEndpointProviderRegistry>(new ContainerControlledLifetimeManager());

      container.RegisterType<RabbitMqConnectionConfiguration>(new InjectionFactory(c =>
        Configuration.RabbitMqConnection));

      container.RegisterType<IClientMessageRouter, ClientMessageRouter>(new ContainerControlledLifetimeManager());
      container.RegisterType<IFrontendMessageBridge, BasicMessageBridge>();
      container.RegisterSingleton<IAsyncProgressService, AsyncProgressService>();

      container.RegisterSingleton<IDeviceHostRegistry, DeviceHostRegistry>();
      container.RegisterSingleton<DeviceConfigurationService>();
      container.RegisterSingleton<DeviceHostFactory>();
      container.RegisterType<DeviceHost>();

      // DEVICES ENDPOINT SPECIFIC INITIALIZATION JOBS
      container.RegisterType<InitializationJobScheduler>(new ContainerControlledLifetimeManager());
      container.RegisterType<IInitializationJobScheduler, InitializationJobScheduler>();
      container.RegisterType<IInitializationJobSchedulerInfo, InitializationJobScheduler>();
      container.RegisterType<ClientRegistrationJob>();
      container.RegisterType<AssemblyVersionConsistencyCheckJob>();
      container.RegisterType<AssemblyVersionCompatibilityCheckJob>();

      container.RegisterType<IDeviceConnectionFactory, DeviceConnectionFactory>();
      container.RegisterType<DeviceMessageSender<WeightMeterMessage>>();
      container.RegisterType<DeviceConfigurationResponseHandler>();
      container.RegisterType<DeviceCommandMessageHandler>();
    }

    protected override void Resolve(IUnityContainer container)
    {
      base.Resolve(container);

      var assemblyConsistencyJob = container.Resolve<AssemblyVersionConsistencyCheckJob>();
      var registrationJob = container.Resolve<ClientRegistrationJob>();
      var assemblyCompatibilityCheckJob = container.Resolve<AssemblyVersionCompatibilityCheckJob>();

      _hostRegistry = container.Resolve<IDeviceHostRegistry>();
      _initJobScheduler = container.Resolve<IInitializationJobScheduler>();
      _initJobScheduler.InitializationCompleted += OnInitializationJobsCompleted;
      _initJobScheduler.InitializationJobExceptionOccurred += OnInitializationJobExceptionOccurred;
      _initJobScheduler.InitializationProgressChanged += OnInitializationJobProgressChanged;
      _initJobScheduler.ScheduleJob(assemblyConsistencyJob);
      _initJobScheduler.ScheduleJob(registrationJob);
      _initJobScheduler.ScheduleJob(assemblyCompatibilityCheckJob);
    }

    private void OnInitializationJobProgressChanged(int value, IInitializationJobInfo info)
    {
      Logger.Info($"Initializing device endpoint ({value}%): {info.LocalizedMessage}");
    }

    private void OnInitializationJobExceptionOccurred(IInitializationJobInfo info)
    {
      Logger.Error($"Initialization step error occurred: {info.LocalizedExceptionMessage}");
      Task.Run(async () =>
      {
        var seconds = 15;
        Logger.Error($"Device endpoint will try to reconnect in {seconds} seconds.");
        await Task.Delay(seconds * 1000);
        await _initJobScheduler.RunAsync();
      });
    }

    private async void OnInitializationJobsCompleted(bool errorOccurred)
    {
      if (errorOccurred)
        return;

      Logger.Info($"Devices endpoint has been initialized.");
      await _hostRegistry.InitializeAsync();
      _hostRegistry.RunDeviceHosts();
      Logger.Info("Device endpoint initialization has been completed successfully.");
    }

    protected override async void OnInitialized()
    {
      base.OnInitialized();

      await _initJobScheduler.RunAsync();
    }

    protected override void OnDisconnect()
    {
      base.OnDisconnect();

      _hostRegistry.StopDeviceHosts();
    }
  }
}
