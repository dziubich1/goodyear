﻿using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Hmi.Core.Communication;

namespace Kalisto.Devices.Endpoint.Handlers
{
  public class DeviceConfigurationResponseHandler : MessageHandlerBase<DeviceConfigurationsResponseMessage>
  {
    private readonly IClientMessageRouter _messageAggregator;

    public DeviceConfigurationResponseHandler(IClientMessageRouter messageAggregator)
    {
      _messageAggregator = messageAggregator;
    }

    protected override void HandleMessage(DeviceConfigurationsResponseMessage message)
    {
      _messageAggregator.Route(message);
    }
  }
}
