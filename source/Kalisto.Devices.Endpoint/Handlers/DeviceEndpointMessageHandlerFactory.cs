﻿using Kalisto.Communication.Core.Messaging;
using Kalisto.Frontend.Handlers;
using Unity;

namespace Kalisto.Devices.Endpoint.Handlers
{
  public class DeviceEndpointMessageHandlerFactory : MessageHandlerFactoryBase
  {
    public DeviceEndpointMessageHandlerFactory(IUnityContainer container)
      : base(container)
    {
    }

    public IMessageHandler GetDeviceConfigurationResponseHandler()
    {
      return DependencyContainer.Resolve<DeviceConfigurationResponseHandler>();
    }

    public IMessageHandler GetDeviceCommandMessageHandler()
    {
      return DependencyContainer.Resolve<DeviceCommandMessageHandler>();
    }
  }
}
