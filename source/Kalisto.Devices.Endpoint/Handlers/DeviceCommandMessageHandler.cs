﻿using Kalisto.Communication.Core.Messaging;
using Kalisto.Devices.Core.Messages;

namespace Kalisto.Devices.Endpoint.Handlers
{
  public class DeviceCommandMessageHandler : MessageHandlerBase<DeviceCommandMessageBase>
  {
    private readonly IDeviceHostRegistry _hostRegistry;

    public DeviceCommandMessageHandler(IDeviceHostRegistry hostRegistry)
    {
      _hostRegistry = hostRegistry;
    }

    protected override void HandleMessage(DeviceCommandMessageBase message)
    {
      _hostRegistry.ForwardCommand(message);
    }
  }
}
