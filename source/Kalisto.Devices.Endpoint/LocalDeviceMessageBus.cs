﻿using Kalisto.Devices.BarcodeScanner.Messages;
using Kalisto.Devices.Core.Messages;
using System;

namespace Kalisto.Devices.Endpoint
{
  public interface ILocalDeviceMessageBus
  {
    event Action<BarcodeScannerMessage> BarcodeScannerMessageReceived;
  }

  public interface ILocalDeviceMessageBusPropagator
  {
    void Propagate<TMessage>(TMessage message) where TMessage : DeviceMessageBase;
  }

  public class LocalDeviceMessageBus : ILocalDeviceMessageBus, ILocalDeviceMessageBusPropagator
  {
    public event Action<BarcodeScannerMessage> BarcodeScannerMessageReceived;

    public void Propagate<TMessage>(TMessage message) where TMessage : DeviceMessageBase
    {
      if (message is BarcodeScannerMessage msg)
        BarcodeScannerMessageReceived?.Invoke(msg);
    }
  }
}
