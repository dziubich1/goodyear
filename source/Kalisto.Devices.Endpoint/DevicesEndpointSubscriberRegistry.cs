﻿using Kalisto.Communication.Core.Communication;
using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Communication.Core.Registries;
using Kalisto.Devices.Core.Messages;
using Kalisto.Devices.Core.Services;
using Kalisto.Devices.Endpoint.Handlers;

namespace Kalisto.Devices.Endpoint
{
  public class DevicesEndpointSubscriberRegistry : SubscriberRegistry
  {
    private readonly ClientInfo _clientInfo;

    private readonly DeviceEndpointMessageHandlerFactory _messageHandlerFactory;

    private readonly ICommunicationComponentFactory _communicationComponentFactory;

    public DevicesEndpointSubscriberRegistry(ClientInfo clientInfo, DeviceEndpointMessageHandlerFactory messageHandlerFactory,
      ICommunicationComponentFactory communicationComponentFactory)
    {
      _clientInfo = clientInfo;
      _messageHandlerFactory = messageHandlerFactory;
      _communicationComponentFactory = communicationComponentFactory;
    }

    public override void Initialize()
    {
      var clientMessageReceiver = _communicationComponentFactory.GetClientMessageReceiver(_clientInfo.Id);
      RegisterSubscriber(clientMessageReceiver, PartnerRegistrationResponseMessage.Identifier,
        _messageHandlerFactory.GetRegistrationMessageHandler());

      RegisterSubscriber(clientMessageReceiver, SystemVersionCompatibilityCheckResponseMessage.Identifier,
        _messageHandlerFactory.GetSystemVersionCheckResponseMessageHandler());

      RegisterSubscriber(clientMessageReceiver, ResultMessage.Identifier,
        _messageHandlerFactory.GetResultMessageHandler());

      RegisterSubscriber(clientMessageReceiver, DeviceConfigurationsResponseMessage.Identifier,
        _messageHandlerFactory.GetDeviceConfigurationResponseHandler());

      RegisterSubscriber(clientMessageReceiver, DeviceCommandMessageBase.Identifier,
        _messageHandlerFactory.GetDeviceCommandMessageHandler());
    }
  }
}
