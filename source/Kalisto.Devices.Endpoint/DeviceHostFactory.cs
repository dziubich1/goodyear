﻿using Kalisto.Configuration.Devices;
using Unity;

namespace Kalisto.Devices.Endpoint
{
  public class DeviceHostFactory
  {
    private readonly IUnityContainer _container;

    public DeviceHostFactory(IUnityContainer container)
    {
      _container = container;
    }

    public DeviceHost Create(DeviceConfiguration configuration)
    {
      var host = _container.Resolve<DeviceHost>();
      host.Configure(configuration);

      return host;
    }
  }
}
