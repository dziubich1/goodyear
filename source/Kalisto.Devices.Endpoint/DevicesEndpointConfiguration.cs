﻿using Kalisto.Communication.RabbitMq;
using Kalisto.Configuration;

namespace Kalisto.Devices.Endpoint
{
  public class DevicesEndpointConfiguration : ConfigurationBase
  {
    public RabbitMqConnectionConfiguration RabbitMqConnection { get; set; }

    public override object GetDefaultConfiguration()
    {
      return new DevicesEndpointConfiguration
      {
        RabbitMqConnection = new RabbitMqConnectionConfiguration
        {
          Hostname = "127.0.0.1",
          Username = "admin",
          Password = "admin",
          Port = 5672
        }
      };
    }
  }
}
