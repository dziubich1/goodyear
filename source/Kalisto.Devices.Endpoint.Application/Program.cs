﻿using log4net.Config;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using Kalisto.Devices.Core.Communication.SerialPort;
using Kalisto.Devices.Core.Communication.Sockets.Tcp;
using Kalisto.Devices.IOController;
using Kalisto.Devices.IOController.Messages.Outgoing;
using Unity;

namespace Kalisto.Devices.Endpoint.Application
{
  class Program
  {
    static void Main(string[] args)
    {
      XmlConfigurator.Configure();

      Console.WriteLine(@"Kalisto.Devices.Endpoint has been started successfully.");
      Console.WriteLine(@"Press ENTER to stop.");

      var appName = typeof(DevicesEndpoint).Assembly.GetName().Name;
      var endpoint = new DevicesEndpoint("Kalisto", appName);
      endpoint.Configure();
      endpoint.Initialize();

      //new TcpClientConnection("192.168.1.210", 23, 10000, 10000, true)
      //new SerialPortConnection("COM3", 9600, Parity.None, 8, StopBits.One)
      //var device = new IOControllerDevice(new SerialPortConnection("COM3", 9600, Parity.None, 8, StopBits.One), 
      //  new IOControllerMessageChunkAggregator(new IOControllerMessageTranslator()));
      //device.StartSampling();
      //Console.ReadLine();
      //device.SendCommand(new GetWeightMeterValueMessage());
      //Console.ReadLine();
    }
  }
}
