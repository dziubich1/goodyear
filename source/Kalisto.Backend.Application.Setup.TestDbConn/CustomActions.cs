﻿using System.Data.SqlClient;
using System;
using System.Diagnostics;
using System.IO;
using Microsoft.Deployment.WindowsInstaller;

namespace Kalisto.Backend.Application.Setup.TestDbConn
{
  public class CustomActions
  {
    [CustomAction]
    public static ActionResult TestDbConnection(Session session)
    {
      session.Log("Testing db connection");
      var connectionString = session["DATABASE_CONNECTIONSTRING"];
      session.Log($"Connection string: {connectionString}");

      using (var connection = new SqlConnection(connectionString))
      {
        try
        {
          connection.Open();
          session["TEST_DB_CONNECTION_RESULT"] = "Połączenie z bazą danych MSSQL zakończone sukcesem!";
        }
        catch
        {
          session["TEST_DB_CONNECTION_RESULT"] = "Połączenie z bazą danych MSSQL nie powiodło się.";
          return ActionResult.Failure;
        }

        return ActionResult.Success;
      }

    }

    [CustomAction]
    public static ActionResult CreateConfigFile(Session session)
    {

      try
      {
        var connectionString = session["DATABASE_CONNECTIONSTRING"];
        var isFirebird = session["IS_FIREBIRD"] == "1" ? "Firebird" : "MsSql";

        var path = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);
        path = Path.Combine(path, "Kalisto", "Kalisto.Backend");

        if (!Directory.Exists(path))
          Directory.CreateDirectory(path);

        var filePath = Path.Combine(path, "Config.xml");
        using (StreamWriter sw = File.CreateText(filePath))
        {
          sw.WriteLine("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
          sw.WriteLine(
            "<BackendConfiguration xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\">");
          sw.WriteLine(
            "<DbConnection>");
          sw.WriteLine(
            $"<DatabaseType>{isFirebird}</DatabaseType>");
          sw.WriteLine(
            $"<ConnectionString>{connectionString}</ConnectionString>");
          sw.WriteLine(
            "</DbConnection>");
          sw.WriteLine(
            "<RabbitMqConnection>");
          sw.WriteLine(
            "<Hostname>127.0.0.1</Hostname>");
          sw.WriteLine(
            "<Username>admin</Username>");
          sw.WriteLine(
            "<Password>admin</Password>");
          sw.WriteLine(
            "<Port>5672</Port>");
          sw.WriteLine(
            "</RabbitMqConnection>");
          sw.WriteLine(
            "<ClientApplicationLogoFilePath/>");
          sw.WriteLine(
            "<ClientReceiptLogoFilePath/>");
          sw.WriteLine(
            "<PhotoDirectoryName>photos</PhotoDirectoryName>");
          sw.WriteLine("</BackendConfiguration>");
        }

        return ActionResult.Success;
      }
      catch (Exception e)
      {
        return ActionResult.Success;
      }
    }
  }
}
