﻿using System;
using System.Collections.Generic;
using Kalisto.Devices.CardReader.Messages;
using Kalisto.Devices.Core;

namespace Kalisto.Devices.CardReader
{
  public class CardReaderMessageChunkAggregator : IMessageChunkAggregator<CardReaderMessage>
  {
    private readonly IMessageTranslator<CardReaderMessage> _messageTranslator;

    private readonly List<byte> _buffer = new List<byte>();

    private readonly List<byte> _messageData = new List<byte>();

    public event Action<CardReaderMessage> MessageReady;

    public CardReaderMessageChunkAggregator(IMessageTranslator<CardReaderMessage> messageTranslator)
    {
      _messageTranslator = messageTranslator;
    }

    public void AddChunk(byte[] bytes)
    {
      if (_buffer.Count < CardReaderMessage.MessageLength)
        _buffer.AddRange(bytes);

      if (_buffer.Count < CardReaderMessage.MessageLength)
        return;

      // message found
      _messageData.Clear();
      var bufferArr = _buffer.ToArray();
      for (int i = 0; i < CardReaderMessage.MessageLength; i++)
        _messageData.Add(bufferArr[i]); // move whole buffer to message

      for (int i = CardReaderMessage.MessageLength - 1; i >= 0; i--)
        _buffer.RemoveAt(i); // clear buffer, but leave part of next message

      var arr = _messageData.ToArray();
      var message = _messageTranslator.Translate(arr);
      if (message != null)
        MessageReady?.Invoke(message);

      _messageData.Clear();
    }
  }
}
