﻿using Kalisto.Devices.CardReader.Messages;
using Kalisto.Devices.Core.Handlers;

namespace Kalisto.Devices.CardReader.Handlers
{
  public interface ICardReaderMessageHandler : IDeviceMessageHandler
  {
    void HandleCardReaderMessage(CardReaderMessage message);
  }
}
