﻿using Kalisto.Devices.CardReader.Handlers;
using Kalisto.Devices.Core.Messages;
using System;

namespace Kalisto.Devices.CardReader.Messages
{
  [Serializable]
  public class CardReaderMessage : DeviceMessage<ICardReaderMessageHandler>
  {
    public const int MessageLength = 18;

    public string Code { get; set; }

    protected override void Handle(ICardReaderMessageHandler handler)
    {
      handler.HandleCardReaderMessage(this);
    }
  }
}
