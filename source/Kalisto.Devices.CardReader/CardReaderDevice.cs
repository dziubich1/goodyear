﻿using Kalisto.Devices.CardReader.Messages;
using Kalisto.Devices.Core;
using Kalisto.Devices.Core.Communication;
using Kalisto.Devices.Core.Communication.Interfaces;
using Kalisto.Devices.Core.Messages;
using System;

namespace Kalisto.Devices.CardReader
{
  public class CardReaderDevice : IDevice<CardReaderMessage>
  {
    private readonly IDeviceClientConnection _clientConnection;

    private readonly IMessageChunkAggregator<CardReaderMessage> _messageChunkAggregator;

    private string _endpointAddress;

    public int Id { get; set; }

    public event Action<CardReaderMessage> MessageReceived;

    public event Action<ConnectionState> ConnectionStateChanged;

    public CardReaderDevice(IDeviceClientConnection clientConnection,
      IMessageChunkAggregator<CardReaderMessage> messageChunkAggregator)
    {
      _clientConnection = clientConnection;
      _clientConnection.BytesMessageReceived += OnBytesReceived;
      _clientConnection.ConnectionStateChanged += OnConnectionStateChanged;
      _messageChunkAggregator = messageChunkAggregator;
      _messageChunkAggregator.MessageReady += OnMessageReady;
    }

    public void SendCommand<TCommand>(TCommand command) where TCommand : DeviceCommandMessageBase
    {
      // card reader does not handle commands
    }

    public void StartSampling()
    {
      _clientConnection.Connect(true);
    }

    public void StopSampling()
    {
      _clientConnection.Disconnect();
    }

    protected virtual void OnConnectionStateChanged(ConnectionState obj)
    {
      ConnectionStateChanged?.Invoke(obj);
    }

    private void OnBytesReceived(object sender, BytesMessageReceivedEventArgs e)
    {
      _endpointAddress = e.EndpointId;
      _messageChunkAggregator.AddChunk(e.Bytes);
    }

    private void OnMessageReady(CardReaderMessage message)
    {
      message.DeviceId = Id;
      if (string.IsNullOrEmpty(message.EndpointAddress))
        message.EndpointAddress = _endpointAddress;
      MessageReceived?.Invoke(message);
    }
  }
}
