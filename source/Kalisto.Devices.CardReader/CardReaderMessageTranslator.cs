﻿using Kalisto.Devices.CardReader.Messages;
using Kalisto.Devices.Core;
using System.Text;

namespace Kalisto.Devices.CardReader
{
  public class CardReaderMessageTranslator : IMessageTranslator<CardReaderMessage>
  {
    public CardReaderMessage Translate(byte[] bytes)
    {
      if (bytes.Length != CardReaderMessage.MessageLength)
        return null;

      var cardCode = Encoding.ASCII.GetString(bytes).Trim();
      return new CardReaderMessage
      {
        Code = cardCode
      };
    }
  }
}
