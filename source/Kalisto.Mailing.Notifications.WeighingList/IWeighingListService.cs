﻿using Kalisto.Backend.DataAccess.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Kalisto.Mailing.Notifications.WeighingList
{
  public interface IWeighingListService
  {
    Task<List<Measurement>> GetWeighingListAsync(DateTime fromDate, DateTime toDate);
  }
}
