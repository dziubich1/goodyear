﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Localization;
using Kalisto.Mailing.Notifications.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Kalisto.Hmi.Dialogs.Common;

namespace Kalisto.Mailing.Notifications.WeighingList
{
  // TODO: Consider changing naming - weighing list could be replaced with e.g. Measurement report or something like that
  public class WeighingListNotification : NotificationMessage
  {
    private readonly IWeighingListService _weighingListService;

    private readonly IUnitTypeService _unitTypeService;

    private readonly NotificationSettingsConfigurationManager _configurationManager;

    private string _htmlNotificationMessage;

    private string _subject;

    public WeighingListNotification(IWeighingListService weighingListService, IUnitTypeService unitTypeService, NotificationSettingsConfigurationManager configurationManager)
    {
      _weighingListService = weighingListService;
      _unitTypeService = unitTypeService;
      _configurationManager = configurationManager;
      _htmlNotificationMessage = string.Empty;
      _subject = string.Empty;
    }

    public override async void Generate()
    {
      var notificationSettings = _configurationManager.Load<NotificationSettings>();
      if (notificationSettings == null)
        return;

      var utcNow = DateTime.UtcNow;
      var fromUtc = utcNow.AddDays(-1).Date;
      if (notificationSettings.Type == NotificationType.Weekly)
        fromUtc = utcNow.AddDays(-7).Date;
      if (notificationSettings.Type == NotificationType.Monthly)
        fromUtc = utcNow.AddMonths(-1).Date;

      var measurements = await _weighingListService.GetWeighingListAsync(fromUtc, utcNow);
      if (measurements == null)
        return;

      GenerateHtmlMessage(measurements, fromUtc, utcNow);
    }

    public override string GetRawHtmlString()
    {
      return _htmlNotificationMessage;
    }

    public override string Subject => _subject;

    private void GenerateHtmlMessage(List<Measurement> measurements, DateTime fromUtc, DateTime toUtc)
    {
      // TODO: Add some localization here
      _subject =
        $"Lista ważeń z okresu: {new LocalizedDate(fromUtc.ToLocalTime()).LocalizedDateValue} - {new LocalizedDate(toUtc.ToLocalTime()).LocalizedDateValue}";

      StringBuilder htmlMessage = new StringBuilder();
      htmlMessage.Append("<html xmlns=\"http://www.w3.org/1999/xhtml\">");
      htmlMessage.Append("<head><title></title></head><body>");
      htmlMessage.Append($"<h3>Lista ważeń z okresu: { new LocalizedDate(fromUtc.ToLocalTime()).LocalizedDateValue } - { new LocalizedDate(toUtc.ToLocalTime()).LocalizedDateValue }</h3>");
      htmlMessage.Append("</br></br>");

      if (measurements.Any())
      {
        htmlMessage.Append("<table border=\"1\">");
        htmlMessage.Append("<tr><th>Numer</th><th>Typ</th><th>Rodzaj</th><th>Data pomiaru I</th><th>Data pomiaru II</th>");
        htmlMessage.Append($"<th>Pomiar I [{_unitTypeService.GetLocalizedCurrentUnit()}]</th><th>Pomiar II [{_unitTypeService.GetLocalizedCurrentUnit()}]</th><th>Netto [{_unitTypeService.GetLocalizedCurrentUnit()}]</th><th>Kontrahent</th><th>Towar</th></tr>");
        foreach (var meas in measurements)
        {
          htmlMessage.Append($"<tr><td>{meas.Number}</td><td>{new LocalizedEnum(meas.Type).LocalizedEnumValue}</td><td>{new LocalizedEnum(meas.Class).LocalizedEnumValue}</td><td>{new LocalizedDate(meas.Date1Utc.ToLocalTime()).LocalizedDateValue}</td><td>{new LocalizedDate(meas.Date2Utc?.ToLocalTime()).LocalizedDateValue}");
          htmlMessage.Append($"<td>{new UnitOfMeasurement(meas.Measurement1, _unitTypeService).DisplayValueUnitless}</td><td>{new UnitOfMeasurement(meas.Measurement2, _unitTypeService).DisplayValueUnitless}</td><td>{new UnitOfMeasurement(meas.NetWeight, _unitTypeService).DisplayValueUnitless}</td><td>{meas.Contractor?.Name}</td><td>{meas.Cargo?.Name}</td></tr>");
        }
        htmlMessage.Append("</table>");
      }
      else
      {
        htmlMessage.Append("<h5>Brak ważeń wykonanych w tym okresie</h5>");
      }

      htmlMessage.Append("</br></br>");
      htmlMessage.Append("Powiadomienie zostało wygenerowane automatycznie przez system WagaPro 2.0");
      htmlMessage.Append("</body>");
      htmlMessage.Append("</html>");

      _htmlNotificationMessage = htmlMessage.ToString();
    }
  }
}
