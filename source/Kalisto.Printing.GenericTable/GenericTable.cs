﻿using System;

namespace Kalisto.Printing.GenericTable
{
  [Serializable]
  public class GenericTable
  {
    public string Name { get; set; }

    public GenericTableColumn[] Columns { get; set; }

    public GenericTableRow[] Rows { get; set; }
  }
}
