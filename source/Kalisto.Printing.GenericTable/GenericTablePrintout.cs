﻿using System;
using System.Drawing;
using System.Linq;
using Kalisto.Localization;
using Kalisto.Localization.Common;
using Kalisto.Printouts;
using Kalisto.Printouts.Documents;
using Kalisto.Printouts.Labels;
using Kalisto.Printouts.Labels.Styles;
using Kalisto.Printouts.Lines;
using Kalisto.Printouts.Tables;
using Kalisto.Printouts.Tables.Cells;
using Kalisto.Printouts.Tables.Columns;
using Kalisto.Printouts.Tables.Rows;

namespace Kalisto.Printing.GenericTable
{
  public class GenericTablePrintout : PrintoutBase
  {
    private readonly ILocalizer<Localization.Printouts.Printouts> _printoutLocalizer;
    private readonly GenericTable _table;

    public GenericTablePrintout(GenericTable table)
    {
      _table = table;
      _printoutLocalizer = new LocalizationManager<Localization.Printouts.Printouts>();
    }

    public override bool Print(int page)
    {
      var printoutBuilder = new PrintDocumentBuilder(Graphics, PrintingArea);

      var printableArea = PrintingArea;
      printoutBuilder.WithLabel(new PrintLabelBuilder()
        .WithText(DateTime.Now.ToString("dd.MM.yyyy r."))
        .AsRegular()
        .WithFontColor(Color.Black)
        .WithFontFamily("Arial")
        .WithFontSize(9)
        .WithVerticalOffset(printableArea.Top)
        .AlignRight()
        .Submit());

      printoutBuilder.WithLabel(new PrintLabelBuilder()
        .WithText(_table.Name)
        .AsBold()
        .WithFontColor(Color.Black)
        .WithFontFamily("Arial")
        .WithFontSize(15)
        .WithVerticalOffset(printableArea.Top + 100)
        .Center()
        .Submit());

      var footerLabel = new PrintLabelBuilder()
        .WithText(_printoutLocalizer.Localize(() => Localization.Printouts.Printouts.PrintedByWagaPro))
        .AsRegular()
        .WithFontColor(Color.Black)
        .WithFontFamily("Arial")
        .WithFontSize(7)
        .WithHorizontalOffset(printableArea.Left)
        .AlignBottom()
        .Submit();
      printoutBuilder.WithLabel(footerLabel);

      printoutBuilder.WithLine(new PrintLineBuilder()
        .StartsAt((int) printableArea.Left, (int) (printableArea.Bottom - footerLabel.GetLabelHeight(Graphics) - 3))
        .EndsAt((int) (printableArea.Width / 2),
          (int) (printableArea.Bottom - footerLabel.GetLabelHeight(Graphics) - 3))
        .WithThickness(0.25f)
        .Submit());

      var itemsPerPage = 1;
      var tablePlaceholderHeight =
        Convert.ToInt32(printableArea.Bottom - footerLabel.GetLabelHeight(Graphics) - 50 - 150 - 20);
      if (tablePlaceholderHeight % 20 != 0)
        itemsPerPage = (int) Math.Round(tablePlaceholderHeight / 20.0);

      decimal pagesCountDecimal = (decimal) _table.Rows.Length / (decimal) itemsPerPage;
      var totalPages = (int) Math.Round(pagesCountDecimal);
      if (_table.Rows.Length - itemsPerPage * totalPages > 0)
        totalPages++;

      printoutBuilder.WithLabel(new PrintLabelBuilder()
        .WithText(string.Format(_printoutLocalizer.Localize(() => Localization.Printouts.Printouts.PageNumber), page, totalPages))
        .AsRegular()
        .WithFontColor(Color.Black)
        .WithFontFamily("Arial")
        .WithFontSize(7)
        .AlignRight()
        .AlignBottom()
        .Submit());

      var actualTableWidth = _table.Columns.Sum(c => c.ActualWidth);
      var printableAreaWidth = PrintingArea.Width - 100;
      var widthRatio = 1f;
      if (actualTableWidth > printableAreaWidth)
        widthRatio = actualTableWidth / printableAreaWidth;

      var table = new PrintTableBuilder()
        .WithColumns(_table.Columns.Select(c => new PrintTableColumnBuilder()
          .WithText(c.Header)
          .WithWidth(c.ActualWidth / widthRatio)
          .Submit()).ToArray())
        .WithHeaderStyle(new PrintLabelStyleBuilder()
          .AsBold()
          .WithFontColor(Color.Black)
          .WithFontFamily("Arial")
          .WithFontSize(9)
          .Submit())
        .WithCellStyle(new PrintLabelStyleBuilder()
          .AsRegular()
          .WithFontColor(Color.Black)
          .WithFontFamily("Arial")
          .WithFontSize(7)
          .Submit())
        .WithBorderThickness(0.25f)
        .WithVerticalOffset(printableArea.Top + 150)
        .WithDataSource(
          _table.Rows.Skip((page - 1) * itemsPerPage).Take(itemsPerPage).Select(c => new PrintTableRow
          {
            Cells = c.Items.Select(GetTableCell).ToArray()
          }).ToArray())
        .Submit();
      printoutBuilder.WithTable(table);

      return page < totalPages;
    }

    private PrintTableCell GetTableCell(GenericTableItem item)
    {
      var cellBuilder = new PrintTableCellBuilder()
        .WithText(string.Empty);

      if (item == null)
        return cellBuilder.Submit();

      if (item.Type == typeof(bool) || item.Type == typeof(Boolean))
      {
        var localizer = new LocalizationManager<Common>();
        item.Value = localizer.Localize(item.Value);
      }

      cellBuilder.WithText(item.Value);
      if (item.Type == typeof(string))
        cellBuilder.AlignLeft();

      if (decimal.TryParse(item.Value, out _))
        cellBuilder.AlignRight();
      else cellBuilder.AlignLeft();

      return cellBuilder.Submit();
    }
  }
}
