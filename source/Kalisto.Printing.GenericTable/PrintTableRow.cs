﻿using System;

namespace Kalisto.Printing.GenericTable
{
  [Serializable]
  public class GenericTableRow
  {
    public GenericTableItem[] Items { get; set; }
  }
}
