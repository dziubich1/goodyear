﻿using System;
using System.Xml.Serialization;

namespace Kalisto.Printing.GenericTable
{
  [Serializable]
  public class GenericTableColumn
  {
    public string Header { get; set; }

    [XmlIgnore]
    public float ActualWidth { get; set; }
  }
}
