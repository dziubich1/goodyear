﻿using System;
using System.Xml.Serialization;

namespace Kalisto.Printing.GenericTable
{
  [Serializable]
  public class GenericTableItem
  {
    public string Value { get; set; }

    [XmlIgnore]
    public Type Type { get; set; }
  }
}
