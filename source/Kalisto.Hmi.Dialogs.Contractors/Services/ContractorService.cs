﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.Operations.Contractors.Commands;
using Kalisto.Backend.Operations.Contractors.Queries;
using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Hmi.Core.Communication;
using Kalisto.Hmi.Core.Services;
using Kalisto.Hmi.Dialogs.Contractors.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kalisto.Hmi.Dialogs.Contractors.Services
{
  public interface IContractorService : IBackendOperationService
  {
    Task<RequestResult<List<ContractorModel>>> GetAllContractorsAsync();

    Task<RequestResult<long>> AddContractorAsync(string name, string address,
      string postalCode, string city, string taxId);

    Task<RequestResult<long>> UpdateContractorAsync(long id, string name, string address,
      string postalCode, string city, string taxId);

    Task<RequestResult<long>> DeleteContractorAsync(long id);

    Task<RequestResult<ContractorModel>> GetOrAddContractorAsync(string name);
  }

  public class ContractorService : BackendOperationService, IContractorService
  {
    public ContractorService(ClientInfo clientInfo, IMessageBuilder messageBuilder, IFrontendMessageBridge messageBridge, IDialogService dialogService)
    : base(clientInfo, messageBuilder, messageBridge, dialogService)
    {
    }

    public async Task<RequestResult<List<ContractorModel>>> GetAllContractorsAsync()
    {
      var message = MessageBuilder.CreateNew<GetAllContractorsQuery>()
        .BasedOn(ClientInfo)
        .Build();

      var result = await MessageBridge.SendMessageWithResultAsync<List<Contractor>>(message);
      return new RequestResult<List<ContractorModel>>
      {
        Succeeded = result?.Succeeded ?? false,
        Response = result?.Response?.Select((c, index) => new ContractorModel
        {
          Id = c.Id,
          OrdinalId = index + 1,
          Name = c.Name,
          Address = c.Address,
          PostalCode = c.PostalCode,
          City = c.City,
          TaxId = c.TaxId
        }).ToList() ?? new List<ContractorModel>()
      };
    }

    public async Task<RequestResult<long>> AddContractorAsync(string name, string address,
      string postalCode, string city, string taxId)
    {
      var message = MessageBuilder.CreateNew<CreateContractorCommand>()
        .BasedOn(ClientInfo)
        .WithProperty(c => c.Name, name)
        .WithProperty(c => c.Address, address)
        .WithProperty(c => c.PostalCode, postalCode)
        .WithProperty(c => c.City, city)
        .WithProperty(c => c.TaxId, taxId)
        .Build();

      var result = await MessageBridge.SendMessageWithResultAsync<long>(message);
      return result ?? RequestResult<long>.Default;
    }

    public async Task<RequestResult<long>> UpdateContractorAsync(long id, string name, string address,
      string postalCode, string city, string taxId)
    {
      var message = MessageBuilder.CreateNew<UpdateContractorCommand>()
        .BasedOn(ClientInfo)
        .WithProperty(c => c.ContractorId, id)
        .WithProperty(c => c.Name, name)
        .WithProperty(c => c.Address, address)
        .WithProperty(c => c.PostalCode, postalCode)
        .WithProperty(c => c.City, city)
        .WithProperty(c => c.TaxId, taxId)
        .Build();

      var result = await MessageBridge.SendMessageWithResultAsync<long>(message);
      return result ?? RequestResult<long>.Default;
    }

    public async Task<RequestResult<long>> DeleteContractorAsync(long id)
    {
      var message = MessageBuilder.CreateNew<DeleteContractorCommand>()
        .BasedOn(ClientInfo)
        .WithProperty(c => c.ContractorId, id)
        .Build();

      var result = await MessageBridge.SendMessageWithResultAsync<long>(message);
      return result ?? RequestResult<long>.Default;
    }

    public async Task<RequestResult<ContractorModel>> GetOrAddContractorAsync(string name)
    {
      var message = MessageBuilder.CreateNew<GetOrAddContractorQuery>()
      .BasedOn(ClientInfo)
      .WithProperty(c => c.Name, name)
      .Build();

      var result = await MessageBridge.SendMessageWithResultAsync<Contractor>(message);
      return new RequestResult<ContractorModel>
      {
        Succeeded = result?.Succeeded ?? false,
        Response = result?.Response == null ? null : new ContractorModel
        {
          Id = result.Response.Id,
          Name = result.Response.Name,
          Address = result.Response.Address,
          City = result.Response.City,
          PostalCode = result.Response.PostalCode,
          TaxId = result.Response.TaxId
        }
      };
    }
  }
}
