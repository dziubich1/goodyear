﻿using System.ComponentModel.DataAnnotations;

namespace Kalisto.Hmi.Dialogs.Contractors.Models
{
  public class ContractorModel
  {
    public long Id { get; set; }

    public int OrdinalId { get; set; }

    public string Name { get; set; }

    public string TaxId { get; set; }

    public string Address { get; set; }

    public string City { get; set; }

    public string PostalCode { get; set; }

  }
}
