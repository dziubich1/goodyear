﻿using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Hmi.Core;
using Kalisto.Hmi.Core.Communication;
using Kalisto.Hmi.Core.Services;
using PropertyChanged;

namespace Kalisto.Hmi.Dialogs.Contractors.Dialogs
{
  [AddINotifyPropertyChangedInterface]
  public class AddOrUpdateContractorModalDialogViewModel : ModalViewModelBase<AddOrUpdateContractorModalDialogViewModel>
  {
    public override string DialogNameKey { get; set; }

    public string Name { get; set; }

    public string Address { get; set; }

    public string PostalCode { get; set; }

    public string City { get; set; }

    public string TaxId { get; set; }

    public AddOrUpdateContractorModalDialogViewModel(IDialogService dialogService, IAsyncProgressService asyncProgressService,
      IMessageBuilder messageBuilder, IFrontendMessageBridge messageBridge, ClientInfo clientInfo)
      : base(dialogService, asyncProgressService, messageBuilder, messageBridge, clientInfo)
    {
    }

    protected override void DefineValidationRules()
    {
      InvalidateProperty(() => Name).When(() => string.IsNullOrEmpty(Name))
        .WithMessageKey("Required").ButIgnoreOnLoad();

      InvalidateProperty(() => Name).When(() => Name.Length < 3)
        .WithMessageKey("TooShort").ButIgnoreOnLoad();

      InvalidateProperty(() => TaxId).When(() => !string.IsNullOrEmpty(TaxId) && TaxId.Length != 10)
        .WithMessageKey("TooShortExactMultiple", 10).ButIgnoreOnLoad();
    }
  }
}
