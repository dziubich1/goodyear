﻿using System;
using Kalisto.Backend.Operations.Contractors.Commands.Interfaces;
using Kalisto.Configuration.Permissions;
using Kalisto.Hmi.Core;
using Kalisto.Hmi.Core.Commands;
using Kalisto.Hmi.Core.Services;
using Kalisto.Hmi.Dialogs.Contractors.Dialogs;
using Kalisto.Hmi.Dialogs.Contractors.Models;
using Kalisto.Hmi.Dialogs.Contractors.Services;
using MahApps.Metro.Controls.Dialogs;
using PropertyChanged;
using System.Threading.Tasks;
using System.Windows.Input;
using Kalisto.Localization;
using Kalisto.Localization.Messages;

namespace Kalisto.Hmi.Dialogs.Contractors.ViewModels
{
  [AddINotifyPropertyChangedInterface]
  public class ContractorsViewModel : DataGridViewModel<ContractorModel>
  {
    private readonly ILocalizer<Messages> _localizer;

    private readonly IContractorService _contractorService;

    public override string DialogTitleKey => "Contractors";

    public ICommand AddItemCommand { get; set; }

    public ICommand EditItemCommand { get; set; }

    public ICommand DeleteItemCommand { get; set; }

    public ICommand SyncItemsCommand { get; set; }

    public ObservableCollectionEx<ContractorModel> Contractors { get; set; }

    public ContractorModel SelectedContractor { get; set; }

    protected override Type[] AssociatedCommandTypes => new[] { typeof(IContractorCommand) };

    public ContractorsViewModel(IDialogService dialogService, IAuthorizationService authorizationService, IContractorService contractorService)
      : base(dialogService, authorizationService)
    {
      _localizer = new LocalizationManager<Messages>();
      _contractorService = contractorService;

      AddItemCommand = new DelegateCommand(AddContractor);
      EditItemCommand = new DelegateCommand(EditItem);
      DeleteItemCommand = new DelegateCommand(DeleteContractor);
      SyncItemsCommand = new DelegateCommand(SyncContractors);

      Contractors = new ObservableCollectionEx<ContractorModel>();
    }

    protected override ObservableCollectionEx<ContractorModel> GetItemsSource()
    {
      return Contractors;
    }

    protected override string GetEditRuleName()
    {
      return PermissionRules.DictionaryDataManagement;
    }

    public override async Task RefreshDataAsync()
    {
      await base.RefreshDataAsync();

      await LoadContractors();
    }

    private async Task LoadContractors()
    {
      var result = await _contractorService.GetAllContractorsAsync();
      if (!result.Succeeded)
        return;

      Contractors.Clear();
      Contractors.AddRange(result.Response);
    }

    private async void AddContractor(object parameter)
    {
      await DialogService.ShowModalDialogAsync<AddOrUpdateContractorModalDialogViewModel>(
      vm =>
      {
        vm.DialogNameKey = "AddContractor";
      },
      async viewModel =>
      {
        var result = await _contractorService.AddContractorAsync(viewModel.Name,
          viewModel.Address, viewModel.PostalCode, viewModel.City, viewModel.TaxId);
        return result.Succeeded;
      });
    }

    protected override async void EditItem(object parameter)
    {
      var selectedContractor = (parameter as ContractorModel) ?? SelectedContractor;
      if (selectedContractor == null)
      {
        await DialogService.ShowMessageAsync(_localizer.Localize(() => Messages.Error),
          _localizer.Localize(() => Messages.SelectContractorToUpdate), MessageDialogType.Ok);
        return;
      }

      await DialogService.ShowModalDialogAsync<AddOrUpdateContractorModalDialogViewModel>(vm =>
      {
        vm.DialogNameKey = "EditContractor";
        vm.Name = selectedContractor.Name;
        vm.Address = selectedContractor.Address;
        vm.PostalCode = selectedContractor.PostalCode;
        vm.City = selectedContractor.City;
        vm.TaxId = selectedContractor.TaxId;
        vm.ValidateOnLoad = true;
      },
      async viewModel =>
      {
        var result = await _contractorService.UpdateContractorAsync(selectedContractor.Id,
          viewModel.Name, viewModel.Address, viewModel.PostalCode, viewModel.City, viewModel.TaxId);
        return result.Succeeded;
      });
    }

    private async void DeleteContractor(object parameter)
    {
      if (SelectedContractor == null)
      {
        await DialogService.ShowMessageAsync(_localizer.Localize(() => Messages.Error),
          _localizer.Localize(() => Messages.SelectContractorToDelete), MessageDialogType.Ok);
        return;
      }

      var dlgResult = await DialogService.ShowMessageAsync(_localizer.Localize(() => Messages.Attention),
        _localizer.Localize(() => Messages.DeleteContractorQuestion),
        MessageDialogType.YesNo);

      if (dlgResult != MessageDialogResult.Affirmative)
        return;

      await _contractorService.DeleteContractorAsync(SelectedContractor.Id);
    }

    private async void SyncContractors(object obj)
    {
      await LoadContractors();
    }
  }
}
