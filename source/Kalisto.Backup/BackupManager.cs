﻿using System;
using System.IO;
using System.Xml;
using Kalisto.Backup.Interfaces;

namespace Kalisto.Backup
{
    public class BackupManager : IBackupManager
    {
        XmlDocument doc = new XmlDocument();
        private string selectedPath;
        private string dbConnection;
        private string dbType;

        public BackupManager()
        {
            Initialize();
        }

        private void Initialize()
        {
            doc.Load(Paths.docUrl);

            XmlNode node = doc.DocumentElement.SelectSingleNode("BackUpPath");
            XmlNode nodeDB = doc.DocumentElement.SelectSingleNode("DbConnection").LastChild;
            XmlNode nodeType = doc.DocumentElement.SelectSingleNode("DbConnection").FirstChild;

            if (node != null)
            {
                selectedPath = (!String.IsNullOrEmpty(node.InnerText) && Directory.Exists(node.InnerText)) ? node.InnerText : Paths.defaultPath;
            }
            else
            {
                selectedPath = Paths.defaultPath;
            }

            dbType = nodeType.InnerText;
            dbConnection = nodeDB.InnerText;
        }

        public bool SaveConfigBackup()
        {
            string destinationDirectory = GetNewPath("Config");
            try
            {
                File.Copy(Paths.devicesFile, destinationDirectory + "\\" + Path.GetFileName(Paths.devicesFile));
                File.Copy(Paths.configFile, destinationDirectory + "\\" + Path.GetFileName(Paths.configFile));
                return true;
            }
            catch(Exception e)
            {
                return false;
            }
        }

        public bool SaveDataBaseConfig()
        {
            string destinationDirectory = GetNewPath("Database");
            string DBDAT = GetBetween(dbConnection, "Database=", ";DataSource");

            if (dbType != "Firebird" || String.IsNullOrEmpty(DBDAT))
            {
                return false;
            }

            else
            {
                try
                {
                    File.Copy(DBDAT, destinationDirectory + "\\" + Path.GetFileName(DBDAT));
                    return true;
                }
                catch(Exception e)
                {
                    return false;
                }
            }
        }

        private string GetNewPath(string finalDestination)
        {
            string Todaysdate = DateTime.Now.ToString("dd-MM-yyyy-HH-mm-ss");
            string currentPath = selectedPath + "\\" + Todaysdate + "\\" + finalDestination;

            if (!Directory.Exists(currentPath))
            {
                Directory.CreateDirectory(currentPath);
            }

            return currentPath;
        }

        private string GetBetween(string strSource, string strStart, string strEnd)
        {
            const int kNotFound = -1;

            var startIdx = strSource.IndexOf(strStart);
            if (startIdx != kNotFound)
            {
                startIdx += strStart.Length;
                var endIdx = strSource.IndexOf(strEnd, startIdx);
                if (endIdx > startIdx)
                {
                    return strSource.Substring(startIdx, endIdx - startIdx);
                }
            }
            return String.Empty;
        }

       
    }
}
