﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kalisto.Backup.Interfaces
{
    interface IBackupManager
    {
        bool SaveConfigBackup();

        bool SaveDataBaseConfig();
    }
}
