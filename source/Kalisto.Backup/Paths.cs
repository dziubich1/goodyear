﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Kalisto.Backup
{
    class Paths
    {
        public static string docUrl = "C:\\ProgramData\\Kalisto\\Kalisto.Backend\\config.xml";
        public static string defaultPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\Kalisto\\Backup";
        public static string devicesFile = @"C:\ProgramData\Kalisto\Kalisto.Backend\devices.xml";
        public static string configFile = @"C:\ProgramData\Kalisto\Kalisto.Backend\config.xml";
    }
}
