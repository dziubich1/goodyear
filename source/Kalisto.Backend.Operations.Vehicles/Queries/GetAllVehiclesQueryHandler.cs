﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Communication.Core.Messaging;
using System.Data.Entity;
using System.Linq;

namespace Kalisto.Backend.Operations.Vehicles.Queries
{
  public class GetAllVehiclesQueryHandler : QueryHandlerBase<GetAllVehiclesQuery>
  {
    public override object Execute(IDbContext context, GetAllVehiclesQuery query, string partnerId)
    {
      return context.Vehicles
        .AsNoTracking()
        .Include(v => v.Driver)
        .Include(v => v.Contractor)
        .ToList();
    }
  }
}
