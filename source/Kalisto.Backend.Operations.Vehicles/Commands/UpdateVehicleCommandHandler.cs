﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.DataAccess.Model.Logging;
using Kalisto.Backend.Operations.Vehicles.Exceptions;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Logging;
using System;
using System.Data.Entity;
using System.Linq;

namespace Kalisto.Backend.Operations.Vehicles.Commands
{
  public class UpdateVehicleCommandHandler : CommandHandlerBase<UpdateVehicleCommand>
  {
    private readonly IDbUpdateEventLogger _dbEventLogger;
    private readonly ILogger _logger;

    public UpdateVehicleCommandHandler(IDbUpdateEventLogger dbEventLogger, ILogger logger)
    {
      _dbEventLogger = dbEventLogger;
      _logger = logger;
    }

    public override object Execute(IDbContext context, UpdateVehicleCommand command, string partnerId)
    {
      var vehicle = context.Vehicles
        .Include(v => v.Driver)
        .Include(v => v.Contractor)
        .FirstOrDefault(c => c.Id == command.VehicleId);
      if (vehicle == null)
      {
        _logger.Error($"Cannot update vehicle with ID: {command.VehicleId} because it does not exist");
        throw new ArgumentException("Vehicle with given identifier does not exist.");
      }

      if (context.Vehicles.Any(c => c.Id != vehicle.Id & c.PlateNumber == command.PlateNumber))
      {
        _logger.Info($"Cannot update vehicle plate number because vehicle with plate number: {command.PlateNumber} already exists. Updated vehicle ID: {vehicle.Id}");
        throw new VehicleExistsException();
      }

      Driver driver = null;
      if (command.DriverId.HasValue)
      {
        driver = context.Drivers.FirstOrDefault(d => d.Id == command.DriverId);
        if (driver == null)
        {
          _logger.Error($"Cannot create vehicle with driver ID: {command.DriverId} because driver with given identifier does not exist");
          throw new ArgumentException("Driver with given identifier does not exist.");
        }
      }

      Contractor contractor = null;
      if (command.ContractorId.HasValue)
      {
        contractor = context.Contractors.FirstOrDefault(d => d.Id == command.ContractorId);
        if (contractor == null)
        {
          _logger.Error($"Cannot create vehicle with contractor ID: {command.ContractorId} because contractor with given identifier does not exist");
          throw new ArgumentException("Contractor with given identifier does not exist.");
        }
      }

      // only for getting current user old values
      var oldVehicle = context.Vehicles.AsNoTracking()
        .FirstOrDefault(v => v.Id == command.VehicleId);

      if (command.Tare.HasValue && command.Tare != vehicle.Tare)
        vehicle.TareDateUtc = DateTime.UtcNow;
      else if (!command.Tare.HasValue)
        vehicle.TareDateUtc = null;

      vehicle.PlateNumber = command.PlateNumber;
      vehicle.Tare = command.Tare;
      vehicle.MaxVehicleWeight = command.MaxVehicleWeight;
      vehicle.Driver = driver;
      vehicle.Contractor = contractor;

      context.SaveChanges();

      _dbEventLogger.LogEvent(partnerId, ctx => ctx.Vehicles, vehicle.Id, vehicle, oldVehicle);
      _logger.Debug($"Successfully created vehicle with ID: {vehicle.Id}");

      return vehicle.Id;
    }
  }
}
