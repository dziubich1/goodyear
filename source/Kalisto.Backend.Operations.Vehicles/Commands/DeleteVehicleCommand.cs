﻿using Kalisto.Backend.Operations.Vehicles.Commands.Interfaces;
using Kalisto.Communication.Core.Messages;

namespace Kalisto.Backend.Operations.Vehicles.Commands
{
  public class DeleteVehicleCommand : BackendDbOperationMessage, IVehicleCommand
  {
    public long VehicleId { get; set; }
  }
}