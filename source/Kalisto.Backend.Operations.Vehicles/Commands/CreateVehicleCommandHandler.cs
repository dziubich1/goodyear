﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.DataAccess.Model.Logging;
using Kalisto.Backend.Operations.Vehicles.Exceptions;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Logging;
using System;
using System.Linq;

namespace Kalisto.Backend.Operations.Vehicles.Commands
{
  public class CreateVehicleCommandHandler : CommandHandlerBase<CreateVehicleCommand>
  {
    private readonly IDbInsertEventLogger _dbEventLogger;
    private readonly ILogger _logger;

    public CreateVehicleCommandHandler(IDbInsertEventLogger dbEventLogger, ILogger logger)
    {
      _dbEventLogger = dbEventLogger;
      _logger = logger;
    }

    public override object Execute(IDbContext context, CreateVehicleCommand command, string partnerId)
    {
      if (context.Vehicles.Any(c => c.PlateNumber == command.PlateNumber))
      {
        _logger.Debug($"Cannot create vehicle with plate number: {command.PlateNumber} because it already exists");
        throw new VehicleExistsException();
      }

      Driver driver = null;
      if (command.DriverId.HasValue)
      {
        driver = context.Drivers.FirstOrDefault(d => d.Id == command.DriverId);
        if (driver == null)
        {
          _logger.Error($"Cannot create vehicle with driver ID: {command.DriverId} because driver with given identifier does not exist");
          throw new ArgumentException("Driver with given identifier does not exist.");
        }
      }

      Contractor contractor = null;
      if (command.ContractorId.HasValue)
      {
        contractor = context.Contractors.FirstOrDefault(d => d.Id == command.ContractorId);
        if (contractor == null)
        {
          _logger.Error($"Cannot create vehicle with contractor ID: {command.ContractorId} because contractor with given identifier does not exist");
          throw new ArgumentException("Contractor with given identifier does not exist.");
        }
      }

      var tare = command.Tare;
      DateTime? tareDate = null;
      if(tare.HasValue)
        tareDate = DateTime.UtcNow;

      var vehicle = new Vehicle
      {
        PlateNumber = command.PlateNumber,
        Tare = tare,
        TareDateUtc = tareDate,
        MaxVehicleWeight = command.MaxVehicleWeight,
        Driver = driver,
        Contractor = contractor
      };

      context.Vehicles.Add(vehicle);
      context.SaveChanges();

      _dbEventLogger.LogEvent(partnerId, ctx => ctx.Vehicles, vehicle.Id, vehicle);
      _logger.Debug($"Successfully created vehicle with ID: {vehicle.Id}");

      return vehicle.Id;
    }
  }
}
