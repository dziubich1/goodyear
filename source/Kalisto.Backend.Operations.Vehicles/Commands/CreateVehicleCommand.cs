﻿using Kalisto.Backend.Operations.Vehicles.Commands.Interfaces;
using Kalisto.Communication.Core.Messages;

namespace Kalisto.Backend.Operations.Vehicles.Commands
{
  public class CreateVehicleCommand : BackendDbOperationMessage, IVehicleCommand
  {
    public string PlateNumber { get; set; }

    public decimal? Tare { get; set; }

    public decimal? MaxVehicleWeight { get; set; }

    public long? DriverId { get; set; }

    public long? ContractorId { get; set; }
  }
}
