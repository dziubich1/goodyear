﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.DataAccess.Model.Logging;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Logging;
using System;
using System.Data.Entity;
using System.Linq;

namespace Kalisto.Backend.Operations.Vehicles.Commands
{
  public class UpdateVehicleTareCommandHandler : CommandHandlerBase<UpdateVehicleTareCommand>
  {
    private readonly IDbUpdateEventLogger _dbEventLogger;
    private readonly ILogger _logger;

    public UpdateVehicleTareCommandHandler(IDbUpdateEventLogger dbEventLogger, ILogger logger)
    {
      _dbEventLogger = dbEventLogger;
      _logger = logger;
    }

    public override object Execute(IDbContext context, UpdateVehicleTareCommand command, string partnerId)
    {
      var vehicle = context.Vehicles.FirstOrDefault(c => c.Id == command.VehicleId);
      if (vehicle == null)
      {
        _logger.Error($"Cannot update tare of vehicle with ID: {command.VehicleId} because it does not exist");
        throw new ArgumentException();
      }

      // only for getting old values
      var oldVehicle = context.Vehicles.AsNoTracking()
        .FirstOrDefault(v => v.Id == command.VehicleId);

      vehicle.Tare = command.Tare;
      vehicle.TareDateUtc = DateTime.UtcNow;
      context.SaveChanges();

      _dbEventLogger.LogEvent(partnerId, ctx => ctx.Vehicles, vehicle.Id, vehicle, oldVehicle);
      _logger.Debug($"Successfully updated tare of vehicle with ID: {vehicle.Id}");

      return vehicle.Id;
    }
  }
}
