﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.DataAccess.Model.Logging;
using Kalisto.Backend.Operations.Vehicles.Exceptions;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Logging;
using System;
using System.Data.Entity;
using System.Linq;

namespace Kalisto.Backend.Operations.Vehicles.Commands
{
  public class DeleteVehicleCommandHandler : CommandHandlerBase<DeleteVehicleCommand>
  {
    private readonly IDbDeleteEventLogger _dbEventLogger;
    private readonly ILogger _logger;

    public DeleteVehicleCommandHandler(IDbDeleteEventLogger dbEventLogger, ILogger logger)
    {
      _dbEventLogger = dbEventLogger;
      _logger = logger;
    }

    public override object Execute(IDbContext context, DeleteVehicleCommand command, string partnerId)
    {
      var vehicle = context.Vehicles
        .Include(v => v.Measurements)
        .Include(v => v.Cards)
        .FirstOrDefault(v => v.Id == command.VehicleId);

      if (vehicle == null)
      {
        _logger.Error($"Cannot delete vehicle with ID: {command.VehicleId} because it does not exist");
        throw new ArgumentException("Vehicle with given identifier does not exist.");
      }

      if (vehicle.Measurements.Count > 0)
      {
        _logger.Info($"Cannot delete vehicle with ID: {command.VehicleId} because it is used in measurements");
        throw new VehicleLockedByMeasurementException();
      }

      if (vehicle.Cards.Count > 0)
      {
        _logger.Debug($"Cannot delete vehicle with ID: {command.VehicleId} because it is used in cards");
        throw new VehicleLockedByCardException();
      }

      context.Vehicles.Remove(vehicle);
      context.SaveChanges();

      _dbEventLogger.LogEvent(partnerId, ctx => ctx.Vehicles, vehicle.Id, null, vehicle);
      _logger.Debug($"Successfully deleted vehicle with ID: {vehicle.Id}");

      return command.VehicleId;
    }
  }
}
