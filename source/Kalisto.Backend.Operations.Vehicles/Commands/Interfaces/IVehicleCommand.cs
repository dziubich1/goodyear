﻿using Kalisto.Communication.Core.Messages;

namespace Kalisto.Backend.Operations.Vehicles.Commands.Interfaces
{
  public interface IVehicleCommand : IAssociatedCommand
  {
  }
}
