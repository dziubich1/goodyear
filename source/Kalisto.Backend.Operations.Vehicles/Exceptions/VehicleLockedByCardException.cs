﻿using Kalisto.Communication.Core.Exceptions;
using System;
using System.Runtime.Serialization;

namespace Kalisto.Backend.Operations.Vehicles.Exceptions
{
  [Serializable]
  public class VehicleLockedByCardException : ExceptionBase
  {
    public VehicleLockedByCardException()
    {
    }

    // REMARKS: below constructor need to be added due to the serialization/deserialization issues
    public VehicleLockedByCardException(SerializationInfo info, StreamingContext context)
    {
    }

    public override string ExceptionCode => "VEHICLES_1003";

    public override string Message => GetLocalizedMessage();
  }
}
