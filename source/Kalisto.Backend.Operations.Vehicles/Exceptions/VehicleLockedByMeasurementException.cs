﻿using Kalisto.Communication.Core.Exceptions;
using System;
using System.Runtime.Serialization;

namespace Kalisto.Backend.Operations.Vehicles.Exceptions
{
  [Serializable]
  public class VehicleLockedByMeasurementException : ExceptionBase
  {
    public VehicleLockedByMeasurementException()
    {
    }

    // REMARKS: below constructor need to be added due to the serialization/deserialization issues
    public VehicleLockedByMeasurementException(SerializationInfo info, StreamingContext context)
    {
    }

    public override string ExceptionCode => "VEHICLES_1002";

    public override string Message => GetLocalizedMessage();
  }
}
