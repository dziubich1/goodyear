﻿using Kalisto.Communication.Core.Exceptions;
using System;
using System.Runtime.Serialization;

namespace Kalisto.Backend.Operations.Vehicles.Exceptions
{
  [Serializable]
  public class VehicleExistsException : ExceptionBase
  {
    public VehicleExistsException()
    {
    }

    // REMARKS: below constructor need to be added due to the serialization/deserialization issues
    public VehicleExistsException(SerializationInfo info, StreamingContext context)
    {
    }

    public override string ExceptionCode => "VEHICLES_1001";

    public override string Message => GetLocalizedMessage();
  }
}
