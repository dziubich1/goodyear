﻿namespace Kalisto.Printing.WeighingReceipt
{
    public class WeighingReceiptDto
    {
        public string Header { get; set; }

        public string CompanyAddress { get; set; }

        public string MeasurementDate { get; set; }

        public string FirstMeasurementDate { get; set; }

        public string SecondMeasurementDate { get; set; }

        public string Contractor { get; set; }

        public string Driver { get; set; }

        public string Car { get; set; }

        public string Comment { get; set; }

        public string Operator { get; set; }

        public WeightReceiptEntryDto WeightEntry { get; set; }

        public string CompanyLogo { get; set; }
        public string FirstMeasurementPhoto1 { get; set; }

        public string FirstMeasurementPhoto2 { get; set; }

        public string FirstMeasurementPhoto3 { get; set; }

        public string SecondMeasurementPhoto1 { get; set; }

        public string SecondMeasurementPhoto2 { get; set; }

        public string SecondMeasurementPhoto3 { get; set; }
    }

    public class WeightReceiptEntryDto
    {
        public string Cargo { get; set; }
        public string FirstMeasurement { get; set; }
        public string SecondMeasurement { get; set; }

        public string Net { get; set; }

        public string Gross { get; set; }

        public string Tare { get; set; }
    }
}