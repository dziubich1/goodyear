﻿using Kalisto.Core;
using Kalisto.Localization;
using Kalisto.Localization.Common;
using Kalisto.Printing.Layout;
using Kalisto.Printouts;
using Kalisto.Printouts.Documents;
using Kalisto.Printouts.Images;
using Kalisto.Printouts.Labels;
using Kalisto.Printouts.Labels.Styles;
using Kalisto.Printouts.Lines;
using Kalisto.Printouts.Tables;
using Kalisto.Printouts.Tables.Cells;
using Kalisto.Printouts.Tables.Columns;
using Kalisto.Printouts.Tables.Rows;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace Kalisto.Printing.WeighingReceipt
{
    public class WeightReceiptPrintout : PrintoutBase
    {
        private readonly ILocalizer<Common> _localizer;

        private readonly WeighingReceiptDto _dto;

        private readonly bool _includeImages;

        private PrintDocumentBuilder _printoutBuilder;

        private bool _doubleCopy;

        private float _globalVerticalOffset;

        public WeightReceiptPrintout(WeighingReceiptDto dto, bool doubleCopy, bool includeImages)
        {
            _dto = dto;
            _doubleCopy = doubleCopy;
            _includeImages = includeImages;
            _localizer = new LocalizationManager<Common>();
        }

        private int GetGridColumnCount()
        {
            return Math.Max(GetPhotoList(1).Count, GetPhotoList(2).Count);
        }

        private int CalculateGridHeight()
        {
            var maxHeight = (int)PrintingArea.Height / 2 - 100;
            var photoList = GetPhotoList(1);
            if (!photoList.Any())
                return 0;

            var initWidth = PrintingArea.Width;
            var approxColumnWidth = (int)initWidth / photoList.Count;
            var samplePhoto = photoList.First();
            var resizedPhoto = samplePhoto.ToImage().ResizeImage(approxColumnWidth, maxHeight);
            return resizedPhoto.Height;
        }

        private List<string> GetPhotoList(int measNumber)
        {
            var photoList = new List<string>();

            if (measNumber == 1)
            {
                if (!string.IsNullOrEmpty(_dto.FirstMeasurementPhoto1))
                    photoList.Add(_dto.FirstMeasurementPhoto1);
                if (!string.IsNullOrEmpty(_dto.FirstMeasurementPhoto2))
                    photoList.Add(_dto.FirstMeasurementPhoto2);
                if (!string.IsNullOrEmpty(_dto.FirstMeasurementPhoto3))
                    photoList.Add(_dto.FirstMeasurementPhoto3);
            }

            if (measNumber == 2)
            {
                if (!string.IsNullOrEmpty(_dto.SecondMeasurementPhoto1))
                    photoList.Add(_dto.SecondMeasurementPhoto1);
                if (!string.IsNullOrEmpty(_dto.SecondMeasurementPhoto2))
                    photoList.Add(_dto.SecondMeasurementPhoto2);
                if (!string.IsNullOrEmpty(_dto.SecondMeasurementPhoto3))
                    photoList.Add(_dto.SecondMeasurementPhoto3);
            }

            return photoList;
        }

        public override bool Print(int page)
        {
            if (_printoutBuilder == null || page == 2)
                _printoutBuilder = new PrintDocumentBuilder(Graphics, PrintingArea);

            if (_includeImages && page == 2)
            {
                var gridLayoutHeight = CalculateGridHeight();
                if (string.IsNullOrEmpty(_dto.FirstMeasurementPhoto1) && string.IsNullOrEmpty(_dto.FirstMeasurementPhoto2) && string.IsNullOrEmpty(_dto.FirstMeasurementPhoto3))
                    return false;

                var header1 = new PrintLabelBuilder()
                  .WithText($"{_localizer.Localize(() => Common.MeasurementNr)} 1")
                  .AsBold()
                  .WithFontColor(Color.Black)
                  .WithFontFamily("Arial")
                  .WithFontSize(15)
                  .Center()
                  .WithVerticalOffset(30)
                  .Submit();
                _printoutBuilder.WithLabel(header1);

                var header1Height = (int)header1.GetLabelHeight(Graphics);
                var layout1 = new GridLayoutBuilder()
                  .WithRows(1)
                  .WithColumns(GetGridColumnCount())
                  .WithWidth((int)PrintingArea.Width)
                  .WithHeight(gridLayoutHeight)
                  .WithMargin(20)
                  .WithPadding(5)
                  .WithVerticalOffset(header1Height + (int)header1.Y)
                  .Build();

                var images1 = GetPhotoList(1).Select(c => c.ToImage()).ToArray();
                for (int i = 0; i < images1.Length; i++)
                {
                    _printoutBuilder.WithImage(new PrintImageBuilder()
                      .WithImageSource(images1[i])
                      .OnGridLayout(layout1)
                      .AtRow(0)
                      .AtColumn(i)
                      .Submit());
                }

                if (string.IsNullOrEmpty(_dto.SecondMeasurementPhoto1) && string.IsNullOrEmpty(_dto.SecondMeasurementPhoto2) && string.IsNullOrEmpty(_dto.SecondMeasurementPhoto3))
                    return false;

                var header2 = new PrintLabelBuilder()
                  .WithText($"{_localizer.Localize(() => Common.MeasurementNr)} 2")
                  .AsBold()
                  .WithFontColor(Color.Black)
                  .WithFontFamily("Arial")
                  .WithFontSize(15)
                  .Center()
                  .WithVerticalOffset(30 + header1.GetLabelHeight(Graphics) + layout1.Height + 30)
                  .Submit();

                _printoutBuilder.WithLabel(header2);

                var layout2 = new GridLayoutBuilder()
                  .WithRows(1)
                  .WithColumns(GetGridColumnCount())
                  .WithWidth((int)PrintingArea.Width)
                  .WithHeight(gridLayoutHeight)
                  .WithMargin(20)
                  .WithPadding(5)
                  .WithVerticalOffset((int)header2.Y + (int)header2.GetLabelHeight(Graphics))
                  .Build();

                var images2 = GetPhotoList(2).Select(c => c.ToImage()).ToArray();
                for (int i = 0; i < images2.Length; i++)
                {
                    _printoutBuilder.WithImage(new PrintImageBuilder()
                      .WithImageSource(images2[i])
                      .OnGridLayout(layout2)
                      .AtRow(0)
                      .AtColumn(i)
                      .Submit());
                }

                return false;
            }

            if (!string.IsNullOrEmpty(_dto.CompanyLogo))
            {
                var logo = ImageSerializer.ResizeImage(_dto.CompanyLogo.ToImage(), 400, 100);
                _printoutBuilder.WithImage(new PrintImageBuilder()
                  .WithImageSource(logo)
                  .WithVerticalOffset(PrintingArea.Top + _globalVerticalOffset)
                  .Submit());
            }

            //ticket
            _printoutBuilder.WithLabel(new PrintLabelBuilder()
              .WithText($"{_localizer.Localize(() => Common.TicketNrLabel)} ")
              .AsBold()
              .WithFontColor(Color.Black)
              .WithFontFamily("Arial")
              .WithFontSize(10)
              .WithHorizontalOffset(PrintingArea.Right * 0.6f)
              .WithVerticalOffset(PrintingArea.Top + _globalVerticalOffset + 20)
                .Submit());

            _printoutBuilder.WithLabel(new PrintLabelBuilder()
              .WithText(_dto.Header)
              .AsRegular()
              .WithFontColor(Color.Black)
              .WithFontFamily("Arial")
              .WithFontSize(10)
              .WithHorizontalOffset(PrintingArea.Right * 0.6f)
              .WithVerticalOffset(PrintingArea.Top + _globalVerticalOffset + 40)
              .Submit());

            //date/timein/weight
            _printoutBuilder.WithLabel(new PrintLabelBuilder()
              .WithText($"{_localizer.Localize(() => Common.Date1WeightHeader)} ")
              .AsBold()
              .WithFontColor(Color.Black)
              .WithFontFamily("Arial")
              .WithFontSize(10)
              .WithHorizontalOffset(PrintingArea.Right * 0.6f)
              .WithVerticalOffset(PrintingArea.Top + _globalVerticalOffset + 120)
                .Submit());


            _printoutBuilder.WithLabel(new PrintLabelBuilder()
              .WithText(GetTimeDateWeight(true))
              .AsRegular()
              .WithFontColor(Color.Black)
              .WithFontFamily("Arial")
              .WithFontSize(10)
              .WithHorizontalOffset(PrintingArea.Right * 0.6f)
              .WithVerticalOffset(PrintingArea.Top + _globalVerticalOffset + 140)
              .Submit());

            //date/timeout/weight
            _printoutBuilder.WithLabel(new PrintLabelBuilder()
              .WithText($"{_localizer.Localize(() => Common.Date2WeightHeader)} ")
              .AsBold()
              .WithFontColor(Color.Black)
              .WithFontFamily("Arial")
              .WithFontSize(10)
              .WithHorizontalOffset(PrintingArea.Right * 0.6f)
              .WithVerticalOffset(PrintingArea.Top + _globalVerticalOffset + 170)
                .Submit());

            _printoutBuilder.WithLabel(new PrintLabelBuilder()
              .WithText(GetTimeDateWeight(false))
              .AsRegular()
              .WithFontColor(Color.Black)
              .WithFontFamily("Arial")
              .WithFontSize(10)
              .WithHorizontalOffset(PrintingArea.Right * 0.6f)
              .WithVerticalOffset(PrintingArea.Top + _globalVerticalOffset + 190)
              .Submit());

            //Cargo
            _printoutBuilder.WithLabel(new PrintLabelBuilder()
              .WithText($"{_localizer.Localize(() => Common.CargoHeader)} ")
              .AsBold()
              .WithFontColor(Color.Black)
              .WithFontFamily("Arial")
              .WithFontSize(10)
              .WithHorizontalOffset(PrintingArea.Right * 0.6f)
              .WithVerticalOffset(PrintingArea.Top + _globalVerticalOffset + 220)
                .Submit());

            _printoutBuilder.WithLabel(new PrintLabelBuilder()
              .WithText(_dto.WeightEntry.Cargo)
              .AsRegular()
              .WithFontColor(Color.Black)
              .WithFontFamily("Arial")
              .WithFontSize(10)
              .WithHorizontalOffset(PrintingArea.Right * 0.6f)
              .WithVerticalOffset(PrintingArea.Top + _globalVerticalOffset + 240)
              .Submit());

            //netto
            _printoutBuilder.WithLabel(new PrintLabelBuilder()
              .WithText($"{_localizer.Localize(() => Common.NetWeightHeader)} ")
              .AsBold()
              .WithFontColor(Color.Black)
              .WithFontFamily("Arial")
              .WithFontSize(10)
              .WithHorizontalOffset(PrintingArea.Right * 0.6f)
              .WithVerticalOffset(PrintingArea.Top + _globalVerticalOffset + 270)
                .Submit());

            _printoutBuilder.WithLabel(new PrintLabelBuilder()
              .WithText(_dto.WeightEntry.Net)
              .AsRegular()
              .WithFontColor(Color.Black)
              .WithFontFamily("Arial")
              .WithFontSize(10)
              .WithHorizontalOffset(PrintingArea.Right * 0.6f)
              .WithVerticalOffset(PrintingArea.Top + _globalVerticalOffset + 290)
              .Submit());

            //time
            _printoutBuilder.WithLabel(new PrintLabelBuilder()
              .WithText($"{_localizer.Localize(() => Common.TimeLabel)} ")
              .AsBold()
              .WithFontColor(Color.Black)
              .WithFontFamily("Arial")
              .WithFontSize(10)
              .WithHorizontalOffset(PrintingArea.Right * 0.6f)
              .WithVerticalOffset(PrintingArea.Top + _globalVerticalOffset + 360)
                .Submit());

            _printoutBuilder.WithLabel(new PrintLabelBuilder()
            .WithText(GetDots(40))
            .AsRegular()
            .WithFontColor(Color.Black)
            .WithFontFamily("Arial")
            .WithFontSize(10)
            .WithHorizontalOffset(PrintingArea.Right * 0.6f + 100)
            .WithVerticalOffset(PrintingArea.Top + _globalVerticalOffset + 360)
            .Submit());

            //loading start
            _printoutBuilder.WithLabel(new PrintLabelBuilder()
              .WithText($"{_localizer.Localize(() => Common.LoadingStartLabel)} ")
              .AsBold()
              .WithFontColor(Color.Black)
              .WithFontFamily("Arial")
              .WithFontSize(10)
              .WithHorizontalOffset(PrintingArea.Right * 0.6f)
              .WithVerticalOffset(PrintingArea.Top + _globalVerticalOffset + 390)
                .Submit());

            _printoutBuilder.WithLabel(new PrintLabelBuilder()
            .WithText(GetDots(40))
            .AsRegular()
            .WithFontColor(Color.Black)
            .WithFontFamily("Arial")
            .WithFontSize(10)
            .WithHorizontalOffset(PrintingArea.Right * 0.6f + 100)
            .WithVerticalOffset(PrintingArea.Top + _globalVerticalOffset + 390)
            .Submit());

            //loading end
            _printoutBuilder.WithLabel(new PrintLabelBuilder()
              .WithText($"{_localizer.Localize(() => Common.LoadingEndLabel)} ")
              .AsBold()
              .WithFontColor(Color.Black)
              .WithFontFamily("Arial")
              .WithFontSize(10)
              .WithHorizontalOffset(PrintingArea.Right * 0.6f)
              .WithVerticalOffset(PrintingArea.Top + _globalVerticalOffset + 420)
                .Submit());

            _printoutBuilder.WithLabel(new PrintLabelBuilder()
            .WithText(GetDots(40))
            .AsRegular()
            .WithFontColor(Color.Black)
            .WithFontFamily("Arial")
            .WithFontSize(10)
            .WithHorizontalOffset(PrintingArea.Right * 0.6f + 100)
            .WithVerticalOffset(PrintingArea.Top + _globalVerticalOffset + 420)
            .Submit());

            //time out
            _printoutBuilder.WithLabel(new PrintLabelBuilder()
              .WithText($"{_localizer.Localize(() => Common.TimeOutLabel)} ")
              .AsBold()
              .WithFontColor(Color.Black)
              .WithFontFamily("Arial")
              .WithFontSize(10)
              .WithHorizontalOffset(PrintingArea.Right * 0.6f)
              .WithVerticalOffset(PrintingArea.Top + _globalVerticalOffset + 450)
                .Submit());

            _printoutBuilder.WithLabel(new PrintLabelBuilder()
            .WithText(GetDots(40))
            .AsRegular()
            .WithFontColor(Color.Black)
            .WithFontFamily("Arial")
            .WithFontSize(10)
            .WithHorizontalOffset(PrintingArea.Right * 0.6f + 100)
            .WithVerticalOffset(PrintingArea.Top + _globalVerticalOffset + 450)
            .Submit());

            //forwarder
            _printoutBuilder.WithLabel(new PrintLabelBuilder()
              .WithText($"{_localizer.Localize(() => Common.ForwarderLabel)} ")
              .AsBold()
              .WithFontColor(Color.Black)
              .WithFontFamily("Arial")
              .WithFontSize(10)
              .WithVerticalOffset(PrintingArea.Top + _globalVerticalOffset + 120)
              .Submit());

            _printoutBuilder.WithLabel(new PrintLabelBuilder()
              .WithText(_dto.Operator)
              .AsRegular()
              .WithFontColor(Color.Black)
              .WithFontFamily("Arial")
              .WithFontSize(10)
              .WithHorizontalOffset(PrintingArea.Left + 200)
              .WithVerticalOffset(PrintingArea.Top + _globalVerticalOffset + 120)
              .Submit());

            //plate
            _printoutBuilder.WithLabel(new PrintLabelBuilder()
              .WithText($"{_localizer.Localize(() => Common.VehiclePlateNumberLabel)} ")
              .AsBold()
              .WithFontColor(Color.Black)
              .WithFontFamily("Arial")
              .WithFontSize(10)
              .WithVerticalOffset(PrintingArea.Top + _globalVerticalOffset + 140)
              .Submit());

            _printoutBuilder.WithLabel(new PrintLabelBuilder()
              .WithText(_dto.Car)
              .AsRegular()
              .WithFontColor(Color.Black)
              .WithFontFamily("Arial")
              .WithFontSize(10)
              .WithHorizontalOffset(PrintingArea.Left + 200)
              .WithVerticalOffset(PrintingArea.Top + _globalVerticalOffset + 140)
              .Submit());

            //contractor
            _printoutBuilder.WithLabel(new PrintLabelBuilder()
              .WithText($"{_localizer.Localize(() => Common.SupplierLabel)} ")
              .AsBold()
              .WithFontColor(Color.Black)
              .WithFontFamily("Arial")
              .WithFontSize(10)
              .WithVerticalOffset(PrintingArea.Top + _globalVerticalOffset + 160)
              .Submit());

            _printoutBuilder.WithLabel(new PrintLabelBuilder()
              .WithText(_dto.Contractor)
              .AsRegular()
              .WithFontColor(Color.Black)
              .WithFontFamily("Arial")
              .WithFontSize(10)
              .WithHorizontalOffset(PrintingArea.Left + 200)
              .WithVerticalOffset(PrintingArea.Top + _globalVerticalOffset + 160)
              .Submit());

            //unloading confirmation
            _printoutBuilder.WithLabel(new PrintLabelBuilder()
              .WithText($"{_localizer.Localize(() => Common.ConfirmationLabel)} ")
              .AsBold()
              .WithFontColor(Color.Black)
              .WithFontFamily("Arial")
              .WithFontSize(10)
              .WithVerticalOffset(PrintingArea.Top + _globalVerticalOffset + 240)
              .Submit());

            if (!string.IsNullOrEmpty(_dto.Comment))
            {
                var unloadingConf = "";
                var unloadingParts = _dto.Comment.Split('\n');
                var checkboxCount = Math.Min(unloadingParts.Length, 5);
                for (int i = 0; i < checkboxCount; i++)
                {
                    unloadingConf += unloadingParts[i] + '\n';
                    Square(15, (int)(PrintingArea.Top + _globalVerticalOffset + 265 + i * 16), 16);
                }

                _printoutBuilder.WithLabel(new PrintLabelBuilder()
                  .WithText(unloadingConf)
                  .AsRegular()
                  .WithFontColor(Color.Black)
                  .WithFontFamily("Arial")
                  .WithFontSize(10)
                  .WithHorizontalOffset(40)
                  .WithVerticalOffset(PrintingArea.Top + _globalVerticalOffset + 265)
                  .Submit());
            }

            //production
            _printoutBuilder.WithLabel(new PrintLabelBuilder()
              .WithText($"{_localizer.Localize(() => Common.ProductionDateLabel)} ")
              .AsBold()
              .WithFontColor(Color.Black)
              .WithFontFamily("Arial")
              .WithFontSize(10)
              .WithVerticalOffset(PrintingArea.Top + _globalVerticalOffset + 350)
              .Submit());

            _printoutBuilder.WithLabel(new PrintLabelBuilder()
            .WithText(GetDots(40))
            .AsRegular()
            .WithFontColor(Color.Black)
            .WithFontFamily("Arial")
            .WithFontSize(10)
            .WithHorizontalOffset(PrintingArea.Left + 200)
            .WithVerticalOffset(PrintingArea.Top + _globalVerticalOffset + 350)
            .Submit());

            //identification
            _printoutBuilder.WithLabel(new PrintLabelBuilder()
              .WithText($"{_localizer.Localize(() => Common.IdentificationLabel)} ")
              .AsBold()
              .WithFontColor(Color.Black)
              .WithFontFamily("Arial")
              .WithFontSize(10)
              .WithVerticalOffset(PrintingArea.Top + _globalVerticalOffset + 370)
              .Submit());

            _printoutBuilder.WithLabel(new PrintLabelBuilder()
            .WithText(GetDots(40))
            .AsRegular()
            .WithFontColor(Color.Black)
            .WithFontFamily("Arial")
            .WithFontSize(10)
            .WithHorizontalOffset(PrintingArea.Left + 200)
            .WithVerticalOffset(PrintingArea.Top + _globalVerticalOffset + 370)
            .Submit());

            //unloading
            _printoutBuilder.WithLabel(new PrintLabelBuilder()
              .WithText($"{_localizer.Localize(() => Common.UnloadingPersonLabel)} ")
              .AsBold()
              .WithFontColor(Color.Black)
              .WithFontFamily("Arial")
              .WithFontSize(10)
              .WithVerticalOffset(PrintingArea.Top + _globalVerticalOffset + 400)
              .Submit());

            _printoutBuilder.WithLabel(new PrintLabelBuilder()
            .WithText(GetDots(40))
            .AsRegular()
            .WithFontColor(Color.Black)
            .WithFontFamily("Arial")
            .WithFontSize(10)
            .WithHorizontalOffset(PrintingArea.Left + 200)
            .WithVerticalOffset(PrintingArea.Top + _globalVerticalOffset + 400)
            .Submit());

            //sign
            _printoutBuilder.WithLabel(new PrintLabelBuilder()
              .WithText($"{_localizer.Localize(() => Common.SignatureLabel)} ")
              .AsBold()
              .WithFontColor(Color.Black)
              .WithFontFamily("Arial")
              .WithFontSize(10)
              .WithVerticalOffset(PrintingArea.Top + _globalVerticalOffset + 440)
              .Submit());

            _printoutBuilder.WithLabel(new PrintLabelBuilder()
            .WithText(GetDots(40))
            .AsRegular()
            .WithFontColor(Color.Black)
            .WithFontFamily("Arial")
            .WithFontSize(10)
            .WithHorizontalOffset(PrintingArea.Left + 200)
            .WithVerticalOffset(PrintingArea.Top + _globalVerticalOffset + 440)
            .Submit());

            //sign forwarder
            _printoutBuilder.WithLabel(new PrintLabelBuilder()
              .WithText($"{_localizer.Localize(() => Common.SignatureForwarderLabel)} ")
              .AsBold()
              .WithFontColor(Color.Black)
              .WithFontFamily("Arial")
              .WithFontSize(10)
              .WithVerticalOffset(PrintingArea.Top + _globalVerticalOffset + 480)
              .Submit());

            _printoutBuilder.WithLabel(new PrintLabelBuilder()
            .WithText(GetDots(40))
            .AsRegular()
            .WithFontColor(Color.Black)
            .WithFontFamily("Arial")
            .WithFontSize(10)
            .WithHorizontalOffset(PrintingArea.Left + 200)
            .WithVerticalOffset(PrintingArea.Top + _globalVerticalOffset + 480)
            .Submit());

            //safety
            _printoutBuilder.WithLabel(new PrintLabelBuilder()
              .WithText($"{_localizer.Localize(() => Common.SafetyRulesHeader)} ")
              .AsBold()
              .WithFontColor(Color.Black)
              .WithFontFamily("Arial")
              .WithFontSize(10)
            .WithHorizontalOffset(PrintingArea.Left + 200)
              .WithVerticalOffset(PrintingArea.Left + _globalVerticalOffset + 520)
              .Submit());


            if (_doubleCopy)
            {
                Graphics.RotateTransform(-45);
                _printoutBuilder.WithLabel(new PrintLabelBuilder()
                  .WithText("HOLD")
                  .AsBold()
                  .WithFontColor(Color.Red)
                  .WithFontFamily("Arial")
                  .WithFontSize(140)
                  .WithHorizontalOffset(-650)
                  .WithVerticalOffset(800)
                  .Submit());
                Graphics.RotateTransform(45);

                var height = PageMiddleHeight - 5;
                _printoutBuilder.WithLine(new PrintLineBuilder()
                  .StartsAt(0, (int)height)
                  .EndsAt(PageBounds.Width, (int)height)
                  .WithThickness(1)
                  .Dashed()
                  .Submit());
                _doubleCopy = false;
                _globalVerticalOffset = PageMiddleHeight + 5;
                Print(page);
            }
            return _includeImages;
        }
        private string GetTimeDateWeight(bool first)
        {
            string s;
            if (first)
            {
                if (!string.IsNullOrEmpty(_dto.FirstMeasurementDate) && !string.IsNullOrEmpty(_dto.WeightEntry.FirstMeasurement))
                {
                    s = _dto.FirstMeasurementDate.Replace(' ', '/');
                    s += "/" + _dto.WeightEntry.FirstMeasurement.ToString();
                    return s;
                }
            }
            else
            {
                if (!string.IsNullOrEmpty(_dto.SecondMeasurementDate) && !string.IsNullOrEmpty(_dto.WeightEntry.SecondMeasurement))
                {
                    s = _dto.SecondMeasurementDate.Replace(' ', '/');
                    s += "/" + _dto.WeightEntry.SecondMeasurement.ToString();
                    return s;
                }
            }
            return string.Empty;
        }

        private string GetMeasurementDateSectionString(string firstMeasDate, string secondMeasDate)
        {
            var section = string.Empty;
            if (!string.IsNullOrEmpty(firstMeasDate))
            {
                section = $"{_localizer.Localize(() => Common.MeasurementDate1Label)} {firstMeasDate}";
                if (!string.IsNullOrEmpty(secondMeasDate))
                {
                    section += "\n";
                    section += $"{_localizer.Localize(() => Common.MeasurementDate2Label)} {secondMeasDate}";
                }
            }

            return section;
        }

        private string GetDots(int pattern)
        {
            string result = "";

            for (int i = 0; i < pattern; i++)
                result += ".";

            return result;
        }
        private void Square(int x, int y, int size)
        {
            _printoutBuilder.WithLine(new PrintLineBuilder()
              .StartsAt(x, y)
              .EndsAt(x, y + size)
              .WithThickness(1)
              .Submit());
            _printoutBuilder.WithLine(new PrintLineBuilder()
              .StartsAt(x, y)
              .EndsAt(x + size, y)
              .WithThickness(1)
              .Submit());
            _printoutBuilder.WithLine(new PrintLineBuilder()
              .StartsAt(x, y + size)
              .EndsAt(x + size, y + size)
              .WithThickness(1)
              .Submit());
            _printoutBuilder.WithLine(new PrintLineBuilder()
              .StartsAt(x + size, y)
              .EndsAt(x + size, y + size)
              .WithThickness(1)
              .Submit());
        }
    }
}
