﻿using Kalisto.Devices.Core;
using Kalisto.Devices.Core.Communication;
using Kalisto.Devices.Core.Communication.Interfaces;
using Kalisto.Devices.Core.Messages;
using Kalisto.Devices.WeightMeter.Messages;
using System;
using System.Threading;

namespace Kalisto.Devices.WeightMeter
{
  public class WeightMeteringDevice : IDevice<WeightMeterMessage>
  {
    private readonly object _syncLock = new object();

    private readonly int _weightValueUpdateFrequency = 750;

    private readonly IDeviceClientConnection _clientConnection;

    private readonly IMessageChunkAggregator<WeightMeterMessage> _messageChunkAggregator;

    private string _endpointAddress;

    private WeightMeterMessage _lastReceivedMessage;

    private Thread _sendingThread;

    public event Action<WeightMeterMessage> MessageReceived;

    public event Action<ConnectionState> ConnectionStateChanged;

    public int Id { get; set; }

    public WeightMeteringDevice(IDeviceClientConnection clientConnection,
      IMessageChunkAggregator<WeightMeterMessage> messageChunkAggregator)
    {
      _clientConnection = clientConnection;
      _clientConnection.BytesMessageReceived += OnBytesReceived;
      _clientConnection.ConnectionStateChanged += OnConnectionStateChanged;
      _messageChunkAggregator = messageChunkAggregator;
      _messageChunkAggregator.MessageReady += OnMessageReady;
    }

    protected void OnMessageReady(WeightMeterMessage message)
    {
      if (message == null)
        return;

      lock (_syncLock)
      {
        _lastReceivedMessage = message;
        _lastReceivedMessage.DeviceId = Id;
        if (string.IsNullOrEmpty(_lastReceivedMessage.EndpointAddress))
          _lastReceivedMessage.EndpointAddress = _endpointAddress;
      }
    }

    private void OnBytesReceived(object sender, BytesMessageReceivedEventArgs e)
    {
      _endpointAddress = e.EndpointId;
      _messageChunkAggregator.AddChunk(e.Bytes);
    }

    protected virtual void OnConnectionStateChanged(ConnectionState obj)
    {
      ConnectionStateChanged?.Invoke(obj);
    }

    public void StartSampling()
    {
      _sendingThread = new Thread(() =>
      {
        while (true)
        {
          TrySendMessage();
          Thread.Sleep(_weightValueUpdateFrequency);
        }
      });

      _clientConnection.Connect(true);
      _sendingThread.IsBackground = true;
      _sendingThread.Start();
    }

    public virtual void StopSampling()
    {
      _sendingThread.Abort();
      _clientConnection.Disconnect();
    }

    private void TrySendMessage()
    {
      lock (_syncLock)
      {
        if (_lastReceivedMessage == null)
          return;

        MessageReceived?.Invoke(_lastReceivedMessage);
      }
    }

    public void SendCommand<TCommand>(TCommand command) 
      where TCommand : DeviceCommandMessageBase
    {
    }
  }

  public class TestWeightMeteringDevice : WeightMeteringDevice
  {
    private Thread _sendingThread;

    public TestWeightMeteringDevice(IDeviceClientConnection tcpClientConnection,
      IMessageChunkAggregator<WeightMeterMessage> messageFactory)
      : base(tcpClientConnection, messageFactory)
    {
    }

    protected override void OnConnectionStateChanged(ConnectionState obj)
    {
      if (obj == ConnectionState.NotAvailable)
        StartTestSampling();
      else base.OnConnectionStateChanged(obj);
    }

    private void StartTestSampling()
    {
      _sendingThread = new Thread(() =>
      {
        while (true)
        {
          TrySendMessage();
          Thread.Sleep(1000);
        }
      });

      _sendingThread.IsBackground = true;
      _sendingThread.Start();
    }

    public override void StopSampling()
    {
      base.StopSampling();

      _sendingThread.Abort();
      OnConnectionStateChanged(ConnectionState.Disconnected);
    }

    private void TrySendMessage()
    {
      var r = new Random();
      int range = 100;
      var rDouble = 1200 + r.NextDouble() * range;
      var stable = true;
      var minutesEven = DateTime.Now.Minute % 2;
      if (minutesEven == 0)
        stable = false;

      OnMessageReady(new WeightMeterMessage
      {
        IsStable = stable,
        EndpointAddress = "test 1",
        Value = (int)rDouble
      });
    }
  }
}
