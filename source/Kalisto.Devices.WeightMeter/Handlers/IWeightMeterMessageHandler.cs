﻿using Kalisto.Devices.Core.Handlers;
using Kalisto.Devices.WeightMeter.Messages;

namespace Kalisto.Devices.WeightMeter.Handlers
{
  public interface IWeightMeterMessageHandler : IDeviceMessageHandler
  {
    void HandleWeightMeterMessage(WeightMeterMessage message);
  }
}
