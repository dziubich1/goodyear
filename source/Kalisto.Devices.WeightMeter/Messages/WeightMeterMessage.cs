﻿using Kalisto.Devices.Core.Messages;
using Kalisto.Devices.WeightMeter.Handlers;
using System;

namespace Kalisto.Devices.WeightMeter.Messages
{
  [Serializable]
  public class WeightMeterMessage : DeviceMessage<IWeightMeterMessageHandler>
  {
    public const int MessageLength = 22;

    public bool IsStable { get; set; }

    public decimal Value { get; set; }

    protected override void Handle(IWeightMeterMessageHandler handler)
    {
      handler.HandleWeightMeterMessage(this);
    }
  }
}
