﻿using Kalisto.Devices.Core;
using System.Linq;
using System.Text;
using Kalisto.Devices.WeightMeter.Messages;

namespace Kalisto.Devices.WeightMeter
{
  public class WeightMeterMessageTranslator : IMessageTranslator<WeightMeterMessage>
  {
    public WeightMeterMessage Translate(byte[] bytes)
    {
      if (bytes.Length != WeightMeterMessage.MessageLength)
        return null;

      var isStable = bytes[12] != 39;
      var measurementString = Encoding.ASCII.GetString(bytes.Skip(1).Take(11).ToArray()).Trim();
      if (!decimal.TryParse(measurementString, out decimal value))
        value = -1;

      return new WeightMeterMessage
      {
        IsStable = isStable,
        Value = value
      };
    }
  }
}
