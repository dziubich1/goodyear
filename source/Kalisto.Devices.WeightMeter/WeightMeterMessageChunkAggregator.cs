﻿using Kalisto.Devices.Core;
using System;
using System.Collections.Generic;
using Kalisto.Devices.WeightMeter.Messages;

namespace Kalisto.Devices.WeightMeter
{
  public class WeightMeterMessageChunkAggregator : IMessageChunkAggregator<WeightMeterMessage>
  {
    private readonly IMessageTranslator<WeightMeterMessage> _messageTranslator;

    private readonly List<byte> _buffer = new List<byte>();

    private readonly List<byte> _messageData = new List<byte>();

    public event Action<WeightMeterMessage> MessageReady;

    public WeightMeterMessageChunkAggregator(IMessageTranslator<WeightMeterMessage> messageTranslator)
    {
      _messageTranslator = messageTranslator;
    }

    public void AddChunk(byte[] buffer)
    {
      if (_buffer.Count <= WeightMeterMessage.MessageLength)
        _buffer.AddRange(buffer);

      if (_buffer.Count <= WeightMeterMessage.MessageLength)
        return;

      var startIndex = _buffer.FindIndex(c => c == 0x02);
      var endIndex = startIndex + WeightMeterMessage.MessageLength - 1;
      if (endIndex >= _buffer.Count)
      {
        for (int i = startIndex - 1; i >= 0; i--)
          _buffer.RemoveAt(i); // clear beginning of buffer, since it contains corrupted previous message
        return;
      }

      if (_buffer[endIndex] != 0x04)
      {
        _buffer.Clear(); // message broken, clear buffer
        return;
      }

      // message found
      _messageData.Clear();
      var bufferArr = _buffer.ToArray();
      for (int i = startIndex; i <= endIndex; i++)
        _messageData.Add(bufferArr[i]); // move whole buffer to message

      for (int i = endIndex; i >= startIndex; i--)
        _buffer.RemoveAt(i); // clear buffer, but leave part of next message

      var arr = _messageData.ToArray();
      var message = _messageTranslator.Translate(arr);
      if (message != null)
        MessageReady?.Invoke(message);

      _messageData.Clear();
    }
  }
}
