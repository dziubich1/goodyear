﻿using System.Collections.Generic;
using Kalisto.Communication.Core.Communication;
using Kalisto.Logging;
using RabbitMQ.Client;

namespace Kalisto.Communication.RabbitMq
{
  public class RabbitMqReusableConnectionFactory : IRabbitMqConnectionFactory
  {
    private readonly ILogger _logger;
    private readonly ConnectionFactory _connectionFactory;

    private readonly Dictionary<string, IConnection> _connectionRegistry = new Dictionary<string, IConnection>();

    public RabbitMqReusableConnectionFactory(ILogger logger, RabbitMqConnectionConfiguration configuration)
    {
      _logger = logger;
      _connectionFactory = new ConnectionFactory
      {
        HostName = configuration.Hostname,
        UserName = configuration.Username,
        Password = configuration.Password,
        Port = configuration.Port
      };
    }

    public IConnection CreateConnectionForTransport(IMessageTransport messageTransport)
    {
      return GetOrCreateConnection(messageTransport.GetType().FullName);
    }

    public void Dispose()
    {
      foreach (var connection in _connectionRegistry.Values)
      {
        connection.Close();
        connection.Dispose();
      }
    }

    private IConnection GetOrCreateConnection(string connectionName)
    {
      IConnection connection;
      if (_connectionRegistry.ContainsKey(connectionName))
      {
        connection = _connectionRegistry[connectionName];
        if (connection != null && connection.IsOpen)
          return connection;

        connection = _connectionFactory.CreateConnection();
        _connectionRegistry[connectionName] = connection;
        return connection;
      }

      connection = _connectionFactory.CreateConnection();
      _connectionRegistry.Add(connectionName, connection);
      return connection;
    }
  }
}
