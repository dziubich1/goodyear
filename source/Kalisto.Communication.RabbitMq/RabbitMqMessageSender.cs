﻿using System.Collections.Generic;
using System.Text;
using Kalisto.Communication.Core.Communication;
using Kalisto.Logging;
using RabbitMQ.Client;

namespace Kalisto.Communication.RabbitMq
{
  public class RabbitMqMessageSender : IMessageSender
  {
    private readonly ILogger _logger;

    private readonly IRabbitMqChannelFactory _channelFactory;

    private IModel _messageChannel;

    public string QueueName { get; private set; }

    public RabbitMqMessageSender(ILogger logger, IRabbitMqChannelFactory channelFactory)
    {
      _logger = logger;
      _channelFactory = channelFactory;
    }

    public void Initialize(string topic)
    {
      QueueName = topic;
      _messageChannel = _channelFactory.CreateModelForTransport(this);
      if (_messageChannel == null)
      {
        _logger.Error($"Failed to set up sender to {QueueName}");
        return;
      }

      _messageChannel.QueueDeclare(queue: topic,
        durable: false,
        exclusive: false,
        autoDelete: true,
        arguments: new Dictionary<string, object> { { "x-message-ttl", 500 }, { "x-expires", 15000 } });
    }

    public void SendMessage(string message)
    {
      if (_messageChannel == null)
      {
        _logger.Error($"Failed to send a message to: {QueueName}. Invalid RabbitMQ transport model detected.");
        return;
      }

      var body = Encoding.UTF8.GetBytes(message);
      _messageChannel.BasicPublish(string.Empty,
        routingKey: QueueName,
        basicProperties: null,
        body: body);
      _logger.Debug($"Successfully sent a message to {QueueName}.");
    }

    public void Dispose()
    {
      _messageChannel?.Close();
      _messageChannel?.Dispose();
    }
  }
}
