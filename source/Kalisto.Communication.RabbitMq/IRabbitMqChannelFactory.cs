﻿using System;
using Kalisto.Communication.Core.Communication;
using RabbitMQ.Client;

namespace Kalisto.Communication.RabbitMq
{
  public interface IRabbitMqChannelFactory : IDisposable
  {
    IModel CreateModelForTransport(IMessageTransport messageTransport);
  }
}
