﻿using System;
using Kalisto.Communication.Core.Communication;
using RabbitMQ.Client;

namespace Kalisto.Communication.RabbitMq
{
  public interface IRabbitMqConnectionFactory : IDisposable
  {
    IConnection CreateConnectionForTransport(IMessageTransport messageTransport);
  }
}
