﻿using System;

namespace Kalisto.Communication.RabbitMq
{
  [Serializable]
  public class RabbitMqConnectionConfiguration
  {
    public string Hostname { get; set; }

    public string Username { get; set; }

    public string Password { get; set; }

    public int Port { get; set; }
  }
}
