﻿using System;
using Kalisto.Communication.Core.Communication;
using Kalisto.Logging;
using RabbitMQ.Client;

namespace Kalisto.Communication.RabbitMq
{
  public class RabbitMqChannelFactory : IRabbitMqChannelFactory
  {
    private readonly ILogger _logger;

    private readonly IRabbitMqConnectionFactory _connectionFactory;

    public RabbitMqChannelFactory(ILogger logger, IRabbitMqConnectionFactory connectionFactory)
    {
      _logger = logger;
      _connectionFactory = connectionFactory;
    }

    public IModel CreateModelForTransport(IMessageTransport messageTransport)
    {
      try
      {
        return _connectionFactory.CreateConnectionForTransport(messageTransport).CreateModel();
      }
      catch(Exception e)
      {
        _logger.Error($"Error occurred while creating RabbitMQ transport model. Exception was thrown: {e.Message}");
        return null;
      }
    }

    public void Dispose()
    {
      _connectionFactory?.Dispose();
    }
  }
}
