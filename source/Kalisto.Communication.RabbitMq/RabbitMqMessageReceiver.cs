﻿using System;
using System.Collections.Generic;
using System.Text;
using Kalisto.Communication.Core.Communication;
using Kalisto.Logging;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;

namespace Kalisto.Communication.RabbitMq
{
  public class RabbitMqMessageReceiver : IMessageReceiver
  {
    private readonly ILogger _logger;

    private readonly IRabbitMqChannelFactory _channelFactory;

    private IModel _messageChannel;

    public string QueueName { get; private set; }

    public event Action<string, string> OnMessageReceived;

    public RabbitMqMessageReceiver(ILogger logger, IRabbitMqChannelFactory channelFactory)
    {
      _logger = logger;
      _channelFactory = channelFactory;
    }

    public void Initialize(string topic)
    {
      QueueName = topic;
      _messageChannel = _channelFactory.CreateModelForTransport(this);
      if (_messageChannel == null)
      {
        _logger.Error($"Failed to set up listener to {QueueName}");
        return;
      }

      _messageChannel.QueueDeclare(queue: QueueName,
        durable: false,
        exclusive: false,
        autoDelete: true,
        arguments: new Dictionary<string, object> { { "x-message-ttl", 500 }, { "x-expires", 15000 } });

      var consumer = new EventingBasicConsumer(_messageChannel);
      consumer.Received += OnConsumerMessageReceived;
      _messageChannel.BasicConsume(queue: QueueName,
        autoAck: true,
        consumer: consumer);
      _logger.Info($"Successfully set up listener to {QueueName}.");
    }

    private void OnConsumerMessageReceived(object sender, BasicDeliverEventArgs e)
    {
      _logger.Debug($"Successfully received message from {QueueName}.");
      var body = e.Body;
      var message = Encoding.UTF8.GetString(body);
      OnMessageReceived?.Invoke(QueueName, message);
    } 

    public void Dispose()
    {
      _messageChannel?.Close();
      _messageChannel?.Dispose();
    }
  }
}
