﻿using System;
using System.Collections.Generic;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Configuration.Permissions;
using Kalisto.Hmi.Core.Services;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Kalisto.Hmi.Core.UnitTests
{
  [TestClass]
  public class AuthorizationServiceTests
  {
    private UserInfo _userInfo;

    // component under tests
    private IAuthorizationService _authorizationService;

    [TestInitialize]
    public void Initialize()
    {
      _userInfo = new UserInfo();
      _authorizationService = new AuthorizationService(_userInfo);
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentNullException))]
    public void CreateUserSession_ShouldThrowArgumentNullException_WhenGivenUserInfoIsNull()
    {
      // Arrange
      UserInfo newUserInfo = null;

      // Act & Assert
      _authorizationService.CreateUserSession(newUserInfo);
    }

    [TestMethod]
    public void CreateUserSession_ShouldUpdateUserInfo()
    {
      // Arrange
      var newUserInfo = new UserInfo
      {
        UserRole = 0,
        Username = "some_user",
        Permissions = new List<PermissionRule>()
      };

      // Act
      _authorizationService.CreateUserSession(newUserInfo);

      // Assert
      Assert.AreNotSame(_userInfo, newUserInfo);
      Assert.AreEqual(_userInfo.Username, newUserInfo.Username);
      Assert.AreEqual(_userInfo.UserRole, newUserInfo.UserRole);
      Assert.AreEqual(_userInfo.Permissions, newUserInfo.Permissions);
    }

    [TestMethod]
    public void CreateUserSession_ShouldRaiseAuthorizedEventOnce()
    {
      // Arrange
      var raisedEventsCount = 0;
      _authorizationService.Authorized += () => { raisedEventsCount++; };
      var newUserInfo = new UserInfo
      {
        UserRole = 0,
        Username = "some_user",
        Permissions = new List<PermissionRule>()
      };

      // Act
      _authorizationService.CreateUserSession(newUserInfo);

      // Assert
      Assert.AreEqual(1, raisedEventsCount);
    }

    [TestMethod]
    public void GetPermissionForRule_ShouldReturnNone_WhenGivenRuleNameDoesNotExist()
    {
      // Arrange
      var notExistingRuleName = "not_existing_rule_name";
      _userInfo.Permissions = new List<PermissionRule>();

      // Act
      var result = _authorizationService.GetPermissionForRule(notExistingRuleName);

      // Assert
      Assert.AreEqual(PermissionType.None, result);
    }

    [TestMethod]
    public void GetPermissionForRule_ShouldReturnNone_WhenPermissionsCollectionIsNull()
    {
      // Arrange
      var ruleName = "rule_name";
      _userInfo.Permissions = null;

      // Act
      var result = _authorizationService.GetPermissionForRule(ruleName);

      // Assert
      Assert.AreEqual(PermissionType.None, result);
    }

    [TestMethod]
    public void GetPermissionForRule_ShouldReturnProperPermission()
    {
      // Arrange
      var fullPermissionRuleName = "fullPermissionRuleName";
      var fullPermission = new PermissionRule
      {
        Type = PermissionType.Full,
        Definition = new PermissionRuleDefinition
        {
          Name = fullPermissionRuleName
        }
      };

      var readOnlyPermissionRuleName = "readOnlyPermissionRuleName";
      var readOnlyPermission = new PermissionRule
      {
        Type = PermissionType.ReadOnly,
        Definition = new PermissionRuleDefinition
        {
          Name = readOnlyPermissionRuleName
        }
      };

      var nonePermissionRuleName = "nonePermissionRuleName";
      var nonePermission = new PermissionRule
      {
        Type = PermissionType.None,
        Definition = new PermissionRuleDefinition
        {
          Name = nonePermissionRuleName
        }
      };

      _userInfo.Permissions = new List<PermissionRule>{ fullPermission, readOnlyPermission, nonePermission };

      // Act
      var fullPermissionResult = _authorizationService.GetPermissionForRule(fullPermissionRuleName);
      var readOnlyPermissionResult = _authorizationService.GetPermissionForRule(readOnlyPermissionRuleName);
      var nonePermissionResult = _authorizationService.GetPermissionForRule(nonePermissionRuleName);

      // Assert
      Assert.AreEqual(fullPermission.Type, fullPermissionResult);
      Assert.AreEqual(readOnlyPermission.Type, readOnlyPermissionResult);
      Assert.AreEqual(nonePermission.Type, nonePermissionResult);
    }
  }
}
