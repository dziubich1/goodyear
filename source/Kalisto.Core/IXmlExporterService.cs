﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kalisto.Core
{
  public interface IXmlExporterService
  {
    void Export<T>(T entity, string entityId);
  }
}
