﻿using System;
using System.Net;

namespace Kalisto.Core
{
  public class TimeoutWebClient : WebClient
  {
    public int Timeout { get; set; } = 10000;

    protected override WebRequest GetWebRequest(Uri address)
    {
      var wr = base.GetWebRequest(address);
      if (wr == null)
        throw new InvalidOperationException($"Error occurred while preparing web request to: {address.AbsolutePath}");

      wr.Timeout = Timeout;
      return wr;
    }
  }
}
