﻿using System;
using System.Threading.Tasks;

namespace Kalisto.Hmi.Core
{
  public interface IExceptionHandler
  {
    Task HandleExceptionAsync(Exception exception);
  }
}
