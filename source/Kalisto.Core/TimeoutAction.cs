﻿using System;
using System.Threading.Tasks;

namespace Kalisto.Core
{
  public class TimeoutAction
  {
    private object _sync = new object();

    private DateTime _lastBumpDate = DateTime.Now;

    private readonly Action _timeoutAction;

    private readonly int _timeout;

    private Task _currentTask;

    private bool _requested;

    public TimeoutAction(Action action, int timeout)
    {
      _timeoutAction = action;
      _timeout = timeout;
    }

    protected DateTime LastBumpDate
    {
      get
      {
        lock (_sync)
        {
          return _lastBumpDate;
        }
      }
      set
      {
        lock (_sync)
        {
          _lastBumpDate = value;
        }
      }
    }

    public void Bump()
    {
      LastBumpDate = DateTime.Now;
    }

    public void StartTimeout()
    {
      Bump();
      _requested = false;

      if (_currentTask != null)
        return;

      _currentTask = Task.Run(async () =>
      {   
        while (true)
        {
          lock (_sync)
          {
            var now = DateTime.Now;
            if (now > LastBumpDate.AddSeconds(_timeout) && !_requested)
            {
              _requested = true;
              _timeoutAction.Invoke();
            }
          }

          await Task.Delay(1000);
        }
      });
    }
  }
}
