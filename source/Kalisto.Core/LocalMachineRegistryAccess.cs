﻿using Microsoft.Win32;
using System;

namespace Kalisto.Core
{
  public interface ISystemRegistryReader
  {
    string GetRegistryValue(string keyName, string valueName);
  }

  public interface ISystemRegistryWriter
  {
    void StoreRegistryValue(string keyName, string valueName, string value);
  }

  public class LocalMachineRegistryAccess : ISystemRegistryReader, ISystemRegistryWriter
  {
    public void StoreRegistryValue(string keyName, string valueName, string value)
    {
      if (!KeyExists(keyName))
        CreateKey(keyName);

      CreateOrUpdateValue(keyName, valueName, value);
    }

    public string GetRegistryValue(string keyName, string valueName)
    {
      if (!KeyExists(keyName))
        return string.Empty;

      var softwareKey = GetSoftwareKey();
      if (softwareKey == null)
        return string.Empty;

      var key = softwareKey.OpenSubKey(keyName);
      if (key == null)
        throw new InvalidOperationException();

      var value = key.GetValue(valueName);
      if (value == null)
        return string.Empty;

      if (key.GetValueKind(valueName) != RegistryValueKind.String)
        throw new InvalidOperationException();

      return value.ToString();
    }

    private bool KeyExists(string keyName)
    {
      var softwareKey = GetSoftwareKey();
      if (softwareKey == null)
        return false;

      var key = softwareKey.OpenSubKey(keyName);
      if (key == null)
        return false;

      return true;
    }

    private void CreateKey(string keyName)
    {
      var softwareKey = GetSoftwareKey();
      if (softwareKey == null)
        throw new InvalidOperationException();

      softwareKey.CreateSubKey(keyName, true);
    }

    private void CreateOrUpdateValue(string keyName, string valueName, string value = "")
    {
      var softwareKey = GetSoftwareKey();
      if (softwareKey == null)
        throw new InvalidOperationException();

      var key = softwareKey.OpenSubKey(keyName, true);
      if (key == null)
        throw new InvalidOperationException();

      key.SetValue(valueName, value);
    }

    private RegistryKey GetSoftwareKey()
    {
      return Registry.LocalMachine.OpenSubKey("SOFTWARE", true);
    }
  }

  public class CurrentUserRegistryAccess : ISystemRegistryReader, ISystemRegistryWriter
  {
    public void StoreRegistryValue(string keyName, string valueName, string value)
    {
      if (!KeyExists(keyName))
        CreateKey(keyName);

      CreateOrUpdateValue(keyName, valueName, value);
    }

    public string GetRegistryValue(string keyName, string valueName)
    {
      if (!KeyExists(keyName))
        return string.Empty;

      var softwareKey = GetSoftwareKey();
      if (softwareKey == null)
        return string.Empty;

      var key = softwareKey.OpenSubKey(keyName);
      if (key == null)
        throw new InvalidOperationException();

      var value = key.GetValue(valueName);
      if (value == null)
        return string.Empty;

      if (key.GetValueKind(valueName) != RegistryValueKind.String)
        throw new InvalidOperationException();

      return value.ToString();
    }

    private bool KeyExists(string keyName)
    {
      var softwareKey = GetSoftwareKey();
      if (softwareKey == null)
        return false;

      var key = softwareKey.OpenSubKey(keyName);
      if (key == null)
        return false;

      return true;
    }

    private void CreateKey(string keyName)
    {
      var softwareKey = GetSoftwareKey();
      if (softwareKey == null)
        throw new InvalidOperationException();

      softwareKey.CreateSubKey(keyName, true);
    }

    private void CreateOrUpdateValue(string keyName, string valueName, string value = "")
    {
      var softwareKey = GetSoftwareKey();
      if (softwareKey == null)
        throw new InvalidOperationException();

      var key = softwareKey.OpenSubKey(keyName, true);
      if (key == null)
        throw new InvalidOperationException();

      key.SetValue(valueName, value);
    }

    private RegistryKey GetSoftwareKey()
    {
      return Registry.CurrentUser.OpenSubKey("SOFTWARE", true);
    }
  }
}
