﻿using Caliburn.Micro;
using System;
using System.Windows;

namespace Kalisto.Core
{
  public interface IWindowInputManager
  {
    bool RegisterView(IViewAware viewAware);

    event System.Action InputDetected;
  }

  public class WindowInputManager : IWindowInputManager
  {
    private Window _window;

    public event System.Action InputDetected;

    public bool RegisterView(IViewAware viewAware)
    {
      _window = viewAware.GetView() as Window;
      if (_window == null)
        return false;

      _window.MouseMove += OnMouseMove;
      _window.KeyDown += OnKeyDown;
      return true;
    }

    private void OnKeyDown(object sender, System.Windows.Input.KeyEventArgs e)
    {
      OnInputDetected();
    }

    private void OnMouseMove(object sender, System.Windows.Input.MouseEventArgs e)
    {
      OnInputDetected();
    }

    private void OnInputDetected()
    {
      if (_window == null)
        return;

      _window.KeyDown -= OnKeyDown;
      _window.MouseMove -= OnMouseMove;
      InputDetected?.Invoke();
    }
  }
}
