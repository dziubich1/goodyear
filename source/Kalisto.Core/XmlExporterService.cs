﻿using Kalisto.Configuration.MeasurementExportSettings;
using System;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Kalisto.Core
{
  public class XmlExporterService : IXmlExporterService
  {
    private string AppDataPath => Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);

    private readonly MeasurementExportConfigurationManager _measurementExportConfigurationManager;
    public XmlExporterService(MeasurementExportConfigurationManager measurementExportConfigurationManager)
    {
      _measurementExportConfigurationManager = measurementExportConfigurationManager;
    }
    public void Export<T>(T entity, string entityId)
    {
      var measurementExportConfig = _measurementExportConfigurationManager.Load();
      if (!measurementExportConfig.IsEnabled)
        return;

      var exportFolderPath = Path.Combine(AppDataPath, _measurementExportConfigurationManager.ConfigDirectory, "Export");
      if (!Directory.Exists(exportFolderPath))
      {
        Directory.CreateDirectory(exportFolderPath);
      }

      XmlSerializer serializer = new XmlSerializer(typeof(T), new XmlRootAttribute("measurement"));
      var xmlFilePath = Path.Combine(exportFolderPath, $"measurement_{entityId}_{DateTime.Now.Ticks}.xml");
      using (var writer = new XmlTextWriter(xmlFilePath, Encoding.UTF8))
      {
        serializer.Serialize(writer, entity);
        writer.Flush();
        writer.Close();
      }
    }
  }
}
