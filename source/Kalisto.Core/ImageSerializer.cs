﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace Kalisto.Core
{
  public static class ImageSerializer
  {
    public static string ToBase64String(this Image image, ImageFormat format = null)
    {
      using (var m = new MemoryStream())
      {
        if (format == null)
          format = image.RawFormat;

        image.Save(m, format);
        return Convert.ToBase64String(m.ToArray());
      }
    }

    public static BitmapImage ToBitmapImage(this Image image)
    {
      using (MemoryStream memory = new MemoryStream())
      {
        image.Save(memory, ImageFormat.Png);
        memory.Position = 0;
        BitmapImage bitmapImage = new BitmapImage();
        bitmapImage.BeginInit();
        bitmapImage.StreamSource = memory;
        bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
        bitmapImage.EndInit();
        return bitmapImage;
      }
    }

    public static Image ToImage(this string base64String)
    {
      if (string.IsNullOrEmpty(base64String))
        return null;

      var bytes = Convert.FromBase64String(base64String);
      using (var m = new MemoryStream(bytes, 0, bytes.Length))
      {
        m.Write(bytes, 0, bytes.Length);
        return Image.FromStream(m);
      }
    }

    public static BitmapImage ToBitmapImage(this string base64String)
    {
      if (string.IsNullOrEmpty(base64String))
        return null;

      var img = base64String.ToImage();
      using (var memory = new MemoryStream())
      {
        img.Save(memory, ImageFormat.Png);
        memory.Position = 0;

        var bitmapImage = new BitmapImage();
        bitmapImage.BeginInit();
        bitmapImage.StreamSource = memory;
        bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
        bitmapImage.EndInit();

        return bitmapImage;
      }
    }

    public static Bitmap ToBitmap(this BitmapImage bitmapImage)
    {
      using (var outStream = new MemoryStream())
      {
        BitmapEncoder enc = new BmpBitmapEncoder();
        enc.Frames.Add(BitmapFrame.Create(bitmapImage));
        enc.Save(outStream);
        return new Bitmap(outStream);
      }
    }

    public static Bitmap ResizeImage(this Image image, int width, int height)
    {
      var desiredSize = CalculateSizeToFit(new Size(image.Width, image.Height), new Size(width, height));
      width = desiredSize.Width;
      height = desiredSize.Height;

      var destRect = new Rectangle(0, 0, width, height);
      var destImage = new Bitmap(width, height);

      destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

      using (var graphics = Graphics.FromImage(destImage))
      {
        graphics.CompositingMode = CompositingMode.SourceCopy;
        graphics.CompositingQuality = CompositingQuality.HighQuality;
        graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
        graphics.SmoothingMode = SmoothingMode.HighQuality;
        graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

        using (var wrapMode = new ImageAttributes())
        {
          wrapMode.SetWrapMode(WrapMode.TileFlipXY);
          graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
        }
      }

      return destImage;
    }

    private static Size CalculateSizeToFit(Size imageSize, Size boxSize)
    {
      var widthScale = boxSize.Width / (double)imageSize.Width;
      var heightScale = boxSize.Height / (double)imageSize.Height;
      var scale = Math.Min(widthScale, heightScale);
      return new Size(
        (int)Math.Round((imageSize.Width * scale)),
        (int)Math.Round((imageSize.Height * scale))
      );
    }
  }
}
