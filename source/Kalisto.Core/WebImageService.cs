﻿using Kalisto.Logging;
using System;
using System.IO;
using System.Net;
using System.Windows.Media.Imaging;

namespace Kalisto.Core
{
  public interface IWebImageService
  {
    byte[] GetImageData(string url, string username, string password);

    BitmapImage GetImageSource(string url, string username, string password);
  }

  public class WebImageService : IWebImageService
  {
    private readonly ILogger _logger;

    public WebImageService(ILogger logger)
    {
      _logger = logger;
    }

    public byte[] GetImageData(string url, string username, string password)
    {
      try
      {
        using (var webClient = new TimeoutWebClient { Timeout = 5000, Credentials = new NetworkCredential(username, password) })
        {
          return webClient.DownloadData(new Uri(url));
        }
      }
      catch (Exception e)
      {
        _logger.Error($"Error occurred while fetching web image from {url}. Exception was thrown: {e.Message}");
        return null;
      }
    }

    public BitmapImage GetImageSource(string url, string username, string password)
    {
      if (string.IsNullOrEmpty(url))
        return null;

      var buffer = GetImageData(url, username, password);
      var image = new BitmapImage();
      if (buffer == null)
        return null;

      try
      {
        using (var stream = new MemoryStream(buffer))
        {
          stream.Position = 0;

          image.BeginInit();
          image.CreateOptions = BitmapCreateOptions.PreservePixelFormat;
          image.CacheOption = BitmapCacheOption.OnLoad;
          image.UriSource = null;
          image.StreamSource = stream;
          image.EndInit();
        }

        image.Freeze();
        return image;
      }
      catch (Exception e)
      {
        _logger.Error($"Error occurred while creating bitmap image from buffer. Exception was thrown: {e.Message}");
        return null;
      }
    }
  }
}
