﻿using System;
using System.Drawing;
using System.Windows.Media.Imaging;

namespace Kalisto.Core
{
  public interface IMeasurementPhotoService
  {
    string Store(BitmapImage bitmapImage, string photoRelPath, long measurementNumber, DateTime measurementDateUtc);

    Image Get(string photoRelPath);

    Image GetThumbnail(string photoRelPath);
  }
}
