﻿using System.Windows;
using System.Windows.Data;
using Kalisto.Hmi.Core.Controls;
using Xceed.Wpf.Toolkit.PropertyGrid;
using Xceed.Wpf.Toolkit.PropertyGrid.Editors;

namespace Kalisto.Configuration.Application.Editors
{
  public class FilePickerEditor : ITypeEditor
  {
    public FrameworkElement ResolveEditor(PropertyItem propertyItem)
    {
      var filePicker = new FilePickerControl();
      var binding = new Binding("Value")
      {
        Source = propertyItem,
        ValidatesOnExceptions = true,
        ValidatesOnDataErrors = true,
        Mode = BindingMode.TwoWay,
        UpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged
      };

      BindingOperations.SetBinding(filePicker, FilePickerControl.FilePathProperty, binding);
      return filePicker;
    }
  }
}
