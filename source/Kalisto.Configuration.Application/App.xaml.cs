﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using MahApps.Metro;

namespace Kalisto.Configuration.Application
{
  /// <summary>
  /// Interaction logic for App.xaml
  /// </summary>
  public partial class App : System.Windows.Application
  {
    public App()
    {
      var culture = new CultureInfo("pl-PL");
      CultureInfo.CurrentCulture = culture;
      CultureInfo.CurrentUICulture = culture;
      CultureInfo.DefaultThreadCurrentCulture = culture;
    }

    protected override void OnStartup(StartupEventArgs e)
    {
      ThemeManager.AddAccent("KalistoTheme", new Uri("pack://application:,,,/Kalisto.Hmi.Dialogs.Common;component/KalistoTheme.xaml"));
      Tuple<AppTheme, Accent> theme = ThemeManager.DetectAppStyle(Current);
      ThemeManager.ChangeAppStyle(Current,
        ThemeManager.GetAccent("KalistoTheme"),
        theme.Item1);

      base.OnStartup(e);
    }
  }
}
