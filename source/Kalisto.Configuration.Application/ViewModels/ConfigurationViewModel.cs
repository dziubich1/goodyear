﻿using Kalisto.Backend.Configuration;
using Kalisto.Backend.Enum;
using Kalisto.Communication.RabbitMq;
using Kalisto.Configuration.Application.Editors;
using Kalisto.Hmi.Core.Attributes;
using Kalisto.Hmi.Core.Validation;
using PropertyChanged;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Data.Common;
using System.IO;
using System.Linq;
using Xceed.Wpf.Toolkit.PropertyGrid.Attributes;

namespace Kalisto.Configuration.Application.ViewModels
{
  [DisplayName("")]
  [AddINotifyPropertyChangedInterface]
  public class ConfigurationViewModel : DataErrorInfoImpl
  {
    [LocalizableCategory("DatabaseConnection", typeof(Localization.Configuration.Configuration))]
    [LocalizableDisplayName("DatabaseType", typeof(Localization.Configuration.Configuration))]
    [LocalizedDescription("DatabaseTypeDescription", typeof(Localization.Configuration.Configuration))]
    [PropertyOrder(1)]
    public DbTypeEnum DatabaseType { get; set; }

    [LocalizableCategory("DatabaseConnection", typeof(Localization.Configuration.Configuration))]
    [LocalizableDisplayName("ConnectionString", typeof(Localization.Configuration.Configuration))]
    [LocalizedDescription("ConnectionStringDescription", typeof(Localization.Configuration.Configuration))]
    [CustomValidation(typeof(ConfigurationViewModel), nameof(ValidateConnectionString))]
    [PropertyOrder(2)]
    public string DatabaseConnectionString { get; set; }

    [LocalizableCategory("RabbitMQConnection", typeof(Localization.Configuration.Configuration))]
    [LocalizableDisplayName("RabbitMqHostname", typeof(Localization.Configuration.Configuration))]
    [LocalizedDescription("RabbitMqHostnameDescription", typeof(Localization.Configuration.Configuration))]
    [CustomValidation(typeof(ConfigurationViewModel), nameof(ValidateHostname))]
    [PropertyOrder(1)]
    public string RabbitMqHostname { get; set; }

    [LocalizableCategory("RabbitMQConnection", typeof(Localization.Configuration.Configuration))]
    [LocalizableDisplayName("RabbitMqPort", typeof(Localization.Configuration.Configuration))]
    [LocalizedDescription("RabbitMqPortDescription", typeof(Localization.Configuration.Configuration))]
    [CustomValidation(typeof(ConfigurationViewModel), nameof(ValidatePort))]
    [PropertyOrder(2)]
    public string RabbitMqPort { get; set; }

    [LocalizableCategory("RabbitMQConnection", typeof(Localization.Configuration.Configuration))]
    [LocalizableDisplayName("RabbitMqUsername", typeof(Localization.Configuration.Configuration))]
    [LocalizedDescription("RabbitMqUsernameDescription", typeof(Localization.Configuration.Configuration))]
    [CustomValidation(typeof(ConfigurationViewModel), nameof(ValidateString))]
    [PropertyOrder(3)]
    public string RabbitMqUsername { get; set; }

    [LocalizableCategory("RabbitMQConnection", typeof(Localization.Configuration.Configuration))]
    [LocalizableDisplayName("RabbitMqPassword", typeof(Localization.Configuration.Configuration))]
    [LocalizedDescription("RabbitMqPasswordDescription", typeof(Localization.Configuration.Configuration))]
    [CustomValidation(typeof(ConfigurationViewModel), nameof(ValidateString))]
    [PropertyOrder(4)]
    public string RabbitMqPassword { get; set; }

    [LocalizableCategory("Misc", typeof(Localization.Configuration.Configuration))]
    [LocalizableDisplayName("LogoFilePath", typeof(Localization.Configuration.Configuration))]
    [LocalizedDescription("LogoFilePathDescription", typeof(Localization.Configuration.Configuration))]
    [CustomValidation(typeof(ConfigurationViewModel), nameof(ValidateFilePath))]
    [Editor(typeof(FilePickerEditor), typeof(FilePickerEditor))]
    [PropertyOrder(1)]
    public string LogoFilePath { get; set; }

    public ConfigurationViewModel(BackendConfiguration configuration)
    {
      DatabaseType = configuration.DbConnection.DatabaseType;
      DatabaseConnectionString = configuration.DbConnection.ConnectionString;
      RabbitMqHostname = configuration.RabbitMqConnection.Hostname;
      RabbitMqUsername = configuration.RabbitMqConnection.Username;
      RabbitMqPassword = configuration.RabbitMqConnection.Password;
      RabbitMqPort = configuration.RabbitMqConnection.Port.ToString();
      LogoFilePath = configuration.ClientApplicationLogoFilePath;
    }

    public BackendConfiguration GetConfiguration()
    {
      return new BackendConfiguration
      {
        DbConnection = new DbConnectionConfiguration
        {
          DatabaseType = DatabaseType,
          ConnectionString = DatabaseConnectionString
        },
        RabbitMqConnection = new RabbitMqConnectionConfiguration
        {
          Hostname = RabbitMqHostname,
          Username = RabbitMqUsername,
          Password = RabbitMqPassword,
          Port = int.Parse(RabbitMqPort)
        },
        ClientApplicationLogoFilePath = LogoFilePath
      };
    }

    public static ValidationResult ValidateConnectionString(string text)
    {
      try
      {
        if (string.IsNullOrEmpty(text))
          return new ValidationResult(Localization.Configuration.Configuration.ConnectionStringEmpty);

        new DbConnectionStringBuilder { ConnectionString = text };
        return ValidationResult.Success;
      }
      catch (Exception)
      {
        return new ValidationResult(Localization.Configuration.Configuration.ConnectionStringIncorrect);
      }
    }

    public static ValidationResult ValidateHostname(string text)
    {
      if (!IsIpAddressValid(text))
        return new ValidationResult(Localization.Configuration.Configuration.RabbitMqHostnameIncorrect);

      return ValidationResult.Success;
    }

    public static ValidationResult ValidatePort(string port)
    {
      if (!int.TryParse(port, out int portNumber))
        return new ValidationResult(Localization.Configuration.Configuration.RabbitMqPortIncorrect);

      if (portNumber > 0 && portNumber <= 65535)
        return ValidationResult.Success;

      return new ValidationResult(Localization.Configuration.Configuration.RabbitMqPortIncorrect);
    }

    public static ValidationResult ValidateString(string text)
    {
      if (string.IsNullOrEmpty(text))
        return new ValidationResult(Localization.Configuration.Configuration.StringEmpty);

      return ValidationResult.Success;
    }

    public static ValidationResult ValidateFilePath(string text)
    {
      if (string.IsNullOrEmpty(text))
        return ValidationResult.Success;

      if (!File.Exists(text))
        return new ValidationResult(Localization.Configuration.Configuration.FilePathIncorrect);

      return ValidationResult.Success;
    }

    private static bool IsIpAddressValid(string address)
    {
      if (string.IsNullOrWhiteSpace(address))
        return false;

      string[] splitValues = address.Split('.');
      if (splitValues.Length != 4)
        return false;

      return splitValues.All(r => byte.TryParse(r, out _));
    }
  }
}
