﻿using Caliburn.Micro;
using Kalisto.Backend;
using Kalisto.Backend.Configuration;
using Kalisto.Hmi.Core.Services;
using Kalisto.Localization;
using Kalisto.Localization.Common;
using MahApps.Metro.Controls.Dialogs;
using PropertyChanged;

namespace Kalisto.Configuration.Application.ViewModels
{
  [AddINotifyPropertyChangedInterface]
  public class ConfigurationShellViewModel : Conductor<Screen>
  {
    private readonly ILocalizer<Common> _localizer;

    private readonly ConfigurationManager _configurationManager;

    private readonly IDialogService _dialogService;

    public ConfigurationViewModel ConfigurationViewModel { get; set; }

    public ConfigurationShellViewModel(IDialogService dialogService)
    {
      _localizer = new LocalizationManager<Common>();
      _dialogService = dialogService;
      _dialogService.RegisterContext(this);

      var appName = typeof(ServerEndpoint).Assembly.GetName().Name;
      _configurationManager = new ProgramDataConfigurationManager("Kalisto", appName);
    }

    protected override void OnViewReady(object view)
    {
      base.OnViewReady(view);

      var backendConfiguration = _configurationManager.Load<BackendConfiguration>();
      ConfigurationViewModel = new ConfigurationViewModel(backendConfiguration);
    }

    protected override void OnInitialize()
    {
      base.OnInitialize();

      DisplayName = "Waga Pro 2.0 - Manadżer konfiguracji";
    }

    public async void SaveConfiguration()
    {
      if (ConfigurationViewModel.HasErrors)
      {
        await _dialogService.ShowMessageAsync(_localizer.Localize(() => Common.Error),
          _localizer.Localize(() => Common.CorrectDialogModel), MessageDialogType.Ok);
        return;
      }

      var result = await _dialogService.ShowMessageAsync(_localizer.Localize(() => Common.Attention),
        _localizer.Localize(() => Common.SaveCurrentSettingsQuestion), MessageDialogType.YesNo);

      if (result == MessageDialogResult.Affirmative)
        _configurationManager.Save(ConfigurationViewModel.GetConfiguration());
    }

    public void CloseApplication()
    {
      TryClose();
    }
  }
}
