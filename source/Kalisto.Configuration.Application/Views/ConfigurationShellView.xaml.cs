﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MahApps.Metro.Controls;
using MahApps.Metro.Controls.Dialogs;

namespace Kalisto.Configuration.Application.Views
{
  /// <summary>
  /// Interaction logic for ConfigurationShellView.xaml
  /// </summary>
  public partial class ConfigurationShellView : MetroWindow
  {
    public ConfigurationShellView()
    {
      InitializeComponent();
    }
  }
}
