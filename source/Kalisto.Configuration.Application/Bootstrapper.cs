﻿using Kalisto.Configuration.Application.ViewModels;
using Kalisto.Hmi.Core;

namespace Kalisto.Configuration.Application
{
  public class Bootstrapper : UnityBootstraper<ConfigurationShellViewModel>
  {
  }
}
