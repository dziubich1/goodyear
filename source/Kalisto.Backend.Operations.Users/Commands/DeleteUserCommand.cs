﻿using Kalisto.Backend.Operations.Users.Commands.Interfaces;
using Kalisto.Communication.Core.Messages;

namespace Kalisto.Backend.Operations.Users.Commands
{
  public class DeleteUserCommand : BackendDbOperationMessage, IUserCommand
  {
    public long UserId { get; set; }
  }
}
