﻿using System;
using System.Data.Entity;
using System.Linq;
using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.DataAccess.Model.Logging;
using Kalisto.Backend.Operations.Users.Exceptions;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Core.Utils;
using Kalisto.Logging;

namespace Kalisto.Backend.Operations.Users.Commands
{
  public class UpdateUserCommandHandler : CommandHandlerBase<UpdateUserCommand>
  {
    private readonly IDbUpdateEventLogger _dbEventLogger;
    private readonly ILogger _logger;

    public UpdateUserCommandHandler(IDbUpdateEventLogger dbEventLogger, ILogger logger)
    {
      _dbEventLogger = dbEventLogger;
      _logger = logger;
    }

    public override object Execute(IDbContext context, UpdateUserCommand command, string partnerId)
    {
      var user = context.Users.Include(c => c.Role).FirstOrDefault(c => c.Id == command.UserId);
      if (user == null)
      {
        _logger.Error($"Cannot update user with ID: {command.UserId} because it does not exist");
        throw new ArgumentException("User with given identifier does not exist.");
      }

      if (context.Users.Any(c => c.Id != user.Id && c.Name == command.Username))
      {
        _logger.Info($"Cannot update user name because user with name: {command.Username} already exists. Updated user ID: {user.Id}");
        throw new UserExistsException();
      }

      // only for getting current user old values
      var oldUser = context.Users.AsNoTracking()
        .Include(c => c.Role).FirstOrDefault(c => c.Id == command.UserId);

      user.Name = command.Username;
      if (command.PasswordChanged)
        user.PasswordHash = PasswordHasher.CreateHash(command.Password);

      if (user.Role.Type != command.RoleNumber)
      {
        var role = context.UserRoles.FirstOrDefault(c => c.Type == command.RoleNumber);
        if (role == null)
        {
          _logger.Debug($"Cannot update user role because role with number: {command.RoleNumber} does not exists. Updated user ID: {user.Id}");
          throw new ArgumentException("Could not find user role with given identifier.");
        }

        user.Role = role;
      }

      context.SaveChanges();

      _dbEventLogger.LogEvent(partnerId, ctx => ctx.Users, user.Id, user, oldUser);
      _logger.Debug($"Successfully updated user with ID: {user.Id}");

      return user.Id;
    }
  }
}
