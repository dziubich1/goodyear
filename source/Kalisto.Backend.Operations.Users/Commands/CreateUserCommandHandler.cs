﻿using System;
using System.Linq;
using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.DataAccess.Model.Logging;
using Kalisto.Backend.Operations.Users.Exceptions;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Core.Utils;
using Kalisto.Logging;

namespace Kalisto.Backend.Operations.Users.Commands
{
  public class CreateUserCommandHandler : CommandHandlerBase<CreateUserCommand>
  {
    private readonly IDbInsertEventLogger _dbEventLogger;
    private readonly ILogger _logger;

    public CreateUserCommandHandler(IDbInsertEventLogger dbEventLogger, ILogger logger)
    {
      _dbEventLogger = dbEventLogger;
      _logger = logger;
    }

    public override object Execute(IDbContext context, CreateUserCommand command, string partnerId)
    {
      var role = context.UserRoles.FirstOrDefault(c => c.Type == command.RoleNumber);
      if (role == null)
      {
        _logger.Error($"Cannot create user with role number: {command.RoleNumber} because this role does not exist");
        throw new ArgumentException("Could not find user role with given identifier.");
      }

      if (context.Users.Any(c => c.Name == command.Username))
      {
        _logger.Info($"Cannot create user with name: {command.Username} because it already exist");
        throw new UserExistsException();
      }

      var user = new User
      {
        Name = command.Username,
        PasswordHash = PasswordHasher.CreateHash(command.Password),
        Role = role
      };

      context.Users.Add(user);
      context.SaveChanges();

      _dbEventLogger.LogEvent(partnerId, ctx => ctx.Users, user.Id, user);
      _logger.Debug($"Successfully created user with ID: {user.Id}");

      return user.Id;
    }
  }
}
