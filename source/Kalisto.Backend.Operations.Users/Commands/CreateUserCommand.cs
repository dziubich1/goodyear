﻿using Kalisto.Backend.Operations.Users.Commands.Interfaces;
using Kalisto.Communication.Core.Messages;

namespace Kalisto.Backend.Operations.Users.Commands
{
  public class CreateUserCommand : BackendDbOperationMessage, IUserCommand
  {
    public string Username { get; set; }

    public string Password { get; set; }

    public int RoleNumber { get; set; }
  }
}
