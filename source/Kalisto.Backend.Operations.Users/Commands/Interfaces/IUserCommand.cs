﻿using Kalisto.Communication.Core.Messages;

namespace Kalisto.Backend.Operations.Users.Commands.Interfaces
{
  public interface IUserCommand : IAssociatedCommand
  {
  }
}
