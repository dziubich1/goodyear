﻿using Kalisto.Backend.Operations.Users.Commands.Interfaces;
using Kalisto.Communication.Core.Messages;

namespace Kalisto.Backend.Operations.Users.Commands
{
  public class UpdateUserCommand : BackendDbOperationMessage, IUserCommand
  {
    public long UserId { get; set; }

    public string Username { get; set; }

    public string Password { get; set; }

    public int RoleNumber { get; set; }

    public bool PasswordChanged { get; set; }
  }
}
