﻿using System;
using System.Data.Entity;
using System.Linq;
using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.DataAccess.Model.Logging;
using Kalisto.Backend.Operations.Users.Exceptions;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Logging;

namespace Kalisto.Backend.Operations.Users.Commands
{
  public class DeleteUserCommandHandler : CommandHandlerBase<DeleteUserCommand>
  {
    private readonly IDbDeleteEventLogger _dbEventLogger;
    private readonly ILogger _logger;

    public DeleteUserCommandHandler(IDbDeleteEventLogger dbEventLogger, ILogger logger)
    {
      _dbEventLogger = dbEventLogger;
      _logger = logger;
    }

    public override object Execute(IDbContext context, DeleteUserCommand command, string partnerId)
    {
      var user = context.Users
        .Include(u => u.Measurements)
        .FirstOrDefault(u => u.Id == command.UserId);

      if (user == null)
      {
        _logger.Error($"Cannot delete user with ID: {command.UserId} because it does not exist");
        throw new ArgumentException("User with given identifier does not exist.");
      }

      if (user.Measurements.Count > 0)
      {
        _logger.Info($"Cannot delete user with ID: {command.UserId} because it is used in measurements");
        throw new UserLockedByRelationException();
      }

      context.Users.Remove(user);
      context.SaveChanges();

      _dbEventLogger.LogEvent(partnerId, ctx => ctx.Users, user.Id, null, user);
      _logger.Debug($"Successfully deleted user with ID: {user.Id}");
      
      return command.UserId;
    }
  }
}
