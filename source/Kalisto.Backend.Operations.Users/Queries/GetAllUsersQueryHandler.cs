﻿using Kalisto.Backend.DataAccess.Model;
using System.Data.Entity;
using System.Linq;
using Kalisto.Communication.Core.Messaging;

namespace Kalisto.Backend.Operations.Users.Queries
{
  public class GetAllUsersQueryHandler : QueryHandlerBase<GetAllUsersQuery>
  {
    public override object Execute(IDbContext context, GetAllUsersQuery query, string partnerId)
    {
      return context.Users.Include(c => c.Role).ToList();
    }
  }
}
