﻿using System;
using System.Runtime.Serialization;
using Kalisto.Communication.Core.Exceptions;

namespace Kalisto.Backend.Operations.Users.Exceptions
{
  [Serializable]
  public class UserExistsException : ExceptionBase
  {
    public UserExistsException()
    {
    }

    // REMARKS: below constructor need to be added due to the serialization/deserialization issues
    public UserExistsException(SerializationInfo info, StreamingContext context)
    {
    }

    public override string ExceptionCode => "USERS_1001";

    public override string Message => GetLocalizedMessage();
  }
}
