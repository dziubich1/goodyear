﻿using System;
using System.Runtime.Serialization;
using Kalisto.Communication.Core.Exceptions;

namespace Kalisto.Backend.Operations.Users.Exceptions
{
  [Serializable]
  public class UserLockedByRelationException : ExceptionBase
  {
    public UserLockedByRelationException()
    {
    }

    // REMARKS: below constructor need to be added due to the serialization/deserialization issues
    public UserLockedByRelationException(SerializationInfo info, StreamingContext context)
    {
    }

    public override string ExceptionCode => "USERS_1002";

    public override string Message => GetLocalizedMessage();
  }
}
