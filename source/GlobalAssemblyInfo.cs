﻿using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyProduct("Waga Pro 2.0")]
[assembly: AssemblyCompany("Kalisto")]
[assembly: AssemblyCopyright("Copyright © Kalisto 2020")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: AssemblyVersion("2.0.2.1")]
[assembly: AssemblyFileVersion("2.0.2.1")]
