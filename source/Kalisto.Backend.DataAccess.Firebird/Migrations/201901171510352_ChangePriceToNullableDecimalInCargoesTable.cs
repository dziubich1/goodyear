namespace Kalisto.Backend.DataAccess.Firebird.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangePriceToNullableDecimalInCargoesTable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Cargoes", "Price", c => c.Decimal(precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Cargoes", "Price", c => c.Int(nullable: false));
        }
    }
}
