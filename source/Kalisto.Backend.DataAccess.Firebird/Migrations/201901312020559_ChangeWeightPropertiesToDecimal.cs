namespace Kalisto.Backend.DataAccess.Firebird.Migrations
{
  using System;
  using System.Data.Entity.Migrations;

  public partial class ChangeWeightPropertiesToDecimal : DbMigration
  {
    public override void Up()
    {
      //This method is intentionally empty. Firebird has problems with updating column types (in this case, from int to decimal).
      //That's why old migrations were changed manually (updated all table ID's to long) and to make it run, this empty migration had to be added.
    }

    public override void Down()
    {

    }
  }
}
