using System.Linq;
using Kalisto.Backend.DataAccess.Model;
using Kalisto.Core.Utils;

namespace Kalisto.Backend.DataAccess.Firebird.Migrations
{
  using System.Data.Entity.Migrations;

  internal sealed class Configuration : DbMigrationsConfiguration<FirebirdDbContext>
  {
    public Configuration()
    {
      AutomaticMigrationsEnabled = false;
    }

    protected override void Seed(FirebirdDbContext context)
    {
      context.UserRoles.AddOrUpdate(c => c.Type,
        new UserRole { Type = 0 },
        new UserRole { Type = 1 },
        new UserRole { Type = 2 });
      context.SaveChanges();

      context.Users.AddOrUpdate(c => c.Name,
        new User
        {
          Name = "admin", PasswordHash = PasswordHasher.CreateHash("admin"),
          Role = context.UserRoles.Single(c => c.Type == 0)
        });
      context.SaveChanges();
    }
  }
}
