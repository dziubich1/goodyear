namespace Kalisto.Backend.DataAccess.Firebird.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedMeasurementClassColumnToNullable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Measurements", "Class", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Measurements", "Class", c => c.Int(nullable: false));
        }
    }
}
