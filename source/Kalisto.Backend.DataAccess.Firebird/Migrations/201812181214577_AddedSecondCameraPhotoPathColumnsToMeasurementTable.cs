namespace Kalisto.Backend.DataAccess.Firebird.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedSecondCameraPhotoPathColumnsToMeasurementTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Measurements", "FirstPhoto1RelPath", c => c.String());
            AddColumn("dbo.Measurements", "FirstPhoto2RelPath", c => c.String());
            AddColumn("dbo.Measurements", "FirstPhoto3RelPath", c => c.String());
            AddColumn("dbo.Measurements", "SecondPhoto1RelPath", c => c.String());
            AddColumn("dbo.Measurements", "SecondPhoto2RelPath", c => c.String());
            AddColumn("dbo.Measurements", "SecondPhoto3RelPath", c => c.String());
            DropColumn("dbo.Measurements", "Photo1RelPath");
            DropColumn("dbo.Measurements", "Photo2RelPath");
            DropColumn("dbo.Measurements", "Photo3RelPath");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Measurements", "Photo3RelPath", c => c.String());
            AddColumn("dbo.Measurements", "Photo2RelPath", c => c.String());
            AddColumn("dbo.Measurements", "Photo1RelPath", c => c.String());
            DropColumn("dbo.Measurements", "SecondPhoto3RelPath");
            DropColumn("dbo.Measurements", "SecondPhoto2RelPath");
            DropColumn("dbo.Measurements", "SecondPhoto1RelPath");
            DropColumn("dbo.Measurements", "FirstPhoto3RelPath");
            DropColumn("dbo.Measurements", "FirstPhoto2RelPath");
            DropColumn("dbo.Measurements", "FirstPhoto1RelPath");
        }
    }
}
