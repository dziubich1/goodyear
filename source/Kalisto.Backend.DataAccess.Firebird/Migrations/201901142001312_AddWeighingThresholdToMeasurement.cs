namespace Kalisto.Backend.DataAccess.Firebird.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddWeighingThresholdToMeasurement : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Measurements", "WeighingThreshold", c => c.Decimal(precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Measurements", "WeighingThreshold");
        }
    }
}
