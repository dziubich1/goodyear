namespace Kalisto.Backend.DataAccess.Firebird.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedCardCodeToMeasurementsTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Measurements", "CardCode", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Measurements", "CardCode");
        }
    }
}
