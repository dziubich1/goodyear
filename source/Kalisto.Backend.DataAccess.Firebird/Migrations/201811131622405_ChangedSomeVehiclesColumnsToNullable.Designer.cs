// <auto-generated />
namespace Kalisto.Backend.DataAccess.Firebird.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class ChangedSomeVehiclesColumnsToNullable : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(ChangedSomeVehiclesColumnsToNullable));
        
        string IMigrationMetadata.Id
        {
            get { return "201811131622405_ChangedSomeVehiclesColumnsToNullable"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
