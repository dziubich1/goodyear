namespace Kalisto.Backend.DataAccess.Firebird.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddBeltConveyorMeasurementsTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BeltConveyorMeasurements",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Number = c.Long(nullable: false),
                        ConfigId = c.Int(nullable: false),
                        B1 = c.Decimal(nullable: false, precision: 18, scale: 2),
                        B2 = c.Decimal(nullable: false, precision: 18, scale: 2),
                        B3 = c.Decimal(nullable: false, precision: 18, scale: 2),
                        B4 = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TotalSum = c.Decimal(nullable: false, precision: 18, scale: 2),
                        IncrementSum = c.Decimal(nullable: false, precision: 18, scale: 2),
                        DateUtc = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.BeltConveyorMeasurements");
        }
    }
}
