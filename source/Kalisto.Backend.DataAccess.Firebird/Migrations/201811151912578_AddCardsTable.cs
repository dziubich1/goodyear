namespace Kalisto.Backend.DataAccess.Firebird.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCardsTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Cards",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Number = c.Int(nullable: false),
                        Code = c.String(),
                        Lifetime = c.Int(nullable: false),
                        MeasurementClass = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        Cargo_Id = c.Long(),
                        Contractor_Id = c.Long(),
                        Driver_Id = c.Long(),
                        Vehicle_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cargoes", t => t.Cargo_Id)
                .ForeignKey("dbo.Contractors", t => t.Contractor_Id, name: "FK_Cards_Cont_Id")
                .ForeignKey("dbo.Drivers", t => t.Driver_Id)
                .ForeignKey("dbo.Vehicles", t => t.Vehicle_Id)
                .Index(t => t.Cargo_Id)
                .Index(t => t.Contractor_Id)
                .Index(t => t.Driver_Id)
                .Index(t => t.Vehicle_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Cards", "Vehicle_Id", "dbo.Vehicles");
            DropForeignKey("dbo.Cards", "Driver_Id", "dbo.Drivers");
            DropForeignKey("dbo.Cards", "FK_Cards_Cont_Id");
            DropForeignKey("dbo.Cards", "Cargo_Id", "dbo.Cargoes");
            DropIndex("dbo.Cards", new[] { "Vehicle_Id" });
            DropIndex("dbo.Cards", new[] { "Driver_Id" });
            DropIndex("dbo.Cards", new[] { "Contractor_Id" });
            DropIndex("dbo.Cards", new[] { "Cargo_Id" });
            DropTable("dbo.Cards");
        }
    }
}
