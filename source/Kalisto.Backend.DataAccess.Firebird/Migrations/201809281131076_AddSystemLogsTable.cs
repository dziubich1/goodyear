namespace Kalisto.Backend.DataAccess.Firebird.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSystemLogsTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SystemLogs",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        LogResourceKey = c.String(),
                        EventDate = c.DateTime(nullable: false),
                        Source = c.String(),
                        Parameters = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.SystemLogs");
        }
    }
}
