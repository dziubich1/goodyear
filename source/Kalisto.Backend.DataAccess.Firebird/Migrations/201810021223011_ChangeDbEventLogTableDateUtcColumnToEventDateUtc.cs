namespace Kalisto.Backend.DataAccess.Firebird.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeDbEventLogTableDateUtcColumnToEventDateUtc : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DbEventLogs", "EventDateUtc", c => c.DateTime(nullable: false));
            DropColumn("dbo.DbEventLogs", "DateUtc");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DbEventLogs", "DateUtc", c => c.DateTime(nullable: false));
            DropColumn("dbo.DbEventLogs", "EventDateUtc");
        }
    }
}
