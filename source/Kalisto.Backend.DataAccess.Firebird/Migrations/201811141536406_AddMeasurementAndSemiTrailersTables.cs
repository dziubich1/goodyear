namespace Kalisto.Backend.DataAccess.Firebird.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMeasurementAndSemiTrailersTables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Measurements",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Number = c.Long(nullable: false),
                        Type = c.Int(nullable: false),
                        Class = c.Int(nullable: false),
                        Date1 = c.DateTime(nullable: false),
                        Date2 = c.DateTime(),
                        Tare = c.Decimal(nullable: false),
                        Measurement1 = c.Decimal(nullable: false),
                        Measurement2 = c.Decimal(),
                        NetWeight = c.Decimal(nullable: false),
                        IsCancelled = c.Boolean(nullable: false),
                        IsManual = c.Boolean(nullable: false),
                        Comment = c.String(),
                        Cargo_Id = c.Long(),
                        Contractor_Id = c.Long(),
                        Driver_Id = c.Long(),
                        Operator_Id = c.Long(),
                        SemiTrailer_Id = c.Long(),
                        Vehicle_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Cargoes", t => t.Cargo_Id, name: "FK_Meas_Cargo_Id")
                .ForeignKey("dbo.Contractors", t => t.Contractor_Id, name: "FK_Meas_Contractor_Id")
                .ForeignKey("dbo.Drivers", t => t.Driver_Id, name: "FK_Meas_Driver_Id")
                .ForeignKey("dbo.Users", t => t.Operator_Id, name: "FK_Meas_Operator_Id")
                .ForeignKey("dbo.SemiTrailers", t => t.SemiTrailer_Id, name: "FK_Meas_SemiTrailer_Id")
                .ForeignKey("dbo.Vehicles", t => t.Vehicle_Id, name: "FK_Meas_Vehicle_Id")
                .Index(t => t.Cargo_Id)
                .Index(t => t.Contractor_Id)
                .Index(t => t.Driver_Id)
                .Index(t => t.Operator_Id)
                .Index(t => t.SemiTrailer_Id)
                .Index(t => t.Vehicle_Id);
            
            CreateTable(
                "dbo.SemiTrailers",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Number = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Measurements", "FK_Meas_Vehicle_Id");
            DropForeignKey("dbo.Measurements", "FK_Meas_SemiTrailer_Id");
            DropForeignKey("dbo.Measurements", "FK_Meas_Operator_Id");
            DropForeignKey("dbo.Measurements", "FK_Meas_Driver_Id");
            DropForeignKey("dbo.Measurements", "FK_Meas_Contractor_Id");
            DropForeignKey("dbo.Measurements", "FK_Meas_Cargo_Id");
            DropIndex("dbo.Measurements", new[] { "Vehicle_Id" });
            DropIndex("dbo.Measurements", new[] { "SemiTrailer_Id" });
            DropIndex("dbo.Measurements", new[] { "Operator_Id" });
            DropIndex("dbo.Measurements", new[] { "Driver_Id" });
            DropIndex("dbo.Measurements", new[] { "Contractor_Id" });
            DropIndex("dbo.Measurements", new[] { "Cargo_Id" });
            DropTable("dbo.SemiTrailers");
            DropTable("dbo.Measurements");
        }
    }
}
