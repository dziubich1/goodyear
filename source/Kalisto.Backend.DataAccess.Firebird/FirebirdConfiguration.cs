﻿using System.Data.Entity;
using EntityFramework.Firebird;

namespace Kalisto.Backend.DataAccess.Firebird
{
  public class FirebirdConfiguration : DbConfiguration
  {
    public FirebirdConfiguration()
    {
      SetProviderServices(FbProviderServices.ProviderInvariantName,
        FbProviderServices.Instance);
    }
  }
}
