﻿using System;
using System.Data.Common;
using System.Data.Entity;
using EntityFramework.Firebird;
using Kalisto.Backend.DataAccess.Firebird.Migrations;
using Kalisto.Backend.DataAccess.Model;

namespace Kalisto.Backend.DataAccess.Firebird
{
  [DbConfigurationType(typeof(FirebirdConfiguration))]
  public class FirebirdDbContext : DbContext, IDbContext
  {
    public IDbSet<User> Users { get; set; }

    public IDbSet<UserRole> UserRoles { get; set; }

    public IDbSet<SystemLog> SystemLogs { get; set; }

    public IDbSet<DbEventLog> DbEventLogs { get; set; }

    public IDbSet<Contractor> Contractors { get; set; }

    public IDbSet<Cargo> Cargoes { get; set; }

    public IDbSet<Driver> Drivers { get; set; }

    public IDbSet<Vehicle> Vehicles { get; set; }

    public IDbSet<Measurement> Measurements { get; set; }

    public IDbSet<SemiTrailer> SemiTrailers { get; set; }

    public IDbSet<Card> Cards { get; set; }

    public IDbSet<BeltConveyorMeasurement> BeltConveyorMeasurements { get; set; }

    public FirebirdDbContext() : base("MigrationConnectionString")
    {
      Database.SetInitializer(new MigrateDatabaseToLatestVersion<FirebirdDbContext, Configuration>());
    }

    public FirebirdDbContext(string connectionString)
      : base(GetConnection(connectionString), true)
    {
      Database.SetInitializer(new CreateDatabaseIfNotExists<FirebirdDbContext>());
      Configuration.ProxyCreationEnabled = false;
    }

    private static DbConnection GetConnection(string connectionString)
    {
      var factory = DbProviderFactories.GetFactory(FbProviderServices.ProviderInvariantName);
      var connection = factory.CreateConnection();
      if (connection == null)
        throw new InvalidOperationException();

      connection.ConnectionString = connectionString;

      return connection;
    }
  }
}
