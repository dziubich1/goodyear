﻿using System.Reflection;
using log4net;

namespace Kalisto.Logging
{
  public class Logger : ILogger
  {
    private readonly ILog _logger;

    public Logger()
    {
      _logger = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
    }

    public void Debug(string message)
    {
      _logger.Debug(message);
    }

    public void Error(string message)
    {
      _logger.Error(message);
    }

    public void Fatal(string message)
    {
      _logger.Fatal(message);
    }

    public void Info(string message)
    {
      _logger.Info(message);
    }
  }
}
