﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kalisto.Hmi.Dialogs.Common.Enum
{
  public enum YesNo
  {
    Yes,
    No
  }
}
