﻿using Kalisto.Hmi.Dialogs.Common.Enum;

namespace Kalisto.Hmi.Dialogs.Common
{
  public class UnitOfMeasurement
  {
    private readonly IUnitTypeService _unitTypeService;

    private readonly decimal? _originalValue;

    public decimal? OriginalValue => _originalValue;

    public string DisplayValue => GetDisplayValue();

    public string DisplayValueUnitless => GetDisplayValue(false);

    public string DashedDisplayValue => GetPositiveDisplayValue();

    public string DashedDisplayValueUnitless => GetPositiveDisplayValue(false);

    public UnitOfMeasurement()
    {
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="value">Value expressed in KG</param>
    /// <param name="unitTypeService"></param>
    public UnitOfMeasurement(decimal? value, IUnitTypeService unitTypeService)
    {
      _originalValue = value;
      _unitTypeService = unitTypeService;
    }

    private string GetDisplayValue(bool withUnit = true)
    {
      if (_unitTypeService == null)
        return "---";

      if (_originalValue == null)
        return null;

      var convertedValue = _originalValue.Value;
      var currentUnit = _unitTypeService.GetCurrentUnit();
      if (currentUnit == UnitType.T)
        convertedValue = _originalValue.Value / 1000;

      var formattedValue = $"{convertedValue:F0}";
      if (currentUnit == UnitType.T)
        formattedValue = $"{convertedValue:F3}";

      if (withUnit)
        return $"{formattedValue} {_unitTypeService.GetLocalizedCurrentUnit()}";
      return $"{formattedValue}";
    }

    private string GetPositiveDisplayValue(bool withUnit = true)
    {
      var dashedValue = "---";
      if (_unitTypeService == null)
        return dashedValue;

      if (_originalValue == null || _originalValue < 0)
        return dashedValue;

      var convertedValue = _originalValue.Value;
      var currentUnit = _unitTypeService.GetCurrentUnit();
      if (currentUnit == UnitType.T)
        convertedValue = _originalValue.Value / 1000;

      var formattedValue = $"{convertedValue:F0}";
      if (currentUnit == UnitType.T)
        formattedValue = $"{convertedValue:F3}";

      if (withUnit)
        return $"{formattedValue} {_unitTypeService.GetLocalizedCurrentUnit()}";
      return $"{formattedValue}";
    }
  }
}
