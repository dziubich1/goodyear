﻿using Kalisto.Hmi.Dialogs.Common.Configuration;
using Kalisto.Hmi.Dialogs.Common.Enum;
using Kalisto.Localization;

namespace Kalisto.Hmi.Dialogs.Common
{
  public interface IUnitTypeService
  {
    UnitType GetCurrentUnit();

    string GetLocalizedCurrentUnit();

    UnitOfMeasurement ConvertBackValue(decimal value);

    UnitOfMeasurement ConvertInValue(decimal value);
  }

  public class UnitTypeService : IUnitTypeService
  {
    private readonly ILocalizer<Localization.Common.Common> _localizer;

    private readonly IUserPreferencesService _userPreferencesService;

    public UnitTypeService(IUserPreferencesService userPreferencesService)
    {
      _localizer = new LocalizationManager<Localization.Common.Common>();
      _userPreferencesService = userPreferencesService;
    }

    public UnitType GetCurrentUnit()
    {
      return _userPreferencesService.Configuration?.UnitType ?? UnitType.Kg;
    }

    public string GetLocalizedCurrentUnit()
    {
      var currentUnit = GetCurrentUnit();
      if (currentUnit == UnitType.Kg)
        return _localizer.Localize(() => Localization.Common.Common.Kilograms);

      if (currentUnit == UnitType.T)
        return _localizer.Localize(() => Localization.Common.Common.Tones);

      return string.Empty;
    }

    public UnitOfMeasurement ConvertBackValue(decimal value)
    {
      var currentUnit = GetCurrentUnit();
      if (currentUnit == UnitType.Kg)
        return new UnitOfMeasurement(value, this);

      if (currentUnit == UnitType.T)
        return new UnitOfMeasurement(value * 1000, this);

      return null;
    }

    public UnitOfMeasurement ConvertInValue(decimal value)
    {
      var currentUnit = GetCurrentUnit();
      if (currentUnit == UnitType.Kg)
        return new UnitOfMeasurement(value, this);

      if (currentUnit == UnitType.T)
        return new UnitOfMeasurement(value / 1000, this);

      return null;
    }
  }
}
