﻿namespace Kalisto.Hmi.Dialogs.Common.Configuration
{
  public interface IUserPreferencesService
  {
    UserPreferencesConfiguration Configuration { get; }

    void StorePreferences(UserPreferencesConfiguration preferences);
  }

  public class UserPreferencesService : IUserPreferencesService
  {
    private readonly UserPreferencesConfigurationManager _configurationManager;

    private UserPreferencesConfiguration _configuration;

    public UserPreferencesConfiguration Configuration => GetConfiguration();

    public UserPreferencesService(UserPreferencesConfigurationManager configurationManager)
    {
      _configurationManager = configurationManager;
    }

    public void StorePreferences(UserPreferencesConfiguration preferences)
    {
      _configurationManager.Save(preferences);
      _configuration = _configurationManager.Load();
    }

    private UserPreferencesConfiguration GetConfiguration()
    {
      if (_configuration == null)
        _configuration = _configurationManager.Load();

      return _configuration;
    }
  }
}
