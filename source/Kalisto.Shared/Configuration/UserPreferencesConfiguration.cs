﻿using Kalisto.Configuration;
using Kalisto.Hmi.Dialogs.Common.Enum;

namespace Kalisto.Hmi.Dialogs.Common.Configuration
{
  public class UserPreferencesConfiguration : ConfigurationBase
  {
    public UnitType UnitType { get; set; }

    public YesNo IncludeImagesOnPrintouts { get; set; }

    public YesNo DoubleCopy { get; set; }

    public YesNoAsk PrintWeighingReceiptAfterMeasurement { get; set; }

    public decimal WeighingThreshold { get; set; }

    public override object GetDefaultConfiguration()
    {
      return new UserPreferencesConfiguration
      {
        UnitType = UnitType.Kg,
        IncludeImagesOnPrintouts = YesNo.No,
        DoubleCopy = YesNo.Yes,
        PrintWeighingReceiptAfterMeasurement = YesNoAsk.Ask,
        WeighingThreshold = 0
      };
    }
  }
}
