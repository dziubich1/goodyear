﻿using Kalisto.Configuration;

namespace Kalisto.Hmi.Dialogs.Common.Configuration
{
  public class UserPreferencesConfigurationManager : AppDataConfigurationManager<UserPreferencesConfiguration>
  {
    protected override string ConfigFileName => "UserPreferences.xml";

    public UserPreferencesConfigurationManager(string domainName, string appName) 
      : base(domainName, appName)
    {
    }
  }
}
