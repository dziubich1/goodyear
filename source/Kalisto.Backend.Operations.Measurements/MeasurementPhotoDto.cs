﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kalisto.Backend.Operations.Measurements
{
  public enum DoubleMeasurementNumber
  {
    First, Second
  }

  public class MeasurementPhotoDto
  {
    public DoubleMeasurementNumber Number { get; set; }

    public string PhotoBase64String { get; set; }
  }
}
