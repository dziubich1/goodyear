﻿using Kalisto.Backend.DataAccess.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kalisto.Backend.Operations.Measurements.Services
{
    public interface IReceiptNumberingService
    {
        string GetNextReceiptNumberAsync(int year, MeasurementClass measClass);
    }
}
