﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Communication.Core.Messaging;
using System.Data.Entity;
using System.Linq;

namespace Kalisto.Backend.Operations.Measurements.Queries
{
  public class GetMeasurementsQueryHandler : QueryHandlerBase<GetMeasurementsQuery>
  {
    public override object Execute(IDbContext context, GetMeasurementsQuery query, string partnerId)
    {
      return context.Measurements
        .Where(c => c.Date1Utc >= query.DateFromUtc && c.Date1Utc <= query.DateToUtc)
        .Where(c => c.Date2Utc == null || c.Date2Utc >= query.DateFromUtc && c.Date2Utc <= query.DateToUtc)
        .OrderBy(c => c.Date1Utc)
        .ThenBy(c => c.Date2Utc)
        .Include(c => c.Contractor)
        .Include(c => c.Driver)
        .Include(c => c.Vehicle)
        .Include(c => c.Cargo)
        .Include(c => c.SemiTrailer)
        .Include(c => c.Operator)
        .AsNoTracking()
        .ToList();
    }
  }
}
