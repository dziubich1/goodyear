﻿using Kalisto.Communication.Core.Messages;

namespace Kalisto.Backend.Operations.Measurements.Queries
{
  public class GetMeasurementByIdQuery : BackendDbOperationMessage
  {
    public long MeasurementId { get; set; }
  }
}
