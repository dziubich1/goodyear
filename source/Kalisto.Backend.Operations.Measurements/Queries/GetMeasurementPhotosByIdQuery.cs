﻿using Kalisto.Communication.Core.Messages;

namespace Kalisto.Backend.Operations.Measurements.Queries
{
  public class GetMeasurementPhotosByIdQuery : BackendDbOperationMessage
  {
    public long MeasurementId { get; set; }
  }
}
