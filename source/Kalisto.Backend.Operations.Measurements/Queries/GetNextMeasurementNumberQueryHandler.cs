﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Communication.Core.Messaging;
using System.Linq;

namespace Kalisto.Backend.Operations.Measurements.Queries
{
  public class GetNextMeasurementNumberQueryHandler : QueryHandlerBase<GetNextMeasurementNumberQuery>
  {
    public override object Execute(IDbContext context, GetNextMeasurementNumberQuery query, string partnerId)
    {
      if (!context.Measurements.Any())
        return 1;

      return context.Measurements.Max(c => c.Number) + 1;
    }
  }
}
