﻿using System;
using Kalisto.Communication.Core.Messages;

namespace Kalisto.Backend.Operations.Measurements.Queries
{
  public class GetMeasurementsQuery : BackendDbOperationMessage
  {
    public DateTime DateFromUtc { get; set; }

    public DateTime DateToUtc { get; set; }
  }
}
