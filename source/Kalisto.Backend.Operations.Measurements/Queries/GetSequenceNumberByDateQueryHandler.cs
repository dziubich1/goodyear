﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Communication.Core.Messaging;
using System.Data.Entity;
using System.Linq;

namespace Kalisto.Backend.Operations.Measurements.Queries
{
  public class GetSequenceNumberByDateQueryHandler : QueryHandlerBase<GetSequenceNumberByDateQuery>
  {
    public override object Execute(IDbContext context, GetSequenceNumberByDateQuery query, string partnerId)
    {
      var measurements = context.Measurements.AsNoTracking().Where(c =>
          c.Class == query.Class &&
          (c.Type == MeasurementType.Single && c.Date1Utc.Year == query.Year ||
           c.Type == MeasurementType.Double &&
            (c.Date2Utc != null && c.Date2Utc.Value.Year == query.Year ||
             c.Date2Utc == null && c.Date1Utc.Year == query.Year)))
        .OrderBy(c => c.Date1Utc)
        .ThenBy(c => c.Date2Utc).ToList();

      var measurement = measurements.FirstOrDefault(c => c.Id == query.MeasurementId);
      if (measurement == null)
        return 0;

      var index = measurements.IndexOf(measurement);
      return index + 1;
    }
  }
}
