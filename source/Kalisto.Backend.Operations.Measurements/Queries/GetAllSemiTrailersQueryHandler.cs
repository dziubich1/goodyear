﻿using System.Data.Entity;
using System.Linq;
using Kalisto.Backend.DataAccess.Model;
using Kalisto.Communication.Core.Messaging;

namespace Kalisto.Backend.Operations.Measurements.Queries
{
  public class GetAllSemiTrailersQueryHandler : QueryHandlerBase<GetAllSemiTrailersQuery>
  {
    public override object Execute(IDbContext context, GetAllSemiTrailersQuery query, string partnerId)
    {
      return context.SemiTrailers.AsNoTracking().ToList();
    }
  }
}
