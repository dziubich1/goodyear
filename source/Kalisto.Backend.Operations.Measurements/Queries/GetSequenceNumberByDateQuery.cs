﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Communication.Core.Messages;

namespace Kalisto.Backend.Operations.Measurements.Queries
{
  public class GetSequenceNumberByDateQuery : BackendDbOperationMessage
  {
    public long MeasurementId { get; set; }

    public MeasurementClass Class { get; set; }

    public int Year { get; set; }
  }
}
