﻿using Kalisto.Communication.Core.Messages;

namespace Kalisto.Backend.Operations.Measurements.Queries
{
  public class GetMeasurementByQrCodeQuery : BackendDbOperationMessage
  {
    public string QrCode { get; set; }
  }
}
