﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Communication.Core.Messaging;
using System.Data.Entity;
using System.Linq;

namespace Kalisto.Backend.Operations.Measurements.Queries
{
  public class GetNotCompletedMeasurementsQueryHandler : QueryHandlerBase<GetNotCompletedMeasurementsQuery>
  {
    public override object Execute(IDbContext context, GetNotCompletedMeasurementsQuery query, string partnerId)
    {
      return context.Measurements
        .Where(m => m.Type == MeasurementType.Double)
        .Where(m => !m.Date2Utc.HasValue)
        .OrderBy(m => m.Number)
        .Include(m => m.Contractor)
        .Include(m => m.Driver)
        .Include(m => m.Vehicle)
        .Include(m => m.Cargo)
        .Include(m => m.Operator)
        .Include(m => m.SemiTrailer)
        .AsNoTracking()
        .ToList();
    }
  }
}

