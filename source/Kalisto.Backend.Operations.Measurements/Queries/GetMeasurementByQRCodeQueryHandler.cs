﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Communication.Core.Messaging;
using System.Data.Entity;
using System.Linq;

namespace Kalisto.Backend.Operations.Measurements.Queries
{
  public class GetMeasurementByQrCodeQueryHandler : QueryHandlerBase<GetMeasurementByQrCodeQuery>
  {
    public override object Execute(IDbContext context, GetMeasurementByQrCodeQuery query, string partnerId)
    {
      var measurement = context.Measurements
        .Include(c => c.Contractor)
        .Include(c => c.Driver)
        .Include(c => c.Vehicle)
        .Include(c => c.Cargo)
        .Include(c => c.SemiTrailer)
        .Include(c => c.Operator)
        .AsNoTracking().FirstOrDefault(c => c.QRCode == query.QrCode);

      return measurement;
    }
  }
}
