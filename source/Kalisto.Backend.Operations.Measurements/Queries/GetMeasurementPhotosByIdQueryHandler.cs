﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Configuration;
using Kalisto.Core;
using Kalisto.Logging;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;

namespace Kalisto.Backend.Operations.Measurements.Queries
{
  public class GetMeasurementPhotosByIdQueryHandler : QueryHandlerBase<GetMeasurementPhotosByIdQuery>
  {
    private readonly ConfigurationManager _configurationManager;
    private readonly ILogger _logger;

    public GetMeasurementPhotosByIdQueryHandler(ConfigurationManager configurationManager, ILogger logger)
    {
      _configurationManager = configurationManager;
      _logger = logger;
    }

    public override object Execute(IDbContext context, GetMeasurementPhotosByIdQuery query, string partnerId)
    {
      var measurement = context.Measurements.FirstOrDefault(c => c.Id == query.MeasurementId);
      if (measurement == null)
      {
        _logger.Error($"Cannot get measureent with ID: {query.MeasurementId}");
        throw new InvalidOperationException();
      }

      return new List<MeasurementPhotoDto>
      {
        new MeasurementPhotoDto
        {
          Number = DoubleMeasurementNumber.First,
          PhotoBase64String = GetPhoto(measurement.FirstPhoto1RelPath)
        },
        new MeasurementPhotoDto
        {
          Number = DoubleMeasurementNumber.First,
          PhotoBase64String = GetPhoto(measurement.FirstPhoto2RelPath)
        },
        new MeasurementPhotoDto
        {
          Number = DoubleMeasurementNumber.First,
          PhotoBase64String = GetPhoto(measurement.FirstPhoto3RelPath)
        },
        new MeasurementPhotoDto
        {
          Number = DoubleMeasurementNumber.Second,
          PhotoBase64String = GetPhoto(measurement.SecondPhoto1RelPath)
        },
        new MeasurementPhotoDto
        {
          Number = DoubleMeasurementNumber.Second,
          PhotoBase64String = GetPhoto(measurement.SecondPhoto2RelPath)
        },
        new MeasurementPhotoDto
        {
          Number = DoubleMeasurementNumber.Second,
          PhotoBase64String = GetPhoto(measurement.SecondPhoto3RelPath)
        }
      };
    }

    private string GetPhoto(string path)
    {
      if (string.IsNullOrEmpty(path))
        return string.Empty;

      try
      {
        var absolutePath = Path.Combine(_configurationManager.ConfigDirectory, path);
        if (!File.Exists(absolutePath))
          return null;

        using (var bmpTemp = new Bitmap(absolutePath))
        {
          var image = new Bitmap(bmpTemp);
          return image.ToBase64String(bmpTemp.RawFormat);
        }
      }
      catch (Exception ex)
      {
        _logger.Error($"Cannot get photo from {path}. Exceptions was thrown: {ex.Message}");
        return string.Empty;
      }
    }
  }
}
