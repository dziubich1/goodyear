﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Logging;
using System;
using System.Data.Entity;
using System.Linq;

namespace Kalisto.Backend.Operations.Measurements.Queries
{
  public class GetMeasurementByIdQueryHandler : QueryHandlerBase<GetMeasurementByIdQuery>
  {
    private readonly ILogger _logger;

    public GetMeasurementByIdQueryHandler(ILogger logger)
    {
      _logger = logger;
    }

    public override object Execute(IDbContext context, GetMeasurementByIdQuery query, string partnerId)
    {
      var measurement = context.Measurements
        .Include(c => c.Contractor)
        .Include(c => c.Driver)
        .Include(c => c.Vehicle)
        .Include(c => c.Cargo)
        .Include(c => c.SemiTrailer)
        .Include(c => c.Operator)
        .AsNoTracking().FirstOrDefault(c => c.Id == query.MeasurementId);

      if (measurement == null)
      {
        _logger.Error($"Cannot get measurement with ID: {query.MeasurementId} because it does not exist");
        throw new ArgumentException();
      }

      return measurement;
    }
  }
}
