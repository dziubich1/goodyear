﻿using Kalisto.Communication.Core.Messages;

namespace Kalisto.Backend.Operations.Measurements.Commands.Interfaces
{
  public interface IMeasurementCommand : IAssociatedCommand
  {
  }
}
