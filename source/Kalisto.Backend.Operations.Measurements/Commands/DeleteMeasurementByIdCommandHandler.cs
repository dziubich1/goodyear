﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.DataAccess.Model.Logging;
using Kalisto.Communication.Core.Messaging;
using System;
using System.Linq;
using Kalisto.Communication.Core.Logging;
using Kalisto.Logging;
using Kalisto.Core;

namespace Kalisto.Backend.Operations.Measurements.Commands
{
  public class DeleteMeasurementByIdCommandHandler : CommandHandlerBase<DeleteMeasurementByIdCommand>
  {
    private readonly IDbDeleteEventLogger _dbLogger;

    private readonly ISystemEventLogger _eventLogger;

    private readonly ILogger _logger;

    private readonly IXmlExporterService _xmlExporterService;

    public DeleteMeasurementByIdCommandHandler(IDbDeleteEventLogger dbLogger, ISystemEventLogger eventLogger, ILogger logger, IXmlExporterService xmlExporterService)
    {
      _dbLogger = dbLogger;
      _eventLogger = eventLogger;
      _logger = logger;
      _xmlExporterService = xmlExporterService;
    }

    public override object Execute(IDbContext context, DeleteMeasurementByIdCommand command, string partnerId)
    {
      var measurement = context.Measurements.FirstOrDefault(c => c.Id == command.MeasurementId);
      if (measurement == null)
      {
        _logger.Error($"Cannot delete measurement with ID: {command.MeasurementId} because it does not exist");
        throw new ArgumentException();
      }

      context.Measurements.Remove(measurement);
      context.SaveChanges();

      var measurementExport = new MeasurementExport(measurement, EntityOperationType.Delete);
      _xmlExporterService.Export(measurementExport, measurementExport.Id.ToString());

      _dbLogger.LogEvent(partnerId, c => c.Measurements, command.MeasurementId, null, measurement);
      _eventLogger.LogEvent(partnerId, "MEAS_DELETED", new[] { measurement.Id.ToString() });
      _logger.Debug($"Successfully deleted measurement with ID: {measurement.Id}");

      return command.MeasurementId;
    }
  }
}
