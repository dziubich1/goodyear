﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Communication.Core.Logging;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Configuration.Devices;
using Kalisto.Configuration.Devices.Connection;
using Kalisto.Core;
using Kalisto.Devices.Camera.Models;
using Kalisto.Devices.Camera.Services;
using Kalisto.Logging;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Kalisto.Backend.Operations.Measurements.Commands
{
  public class CreateOrCompleteMeasurementWithCardCommandHandler : CommandHandlerBase<CreateOrCompleteMeasurementWithCardCommand>
  {
    private readonly ISystemEventLogger _eventLogger;

    private readonly ILogger _logger;

    private readonly IMeasurementPhotoService _measurementPhotoService;

    private readonly ICameraService _cameraService;

    private readonly IXmlExporterService _xmlExporterService;

    private readonly ServerDeviceConfigurationManager _deviceConfigurationManager;

    public CreateOrCompleteMeasurementWithCardCommandHandler(ISystemEventLogger eventLogger, ILogger logger, IMeasurementPhotoService measurementPhotoService,
      ICameraService cameraService, IXmlExporterService xmlExporterService, ServerDeviceConfigurationManager deviceConfigurationManager)
    {
      _eventLogger = eventLogger;
      _logger = logger;
      _measurementPhotoService = measurementPhotoService;
      _cameraService = cameraService;
      _xmlExporterService = xmlExporterService;
      _deviceConfigurationManager = deviceConfigurationManager;
    }

    public override object Execute(IDbContext context, CreateOrCompleteMeasurementWithCardCommand command, string partnerId)
    {
      var card = context.Cards
        .Include(c => c.Cargo)
        .Include(c => c.Driver)
        .Include(c => c.Vehicle)
        .Include(c => c.Contractor)
        .FirstOrDefault(c => c.Code == command.CardCode);

      if (!ValidateCard(card, command.CardCode))
        return -1;

      var firstMeasurement =
        context.Measurements.FirstOrDefault(c =>
          c.CardCode == command.CardCode && c.Date2Utc == null); // try to find incomplete measurement for given card code
      if (firstMeasurement == null)
        return PerformFirstMeasurement(context, card, command.DeviceId, command.WeightValue);

      return PerformSecondMeasurement(context, firstMeasurement, card, command.DeviceId, command.WeightValue);
    }

    private bool ValidateCard(Card card, string cardCode)
    {
      if (card == null)
      {
        _logger.Error($"Measurement failed: could not find card with code {cardCode}.");
        _eventLogger.LogEvent("NO_CARD_FOUND", new string[] { cardCode });
        return false;
      }

      if (!card.IsActive)
      {
        _logger.Error($"Measurement failed: card with code {cardCode} is inactive.");
        _eventLogger.LogEvent("CARD_INACTIVE", new string[] { cardCode });
        return false;
      }

      return true;
    }

    private long PerformFirstMeasurement(IDbContext context, Card card, int deviceId, decimal weightValue)
    {
      var nextMeasurementNumber = 1L;
      if (context.Measurements.Any())
        nextMeasurementNumber = context.Measurements.Max(c => c.Number) + 1;

      var cargo = card.Cargo;
      var driver = card.Driver;
      var vehicle = card.Vehicle;
      var contractor = card.Contractor;
      var utcNow = DateTime.UtcNow;
      var photoPaths = TakeCameraSnapshots(deviceId, nextMeasurementNumber, utcNow);
      var measurement = new Measurement
      {
        Number = nextMeasurementNumber,
        Type = MeasurementType.Double,
        Class = null,
        Date1Utc = utcNow,
        Date2Utc = null,
        DeviceId = deviceId,
        FirstPhoto1RelPath = photoPaths.ElementAtOrDefault(0),
        FirstPhoto2RelPath = photoPaths.ElementAtOrDefault(1),
        FirstPhoto3RelPath = photoPaths.ElementAtOrDefault(2),
        Measurement1 = weightValue,
        Measurement2 = null,
        DeclaredWeight = null,
        Tare = 0,
        NetWeight = 0,
        Comment = null,
        IsManual = false,
        IsCancelled = false,
        Operator = null,
        Driver = driver,
        SemiTrailer = null,
        Vehicle = vehicle,
        Cargo = cargo,
        Contractor = contractor,
        CardCode = card.Code
      };

      context.Measurements.Add(measurement);
      context.SaveChanges();

      _eventLogger.LogEvent("CARD_MEAS_CREATED", new[] { measurement.Id.ToString() });
      _logger.Info($"Successfully created card measurement with ID: {measurement.Id}");

      return measurement.Id;
    }

    private long PerformSecondMeasurement(IDbContext context, Measurement measurement, Card card, int deviceId, decimal weightValue)
    {
      if (measurement.Type == MeasurementType.Single)
      {
        _logger.Error($"Cannot complete measurement with ID: {measurement.Id} because it is single measurement.");
        return -1;
      }

      var cargo = card.Cargo;
      var driver = card.Driver;
      var vehicle = card.Vehicle;
      var contractor = card.Contractor;
      var utcNow = DateTime.UtcNow;
      var maxWeight = new[] { measurement.Measurement1, weightValue }.Max();
      var minWeight = new[] { measurement.Measurement1, weightValue }.Min();
      var photoPaths = TakeCameraSnapshots(deviceId, measurement.Number, utcNow);

      measurement.Class = measurement.Measurement1 > weightValue ? MeasurementClass.Income : MeasurementClass.Outcome;
      measurement.DeviceId = deviceId;
      measurement.Date2Utc = utcNow;
      measurement.Measurement2 = weightValue;
      measurement.DeclaredWeight = null;
      measurement.WeighingThreshold = null;
      measurement.Tare = minWeight;
      measurement.NetWeight = maxWeight - minWeight;
      measurement.Comment = null;
      measurement.Operator = null;
      measurement.Driver = driver;
      measurement.SemiTrailer = null;
      measurement.Vehicle = vehicle;
      measurement.Cargo = cargo;
      measurement.Contractor = contractor;
      measurement.SecondPhoto1RelPath = photoPaths.ElementAtOrDefault(0);
      measurement.SecondPhoto2RelPath = photoPaths.ElementAtOrDefault(1);
      measurement.SecondPhoto3RelPath = photoPaths.ElementAtOrDefault(2);
      measurement.CardCode = card.Code;

      if (card.Lifetime.HasValue && card.Lifetime.Value == CardLifetime.Disposable)
        card.IsActive = false;

      context.SaveChanges();

      _eventLogger.LogEvent("CARD_MEAS_COMPLETED", new[] { measurement.Id.ToString() });
      _logger.Info($"Successfully completed card measurement with ID: {measurement.Id}");

      try
      {
        _xmlExporterService.Export(measurement, measurement.Id.ToString());
      }
      catch (Exception e)
      {
        _logger.Error($"Could not export measurement to XML file. Exception was thrown: {e}");
      }

      return measurement.Id;
    }

    private List<string> TakeCameraSnapshots(int deviceId, long measurementNumber, DateTime measurementDateUtc)
    {
      var cameras = _deviceConfigurationManager.Load().Cameras
        .Where(c => c.WeightMeterId == deviceId && c.IsActive).ToList();
      var photoPaths = new List<string>();
      foreach (var cam in cameras)
      {
        try
        {
          var connectionInfo = cam.ConnectionConfiguration as HttpConnectionConfiguration;
          var photoResult = _cameraService.TakeSnapshot(new CameraInfo
          {
            CameraId = cam.Id,
            DeviceId = cam.WeightMeterId,
            CameraAddress = connectionInfo?.Url,
            Username = connectionInfo?.Username,
            Password = connectionInfo?.Password
          });

          if (photoResult?.Image == null)
            continue;

          var path = _measurementPhotoService.Store(photoResult.Image, $"{Guid.NewGuid().ToString()}.png", measurementNumber, measurementDateUtc);
          if (!string.IsNullOrEmpty(path))
            photoPaths.Add(path);
        }
        catch (Exception e)
        {
          _logger.Error($"Could not take a camera snapshot (Device ID: {deviceId}). Exception was thrown: {e}.");
        }
      }

      return photoPaths;
    }
  }
}
