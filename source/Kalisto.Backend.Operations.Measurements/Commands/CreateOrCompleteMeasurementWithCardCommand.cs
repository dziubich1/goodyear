﻿using Kalisto.Backend.Operations.Measurements.Commands.Interfaces;
using Kalisto.Communication.Core.Messages;

namespace Kalisto.Backend.Operations.Measurements.Commands
{
  public class CreateOrCompleteMeasurementWithCardCommand : BackendDbOperationMessage, IMeasurementCommand
  {
    public int DeviceId { get; set; }

    public string CardCode { get; set; }

    public decimal WeightValue { get; set; }
  }
}
