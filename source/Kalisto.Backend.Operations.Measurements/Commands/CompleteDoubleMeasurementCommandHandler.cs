﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.DataAccess.Model.Logging;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Communication.Core.Registries;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Kalisto.Backend.StateMachine;
using Kalisto.Backend.StateMachine.Payloads;
using Kalisto.Communication.Core.Logging;
using Kalisto.Configuration.CompanyInfo;
using Kalisto.Configuration.Devices;
using Kalisto.Configuration.Devices.Connection;
using Kalisto.Core;
using Kalisto.Devices.Camera.Models;
using Kalisto.Devices.Camera.Services;
using Kalisto.Logging;

namespace Kalisto.Backend.Operations.Measurements.Commands
{
  public class CompleteDoubleMeasurementCommandHandler : CommandHandlerBase<CompleteDoubleMeasurementCommand>
  {
    private readonly IDbUpdateEventLogger _dbLogger;

    private readonly ISystemEventLogger _eventLogger;

    private readonly IPartnerRegistryReader _partnerRegistryReader;

    private readonly IMeasurementPhotoService _measurementPhotoService;

    private readonly ICameraService _cameraService;

    private readonly ServerDeviceConfigurationManager _deviceConfigurationManager;

    private readonly CompanyInfoConfiguration _companyInfoConfiguration;

    private readonly ILogger _logger;

    private readonly IXmlExporterService _xmlExporterService;

    private readonly IMeasurementStateMachineRegistry _stateMachineRegistry;

    public CompleteDoubleMeasurementCommandHandler(IDbUpdateEventLogger dbLogger, ISystemEventLogger eventLogger,
      IPartnerRegistryReader partnerRegistryReader, IMeasurementPhotoService measurementPhotoService,
      ICameraService cameraService, ServerDeviceConfigurationManager deviceConfigurationManager,
      CompanyInfoConfigurationManager companyInfoConfigurationManager, ILogger logger, IXmlExporterService xmlExporterService, IMeasurementStateMachineRegistry stateMachineRegistry)
    {
      _dbLogger = dbLogger;
      _eventLogger = eventLogger;
      _partnerRegistryReader = partnerRegistryReader;
      _measurementPhotoService = measurementPhotoService;
      _cameraService = cameraService;
      _deviceConfigurationManager = deviceConfigurationManager;
      _companyInfoConfiguration = companyInfoConfigurationManager.Load();
      _logger = logger;
      _xmlExporterService = xmlExporterService;
      _stateMachineRegistry = stateMachineRegistry;
    }

    public override object Execute(IDbContext context, CompleteDoubleMeasurementCommand command, string partnerId)
    {
      var measurement =
        context.Measurements.FirstOrDefault(c =>
          c.Id == command.MeasurementId && c.Number == command.MeasurementNumber);

      if (measurement == null)
      {
        _logger.Error($"Cannot complete measurement with ID: {command.MeasurementId} because it does not exist");
        throw new ArgumentException();
      }

      if (measurement.Type == MeasurementType.Single)
      {
        _logger.Error($"Cannot complete measurement with ID: {command.MeasurementId} because it is single measurement");
        throw new InvalidOperationException("Exception occurred while completing double measurement. " +
          "Cannot perform requested operation on single measurement.");
      }

      var oldMeasurement =
      context.Measurements.AsNoTracking().FirstOrDefault(c =>
        c.Id == command.MeasurementId && c.Number == command.MeasurementNumber);

      var user = _partnerRegistryReader.GetCurrentUser(partnerId);
      var userEntity = context.Users.FirstOrDefault(c => c.Name == user.Name);

      Cargo cargo = null;
      if (command.CargoId != null)
        cargo = context.Cargoes.FirstOrDefault(c => c.Id == command.CargoId);

      Driver driver = null;
      if (command.DriverId != null)
        driver = context.Drivers.FirstOrDefault(c => c.Id == command.DriverId);

      Vehicle vehicle = null;
      if (command.VehicleId != null)
        vehicle = context.Vehicles.FirstOrDefault(c => c.Id == command.VehicleId);

      Contractor contractor = null;
      if (command.ContractorId != null)
        contractor = context.Contractors.FirstOrDefault(c => c.Id == command.ContractorId);

      SemiTrailer semiTrailer = null;
      if (!string.IsNullOrEmpty(command.SemiTrailerNumber))
      {
        semiTrailer = context.SemiTrailers.FirstOrDefault(c => c.Number == command.SemiTrailerNumber);
        if (semiTrailer == null)
        {
          semiTrailer = new SemiTrailer
          {
            Number = command.SemiTrailerNumber
          };

          context.SemiTrailers.Add(semiTrailer);
          _logger.Debug($"Successfully created semi trailer with number: {command.SemiTrailerNumber} during measurement completing");
        }
      }

      var maxWeight = new[] { measurement.Measurement1, command.Measurement }.Max();
      var minWeight = new[] { measurement.Measurement1, command.Measurement }.Min();
      var photoPaths = TakeCameraSnapshots(command.DeviceId, measurement.Number, command.MeasurementDateUtc);

      measurement.Class = command.Class;
      measurement.DeviceId = command.DeviceId;
      measurement.Date2Utc = command.MeasurementDateUtc;
      measurement.Measurement2 = command.Measurement;
      measurement.DeclaredWeight = command.DeclaredWeight;
      measurement.WeighingThreshold = command.WeighingThreshold;
      measurement.Tare = minWeight;
      measurement.NetWeight = maxWeight - minWeight;
      measurement.Comment = command.Comment;
      measurement.Operator = userEntity;
      measurement.Driver = driver;
      measurement.SemiTrailer = semiTrailer;
      measurement.Vehicle = vehicle;
      measurement.Cargo = cargo;
      measurement.Contractor = contractor;
      measurement.SecondPhoto1RelPath = photoPaths.ElementAtOrDefault(0);
      measurement.SecondPhoto2RelPath = photoPaths.ElementAtOrDefault(1);
      measurement.SecondPhoto3RelPath = photoPaths.ElementAtOrDefault(2);
      measurement.IsManual = command.IsManual;

      context.SaveChanges();

      // inform state machine about measurement stored event to handle semi-auto mode
      _stateMachineRegistry.Fire(MeasurementEvent.MeasurementStored, new StateMachinePayloadBase
      {
        AssignedWeightMeterDeviceId = command.DeviceId
      });

      var measurementExport = new MeasurementExport(measurement, EntityOperationType.Create);
      _xmlExporterService.Export(measurementExport, measurementExport.Id.ToString());

      _dbLogger.LogEvent(partnerId, c => c.Measurements, measurement.Id, measurement, oldMeasurement);
      _eventLogger.LogEvent(partnerId, "DOUBLE_MEAS_COMPLETED", new[] { measurement.Id.ToString() });
      _logger.Debug($"Successfully completed measurement with ID: {measurement.Id}");

      return measurement.Id;
    }

    private List<string> TakeCameraSnapshots(int deviceId, long measurementNumber, DateTime measurementDateUtc)
    {
      var cameras = _deviceConfigurationManager.Load().Cameras
        .Where(c => c.WeightMeterId == deviceId && c.IsActive).ToList();
      var photoPaths = new List<string>();
      foreach (var cam in cameras)
      {
        try
        {
          var connectionInfo = cam.ConnectionConfiguration as HttpConnectionConfiguration;
          var photoResult = _cameraService.TakeSnapshot(new CameraInfo
          {
            CameraId = cam.Id,
            DeviceId = cam.WeightMeterId,
            CameraAddress = connectionInfo?.Url,
            Username = connectionInfo?.Username,
            Password = connectionInfo?.Password
          });

          if (photoResult?.Image == null)
            continue;

          var path = _measurementPhotoService.Store(photoResult.Image, $"{Guid.NewGuid().ToString()}.png", measurementNumber, measurementDateUtc);
          if (!string.IsNullOrEmpty(path))
            photoPaths.Add(path);
        }
        catch
        {
          // ignore
        }
      }

      return photoPaths;
    }
  }
}
