﻿using Kalisto.Backend.Operations.Measurements.Commands.Interfaces;
using Kalisto.Communication.Core.Messages;

namespace Kalisto.Backend.Operations.Measurements.Commands
{
  public class DeleteMeasurementByIdCommand : BackendDbOperationMessage, IMeasurementCommand
  {
    public long MeasurementId { get; set; }
  }
}
