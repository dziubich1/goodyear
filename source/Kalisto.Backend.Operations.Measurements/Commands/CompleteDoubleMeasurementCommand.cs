﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.Operations.Measurements.Commands.Interfaces;
using Kalisto.Communication.Core.Messages;
using System;

namespace Kalisto.Backend.Operations.Measurements.Commands
{
  public class CompleteDoubleMeasurementCommand : BackendDbOperationMessage, IMeasurementCommand
  {
    public long MeasurementId { get; set; }

    public long MeasurementNumber { get; set; }

    public MeasurementClass Class { get; set; }

    public int DeviceId { get; set; }

    public long? ContractorId { get; set; }

    public long? CargoId { get; set; }

    public long? DriverId { get; set; }

    public long? VehicleId { get; set; }

    public string SemiTrailerNumber { get; set; }

    public decimal Measurement { get; set; }

    public DateTime MeasurementDateUtc { get; set; }

    public decimal? DeclaredWeight { get; set; }

    public decimal? WeighingThreshold { get; set; }

    public string Comment { get; set; }

    public bool IsManual { get; set; }
  }
}
