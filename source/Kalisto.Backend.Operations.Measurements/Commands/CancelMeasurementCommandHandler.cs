﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.DataAccess.Model.Logging;
using Kalisto.Communication.Core.Messaging;
using System;
using System.Data.Entity;
using System.Linq;
using Kalisto.Communication.Core.Logging;
using Kalisto.Logging;
using Kalisto.Core;

namespace Kalisto.Backend.Operations.Measurements.Commands
{
  public class CancelMeasurementCommandHandler : CommandHandlerBase<CancelMeasurementCommand>
  {
    private readonly IDbUpdateEventLogger _dbEventLogger;

    private readonly ISystemEventLogger _eventLogger;

    private readonly ILogger _logger;

    private readonly IXmlExporterService _xmlExporterService;

    public CancelMeasurementCommandHandler(IDbUpdateEventLogger dbEventLogger, ISystemEventLogger eventLogger, ILogger logger, IXmlExporterService xmlExporterService)
    {
      _dbEventLogger = dbEventLogger;
      _eventLogger = eventLogger;
      _logger = logger;
      _xmlExporterService = xmlExporterService;
    }

    public override object Execute(IDbContext context, CancelMeasurementCommand command, string partnerId)
    {
      var measurement = context.Measurements.FirstOrDefault(c => c.Id == command.MeasurementId);
      if (measurement == null)
      {
        _logger.Error($"Cannot cancel measurement with ID: {command.MeasurementId} because it does not exist");
        throw new ArgumentException("Measurement with given identifier does not exist.");
      }

      var oldMeasurement = context.Measurements.AsNoTracking().FirstOrDefault(c => c.Id == command.MeasurementId);

      measurement.IsCancelled = true;
      context.SaveChanges();

      //Do not export when it is 1st measurement of double measurement (we export only finished double measurements)
      if (measurement.Type == MeasurementType.Single || 
         (measurement.Type == MeasurementType.Double && measurement.Measurement2 != null))
      {
        var measurementExport = new MeasurementExport(measurement, EntityOperationType.Update);
        _xmlExporterService.Export(measurementExport, measurementExport.Id.ToString());
      }

      _dbEventLogger.LogEvent(partnerId, ctx => ctx.Measurements, measurement.Id, measurement, oldMeasurement);
      _eventLogger.LogEvent(partnerId, "MEAS_CANCELED", new[] { measurement.Id.ToString() });
      _logger.Debug($"Succesfully cancelled measurment with ID: {measurement.Id}");

      return measurement.Id;
    }
  }
}
