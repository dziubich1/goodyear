﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Communication.Core.Messages;
using System;
using Kalisto.Backend.Operations.Measurements.Commands.Interfaces;

namespace Kalisto.Backend.Operations.Measurements.Commands
{
  public class CreateDoubleMeasurementWithQrCodeCommand : BackendDbOperationMessage, IMeasurementCommand
  {
    public int DeviceId { get; set; }

    public long? ContractorId { get; set; }

    public long? CargoId { get; set; }

    public long? DriverId { get; set; }

    public long? VehicleId { get; set; }

    public string SemiTrailerNumber { get; set; }

    public decimal Measurement { get; set; }

    public DateTime MeasurementDateUtc { get; set; }

    public decimal? DeclaredWeight { get; set; }

    public string QrCode { get; set; }

    public bool IsManual { get; set; }

    public string Comment { get; set; }
  }
}
