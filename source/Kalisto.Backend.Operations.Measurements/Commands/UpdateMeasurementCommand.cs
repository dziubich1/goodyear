﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Communication.Core.Messages;
using System;
using Kalisto.Backend.Operations.Measurements.Commands.Interfaces;

namespace Kalisto.Backend.Operations.Measurements.Commands
{
  public class UpdateMeasurementCommand : BackendDbOperationMessage, IMeasurementCommand
  {
    public long MeasurementId { get; set; }

    public DateTime MeasurementDate1Utc { get; set; }

    public DateTime? MeasurementDate2Utc { get; set; }

    public decimal Measurement1 { get; set; }

    public decimal? Measurement2 { get; set; }

    public decimal NetWeight { get; set; }

    public decimal? DeclaredWeight { get; set; }

    public decimal Tare { get; set; }

    public MeasurementClass? Class { get; set; }

    public long? ContractorId { get; set; }

    public long? CargoId { get; set; }

    public long? DriverId { get; set; }

    public long? VehicleId { get; set; }

    public string SemiTrailerNumber { get; set; }

    public string Comment { get; set; }

    public bool IsCancelled { get; set; }
  }
}
