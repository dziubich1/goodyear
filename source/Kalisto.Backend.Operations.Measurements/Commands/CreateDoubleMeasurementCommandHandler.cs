﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.DataAccess.Model.Logging;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Communication.Core.Registries;
using System;
using System.Collections.Generic;
using System.Linq;
using Kalisto.Backend.StateMachine;
using Kalisto.Backend.StateMachine.Payloads;
using Kalisto.Communication.Core.Logging;
using Kalisto.Configuration.CompanyInfo;
using Kalisto.Configuration.Devices;
using Kalisto.Configuration.Devices.Connection;
using Kalisto.Core;
using Kalisto.Devices.Camera.Models;
using Kalisto.Devices.Camera.Services;
using Kalisto.Logging;

namespace Kalisto.Backend.Operations.Measurements.Commands
{
  public class CreateDoubleMeasurementCommandHandler : CommandHandlerBase<CreateDoubleMeasurementCommand>
  {
    private readonly IDbInsertEventLogger _dbLogger;

    private readonly ISystemEventLogger _eventLogger;

    private readonly IPartnerRegistryReader _partnerRegistryReader;

    private readonly IMeasurementPhotoService _measurementPhotoService;

    private readonly ICameraService _cameraService;

    private readonly ServerDeviceConfigurationManager _deviceConfigurationManager;

    private readonly CompanyInfoConfiguration _companyInfoConfiguration;

    private readonly ILogger _logger;

    private readonly IMeasurementStateMachineRegistry _stateMachineRegistry;

    public CreateDoubleMeasurementCommandHandler(IDbInsertEventLogger dbLogger, ISystemEventLogger eventLogger,
      IPartnerRegistryReader partnerRegistryReader, IMeasurementPhotoService measurementPhotoService,
      ICameraService cameraService, ServerDeviceConfigurationManager deviceConfigurationManager,
      CompanyInfoConfigurationManager companyInfoConfigurationManager, ILogger logger, IMeasurementStateMachineRegistry stateMachineRegistry)
    {
      _dbLogger = dbLogger;
      _eventLogger = eventLogger;
      _partnerRegistryReader = partnerRegistryReader;
      _measurementPhotoService = measurementPhotoService;
      _cameraService = cameraService;
      _deviceConfigurationManager = deviceConfigurationManager;
      _companyInfoConfiguration = companyInfoConfigurationManager.Load();
      _logger = logger;
      _stateMachineRegistry = stateMachineRegistry;
    }

    public override object Execute(IDbContext context, CreateDoubleMeasurementCommand command, string partnerId)
    {
      var nextMeasurementNumber = 1L;
      if (context.Measurements.Any())
        nextMeasurementNumber = context.Measurements.Max(c => c.Number) + 1;

      var user = _partnerRegistryReader.GetCurrentUser(partnerId);
      var userEntity = context.Users.FirstOrDefault(c => c.Name == user.Name);

      Cargo cargo = null;
      if (command.CargoId != null)
        cargo = context.Cargoes.FirstOrDefault(c => c.Id == command.CargoId);

      Driver driver = null;
      if (command.DriverId != null)
        driver = context.Drivers.FirstOrDefault(c => c.Id == command.DriverId);

      Vehicle vehicle = null;
      if (command.VehicleId != null)
        vehicle = context.Vehicles.FirstOrDefault(c => c.Id == command.VehicleId);

      Contractor contractor = null;
      if (command.ContractorId != null)
        contractor = context.Contractors.FirstOrDefault(c => c.Id == command.ContractorId);

      SemiTrailer semiTrailer = null;
      if (!string.IsNullOrEmpty(command.SemiTrailerNumber))
      {
        semiTrailer = context.SemiTrailers.FirstOrDefault(c => c.Number == command.SemiTrailerNumber);
        if (semiTrailer == null)
        {
          semiTrailer = new SemiTrailer
          {
            Number = command.SemiTrailerNumber
          };

          context.SemiTrailers.Add(semiTrailer);
          _logger.Debug($"Successfully created semi trailer with number: {command.SemiTrailerNumber} during double measurement creating");
        }
      }

      var photoPaths = TakeCameraSnapshots(command.DeviceId, nextMeasurementNumber, command.MeasurementDateUtc);
      var measurement = new Measurement
      {
        Number = nextMeasurementNumber,
        Type = MeasurementType.Double,
        Class = null,
        Date1Utc = command.MeasurementDateUtc,
        Date2Utc = null,
        DeviceId = command.DeviceId,
        FirstPhoto1RelPath = photoPaths.ElementAtOrDefault(0),
        FirstPhoto2RelPath = photoPaths.ElementAtOrDefault(1),
        FirstPhoto3RelPath = photoPaths.ElementAtOrDefault(2),
        Measurement1 = command.Measurement,
        Measurement2 = null,
        DeclaredWeight = command.DeclaredWeight,
        Tare = 0,
        NetWeight = 0,
        Comment = command.Comment,
        IsManual = command.IsManual,
        IsCancelled = false,
        Operator = userEntity,
        Driver = driver,
        SemiTrailer = semiTrailer,
        Vehicle = vehicle,
        Cargo = cargo,
        Contractor = contractor
      };

      context.Measurements.Add(measurement);
      context.SaveChanges();

      // inform state machine about measurement stored event to handle semi-auto mode
      _stateMachineRegistry.Fire(MeasurementEvent.MeasurementStored, new StateMachinePayloadBase
      {
        AssignedWeightMeterDeviceId = command.DeviceId
      });

      _dbLogger.LogEvent(partnerId, c => c.Measurements, measurement.Id, measurement);
      _eventLogger.LogEvent(partnerId, "DOUBLE_MEAS_CREATED", new [] { measurement.Id.ToString() });
      _logger.Debug($"Successfully created double measurement with ID: {measurement.Id}");

      return measurement.Id;
    }

    private List<string> TakeCameraSnapshots(int deviceId, long measurementNumber, DateTime measurementDateUtc)
    {
      var cameras = _deviceConfigurationManager.Load().Cameras
        .Where(c => c.WeightMeterId == deviceId && c.IsActive).ToList();
      var photoPaths = new List<string>();
      foreach (var cam in cameras)
      {
        try
        {
          var connectionInfo = cam.ConnectionConfiguration as HttpConnectionConfiguration;
          var photoResult = _cameraService.TakeSnapshot(new CameraInfo
          {
            CameraId = cam.Id,
            DeviceId = cam.WeightMeterId,
            CameraAddress = connectionInfo?.Url,
            Username = connectionInfo?.Username,
            Password = connectionInfo?.Password
          });

          if (photoResult?.Image == null)
            continue;

          var path = _measurementPhotoService.Store(photoResult.Image, $"{Guid.NewGuid().ToString()}.png", measurementNumber, measurementDateUtc);
          if (!string.IsNullOrEmpty(path))
            photoPaths.Add(path);
        }
        catch
        {
          // ignore
        }
      }

      return photoPaths;
    }
  }
}
