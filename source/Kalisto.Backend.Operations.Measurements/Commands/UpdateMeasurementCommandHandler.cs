﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.DataAccess.Model.Logging;
using Kalisto.Communication.Core.Logging;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Core;
using Kalisto.Logging;
using System;
using System.Data.Entity;
using System.Linq;

namespace Kalisto.Backend.Operations.Measurements.Commands
{
  public class UpdateMeasurementCommandHandler : CommandHandlerBase<UpdateMeasurementCommand>
  {
    private readonly IDbUpdateEventLogger _dbLogger;

    private readonly ISystemEventLogger _eventLogger;

    private readonly ILogger _logger;

    private readonly IXmlExporterService _xmlExporterService;

    public UpdateMeasurementCommandHandler(IDbUpdateEventLogger dbLogger, ISystemEventLogger eventLogger, ILogger logger, IXmlExporterService xmlExporterService)
    {
      _dbLogger = dbLogger;
      _eventLogger = eventLogger;
      _logger = logger;
      _xmlExporterService = xmlExporterService;
    }

    public override object Execute(IDbContext context, UpdateMeasurementCommand command, string partnerId)
    {
      var measurement = context.Measurements
        .Include(m => m.Cargo)
        .Include(m => m.Driver)
        .Include(m => m.Vehicle)
        .Include(m => m.Contractor)
        .Include(m => m.SemiTrailer)
        .FirstOrDefault(c => c.Id == command.MeasurementId);
      if (measurement == null)
      {
        _logger.Error($"Cannot update measurement with ID: {command.MeasurementId} because it does not exist");
        throw new ArgumentException("Measurement with given identifier does not exist.");
      }

      // only for getting current measurement old values
      var oldMeasurement = context.Measurements.AsNoTracking().FirstOrDefault(c => c.Id == command.MeasurementId);

      measurement.Date1Utc = command.MeasurementDate1Utc;
      measurement.Date2Utc = command.MeasurementDate2Utc;
      measurement.Measurement1 = command.Measurement1;
      measurement.Measurement2 = command.Measurement2;
      measurement.Tare = command.Tare;
      measurement.NetWeight = command.NetWeight;
      measurement.DeclaredWeight = command.DeclaredWeight;
      measurement.Comment = command.Comment?.Trim();
      measurement.Class = command.Class;
      measurement.IsCancelled = command.IsCancelled;

      measurement.Cargo = context.Cargoes.FirstOrDefault(c => c.Id == command.CargoId);
      measurement.Driver = context.Drivers.FirstOrDefault(c => c.Id == command.DriverId);
      measurement.Vehicle = context.Vehicles.FirstOrDefault(c => c.Id == command.VehicleId);
      measurement.Contractor = context.Contractors.FirstOrDefault(c => c.Id == command.ContractorId);

      if (!string.IsNullOrEmpty(command.SemiTrailerNumber))
      {
        var semiTrailer = context.SemiTrailers.FirstOrDefault(c => c.Number == command.SemiTrailerNumber);
        if (semiTrailer == null)
        {
          semiTrailer = new SemiTrailer
          {
            Number = command.SemiTrailerNumber
          };
          context.SemiTrailers.Add(semiTrailer);
        }

        measurement.SemiTrailer = semiTrailer;
        _logger.Debug($"Successfully created semi trailer with number: {command.SemiTrailerNumber} during measurement updating");
      }

      context.SaveChanges();

      if (measurement.Type == MeasurementType.Single || 
         (measurement.Type == MeasurementType.Double && measurement.Measurement2 != null))
      {
        var measurementExport = new MeasurementExport(measurement, EntityOperationType.Update);
        _xmlExporterService.Export(measurementExport, measurementExport.Id.ToString());
      }

      _dbLogger.LogEvent(partnerId, ctx => ctx.Measurements, measurement.Id, measurement, oldMeasurement);
      _eventLogger.LogEvent(partnerId, "MEAS_UPDATED", new[] { measurement.Id.ToString() });
      _logger.Debug($"Successfully updated measurement with ID: {measurement.Id}");

      return measurement.Id;
    }
  }
}
