﻿using Kalisto.Communication.Core.Messaging;
using Kalisto.Frontend.Notifications.Base;
using Kalisto.Hmi.Core.Communication;

namespace Kalisto.Frontend.Handlers
{
  public class NotificationHandler : MessageHandlerBase<NotificationBase>
  {
    private readonly IClientMessageRouter _messageAggregator;

    public NotificationHandler(IClientMessageRouter messageAggregator)
    {
      _messageAggregator = messageAggregator;
    }

    protected override void HandleMessage(NotificationBase message)
    {
      _messageAggregator.Route(message);
    }
  }
}
