﻿using System;
using System.Collections.Generic;
using System.Text;
using Kalisto.Backend.Operations.Shell.Commands;
using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Hmi.Core.Communication;
using Kalisto.Hmi.Core.Services;

namespace Kalisto.Frontend.Handlers
{
  public class LoginRequiredMessageHandler : MessageHandlerBase<LoginRequiredMessage>
  {
    private readonly IClientMessageRouter _messageAggregator;

    public LoginRequiredMessageHandler(IClientMessageRouter messageAggregator)
    {
      _messageAggregator = messageAggregator;
    }

    protected override void HandleMessage(LoginRequiredMessage message)
    {
      _messageAggregator.Route(message);
    }
  }
}
