﻿using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Hmi.Core.Communication;

namespace Kalisto.Frontend.Handlers
{
  public class CommandResponseMessageHandler : MessageHandlerBase<BackendDbOperationResponseMessage>
  {
    private readonly IClientMessageRouter _messageAggregator;

    public CommandResponseMessageHandler(IClientMessageRouter messageAggregator)
    {
      _messageAggregator = messageAggregator;
    }

    protected override void HandleMessage(BackendDbOperationResponseMessage message)
    {
      _messageAggregator.Route(message);
    }
  }
}
