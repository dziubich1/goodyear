﻿using Kalisto.Communication.Core.Messaging;
using Unity;

namespace Kalisto.Frontend.Handlers
{
  public class MessageHandlerFactory : MessageHandlerFactoryBase
  {
    public MessageHandlerFactory(IUnityContainer container) : base(container)
    {
    }

    public IMessageHandler GetNotificationMessageHandler()
    {
      return DependencyContainer.Resolve<NotificationHandler>();
    }

    public IMessageHandler GetLoginRequiredMessageHandler()
    {
      return DependencyContainer.Resolve<LoginRequiredMessageHandler>();
    }

    public IMessageHandler GetServerImageResponseMessageHandler()
    {
      return DependencyContainer.Resolve<ServerImageResponseMessageHandler>();
    }

    public IMessageHandler GetServerImageThumbnailResponseMessageHandler()
    {
      return DependencyContainer.Resolve<ServerImageThumbnailResponseMessageHandler>();
    }

    public IMessageHandler GetCommandResponseMessageHandler()
    {
      return DependencyContainer.Resolve<CommandResponseMessageHandler>();
    }

    public IMessageHandler GetClientLogoResponseMessageHandler()
    {
      return DependencyContainer.Resolve<ClientLogoResponseHandler>();
    }
  }
}
