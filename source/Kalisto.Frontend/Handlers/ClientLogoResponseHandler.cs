﻿using System;
using System.Collections.Generic;
using System.Text;
using Kalisto.Backend.Handlers;
using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Hmi.Core.Communication;

namespace Kalisto.Frontend.Handlers
{
  public class ClientLogoResponseHandler : MessageHandlerBase<ClientLogoResponseMessage>
  {
    private readonly IClientMessageRouter _messageAggregator;

    public ClientLogoResponseHandler(IClientMessageRouter messageAggregator)
    {
      _messageAggregator = messageAggregator;
    }

    protected override void HandleMessage(ClientLogoResponseMessage message)
    {
      _messageAggregator.Route(message);
    }
  }
}
