﻿using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Hmi.Core.Communication;

namespace Kalisto.Frontend.Handlers
{
  class ServerImageThumbnailResponseMessageHandler
    : MessageHandlerBase<ServerImageThumbnailResponse>
  {
    private readonly IClientMessageRouter _messageAggregator;

    public ServerImageThumbnailResponseMessageHandler(IClientMessageRouter messageAggregator)
    {
      _messageAggregator = messageAggregator;
    }

    protected override void HandleMessage(ServerImageThumbnailResponse message)
    {
      _messageAggregator.Route(message);
    }
  }
}
