﻿using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Hmi.Core.Communication;

namespace Kalisto.Frontend.Handlers
{
  class ServerImageResponseMessageHandler
    : MessageHandlerBase<ServerImageResponse>
  {
    private readonly IClientMessageRouter _messageAggregator;

    public ServerImageResponseMessageHandler(IClientMessageRouter messageAggregator)
    {
      _messageAggregator = messageAggregator;
    }

    protected override void HandleMessage(ServerImageResponse message)
    {
      _messageAggregator.Route(message);
    }
  }
}
