﻿using Kalisto.Communication.Core.Communication;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Communication.Core.Registries;

namespace Kalisto.Frontend
{
  public class FrontendProviderRegistry : MessageProviderRegistry
  {
    private readonly ClientInfo _clientInfo;

    private readonly ICommunicationComponentFactory _communicationComponentFactory;

    public FrontendProviderRegistry(ClientInfo clientInfo, ICommunicationComponentFactory communicationComponentFactory)
    {
      _clientInfo = clientInfo;
      _communicationComponentFactory = communicationComponentFactory;
    }

    public override void Initialize()
    {
      var registrationSender = _communicationComponentFactory.GetRegistrationMessageSender();
      RegisterProvider(registrationSender);

      var heartbeatSender = _communicationComponentFactory.GetHeartbeatMessageSender();
      RegisterProvider(heartbeatSender);

      var clientSender = _communicationComponentFactory.GetClientMessageSender(_clientInfo.Id);
      RegisterProvider(clientSender);
    }
  }
}
