﻿using System;
using System.Globalization;
using Kalisto.Communication.Core;
using Kalisto.Communication.Core.Communication;
using Kalisto.Communication.Core.Jobs;
using Kalisto.Communication.Core.Jobs.Base;
using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Communication.Core.Registries;
using Kalisto.Communication.RabbitMq;
using Kalisto.Configuration.Devices;
using Kalisto.Core;
using Kalisto.Devices.Camera.Drivers;
using Kalisto.Devices.Camera.Services;
using Kalisto.Devices.Core.Communication;
using Kalisto.Devices.Endpoint;
using Kalisto.Frontend.Handlers;
using Kalisto.Frontend.Notifications.Handlers;
using Kalisto.Frontend.Notifications.Misc;
using Kalisto.Hmi.Core.Communication;
using Kalisto.Hmi.Core.Jobs;
using Kalisto.Hmi.Core.Services;
using Kalisto.Hmi.Dialogs.BeltConveyorMeasurements.Services;
using Kalisto.Hmi.Dialogs.Cards.Services;
using Kalisto.Hmi.Dialogs.Cargoes.Services;
using Kalisto.Hmi.Dialogs.Common;
using Kalisto.Hmi.Dialogs.Common.Configuration;
using Kalisto.Hmi.Dialogs.Contractors.Services;
using Kalisto.Hmi.Dialogs.Dashboard.Services;
using Kalisto.Hmi.Dialogs.DbEventLogs.Services;
using Kalisto.Hmi.Dialogs.Drivers.Services;
using Kalisto.Hmi.Dialogs.Measurements.Services;
using Kalisto.Hmi.Dialogs.Shell;
using Kalisto.Hmi.Dialogs.Shell.Services;
using Kalisto.Hmi.Dialogs.SystemEventLogs.Services;
using Kalisto.Hmi.Dialogs.Users.Services;
using Kalisto.Hmi.Dialogs.Vehicles.Services;
using Kalisto.Printing;
using Unity;
using Unity.Injection;
using Unity.Lifetime;
using Kalisto.Hmi.Dialogs.Common.Services;
using Kalisto.Configuration;

namespace Kalisto.Frontend
{
  public class FrontendEndpoint : ClientEndpoint<FrontendConfiguration>
  {
    private readonly UserPreferencesConfigurationManager _userPreferencesConfigurationManager;

    private IRabbitMqConnectionFactory _connectionFactory;

    private IInitializationJobScheduler _initJobScheduler;

    public event Action<FrontendConfiguration> ConfigurationLoaded;

    public FrontendEndpoint(IUnityContainer container, string domainName, string appName)
      : base(container, domainName, appName)
    {
      _userPreferencesConfigurationManager
        = new UserPreferencesConfigurationManager(domainName, appName);
      _configurationManager = new AppDataConfigurationManager(domainName, appName);
    }

    protected override ClientType GetClientType() => ClientType.Frontend;

    protected override void Bind(IUnityContainer container)
    {
      base.Bind(container);

      container.RegisterType<IRabbitMqConnectionFactory, RabbitMqReusableConnectionFactory>(
        new ContainerControlledLifetimeManager());
      container.RegisterType<IRabbitMqChannelFactory, RabbitMqChannelFactory>(
        new ContainerControlledLifetimeManager());
      container.RegisterType<IMessageReceiver, RabbitMqMessageReceiver>();
      container.RegisterType<IMessageSender, RabbitMqMessageSender>();

      container.RegisterType<UserInfo>(new ContainerControlledLifetimeManager());
      container.RegisterType<ISubscriberRegistry, FrontendSubscriberRegistry>(new ContainerControlledLifetimeManager());
      container.RegisterType<IMessageProviderRegistry, FrontendProviderRegistry>(new ContainerControlledLifetimeManager());

      container.RegisterInstance<UserPreferencesConfigurationManager>(_userPreferencesConfigurationManager);
      container.RegisterSingleton<IUserPreferencesService, UserPreferencesService>();

      container.RegisterType<RabbitMqConnectionConfiguration>(new InjectionFactory(c =>
        Configuration.RabbitMqConnection));
      container.RegisterType<BarcodeScannerConfiguration>(new InjectionFactory(c =>
        Configuration.BarcodeScannerConfiguration));

      container.RegisterType<IClientMessageRouter, ClientMessageRouter>(new ContainerControlledLifetimeManager());
      container.RegisterType<CommandResponseMessageHandler>();
      container.RegisterType<MessageHandlerFactory>();
      container.RegisterType<ClientLogoResponseHandler>();
      container.RegisterSingleton<ResultMessageHandler>();
      container.RegisterSingleton<NotificationHandler>();
      container.RegisterSingleton<ServerImageResponseMessageHandler>();
      container.RegisterSingleton<ServerImageThumbnailResponseMessageHandler>();

      // FRONTEND SPECIFIC INITIALIZATION JOBS
      container.RegisterType<InitializationJobScheduler>(new ContainerControlledLifetimeManager());
      container.RegisterType<IInitializationJobScheduler, InitializationJobScheduler>();
      container.RegisterType<IInitializationJobSchedulerInfo, InitializationJobScheduler>();
      container.RegisterType<ClientRegistrationJob>();
      container.RegisterType<AssemblyVersionConsistencyCheckJob>();
      container.RegisterType<AssemblyVersionCompatibilityCheckJob>();
      container.RegisterType<ClientLogoRequestJob>();

      container.RegisterType<IPrintoutManager, PrintoutManager>();
      container.RegisterType<IUserService, UserService>();
      container.RegisterType<ISystemEventLogsService, SystemEventLogsService>();
      container.RegisterType<IDbEventLogsService, DbEventLogsService>();
      container.RegisterType<IContractorService, ContractorService>();
      container.RegisterType<ICargoService, CargoService>();
      container.RegisterType<IDriverService, DriverService>();
      container.RegisterType<IDashboardService, DashboardService>();
      container.RegisterType<IMeasurementService, MeasurementService>();
      container.RegisterType<IBeltConveyorMeasurementsService, BeltConveyorMeasurementsService>();

      container.RegisterSingleton<IUnitTypeService, UnitTypeService>();
      container.RegisterSingleton<IVehicleService, VehicleService>();
      container.RegisterSingleton<ISoundNotification, SoundNotification>();
      container.RegisterType<IWindowInputManager, WindowInputManager>();
      container.RegisterSingleton<INotificationHandlerFactory, NotificationHandlerFactory>();
      container.RegisterType<IDbNotificationService, DbNotificationService>();

      container.RegisterType<WeightUpdateNotificationHandler>();
      container.RegisterType<ICardService, CardService>();
      container.RegisterType<INumberingService, NumberingService>();
      container.RegisterType<IWeighingReportService, WeighingReportService>();
      container.RegisterSingleton<IWeighingDeviceService, WeighingDeviceService>();

      container.RegisterType<IServerImageService, ServerImageService>();
      container.RegisterSingleton<IServerDeviceConfiguration, ServerConfigurationLoader>();
      container.RegisterType<ICameraDriver, CameraDriver>();
      container.RegisterType<ICameraService, CameraService>();
      container.RegisterType<IFilesService, FilesService>();

      // local devices
      container.RegisterSingleton<IDeviceConnectionFactory, DeviceConnectionFactory>();
      container.RegisterSingleton<LocalDeviceMessageBus>();
      container.RegisterType<ILocalDeviceMessageBus, LocalDeviceMessageBus>();
      container.RegisterType<ILocalDeviceMessageBusPropagator, LocalDeviceMessageBus>();
    }

    protected override void Resolve(IUnityContainer container)
    {
      base.Resolve(container);

      _connectionFactory = container.Resolve<IRabbitMqConnectionFactory>();

      var assemblyConsistencyJob = container.Resolve<AssemblyVersionConsistencyCheckJob>();
      var registrationJob = container.Resolve<ClientRegistrationJob>();
      var assemblyCompatibilityCheckJob = container.Resolve<AssemblyVersionCompatibilityCheckJob>();
      var clientLogoRequestJob = container.Resolve<ClientLogoRequestJob>();

      _initJobScheduler = container.Resolve<IInitializationJobScheduler>();
      _initJobScheduler.ScheduleJob(assemblyConsistencyJob);
      _initJobScheduler.ScheduleJob(registrationJob);
      _initJobScheduler.ScheduleJob(assemblyCompatibilityCheckJob);
      _initJobScheduler.ScheduleJob(clientLogoRequestJob);
    }

    protected override async void OnInitialized()
    {
      base.OnInitialized();

      await _initJobScheduler.RunAsync();
    }

    protected override void OnConfigurationLoaded()
    {
      base.OnConfigurationLoaded();

      ConfigurationLoaded?.Invoke(Configuration);
    }

    protected override void OnDisconnect()
    {
      base.OnDisconnect();

      _connectionFactory.Dispose();
    }
  }
}
