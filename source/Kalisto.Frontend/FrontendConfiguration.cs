﻿using Kalisto.Communication.RabbitMq;
using Kalisto.Configuration;
using Kalisto.Configuration.Devices;
using Kalisto.Configuration.Devices.Connection;
using Kalisto.Configuration.Enums;

namespace Kalisto.Frontend
{
  public class FrontendConfiguration : ConfigurationBase
  {
    public RabbitMqConnectionConfiguration RabbitMqConnection { get; set; }

    public BarcodeScannerConfiguration BarcodeScannerConfiguration { get; set; }

    public string Language { get; set; }

    public override object GetDefaultConfiguration()
    {
      return new FrontendConfiguration
      {
        RabbitMqConnection = new RabbitMqConnectionConfiguration
        {
          Hostname = "127.0.0.1",
          Username = "admin",
          Password = "admin",
          Port = 5672
        },
        Language = "pl-PL"
      };
    }
  }
}
