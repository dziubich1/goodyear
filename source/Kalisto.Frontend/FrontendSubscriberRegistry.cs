﻿using Kalisto.Communication.Core.Communication;
using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Communication.Core.Registries;
using Kalisto.Frontend.Handlers;
using Kalisto.Frontend.Notifications.Base;
using Kalisto.Hmi.Core.Communication;

namespace Kalisto.Frontend
{
  public class FrontendSubscriberRegistry : SubscriberRegistry
  {
    private readonly ClientInfo _clientInfo;

    private readonly MessageHandlerFactory _messageHandlerFactory;

    private readonly ICommunicationComponentFactory _communicationComponentFactory;

    public FrontendSubscriberRegistry(ClientInfo clientInfo, MessageHandlerFactory messageHandlerFactory,
      ICommunicationComponentFactory communicationComponentFactory)
    {
      _clientInfo = clientInfo;
      _messageHandlerFactory = messageHandlerFactory;
      _communicationComponentFactory = communicationComponentFactory;
    }

    public override void Initialize()
    {
      var clientMessageReceiver = _communicationComponentFactory.GetClientMessageReceiver(_clientInfo.Id);
      RegisterSubscriber(clientMessageReceiver, PartnerRegistrationResponseMessage.Identifier,
        _messageHandlerFactory.GetRegistrationMessageHandler());

      RegisterSubscriber(clientMessageReceiver, BackendDbOperationResponseMessage.Identifier,
        _messageHandlerFactory.GetCommandResponseMessageHandler());

      RegisterSubscriber(clientMessageReceiver, ClientLogoResponseMessage.Identifier,
        _messageHandlerFactory.GetClientLogoResponseMessageHandler());

      RegisterSubscriber(clientMessageReceiver, SystemVersionCompatibilityCheckResponseMessage.Identifier,
        _messageHandlerFactory.GetSystemVersionCheckResponseMessageHandler());

      RegisterSubscriber(clientMessageReceiver, ResultMessage.Identifier,
        _messageHandlerFactory.GetResultMessageHandler());

      RegisterSubscriber(clientMessageReceiver, NotificationBase.Identifier,
        _messageHandlerFactory.GetNotificationMessageHandler());

      RegisterSubscriber(clientMessageReceiver, LoginRequiredMessage.Identifier,
        _messageHandlerFactory.GetLoginRequiredMessageHandler());

      RegisterSubscriber(clientMessageReceiver, ServerImageResponse.Identifier,
        _messageHandlerFactory.GetServerImageResponseMessageHandler());

      RegisterSubscriber(clientMessageReceiver, ServerImageThumbnailResponse.Identifier,
        _messageHandlerFactory.GetServerImageThumbnailResponseMessageHandler());
    }
  }
}
