﻿using System;
using System.Linq;
using Kalisto.Backend.DataAccess.Effort;
using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.DataAccess.Model.Logging;
using Kalisto.Backend.Operations.Drivers.Commands;
using Kalisto.Backend.Operations.Drivers.Exceptions;
using Kalisto.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Kalisto.Backend.Operations.Drivers.UnitTests
{
  [TestClass]
  public class CreateDriverCommandHandlerTest
  {
    private IDbContext _dbContext;

    private readonly string _partnerId = "unknown";

    private Mock<IDbInsertEventLogger> _dbEventLoggerMock;

    // component under tests
    private CreateDriverCommandHandler _createDriverCommandHandler;

    [TestInitialize]
    public void Initialize()
    {
      var effortConnection = Effort.DbConnectionFactory.CreateTransient();
      _dbContext = new EffortDbContext(effortConnection);

      _dbEventLoggerMock = new Mock<IDbInsertEventLogger>();

      var logger = new Mock<ILogger>();

      _createDriverCommandHandler = new CreateDriverCommandHandler(_dbEventLoggerMock.Object, logger.Object);
    }

    [TestCleanup]
    public void CleanUp()
    {
      _dbContext.Dispose();
    }

    [TestMethod]
    [ExpectedException(typeof(DriverExistsException))]
    public void Execute_ShouldThrowDriverExistsException_WhenDriverWithGivenIdentifierAlreadyExists()
    {
      // Setup
      var testDriverIdentifier = "test";
      _dbContext.Drivers.Add(new Driver { Identifier = testDriverIdentifier });
      _dbContext.SaveChanges();

      // Arrange
      var createDriverCommand = new CreateDriverCommand
      {
        DriverIdentifier = testDriverIdentifier
      };

      // Act & Assert
      _createDriverCommandHandler.Execute(_dbContext, createDriverCommand, _partnerId);
    }

    [TestMethod]
    public void Execute_ShouldNotCreateDriver_WhenDriverWithGivenIdentifierAlreadyExists()
    {
      // Setup
      var testDriverIdentifier = "test";
      _dbContext.Drivers.Add(new Driver { Identifier = testDriverIdentifier });
      _dbContext.SaveChanges();
      var expectedDriversCount = _dbContext.Drivers.Count();

      // Arrange
      var createDriverCommand = new CreateDriverCommand
      {
        DriverIdentifier = testDriverIdentifier
      };

      try
      {
        // Act
        _createDriverCommandHandler.Execute(_dbContext, createDriverCommand, _partnerId);
      }
      catch (DriverExistsException)
      {
        // Assert
        Assert.AreEqual(expectedDriversCount, _dbContext.Drivers.Count());
      }
    }

    [TestMethod]
    [ExpectedException(typeof(DriverExistsException))]
    public void Execute_ShouldThrowDriverExistsException_WhenDriverWithGivenIdentifierWithDifferentCapitalizationAlreadyExists()
    {
      // Setup
      var testDriverIdentifier = "tEsT";
      _dbContext.Drivers.Add(new Driver { Identifier = testDriverIdentifier });
      _dbContext.SaveChanges();

      // Arrange
      var createDriverCommand = new CreateDriverCommand
      {
        DriverIdentifier = testDriverIdentifier
      };

      // Act & Assert
      _createDriverCommandHandler.Execute(_dbContext, createDriverCommand, _partnerId);
    }

    [TestMethod]
    public void Execute_ShouldNotCreateDrivre_WhenDriverWithGivenIdentifierWithDifferentCapitalizationAlreadyExists()
    {
      // Setup
      var testDriverIdentifier = "tEsT";
      _dbContext.Drivers.Add(new Driver { Identifier = testDriverIdentifier });
      _dbContext.SaveChanges();
      var expectedDriversCount = _dbContext.Drivers.Count();

      // Arrange
      var createDriverCommand = new CreateDriverCommand
      {
        DriverIdentifier = testDriverIdentifier
      };

      try
      {
        // Act
        _createDriverCommandHandler.Execute(_dbContext, createDriverCommand, _partnerId);
      }
      catch (DriverExistsException)
      {
        // Assert
        Assert.AreEqual(expectedDriversCount, _dbContext.Drivers.Count());
      }
    }

    [TestMethod]
    [ExpectedException(typeof(DriverExistsException))]
    public void Execute_ShouldThrowDriverExistsException_WhenDriverWithGivenIdentifierWithoutLeadingAndTrailingWhitespacesAlreadyExists()
    {
      // Setup
      var testDriverIdentifier = "  test     ";
      _dbContext.Drivers.Add(new Driver { Identifier = testDriverIdentifier });
      _dbContext.SaveChanges();

      // Arrange
      var createDriverCommand = new CreateDriverCommand
      {
        DriverIdentifier = testDriverIdentifier
      };

      // Act & Assert
      _createDriverCommandHandler.Execute(_dbContext, createDriverCommand, _partnerId);
    }

    [TestMethod]
    public void Execute_ShouldNotCreateDriver_WhenDriverWithGivenIdentifierWithoutLeadingAndTrailingWhitespacesAlreadyExists()
    {
      // Setup
      var testDriverIdentifier = "  test     ";
      _dbContext.Drivers.Add(new Driver { Identifier = testDriverIdentifier });
      _dbContext.SaveChanges();
      var expectedDriversCount = _dbContext.Drivers.Count();

      // Arrange
      var createDriverCommand = new CreateDriverCommand
      {
        DriverIdentifier = testDriverIdentifier
      };

      try
      {
        // Act
        _createDriverCommandHandler.Execute(_dbContext, createDriverCommand, _partnerId);
      }
      catch (DriverExistsException)
      {
        // Assert
        Assert.AreEqual(expectedDriversCount, _dbContext.Drivers.Count());
      }
    }

    [TestMethod]
    public void Execute_ShouldCreateDriver_WithoutPhoneNumber()
    {
      // Arrange
      var createDriverCommand = new CreateDriverCommand
      {
        DriverIdentifier = "test"
      };

      // Act
      _createDriverCommandHandler.Execute(_dbContext, createDriverCommand, _partnerId);

      // Assert
      Assert.AreEqual(1, _dbContext.Drivers.Count(), "Drivers table should contain exactly one driver");

      var createdDriver = _dbContext.Drivers.Single();
      Assert.AreEqual(1, createdDriver.Id, "Id of created driver should be auto generated");
      Assert.AreEqual(createDriverCommand.DriverIdentifier, createdDriver.Identifier, "Name of created driver should be the same like in a command");
      Assert.IsNull(createDriverCommand.DriverPhoneNumber, "PhoneNumber of the created driver should be null");
    }

    [TestMethod]
    public void Execute_ShouldCreateDriver_WithPhoneNumber()
    {
      // Arrange
      var createDriverCommand = new CreateDriverCommand
      {
        DriverIdentifier = "test",
        DriverPhoneNumber = "124543555"
      };

      // Act
      _createDriverCommandHandler.Execute(_dbContext, createDriverCommand, _partnerId);

      // Assert
      Assert.AreEqual(1, _dbContext.Drivers.Count(), "Drivers table should contain exactly one driver");

      var createdDriver = _dbContext.Drivers.Single();
      Assert.AreEqual(1, createdDriver.Id, "Id of created driver should be auto generated");
      Assert.AreEqual(createDriverCommand.DriverIdentifier, createdDriver.Identifier, "Name of created driver should be the same like in a command");
      Assert.AreEqual(createDriverCommand.DriverPhoneNumber, createdDriver.PhoneNumber, "PhoneNumber of created driver should be the same like in a command");
    }

    [TestMethod]
    public void Execute_ShouldReturnIdentifierOfNewlyCreatedUser()
    {
      // Arrange
      var createDriverCommand = new CreateDriverCommand
      {
        DriverIdentifier = "test",
        DriverPhoneNumber = "124543555"
      };

      // Act
      var id = _createDriverCommandHandler.Execute(_dbContext, createDriverCommand, _partnerId);

      // Assert
      var createdDriver = _dbContext.Drivers.Single();
      Assert.AreEqual(1L, id);
      Assert.AreEqual(1L, createdDriver.Id, "Id of created driver should be auto generated and returned properly by Execute(...) method");
    }

    [TestMethod]
    public void Execute_ShouldCreateDbInsertEventLog()
    {
      // Setup
      _dbEventLoggerMock.Setup(logger => logger.LogEvent(_partnerId, ctx => ctx.Drivers, 
          It.IsAny<long>(), It.IsAny<Driver>()))
        .Verifiable();

      // Arrange
      var createDriverCommand = new CreateDriverCommand
      {
        DriverIdentifier = "test",
        DriverPhoneNumber = "124543555"
      };

      // Act
      _createDriverCommandHandler.Execute(_dbContext, createDriverCommand, _partnerId);

      // Assert
      _dbEventLoggerMock.Verify(logger => logger.LogEvent(_partnerId, ctx => ctx.Drivers,
          It.IsAny<long>(), It.IsAny<Driver>()), Times.Once);
    }
  }
}
