﻿using Kalisto.Backend.DataAccess.Effort;
using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.DataAccess.Model.Logging;
using Kalisto.Backend.Operations.Drivers.Commands;
using Kalisto.Backend.Operations.Drivers.Exceptions;
using Kalisto.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Linq;

namespace Kalisto.Backend.Operations.Drivers.UnitTests
{
  [TestClass]
  public class UpdateDriverCommandHandlerTests
  {
    private IDbContext _dbContext;

    private readonly string _partnerId = "unknown";

    private Mock<IDbUpdateEventLogger> _dbEventLoggerMock;

    // component under tests
    private UpdateDriverCommandHandler _updateDriverCommandHandler;

    [TestInitialize]
    public void Initialize()
    {
      var effortConnection = Effort.DbConnectionFactory.CreateTransient();
      _dbContext = new EffortDbContext(effortConnection);

      _dbEventLoggerMock = new Mock<IDbUpdateEventLogger>();

      var logger = new Mock<ILogger>();

      _updateDriverCommandHandler = new UpdateDriverCommandHandler(_dbEventLoggerMock.Object, logger.Object);
    }

    [TestCleanup]
    public void CleanUp()
    {
      _dbContext.Dispose();
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void Execute_ShouldThrowArgumentException_WhenDriverWithGivenIdDoesNotExist()
    {
      // Arrange
      var notExisitingDriverId = 99;
      var updateDriverCommand = new UpdateDriverCommand
      {
        DriverId = notExisitingDriverId
      };

      // Act & Assert
      _updateDriverCommandHandler.Execute(_dbContext, updateDriverCommand, _partnerId);
    }

    [TestMethod]
    [ExpectedException(typeof(DriverExistsException))]
    public void Execute_ShouldThrowUserExistsException_WhenIdentifierOfCurrentDriverHasChangedButDriverWithSameIdentifierAlreadyExists()
    {
      // Setup
      var testDriverIdentifier = "test";
      _dbContext.Drivers.Add(new Driver { Identifier = testDriverIdentifier }); // generated id: 1
      var currentDriverIdentifier= "current";
      _dbContext.Drivers.Add(new Driver { Identifier = currentDriverIdentifier }); // generated id: 2
      _dbContext.SaveChanges();

      // Arrange
      var updateDriverCommand = new UpdateDriverCommand { DriverId = 2, DriverIdentifier = testDriverIdentifier };

      // Act & Assert
      _updateDriverCommandHandler.Execute(_dbContext, updateDriverCommand, _partnerId);
    }

    [TestMethod]
    public void Execute_ShouldNotUpdateIdentifier_WhenIdentifierOfCurrentDriverHasChangedButDriverWithSameIdentifierAlreadyExists()
    {
      // Setup
      var testDriverIdentifier = "test";
      _dbContext.Drivers.Add(new Driver { Identifier = testDriverIdentifier }); // generated id: 1
      var currentDriverIdentifier = "current";
      _dbContext.Drivers.Add(new Driver { Identifier = currentDriverIdentifier }); // generated id: 2
      _dbContext.SaveChanges();

      // Arrange
      var updateDriverCommand = new UpdateDriverCommand { DriverId = 2, DriverIdentifier = testDriverIdentifier };

      try
      {
        // Act
        _updateDriverCommandHandler.Execute(_dbContext, updateDriverCommand, _partnerId);
      }
      catch (DriverExistsException)
      {
        // Assert
        Assert.AreEqual(currentDriverIdentifier, _dbContext.Drivers.FirstOrDefault(d => d.Id == 2).Identifier);
      }
    }

    [TestMethod]
    [ExpectedException(typeof(DriverExistsException))]
    public void Execute_ShouldThrowUserExistsException_WhenIdentifierOfCurrentDriverHasChangedButDriverWithSameIdentifierWithDifferentCapitalizationAlreadyExists()
    {
      // Setup
      var testDriverIdentifier = "test";
      _dbContext.Drivers.Add(new Driver { Identifier = testDriverIdentifier }); // generated id: 1
      var currentDriverIdentifier = "current";
      _dbContext.Drivers.Add(new Driver { Identifier = currentDriverIdentifier }); // generated id: 2
      _dbContext.SaveChanges();

      var testDriverIdentifierWithDifferentCapitalization = "TeSt";

      // Arrange
      var updateDriverCommand = new UpdateDriverCommand { DriverId = 2, DriverIdentifier = testDriverIdentifierWithDifferentCapitalization };

      // Act & Assert
      _updateDriverCommandHandler.Execute(_dbContext, updateDriverCommand, _partnerId);
    }

    [TestMethod]
    public void Execute_ShouldNotUpdateIdentifier_WhenIdentifierOfCurrentDriverHasChangedButDriverWithSameIdentifierWithDifferentCapitalizationAlreadyExists()
    {
      // Setup
      var testDriverIdentifier = "test";
      _dbContext.Drivers.Add(new Driver { Identifier = testDriverIdentifier }); // generated id: 1
      var currentDriverIdentifier = "current";
      _dbContext.Drivers.Add(new Driver { Identifier = currentDriverIdentifier }); // generated id: 2
      _dbContext.SaveChanges();

      var testDriverIdentifierWithDifferentCapitalization = "TeSt";

      // Arrange
      var updateDriverCommand = new UpdateDriverCommand { DriverId = 2, DriverIdentifier = testDriverIdentifierWithDifferentCapitalization };

      try
      {
        // Act
        _updateDriverCommandHandler.Execute(_dbContext, updateDriverCommand, _partnerId);
      }
      catch (DriverExistsException)
      {
        // Assert
        Assert.AreEqual(currentDriverIdentifier, _dbContext.Drivers.FirstOrDefault(d => d.Id == 2).Identifier);
      }
    }

    [TestMethod]
    [ExpectedException(typeof(DriverExistsException))]
    public void Execute_ShouldThrowUserExistsException_WhenIdentifierOfCurrentDriverHasChangedButDriverWithSameIdentifierWithoutLeadingAndTrailingWhitespacesAlreadyExists()
    {
      // Setup
      var testDriverIdentifier = "test";
      _dbContext.Drivers.Add(new Driver { Identifier = testDriverIdentifier }); // generated id: 1
      var currentDriverIdentifier = "current";
      _dbContext.Drivers.Add(new Driver { Identifier = currentDriverIdentifier }); // generated id: 2
      _dbContext.SaveChanges();

      var testDriverIdentifierWithLeadingAndTrailingWhitespaces = "   test     ";

      // Arrange
      var updateDriverCommand = new UpdateDriverCommand { DriverId = 2, DriverIdentifier = testDriverIdentifierWithLeadingAndTrailingWhitespaces };

      // Act & Assert
      _updateDriverCommandHandler.Execute(_dbContext, updateDriverCommand, _partnerId);
    }

    [TestMethod]
    public void Execute_ShouldNotUpdateIdentifier_WhenIdentifierOfCurrentDriverHasChangedButDriverWithSameIdentifierWithoutLeadingAndTrailingWhitespacesAlreadyExists()
    {
      // Setup
      var testDriverIdentifier = "test";
      _dbContext.Drivers.Add(new Driver { Identifier = testDriverIdentifier }); // generated id: 1
      var currentDriverIdentifier = "current";
      _dbContext.Drivers.Add(new Driver { Identifier = currentDriverIdentifier }); // generated id: 2
      _dbContext.SaveChanges();

      var testDriverIdentifierWithLeadingAndTrailingWhitespaces = "   test     ";

      // Arrange
      var updateDriverCommand = new UpdateDriverCommand { DriverId = 2, DriverIdentifier = testDriverIdentifierWithLeadingAndTrailingWhitespaces };

      try
      {
        // Act
        _updateDriverCommandHandler.Execute(_dbContext, updateDriverCommand, _partnerId);
      }
      catch (DriverExistsException)
      {
        // Assert
        Assert.AreEqual(currentDriverIdentifier, _dbContext.Drivers.FirstOrDefault(d => d.Id == 2).Identifier);
      }
    }

    [TestMethod]
    public void Execute_ShouldUpdateDriver()
    {
      // Setup
      var currentDriverIdentifier = "current";
      var currentDriverPhoneNumber = "1231234556";
      var currentDriver = _dbContext.Drivers.Add(new Driver
      {
        Identifier = currentDriverIdentifier,
        PhoneNumber = currentDriverPhoneNumber
      });
      _dbContext.SaveChanges();

      // Arrange
      var newDriverIdentifier = "new";
      var newDriverPhoneNumber = "213";
      var updateDriverCommand = new UpdateDriverCommand
      {
        DriverId = currentDriver.Id,
        DriverIdentifier = newDriverIdentifier,
        DriverPhoneNumber = newDriverPhoneNumber
      };

      // Act
      _updateDriverCommandHandler.Execute(_dbContext, updateDriverCommand, _partnerId);

      // Assert
      var newDriver = _dbContext.Drivers.Single();
      Assert.AreEqual(newDriverIdentifier, newDriver.Identifier);
      Assert.AreEqual(newDriverPhoneNumber, newDriver.PhoneNumber);
    }

    [TestMethod]
    public void Execute_ShouldReturnUpdateDriverId()
    {
      // Setup
      var currentDriverIdentifier = "current";
      var currentDriverPhoneNumber = "1231234556";
      var currentDriver = _dbContext.Drivers.Add(new Driver
      {
        Identifier = currentDriverIdentifier,
        PhoneNumber = currentDriverPhoneNumber
      });
      _dbContext.SaveChanges();

      // Arrange
      var newDriverIdentifier = "new";
      var newDriverPhoneNumber = "213";
      var updateDriverCommand = new UpdateDriverCommand
      {
        DriverId = currentDriver.Id,
        DriverIdentifier = newDriverIdentifier,
        DriverPhoneNumber = newDriverPhoneNumber
      };

      // Act
      var resultId = _updateDriverCommandHandler.Execute(_dbContext, updateDriverCommand, _partnerId);

      // Assert
      var newDriver = _dbContext.Drivers.Single();
      Assert.AreEqual(1L, newDriver.Id);
      Assert.AreEqual(1L, resultId);
    }

    [TestMethod]
    public void Execute_ShouldCreateDbUpdateEventLog()
    {
      // Setup
      _dbEventLoggerMock.Setup(c => c.LogEvent(_partnerId, ctx => ctx.Drivers,
          It.IsAny<long>(), It.IsAny<Driver>()))
        .Verifiable();

      var currentDriverIdentifier = "current";
      var currentDriverPhoneNumber = "1231234556";
      var currentDriver = _dbContext.Drivers.Add(new Driver
      {
        Identifier = currentDriverIdentifier,
        PhoneNumber = currentDriverPhoneNumber
      });
      _dbContext.SaveChanges();

      // Arrange
      var newDriverIdentifier = "new";
      var newDriverPhoneNumber = "213";
      var updateDriverCommand = new UpdateDriverCommand
      {
        DriverId = currentDriver.Id,
        DriverIdentifier = newDriverIdentifier,
        DriverPhoneNumber = newDriverPhoneNumber
      };

      // Act
      _updateDriverCommandHandler.Execute(_dbContext, updateDriverCommand, _partnerId);

      // Assert
      _dbEventLoggerMock.Verify(c => c.LogEvent(_partnerId, ctx => ctx.Drivers,
        It.IsAny<long>(), It.IsAny<Driver>(), It.IsAny<Driver>()), Times.Once);
    }
  }
}
