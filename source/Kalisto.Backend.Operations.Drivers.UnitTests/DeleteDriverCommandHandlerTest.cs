﻿using Kalisto.Backend.DataAccess.Effort;
using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.Operations.Drivers.Commands;
using Kalisto.Backend.DataAccess.Model.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Linq;
using Kalisto.Logging;

namespace Kalisto.Backend.Operations.Drivers.UnitTests
{
  [TestClass]
  public class DeleteDriverCommandHandlerTest
  {
    private IDbContext _dbContext;

    private readonly string _partnerId = "unknown";

    private Mock<IDbDeleteEventLogger> _dbEventLoggerMock;

    // component under tests
    private DeleteDriverCommandHandler _deleteDriverCommandHandler;

    [TestInitialize]
    public void Initialize()
    {
      var effortConnection = Effort.DbConnectionFactory.CreateTransient();
      _dbContext = new EffortDbContext(effortConnection);

      _dbEventLoggerMock = new Mock<IDbDeleteEventLogger>();

      var logger = new Mock<ILogger>();

      _deleteDriverCommandHandler = new DeleteDriverCommandHandler(_dbEventLoggerMock.Object, logger.Object);
    }

    [TestCleanup]
    public void CleanUp()
    {
      _dbContext.Dispose();
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void Execute_ShouldThrowArgumentException_WhenDriverWithGivenIdDoesNotExist()
    {
      // Setup
      var testDriverIdentifier = "test";
      _dbContext.Drivers.Add(new Driver { Identifier = testDriverIdentifier }); // Returns generated id: 1
      _dbContext.SaveChanges();

      // Arrange
      var deleteDriverCommand = new DeleteDriverCommand
      {
        DriverId = 2 // Not exists
      };

      // Act & Assert
      _deleteDriverCommandHandler.Execute(_dbContext, deleteDriverCommand, _partnerId);
    }

    [TestMethod]
    public void Execute_ShouldRemoveDriver()
    {
      // Setup
      var testDriverIdentifier = "test";
      var driver = _dbContext.Drivers.Add(new Driver { Identifier = testDriverIdentifier });
      _dbContext.SaveChanges();

      // Arrange
      var deleteDriverCommand = new DeleteDriverCommand
      {
        DriverId = driver.Id
      };

      // Act
      _deleteDriverCommandHandler.Execute(_dbContext, deleteDriverCommand, _partnerId);

      // Assert
      Assert.AreEqual(0, _dbContext.Drivers.Count());
    }

    [TestMethod]
    public void Execute_ShouldReturnIdOfDeletedDriver()
    {
      // Setup
      var testDriverIdentifier = "test";
      var driver = _dbContext.Drivers.Add(new Driver { Identifier = testDriverIdentifier });
      _dbContext.SaveChanges();

      // Arrange
      var deleteDriverCommand = new DeleteDriverCommand
      {
        DriverId = driver.Id
      };

      // Act
      var driverId = _deleteDriverCommandHandler.Execute(_dbContext, deleteDriverCommand, _partnerId);

      // Assert
      Assert.AreEqual(deleteDriverCommand.DriverId, driverId);
    }

    [TestMethod]
    public void Execute_ShouldCreateDbDeleteEventLog()
    {
      // Setup
      _dbEventLoggerMock.Setup(logger => logger.LogEvent(_partnerId, ctx => ctx.Drivers,
          It.IsAny<long>(), null, It.IsAny<Driver>()))
        .Verifiable();

      var testDriverIdentifier = "test";
      var driver = _dbContext.Drivers.Add(new Driver { Identifier = testDriverIdentifier });
      _dbContext.SaveChanges();

      // Arrange
      var deleteDriverCommand = new DeleteDriverCommand
      {
        DriverId = driver.Id
      };

      // Act
      _deleteDriverCommandHandler.Execute(_dbContext, deleteDriverCommand, _partnerId);

      // Assert
      _dbEventLoggerMock.Verify(logger => logger.LogEvent(_partnerId, ctx => ctx.Drivers,
          It.IsAny<long>(), null, It.IsAny<Driver>()), Times.Once);
    }
  }
}
