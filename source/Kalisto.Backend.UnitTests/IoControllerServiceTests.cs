﻿using Kalisto.Backend.Services;
using Kalisto.Communication.Core.Logging;
using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Devices.IOController.Messages.Incoming;
using Kalisto.Devices.IOController.Messages.Outgoing;
using Kalisto.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Threading.Tasks;

namespace Kalisto.Backend.UnitTests
{
  [TestClass]
  public class IoControllerServiceTests
  {
    private Mock<ILogger> _loggerMock;

    private Mock<ISystemEventLogger> _eventLoggerMock;

    private Mock<IServerMessageProvider> _messageProviderMock;

    private IMessageBuilder _messageBuilder;

    // component under tests
    private IoControllerService _ioControllerService;

    [TestInitialize]
    public void Initialize()
    {
      _loggerMock = new Mock<ILogger>();
      _eventLoggerMock = new Mock<ISystemEventLogger>();
      _messageProviderMock = new Mock<IServerMessageProvider>();
      _messageProviderMock.Setup(c => c.SendMessage(It.IsAny<EndpointMessageBase>())).Verifiable();
      _messageBuilder = new MessageBuilder();

      _ioControllerService = new IoControllerService(_loggerMock.Object, _eventLoggerMock.Object, _messageProviderMock.Object, _messageBuilder);
    }

    [TestMethod]
    public void SendCommand_ShouldEnqueueCommand()
    {
      // Arrange
      int deviceId = 1;

      // Act
      _ioControllerService.SendGetWeightCommand(deviceId);

      // Assert
      Assert.AreEqual(1, _ioControllerService.CurrentMessageLoad);
    }

    [TestMethod]
    public void IoControllerService_ShouldDequeueMessageAutomatically_WhenCommandResponseTimeoutOccurred_V1()
    {
      // Arrange
      int deviceId = 1;

      // Act
      _ioControllerService.SendGetWeightCommand(deviceId);
      Task.Delay(IoControllerService.CommandResponseTimeout + 1000).Wait();

      // Assert
      Assert.AreEqual(0, _ioControllerService.CurrentMessageLoad);
    }

    [TestMethod]
    public void IoControllerService_ShouldDequeueMessageAutomatically_WhenCommandResponseTimeoutOccurred_V2()
    {
      // Setup
      var messagesToLoose = 2;

      // Arrange
      int deviceId = 1;

      // Act
      _ioControllerService.SendSetOutputCommand(deviceId, 1);
      _ioControllerService.SendGetWeightCommand(deviceId);
      _ioControllerService.SendSetOutputCommand(deviceId, 1);
      _ioControllerService.SendGetWeightCommand(deviceId);
      Task.Delay(IoControllerService.CommandResponseTimeout * messagesToLoose + 100).Wait();

      // Assert
      Assert.AreEqual(1, _ioControllerService.CurrentMessageLoad);
      _messageProviderMock.Verify(c => c.SendMessage(It.IsAny<EndpointMessageBase>()), Times.Exactly(messagesToLoose + 1));
    }

    [TestMethod]
    public void IoControllerService_ShouldEnqueueOnlyOneMessageOfSpecificTypeInRow()
    {
      // Arrange
      int deviceId = 1;

      // Act
      _ioControllerService.SendSetOutputCommand(deviceId, 1);
      _ioControllerService.SendSetOutputCommand(deviceId, 1);
      _ioControllerService.SendSetOutputCommand(deviceId, 1);
      _ioControllerService.SendSetOutputCommand(deviceId, 1);
      _ioControllerService.SendSetOutputCommand(deviceId, 1);

      // Assert
      Assert.AreEqual(1, _ioControllerService.CurrentMessageLoad);
      _messageProviderMock.Verify(c => c.SendMessage(It.IsAny<EndpointMessageBase>()), Times.Exactly(1));
    }

    [TestMethod]
    public void IoControllerService_ShouldDequeueConfirmedCommand()
    {
      // Arrange
      int deviceId = 1;

      // Act
      _ioControllerService.SendSetOutputCommand(deviceId, 1);
      _ioControllerService.ConfirmCommandResponse<SetOutputMessage>();

      // Assert
      Assert.AreEqual(0, _ioControllerService.CurrentMessageLoad);
      _messageProviderMock.Verify(c => c.SendMessage(It.IsAny<EndpointMessageBase>()), Times.Exactly(1));
    }

    [TestMethod]
    public void IoControllerService_ShouldWorkProperlyInRow()
    {
      // Arrange
      int deviceId = 1;
      var commandResponse = new WeightMeterResponseMessage();

      // Act
      _ioControllerService.SendGetWeightCommand(deviceId);
      _ioControllerService.ConfirmCommandResponse<GetWeightMeterValueMessage>();
      _ioControllerService.SendGetWeightCommand(deviceId);
      _ioControllerService.ConfirmCommandResponse<GetWeightMeterValueMessage>();
      _ioControllerService.SendGetWeightCommand(deviceId);
      _ioControllerService.ConfirmCommandResponse<GetWeightMeterValueMessage>();

      // Assert
      Assert.AreEqual(0, _ioControllerService.CurrentMessageLoad);
      _messageProviderMock.Verify(c => c.SendMessage(It.IsAny<EndpointMessageBase>()), Times.Exactly(3));
    }

    [TestMethod]
    public void IoControllerService_ShouldWorkProperlyInRow_WhenReceivedWrongResponse()
    {
      // Setup
      var messagesToLoose = 1;

      // Arrange
      int deviceId = 1;

      // Act
      _ioControllerService.SendGetWeightCommand(deviceId);
      _ioControllerService.ConfirmCommandResponse<GetWeightMeterValueMessage>();
      Task.Delay(IoControllerService.CommandResponseTimeout * messagesToLoose + 1000).Wait();

      _ioControllerService.SendResetOutputCommand(deviceId, 1);
      _ioControllerService.SendGetWeightCommand(deviceId);
      _ioControllerService.ConfirmCommandResponse<ResetOutputMessage>();
      _ioControllerService.ConfirmCommandResponse<GetWeightMeterValueMessage>();

      // Assert
      Assert.AreEqual(0, _ioControllerService.CurrentMessageLoad);
      _messageProviderMock.Verify(c => c.SendMessage(It.IsAny<EndpointMessageBase>()), Times.Exactly(3));
    }
  }
}
