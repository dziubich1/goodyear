﻿using Kalisto.Backend.Configuration;
using Kalisto.Backend.DataAccess.Effort;
using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.Logging;
using Kalisto.Communication.Core.Registries;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;

namespace Kalisto.Backend.UnitTests
{
  [TestClass]
  public class SystemEventLoggerTests
  {
    private IDbContext _dbContext;

    private readonly string _partnerId = "anything";

    private Mock<IPartnerRegistryReader> _partnerRegistryReaderMock;

    // component under tests
    private SystemEventLogger _systemEventLogger;

    [TestInitialize]
    public void Initialize()
    {
      var effortConnection = Effort.DbConnectionFactory.CreateTransient();
      _dbContext = new EffortDbContext(effortConnection);

      InitializeUserRoles();
      InitializeUsers();

      var backendConfiguration = new BackendConfiguration();
      backendConfiguration.DbConnection = new DbConnectionConfiguration();
      backendConfiguration.DbConnection.ConnectionString = "anything";

      var dbContextFactory = new Mock<IDbContextFactory>();
      dbContextFactory
        .Setup(x => x.CreateDbContext(It.IsAny<string>()))
        .Returns(_dbContext);

      _partnerRegistryReaderMock = new Mock<IPartnerRegistryReader>();

      _systemEventLogger = new SystemEventLogger(backendConfiguration, 
        dbContextFactory.Object, _partnerRegistryReaderMock.Object);
    }

    [TestCleanup]
    public void CleanUp()
    {
      _dbContext.Dispose();
    }

    [TestMethod]
    public void LogEvent_ShouldAddCorrectLogEntityWithValidResourceKey()
    {
      // Arrange
      var resourceKey = "LOGIN";

      // Act
      _systemEventLogger.LogEvent(resourceKey, new string[] { });

      // Assert
      var log = _dbContext.SystemLogs.FirstOrDefault();
      Assert.IsNotNull(log);
      Assert.AreEqual(resourceKey, log.LogResourceKey);
    }

    [TestMethod]
    public void LogEvent_ShouldAddCorrectLogEntityWithSystemAsSource_WhenPartnerIdIsNotSpecified()
    {
      // Act
      _systemEventLogger.LogEvent("LOGIN", new string[] { "a" });

      // Assert
      var log = _dbContext.SystemLogs.FirstOrDefault();
      Assert.IsNotNull(log);
      Assert.AreEqual("SYSTEM", log.Source);
    }

    [TestMethod]
    public void LogEvent_ShouldAddCorrectLogEntityWithCorrectSource_WhenPartnerIdIsSpecified()
    {
      // Setup
      var user = _dbContext.Users.First();
      _partnerRegistryReaderMock
        .Setup(x => x.GetCurrentUser(_partnerId))
        .Returns(user);

      // Act
      _systemEventLogger.LogEvent(_partnerId, "LOGIN", new string[] { "a" });

      // Assert
      var log = _dbContext.SystemLogs.FirstOrDefault();
      Assert.IsNotNull(log);
      Assert.AreEqual(user.Name, log.Source);
    }

    [TestMethod]
    public void LogEvent_ShouldAddCorrectLogEntityWithEmptyParameters_WhenParametersAreNull()
    {
      // Arrange
      string[] parameters = null;

      // Act
      _systemEventLogger.LogEvent("LOGIN", parameters);

      // Assert
      var log = _dbContext.SystemLogs.FirstOrDefault();
      Assert.IsNotNull(log);
      Assert.AreEqual(string.Empty, log.Parameters);
    }

    [TestMethod]
    public void LogEvent_ShouldAddCorrectLogEntityWithCommaSeparatedParameters_WhenThereAreMoreThenOneParameters()
    {
      // Act
      _systemEventLogger.LogEvent("LOGIN", new string[] { "a", "b" });

      // Assert
      var log = _dbContext.SystemLogs.FirstOrDefault();
      Assert.IsNotNull(log);
      Assert.AreEqual("a,b", log.Parameters);
    }

    [TestMethod]
    public void LogEvent_ShouldAddCorrectLogEntityWithOneParameter_WhenThereIsOnlyOneParameter()
    {
      // Act
      _systemEventLogger.LogEvent("LOGIN", new string[] { "a" });

      // Assert
      var log = _dbContext.SystemLogs.FirstOrDefault();
      Assert.IsNotNull(log);
      Assert.AreEqual("a", log.Parameters);
    }

    private void InitializeUserRoles()
    {
      _dbContext.UserRoles.Add(new UserRole { Type = 0 });
      _dbContext.SaveChanges();
    }

    private void InitializeUsers()
    {
      var user = new User
      {
        Name = "test",
        PasswordHash = "test",
        Role = _dbContext.UserRoles.FirstOrDefault()
      };
      _dbContext.Users.Add(user);
      _dbContext.SaveChanges();
    }
  }
}
