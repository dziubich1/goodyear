﻿using System.Linq;
using Effort.Provider;
using Kalisto.Backend.Configuration;
using Kalisto.Backend.DataAccess.Effort;
using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.DataAccess.Model.Enum;
using Kalisto.Backend.Logging;
using Kalisto.Communication.Core.Registries;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json;

namespace Kalisto.Backend.UnitTests
{
  [TestClass]
  public class DbInsertEventLoggerTests
  {
    private EffortConnection _effortConnection;

    private IDbContext _dbContext;

    private readonly string _partnerId = "unknown";

    private readonly User _user = new User { Name = "username" };

    private BackendConfiguration _backendConfiguration;

    private Mock<IDbContextFactory> _dbContextFactoryMock;

    private Mock<IPartnerRegistryReader> _partnerRegistryReaderMock;

    // component under tests
    private DbInsertEventLogger _dbInsertEventLogger;

    [TestInitialize]
    public void Initialize()
    {
      _effortConnection = Effort.DbConnectionFactory.CreateTransient();
      _dbContext = new EffortDbContext(_effortConnection);

      _backendConfiguration = new BackendConfiguration
      {
        DbConnection = new DbConnectionConfiguration
        {
          ConnectionString = "whatever"
        }
      };

      _dbContextFactoryMock = new Mock<IDbContextFactory>();
      _dbContextFactoryMock.Setup(c => c.CreateDbContext(_backendConfiguration.DbConnection.ConnectionString))
        .Returns(new EffortDbContext(_effortConnection));

      _partnerRegistryReaderMock = new Mock<IPartnerRegistryReader>();
      _partnerRegistryReaderMock.Setup(c => c.GetCurrentUser(_partnerId)).Returns(_user);

      _dbInsertEventLogger = new DbInsertEventLogger(_backendConfiguration, _dbContextFactoryMock.Object,
        _partnerRegistryReaderMock.Object);
    }

    [TestMethod]
    public void LogEvent_ShouldCreateLogEntry()
    {
      // Arrange
      var insertedEntityId = 1;

      // Act
      _dbInsertEventLogger.LogEvent(_partnerId, ctx => ctx.Users, insertedEntityId, new User());

      // Assert
      var logEntry = _dbContext.DbEventLogs.FirstOrDefault();
      Assert.IsNotNull(logEntry);
    }

    [TestMethod]
    public void LogEvent_ShouldSetCorrectDbEventType()
    {
      // Arrange
      var insertedEntityId = 1;

      // Act
      _dbInsertEventLogger.LogEvent(_partnerId, ctx => ctx.Users, insertedEntityId, new User());

      // Assert
      var logEntry = _dbContext.DbEventLogs.FirstOrDefault();
      Assert.IsNotNull(logEntry);
      Assert.AreEqual(DbEventType.Insert, logEntry.EventType);
    }

    [TestMethod]
    public void LogEvent_ShouldSetCorrectSource_WhenUserForGivenPartnerIdWasFound()
    {
      // Arrange
      var insertedEntityId = 1;

      // Act
      _dbInsertEventLogger.LogEvent(_partnerId, ctx => ctx.Users, insertedEntityId, new User());

      // Assert
      var logEntry = _dbContext.DbEventLogs.FirstOrDefault();
      Assert.IsNotNull(logEntry);
      Assert.AreEqual(_user.Name, logEntry.Source);
    }

    [TestMethod]
    public void LogEvent_ShouldSetCorrectSource_WhenUserForGivenPartnerIdWasNotFound()
    {
      // Arrange
      var insertedEntityId = 1;
      var notExistingPartnerId = "not_existing_partner_id";

      // Act
      _dbInsertEventLogger.LogEvent(notExistingPartnerId, ctx => ctx.Users, insertedEntityId, new User());

      // Assert
      var logEntry = _dbContext.DbEventLogs.FirstOrDefault();
      Assert.IsNotNull(logEntry);
      Assert.AreEqual("UNKNOWN", logEntry.Source);
    }

    [TestMethod]
    public void LogEvent_ShouldSetCorrectTableName()
    {
      // Arrange
      var insertedEntityId = 1;

      // Act
      _dbInsertEventLogger.LogEvent(_partnerId, ctx => ctx.Users, insertedEntityId, new User());

      // Assert
      var logEntry = _dbContext.DbEventLogs.FirstOrDefault();
      Assert.IsNotNull(logEntry);
      Assert.AreEqual("Users", logEntry.TableName);
    }

    [TestMethod]
    public void LogEvent_ShouldSetCorrectEntityId()
    {
      // Arrange
      var insertedEntityId = 1;

      // Act
      _dbInsertEventLogger.LogEvent(_partnerId, ctx => ctx.Users, insertedEntityId, new User());

      // Assert
      var logEntry = _dbContext.DbEventLogs.FirstOrDefault();
      Assert.IsNotNull(logEntry);
      Assert.AreEqual(insertedEntityId, logEntry.EntityId);
    }

    [TestMethod]
    public void LogEvent_ShouldSetCorrectNewValue()
    {
      // Arrange
      var insertedEntityId = 1;
      var newValue = new User {Name = "test"};

      // Act
      _dbInsertEventLogger.LogEvent(_partnerId, ctx => ctx.Users, insertedEntityId, newValue);

      // Assert
      var logEntry = _dbContext.DbEventLogs.FirstOrDefault();
      Assert.IsNotNull(logEntry);
      Assert.IsNotNull(logEntry.NewValue);
      Assert.AreEqual(JsonConvert.SerializeObject(newValue), logEntry.NewValue);
    }

    [TestMethod]
    public void LogEvent_ShouldSetCorrectNewAndOldValue()
    {
      // Arrange
      var insertedEntityId = 1;
      var newValue = new User { Name = "new_test" };
      var oldValue = new User { Name = "old_test" };

      // Act
      _dbInsertEventLogger.LogEvent(_partnerId, ctx => ctx.Users, insertedEntityId, newValue, oldValue);

      // Assert
      var logEntry = _dbContext.DbEventLogs.FirstOrDefault();
      Assert.IsNotNull(logEntry);
      Assert.IsNotNull(logEntry.NewValue);
      Assert.AreEqual(JsonConvert.SerializeObject(newValue), logEntry.NewValue);
      Assert.IsNotNull(logEntry.OldValue);
      Assert.AreEqual(JsonConvert.SerializeObject(oldValue), logEntry.OldValue);
    }
  }
}
