﻿using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Hmi.Core;
using Kalisto.Hmi.Core.Communication;
using Kalisto.Hmi.Core.Services;
using PropertyChanged;
using System;

namespace Kalisto.Hmi.Dialogs.Cargoes.Dialogs
{
  [AddINotifyPropertyChangedInterface]
  public class AddOrUpdateCargoModalDialogViewModel : ModalViewModelBase<AddOrUpdateCargoModalDialogViewModel>
  {
    public override string DialogNameKey { get; set; }

    public string Name { get; set; }

    public string Price { get; set; }

    public decimal? PriceValue => decimal.TryParse(Price, out decimal tempPriceValue) ? tempPriceValue : null as decimal?;

    public AddOrUpdateCargoModalDialogViewModel(IDialogService dialogService, IAsyncProgressService asyncProgressService,
      IMessageBuilder messageBuilder, IFrontendMessageBridge messageBridge, ClientInfo clientInfo)
      : base(dialogService, asyncProgressService, messageBuilder, messageBridge, clientInfo)
    {
    }

    protected override void DefineValidationRules()
    {
      InvalidateProperty(() => Name).When(() => string.IsNullOrEmpty(Name))
        .WithMessageKey("Required").ButIgnoreOnLoad();

      InvalidateProperty(() => Name).When(() => Name.Length < 3)
        .WithMessageKey("TooShort").ButIgnoreOnLoad();

      InvalidateProperty(() => Price).When(() => !IsDecimalOrNull(Price))
        .WithMessageKey("InvalidNumber").ButIgnoreOnLoad();
    }
  }
}
