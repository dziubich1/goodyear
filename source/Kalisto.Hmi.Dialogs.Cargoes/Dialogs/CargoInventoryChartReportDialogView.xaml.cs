﻿using MahApps.Metro.Controls;

namespace Kalisto.Hmi.Dialogs.Cargoes.Dialogs
{
  /// <summary>
  /// Interaction logic for CargoInventoryChartReportDialogView.xaml
  /// </summary>
  public partial class CargoInventoryChartReportDialogView : MetroWindow
  {
    public CargoInventoryChartReportDialogView()
    {
      InitializeComponent();
    }
  }
}
