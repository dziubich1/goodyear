﻿using MahApps.Metro.Controls;

namespace Kalisto.Hmi.Dialogs.Cargoes.Dialogs
{
  /// <summary>
  /// Interaction logic for AddOrUpdateCargoModalDialogView.xaml
  /// </summary>
  public partial class AddOrUpdateCargoModalDialogView : MetroWindow
  {
    public AddOrUpdateCargoModalDialogView()
    {
      InitializeComponent();
    }
  }
}
