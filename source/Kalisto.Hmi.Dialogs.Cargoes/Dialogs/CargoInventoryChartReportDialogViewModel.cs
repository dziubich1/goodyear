﻿using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Hmi.Core;
using Kalisto.Hmi.Core.Commands;
using Kalisto.Hmi.Core.Communication;
using Kalisto.Hmi.Core.Services;
using Kalisto.Hmi.Dialogs.Cargoes.Models;
using Kalisto.Hmi.Dialogs.Cargoes.Services;
using Kalisto.Hmi.Dialogs.Cargoes.ViewModels;
using Kalisto.Localization;
using Kalisto.Localization.Messages;
using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;

namespace Kalisto.Hmi.Dialogs.Cargoes.Dialogs
{
  [AddINotifyPropertyChangedInterface]
  public class CargoInventoryChartReportDialogViewModel : ModalViewModelBase<CargoInventoryChartReportDialogViewModel>
  {
    private readonly ICargoService _cargoService;

    private readonly ILocalizer<Messages> _messagesLocalizer;

    private readonly ILocalizer<Localization.Common.Common> _commonLocalizer;

    public override string DialogNameKey { get; set; }

    public long CargoId { get; set; }

    public DateTime? DateFrom { get; set; }

    public DateTime? DateTo { get; set; }

    public ChartDataViewModel ChartData { get; set; }

    public ICommand CreateCargoInventoryChartReportCommand { get; set; }

    public string VerticalAxisTitle { get; set; }

    public bool IsDialogOkEnabled { get; set; }

    public CargoInventoryChartReportDialogViewModel(IDialogService dialogService, IAsyncProgressService asyncProgressService,
      IMessageBuilder messageBuilder, IFrontendMessageBridge messageBridge, ClientInfo clientInfo, ICargoService cargoService)
      : base(dialogService, asyncProgressService, messageBuilder, messageBridge, clientInfo)
    {
      _messagesLocalizer = new LocalizationManager<Messages>();
      _commonLocalizer = new LocalizationManager<Localization.Common.Common>();

      ChartData = new ChartDataViewModel();

      _cargoService = cargoService;
      CreateCargoInventoryChartReportCommand = new DelegateCommand(CreateCargoInventoryChartReportAsync);
      IsDialogOkEnabled = true;
    }

    public async void OnDateFromChanged()
    {
      if (!DateFrom.HasValue || !DateTo.HasValue)
      {
        return;
      }

      if (DateFrom.Value.Date <= DateTo.Value.Date)
      {
        return;
      }
      DateFrom = DateTo;

      var errorMessage = _messagesLocalizer.Localize(() => Messages.DateFromCannotBeGreaterThanDateTo);
      await DialogService.ShowMessageAsync(_messagesLocalizer.Localize(() => Messages.Error), errorMessage, MessageDialogType.Ok);
    }

    public async void OnDateToChanged()
    {
      if (!DateFrom.HasValue || !DateTo.HasValue)
      {
        return;
      }
      var errorOccurred = false;
      var now = DateTime.Now.Date;
      var errorMessage = string.Empty;
      if (DateTo.Value.Date > now)
      {
        errorOccurred = true;
        errorMessage = _messagesLocalizer.Localize(() => Messages.DateCannotBeFromFuture);
        DateTo = now;
      }
      else if (DateTo.Value.Date < DateFrom.Value.Date)
      {
        errorOccurred = true;
        errorMessage = _messagesLocalizer.Localize(() => Messages.DateToCannotBeLessThanDateFrom);
        DateTo = DateFrom;
      }

      if (!errorOccurred)
      {
        return;
      }
      await DialogService.ShowMessageAsync(_messagesLocalizer.Localize(() => Messages.Error), errorMessage, MessageDialogType.Ok);
    }

    protected override void DefineValidationRules()
    {
      InvalidateProperty(() => DateFrom).When(() => DateFrom == null)
        .WithMessageKey("Required").ButIgnoreOnLoad();

      InvalidateProperty(() => DateTo).When(() => DateTo == null)
        .WithMessageKey("Required").ButIgnoreOnLoad();
    }

    private async void CreateCargoInventoryChartReportAsync(object parameter)
    {
      ChartData.ClearChartData();

      var inventoryChartReportData = new List<CargoInventoryReportModel>();
      var result = await _cargoService.GetInventoryChartReportDataAsync(CargoId, DateFrom.Value, DateTo.Value);
      if (!result.Succeeded)
      {
        await DialogService.ShowMessageAsync(_messagesLocalizer.Localize(() => Messages.ErrorDuringDataDownload),
          _messagesLocalizer.Localize(() => Messages.NoDataFromDateRange), MessageDialogType.Ok);
        return;
      }
      inventoryChartReportData.AddRange(result.Response.CargoInventoryReportModels);

      //if (inventoryChartReportData.All(d => d.Amount.OriginalValue.Equals(0M)))
      //{
      //  await DialogService.ShowMessageAsync(_messagesLocalizer.Localize(() => Messages.NoData), 
      //    _messagesLocalizer.Localize(() => Messages.NoDataFromDateRange), MessageDialogType.Ok);
      //  return;
      //}

      var labels = inventoryChartReportData.Select(c => c.Date.LocalizedDateOnlyValue);
      var values = inventoryChartReportData.Select(c => decimal.Parse(c.Amount.DisplayValueUnitless));

      ChartData.AddChartData(labels, values);
      ChartData.VerticalAxisTitle = $"{_commonLocalizer.Localize(() => Localization.Common.Common.InventoryState)} [{result.Response.UnitName}]";
      ChartData.IsChartVisible = true;
    }
  }
}
