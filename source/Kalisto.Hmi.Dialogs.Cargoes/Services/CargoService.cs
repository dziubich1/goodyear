﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.Operations.Cargoes;
using Kalisto.Backend.Operations.Cargoes.Commands;
using Kalisto.Backend.Operations.Cargoes.Queries;
using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Hmi.Core.Communication;
using Kalisto.Hmi.Core.Services;
using Kalisto.Hmi.Dialogs.Cargoes.Models;
using Kalisto.Hmi.Dialogs.Common;
using Kalisto.Localization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Kalisto.Hmi.Dialogs.Cargoes.Services
{
  public interface ICargoService : IBackendOperationService
  {
    Task<RequestResult<List<CargoModel>>> GetAllCargoesWithInventoryAsync();

    Task<RequestResult<List<CargoModel>>> GetAllCargoesAsync();

    Task<RequestResult<long>> AddCargoAsync(string name, decimal? price);

    Task<RequestResult<long>> UpdateCargoAsync(long id, string name, decimal? price);

    Task<RequestResult<long>> DeleteCargoAsync(long id);

    Task<RequestResult<CargoModel>> GetOrAddCargoAsync(string name);

    Task<RequestResult<CargoInventoryChartReportModel>> GetInventoryChartReportDataAsync(long id, DateTime dateFrom, DateTime dateTo);
  }

  public class CargoService : BackendOperationService, ICargoService
  {
    private readonly IUnitTypeService _unitTypeService;

    public CargoService(ClientInfo clientInfo, IMessageBuilder messageBuilder, IFrontendMessageBridge messageBridge, IDialogService dialogService, IUnitTypeService unitTypeService)
      : base(clientInfo, messageBuilder, messageBridge, dialogService)
    {
      _unitTypeService = unitTypeService;
    }

    public async Task<RequestResult<List<CargoModel>>> GetAllCargoesAsync()
    {
      var message = MessageBuilder.CreateNew<GetAllCargoesQuery>()
        .BasedOn(ClientInfo)
        .Build();

      var result = await MessageBridge.SendMessageWithResultAsync<List<Cargo>>(message);
      return new RequestResult<List<CargoModel>>
      {
        Succeeded = result?.Succeeded ?? false,
        Response = result?.Response?.Select((c, index) => new CargoModel
        {
          Id = c.Id,
          OrdinalId = index + 1,
          Name = c.Name,
          Price = c.Price
        }).ToList() ?? new List<CargoModel>()
      };
    }

    public async Task<RequestResult<long>> AddCargoAsync(string name, decimal? price)
    {
      var message = MessageBuilder.CreateNew<CreateCargoCommand>()
        .BasedOn(ClientInfo)
        .WithProperty(c => c.Name, name)
        .WithProperty(c => c.Price, price)
        .Build();

      var result = await MessageBridge.SendMessageWithResultAsync<long>(message);
      return result ?? RequestResult<long>.Default;
    }

    public async Task<RequestResult<long>> UpdateCargoAsync(long id, string name, decimal? price)
    {
      var message = MessageBuilder.CreateNew<UpdateCargoCommand>()
        .BasedOn(ClientInfo)
        .WithProperty(c => c.CargoId, id)
        .WithProperty(c => c.Name, name)
        .WithProperty(c => c.Price, price)
        .Build();

      var result = await MessageBridge.SendMessageWithResultAsync<long>(message);
      return result ?? RequestResult<long>.Default;
    }

    public async Task<RequestResult<long>> DeleteCargoAsync(long id)
    {
      var message = MessageBuilder.CreateNew<DeleteCargoCommand>()
        .BasedOn(ClientInfo)
        .WithProperty(c => c.CargoId, id)
        .Build();

      var result = await MessageBridge.SendMessageWithResultAsync<long>(message);
      return result ?? RequestResult<long>.Default;
    }

    public async Task<RequestResult<CargoModel>> GetOrAddCargoAsync(string name)
    {
      var message = MessageBuilder.CreateNew<GetOrAddCargoQuery>()
      .BasedOn(ClientInfo)
      .WithProperty(c => c.Name, name)
      .Build();

      var result = await MessageBridge.SendMessageWithResultAsync<Cargo>(message);
      return new RequestResult<CargoModel>
      {
        Succeeded = result?.Succeeded ?? false,
        Response = result?.Response == null ? null : new CargoModel
        {
          Id = result.Response.Id,
          Name = result.Response.Name,
          Price = result.Response.Price
        }
      };
    }

    public async Task<RequestResult<List<CargoModel>>> GetAllCargoesWithInventoryAsync()
    {
      var message = MessageBuilder.CreateNew<GetAllCargoesInventoryQuery>()
      .BasedOn(ClientInfo)
      .Build();

      var result = await MessageBridge.SendMessageWithResultAsync<List<CargoInventoryDto>>(message);
      return new RequestResult<List<CargoModel>>
      {
        Succeeded = result?.Succeeded ?? false,
        Response = result?.Response?.Select((warehouse, index) => new CargoModel
        {
          Id = warehouse.Id,
          OrdinalId = index + 1,
          ActualAmount = new UnitOfMeasurement(warehouse.ActualAmount ?? 0M, _unitTypeService),
          Name = warehouse.Name,
          Price = warehouse.Price
        }).ToList() ?? new List<CargoModel>()
      };
    }

    public async Task<RequestResult<CargoInventoryChartReportModel>> GetInventoryChartReportDataAsync(long id, DateTime dateFrom, DateTime dateTo)
    {
      var message = MessageBuilder.CreateNew<GetCargoInventoryChartReportDataQuery>()
            .BasedOn(ClientInfo)
            .WithProperty(c => c.CargoId, id)
            .WithProperty(c => c.DateFrom, dateFrom)
            .WithProperty(c => c.DateTo, dateTo)
            .Build();

      var result = await MessageBridge.SendMessageWithResultAsync<List<CargoInventoryReportDto>>(message);

      return new RequestResult<CargoInventoryChartReportModel>
      {
        Succeeded = result?.Succeeded ?? false,
        Response = new CargoInventoryChartReportModel
        {
          UnitName = _unitTypeService.GetLocalizedCurrentUnit(),
          CargoInventoryReportModels = result?.Response?.Select(d => new CargoInventoryReportModel
          {
            Amount = new UnitOfMeasurement(d.Amount, _unitTypeService),
            Date = new LocalizedDate(d.Date)
          }).ToList() ?? new List<CargoInventoryReportModel>()
        }
      };
    }
  }
}
