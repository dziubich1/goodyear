﻿using LiveCharts;
using LiveCharts.Wpf;
using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LiveCharts.Helpers;

namespace Kalisto.Hmi.Dialogs.Cargoes.ViewModels
{
  [AddINotifyPropertyChangedInterface]
  public class ChartDataViewModel
  {
    public SeriesCollection InventoryData { get; set; }

    public List<string> Labels { get; set; }

    public bool IsChartVisible { get; set; }

    public string VerticalAxisTitle { get; set; }

    public Func<double, string> YLabelFormatter { get; set; }

    private IChartValues _chartValues
    {
      get
      {
        return InventoryData.First().Values;
      }
    }

    public ChartDataViewModel()
    {
      InventoryData = new SeriesCollection
      {
        new LineSeries
        {
          Values = new ChartValues<decimal>(),
        }
      };
      Labels = new List<string>();
      YLabelFormatter = value => value.ToString("N2");
    }

    public void ClearChartData()
    {
      InventoryData.Clear();
      InventoryData.Add(new LineSeries
      {
        Values = new ChartValues<decimal>()
      });
      Labels.Clear();
      IsChartVisible = false;
    }

    public void AddChartData(IEnumerable<string> labels, IEnumerable<decimal> chartValues)
    {
      _chartValues.AddRange(chartValues.Cast<object>());
      Labels.AddRange(labels);
    }
  }
}
