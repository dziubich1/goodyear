﻿using Kalisto.Backend.Operations.Cargoes.Commands.Interfaces;
using Kalisto.Configuration.Permissions;
using Kalisto.Hmi.Core;
using Kalisto.Hmi.Core.Commands;
using Kalisto.Hmi.Core.Services;
using Kalisto.Hmi.Dialogs.Cargoes.Dialogs;
using Kalisto.Hmi.Dialogs.Cargoes.Models;
using Kalisto.Hmi.Dialogs.Cargoes.Services;
using MahApps.Metro.Controls.Dialogs;
using PropertyChanged;
using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Kalisto.Localization;
using Kalisto.Localization.Messages;
using Kalisto.Backend.Operations.Measurements.Commands.Interfaces;

namespace Kalisto.Hmi.Dialogs.Cargoes.ViewModels
{
  [AddINotifyPropertyChangedInterface]
  public class CargoesViewModel : DataGridViewModel<CargoModel>
  {
    private readonly ILocalizer<Messages> _localizer;

    private readonly ICargoService _cargoService;

    public override string DialogTitleKey => "Cargoes";

    public ICommand AddItemCommand { get; set; }

    public ICommand EditItemCommand { get; set; }

    public ICommand DeleteItemCommand { get; set; }

    public ICommand SyncItemsCommand { get; set; }

    public ICommand ShowChartReportModalCommand { get; set; }

    public ObservableCollectionEx<CargoModel> Cargoes { get; set; }

    public CargoModel SelectedCargo { get; set; }

    protected override Type[] AssociatedCommandTypes => new[] { typeof(ICargoCommand), typeof(IMeasurementCommand) };

    public CargoesViewModel(IDialogService dialogService, IAuthorizationService authorizationService, ICargoService cargoService)
      : base(dialogService, authorizationService)
    {
      _localizer = new LocalizationManager<Messages>();
      _cargoService = cargoService;

      AddItemCommand = new DelegateCommand(AddCargo);
      EditItemCommand = new DelegateCommand(EditItem);
      DeleteItemCommand = new DelegateCommand(DeleteCargo);
      SyncItemsCommand = new DelegateCommand(SyncCargoes);
      ShowChartReportModalCommand = new DelegateCommand(ShowCargoInventoryChartReportModal);

      Cargoes = new ObservableCollectionEx<CargoModel>();
    }

    protected override ObservableCollectionEx<CargoModel> GetItemsSource()
    {
      return Cargoes;
    }

    protected override string GetEditRuleName()
    {
      return PermissionRules.DictionaryDataManagement;
    }

    public override async Task RefreshDataAsync()
    {
      await base.RefreshDataAsync();

      await LoadCargoes();
    }

    private async Task LoadCargoes()
    {
      var result = await _cargoService.GetAllCargoesWithInventoryAsync();
      if (!result.Succeeded)
        return;

      Cargoes.Clear();
      Cargoes.AddRange(result.Response);
    }

    private async void AddCargo(object parameter)
    {
      await DialogService.ShowModalDialogAsync<AddOrUpdateCargoModalDialogViewModel>(
      vm =>
      {
        vm.DialogNameKey = "AddCargo";
      },
      async viewModel =>
      {
        var result = await _cargoService.AddCargoAsync(viewModel.Name, viewModel.PriceValue);
        return result.Succeeded;
      });
    }

    private async void ShowCargoInventoryChartReportModal(object parameter)
    {
      var selectedCargo = (parameter as CargoModel) ?? SelectedCargo;
      if (selectedCargo == null)
      {
        await DialogService.ShowMessageAsync(_localizer.Localize(() => Messages.Error), _localizer.Localize(() => Messages.SelectCargoToCreateInventoryChartReport), MessageDialogType.Ok);
        return;
      }

      await DialogService.ShowModalDialogAsync<CargoInventoryChartReportDialogViewModel>(async vm =>
      {
        vm.DialogNameKey = "CargoInventoryChartReport";
        vm.CargoId = selectedCargo.Id;
        vm.DateFrom = DateTime.Now.AddMonths(-1); // Set initial date range to last month
        vm.DateTo = DateTime.Now;
      },
      async viewModel =>
      {
        return true;
      });
    }

    protected override async void EditItem(object parameter)
    {
      var selectedCargo = (parameter as CargoModel) ?? SelectedCargo;
      if (selectedCargo == null)
      {
        await DialogService.ShowMessageAsync(_localizer.Localize(() => Messages.Error), _localizer.Localize(() => Messages.SelectCargoToUpdate), MessageDialogType.Ok);
        return;
      }

      await DialogService.ShowModalDialogAsync<AddOrUpdateCargoModalDialogViewModel>(vm =>
      {
        vm.DialogNameKey = "EditCargo";
        vm.Name = selectedCargo.Name;
        vm.Price = selectedCargo.Price.ToString();
        vm.ValidateOnLoad = true;
      },
      async viewModel =>
      {
        var result = await _cargoService.UpdateCargoAsync(selectedCargo.Id,
          viewModel.Name, viewModel.PriceValue);
        return result.Succeeded;
      });
    }

    private async void DeleteCargo(object parameter)
    {
      if (SelectedCargo == null)
      {
        await DialogService.ShowMessageAsync(_localizer.Localize(() => Messages.Error), _localizer.Localize(() => Messages.SelectCargoToDelete), MessageDialogType.Ok);
        return;
      }

      var dlgResult = await DialogService.ShowMessageAsync(_localizer.Localize(() => Messages.Attention), _localizer.Localize(() => Messages.DeleteCargoQuestion),
        MessageDialogType.YesNo);

      if (dlgResult != MessageDialogResult.Affirmative)
        return;

      await _cargoService.DeleteCargoAsync(SelectedCargo.Id);
    }

    private async void SyncCargoes(object obj)
    {
      await LoadCargoes();
    }
  }
}
