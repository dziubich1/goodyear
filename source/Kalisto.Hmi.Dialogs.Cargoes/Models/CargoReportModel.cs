﻿using Kalisto.Hmi.Dialogs.Common;
using Kalisto.Localization;
using System.Collections.Generic;

namespace Kalisto.Hmi.Dialogs.Cargoes.Models
{
  public class CargoInventoryChartReportModel
  {
    public List<CargoInventoryReportModel> CargoInventoryReportModels { get; set; }

    public string UnitName { get; set; }
  }

  public class CargoInventoryReportModel
  {
    public LocalizedDate Date { get; set; }

    public UnitOfMeasurement Amount { get; set; }
  }
}
