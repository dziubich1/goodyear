﻿using Kalisto.Hmi.Dialogs.Common;

namespace Kalisto.Hmi.Dialogs.Cargoes.Models
{
  public class CargoModel
  {
    public long Id { get; set; }

    public int OrdinalId { get; set; }

    public string Name { get; set; }

    public decimal? Price { get; set; }

    public UnitOfMeasurement ActualAmount { get; set; }
  }
}
