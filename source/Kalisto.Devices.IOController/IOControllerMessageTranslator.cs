﻿using System;
using Kalisto.Devices.Core;
using Kalisto.Devices.IOController.Messages.Base;
using Kalisto.Devices.IOController.Messages.Incoming;
using System.Linq;

namespace Kalisto.Devices.IOController
{
  public class IOControllerMessageTranslator : IMessageTranslator<string, ExternalControllerSilentMessage>
  {
    public ExternalControllerSilentMessage Translate(string source)
    {
      if (string.IsNullOrEmpty(source))
        return null;

      var message = source;
      if (message.StartsWith("sewy"))
      {
        var outputNumberString = message.Replace("sewy", string.Empty);
        if (int.TryParse(outputNumberString, out int outputNumber))
        {
          return new OutputSetConfirmationMessage
          {
            OutputNumber = outputNumber
          };
        }

        return null;
      }

      if (message.StartsWith("rewy"))
      {
        var outputNumberString = message.Replace("rewy", string.Empty);
        if (int.TryParse(outputNumberString, out int outputNumber))
        {
          return new OutputResetConfirmationMessage
          {
            OutputNumber = outputNumber
          };
        }

        return null;
      }

      if (message.Contains("WE") && !message.Contains("WY")) // +XXXXXXYWE111111
      {
        var inputStartIndex = message.IndexOf("WE", StringComparison.Ordinal);
        var weightValueString = message.Substring(0, inputStartIndex - 1).Trim();
        decimal weightValue = 0;
        bool isStable = false;

        if (!string.IsNullOrEmpty(weightValueString) && decimal.TryParse(weightValueString, out decimal weightValueResult))
        {
          weightValue = weightValueResult;
          isStable = message[inputStartIndex - 1] == '&';
        }

        return new WeightMeterResponseMessage
        {
          IsStable = isStable,
          Value = weightValue,
          Input1 = message.ElementAtOrDefault(inputStartIndex + 2) == '1',
          Input2 = message.ElementAtOrDefault(inputStartIndex + 3) == '1',
          Input3 = message.ElementAtOrDefault(inputStartIndex + 4) == '1',
          Input4 = message.ElementAtOrDefault(inputStartIndex + 5) == '1',
          Input5 = message.ElementAtOrDefault(inputStartIndex + 6) == '1',
          Input6 = message.ElementAtOrDefault(inputStartIndex + 7) == '1',
          Input7 = message.ElementAtOrDefault(inputStartIndex + 8) == '1',
          Input8 = message.ElementAtOrDefault(inputStartIndex + 9) == '1'
        };
      }

      if (message.Contains("WY") && message.Contains("WE")) // WY0000010000000000WE010000
      {
        var outputStartIndex = message.IndexOf("WY", StringComparison.Ordinal);
        return new InputOutputStatesResponseMessage
        {
          Output1 = message.ElementAtOrDefault(outputStartIndex + 2) == '1',
          Output2 = message.ElementAtOrDefault(outputStartIndex + 3) == '1',
          Output3 = message.ElementAtOrDefault(outputStartIndex + 4) == '1',
          Output4 = message.ElementAtOrDefault(outputStartIndex + 5) == '1',
          Output5 = message.ElementAtOrDefault(outputStartIndex + 6) == '1',
          Output6 = message.ElementAtOrDefault(outputStartIndex + 7) == '1',
          Output7 = message.ElementAtOrDefault(outputStartIndex + 8) == '1',
          Output8 = message.ElementAtOrDefault(outputStartIndex + 9) == '1'
        };
      }

      return null;
    }
  }
}
