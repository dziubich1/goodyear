﻿using System.Threading.Tasks;

namespace Kalisto.Devices.IOController.Interfaces
{
  public enum TrafficState
  {
    Red, Green
  }

  public interface ITrafficLightsService
  {
    void SetGreenLight(int weightMeterId);

    void SetRedLight(int weightMeterId);

    Task RequestTrafficStateAsync();
  }
}
