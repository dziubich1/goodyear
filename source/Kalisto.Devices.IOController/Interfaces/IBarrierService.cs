﻿using System.Threading.Tasks;

namespace Kalisto.Devices.IOController.Interfaces
{
  public enum BarrierState
  {
    Up, Down
  }

  public interface IBarrierService
  {
    Task RaiseBarrierUpAsync(int weightMeterId);
    Task LeaveBarrierDownAsync(int weightMeterId);
    Task RequestBarrierStateAsync();
  }
}
