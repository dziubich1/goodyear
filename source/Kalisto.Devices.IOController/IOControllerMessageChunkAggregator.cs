﻿using Kalisto.Devices.Core;
using Kalisto.Devices.IOController.Messages.Base;
using System;
using System.Text;

namespace Kalisto.Devices.IOController
{
  public class IOControllerMessageChunkAggregator : IMessageChunkAggregator<ExternalControllerSilentMessage>
  {
    public event Action<ExternalControllerSilentMessage> MessageReady;

    private string _chunkBuffer;

    private const string Eol = "\r\n";

    private const string Stx = "\u0002";

    private const int FrameMaxLength = 200;

    private readonly IMessageTranslator<string, ExternalControllerSilentMessage> _messageTranslator;

    public IOControllerMessageChunkAggregator(IMessageTranslator<string, ExternalControllerSilentMessage> messageTranslator)
    {
      _messageTranslator = messageTranslator;
    }

    public void AddChunk(byte[] bytes)
    {
      try
      {
        var chunkString = Encoding.ASCII.GetString(bytes);

        _chunkBuffer += chunkString;
        if (_chunkBuffer.Contains(Stx) && _chunkBuffer.Contains(Eol))
        {
          int firstStxIndex = _chunkBuffer.IndexOf(Stx, StringComparison.Ordinal);
          int firstEolIndex = _chunkBuffer.IndexOf(Eol, StringComparison.Ordinal);
          if (firstStxIndex > firstEolIndex)
          {
            _chunkBuffer = _chunkBuffer.Substring(firstStxIndex, _chunkBuffer.Length - firstStxIndex); // remove beginning of message (broken part)
            firstEolIndex = _chunkBuffer.IndexOf(Eol, StringComparison.Ordinal);
            if (firstEolIndex < 0)
              return;
          }

          var messageString = _chunkBuffer.Substring(firstStxIndex, (firstEolIndex - firstStxIndex) + Eol.Length);
          var escapedSource = messageString.Replace(Stx, string.Empty).Replace(Eol, string.Empty);
          var message = _messageTranslator.Translate(escapedSource);
          if (message != null)
            MessageReady?.Invoke(message);

          _chunkBuffer = _chunkBuffer.Remove(0, firstEolIndex + Eol.Length);
          return;
        }
      }
      catch
      {
        // discard chunk buffer when something goes wrong
        _chunkBuffer = string.Empty;
      }

      if (_chunkBuffer.Length > FrameMaxLength)
        _chunkBuffer = string.Empty;
    }
  }
}
