﻿using Kalisto.Devices.Core;
using Kalisto.Devices.Core.Communication;
using Kalisto.Devices.Core.Messages;
using Kalisto.Devices.IOController.Messages.Base;
using System;
using System.Text;
using System.Threading.Tasks;
using Kalisto.Devices.Core.Communication.Interfaces;
using Kalisto.Devices.IOController.Messages.Incoming;

namespace Kalisto.Devices.IOController
{
  public class IOControllerDevice : IDevice<ExternalControllerSilentMessage>
  {
    public int Id { get; set; }

    private readonly IDeviceClientConnection _clientConnection;

    private readonly IMessageChunkAggregator<ExternalControllerSilentMessage> _messageChunkAggregator;

    private string _endpointAddress;

    public event Action<ExternalControllerSilentMessage> MessageReceived;

    public event Action<ConnectionState> ConnectionStateChanged;

    public IOControllerDevice(IDeviceClientConnection tcpClientConnection,
      IMessageChunkAggregator<ExternalControllerSilentMessage> messageChunkAggregator)
    {
      _clientConnection = tcpClientConnection;
      _clientConnection.BytesMessageReceived += OnBytesReceived;
      _clientConnection.ConnectionStateChanged += OnConnectionStateChanged;
      _messageChunkAggregator = messageChunkAggregator;
      _messageChunkAggregator.MessageReady += OnMessageReady;
    }

    public void SendCommand<TCommand>(TCommand command)
      where TCommand : DeviceCommandMessageBase
    {
      if (string.IsNullOrEmpty(command.Body))
        return;

      _clientConnection.SendBytes(Encoding.ASCII.GetBytes(command.Body));
    }

    public void StartSampling()
    {
      _clientConnection.Connect(true);
      ResetController();
    }

    public void StopSampling()
    {
      _clientConnection.Disconnect();
    }

    protected virtual void OnConnectionStateChanged(ConnectionState obj)
    {
      ConnectionStateChanged?.Invoke(obj);
    }

    private void OnBytesReceived(object sender, BytesMessageReceivedEventArgs e)
    {
      _endpointAddress = e.EndpointId;
      _messageChunkAggregator.AddChunk(e.Bytes);
    }

    private void OnMessageReady(ExternalControllerSilentMessage message)
    {
      message.DeviceId = Id;
      if (string.IsNullOrEmpty(message.EndpointAddress))
        message.EndpointAddress = _endpointAddress;
      MessageReceived?.Invoke(message);
    }

    private void ResetController()
    {
      _clientConnection.SendBytes(Encoding.ASCII.GetBytes("resta\r\n"));
      Task.Run(async () =>
      {
        await Task.Delay(15000);
        OnMessageReady(new WeightMeterResponseMessage
        {
          Corrupted = true
        });
      });
    }
  }
}
