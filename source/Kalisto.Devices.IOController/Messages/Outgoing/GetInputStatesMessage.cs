﻿using Kalisto.Devices.Core.Messages;

namespace Kalisto.Devices.IOController.Messages.Outgoing
{
  public class GetInputStatesMessage : DeviceCommandMessageBase
  {
    public override string Body => $"swewy\r\n";
  }
}
