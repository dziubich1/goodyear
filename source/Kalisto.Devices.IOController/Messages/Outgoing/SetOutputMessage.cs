﻿using Kalisto.Devices.Core.Messages;

namespace Kalisto.Devices.IOController.Messages.Outgoing
{
  public class SetOutputMessage : DeviceCommandMessageBase
  {
    public int OutputNumber { get; set; }

    public override string Body => $"sewy{OutputNumber}\r\n";
  }
}
