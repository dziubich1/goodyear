﻿using Kalisto.Devices.Core.Messages;

namespace Kalisto.Devices.IOController.Messages.Outgoing
{
  public class ResetOutputMessage : DeviceCommandMessageBase
  {
    public int OutputNumber { get; set; }

    public override string Body => $"rewy{OutputNumber}\r\n";
  }
}
