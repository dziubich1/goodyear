﻿using Kalisto.Devices.Core.Messages;

namespace Kalisto.Devices.IOController.Messages.Outgoing
{
  public class GetWeightMeterValueMessage : DeviceCommandMessageBase
  {
    public override string Body => $"waga1\r\n";
  }
}
