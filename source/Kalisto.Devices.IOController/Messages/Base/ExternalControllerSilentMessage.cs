﻿using System;
using Kalisto.Devices.Core.Messages;
using Kalisto.Devices.IOController.Handlers;

namespace Kalisto.Devices.IOController.Messages.Base
{
  public class ExternalControllerSilentMessage : DeviceMessage<IExternalControllerMessageHandler>
  {
  }
}
