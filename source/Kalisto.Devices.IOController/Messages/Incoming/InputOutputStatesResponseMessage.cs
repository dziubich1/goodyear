﻿using Kalisto.Devices.IOController.Handlers;
using Kalisto.Devices.IOController.Messages.Base;

namespace Kalisto.Devices.IOController.Messages.Incoming
{
  public class InputOutputStatesResponseMessage : ExternalControllerSilentMessage
  {
    public bool Output1 { get; set; }

    public bool Output2 { get; set; }

    public bool Output3 { get; set; }

    public bool Output4 { get; set; }

    public bool Output5 { get; set; }

    public bool Output6 { get; set; }

    public bool Output7 { get; set; }

    public bool Output8 { get; set; }

    // TODO: add input states later

    protected override void Handle(IExternalControllerMessageHandler handler)
    {
      handler.HandleOutputStatesMessage(this);
    }
  }
}
