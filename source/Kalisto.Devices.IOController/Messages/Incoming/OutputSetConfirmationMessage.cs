﻿using Kalisto.Devices.IOController.Messages.Base;
using Kalisto.Devices.IOController.Messages.Outgoing;
using System;
using Kalisto.Devices.IOController.Handlers;

namespace Kalisto.Devices.IOController.Messages.Incoming
{
  public class OutputSetConfirmationMessage : ExternalControllerSilentMessage
  {
    public int OutputNumber { get; set; }

    protected override void Handle(IExternalControllerMessageHandler handler)
    {
      handler.HandleOutputSetResponseMessage(this);
    }
  }
}
