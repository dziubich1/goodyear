﻿using Kalisto.Devices.IOController.Handlers;
using Kalisto.Devices.IOController.Messages.Base;

namespace Kalisto.Devices.IOController.Messages.Incoming
{
  public class OutputResetConfirmationMessage : ExternalControllerSilentMessage
  {
    public int OutputNumber { get; set; }

    protected override void Handle(IExternalControllerMessageHandler handler)
    {
      handler.HandleOutputResetResponseMessage(this);
    }
  }
}
