﻿using Kalisto.Devices.IOController.Handlers;
using Kalisto.Devices.IOController.Messages.Base;

namespace Kalisto.Devices.IOController.Messages.Incoming
{
  public class WeightMeterResponseMessage : ExternalControllerSilentMessage
  {
    public bool IsStable { get; set; }

    public decimal Value { get; set; }

    public bool Input1 { get; set; }

    public bool Input2 { get; set; }

    public bool Input3 { get; set; }

    public bool Input4 { get; set; }

    public bool Input5 { get; set; }

    public bool Input6 { get; set; }

    public bool Input7 { get; set; }

    public bool Input8 { get; set; }
    
    protected override void Handle(IExternalControllerMessageHandler handler)
    {
      handler.HandleWeightMeterMessage(this);
    }
  }
}
