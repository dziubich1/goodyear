﻿using Kalisto.Devices.Core.Handlers;
using Kalisto.Devices.IOController.Messages.Incoming;

namespace Kalisto.Devices.IOController.Handlers
{
  public interface IExternalControllerMessageHandler : IDeviceMessageHandler
  {
    void HandleWeightMeterMessage(WeightMeterResponseMessage message);

    void HandleOutputStatesMessage(InputOutputStatesResponseMessage message);

    void HandleOutputSetResponseMessage(OutputSetConfirmationMessage message);

    void HandleOutputResetResponseMessage(OutputResetConfirmationMessage message);
  }
}
