﻿using Kalisto.Devices.BeltConveyorCounter.Handlers;
using Kalisto.Devices.BeltConveyorCounter.Messages.Base;
using System;

namespace Kalisto.Devices.BeltConveyorCounter.Messages.Incoming
{
  [Serializable]
  public class BeltConveyorDataImportResponseMessage : BeltConveyorSilentMessage
  {
    public int ItemsCount { get; set; }

    public bool HasMemoryError { get; set; }

    protected override void Handle(IBeltConveyorMessageHandler handler)
    {
      handler.HandleBeltConveyorDataImportResponse(this);
    }
  }
}
