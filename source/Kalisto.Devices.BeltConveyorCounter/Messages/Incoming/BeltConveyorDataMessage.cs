﻿using Kalisto.Devices.BeltConveyorCounter.Messages.Base;
using System;
using Kalisto.Devices.BeltConveyorCounter.Handlers;

namespace Kalisto.Devices.BeltConveyorCounter.Messages.Incoming
{
  [Serializable]
  public class BeltConveyorDataMessage : BeltConveyorSilentMessage
  {
    public DateTime DateUtc { get; set; }

    public DateTime Date { get; set; }

    public decimal IncrementalWeight { get; set; }

    public decimal B1 { get; set; }

    public decimal B2 { get; set; }

    public decimal B3 { get; set; }

    public decimal B4 { get; set; }

    protected override void Handle(IBeltConveyorMessageHandler handler)
    {
      handler.HandleBeltConveyorDataMessage(this);
    }
  }
}
