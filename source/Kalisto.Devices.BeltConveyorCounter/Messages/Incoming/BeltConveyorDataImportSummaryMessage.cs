﻿using Kalisto.Devices.BeltConveyorCounter.Handlers;
using Kalisto.Devices.BeltConveyorCounter.Messages.Base;
using System;

namespace Kalisto.Devices.BeltConveyorCounter.Messages.Incoming
{
  [Serializable]
  public class BeltConveyorDataImportSummaryMessage : BeltConveyorSilentMessage
  {
    public int TransferredItemsCount { get; set; }

    protected override void Handle(IBeltConveyorMessageHandler handler)
    {
      handler.HandleBeltConveyorDataImportSummary(this);
    }
  }
}
