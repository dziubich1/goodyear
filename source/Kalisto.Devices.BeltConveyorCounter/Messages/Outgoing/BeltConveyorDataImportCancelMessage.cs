﻿using Kalisto.Devices.Core.Messages;

namespace Kalisto.Devices.BeltConveyorCounter.Messages.Outgoing
{
  public class BeltConveyorDataImportCancelMessage : DeviceCommandMessageBase
  {
    public override string Body => "no_i\r\n";
  }
}
