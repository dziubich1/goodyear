﻿using Kalisto.Devices.Core.Messages;

namespace Kalisto.Devices.BeltConveyorCounter.Messages.Outgoing
{
  public class BeltConveyorDataImportConfirmMessage : DeviceCommandMessageBase
  {
    public override string Body => "yes_f\r\n";
  }
}
