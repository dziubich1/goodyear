﻿using Kalisto.Devices.Core.Messages;

namespace Kalisto.Devices.BeltConveyorCounter.Messages.Outgoing
{
  public class BeltConveyorDataImportAcceptMessage : DeviceCommandMessageBase
  {
    public override string Body => "yes_i\r\n";
  }
}
