﻿using Kalisto.Devices.Core.Messages;

namespace Kalisto.Devices.BeltConveyorCounter.Messages.Outgoing
{
  public class BeltConveyorDataImportRequestMessage : DeviceCommandMessageBase
  {
    public override string Body => "imp_dni\r\n";
  }
}
