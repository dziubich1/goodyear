﻿using Kalisto.Devices.Core.Messages;

namespace Kalisto.Devices.BeltConveyorCounter.Messages.Outgoing
{
  public class BeltConveyorDataImportRejectMessage : DeviceCommandMessageBase
  {
    public override string Body => "no_f\r\n";
  }
}
