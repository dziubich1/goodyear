﻿using Kalisto.Devices.Core.Messages;
using System;
using Kalisto.Devices.BeltConveyorCounter.Handlers;

namespace Kalisto.Devices.BeltConveyorCounter.Messages.Base
{
  [Serializable]
  public abstract class BeltConveyorSilentMessage : DeviceMessage<IBeltConveyorMessageHandler>
  {
  }
}
