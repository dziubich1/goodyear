﻿using Kalisto.Devices.BeltConveyorCounter.Messages.Base;
using Kalisto.Devices.Core;
using System;
using System.Text;
using System.Text.RegularExpressions;

namespace Kalisto.Devices.BeltConveyorCounter
{
  public class BeltConveyorMessageChunkAggregator : IMessageChunkAggregator<BeltConveyorSilentMessage>
  {
    public event Action<BeltConveyorSilentMessage> MessageReady;

    private string _chunkBuffer;

    private const string Eol = "\r\n";

    private const string Delete = "\u007F?";

    private const int FrameMaxLength = 200;

    private readonly IMessageTranslator<string, BeltConveyorSilentMessage> _messageTranslator;

    public BeltConveyorMessageChunkAggregator(IMessageTranslator<string, BeltConveyorSilentMessage> messageTranslator)
    {
      _messageTranslator = messageTranslator;
    }

    public void AddChunk(byte[] bytes)
    {
      try
      {
        var chunkString = Encoding.ASCII.GetString(bytes);
        if (chunkString.Contains(Delete))
          chunkString = chunkString.Replace(Delete, string.Empty);

        _chunkBuffer += chunkString;
        if (_chunkBuffer.Contains(Eol) && GetEndOfLineOccurrences() >= 1)
        {
          int firstIndex = _chunkBuffer.IndexOf(Eol, StringComparison.Ordinal);
          var messageString = _chunkBuffer.Substring(0, firstIndex + 2);
          var message = _messageTranslator.Translate(messageString.Replace(Eol, string.Empty));
          if (message != null)
            MessageReady?.Invoke(message);

          _chunkBuffer = _chunkBuffer.Replace(messageString, string.Empty).Replace(Eol, string.Empty);
          return;
        }
      }
      catch
      {
        // discard chunk buffer when something goes wrong
        _chunkBuffer = string.Empty;
      }

      if (_chunkBuffer.Length > FrameMaxLength)
        _chunkBuffer = string.Empty;
    }

    private int GetEndOfLineOccurrences()
    {
      return Regex.Matches(_chunkBuffer, Eol).Count;
    }
  }
}
