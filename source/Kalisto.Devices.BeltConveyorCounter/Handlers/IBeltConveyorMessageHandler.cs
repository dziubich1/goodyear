﻿using Kalisto.Devices.BeltConveyorCounter.Messages.Incoming;
using Kalisto.Devices.Core.Handlers;

namespace Kalisto.Devices.BeltConveyorCounter.Handlers
{
  public interface IBeltConveyorMessageHandler : IDeviceMessageHandler
  {
    void HandleBeltConveyorDataImportResponse(BeltConveyorDataImportResponseMessage message);

    void HandleBeltConveyorDataMessage(BeltConveyorDataMessage message);

    void HandleBeltConveyorDataImportSummary(BeltConveyorDataImportSummaryMessage message);
  }
}
