﻿using Kalisto.Devices.BeltConveyorCounter.Messages.Base;
using Kalisto.Devices.Core;
using Kalisto.Devices.Core.Communication;
using Kalisto.Devices.Core.Communication.Interfaces;
using Kalisto.Devices.Core.Messages;
using System;
using System.Text;

namespace Kalisto.Devices.BeltConveyorCounter
{
  public class BeltConveyorCounterDevice : IDevice<BeltConveyorSilentMessage>
  {
    private readonly object _syncLock = new object();

    private readonly IDeviceClientConnection _tcpClientConnection;

    private readonly IMessageChunkAggregator<BeltConveyorSilentMessage> _messageChunkAggregator;

    private string _endpointAddress;

    public int Id { get; set; }

    public event Action<BeltConveyorSilentMessage> MessageReceived;

    public event Action<ConnectionState> ConnectionStateChanged;

    public BeltConveyorCounterDevice(IDeviceClientConnection tcpClientConnection,
      IMessageChunkAggregator<BeltConveyorSilentMessage> messageChunkAggregator)
    {
      _tcpClientConnection = tcpClientConnection;
      _tcpClientConnection.BytesMessageReceived += OnBytesReceived;
      _tcpClientConnection.ConnectionStateChanged += OnConnectionStateChanged;
      _messageChunkAggregator = messageChunkAggregator;
      _messageChunkAggregator.MessageReady += OnMessageReady;
    }

    public void SendCommand<TCommand>(TCommand command)
      where TCommand : DeviceCommandMessageBase
    {
      if (string.IsNullOrEmpty(command.Body))
        return;

      _tcpClientConnection.SendBytes(Encoding.ASCII.GetBytes(command.Body));
    }

    public void StartSampling()
    {
      _tcpClientConnection.Connect(true);
    }

    public void StopSampling()
    {
      _tcpClientConnection.Disconnect();
    }

    protected virtual void OnConnectionStateChanged(ConnectionState obj)
    {
      ConnectionStateChanged?.Invoke(obj);
    }

    private void OnBytesReceived(object sender, BytesMessageReceivedEventArgs e)
    {
      _endpointAddress = e.EndpointId;
      _messageChunkAggregator.AddChunk(e.Bytes);
    }

    private void OnMessageReady(BeltConveyorSilentMessage message)
    {
      message.DeviceId = Id;
      if (string.IsNullOrEmpty(message.EndpointAddress))
        message.EndpointAddress = _endpointAddress;
      MessageReceived?.Invoke(message);
    }
  }
}
