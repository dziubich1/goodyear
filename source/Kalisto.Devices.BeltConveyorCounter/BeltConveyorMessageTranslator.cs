﻿using Kalisto.Devices.BeltConveyorCounter.Messages.Base;
using Kalisto.Devices.BeltConveyorCounter.Messages.Incoming;
using Kalisto.Devices.Core;
using System;
using System.Globalization;
using System.Linq;

namespace Kalisto.Devices.BeltConveyorCounter
{
  public class BeltConveyorMessageTranslator : IMessageTranslator<string, BeltConveyorSilentMessage>
  {
    private const string DateFormat = "dd/MM/yy HH:mm:ss";

    public BeltConveyorSilentMessage Translate(string source)
    {
      if (string.IsNullOrEmpty(source))
        return null;

      // Device SD Memory card error message
      // "b_sd" message means that device has detected SD memory card issue and it can no longer work properly
      if (source.ToLower() == "b_sd")
      {
        return new BeltConveyorDataImportResponseMessage
        {
          HasMemoryError = true,
          ItemsCount = 0
        };
      }

      // Data import request response message
      // Frames: L1 or L0 - 1 means that there is data available, 0 means that there is nothing to read
      if (source.Length == 2 && source.StartsWith("L"))
        return ExtractDataImportResponseMessage(source);

      // Data message
      // Example frame: "#25/01/19;08:51:09;LG-00010010.30;A-000.00;B-000.00;C-000.00;D-000.00",
      // "# - this is beginning of the frame
      // ", - this is end of the frame
      if (source.Length == 72 && source.StartsWith("\"#") && source.EndsWith("\","))
        return ExtractDataMessage(source);

      // Data import summary message
      // Example frame: LP00091
      // Right after 'LP' part we always receive 5 bytes containing number of data frames sent by device
      // We need to trim leading zeros to get the proper value
      if (source.Length == 7 && source.StartsWith("LP"))
        return ExtractDataImportSummaryMessage(source);

      return null;
    }

    private BeltConveyorDataImportResponseMessage ExtractDataImportResponseMessage(string message)
    {
      if (!int.TryParse(message[1].ToString(), out int value))
        return new BeltConveyorDataImportResponseMessage
        {
          HasMemoryError = false,
          ItemsCount = 0,
          Corrupted = true
        };

      return new BeltConveyorDataImportResponseMessage
      {
        HasMemoryError = false,
        ItemsCount = value,
        Corrupted = false
      };
    }

    private BeltConveyorDataImportSummaryMessage ExtractDataImportSummaryMessage(string message)
    {
      // example frame: LP00091 - we want to get rid of 'LP' marker
      message = message.Replace("LP", string.Empty); // leave value only

      // e.g. 00091 - we want to get rid of leading zeros
      var zeroString = 0.ToString();
      var valString = message.TrimStart(zeroString.ToCharArray());
      if (string.IsNullOrEmpty(valString))
        valString = zeroString;

      // e.g. 91 - we want to make sure that value is valid number
      if (!int.TryParse(valString, out int value))
      {
        return new BeltConveyorDataImportSummaryMessage
        {
          TransferredItemsCount = 0,
          Corrupted = true
        };
      }

      return new BeltConveyorDataImportSummaryMessage
      {
        TransferredItemsCount = value,
        Corrupted = false
      };
    }

    private BeltConveyorDataMessage ExtractDataMessage(string message)
    {
      // Example frame: "#25/01/19;08:51:09;LG-00010010.30;A-000.00;B-000.00;C-000.00;D-000.00",
      // We want to get rid of the leading and trailing markers
      var trimmedMessage = message.TrimStart("\"#".ToCharArray()).TrimEnd("\",".ToCharArray());
      if (string.IsNullOrEmpty(trimmedMessage))
        return new BeltConveyorDataMessage { Corrupted = true };

      // Split data to receive labeled values in format LABEL-VALUE
      // Result should contain 7 parts: Date, Time, LG (Increment weight), A, B, C, D
      var messageParts = trimmedMessage.Split(";".ToCharArray());
      if (!messageParts.Any() || messageParts.Length != 7 || !AreMessagePartsValid(messageParts))
        return new BeltConveyorDataMessage { Corrupted = true };

      var dateTimeString = $"{messageParts[0]} {messageParts[1]}";
      var incrementSumString = GetLabeledValue("LG", messageParts[2]); // Get value from labeled value for Increment weight
      var b1String = GetLabeledValue("A", messageParts[3]); // Get value from labeled value for A boiler (B1)
      var b2String = GetLabeledValue("B", messageParts[4]); // Get value from labeled value for B boiler (B2)
      var b3String = GetLabeledValue("C", messageParts[5]); // Get value from labeled value for C boiler (B3)
      var b4String = GetLabeledValue("D", messageParts[6]); // Get value from labeled value for D boiler (B4)

      var date = DateTime.ParseExact(dateTimeString, DateFormat, CultureInfo.InvariantCulture);
      return new BeltConveyorDataMessage
      {
        B1 = decimal.Parse(GetValidNumberString(b1String)),
        B2 = decimal.Parse(GetValidNumberString(b2String)),
        B3 = decimal.Parse(GetValidNumberString(b3String)),
        B4 = decimal.Parse(GetValidNumberString(b4String)),
        IncrementalWeight = decimal.Parse(GetValidNumberString(incrementSumString)),
        DateUtc = date.ToUniversalTime(),
        Date = date,
        Corrupted = false
      };
    }

    private bool AreMessagePartsValid(string[] parts)
    {
      var dateTimeString = $"{parts[0]} {parts[1]}";
      if (!DateTime.TryParseExact(dateTimeString, DateFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out _))
        return false;

      // Get value from labeled value for Increment weight
      var value = GetLabeledValue("LG", parts[2]);
      if (!decimal.TryParse(GetValidNumberString(value), out _))
        return false;

      // Get value from labeled value for A boiler (B1)
      value = GetLabeledValue("A", parts[3]);
      if (!decimal.TryParse(GetValidNumberString(value), out _))
        return false;

      // Get value from labeled value for A boiler (B2)
      value = GetLabeledValue("B", parts[4]);
      if (!decimal.TryParse(GetValidNumberString(value), out _))
        return false;

      // Get value from labeled value for A boiler (B3)
      value = GetLabeledValue("C", parts[5]);
      if (!decimal.TryParse(GetValidNumberString(value), out _))
        return false;

      // Get value from labeled value for A boiler (B4)
      value = GetLabeledValue("D", parts[6]);
      if (!decimal.TryParse(GetValidNumberString(value), out _))
        return false;

      return true;
    }

    private string GetLabeledValue(string label, string val)
    {
      var valWithoutLabel = val.Replace($"{label}-", string.Empty);
      var zeroString = 0.ToString();
      var valString = valWithoutLabel.TrimStart(zeroString.ToCharArray());
      if (string.IsNullOrEmpty(valString))
        valString = zeroString;

      return valString;
    }

    private string GetValidNumberString(string value)
    {
      if (NumberFormatInfo.CurrentInfo.NumberDecimalSeparator == "." && value.Contains(","))
        value = value.Replace(",", ".");
      if (NumberFormatInfo.CurrentInfo.NumberDecimalSeparator == "," && value.Contains("."))
        value = value.Replace(".", ",");

      return value;
    }
  }
}
