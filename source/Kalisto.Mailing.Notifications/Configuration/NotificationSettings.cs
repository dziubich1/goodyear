﻿using System;
using Kalisto.Configuration;

namespace Kalisto.Mailing.Notifications.Configuration
{
  public enum NotificationType
  {
    Daily,
    Weekly,
    Monthly
  }

  [Serializable]
  public class NotificationSettings : ConfigurationBase
  {
    public bool Enabled { get; set; }

    public string From { get; set; }

    public string To { get; set; }

    public NotificationType Type { get; set; }

    public DateTime? LastSendDate { get; set; }

    public override object GetDefaultConfiguration()
    {
      return new NotificationSettings
      {
        To = string.Empty,
        Type = NotificationType.Weekly,
        LastSendDate = null
      };
    }
  }
}
