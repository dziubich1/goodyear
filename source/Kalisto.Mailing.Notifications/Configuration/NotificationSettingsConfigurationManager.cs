﻿using Kalisto.Configuration;

namespace Kalisto.Mailing.Notifications.Configuration
{
  public class NotificationSettingsConfigurationManager : ProgramDataConfigurationManager
  {
    protected override string ConfigFileName => "NotificationSettings.xml";

    public NotificationSettingsConfigurationManager(string domainName, string appName) 
      : base(domainName, appName)
    {
    }
  }
}
