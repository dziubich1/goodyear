﻿using System.CodeDom.Compiler;

namespace Kalisto.Mailing.Notifications
{
  public abstract class NotificationMessage : IHtmlMessage
  {
    public virtual string Subject => "No subject";

    public abstract string GetRawHtmlString();

    public abstract void Generate();
  }
}
