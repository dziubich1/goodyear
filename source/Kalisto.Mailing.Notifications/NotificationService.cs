﻿using Kalisto.Mailing.Notifications.Configuration;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Unity;

namespace Kalisto.Mailing.Notifications
{
  public interface INotificationService
  {
    bool ShouldSendNotification();

    void RegisterNotificationType<T>() where T : NotificationMessage;

    Task<bool> SendNotificationsAsync();
  }

  public class NotificationService : INotificationService
  {
    private readonly IUnityContainer _container;

    private readonly NotificationSettingsConfigurationManager _configurationManager;

    private readonly IEmailService _emailService;

    private readonly List<Type> _messageTypes = new List<Type>();

    private bool _isBusy;

    public NotificationService(IUnityContainer container, NotificationSettingsConfigurationManager configurationManager, IEmailService emailService)
    {
      _container = container;
      _configurationManager = configurationManager;
      _emailService = emailService;
    }

    public bool ShouldSendNotification()
    {
      var settings = _configurationManager.Load<NotificationSettings>();
      if (!settings.Enabled)
        return false;

      if (settings.LastSendDate == null)
        return true;

      return HasTimeElapsed(settings.Type, settings.LastSendDate.Value);
    }

    public void RegisterNotificationType<T>() where T : NotificationMessage
    {
      if (_messageTypes.Contains(typeof(T)))
        return;

      _messageTypes.Add(typeof(T));
    }

    public async Task<bool> SendNotificationsAsync()
    {
      if (_isBusy)
        return true;

      _isBusy = true;
      var settings = _configurationManager.Load<NotificationSettings>();
      if (!settings.Enabled)
      {
        _isBusy = false;
        return true;
      }

      try
      {
        var notifications = new List<NotificationMessage>();
        foreach (var notificationType in _messageTypes)
        {
          var notification = _container.Resolve(notificationType) as NotificationMessage;
          if (notification == null)
            continue;

          notification.Generate();
          notifications.Add(notification);
        }

        bool result = false;
        foreach (var notification in notifications)
          result = await _emailService.SendMessageAsync(settings.From, settings.To, notification);

        if (result)
          UpdateLastSendDate();

        _isBusy = false;
        return true;
      }
      catch
      {
        return false;
      }
    }

    private void UpdateLastSendDate()
    {
      var settings = _configurationManager.Load<NotificationSettings>();
      settings.LastSendDate = DateTime.Now;
      _configurationManager.Save(settings);
    }

    private bool HasTimeElapsed(NotificationType type, DateTime lastSendDate)
    {
      var daySpan = default(TimeSpan);
      if (type == NotificationType.Daily)
        daySpan = TimeSpan.FromDays(1);
      if (type == NotificationType.Weekly)
        daySpan = TimeSpan.FromDays(7);
      if (type == NotificationType.Monthly)
        daySpan = TimeSpan.FromDays(30);

      if (lastSendDate.Add(daySpan) <= DateTime.Now)
        return true;

      return false;
    }
  }
}
