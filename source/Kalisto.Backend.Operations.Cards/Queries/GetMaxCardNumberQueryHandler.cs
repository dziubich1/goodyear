﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Communication.Core.Messaging;
using System.Linq;

namespace Kalisto.Backend.Operations.Cards.Queries
{
  public class GetMaxCardNumberQueryHandler : QueryHandlerBase<GetMaxCardNumberQuery>
  {
    public override object Execute(IDbContext context, GetMaxCardNumberQuery query, string partnerId)
    {
      var maxCardNumber = 0;
      if (context.Cards.Any())
      {
        maxCardNumber = context.Cards.Max(c => c.Number);
      }
      return maxCardNumber;
    }
  }
}
