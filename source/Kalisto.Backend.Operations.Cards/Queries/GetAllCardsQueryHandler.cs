﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Communication.Core.Messaging;
using System.Data.Entity;
using System.Linq;

namespace Kalisto.Backend.Operations.Cards.Queries
{
  public class GetAllCardsQueryHandler : QueryHandlerBase<GetAllCardsQuery>
  {
    public override object Execute(IDbContext context, GetAllCardsQuery query, string partnerId)
    {
      return context.Cards
        .AsNoTracking()
        .Include(c => c.Cargo)
        .Include(c => c.Driver)
        .Include(c => c.Vehicle)
        .Include(c => c.Contractor)
        .ToList();
    }
  }
}
