﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.DataAccess.Model.Logging;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kalisto.Backend.Operations.Cards.Commands
{
  public class DeleteCardCommandHandler : CommandHandlerBase<DeleteCardCommand>
  {
    private readonly IDbDeleteEventLogger _dbEventLogger;
    private readonly ILogger _logger;

    public DeleteCardCommandHandler(IDbDeleteEventLogger dbEventLogger, ILogger logger)
    {
      _dbEventLogger = dbEventLogger;
      _logger = logger;
    }

    public override object Execute(IDbContext context, DeleteCardCommand command, string partnerId)
    {
      var card = context.Cards.FirstOrDefault(c => c.Id == command.CardId);
      if (card == null)
      {
        _logger.Error($"Cannot delete card with ID: {command.CardId} because it does not exist");
        throw new ArgumentException("Card with given identifier does not exist.");
      }

      context.Cards.Remove(card);
      context.SaveChanges();

      _dbEventLogger.LogEvent(partnerId, ctx => ctx.Cards, card.Id, null, card);
      _logger.Debug($"Successfully deleted card with ID: {card.Id}");

      return command.CardId;
    }
  }
}
