﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.DataAccess.Model.Logging;
using Kalisto.Backend.Operations.Cards.Exceptions;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Logging;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kalisto.Backend.Operations.Cards.Commands
{
  public class UpdateCardCommandHandler : CommandHandlerBase<UpdateCardCommand>
  {
    private readonly IDbUpdateEventLogger _dbEventLogger;
    private readonly ILogger _logger;

    public UpdateCardCommandHandler(IDbUpdateEventLogger dbEventLogger, ILogger logger)
    {
      _dbEventLogger = dbEventLogger;
      _logger = logger;
    }

    public override object Execute(IDbContext context, UpdateCardCommand command, string partnerId)
    {
      var card = context.Cards
        .Include(c => c.Cargo).Include(c => c.Driver).Include(c => c.Vehicle).Include(c => c.Contractor)
        .FirstOrDefault(c => c.Id == command.CardId);
      if (card == null)
      {
        _logger.Error($"Cannot update card with ID: {command.CardId} because it does not exist");
        throw new ArgumentException("Card with given identifier does not exist.");
      }

      if (context.Cards.Any(c => c.Id != card.Id && c.Code == command.Code))
      {
        _logger.Debug($"Cannot update card code because card with code: {command.Code} already exists. Updated card ID: {card.Id}");
        throw new CardExistsException();
      }

      CardLifetime? lifetime = null;
      if (command.LifetimeId.HasValue)
      {
        lifetime = (CardLifetime)command.LifetimeId;
      }

      MeasurementClass? measurmentClass = null;
      if (command.MeasurmentClassId.HasValue)
      {
        measurmentClass = (MeasurementClass)command.MeasurmentClassId;
      }

      Cargo cargo = null;
      if (command.CargoId.HasValue)
      {
        cargo = context.Cargoes.FirstOrDefault(c => c.Id == command.CargoId);
        if (cargo == null)
        {
          throw new ArgumentException("Could not find cargo with given identifier.");
        }
      }

      Contractor contractor = null;
      if (command.ContractorId.HasValue)
      {
        contractor = context.Contractors.FirstOrDefault(c => c.Id == command.ContractorId);
        if (contractor == null)
        {
          _logger.Error($"Cannot update card contractor because contractor with ID: {command.ContractorId} does not exist. Updated card ID: {card.Id}");
          throw new ArgumentException("Could not find contractor with given identifier.");
        }
      }

      Driver driver = null;
      if (command.DriverId.HasValue)
      {
        driver = context.Drivers.FirstOrDefault(d => d.Id == command.DriverId);
        if (driver == null)
        {
          _logger.Error($"Cannot update card driver because driver with ID: {command.DriverId} does not exist. Updated card ID: {card.Id}");
          throw new ArgumentException("Could not find driver with given identifier.");
        }
      }

      Vehicle vehicle = null;
      if (command.VehicleId.HasValue)
      {
        vehicle = context.Vehicles.FirstOrDefault(v => v.Id == command.VehicleId);
        if (vehicle == null)
        {
          _logger.Error($"Cannot update card vehicle because vehicle with ID: {command.VehicleId} does not exist. Updated card ID: {card.Id}");
          throw new ArgumentException("Could not find vehicle with given identifier.");
        }
      }

      var oldCard = context.Cards.AsNoTracking()
        .Include(c => c.Cargo).Include(c => c.Driver).Include(c => c.Vehicle).Include(c => c.Contractor)
        .FirstOrDefault(c => c.Id == command.CardId);

      card.Code = command.Code;
      card.IsActive = command.IsActive;
      card.Lifetime = lifetime;
      card.MeasurementClass = measurmentClass;
      card.Cargo = cargo;
      card.Contractor = contractor;
      card.Driver = driver;
      card.Vehicle = vehicle;

      context.SaveChanges();

      _dbEventLogger.LogEvent(partnerId, ctx => ctx.Cards, card.Id, card, oldCard);
      _logger.Debug($"Successfully updated card with ID: {card.Id}");

      return card.Id;
    }
  }
}
