﻿using Kalisto.Backend.Operations.Cards.Commands.Interfaces;
using Kalisto.Communication.Core.Messages;

namespace Kalisto.Backend.Operations.Cards.Commands
{
  public class DeleteCardCommand : BackendDbOperationMessage, ICardCommand
  {
    public long CardId { get; set; }
  }
}
