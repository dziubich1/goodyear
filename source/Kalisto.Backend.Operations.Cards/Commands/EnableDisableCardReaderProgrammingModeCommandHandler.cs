﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.Operations.Cards.Services;
using Kalisto.Communication.Core.Messaging;

namespace Kalisto.Backend.Operations.Cards.Commands
{
  public class EnableDisableCardReaderProgrammingModeCommandHandler : CommandHandlerBase<EnableDisableCardReaderProgrammingModeCommand>
  {
    private readonly ICardService _cardService;

    public EnableDisableCardReaderProgrammingModeCommandHandler(ICardService cardService)
    {
      _cardService = cardService;
    }

    public override object Execute(IDbContext context, EnableDisableCardReaderProgrammingModeCommand command, string partnerId)
    {
      if (command.ProgrammingModeEnabled)
        _cardService.EnableProgrammingMode();
      else _cardService.DisableProgrammingMode();

      return command.ProgrammingModeEnabled;
    }
  }
}
