﻿using Kalisto.Backend.Operations.Cards.Commands.Interfaces;
using Kalisto.Communication.Core.Messages;

namespace Kalisto.Backend.Operations.Cards.Commands
{
  public class CreateCardCommand : BackendDbOperationMessage, ICardCommand
  {
    public string Code  { get; set; }
  }
}
