﻿using Kalisto.Communication.Core.Messages;

namespace Kalisto.Backend.Operations.Cards.Commands.Interfaces
{
  public interface ICardCommand : IAssociatedCommand
  {
  }
}
