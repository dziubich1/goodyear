﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.DataAccess.Model.Logging;
using Kalisto.Backend.Operations.Cards.Exceptions;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kalisto.Backend.Operations.Cards.Commands
{
  public class CreateCardCommandHandler : CommandHandlerBase<CreateCardCommand>
  {
    private readonly IDbInsertEventLogger _dbEventLogger;
    private readonly ILogger _logger;

    public CreateCardCommandHandler(IDbInsertEventLogger dbEventLogger, ILogger logger)
    {
      _dbEventLogger = dbEventLogger;
      _logger = logger;
    }

    public override object Execute(IDbContext context, CreateCardCommand command, string partnerId)
    {
      var maxCardNumber = 0;
      if (context.Cards.Any())
      {
        maxCardNumber = context.Cards.Max(c => c.Number);
      }
      if (context.Cards.Any(c => c.Code == command.Code))
      {
        _logger.Debug($"Cannot create card with code: {command.Code} because it already exists");
        throw new CardExistsException();
      }

      var card = new Card
      {
        Number = maxCardNumber + 1,
        Code = command.Code,
        IsActive = true,
        Lifetime = CardLifetime.Reusable
      };

      context.Cards.Add(card);
      context.SaveChanges();

      _dbEventLogger.LogEvent(partnerId, ctx => ctx.Cards, card.Id, card);
      _logger.Debug($"Successfully created card with ID: {card.Id}");

      return card.Id;
    }
  }
}
