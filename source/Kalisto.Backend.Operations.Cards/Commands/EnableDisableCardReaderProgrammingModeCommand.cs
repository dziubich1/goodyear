﻿using Kalisto.Communication.Core.Messages;

namespace Kalisto.Backend.Operations.Cards.Commands
{
  public class EnableDisableCardReaderProgrammingModeCommand : BackendDbOperationMessage
  {
    public bool ProgrammingModeEnabled { get; set; }
  }
}
