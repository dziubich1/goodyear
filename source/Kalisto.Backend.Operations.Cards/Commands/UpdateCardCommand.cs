﻿using Kalisto.Backend.Operations.Cards.Commands.Interfaces;
using Kalisto.Communication.Core.Messages;

namespace Kalisto.Backend.Operations.Cards.Commands
{
  public class UpdateCardCommand : BackendDbOperationMessage, ICardCommand
  {
    public long CardId { get; set; }

    public string Code { get; set; }

    public long? DriverId { get; set; }

    public long? VehicleId { get; set; }

    public long? CargoId { get; set; }

    public long? ContractorId { get; set; }

    public bool IsActive { get; set; }

    public long? LifetimeId { get; set; }

    public long? MeasurmentClassId { get; set; }
  }
}
