﻿namespace Kalisto.Backend.Operations.Cards.Services
{
  public interface ICardService
  {
    bool ProgrammingModeEnabled { get; }

    void EnableProgrammingMode();

    void DisableProgrammingMode();
  }
}
