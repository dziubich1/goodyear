﻿using Kalisto.Communication.Core.Exceptions;
using System.Runtime.Serialization;

namespace Kalisto.Backend.Operations.Cards.Exceptions
{
  public class CardExistsException : ExceptionBase
  {
    public CardExistsException()
    {
    }

    public CardExistsException(SerializationInfo info, StreamingContext context)
    {
    }

    public override string ExceptionCode => "CARDS_1001";

    public override string Message => GetLocalizedMessage();
  }
}
