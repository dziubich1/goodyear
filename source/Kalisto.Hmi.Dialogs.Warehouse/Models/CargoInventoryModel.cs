﻿namespace Kalisto.Hmi.Dialogs.Warehouse.Models
{
  public class CargoInventoryModel
  {
    public int OrdinalId { get; set; }

    public string Name { get; set; }

    public decimal? Price { get; set; }

    public decimal ActualAmount { get; set; }
  }
}
