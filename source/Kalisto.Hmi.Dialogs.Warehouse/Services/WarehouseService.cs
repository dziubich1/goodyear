﻿using Kalisto.Backend.Operations.Cargoes;
using Kalisto.Backend.Operations.Cargoes.Queries;
using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Frontend.Notifications.Base;
using Kalisto.Hmi.Core.Communication;
using Kalisto.Hmi.Core.Services;
using Kalisto.Hmi.Dialogs.Warehouse.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kalisto.Hmi.Dialogs.Warehouse.Services
{
  public interface IWarehouseService : IBackendOperationService
  {
    Task<RequestResult<List<CargoInventoryModel>>> GetAllCargoesInventory();
  }

  public class WarehouseService : BackendOperationService, IWarehouseService
  {
    public WarehouseService(ClientInfo clientInfo, IMessageBuilder messageBuilder, IFrontendMessageBridge messageBridge, IDialogService dialogService)
      : base(clientInfo, messageBuilder, messageBridge, dialogService)
    {
    }

    public async Task<RequestResult<List<CargoInventoryModel>>> GetAllCargoesInventory()
    {
      var message = MessageBuilder.CreateNew<GetAllCargoesInventoryQuery>()
      .BasedOn(ClientInfo)
      .Build();

      var result = await MessageBridge.SendMessageWithResultAsync<List<CargoInventoryDto>>(message);
      return new RequestResult<List<CargoInventoryModel>>
      {
        Succeeded = result?.Succeeded ?? false,
        Response = result?.Response?.Select((warehouse, index) => new CargoInventoryModel
        {
          OrdinalId = index + 1,
          ActualAmount = warehouse.ActualAmount != null ? warehouse.ActualAmount.Value : 0M,
          Name = warehouse.Name,
          Price = warehouse.Price
        }).ToList() ?? new List<CargoInventoryModel>()
      };
    }
  }
}
