﻿using Kalisto.Configuration.Permissions;
using Kalisto.Hmi.Core;
using Kalisto.Hmi.Core.Commands;
using Kalisto.Hmi.Core.Services;
using Kalisto.Hmi.Dialogs.Warehouse.Models;
using Kalisto.Hmi.Dialogs.Warehouse.Services;
using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Kalisto.Backend.Operations.Cargoes.Commands.Interfaces;
using Kalisto.Backend.Operations.Measurements.Commands.Interfaces;

namespace Kalisto.Hmi.Dialogs.Warehouse.ViewModels
{
  [AddINotifyPropertyChangedInterface]
  public class WarehouseViewModel : DataGridViewModel<CargoInventoryModel>
  {
    private readonly IWarehouseService _warehouseService;

    public override string DialogTitleKey => "Warehouse";

    public ICommand SyncItemsCommand { get; set; }

    public ObservableCollectionEx<CargoInventoryModel> CargoesInventory { get; set; }

    protected override Type[] AssociatedCommandTypes => new[] { typeof(ICargoCommand), typeof(IMeasurementCommand) };

    public WarehouseViewModel(IDialogService dialogService, IAuthorizationService authorizationService, IWarehouseService warehouseService)
      : base(dialogService, authorizationService)
    {
      _warehouseService = warehouseService;

      SyncItemsCommand = new DelegateCommand(SyncWarehouse);

      CargoesInventory = new ObservableCollectionEx<CargoInventoryModel>();
    }

    protected override ObservableCollectionEx<CargoInventoryModel> GetItemsSource()
    {
      return CargoesInventory;
    }

    protected override string GetEditRuleName()
    {
      return null;
    }

    public override async Task RefreshDataAsync()
    {
      await base.RefreshDataAsync();

      await LoadCargoesInventory();
    }

    private async Task LoadCargoesInventory()
    {
      var result = await _warehouseService.GetAllCargoesInventory();
      if (!result.Succeeded)
        return;

      CargoesInventory.Clear();
      CargoesInventory.AddRange(result.Response);
    }

    private async void SyncWarehouse(object obj)
    {
      await LoadCargoesInventory();
    }
  }
}
