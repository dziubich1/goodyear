﻿using Kalisto.Backend.Operations.Vehicles.Commands.Interfaces;
using Kalisto.Configuration.Permissions;
using Kalisto.Hmi.Core;
using Kalisto.Hmi.Core.Commands;
using Kalisto.Hmi.Core.Services;
using Kalisto.Hmi.Dialogs.Common;
using Kalisto.Hmi.Dialogs.Vehicles.Dialogs;
using Kalisto.Hmi.Dialogs.Vehicles.Models;
using Kalisto.Hmi.Dialogs.Vehicles.Services;
using Kalisto.Localization;
using Kalisto.Localization.Messages;
using MahApps.Metro.Controls.Dialogs;
using PropertyChanged;
using System;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Kalisto.Hmi.Dialogs.Vehicles.ViewModels
{
  [AddINotifyPropertyChangedInterface]
  public class VehiclesViewModel : DataGridViewModel<VehicleModel>
  {
    private readonly ILocalizer<Messages> _localizer;

    private readonly IUnitTypeService _unitTypeService;

    private readonly IVehicleService _vehicleService;

    public override string DialogTitleKey => "Vehicles";

    public ICommand AddItemCommand { get; set; }

    public ICommand EditItemCommand { get; set; }

    public ICommand DeleteItemCommand { get; set; }

    public ICommand SyncItemsCommand { get; set; }

    public ObservableCollectionEx<VehicleModel> Vehicles { get; set; }

    public VehicleModel SelectedVehicle { get; set; }

    protected override Type[] AssociatedCommandTypes => new[] { typeof(IVehicleCommand) };

    public VehiclesViewModel(IDialogService dialogService, IAuthorizationService authorizationService, IUnitTypeService unitTypeService, IVehicleService vehicleService)
      : base(dialogService, authorizationService)
    {
      _localizer = new LocalizationManager<Messages>();
      _unitTypeService = unitTypeService;
      _vehicleService = vehicleService;

      AddItemCommand = new DelegateCommand(AddVehicle);
      EditItemCommand = new DelegateCommand(EditItem);
      DeleteItemCommand = new DelegateCommand(DeleteVehicle);
      SyncItemsCommand = new DelegateCommand(SyncVehicles);

      Vehicles = new ObservableCollectionEx<VehicleModel>();
    }

    protected override ObservableCollectionEx<VehicleModel> GetItemsSource()
    {
      return Vehicles;
    }

    protected override string GetEditRuleName()
    {
      return PermissionRules.DictionaryDataManagement;
    }

    public override async Task RefreshDataAsync()
    {
      await base.RefreshDataAsync();

      await LoadVehicles();
    }

    private async Task LoadVehicles()
    {
      var result = await _vehicleService.GetAllVehiclesAsync();
      if (!result.Succeeded)
        return;

      Vehicles.Clear();
      Vehicles.AddRange(result.Response);
    }

    private async void AddVehicle(object parameter)
    {
      await DialogService.ShowModalDialogAsync<AddOrUpdateVehicleModalDialogViewModel>(
      vm =>
      {
        vm.DialogNameKey = "AddVehicle";
      },
      async vm =>
      {
        var result = await _vehicleService.AddVehicleAsync(vm.PlateNumber,
          vm.TareUom?.OriginalValue, vm.MaxVehicleWeightUom?.OriginalValue,
          vm.Driver != null && vm.Driver.Id != -1 ? vm.Driver.Id : (long?)null,
          vm.Contractor != null && vm.Contractor.Id != -1 ? vm.Contractor.Id : (long?)null);
        return result.Succeeded;
      });
    }

    protected override async void EditItem(object parameter)
    {
      var selectedVehicle = (parameter as VehicleModel) ?? SelectedVehicle;
      if (selectedVehicle == null)
      {
        await DialogService.ShowMessageAsync(_localizer.Localize(() => Messages.Error),
          _localizer.Localize(() => Messages.SelectVehicleToUpdate), MessageDialogType.Ok);
        return;
      }

      await DialogService.ShowModalDialogAsync<AddOrUpdateVehicleModalDialogViewModel>(vm =>
      {
        vm.DialogNameKey = "EditVehicle";
        vm.PlateNumber = selectedVehicle.PlateNumber;
        vm.TareValue = selectedVehicle.Tare.DisplayValueUnitless;
        vm.MaxVehicleWeight = selectedVehicle.MaxVehicleWeight.DisplayValueUnitless;
        vm.Driver = selectedVehicle.Driver;
        vm.Contractor = selectedVehicle.Contractor;
        vm.ValidateOnLoad = false;
      },
      async vm =>
      {
        var result = await _vehicleService.UpdateVehicleAsync(selectedVehicle.Id,
          vm.PlateNumber, vm.TareUom?.OriginalValue, vm.MaxVehicleWeightUom?.OriginalValue,
          vm.Driver != null && vm.Driver.Id != -1 ? vm.Driver.Id : (long?)null,
          vm.Contractor != null && vm.Contractor.Id != -1 ? vm.Contractor.Id : (long?)null);
        return result.Succeeded;
      });
    }

    private async void DeleteVehicle(object parameter)
    {
      if (SelectedVehicle == null)
      {
        await DialogService.ShowMessageAsync(_localizer.Localize(() => Messages.Error),
          _localizer.Localize(() => Messages.SelectVehicleToDelete), MessageDialogType.Ok);
        return;
      }

      var dlgResult = await DialogService.ShowMessageAsync(_localizer.Localize(() => Messages.Attention),
        _localizer.Localize(() => Messages.DeleteVehicleQuestion),
        MessageDialogType.YesNo);

      if (dlgResult != MessageDialogResult.Affirmative)
        return;

      await _vehicleService.DeleteVehicleAsync(SelectedVehicle.Id);
    }

    private async void SyncVehicles(object obj)
    {
      await LoadVehicles();
    }
  }
}
