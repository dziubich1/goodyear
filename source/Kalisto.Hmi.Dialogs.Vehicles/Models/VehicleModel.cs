﻿using Kalisto.Hmi.Dialogs.Common;
using Kalisto.Hmi.Dialogs.Common.Models;
using Kalisto.Localization;

namespace Kalisto.Hmi.Dialogs.Vehicles.Models
{
  public class VehicleModel
  {
    public long Id { get; set; }

    public int OrdinalId { get; set; }

    public string PlateNumber { get; set; }

    public UnitOfMeasurement Tare { get; set; }

    public LocalizedDate TareDate { get; set; }

    public UnitOfMeasurement MaxVehicleWeight { get; set; }

    public ReferenceModel Driver { get; set; }

    public ReferenceModel Contractor { get; set; }
  }
}
