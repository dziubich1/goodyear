﻿using System.Collections.Generic;
using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Frontend.Notifications;
using Kalisto.Frontend.Notifications.Enums;
using Kalisto.Hmi.Core;
using Kalisto.Hmi.Core.Commands;
using Kalisto.Hmi.Core.Communication;
using Kalisto.Hmi.Core.Controls;
using Kalisto.Hmi.Core.Services;
using Kalisto.Hmi.Dialogs.Common;
using Kalisto.Hmi.Dialogs.Common.Models;
using Kalisto.Hmi.Dialogs.Contractors.Dialogs;
using Kalisto.Hmi.Dialogs.Contractors.Services;
using Kalisto.Hmi.Dialogs.Dashboard.Services;
using Kalisto.Hmi.Dialogs.Drivers.Dialogs;
using Kalisto.Hmi.Dialogs.Drivers.Services;
using PropertyChanged;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using Kalisto.Hmi.Dialogs.Common.Services;
using Kalisto.Hmi.Dialogs.Dashboard.Models;
using Kalisto.Localization;
using Kalisto.Localization.Messages;

namespace Kalisto.Hmi.Dialogs.Vehicles.Dialogs
{
  [AddINotifyPropertyChangedInterface]
  public class AddOrUpdateVehicleModalDialogViewModel : ModalViewModelBase<AddOrUpdateVehicleModalDialogViewModel>
  {
    private readonly ILocalizer<Messages> _localizer;

    private readonly IUnitTypeService _unitTypeService;

    private readonly IDriverService _driverService;

    private readonly IContractorService _contractorService;

    private readonly IWeighingDeviceService _weighingDeviceService;

    private readonly ReferenceModel _emptyItem;

    public override string DialogNameKey { get; set; }

    public ObservableCollectionEx<ReferenceModel> Drivers { get; set; }

    public ObservableCollectionEx<ReferenceModel> Contractors { get; set; }

    public ICommand AddDriverCommand { get; set; }

    public ICommand AddContractorCommand { get; set; }

    public ICommand DriverNotFoundCommand { get; set; }

    public ICommand ContractorNotFoundCommand { get; set; }

    public WeighingMode WeighingMode { get; set; }

    public WeighingDeviceStatus DeviceStatus { get; set; }

    public ObservableCollectionEx<WeightDevice> DeviceCollection { get; set; }

    public WeightDevice SelectedDevice { get; set; }

    public string PlateNumber { get; set; }

    public string Tare { get; set; }

    public string TareValue { get; set; }

    public UnitOfMeasurement TareUom =>
      decimal.TryParse(Tare, out decimal value) ? _unitTypeService.ConvertBackValue(value) : null;

    public string MaxVehicleWeight { get; set; }

    public UnitOfMeasurement MaxVehicleWeightUom =>
      decimal.TryParse(MaxVehicleWeight, out decimal value) ? _unitTypeService.ConvertBackValue(value) : null;

    public ReferenceModel Driver { get; set; }

    public ReferenceModel Contractor { get; set; }

    public WeighingDeviceInfo Device1Info { get; set; }

    public WeighingDeviceInfo Device2Info { get; set; }

    public bool IsTareVisible { get; set; } = true;

    public Visibility TareVisibility
    {
      get
      {
        if (IsTareVisible)
          return Visibility.Visible;

        return Visibility.Collapsed;
      }
    }

    public AddOrUpdateVehicleModalDialogViewModel(IDialogService dialogService,
      IDriverService driverService, IContractorService contractorService, IDashboardService dashboardService, IWeighingDeviceService weighingDeviceService,
      IAsyncProgressService asyncProgressService, IMessageBuilder messageBuilder,
      IFrontendMessageBridge messageBridge, IUnitTypeService unitTypeService, ClientInfo clientInfo)
      : base(dialogService, asyncProgressService, messageBuilder, messageBridge, clientInfo)
    {
      _localizer = new LocalizationManager<Messages>();
      _unitTypeService = unitTypeService;
      _driverService = driverService;
      _contractorService = contractorService;
      _weighingDeviceService = weighingDeviceService;
      dashboardService.WeighingValueUpdated += OnWeighingValueUpdated;

      _emptyItem = new ReferenceModel() { DisplayValue = "", Id = -1 };

      AddDriverCommand = new DelegateCommand(OpenAddDriverDialog);
      AddContractorCommand = new DelegateCommand(OpenAddContractorDialog);
      DriverNotFoundCommand = new DelegateCommand(OpenDriverNotFoundDialog);
      ContractorNotFoundCommand = new DelegateCommand(OpenContractorNotFoundDialog);
      Drivers = new ObservableCollectionEx<ReferenceModel>();
      Contractors = new ObservableCollectionEx<ReferenceModel>();
      DeviceCollection = new ObservableCollectionEx<WeightDevice>();
      DeviceStatus = WeighingDeviceStatus.NotStable;
    }

    protected override async void OnViewLoaded(object view)
    {
      base.OnViewLoaded(view);

      await LoadDrivers();
      await LoadContractors();
      await LoadDeviceConfiguration();
      var devices = GetWeighingDevices();

      SetWeighingDevices(devices);
      SetSelectedItemsReferences();
    }

    private async Task LoadDeviceConfiguration()
    {
      // weight metering devices config
      var weightConfigs = await _weighingDeviceService.GetWeighingDevicesAsync();

      if (!weightConfigs.Any())
        return;

      var weight1 = weightConfigs.ElementAtOrDefault(0);
      if (weight1 != null)
      {
        Device1Info = new WeighingDeviceInfo
        {
          Id = weight1.Id,
          Name = weight1.Name,
          DeviceStatus = WeighingDeviceStatus.Unknown
        };
      }

      var weight2 = weightConfigs.ElementAtOrDefault(1);
      if (weight2 != null)
      {
        Device2Info = new WeighingDeviceInfo
        {
          Id = weight2.Id,
          Name = weight2.Name,
          DeviceStatus = WeighingDeviceStatus.Unknown
        };
      }
    }

    private IList<WeighingDeviceInfo> GetWeighingDevices()
    {
      var list = new List<WeighingDeviceInfo>();
      if (Device1Info != null)
        list.Add(Device1Info);
      if (Device2Info != null)
        list.Add(Device2Info);

      return list;
    }

    private void SetWeighingDevices(IList<WeighingDeviceInfo> deviceInfo)
    {
      if (!deviceInfo.Any())
        return;

      DeviceCollection.AddRange(deviceInfo.Select(c => new WeightDevice
      {
        Id = c.Id,
        Name = c.Name
      }));

      SelectedDevice = DeviceCollection.FirstOrDefault();
    }

    private void SetSelectedItemsReferences()
    {
      if (Driver != null)
        Driver = Drivers.FirstOrDefault(d => d.Id == Driver.Id);
      else
        Driver = Drivers.FirstOrDefault();

      if (Contractor != null)
        Contractor = Contractors.FirstOrDefault(c => c.Id == Contractor.Id);
      else
        Contractor = Contractors.FirstOrDefault();
    }

    private async Task LoadDrivers()
    {
      var result = await _driverService.GetAllDriversAsync();
      if (!result.Succeeded)
        return;

      var mappedDrivers = result.Response.Select(d => new ReferenceModel()
      {
        Id = d.Id,
        DisplayValue = d.Identifier
      });
      Drivers.Clear();
      Drivers.Add(_emptyItem);
      Drivers.AddRange(mappedDrivers);
    }

    private async Task LoadContractors()
    {
      var result = await _contractorService.GetAllContractorsAsync();
      if (!result.Succeeded)
        return;

      var mappedContractors = result.Response.Select(d => new ReferenceModel()
      {
        Id = d.Id,
        DisplayValue = d.Name
      });
      Contractors.Clear();
      Contractors.Add(_emptyItem);
      Contractors.AddRange(mappedContractors);
    }

    private async void OpenAddDriverDialog(object parameter)
    {
      long driverId = -1;
      var dlg = await DialogService.ShowModalDialogAsync<AddOrUpdateDriverModalDialogViewModel>(
        vm =>
        {
          vm.DialogNameKey = "AddDriver";
        },
        async vm =>
        {
          var result = await _driverService.AddDriverAsync(vm.Identifier, vm.PhoneNumber);

          if (result.Succeeded)
            driverId = result.Response;

          return result.Succeeded;
        });

      if (!dlg.Result.HasValue || !dlg.Result.Value)
      {
        return;
      }
      await LoadDrivers();

      Driver = Drivers.FirstOrDefault(c => c.Id == driverId);
    }

    private async void OpenAddContractorDialog(object parameter)
    {
      long contractorId = -1;
      var dlg = await DialogService.ShowModalDialogAsync<AddOrUpdateContractorModalDialogViewModel>(
      vm =>
      {
        vm.DialogNameKey = "AddContractor";
      },
      async vm =>
      {
        var result = await _contractorService.AddContractorAsync(vm.Name,
          vm.Address, vm.PostalCode, vm.City, vm.TaxId);

        if (result.Succeeded)
          contractorId = result.Response;

        return result.Succeeded;
      });

      if (!dlg.Result.HasValue || !dlg.Result.Value)
        return;

      await LoadContractors();

      Contractor = Contractors.FirstOrDefault(c => c.Id == contractorId);
    }

    private async void OpenDriverNotFoundDialog(object parameter)
    {
      await DialogService.ShowMessageAsync(_localizer.Localize(() => Messages.Error),
        _localizer.Localize(() => Messages.NoDriverFound), MessageDialogType.Ok);
    }

    private async void OpenContractorNotFoundDialog(object parameter)
    {
      await DialogService.ShowMessageAsync(_localizer.Localize(() => Messages.Error),
        _localizer.Localize(() => Messages.NoContractorFound), MessageDialogType.Ok);
    }

    protected override void DefineValidationRules()
    {
      InvalidateProperty(() => PlateNumber).When(() => string.IsNullOrEmpty(PlateNumber))
        .WithMessageKey("Required").ButIgnoreOnLoad();

      InvalidateProperty(() => PlateNumber).When(() => PlateNumber.Length < 3)
        .WithMessageKey("TooShort").ButIgnoreOnLoad();

      InvalidateProperty(() => Tare).When(() => !IsDecimalOrNull(Tare))
        .WithMessageKey("InvalidNullableNumber").ButIgnoreOnLoad();

      InvalidateProperty(() => Tare).When(() => !IsGreaterOrEqualThan(Tare, 0) && !string.IsNullOrEmpty(Tare))
        .WithMessageKey("MustBeGreaterOrEqualThan", 0).ButIgnoreOnLoad();

      InvalidateProperty(() => MaxVehicleWeight).When(() => !IsDecimalOrNull(MaxVehicleWeight))
        .WithMessageKey("InvalidNullableNumber").ButIgnoreOnLoad();

      InvalidateProperty(() => MaxVehicleWeight).When(() => !IsGreaterOrEqualThan(MaxVehicleWeight, 0) && !string.IsNullOrEmpty(MaxVehicleWeight))
        .WithMessageKey("MustBeGreaterOrEqualThan", 0).ButIgnoreOnLoad();
    }

    public void OnWeighingModeChanged()
    {
      Tare = TareValue;
      if (WeighingMode == WeighingMode.Manual)
        DeviceStatus = WeighingDeviceStatus.Stable;
      else
      {
        DeviceStatus = WeighingDeviceStatus.NotStable;
        Tare = string.Empty;
      }
    }

    public void OnTareValueChanged()
    {
      if (WeighingMode == WeighingMode.Manual)
        Tare = TareValue;
    }

    public void OnSelectedDeviceChanged()
    {
      if (WeighingMode == WeighingMode.Manual)
        return;

      DeviceStatus = WeighingDeviceStatus.NotStable;
      Tare = string.Empty;
    }

    private void OnWeighingValueUpdated(WeighingValueUpdatedNotification notification)
    {
      if (WeighingMode == WeighingMode.Manual)
        return;

      if (SelectedDevice == null || SelectedDevice.Id != notification.DeviceId)
        return;

      DeviceStatus = notification.Status;
      Tare = new UnitOfMeasurement((decimal)notification.Value, _unitTypeService).DisplayValueUnitless;
    }
  }
}
