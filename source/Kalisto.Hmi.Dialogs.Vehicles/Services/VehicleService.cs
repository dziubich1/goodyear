﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.Operations.Vehicles.Commands;
using Kalisto.Backend.Operations.Vehicles.Queries;
using Kalisto.Communication.Core.Messages;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Hmi.Core.Communication;
using Kalisto.Hmi.Core.Services;
using Kalisto.Hmi.Dialogs.Vehicles.Models;
using Kalisto.Localization;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Kalisto.Hmi.Dialogs.Common;
using Kalisto.Hmi.Dialogs.Common.Models;

namespace Kalisto.Hmi.Dialogs.Vehicles.Services
{
  public interface IVehicleService : IBackendOperationService
  {
    Task<RequestResult<List<VehicleModel>>> GetAllVehiclesAsync();

    Task<RequestResult<long>> AddVehicleAsync(string plateNumber, decimal? tare,
      decimal? maxVehicleWeight, long? driverId, long? contractorId);

    Task<RequestResult<long>> UpdateVehicleAsync(long id, string plateNumber, decimal? tare,
      decimal? maxVehicleWeight, long? driverId, long? contractorId);

    Task<RequestResult<long>> UpdateVehicleTareAsync(long id, decimal tare);

    Task<RequestResult<long>> DeleteVehicleAsync(long id);
  }

  public class VehicleService : BackendOperationService, IVehicleService
  {
    private readonly IUnitTypeService _unitTypeService;

    public VehicleService(ClientInfo clientInfo, IMessageBuilder messageBuilder, IFrontendMessageBridge messageBridge, IDialogService dialogService, IUnitTypeService unitTypeService)
      : base(clientInfo, messageBuilder, messageBridge, dialogService)
    {
      _unitTypeService = unitTypeService;
    }

    public async Task<RequestResult<List<VehicleModel>>> GetAllVehiclesAsync()
    {
      var message = MessageBuilder.CreateNew<GetAllVehiclesQuery>()
        .BasedOn(ClientInfo)
        .Build();

      var result = await MessageBridge.SendMessageWithResultAsync<List<Vehicle>>(message);
      return new RequestResult<List<VehicleModel>>
      {
        Succeeded = result?.Succeeded ?? false,
        Response = result?.Response?.Select((v, index) => new VehicleModel
        {
          Id = v.Id,
          OrdinalId = index + 1,
          PlateNumber = v.PlateNumber,
          Tare = new UnitOfMeasurement(v.Tare, _unitTypeService),
          TareDate = new LocalizedDate(v.TareDateUtc?.ToLocalTime()),
          MaxVehicleWeight = new UnitOfMeasurement(v.MaxVehicleWeight, _unitTypeService),
          Driver = v.Driver != null ? new ReferenceModel()
          {
            Id = v.Driver.Id,
            DisplayValue = v.Driver.Identifier
          } : null,
          Contractor = v.Contractor != null ? new ReferenceModel()
          {
            Id = v.Contractor.Id,
            DisplayValue = v.Contractor.Name
          } : null
        }).ToList() ?? new List<VehicleModel>()
      };
    }

    public async Task<RequestResult<long>> AddVehicleAsync(string plateNumber, decimal? tare,
      decimal? maxVehicleWeight, long? driverId, long? contractorId)
    {
      var message = MessageBuilder.CreateNew<CreateVehicleCommand>()
        .BasedOn(ClientInfo)
        .WithProperty(v => v.PlateNumber, plateNumber)
        .WithProperty(v => v.Tare, tare)
        .WithProperty(v => v.MaxVehicleWeight, maxVehicleWeight)
        .WithProperty(v => v.DriverId, driverId)
        .WithProperty(v => v.ContractorId, contractorId)
        .Build();

      var result = await MessageBridge.SendMessageWithResultAsync<long>(message);
      return result ?? RequestResult<long>.Default;
    }

    public async Task<RequestResult<long>> UpdateVehicleAsync(long id, string plateNumber, decimal? tare,
      decimal? maxVehicleWeight, long? driverId, long? contractorId)
    {
      var message = MessageBuilder.CreateNew<UpdateVehicleCommand>()
        .BasedOn(ClientInfo)
        .WithProperty(v => v.VehicleId, id)
        .WithProperty(v => v.PlateNumber, plateNumber)
        .WithProperty(v => v.Tare, tare)
        .WithProperty(v => v.MaxVehicleWeight, maxVehicleWeight)
        .WithProperty(v => v.DriverId, driverId)
        .WithProperty(v => v.ContractorId, contractorId)
        .Build();

      var result = await MessageBridge.SendMessageWithResultAsync<long>(message);
      return result ?? RequestResult<long>.Default;
    }

    public async Task<RequestResult<long>> UpdateVehicleTareAsync(long id, decimal tare)
    {
      var message = MessageBuilder.CreateNew<UpdateVehicleTareCommand>()
        .BasedOn(ClientInfo)
        .WithProperty(v => v.VehicleId, id)
        .WithProperty(v => v.Tare, tare)
        .Build();

      var result = await MessageBridge.SendMessageWithResultAsync<long>(message);
      return result ?? RequestResult<long>.Default;
    }

    public async Task<RequestResult<long>> DeleteVehicleAsync(long id)
    {
      var message = MessageBuilder.CreateNew<DeleteVehicleCommand>()
        .BasedOn(ClientInfo)
        .WithProperty(v => v.VehicleId, id)
        .Build();

      var result = await MessageBridge.SendMessageWithResultAsync<long>(message);
      return result ?? RequestResult<long>.Default;
    }
  }
}
