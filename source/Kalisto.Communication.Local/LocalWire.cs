﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Kalisto.Communication.Core.Communication;

namespace Kalisto.Communication.Local
{
  public class LocalWire
  {
    private  readonly List<ReceiverProcessor> _receivers = new List<ReceiverProcessor>();

    public void SendMessage(string queueName, string message)
    {
      var receiverProcessor = _receivers.FirstOrDefault(c => c.Receiver.QueueName == queueName);
      if (receiverProcessor == null)
        return;

      receiverProcessor.ProcessMessage(message);
    }

    public void InitializeReceiver(IMessageReceiver messageReceiver)
    {
      if (_receivers.Any(c => c.Receiver.QueueName == messageReceiver.QueueName))
        return;

      var proc = new ReceiverProcessor
      {
        Receiver = (MessageLocalReceiver)messageReceiver
      };

      _receivers.Add(proc);

      proc.StartProcessing();
    }

    public void DisposeReceiver(IMessageReceiver messageReceiver)
    {
      var proc = _receivers.FirstOrDefault(c => ReferenceEquals(c.Receiver, messageReceiver));
      if (proc == null)
        return;

      _receivers.Remove(proc);
    }

    private class ReceiverProcessor
    {
      private readonly BlockingCollection<string> _messages = new BlockingCollection<string>();

      public MessageLocalReceiver Receiver { get; set; }

      private Task _processingTask;

      private readonly CancellationTokenSource _tokenSource = new CancellationTokenSource();

      public void ProcessMessage(string message)
      {
        _messages.Add(message);
      }

      public void StartProcessing()
      {
        _processingTask = Task.Factory.StartNew(() =>
        {
          while (true)
          {
            var message = _messages.Take(_tokenSource.Token);
            if (message != null)
            {
              Receiver.ProcessReceivedMessage(message);
            }

            if (_tokenSource.Token.IsCancellationRequested)
            {
              break;
            }
          }
        });
      }

      public void Dispose()
      {
        _tokenSource.Cancel();
      }
    }
  }

  public class MessageLocalSender : IMessageSender
  {
    public string QueueName { get; private set; }

    private readonly LocalWire _wire;

    public MessageLocalSender(LocalWire wire)
    {
      _wire = wire;
    }

    public void Dispose()
    {
    }

    public void Initialize(string topic)
    {
      QueueName = topic;
    }

    public void SendMessage(string message)
    {
      _wire.SendMessage(QueueName, message);
    }
  }

  public class MessageLocalReceiver : IMessageReceiver
  {
    public string QueueName { get; private set; }

    public event Action<string, string> OnMessageReceived;

    private readonly LocalWire _wire;

    public MessageLocalReceiver(LocalWire wire)
    {
      _wire = wire;
    }

    public void Dispose()
    {
      _wire.DisposeReceiver(this);
    }

    public void Initialize(string topic)
    {
      QueueName = topic;

      _wire.InitializeReceiver(this);
    }

    public void ProcessReceivedMessage(string message)
    {
      OnMessageReceived?.Invoke(QueueName, message);
    }
  }
}
