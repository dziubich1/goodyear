﻿using System.Linq;
using Kalisto.Backend.DataAccess.Effort;
using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.Operations.Shell.Commands;
using Kalisto.Communication.Core.Logging;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Communication.Core.Registries;
using Kalisto.Core.Utils;
using Kalisto.Configuration.Permissions;
using Kalisto.Licensing;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json;
using Kalisto.Logging;

namespace Kalisto.Backend.Operations.Shell.UnitTests
{
  [TestClass]
  public class LoginCommandHandlerTests
  {
    private IDbContext _dbContext;

    private readonly string partnerId = "some_partner_id";

    private PermissionRules _permissionRulesConfiguration;

    private Mock<IPartnerRegistry> _partnerRegistryMock;

    private Mock<ISystemEventLogger> _systemEventLoggerMock;

    private Mock<ILicenseInfo> _licenseInfoMock;

    // component under tests
    private LoginCommandHandler _loginCommandHandler;

    [TestInitialize]
    public void Initialize()
    {
      var effortConnection = Effort.DbConnectionFactory.CreateTransient();
      _dbContext = new EffortDbContext(effortConnection);

      _permissionRulesConfiguration = PermissionRules.Default;
      _partnerRegistryMock = new Mock<IPartnerRegistry>();
      _systemEventLoggerMock = new Mock<ISystemEventLogger>();
      _licenseInfoMock = new Mock<ILicenseInfo>();
      _licenseInfoMock.Setup(c => c.GetLicenseInfo()).Returns(new LicenseInfo
      {
        Type = LicenseType.Full,
        StationCount = 10
      });

      var logger = new Mock<ILogger>();

      _loginCommandHandler = new LoginCommandHandler(_permissionRulesConfiguration, _partnerRegistryMock.Object,
        _systemEventLoggerMock.Object, _licenseInfoMock.Object, logger.Object);

      InitializeUserRoles();
    }

    [TestMethod]
    public void Execute_ShouldReturnNull_WhenUserWithGivenNameWasNotFound()
    {
      // Arrange
      var notExistingUsername = "not_existing_username";
      var loginCommand = new LoginCommand
      {
        Username = notExistingUsername
      };

      // Act
      var result = _loginCommandHandler.Execute(_dbContext, loginCommand, partnerId);

      // Assert
      Assert.AreEqual(null, result);
    }

    [TestMethod]
    public void Execute_ShouldReturnNull_WhenPasswordForGivenUserWasIncorrect()
    {
      // Setup
      var username = "username";
      _dbContext.Users.Add(new User
      {
        Name = username,
        PasswordHash = PasswordHasher.CreateHash("test_1"),
        Role = _dbContext.UserRoles.FirstOrDefault()
      });
      _dbContext.SaveChanges();

      // Arrange
      var loginCommand = new LoginCommand
      {
        Username = username,
        Password = "test_2"
      };

      // Act
      var result = _loginCommandHandler.Execute(_dbContext, loginCommand, partnerId);

      // Assert
      Assert.AreEqual(null, result);
    }

    [TestMethod]
    public void Execute_ShouldReturnUserInfo()
    {
      // Setup
      var username = "username";
      var user = new User
      {
        Name = username,
        PasswordHash = PasswordHasher.CreateHash("test"),
        Role = _dbContext.UserRoles.FirstOrDefault()
      };
      _dbContext.Users.Add(user);
      _dbContext.SaveChanges();

      // Arrange
      var loginCommand = new LoginCommand
      {
        Username = username,
        Password = "test"
      };

      // Act
      var result = _loginCommandHandler.Execute(_dbContext, loginCommand, partnerId) as UserInfo;

      // Assert
      Assert.AreNotEqual(null, result);
      Assert.AreEqual(user.Name, result?.Username);
      Assert.AreEqual(user.Role?.Type, result?.UserRole);
      Assert.AreSame(_permissionRulesConfiguration.UserPermissions
          .FirstOrDefault(c => c.UserRole == user.Role?.Type)?.Permissions,
        result?.Permissions);
    }

    [TestMethod]
    public void Execute_ShouldReturnUserInfo_WhichIsSerializable()
    {
      // Setup
      var username = "username";
      var user = new User
      {
        Name = username,
        PasswordHash = PasswordHasher.CreateHash("test"),
        Role = _dbContext.UserRoles.FirstOrDefault()
      };
      _dbContext.Users.Add(user);
      _dbContext.SaveChanges();

      // Arrange
      var loginCommand = new LoginCommand
      {
        Username = username,
        Password = "test"
      };

      // Act
      var result = _loginCommandHandler.Execute(_dbContext, loginCommand, partnerId) as UserInfo;
      var serializedResult = MessageSerializer.Serialize(result);
      var deserializedResult = JsonConvert.DeserializeObject<UserInfo>(serializedResult, new JsonSerializerSettings
      {
        TypeNameHandling = TypeNameHandling.All
      });

      // Assert
      Assert.AreNotEqual(null, deserializedResult);
      Assert.AreEqual(user.Name, deserializedResult?.Username);
      Assert.AreEqual(user.Role?.Type, deserializedResult?.UserRole);
      Assert.AreEqual(_permissionRulesConfiguration.UserPermissions
          .FirstOrDefault(c => c.UserRole == user.Role?.Type)?.Permissions.Count,
        deserializedResult?.Permissions.Count);
    }

    private void InitializeUserRoles()
    {
      _dbContext.UserRoles.Add(new UserRole { Type = 0 });
      _dbContext.UserRoles.Add(new UserRole { Type = 1 });
      _dbContext.UserRoles.Add(new UserRole { Type = 2 });
      _dbContext.SaveChanges();
    }
  }
}
