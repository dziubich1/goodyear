﻿using Kalisto.Backend.DataAccess.Effort;
using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.DataAccess.Model.Logging;
using Kalisto.Backend.Operations.Cards.Commands;
using Kalisto.Backend.Operations.Cards.Exceptions;
using Kalisto.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kalisto.Backend.Operations.Cards.UnitTests
{
  [TestClass]
  public class UpdateCardCommandHandlerTests
  {
    private IDbContext _dbContext;

    private readonly string _partnerId = "unknown";

    private Mock<IDbUpdateEventLogger> _dbEventLoggerMock;

    // component under tests
    private UpdateCardCommandHandler _updateCardCommandHandler;

    [TestInitialize]
    public void Initialize()
    {
      var effortConnection = Effort.DbConnectionFactory.CreateTransient();
      _dbContext = new EffortDbContext(effortConnection);

      _dbEventLoggerMock = new Mock<IDbUpdateEventLogger>();

      var logger = new Mock<ILogger>();

      _updateCardCommandHandler = new UpdateCardCommandHandler(_dbEventLoggerMock.Object, logger.Object);
    }

    [TestCleanup]
    public void CleanUp()
    {
      _dbContext.Dispose();
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void Execute_ShouldThrowArgumentException_WhenCardWithGivenIdDoesNotExist()
    {
      // Arrange
      var notExisitingCardId = 666;
      var updateCardCommand = new UpdateCardCommand
      {
        CardId = notExisitingCardId
      };

      // Act & Assert
      try
      {
        _updateCardCommandHandler.Execute(_dbContext, updateCardCommand, _partnerId);
      }
      catch (Exception e)
      {
        Assert.AreEqual(true, e.Message.ToLower().Contains("card"));
        throw e;
      }
    }

    [TestMethod]
    public void Execute_ShouldNotUpdateCargo_WhenCargoWithGivenIdDoesNotExist()
    {
      // Setup
      var existingCargo = _dbContext.Cargoes.Add(new Cargo());
      var card = _dbContext.Cards.Add(new Card { Cargo = existingCargo });
      _dbContext.SaveChanges();
      var notExistingCargoId = 5;

      // Arrange
      var updateCardCommand = new UpdateCardCommand
      {
        CardId = card.Id,
        CargoId = notExistingCargoId
      };

      // Act & Assert
      try
      {
        _updateCardCommandHandler.Execute(_dbContext, updateCardCommand, _partnerId);
      }
      catch
      {
        Assert.AreEqual(existingCargo, _dbContext.Cards.Include(c => c.Cargo).FirstOrDefault(c => c.Id == 1).Cargo);
      }
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void Execute_ShouldThrowArgumentException_WhenCargoWithGivenIdDoesNotExist()
    {
      // Setup
      var card = _dbContext.Cards.Add(new Card());
      _dbContext.SaveChanges();
      var notExistingCargoId = 5;

      // Arrange
      var updateCardCommand = new UpdateCardCommand
      {
        CardId = card.Id,
        CargoId = notExistingCargoId
      };

      // Act & Assert
      try
      {
        _updateCardCommandHandler.Execute(_dbContext, updateCardCommand, _partnerId);
      }
      catch (Exception e)
      {
        Assert.AreEqual(true, e.Message.ToLower().Contains("cargo"));
        throw e;
      }
    }

    [TestMethod]
    public void Execute_ShouldNotUpdateContractor_WhenContractorWithGivenIdDoesNoteExist()
    {
      // Setup
      var existingContractor = _dbContext.Contractors.Add(new Contractor());
      var card = _dbContext.Cards.Add(new Card { Contractor = existingContractor });
      _dbContext.SaveChanges();
      var notExistingContractorId = 5;

      // Arrange
      var updateCardCommand = new UpdateCardCommand
      {
        CardId = card.Id,
        ContractorId = notExistingContractorId
      };

      // Act & Assert
      try
      {
        _updateCardCommandHandler.Execute(_dbContext, updateCardCommand, _partnerId);
      }
      catch
      {
        Assert.AreEqual(existingContractor, _dbContext.Cards.Include(c => c.Contractor).FirstOrDefault(c => c.Id == 1).Contractor);
      }
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void Execute_ShouldThrowArgumentException_WhenContractorWithGivenIdDoesNotExist()
    {
      // Setup
      var card = _dbContext.Cards.Add(new Card());
      _dbContext.SaveChanges();
      var notExistingContractorId = 5;

      // Arrange
      var updateCardCommand = new UpdateCardCommand
      {
        CardId = card.Id,
        ContractorId = notExistingContractorId
      };

      // Act & Assert
      try
      {
        _updateCardCommandHandler.Execute(_dbContext, updateCardCommand, _partnerId);
      }
      catch (Exception e)
      {
        Assert.AreEqual(true, e.Message.ToLower().Contains("contractor"));
        throw e;
      }
    }

    [TestMethod]
    public void Execute_ShouldNotUpdateDriver_WhenDriverWithGivenIdDoesNotExist()
    {
      // Setup
      var existingDriver = _dbContext.Drivers.Add(new Driver());
      var card = _dbContext.Cards.Add(new Card { Driver = existingDriver });
      _dbContext.SaveChanges();
      var notExistingDriverId = 5;

      // Arrange
      var updateCardCommand = new UpdateCardCommand
      {
        CardId = card.Id,
        DriverId = notExistingDriverId
      };

      // Act & Assert
      try
      {
        _updateCardCommandHandler.Execute(_dbContext, updateCardCommand, _partnerId);
      }
      catch
      {
        Assert.AreEqual(existingDriver, _dbContext.Cards.Include(c => c.Driver).FirstOrDefault(c => c.Id == 1).Driver);
      }
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void Execute_ShouldThrowArgumentException_WhenDriverWithGivenIdDoesNotExist()
    {
      // Setup
      var card = _dbContext.Cards.Add(new Card());
      _dbContext.SaveChanges();
      var notExistingDriverId = 5;

      // Arrange
      var updateCardCommand = new UpdateCardCommand
      {
        CardId = card.Id,
        DriverId = notExistingDriverId
      };

      // Act & Assert
      try
      {
        _updateCardCommandHandler.Execute(_dbContext, updateCardCommand, _partnerId);
      }
      catch (Exception e)
      {
        Assert.AreEqual(true, e.Message.ToLower().Contains("driver"));
        throw e;
      }
    }

    [TestMethod]
    public void Execute_ShouldNotUpdateVehicle_WhenVehicleWithGivenIdDoesNotExist()
    {
      // Setup
      var existingVehicle = _dbContext.Vehicles.Add(new Vehicle());
      var card = _dbContext.Cards.Add(new Card { Vehicle = existingVehicle });
      _dbContext.SaveChanges();
      var notExistingVehicleId = 5;

      // Arrange
      var updateCardCommand = new UpdateCardCommand
      {
        CardId = card.Id,
        VehicleId = notExistingVehicleId
      };

      // Act & Assert
      try
      {
        _updateCardCommandHandler.Execute(_dbContext, updateCardCommand, _partnerId);
      }
      catch
      {
        Assert.AreEqual(existingVehicle, _dbContext.Cards.Include(c => c.Vehicle).FirstOrDefault(c => c.Id == 1).Vehicle);
      }
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void Execute_ShouldThrowArgumentException_WhenVehicleWithGivenIdDoesNotExist()
    {
      // Setup
      var card = _dbContext.Cards.Add(new Card());
      _dbContext.SaveChanges();
      var notExistingVehicleId = 5;

      // Arrange
      var updateCardCommand = new UpdateCardCommand
      {
        CardId = card.Id,
        VehicleId = notExistingVehicleId
      };

      // Act & Assert
      try
      {
        _updateCardCommandHandler.Execute(_dbContext, updateCardCommand, _partnerId);
      }
      catch (Exception e)
      {
        Assert.AreEqual(true, e.Message.ToLower().Contains("vehicle"));
        throw e;
      }
    }

    [TestMethod]
    public void Execute_ShouldNotUpdateCard_WhenCodeOfCurrentCardHasChangedButCardWithSameCodeAlreadyExists()
    {
      // Setup
      var testCardCode = "test";
      var testCard = _dbContext.Cards.Add(new Card { Code = testCardCode });

      var currentCardCode = "current";
      var currentCard = _dbContext.Cards.Add(new Card { Code = currentCardCode });
      _dbContext.SaveChanges();

      // Arrange
      var command = new UpdateCardCommand
      {
        CardId = currentCard.Id,
        Code = testCardCode
      };

      try
      {
        // Act
        _updateCardCommandHandler.Execute(_dbContext, command, _partnerId);
      }
      catch
      {
        // Assert
        Assert.AreEqual(currentCardCode, _dbContext.Cards.FirstOrDefault(c => c.Id == currentCard.Id).Code);
      }
    }

    [TestMethod]
    [ExpectedException(typeof(CardExistsException))]
    public void Execute_ShouldThrowCardExistsException_WhenCodeOfCurrentCardHasChangedButCardWithSameCodeAlreadyExists()
    {
      // Setup
      var testCardCode = "test";
      var testCard = _dbContext.Cards.Add(new Card { Code = testCardCode });

      var currentCardCode = "current";
      var currentCard = _dbContext.Cards.Add(new Card { Code = currentCardCode });
      _dbContext.SaveChanges();

      // Arrange
      var command = new UpdateCardCommand
      {
        CardId = currentCard.Id,
        Code = testCardCode
      };

      // Act & Assert
      _updateCardCommandHandler.Execute(_dbContext, command, _partnerId);
    }

    [TestMethod]
    public void Execute_ShouldUpdateCard_WithNullLifetime()
    {
      // Setup
      var existingDriver = _dbContext.Drivers.Add(new Driver());
      var existingContractor = _dbContext.Contractors.Add(new Contractor());
      var existingCargo = _dbContext.Cargoes.Add(new Cargo());
      var existingVehicle = _dbContext.Vehicles.Add(new Vehicle());
      var card = _dbContext.Cards.Add(new Card { Code = "test" });
      _dbContext.SaveChanges();

      var newCardCode = "new";
      // Arrange
      var updateCardCommand = new UpdateCardCommand
      {
        CardId = card.Id,
        DriverId = existingDriver.Id,
        ContractorId = existingContractor.Id,
        CargoId = existingCargo.Id,
        VehicleId = existingVehicle.Id,
        MeasurmentClassId = (int)MeasurementClass.Income,
        Code = newCardCode
      };

      // Act
      _updateCardCommandHandler.Execute(_dbContext, updateCardCommand, _partnerId);

      // Assert
      var updatedCard = _dbContext.Cards.Include(c => c.Cargo).Include(c => c.Contractor)
          .Include(c => c.Driver).Include(c => c.Vehicle).Single();
      Assert.AreEqual(newCardCode, updatedCard.Code);
      Assert.AreEqual(existingCargo.Id, updatedCard.Cargo.Id);
      Assert.AreEqual(existingContractor.Id, updatedCard.Contractor.Id);
      Assert.AreEqual(existingDriver.Id, updatedCard.Driver.Id);
      Assert.AreEqual(existingVehicle.Id, updatedCard.Vehicle.Id);
      Assert.AreEqual(null, updatedCard.Lifetime);
      Assert.AreEqual(MeasurementClass.Income, updatedCard.MeasurementClass);
    }

    [TestMethod]
    public void Execute_ShouldUpdateCard_WithNullMeasurmentClass()
    {
      // Setup
      var existingDriver = _dbContext.Drivers.Add(new Driver());
      var existingContractor = _dbContext.Contractors.Add(new Contractor());
      var existingCargo = _dbContext.Cargoes.Add(new Cargo());
      var existingVehicle = _dbContext.Vehicles.Add(new Vehicle());
      var card = _dbContext.Cards.Add(new Card { Code = "test" });
      _dbContext.SaveChanges();

      var newCardCode = "new";
      // Arrange
      var updateCardCommand = new UpdateCardCommand
      {
        CardId = card.Id,
        DriverId = existingDriver.Id,
        ContractorId = existingContractor.Id,
        CargoId = existingCargo.Id,
        VehicleId = existingVehicle.Id,
        LifetimeId = (int)CardLifetime.Disposable,
        Code = newCardCode
      };

      // Act
      _updateCardCommandHandler.Execute(_dbContext, updateCardCommand, _partnerId);

      // Assert
      var updatedCard = _dbContext.Cards.Include(c => c.Cargo).Include(c => c.Contractor)
          .Include(c => c.Driver).Include(c => c.Vehicle).Single();
      Assert.AreEqual(newCardCode, updatedCard.Code);
      Assert.AreEqual(existingCargo.Id, updatedCard.Cargo.Id);
      Assert.AreEqual(existingContractor.Id, updatedCard.Contractor.Id);
      Assert.AreEqual(existingDriver.Id, updatedCard.Driver.Id);
      Assert.AreEqual(existingVehicle.Id, updatedCard.Vehicle.Id);
      Assert.AreEqual(CardLifetime.Disposable, updatedCard.Lifetime);
      Assert.AreEqual(null, updatedCard.MeasurementClass);
    }

    [TestMethod]
    public void Execute_ShouldUpdateCard_WithNullDriver()
    {
      // Setup
      var existingContractor = _dbContext.Contractors.Add(new Contractor());
      var existingCargo = _dbContext.Cargoes.Add(new Cargo());
      var existingVehicle = _dbContext.Vehicles.Add(new Vehicle());
      var card = _dbContext.Cards.Add(new Card { Code = "test" });
      _dbContext.SaveChanges();

      var newCardCode = "new";
      // Arrange
      var updateCardCommand = new UpdateCardCommand
      {
        CardId = card.Id,
        ContractorId = existingContractor.Id,
        CargoId = existingCargo.Id,
        VehicleId = existingVehicle.Id,
        MeasurmentClassId = (int)MeasurementClass.Income,
        LifetimeId = (int)CardLifetime.Disposable,
        Code = newCardCode
      };

      // Act
      _updateCardCommandHandler.Execute(_dbContext, updateCardCommand, _partnerId);

      // Assert
      var updatedCard = _dbContext.Cards.Include(c => c.Cargo).Include(c => c.Contractor)
          .Include(c => c.Driver).Include(c => c.Vehicle).Single();
      Assert.AreEqual(newCardCode, updatedCard.Code);
      Assert.AreEqual(existingCargo.Id, updatedCard.Cargo.Id);
      Assert.AreEqual(existingContractor.Id, updatedCard.Contractor.Id);
      Assert.AreEqual(null, updatedCard.Driver);
      Assert.AreEqual(existingVehicle.Id, updatedCard.Vehicle.Id);
      Assert.AreEqual(CardLifetime.Disposable, updatedCard.Lifetime);
      Assert.AreEqual(MeasurementClass.Income, updatedCard.MeasurementClass);
    }

    [TestMethod]
    public void Execute_ShouldUpdateCard_WithNullContractor()
    {
      // Setup
      var existingDriver = _dbContext.Drivers.Add(new Driver());
      var existingCargo = _dbContext.Cargoes.Add(new Cargo());
      var existingVehicle = _dbContext.Vehicles.Add(new Vehicle());
      var card = _dbContext.Cards.Add(new Card { Code = "test" });
      _dbContext.SaveChanges();

      var newCardCode = "new";
      // Arrange
      var updateCardCommand = new UpdateCardCommand
      {
        CardId = card.Id,
        DriverId = existingDriver.Id,
        CargoId = existingCargo.Id,
        VehicleId = existingVehicle.Id,
        MeasurmentClassId = (int)MeasurementClass.Outcome,
        LifetimeId = (int)CardLifetime.Disposable,
        Code = newCardCode
      };

      // Act
      _updateCardCommandHandler.Execute(_dbContext, updateCardCommand, _partnerId);

      // Assert
      var updatedCard = _dbContext.Cards.Include(c => c.Cargo).Include(c => c.Contractor)
          .Include(c => c.Driver).Include(c => c.Vehicle).Single();
      Assert.AreEqual(newCardCode, updatedCard.Code);
      Assert.AreEqual(existingCargo.Id, updatedCard.Cargo.Id);
      Assert.AreEqual(null, updatedCard.Contractor);
      Assert.AreEqual(existingDriver.Id, updatedCard.Driver.Id);
      Assert.AreEqual(existingVehicle.Id, updatedCard.Vehicle.Id);
      Assert.AreEqual(CardLifetime.Disposable, updatedCard.Lifetime);
      Assert.AreEqual(MeasurementClass.Outcome, updatedCard.MeasurementClass);
    }

    [TestMethod]
    public void Execute_ShouldUpdateCard_WithNullCargo()
    {
      // Setup
      var existingDriver = _dbContext.Drivers.Add(new Driver());
      var existingContractor = _dbContext.Contractors.Add(new Contractor());
      var existingVehicle = _dbContext.Vehicles.Add(new Vehicle());
      var card = _dbContext.Cards.Add(new Card { Code = "test" });
      _dbContext.SaveChanges();

      var newCardCode = "new";
      // Arrange
      var updateCardCommand = new UpdateCardCommand
      {
        CardId = card.Id,
        DriverId = existingDriver.Id,
        ContractorId = existingContractor.Id,
        VehicleId = existingVehicle.Id,
        MeasurmentClassId = (int)MeasurementClass.Service,
        LifetimeId = (int)CardLifetime.Disposable,
        Code = newCardCode
      };

      // Act
      _updateCardCommandHandler.Execute(_dbContext, updateCardCommand, _partnerId);

      // Assert
      var updatedCard = _dbContext.Cards.Include(c => c.Cargo).Include(c => c.Contractor)
          .Include(c => c.Driver).Include(c => c.Vehicle).Single();
      Assert.AreEqual(newCardCode, updatedCard.Code);
      Assert.AreEqual(null, updatedCard.Cargo);
      Assert.AreEqual(existingContractor.Id, updatedCard.Contractor.Id);
      Assert.AreEqual(existingDriver.Id, updatedCard.Driver.Id);
      Assert.AreEqual(existingVehicle.Id, updatedCard.Vehicle.Id);
      Assert.AreEqual(CardLifetime.Disposable, updatedCard.Lifetime);
      Assert.AreEqual(MeasurementClass.Service, updatedCard.MeasurementClass);
    }

    [TestMethod]
    public void Execute_ShouldUpdateCard_WithNullVehicle()
    {
      // Setup
      var existingDriver = _dbContext.Drivers.Add(new Driver());
      var existingContractor = _dbContext.Contractors.Add(new Contractor());
      var existingCargo = _dbContext.Cargoes.Add(new Cargo());
      var card = _dbContext.Cards.Add(new Card { Code = "test" });
      _dbContext.SaveChanges();

      var newCardCode = "new";
      // Arrange
      var updateCardCommand = new UpdateCardCommand
      {
        CardId = card.Id,
        DriverId = existingDriver.Id,
        ContractorId = existingContractor.Id,
        CargoId = existingCargo.Id,
        MeasurmentClassId = (int)MeasurementClass.Outcome,
        LifetimeId = (int)CardLifetime.Disposable,
        Code = newCardCode
      };

      // Act
      _updateCardCommandHandler.Execute(_dbContext, updateCardCommand, _partnerId);

      // Assert
      var updatedCard = _dbContext.Cards.Include(c => c.Cargo).Include(c => c.Contractor)
          .Include(c => c.Driver).Include(c => c.Vehicle).Single();
      Assert.AreEqual(newCardCode, updatedCard.Code);
      Assert.AreEqual(existingCargo.Id, updatedCard.Cargo.Id);
      Assert.AreEqual(existingContractor.Id, updatedCard.Contractor.Id);
      Assert.AreEqual(existingDriver.Id, updatedCard.Driver.Id);
      Assert.AreEqual(null, updatedCard.Vehicle);
      Assert.AreEqual(CardLifetime.Disposable, updatedCard.Lifetime);
      Assert.AreEqual(MeasurementClass.Outcome, updatedCard.MeasurementClass);
    }

    [TestMethod]
    public void Execute_ShouldCreateDbUpdateEventLog()
    {
      // Setup
      _dbEventLoggerMock.Setup(c => c.LogEvent(_partnerId, ctx => ctx.Cards,
          It.IsAny<long>(), It.IsAny<Card>()))
          .Verifiable();

      var testCardCode = "test";
      var testCard = _dbContext.Cards.Add(new Card { Code = testCardCode });
      _dbContext.SaveChanges();

      // Arrange
      var newCardCode = "new";
      var updateCardCommand = new UpdateCardCommand
      {
        CardId = testCard.Id,
        Code = newCardCode
      };

      // Act
      _updateCardCommandHandler.Execute(_dbContext, updateCardCommand, _partnerId);

      // Assert
      _dbEventLoggerMock.Verify(c => c.LogEvent(_partnerId, ctx => ctx.Cards,
        It.IsAny<long>(), It.IsAny<Card>(), It.IsAny<Card>()), Times.Once);
    }
  }
}
