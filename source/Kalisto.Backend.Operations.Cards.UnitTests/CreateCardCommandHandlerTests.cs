﻿using System.Collections.Generic;
using System.Linq;
using Kalisto.Backend.DataAccess.Effort;
using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.DataAccess.Model.Logging;
using Kalisto.Backend.Operations.Cards.Commands;
using Kalisto.Backend.Operations.Cards.Exceptions;
using Kalisto.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace Kalisto.Backend.Operations.Cards.UnitTests
{
  [TestClass]
  public class CreateCardCommandHandlerTests
  {
    private IDbContext _dbContext;

    private readonly string _partnerId = "unknown";

    private Mock<IDbInsertEventLogger> _dbEventLoggerMock;

    // component under tests
    private CreateCardCommandHandler _createCardCommandHandler;

    [TestInitialize]
    public void Initialize()
    {
      var effortConnection = Effort.DbConnectionFactory.CreateTransient();
      _dbContext = new EffortDbContext(effortConnection);

      _dbEventLoggerMock = new Mock<IDbInsertEventLogger>();

      var logger = new Mock<ILogger>();

      _createCardCommandHandler = new CreateCardCommandHandler(_dbEventLoggerMock.Object, logger.Object);
    }

    [TestCleanup]
    public void CleanUp()
    {
      _dbContext.Dispose();
    }

    [TestMethod]
    [ExpectedException(typeof(CardExistsException))]
    public void Execute_ShouldThrowCardExistsException_WhenCardWithGivenCodeAlreadyExists()
    {
      // Setup
      var testCardCode = "already exists";
      _dbContext.Cards.Add(new Card { Code = testCardCode, Number = 1 });
      _dbContext.SaveChanges();

      // Arrange
      var createCardCommand = new CreateCardCommand
      {
        Code = testCardCode
      };

      // Act & Assert
      _createCardCommandHandler.Execute(_dbContext, createCardCommand, _partnerId);
    }

    [TestMethod]
    public void Execute_ShouldNotCreateCard_WhenCardWithGivenCodeAlreadyExists()
    {
      // Setup
      var testCardCode = "already exists";
      _dbContext.Cards.Add(new Card { Code = testCardCode, Number = 1 });
      _dbContext.SaveChanges();
      var expectedCardsCount = _dbContext.Cards.Count();

      // Arrange
      var createCardCommand = new CreateCardCommand
      {
        Code = testCardCode
      };

      try
      {
        // Act
        _createCardCommandHandler.Execute(_dbContext, createCardCommand, _partnerId);
      }
      catch
      {
        // Assert
        Assert.AreEqual(expectedCardsCount, _dbContext.Cards.Count());
      }
    }

    [TestMethod]
    public void Execute_ShouldCreateCard_WithGivenCodeAndDefaultValues()
    {
      // Arrange
      var createCardCommand = new CreateCardCommand
      {
        Code = "test"
      };

      // Act
      _createCardCommandHandler.Execute(_dbContext, createCardCommand, _partnerId);

      // Assert
      Assert.AreEqual(1, _dbContext.Cards.Count(), "Cards table hould contain exactly one card");

      var createdCard = _dbContext.Cards.Single();
      Assert.AreEqual(1, createdCard.Id, "Id of created card should be auto generated");
      Assert.AreEqual(1, createdCard.Number, "Number of created card should be auto generated");
      Assert.AreEqual(createCardCommand.Code, createdCard.Code, "Code of created card should be the same like in a command");
      Assert.AreEqual(CardLifetime.Reusable, createdCard.Lifetime, "Lifetime of created card should be Reusable by default");
      Assert.AreEqual(true, createdCard.IsActive, "Created card should be active after creation");
    }

    [TestMethod]
    public void Execute_ShouldIncrementCardNumber()
    {
      // Arrange
      var createCardCommands = new List<CreateCardCommand>();
      var numberOfCards = 4;
      for (int i = 0; i < numberOfCards; i++)
      {
        createCardCommands.Add(new CreateCardCommand
        {
          Code = $"test {i}"
        });
      }

      // Act
      for (int i = 0; i < numberOfCards; i++)
      {
        _createCardCommandHandler.Execute(_dbContext, createCardCommands[i], _partnerId);
      }

      // Assert
      for (int i = 1; i <= numberOfCards; i++)
      {
        var createdCard = _dbContext.Cards.FirstOrDefault(c => c.Id == i);
        Assert.AreEqual(i, createdCard.Number, "Number of created card should be auto generated");
      }
    }

    [TestMethod]
    public void Execute_ShouldCreateDbInsertEventLog()
    {
      // Setup
      _dbEventLoggerMock.Setup(logger => logger.LogEvent(_partnerId, ctx => ctx.Cards,
          It.IsAny<long>(), It.IsAny<Card>()))
        .Verifiable();

      // Arrange
      var createDriverCommand = new CreateCardCommand
      {
        Code = "test"
      };

      // Act
      _createCardCommandHandler.Execute(_dbContext, createDriverCommand, _partnerId);

      // Assert
      _dbEventLoggerMock.Verify(logger => logger.LogEvent(_partnerId, ctx => ctx.Cards,
          It.IsAny<long>(), It.IsAny<Card>()), Times.Once);
    }
  }
}
