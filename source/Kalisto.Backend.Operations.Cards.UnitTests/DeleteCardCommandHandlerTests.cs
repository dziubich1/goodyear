﻿using Kalisto.Backend.DataAccess.Effort;
using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.DataAccess.Model.Logging;
using Kalisto.Backend.Operations.Cards.Commands;
using Kalisto.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Linq;

namespace Kalisto.Backend.Operations.Cards.UnitTests
{
  [TestClass]
  public class DeleteCardCommandHandlerTests
  {
    private IDbContext _dbContext;

    private readonly string _partnerId = "unknown";

    private Mock<IDbDeleteEventLogger> _dbEventLoggerMock;

    // component under tests
    private DeleteCardCommandHandler _deleteCardCommandHandler;

    [TestInitialize]
    public void Initialize()
    {
      var effortConnection = Effort.DbConnectionFactory.CreateTransient();
      _dbContext = new EffortDbContext(effortConnection);

      _dbEventLoggerMock = new Mock<IDbDeleteEventLogger>();

      var logger = new Mock<ILogger>();

      _deleteCardCommandHandler = new DeleteCardCommandHandler(_dbEventLoggerMock.Object, logger.Object);
    }

    [TestCleanup]
    public void CleanUp()
    {
      _dbContext.Dispose();
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void Execute_ShouldThrowArgumentException_WhenCardWthGivenIdDoesNotExist()
    {
      // Setup
      _dbContext.Cards.Add(new Card()); // Returns generated id: 1
      _dbContext.SaveChanges();

      // Arrange
      var deleteCardCommand = new DeleteCardCommand
      {
        CardId = 2 // Not exists
      };

      // Act & Assert
      _deleteCardCommandHandler.Execute(_dbContext, deleteCardCommand, _partnerId);
    }

    [TestMethod]
    public void Execute_ShouldRemoveCard()
    {
      // Setup
      var card =_dbContext.Cards.Add(new Card()); // Returns generated id: 1
      _dbContext.SaveChanges();

      // Arrange
      var deleteCardCommand = new DeleteCardCommand
      {
        CardId = card.Id
      };

      // Act
      _deleteCardCommandHandler.Execute(_dbContext, deleteCardCommand, _partnerId);

      // Assert
      Assert.AreEqual(0, _dbContext.Cards.Count());
    }

    [TestMethod]
    public void Execute_ShouldReturnIdOfDeletedCard()
    {
      // Setup
      var card = _dbContext.Cards.Add(new Card());
      _dbContext.SaveChanges();

      // Arrange
      var deleteCardCommand = new DeleteCardCommand
      {
        CardId = card.Id
      };

      // Act
      var cardId = _deleteCardCommandHandler.Execute(_dbContext, deleteCardCommand, _partnerId);

      // Assert
      Assert.AreEqual(deleteCardCommand.CardId, cardId);
    }

    [TestMethod]
    public void Execute_ShouldCreateDbDeleteEventLog()
    {
      // Setup
      _dbEventLoggerMock.Setup(logger => logger.LogEvent(_partnerId, ctx => ctx.Cards,
          It.IsAny<long>(), null, It.IsAny<Card>()))
        .Verifiable();

      var testCardCode = "test";
      var card = _dbContext.Cards.Add(new Card { Code = testCardCode });
      _dbContext.SaveChanges();

      // Arrange
      var deleteCardCommand = new DeleteCardCommand
      {
        CardId = card.Id
      };

      // Act
      _deleteCardCommandHandler.Execute(_dbContext, deleteCardCommand, _partnerId);

      // Assert
      _dbEventLoggerMock.Verify(logger => logger.LogEvent(_partnerId, ctx => ctx.Cards,
          It.IsAny<long>(), null, It.IsAny<Card>()), Times.Once);
    }
  }
}
