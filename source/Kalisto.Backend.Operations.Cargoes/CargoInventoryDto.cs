﻿namespace Kalisto.Backend.Operations.Cargoes
{
  public class CargoInventoryDto
  {
    public long Id { get; set; }

    public string Name { get; set; }

    public decimal? Price { get; set; }

    public decimal? ActualAmount { get; set; }
  }
}
