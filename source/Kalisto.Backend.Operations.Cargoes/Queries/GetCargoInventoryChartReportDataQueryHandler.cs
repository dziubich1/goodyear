﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Communication.Core.Messaging;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Kalisto.Backend.Operations.Cargoes.Queries
{
  public class GetCargoInventoryChartReportDataQueryHandler : QueryHandlerBase<GetCargoInventoryChartReportDataQuery>
  {
    public override object Execute(IDbContext context, GetCargoInventoryChartReportDataQuery query, string partnerId)
    {
      var measurements = context.Cargoes
        .Where(c => c.Id == query.CargoId)
        .Include(c =>
          c.Measurements
          )
        .SelectMany(c => c.Measurements)
        .AsNoTracking()
        .ToList();

      var initialState = measurements
       .Where(m => m.Date1Utc < query.DateFrom)
       .Where(m => (m.Date2Utc == null && m.Type == MeasurementType.Single) || m.Date2Utc < query.DateFrom)
       .Sum(m => GetMeasurementNewWeightValueBasedOnMeasurementClass(m));

      var dayBeforeReportDateFrom = query.DateFrom.AddDays(-1).Date;
      var result = new List<CargoInventoryReportDto>();

      result.Add(new CargoInventoryReportDto
      {
        Date = dayBeforeReportDateFrom,
        Amount = initialState
      });

      var i = 0;
      for (var date = query.DateFrom.Date; date <= query.DateTo.Date; date = date.AddDays(1))
      {
        var previousAmount = result[i].Amount;

        var dailyAmount = measurements
          .Where(d => d.Date2Utc == null ? d.Date1Utc.Date == date : d.Date2Utc.Value.Date == date)
          .Sum(m => GetMeasurementNewWeightValueBasedOnMeasurementClass(m));

        var actualAmount = dailyAmount + previousAmount;
        result.Add(new CargoInventoryReportDto
        {
          Date = date,
          Amount = actualAmount
        });
        i++;
      }
      result.RemoveAt(0);
      return result;
    }

    private decimal GetMeasurementNewWeightValueBasedOnMeasurementClass(Measurement measurement)
    {
      return measurement.Class == MeasurementClass.Income ? measurement.NetWeight : measurement.Class == MeasurementClass.Outcome ? -measurement.NetWeight : 0M;
    }
  }
}
