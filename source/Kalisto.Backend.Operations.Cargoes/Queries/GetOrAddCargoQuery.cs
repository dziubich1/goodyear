﻿using Kalisto.Communication.Core.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kalisto.Backend.Operations.Cargoes.Queries
{
  public class GetOrAddCargoQuery : BackendDbOperationMessage
  {
    public string Name { get; set; }
  }
}
