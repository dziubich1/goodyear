﻿using System.Data.Entity;
using Kalisto.Backend.DataAccess.Model;
using Kalisto.Communication.Core.Messaging;
using System.Linq;

namespace Kalisto.Backend.Operations.Cargoes.Queries
{
  public class GetAllCargoesQueryHandler : QueryHandlerBase<GetAllCargoesQuery>
  {
    public override object Execute(IDbContext context, GetAllCargoesQuery query, string partnerId)
    {
      return context.Cargoes.AsNoTracking().ToList();
    }
  }
}
