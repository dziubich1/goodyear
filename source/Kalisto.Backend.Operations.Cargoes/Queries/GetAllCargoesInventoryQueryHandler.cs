﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Communication.Core.Messaging;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
namespace Kalisto.Backend.Operations.Cargoes.Queries
{
  public class GetAllCargoesInventoryQueryHandler : QueryHandlerBase<GetAllCargoesInventoryQuery>
  {
    public override object Execute(IDbContext context, GetAllCargoesInventoryQuery query, string partnerId)
    {
      var result = new List<CargoInventoryDto>();

      context.Cargoes
        .Include(c => c.Measurements)
        .AsNoTracking()
        .ToList()
        .ForEach(c =>
        {
          result.Add(new CargoInventoryDto
          {
            Id = c.Id,
            Name = c.Name,
            Price = c.Price,
            ActualAmount = c.Measurements?.Sum(m => (m.Class == MeasurementClass.Income) ? (m.NetWeight) : (m.Class == MeasurementClass.Outcome) ? -(m.NetWeight) : 0M)
          });
        });

      return result;
    }
  }
}
