﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.DataAccess.Model.Logging;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kalisto.Backend.Operations.Cargoes.Queries
{
  public class GetOrAddCargoQueryHandler : QueryHandlerBase<GetOrAddCargoQuery>
  {
    private readonly IDbInsertEventLogger _dbEventLogger;
    private readonly ILogger _logger;

    public GetOrAddCargoQueryHandler(IDbInsertEventLogger dbEventLogger, ILogger logger)
    {
      _dbEventLogger = dbEventLogger;
      _logger = logger;
    }

    public override object Execute(IDbContext context, GetOrAddCargoQuery query, string partnerId)
    {
      var cargo = context.Cargoes.FirstOrDefault(x => x.Name.ToLower().Equals(query.Name.ToLower()));

      if (cargo == null)
      {
        cargo = context.Cargoes.Add(new Cargo() { Name = query.Name });
        context.SaveChanges();
        _dbEventLogger.LogEvent(partnerId, ctx => ctx.Cargoes, cargo.Id, cargo);
        _logger.Debug($"Successfully created cargo with ID: {cargo.Id}");
      }

      return cargo;
    }
  }
}
