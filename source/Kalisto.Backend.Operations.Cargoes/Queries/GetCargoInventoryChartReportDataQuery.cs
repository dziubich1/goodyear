﻿using Kalisto.Communication.Core.Messages;
using System;

namespace Kalisto.Backend.Operations.Cargoes.Queries
{
  public class GetCargoInventoryChartReportDataQuery : BackendDbOperationMessage
  {
    public long CargoId { get; set; }

    public DateTime DateFrom { get; set; }

    public DateTime DateTo { get; set; }
  }
}
