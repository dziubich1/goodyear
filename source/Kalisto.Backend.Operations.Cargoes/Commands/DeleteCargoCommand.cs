﻿using Kalisto.Backend.Operations.Cargoes.Commands.Interfaces;
using Kalisto.Communication.Core.Messages;

namespace Kalisto.Backend.Operations.Cargoes.Commands
{
  public class DeleteCargoCommand : BackendDbOperationMessage, ICargoCommand
  {
    public long CargoId { get; set; }
  }
}
