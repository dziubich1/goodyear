﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.DataAccess.Model.Logging;
using Kalisto.Backend.Operations.Cargoes.Exceptions;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Logging;
using System;
using System.Data.Entity;
using System.Linq;

namespace Kalisto.Backend.Operations.Cargoes.Commands
{
  public class UpdateCargoCommandHandler : CommandHandlerBase<UpdateCargoCommand>
  {
    private readonly IDbUpdateEventLogger _dbEventLogger;
    private readonly ILogger _logger;

    public UpdateCargoCommandHandler(IDbUpdateEventLogger dbEventLogger, ILogger logger)
    {
      _dbEventLogger = dbEventLogger;
      _logger = logger;
    }

    public override object Execute(IDbContext context, UpdateCargoCommand command, string partnerId)
    {
      var cargo = context.Cargoes.FirstOrDefault(c => c.Id == command.CargoId);
      if (cargo == null)
      {
        _logger.Error($"Cannot update cargo with ID: {command.CargoId} because it does not exist");
        throw new ArgumentException("Cargo with given identifier does not exist.");
      }

      if (context.Cargoes.Any(
        c => c.Id != cargo.Id && c.Name.ToLower() == command.Name.ToLower()))
      {
        _logger.Info($"Cannot update cargo name because cargo with name: {command.Name} already exists. Updated cargo ID: {cargo.Id}");
        throw new CargoExistsException();
      }

      // only for getting current cargo old values
      var oldCargo = context.Cargoes.AsNoTracking()
        .FirstOrDefault(c => c.Id == command.CargoId);

      cargo.Name = command.Name;
      cargo.Price = command.Price;

      context.SaveChanges();

      _dbEventLogger.LogEvent(partnerId, ctx => ctx.Cargoes, cargo.Id, cargo, oldCargo);
      _logger.Debug($"Successfully updated cargo with ID: {cargo.Id}");

      return cargo.Id;
    }
  }
}
