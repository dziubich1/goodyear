﻿using Kalisto.Backend.Operations.Cargoes.Commands.Interfaces;
using Kalisto.Communication.Core.Messages;

namespace Kalisto.Backend.Operations.Cargoes.Commands
{
  public class CreateCargoCommand : BackendDbOperationMessage, ICargoCommand
  {
    public string Name { get; set; }

    public decimal? Price { get; set; }
  }
}
