﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.DataAccess.Model.Logging;
using Kalisto.Backend.Operations.Cargoes.Exceptions;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Logging;
using System.Linq;

namespace Kalisto.Backend.Operations.Cargoes.Commands
{
  public class CreateCargoCommandHandler : CommandHandlerBase<CreateCargoCommand>
  {
    private readonly IDbInsertEventLogger _dbEventLogger;
    private readonly ILogger _logger;

    public CreateCargoCommandHandler(IDbInsertEventLogger dbEventLogger, ILogger logger)
    {
      _dbEventLogger = dbEventLogger;
      _logger = logger;
    }

    public override object Execute(IDbContext context, CreateCargoCommand command, string partnerId)
    {
      var cargoNameNormalized = command.Name.ToLower();
      if (context.Cargoes.Any(c => c.Name.ToLower() == cargoNameNormalized))
      {
        _logger.Debug($"Cannot create cargo with name: {command.Name} because it already exists");
        throw new CargoExistsException();
      }

      var cargo = new Cargo
      {
        Name = command.Name,
        Price = command.Price
      };

      context.Cargoes.Add(cargo);
      context.SaveChanges();
      _dbEventLogger.LogEvent(partnerId, ctx => ctx.Cargoes, cargo.Id, cargo);
      _logger.Debug($"Successfully created cargo with ID: {cargo.Id}");

      return cargo.Id;
    }
  }
}
