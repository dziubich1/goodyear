﻿using Kalisto.Backend.Operations.Cargoes.Commands.Interfaces;
using Kalisto.Communication.Core.Messages;

namespace Kalisto.Backend.Operations.Cargoes.Commands
{
  public class UpdateCargoCommand : BackendDbOperationMessage, ICargoCommand
  {
    public long CargoId { get; set; }

    public string Name { get; set; }

    public decimal? Price { get; set; }
  }
}
