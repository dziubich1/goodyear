﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.DataAccess.Model.Logging;
using Kalisto.Backend.Operations.Cargoes.Exceptions;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Logging;
using System;
using System.Data.Entity;
using System.Linq;

namespace Kalisto.Backend.Operations.Cargoes.Commands
{
  public class DeleteCargoCommandHandler : CommandHandlerBase<DeleteCargoCommand>
  {
    private readonly IDbDeleteEventLogger _dbEventLogger;
    private readonly ILogger _logger;

    public DeleteCargoCommandHandler(IDbDeleteEventLogger dbEventLogger, ILogger logger)
    {
      _dbEventLogger = dbEventLogger;
      _logger = logger;
    }

    public override object Execute(IDbContext context, DeleteCargoCommand command, string partnerId)
    {
      var cargo = context.Cargoes
        .Include(c => c.Measurements)
        .Include(c => c.Cards)
        .FirstOrDefault(c => c.Id == command.CargoId);

      if (cargo == null)
      {
        _logger.Error($"Cannot delete cargo with ID: {command.CargoId} because it does not exist");
        throw new ArgumentException("Cargo with given identifier does not exist.");
      }

      if (cargo.Measurements.Count > 0)
      {
        _logger.Debug($"Cannot delete cargo with ID: {command.CargoId} because it is used in measurements");
        throw new CargoLockedByMeasurementException();
      }
      if (cargo.Cards.Count > 0)
      {
        _logger.Debug($"Cannot delete cargo with ID: {command.CargoId} because it is used in cards");
        throw new CargoLockedByCardException();
      }

      context.Cargoes.Remove(cargo);
      context.SaveChanges();

      _dbEventLogger.LogEvent(partnerId, ctx => ctx.Cargoes, cargo.Id, null, cargo);
      _logger.Debug($"Successfully deleted cargo with ID: {cargo.Id}");

      return command.CargoId;
    }
  }
}
