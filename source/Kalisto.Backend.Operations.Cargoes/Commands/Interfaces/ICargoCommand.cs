﻿using Kalisto.Communication.Core.Messages;

namespace Kalisto.Backend.Operations.Cargoes.Commands.Interfaces
{
  public interface ICargoCommand : IAssociatedCommand
  {
  }
}
