﻿using Kalisto.Communication.Core.Exceptions;
using System;
using System.Runtime.Serialization;

namespace Kalisto.Backend.Operations.Cargoes.Exceptions
{
  [Serializable]
  public class CargoExistsException : ExceptionBase
  {
    public CargoExistsException()
    {
    }

    // REMARKS: below constructor need to be added due to the serialization/deserialization issues
    public CargoExistsException(SerializationInfo info, StreamingContext context)
    {
    }

    public override string ExceptionCode => "CARGOES_1001";

    public override string Message => GetLocalizedMessage();
  }
}
