﻿using Kalisto.Communication.Core.Exceptions;
using System;
using System.Runtime.Serialization;

namespace Kalisto.Backend.Operations.Cargoes.Exceptions
{
  [Serializable]
  public class CargoLockedByMeasurementException : ExceptionBase
  {
    public CargoLockedByMeasurementException()
    {
    }

    // REMARKS: below constructor need to be added due to the serialization/deserialization issues
    public CargoLockedByMeasurementException(SerializationInfo info, StreamingContext context)
    {
    }

    public override string ExceptionCode => "CARGOES_1002";

    public override string Message => GetLocalizedMessage();
  }
}
