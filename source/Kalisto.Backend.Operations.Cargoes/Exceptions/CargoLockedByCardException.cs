﻿using Kalisto.Communication.Core.Exceptions;
using System;
using System.Runtime.Serialization;

namespace Kalisto.Backend.Operations.Cargoes.Exceptions
{
  [Serializable]
  public class CargoLockedByCardException : ExceptionBase
  {
    public CargoLockedByCardException()
    {
    }

    // REMARKS: below constructor need to be added due to the serialization/deserialization issues
    public CargoLockedByCardException(SerializationInfo info, StreamingContext context)
    {
    }

    public override string ExceptionCode => "CARGOES_1003";

    public override string Message => GetLocalizedMessage();
  }
}
