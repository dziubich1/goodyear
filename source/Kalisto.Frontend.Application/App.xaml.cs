﻿using Caliburn.Micro;
using MahApps.Metro;
using System;
using System.Globalization;
using System.Windows;

namespace Kalisto.Frontend.Application
{
  /// <summary>
  /// Interaction logic for App.xaml
  /// </summary>
  public partial class App : System.Windows.Application
  {
    public App()
    {
    }

    protected override void OnStartup(StartupEventArgs e)
    {
      ThemeManager.AddAccent("KalistoTheme", new Uri("pack://application:,,,/Kalisto.Hmi.Dialogs.Common;component/KalistoTheme.xaml"));
      Tuple<AppTheme, Accent> theme = ThemeManager.DetectAppStyle(Current);
      ThemeManager.ChangeAppStyle(Current,
        ThemeManager.GetAccent("KalistoTheme"),
        theme.Item1);

      base.OnStartup(e);
    }
  }
}
