﻿using Kalisto.Configuration.Devices;
using Kalisto.Devices.Core.Communication;
using Kalisto.Devices.Endpoint;
using Kalisto.Hmi.Core;
using Kalisto.Hmi.Dialogs.BeltConveyorMeasurements.ViewModels;
using Kalisto.Hmi.Dialogs.Cards.ViewModels;
using Kalisto.Hmi.Dialogs.Cargoes.ViewModels;
using Kalisto.Hmi.Dialogs.Common.Dialogs;
using Kalisto.Hmi.Dialogs.Contractors.ViewModels;
using Kalisto.Hmi.Dialogs.Dashboard.ViewModels;
using Kalisto.Hmi.Dialogs.DbEventLogs.ViewModels;
using Kalisto.Hmi.Dialogs.Drivers.ViewModels;
using Kalisto.Hmi.Dialogs.Measurements.ViewModels;
using Kalisto.Hmi.Dialogs.Shell.ViewModels;
using Kalisto.Hmi.Dialogs.SplashScreen;
using Kalisto.Hmi.Dialogs.SystemEventLogs.ViewModels;
using Kalisto.Hmi.Dialogs.Users.ViewModels;
using Kalisto.Hmi.Dialogs.Vehicles.ViewModels;
using Kalisto.Logging;
using log4net.Config;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Windows;
using Kalisto.Communication.Core.Jobs.Base;
using Unity;

namespace Kalisto.Frontend.Application
{
  public class Bootstrapper : UnityBootstraper<ShellViewModel>
  {
    private FrontendEndpoint _endpoint;

    private DeviceHost _barcodeScannerHost;

    protected override void Configure()
    {
      Application.ShutdownMode = ShutdownMode.OnExplicitShutdown;

      base.Configure();

      XmlConfigurator.Configure();

      var appName = Assembly.GetExecutingAssembly().GetName().Name;
      _endpoint = new FrontendEndpoint(Container, "Kalisto", appName);
      _endpoint.ConfigurationLoaded += OnConfigurationLoaded;
      _endpoint.Configure();
    }

    private void OnConfigurationLoaded(FrontendConfiguration configuration)
    {
      var language = configuration.Language ?? "pl-PL";
      var culture = new CultureInfo(language);
      CultureInfo.CurrentCulture = culture;
      CultureInfo.CurrentUICulture = culture;
      CultureInfo.DefaultThreadCurrentCulture = culture;
      WPFLocalizeExtension.Engine.LocalizeDictionary.Instance.SetCurrentThreadCulture = true;
      WPFLocalizeExtension.Engine.LocalizeDictionary.Instance.Culture = culture;
    }

    protected override IEnumerable<Assembly> SelectAssemblies()
    {
      var assemblies = base.SelectAssemblies().ToList();
      assemblies.Add(typeof(ImagePreviewWindowDialogViewModel).Assembly);
      assemblies.Add(typeof(ShellViewModel).Assembly);
      assemblies.Add(typeof(UsersViewModel).Assembly);
      assemblies.Add(typeof(DashboardViewModel).Assembly);
      assemblies.Add(typeof(SystemEventLogsViewModel).Assembly);
      assemblies.Add(typeof(DbEventLogsViewModel).Assembly);
      assemblies.Add(typeof(ContractorsViewModel).Assembly);
      assemblies.Add(typeof(CargoesViewModel).Assembly);
      assemblies.Add(typeof(DriversViewModel).Assembly);
      assemblies.Add(typeof(VehiclesViewModel).Assembly);
      assemblies.Add(typeof(MeasurementsViewModel).Assembly);
      assemblies.Add(typeof(CardsViewModel).Assembly);
      assemblies.Add(typeof(BeltConveyorMeasurementsViewModel).Assembly);

      return assemblies;
    }

    protected override bool DisplaySplashScreen()
    {
      var jobSchedulerInfo = Container.Resolve<IInitializationJobSchedulerInfo>();
      var splashScreenDialog = new SplashScreenView(jobSchedulerInfo,
        onShowAction: _endpoint.Initialize);

      var result = splashScreenDialog.ShowDialog();
      return result ?? false;
    }

    protected override void SetupLocalDevices()
    {
      base.SetupLocalDevices();

      var configuration = Container.Resolve<BarcodeScannerConfiguration>();
      var connectionFactory = Container.Resolve<IDeviceConnectionFactory>();
      var logger = Container.Resolve<ILogger>();
      _barcodeScannerHost = new DeviceHost(Container, logger, connectionFactory);
      _barcodeScannerHost.Configure(configuration);
      _barcodeScannerHost.Run();
    }

    protected override void OnExit(object sender, EventArgs e)
    {
      var logger = Container.Resolve<ILogger>();
      if (logger != null)
        logger.Info("Application is closing");

      _barcodeScannerHost?.Stop();
      _endpoint.Dispose();
      Environment.Exit(0);
    }
  }
}
