﻿using Kalisto.Communication.Core.Exceptions;
using System;
using System.Runtime.Serialization;

namespace Kalisto.Backend.Operations.Contractors.Exceptions
{
  [Serializable]
  public class ContractorLockedByVehicleException : ExceptionBase
  {
    public ContractorLockedByVehicleException()
    {
    }

    // REMARKS: below constructor need to be added due to the serialization/deserialization issues
    public ContractorLockedByVehicleException(SerializationInfo info, StreamingContext context)
    {
    }

    public override string ExceptionCode => "CONTRACTORS_1004";

    public override string Message => GetLocalizedMessage();
  }
}
