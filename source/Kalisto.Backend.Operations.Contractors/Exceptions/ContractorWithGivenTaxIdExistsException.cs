﻿using Kalisto.Communication.Core.Exceptions;
using System;
using System.Runtime.Serialization;

namespace Kalisto.Backend.Operations.Contractors.Exceptions
{
  [Serializable]
  public class ContractorWithGivenTaxIdExistsException : ExceptionBase
  {
    public ContractorWithGivenTaxIdExistsException()
    {
    }

    // REMARKS: below constructor need to be added due to the serialization/deserialization issues
    public ContractorWithGivenTaxIdExistsException(SerializationInfo info, StreamingContext context)
    {
    }

    public override string ExceptionCode => "CONTRACTORS_1001";

    public override string Message => GetLocalizedMessage();
  }
}
