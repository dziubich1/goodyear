﻿using Kalisto.Communication.Core.Exceptions;
using System;
using System.Runtime.Serialization;

namespace Kalisto.Backend.Operations.Contractors.Exceptions
{
  [Serializable]
  public class ContractorLockedByCardException : ExceptionBase
  {
    public ContractorLockedByCardException()
    {
    }

    // REMARKS: below constructor need to be added due to the serialization/deserialization issues
    public ContractorLockedByCardException(SerializationInfo info, StreamingContext context)
    {
    }

    public override string ExceptionCode => "CONTRACTORS_1003";

    public override string Message => GetLocalizedMessage();
  }
}
