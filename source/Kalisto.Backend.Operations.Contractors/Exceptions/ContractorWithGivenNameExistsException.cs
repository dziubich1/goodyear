﻿using Kalisto.Communication.Core.Exceptions;
using System;
using System.Runtime.Serialization;

namespace Kalisto.Backend.Operations.Contractors.Exceptions
{
  [Serializable]
  public class ContractorWithGivenNameExistsException : ExceptionBase
  {
    public ContractorWithGivenNameExistsException() 
    {

    }

    // REMARKS: below constructor need to be added due to the serialization/deserialization issues
    public ContractorWithGivenNameExistsException(SerializationInfo info, StreamingContext context)
    {
    }

    public override string ExceptionCode => "CONTRACTORS_1005";

    public override string Message => GetLocalizedMessage();
  }
}
