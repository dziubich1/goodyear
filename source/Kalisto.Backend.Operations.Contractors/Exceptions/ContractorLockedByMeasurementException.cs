﻿using Kalisto.Communication.Core.Exceptions;
using System;
using System.Runtime.Serialization;

namespace Kalisto.Backend.Operations.Contractors.Exceptions
{
  [Serializable]
  public class ContractorLockedByMeasurementException : ExceptionBase
  {
    public ContractorLockedByMeasurementException()
    {
    }

    // REMARKS: below constructor need to be added due to the serialization/deserialization issues
    public ContractorLockedByMeasurementException(SerializationInfo info, StreamingContext context)
    {
    }

    public override string ExceptionCode => "CONTRACTORS_1002";

    public override string Message => GetLocalizedMessage();
  }
}
