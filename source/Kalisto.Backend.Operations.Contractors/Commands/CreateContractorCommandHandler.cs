﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.DataAccess.Model.Logging;
using Kalisto.Backend.Operations.Contractors.Exceptions;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Logging;
using System.Linq;

namespace Kalisto.Backend.Operations.Contractors.Commands
{
  public class CreateContractorCommandHandler : CommandHandlerBase<CreateContractorCommand>
  {
    private readonly IDbInsertEventLogger _dbEventLogger;
    private readonly ILogger _logger;

    public CreateContractorCommandHandler(IDbInsertEventLogger dbEventLogger, ILogger logger)
    {
      _dbEventLogger = dbEventLogger;
      _logger = logger;
    }

    public override object Execute(IDbContext context, CreateContractorCommand command, string partnerId)
    {
      if (!string.IsNullOrEmpty(command.TaxId) && context.Contractors.Any(c => c.TaxId == command.TaxId))
      {
        _logger.Debug($"Cannot create contractor with tax id: {command.TaxId} because it already exists");
        throw new ContractorWithGivenTaxIdExistsException();
      }
      else if (string.IsNullOrEmpty(command.TaxId) && context.Contractors.Any(c => c.Name == command.Name))
      {
        _logger.Debug($"Cannot create contractor with name: {command.Name} because it already exists");
        throw new ContractorWithGivenNameExistsException();
      }

      var contractor = new Contractor
      {
        Name = command.Name,
        TaxId = command.TaxId,
        Address = command.Address,
        City = command.City,
        PostalCode = command.PostalCode
      };

      context.Contractors.Add(contractor);
      context.SaveChanges();
      // TODO: We are loosing capacity because of long to int casting
      _dbEventLogger.LogEvent(partnerId, ctx => ctx.Contractors, contractor.Id, contractor);
      _logger.Debug($"Successfully created contractor with ID: {contractor.Id}");

      return contractor.Id;
    }
  }
}
