﻿using Kalisto.Backend.Operations.Contractors.Commands.Interfaces;
using Kalisto.Communication.Core.Messages;

namespace Kalisto.Backend.Operations.Contractors.Commands
{
  public class DeleteContractorCommand : BackendDbOperationMessage, IContractorCommand
  {
    public long ContractorId { get; set; }
  }
}
