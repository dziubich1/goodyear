﻿using Kalisto.Communication.Core.Messages;

namespace Kalisto.Backend.Operations.Contractors.Commands.Interfaces
{
  public interface IContractorCommand : IAssociatedCommand
  {
  }
}
