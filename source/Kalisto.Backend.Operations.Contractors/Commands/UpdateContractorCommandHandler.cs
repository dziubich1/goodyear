﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.DataAccess.Model.Logging;
using Kalisto.Backend.Operations.Contractors.Exceptions;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Logging;
using System;
using System.Data.Entity;
using System.Linq;

namespace Kalisto.Backend.Operations.Contractors.Commands
{
  public class UpdateContractorCommandHandler : CommandHandlerBase<UpdateContractorCommand>
  {
    private readonly IDbUpdateEventLogger _dbEventLogger;
    private readonly ILogger _logger;

    public UpdateContractorCommandHandler(IDbUpdateEventLogger dbEventLogger, ILogger logger)
    {
      _dbEventLogger = dbEventLogger;
      _logger = logger;
    }

    public override object Execute(IDbContext context, UpdateContractorCommand command, string partnerId)
    {
      var contractor = context.Contractors.FirstOrDefault(c => c.Id == command.ContractorId);
      if (contractor == null)
      {
        _logger.Error($"Cannot update contractor with ID: {command.ContractorId} because it does not exist");
        throw new ArgumentException("Contractor with given identifier does not exist.");
      }

      if (!string.IsNullOrEmpty(command.TaxId) && context.Contractors.Any(c => c.Id != contractor.Id && c.TaxId == command.TaxId))
      { 
        _logger.Info($"Cannot update contractor tax id because contractor with tax id: {command.TaxId} already exists. Updated contractor ID: {contractor.Id}");
        throw new ContractorWithGivenTaxIdExistsException();
      }
      else if(string.IsNullOrEmpty(command.TaxId) && context.Contractors.Any(c => c.Name == command.Name))
      {
        _logger.Info($"Cannot update contractor name because contractor with name: {command.Name} already exists. Updated contractor ID: {contractor.Id}");
        throw new ContractorWithGivenNameExistsException();
      }

      // only for getting current user old values
      var oldContractor = context.Contractors.AsNoTracking()
        .FirstOrDefault(c => c.Id == command.ContractorId);

      contractor.Name = command.Name;
      contractor.Address = command.Address;
      contractor.PostalCode = command.PostalCode;
      contractor.City = command.City;
      contractor.TaxId = command.TaxId;

      context.SaveChanges();

      _dbEventLogger.LogEvent(partnerId, ctx => ctx.Contractors, contractor.Id, contractor, oldContractor);
      _logger.Debug($"Successfully updated contractor with ID: {contractor.Id}");

      return contractor.Id;
    }
  }
}
