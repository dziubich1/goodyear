﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.DataAccess.Model.Logging;
using Kalisto.Backend.Operations.Contractors.Exceptions;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Logging;
using System;
using System.Data.Entity;
using System.Linq;

namespace Kalisto.Backend.Operations.Contractors.Commands
{
  public class DeleteContractorCommandHandler : CommandHandlerBase<DeleteContractorCommand>
  {
    private readonly IDbDeleteEventLogger _dbEventLogger;
    private readonly ILogger _logger;

    public DeleteContractorCommandHandler(IDbDeleteEventLogger dbEventLogger, ILogger logger)
    {
      _dbEventLogger = dbEventLogger;
      _logger = logger;
    }

    public override object Execute(IDbContext context, DeleteContractorCommand command, string partnerId)
    {
      var contractor = context.Contractors
        .Include(c => c.Measurements)
        .Include(c => c.Cards)
        .Include(c => c.Vehicles)
        .FirstOrDefault(c => c.Id == command.ContractorId);

      if (contractor == null)
      {
        _logger.Info($"Cannot delete contractor with ID: {command.ContractorId} because it does not exist");
        throw new ArgumentException("Contractor with given identifier does not exist.");
      }

      if (contractor.Measurements.Count > 0)
      {
        _logger.Debug($"Cannot delete contractor with ID: {command.ContractorId} because it is used in measurements");
        throw new ContractorLockedByMeasurementException();
      }

      if (contractor.Cards.Count > 0)
      {
        _logger.Debug($"Cannot delete contractor with ID: {command.ContractorId} because it is used in cards");
        throw new ContractorLockedByCardException();
      }

      if (contractor.Vehicles.Count > 0)
      {
        _logger.Debug($"Cannot delete contractor with ID: {command.ContractorId} because it is used in vehicles");
        throw new ContractorLockedByVehicleException();
      }

      context.Contractors.Remove(contractor);
      context.SaveChanges();

      _dbEventLogger.LogEvent(partnerId, ctx => ctx.Contractors, contractor.Id, null, contractor);
      _logger.Debug($"Successfully deleted contractor with ID: {contractor.Id}");

      return command.ContractorId;
    }
  }
}
