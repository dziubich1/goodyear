﻿using Kalisto.Backend.Operations.Contractors.Commands.Interfaces;
using Kalisto.Communication.Core.Messages;

namespace Kalisto.Backend.Operations.Contractors.Commands
{
  public class CreateContractorCommand : BackendDbOperationMessage, IContractorCommand
  {
    public string Name { get; set; }

    public string Address { get; set; }

    public string PostalCode { get; set; }

    public string City { get; set; }

    public string TaxId { get; set; }
  }
}
