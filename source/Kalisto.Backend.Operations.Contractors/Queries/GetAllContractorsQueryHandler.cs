﻿using System.Data.Entity;
using Kalisto.Backend.DataAccess.Model;
using Kalisto.Communication.Core.Messaging;
using System.Linq;

namespace Kalisto.Backend.Operations.Contractors.Queries
{
  public class GetAllContractorsQueryHandler : QueryHandlerBase<GetAllContractorsQuery>
  {
    public override object Execute(IDbContext context, GetAllContractorsQuery query, string partnerId)
    {
      return context.Contractors.AsNoTracking().ToList();
    }
  }
}
