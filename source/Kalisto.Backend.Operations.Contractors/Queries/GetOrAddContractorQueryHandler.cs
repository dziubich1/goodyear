﻿using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.DataAccess.Model.Logging;
using Kalisto.Communication.Core.Messaging;
using Kalisto.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kalisto.Backend.Operations.Contractors.Queries
{
  public class GetOrAddContractorQueryHandler : QueryHandlerBase<GetOrAddContractorQuery>
  {
    private readonly IDbInsertEventLogger _dbEventLogger;
    private readonly ILogger _logger;

    public GetOrAddContractorQueryHandler(IDbInsertEventLogger dbEventLogger, ILogger logger)
    {
      _dbEventLogger = dbEventLogger;
      _logger = logger;
    }

    public override object Execute(IDbContext context, GetOrAddContractorQuery query, string partnerId)
    {
      var contractor = context.Contractors.FirstOrDefault(x => x.Name.ToLower().Equals(query.Name.ToLower()));

      if (contractor == null)
      {
        contractor = context.Contractors.Add(new Contractor() { Name = query.Name });
        context.SaveChanges();
        _dbEventLogger.LogEvent(partnerId, ctx => ctx.Contractors, contractor.Id, contractor);
        _logger.Debug($"Successfully created contractor with ID: {contractor.Id}");
      }

      return contractor;
    }
  }
}
