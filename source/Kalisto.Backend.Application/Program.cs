﻿using System;
using log4net.Config;

namespace Kalisto.Backend.Application
{
  class Program
  {
    static void Main(string[] args)
    {
      XmlConfigurator.Configure();

      Console.WriteLine("Kalisto.Backend has been started successfully.");
      Console.WriteLine("Press ENTER to stop.");

      var appName = typeof(ServerEndpoint).Assembly.GetName().Name;
      var endpoint = new ServerEndpoint("Kalisto", appName);
      endpoint.Configure();
      endpoint.Initialize();

      Console.ReadLine();
    }
  }
}
