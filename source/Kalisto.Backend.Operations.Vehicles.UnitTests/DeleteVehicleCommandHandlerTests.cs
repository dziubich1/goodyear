﻿using Kalisto.Backend.DataAccess.Effort;
using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.DataAccess.Model.Logging;
using Kalisto.Backend.Operations.Vehicles.Commands;
using Kalisto.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Linq;

namespace Kalisto.Backend.Operations.Vehicles.UnitTests
{
  [TestClass]
  public class DeleteVehicleCommandHandlerTests
  {
    private IDbContext _dbContext;

    private readonly string _partnerId = "unknown";

    private Mock<IDbDeleteEventLogger> _dbEventLoggerMock;

    // component under tests
    private DeleteVehicleCommandHandler _deleteVehicleCommandHandler;

    [TestInitialize]
    public void Initialize()
    {
      var effortConnection = Effort.DbConnectionFactory.CreateTransient();
      _dbContext = new EffortDbContext(effortConnection);
      _dbEventLoggerMock = new Mock<IDbDeleteEventLogger>();
      var logger = new Mock<ILogger>();
      _deleteVehicleCommandHandler = new DeleteVehicleCommandHandler(_dbEventLoggerMock.Object, logger.Object);
    }

    [TestCleanup]
    public void CleanUp()
    {
      _dbContext.Dispose();
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void Execute_ShouldThrowArgumentException_WhenVehicleWithGivenIdDoesNotExist()
    {
      // Setup
      var testPlateNumber = "test";
      _dbContext.Vehicles.Add(new Vehicle { PlateNumber = testPlateNumber }); // generated id: 1
      _dbContext.SaveChanges();

      // Arrange
      var command = new DeleteVehicleCommand
      {
        VehicleId = 2 // not existing user id
      };

      // Act & Assert
      _deleteVehicleCommandHandler.Execute(_dbContext, command, _partnerId);
    }

    [TestMethod]
    public void Execute_ShouldRemoveVehicle()
    {
      // Setup
      var testPlateNumber = "test";
      _dbContext.Vehicles.Add(new Vehicle { PlateNumber = testPlateNumber }); // generated id: 1
      _dbContext.SaveChanges();

      // Arrange
      var command = new DeleteVehicleCommand
      {
        VehicleId = 1
      };

      // Act
      _deleteVehicleCommandHandler.Execute(_dbContext, command, _partnerId);

      // Assert
      Assert.AreEqual(0, _dbContext.Vehicles.Count());
    }

    [TestMethod]
    public void Execute_ShouldCreateDbDeleteEventLog()
    {
      // Setup
      _dbEventLoggerMock.Setup(c => c.LogEvent(_partnerId, ctx => ctx.Vehicles,
          It.IsAny<long>(), null, It.IsAny<Vehicle>()))
        .Verifiable();

      var testPlateNumber = "test";
      _dbContext.Vehicles.Add(new Vehicle { PlateNumber = testPlateNumber }); // generated id: 1
      _dbContext.SaveChanges();

      // Arrange
      var command = new DeleteVehicleCommand
      {
        VehicleId = 1
      };

      // Act
      _deleteVehicleCommandHandler.Execute(_dbContext, command, "unknown");

      // Assert
      _dbEventLoggerMock.Verify(c => c.LogEvent(_partnerId, ctx => ctx.Vehicles,
        It.IsAny<long>(), null, It.IsAny<Vehicle>()), Times.Once);
    }
  }
}
