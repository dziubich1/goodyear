﻿using Kalisto.Backend.DataAccess.Effort;
using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.DataAccess.Model.Logging;
using Kalisto.Backend.Operations.Vehicles.Commands;
using Kalisto.Backend.Operations.Vehicles.Exceptions;
using Kalisto.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Linq;

namespace Kalisto.Backend.Operations.Vehicles.UnitTests
{
  [TestClass]
  public class UpdateVehicleCommandHandlerTests
  {
    private IDbContext _dbContext;

    private readonly string _partnerId = "unknown";

    private Mock<IDbUpdateEventLogger> _dbEventLoggerMock;

    // component under tests
    private UpdateVehicleCommandHandler _updateVehicleCommandHandler;

    [TestInitialize]
    public void Initialize()
    {
      var effortConnection = Effort.DbConnectionFactory.CreateTransient();
      _dbContext = new EffortDbContext(effortConnection);
      _dbEventLoggerMock = new Mock<IDbUpdateEventLogger>();
      var logger = new Mock<ILogger>();
      _updateVehicleCommandHandler = new UpdateVehicleCommandHandler(_dbEventLoggerMock.Object, logger.Object);
    }

    [TestCleanup]
    public void CleanUp()
    {
      _dbContext.Dispose();
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void Execute_ShouldThrowArgumentException_WhenVehicleWithGivenIdDoesNotExist()
    {
      // Arrange
      var notExistingVehicleId = 5;
      var command = new UpdateVehicleCommand { VehicleId = notExistingVehicleId };

      // Act & Assert
      _updateVehicleCommandHandler.Execute(_dbContext, command, _partnerId);
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void Execute_ShouldThrowArgumentException_WhenDriverWithGivenIdDoesNotExist()
    {
      // Setup
      var notExistingDriverId = 5;
      var currentPlateNumber = "currentPlateNumber";
      _dbContext.Vehicles.Add(new Vehicle
      {
        PlateNumber = currentPlateNumber
      }); // generated id: 1
      _dbContext.SaveChanges();

      // Arrange
      var newPlateNumber = "newPlateNumber";
      var command = new UpdateVehicleCommand
      {
        VehicleId = 1,
        PlateNumber = newPlateNumber,
        DriverId = notExistingDriverId
      };

      // Act & Assert
      _updateVehicleCommandHandler.Execute(_dbContext, command, _partnerId);
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void Execute_ShouldThrowArgumentException_WhenContractorWithGivenIdDoesNotExist()
    {
      // Setup
      var notExistingContracorId = 5;
      var currentPlateNumber = "currentPlateNumber";
      _dbContext.Vehicles.Add(new Vehicle
      {
        PlateNumber = currentPlateNumber
      }); // generated id: 1
      _dbContext.SaveChanges();

      // Arrange
      var newPlateNumber = "newPlateNumber";
      var command = new UpdateVehicleCommand
      {
        VehicleId = 1,
        PlateNumber = newPlateNumber,
        ContractorId = notExistingContracorId
      };

      // Act & Assert
      _updateVehicleCommandHandler.Execute(_dbContext, command, _partnerId);
    }

    [TestMethod]
    [ExpectedException(typeof(VehicleExistsException))]
    public void Execute_ShouldThrowVehicleExistsException_WhenPlateNumberOfCurrentVehicleHasChangedButVehicleWithSamePlateNumberAlreadyExists()
    {
      // Setup
      var testPlateNumber = "test";
      _dbContext.Vehicles.Add(new Vehicle { PlateNumber = testPlateNumber }); // generated id: 1
      var currentPlateNumber = "current";
      _dbContext.Vehicles.Add(new Vehicle { PlateNumber = currentPlateNumber }); // generated id: 2
      _dbContext.SaveChanges();

      // Arrange
      var command = new UpdateVehicleCommand { VehicleId = 2, PlateNumber = testPlateNumber };

      // Act & Assert
      _updateVehicleCommandHandler.Execute(_dbContext, command, _partnerId);
    }

    [TestMethod]
    public void Execute_ShouldUpdateVehicle()
    {
      // Setup
      var currentPlateNumber = "currentPlateNumber";
      _dbContext.Vehicles.Add(new Vehicle
      {
        PlateNumber = currentPlateNumber
      }); // generated id: 1
      _dbContext.SaveChanges();

      // Arrange
      var newPlateNumber = "newPlateNumber";
      var command = new UpdateVehicleCommand
      {
        VehicleId = 1,
        PlateNumber = newPlateNumber,
      };

      // Act
      _updateVehicleCommandHandler.Execute(_dbContext, command, _partnerId);

      // Assert
      var vehicle = _dbContext.Vehicles.Single();
      Assert.AreEqual(newPlateNumber, vehicle.PlateNumber);
    }

    [TestMethod]
    public void Execute_ShouldReturnUpdatedVehicleId()
    {
      // Setup
      var currentPlateNumber = "currentPlateNumber";
      _dbContext.Vehicles.Add(new Vehicle
      {
        PlateNumber = currentPlateNumber
      }); // generated id: 1
      _dbContext.SaveChanges();

      // Arrange
      var newPlateNumber = "newPlateNumber";
      var command = new UpdateVehicleCommand
      {
        VehicleId = 1,
        PlateNumber = newPlateNumber,
      };

      // Act
      var resultId = _updateVehicleCommandHandler.Execute(_dbContext, command, _partnerId);

      // 
      var vehicle = _dbContext.Vehicles.Single();
      Assert.AreEqual(1L, vehicle.Id);
      Assert.AreEqual(1L, resultId);
    }

    [TestMethod]
    public void Execute_ShouldCreateDbUpdateEventLog()
    {
      // Setup
      _dbEventLoggerMock.Setup(c => c.LogEvent(_partnerId, ctx => ctx.Vehicles,
          It.IsAny<long>(), It.IsAny<Vehicle>()))
        .Verifiable();

      _dbContext.Vehicles.Add(new Vehicle
      {
        PlateNumber = "currentPlateNumber"
      }); // generated id: 1
      _dbContext.SaveChanges();

      // Arrange
      var command = new UpdateVehicleCommand
      {
        VehicleId = 1,
        PlateNumber = "newPlateNumber"
      };

      // Act
      _updateVehicleCommandHandler.Execute(_dbContext, command, _partnerId);

      // Assert
      _dbEventLoggerMock.Verify(c => c.LogEvent(_partnerId, ctx => ctx.Vehicles,
        It.IsAny<long>(), It.IsAny<Vehicle>(), It.IsAny<Vehicle>()), Times.Once);
    }
  }
}
