﻿using Kalisto.Backend.DataAccess.Effort;
using Kalisto.Backend.DataAccess.Model;
using Kalisto.Backend.DataAccess.Model.Logging;
using Kalisto.Backend.Operations.Vehicles.Commands;
using Kalisto.Backend.Operations.Vehicles.Exceptions;
using Kalisto.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Linq;

namespace Kalisto.Backend.Operations.Vehicles.UnitTests
{
  [TestClass]
  public class CreateVehicleCommandHandlerTests
  {
    private IDbContext _dbContext;

    private readonly string _partnerId = "unknown";

    private Mock<IDbInsertEventLogger> _dbEventLoggerMock;

    // component under tests
    private CreateVehicleCommandHandler _createVehicleCommandHandler;

    [TestInitialize]
    public void Initialize()
    {
      var effortConnection = Effort.DbConnectionFactory.CreateTransient();
      _dbContext = new EffortDbContext(effortConnection);
      _dbEventLoggerMock = new Mock<IDbInsertEventLogger>();
      var logger = new Mock<ILogger>();
      _createVehicleCommandHandler = new CreateVehicleCommandHandler(_dbEventLoggerMock.Object, logger.Object);
    }

    [TestCleanup]
    public void CleanUp()
    {
      _dbContext.Dispose();
    }

    [TestMethod]
    [ExpectedException(typeof(VehicleExistsException))]
    public void Execute_ShouldThrowVehicleExistsException_WhenVehicleWithGivenPlateNumberAlreadyExists()
    {
      // Setup
      var testPlateNumber = "test";
      _dbContext.Vehicles.Add(new Vehicle { PlateNumber = testPlateNumber });
      _dbContext.SaveChanges();

      // Arrange
      var createCommand = new CreateVehicleCommand
      {
        PlateNumber = testPlateNumber
      };

      // Act & Assert
      _createVehicleCommandHandler.Execute(_dbContext,
        createCommand, _partnerId);
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void Execute_ShouldThrowArgumentException_WhenContractorWithGivenIdDoesNotExist()
    {
      // Setup
      var testPlateNumber = "test";
      var notExistingContractorId = 5;

      // Arrange
      var createCommand = new CreateVehicleCommand
      {
        PlateNumber = testPlateNumber,
        ContractorId = notExistingContractorId
      };

      // Act & Assert
      _createVehicleCommandHandler.Execute(_dbContext,
        createCommand, _partnerId);
    }

    [TestMethod]
    [ExpectedException(typeof(ArgumentException))]
    public void Execute_ShouldThrowArgumentException_WhenDriverWithGivenIdDoesNotExist()
    {
      // Setup
      var testPlateNumber = "test";
      var notExistingDriverId = 5;

      // Arrange
      var createCommand = new CreateVehicleCommand
      {
        PlateNumber = testPlateNumber,
        DriverId = notExistingDriverId
      };

      // Act & Assert
      _createVehicleCommandHandler.Execute(_dbContext,
        createCommand, _partnerId);
    }

    [TestMethod]
    public void Execute_ShouldNotCreateVehicle_WhenVehicleWithGivenPlateNumberAlreadyExists()
    {
      // Setup
      var testPlateNumber = "test";
      _dbContext.Vehicles.Add(new Vehicle { PlateNumber = testPlateNumber });
      _dbContext.SaveChanges();
      var expectedVehiclesCount = _dbContext.Vehicles.Count();

      // Arrange
      var createCommand = new CreateVehicleCommand
      {
        PlateNumber = testPlateNumber
      };

      try
      {
        // Act
        _createVehicleCommandHandler.Execute(_dbContext,
          createCommand, _partnerId);
      }
      catch (VehicleExistsException)
      {
        // Assert
        Assert.AreEqual(expectedVehiclesCount,
          _dbContext.Vehicles.Count());
      }
    }

    [TestMethod]
    public void Execute_ShouldCreateVehicle()
    {
      // Arrange
      var createCommand = new CreateVehicleCommand
      {
        PlateNumber = "TestPlateNumber"
      };

      // Act
      _createVehicleCommandHandler.Execute(_dbContext,
        createCommand, _partnerId);

      // Assert
      Assert.AreEqual(1, _dbContext.Vehicles.Count(),
        "Vehicles table should contain one entry");

      var createdVehicle = _dbContext.Vehicles.Single();
      Assert.AreEqual(1, createdVehicle.Id,
        "Id of created vehicle should be auto generated");
      Assert.AreEqual(createCommand.PlateNumber, createdVehicle.PlateNumber,
        "PlateNumber of created vehicle should be the same like in command");
    }

    [TestMethod]
    public void Execute_ShouldReturnIdentifierOfNewlyCreatedVehicle()
    {
      // Arrange
      var createCommand = new CreateVehicleCommand
      {
        PlateNumber = "TestPlateNumber"
      };

      // Act
      var id = _createVehicleCommandHandler.Execute(_dbContext,
        createCommand, _partnerId);

      // Assert
      var createdVehicle = _dbContext.Vehicles.Single();
      Assert.AreEqual(1L, id, "Id of created vehicle should be auto generated and returned properly by Execute(...) method");
      Assert.AreEqual(1L, createdVehicle.Id, "Id of created vehicle entity should be auto generated and returned properly by Execute(...) method");
    }

    [TestMethod]
    public void Execute_ShouldCreateDbInsertEventLog()
    {
      // Setup
      _dbEventLoggerMock.Setup(c => c.LogEvent(_partnerId, ctx => ctx.Vehicles,
          It.IsAny<long>(), It.IsAny<Vehicle>()))
        .Verifiable();

      // Arrange
      var createCommand = new CreateVehicleCommand
      {
        PlateNumber = "TestPlateNumber",
      };

      // Act
      _createVehicleCommandHandler.Execute(_dbContext,
        createCommand, _partnerId);

      // Assert
      _dbEventLoggerMock.Verify(c => c.LogEvent(_partnerId, ctx => ctx.Vehicles,
        1, It.IsAny<Vehicle>()), Times.Once);
    }
  }
}
